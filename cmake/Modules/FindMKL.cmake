# - Find Intel MKL
# Find the MKL libraries
#
# Options:
#
#   MKL_STATIC        :   use static linking
#   MKL_MULTI_THREADED:   use multi-threading
#   MKL_SDL           :   Single Dynamic Library interface
#
# This module defines the following variables:
#
#   MKL_FOUND            : True if MKL_INCLUDE_DIR are found
#   MKL_INCLUDE_DIR      : where to find mkl.h, etc.
#   MKL_INCLUDE_DIRS     : set when MKL_INCLUDE_DIR found
#   MKL_LIBRARIES        : the library to link against.


include(FindPackageHandleStandardArgs)

set(INTEL_ROOT $ENV{INTELROOT} CACHE PATH "Folder contains intel libs")
set(MKL_ROOT ${INTEL_ROOT}/mkl CACHE PATH "Folder contains MKL")

# Find include dir
find_path(MKL_INCLUDE_DIR mkl.h
    PATHS ${MKL_ROOT}/include)
find_path(MKL_LIB_DIR libmkl_rt.so PATHS ${MKL_ROOT}/lib/intel64/)

# Find include directory
#  There is no include folder under linux
if(WIN32)
    find_path(INTEL_INCLUDE_DIR omp.h
        PATHS ${INTEL_ROOT}/include)
    set(MKL_INCLUDE_DIR ${MKL_INCLUDE_DIR} ${INTEL_INCLUDE_DIR})
endif()

# Find libraries

# Handle suffix
set(_MKL_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES})

if(WIN32)
    if(MKL_STATIC)
        set(CMAKE_FIND_LIBRARY_SUFFIXES .lib)
    else()
        set(CMAKE_FIND_LIBRARY_SUFFIXES _dll.lib)
    endif()
else()
    if(MKL_STATIC)
        set(CMAKE_FIND_LIBRARY_SUFFIXES .a)
    else()
        set(CMAKE_FIND_LIBRARY_SUFFIXES .so)
    endif()
endif()


# MKL is composed by four layers: Interface, Threading, Computational and RTL

if(MKL_SDL)
    find_library(MKL_LIBRARY NAMES mkl_rt
        PATHS ${MKL_ROOT}/lib/intel64/)

    set(MKL_MINIMAL_LIBRARY ${MKL_LIBRARY})

    if(NOT MKL_LIBRARY)
        message(FATAL_ERROR "???")
    endif()
else()
    ######################### Interface layer #######################
    if(WIN32)
        set(MKL_INTERFACE_LIBNAME mkl_intel_c)
    else()
        set(MKL_INTERFACE_LIBNAME mkl_intel_lp64)
    endif()

    find_library(MKL_INTERFACE_LIBRARY NAMES ${MKL_INTERFACE_LIBNAME}
        PATHS ${MKL_ROOT}/lib/intel64/)

    ######################## Threading layer ########################
    if(MKL_MULTI_THREADED)
        set(MKL_THREADING_LIBNAME mkl_intel_thread)
    else()
        set(MKL_THREADING_LIBNAME mkl_sequential)
    endif()

    find_library(MKL_THREADING_LIBRARY NAMES ${MKL_THREADING_LIBNAME}
        PATHS ${MKL_ROOT}/lib/intel64/)

    ####################### Computational layer #####################
    find_library(MKL_CORE_LIBRARY NAMES mkl_core
        PATHS ${MKL_ROOT}/lib/intel64/)
    find_library(MKL_FFT_LIBRARY NAMES mkl_cdft_core
        PATHS ${MKL_ROOT}/lib/intel64/)
    find_library(MKL_SCALAPACK_LIBRARY NAMES mkl_scalapack_core
        PATHS ${MKL_ROOT}/lib/intel64/)

    ############################ RTL layer ##########################
    if(WIN32)
        set(MKL_RTL_LIBNAME iomp5md)
    else()
        set(MKL_RTL_LIBNAME iomp5)
    endif()
    find_library(MKL_RTL_LIBRARY NAMES ${MKL_RTL_LIBNAME}
        PATHS ${INTEL_ROOT}/lib/intel64/)

    if(NOT MKL_RTL_LIBRARY)
        message(FATAL_ERROR "???")
    endif()

    set(MKL_LIBRARY ${MKL_INTERFACE_LIBRARY} ${MKL_THREADING_LIBRARY} ${MKL_CORE_LIBRARY} ${MKL_FFT_LIBRARY} ${MKL_SCALAPACK_LIBRARY} ${MKL_RTL_LIBRARY})
    set(MKL_MINIMAL_LIBRARY ${MKL_INTERFACE_LIBRARY} ${MKL_THREADING_LIBRARY} ${MKL_CORE_LIBRARY} ${MKL_RTL_LIBRARY})
endif()

set(CMAKE_FIND_LIBRARY_SUFFIXES ${_MKL_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES})

find_package_handle_standard_args(MKL DEFAULT_MSG
    MKL_INCLUDE_DIR MKL_LIB_DIR MKL_LIBRARY MKL_MINIMAL_LIBRARY)

if(MKL_FOUND)
    set(MKL_INCLUDE_DIRS ${MKL_INCLUDE_DIR})
    set(MKL_LIBRARIES ${MKL_LIBRARY})
    set(MKL_MINIMAL_LIBRARIES ${MKL_MINIMAL_LIBRARY})
endif()

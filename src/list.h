/********************
	Name	:	Peter Kemper
	Datum	:	07.10.1990
	Datei	:	list.h
	Version	:	0.1
	C-	:	

	Beschreibung :	Headerdatei fuer list Modul
			Das list Modul bietet eine Verwaltung von Objekten
			in einer linearen Liste.

	Modifikationen :

	Datum		Beschreibung
	-------------------------------------------------------
	08.11.90	Funktionen deklariert
	10.2.95         neue funktion: list_file_print
	7.11.96         err_print ausgebaut

	Inhalt:	
		Datenstrukturen
 		Funktionskoepfe
		globale Variablen

Uebersicht:
    Operationen auf Listen :
	List	**list_new() ;
	int	list_free( List **list, int (*del)( void *elem ) ) ;
	int	list_kill( List **list, int (*del)( void *elem ) ) ;
	int	list_find(List **list,void **adr,void *key,int (*comp)()) ;
	int	list_insert(List **list,void **adr,void *key,void *elem,
			    int(*comp)()) ;
	int	list_append( List **list, void *elem ) ;
	int	list_prepend( List **list) ;
	void	*list_first( List **list) ;
	List	**list_next( List **list) ;
	int	list_delete( List **list,void *key, int (*del)(), int (*comp)())
	void	*list_cont( List **list) ;
	int	list_empty( List **list ) ;
	int	list_print( List **list, int (*print)( void *elem ) ) ;
	int	list_file_print( List **list, FILE *fp, int (*print)( void *elem ) ) ;
	int	list_delete_forall( List **list,void *key, int (*del)(), 
					int (*comp)())
        int     list_compare_element(void *key, void *element )
				       
        int     list_free_delete_standard(void *element)
        int     list_free_delete_dummy(void *element)

Annahmen fuer Funktion comp	
1.	int	comp( void *, void * )
2.	comp( key, elem2 ) <  0         falls key < elem2->key
3.	comp( key, elem2 ) == 0 	falls key = elem2->key
4.	comp( key, elem2 ) >  0 	falls key > elem2->key
5. 	Alle Routinen fuer diese Liste arbeiten mit demselben comp ! 

Annahmen fuer Funktion del
1.	int	del( void * )
2.	Die Funktion wird mit einem Zeiger auf ein verwaltetes Objekt oder Null
	aufgerufen.
			
Annahmen fuer Funktion print in list_file_print
1.      int print( fp, elem )			
**********************/
#ifndef LIST_H
#define LIST_H

#include <stdio.h>

#ifndef NULL
#define NULL	0
#endif

#ifndef TRUE
#define TRUE	1
#define FALSE	0
#endif

#ifdef	MODUL
#define	 EXTERN
#else
#define	EXTERN	extern
#endif

/************************ Schalter fuer Versionen **********************/
#ifndef QPN
#define QPN             TRUE           /* Anwendung im QPN-Tool, z.B. err_print */
#endif
#ifndef STANDARD
#define STANDARD        FALSE            /* Fuer Fehlermeldungen, kein err_print,
					    kein ememory */
#endif
#ifdef	MODUL
#define	OUTPUT_1	FALSE		/* Kontrollausgaben */
#endif				

#define LIST_PREPROC   TRUE  /* list_new(), list_cont(),list_next(),list_empty() werden
				durch Preprozessormacros durchgefuehrt.
				Dient lediglich zur Performancesteigerung,
				dafuer weniger Fehlermeldungen */
#if STANDARD
#define err_print(A,B,C)    printf("Fehlermeldung aus list.c!!!\n")
#define ecalloc         calloc
#define emalloc         malloc
#define efree           free
#endif
/************************ Datenstrukturen ******************************/
typedef struct liste {
    void		*elem ;		/* Elemente des Baumes */
    struct liste	*next ;
} List ;

/************************ Funktionskoepfe ******************************/
/* funktioniert leider nicht 
#ifndef CC_COMP
extern int list_free_delete_standard(void  *element );
extern int list_free_delete_dummy( void *element );
extern int list_compare_element( void *key, void *element );
extern int list_free( List **list, int (*del)() );
extern int list_kill( List **list, int (*del)() );
extern int list_find( List **list, void *(*adr), void *key, int  (*comp)() );
extern int list_insert( List **list, void *(*adr), void *key, void *elem, 
		       int (*comp)() );
extern int list_append( List **list, void *elem);
extern int list_prepend( List **list, void *elem );
extern void    *list_first( List **list);
extern int list_delete( List **list, void *key, int (*del)(), int (*comp)() );
extern int list_print( List **list, int (*print)() ) ;
extern int list_file_print( List **list, FILE *fp, int (*print)() );
extern int list_delete_forall( List **list, void *key, int    (*del)(), 
			      int   (*comp)() );

#else
EXTERN	int	list_free() ;
EXTERN	int	list_kill() ;
EXTERN	int	list_find() ;
EXTERN	int	list_insert() ;
EXTERN	int	list_append() ;
EXTERN	int	list_prepend() ;
EXTERN	void	*list_first() ;
EXTERN	int	list_delete() ;
EXTERN	int	list_print() ;
EXTERN	int	list_file_print() ;
EXTERN  int     list_free_delete_standard() ;
EXTERN  int     list_free_delete_dummy() ;
EXTERN  int     list_compare_element() ;
EXTERN  int     list_delete_forall() ;
#endif
*/
/* EXTERN	int	list_free() ;
EXTERN	int	list_kill() ;
EXTERN	int	list_find() ;
EXTERN	int	list_insert() ;
EXTERN	int	list_append() ;
EXTERN	int	list_prepend() ;
EXTERN	void	*list_first() ;
EXTERN	int	list_delete() ;
EXTERN	int	list_print() ;
EXTERN	int	list_file_print() ;
EXTERN  int     list_free_delete_standard() ;
EXTERN  int     list_free_delete_dummy() ;
EXTERN  int     list_compare_element() ;
EXTERN  int     list_delete_forall() ;
EXTERN  List  **list_remove_elem() ; */

/**
 * Listenelement erzeugen.
 * Warum gibt es diese Funktion?     
 * \date 08.11.90
 * \author Peter Kemper
 * \param elem ???
 * \return Listenelement 
 */
extern List *l_neu(void *elem);

/**
 * Freigabe der Verwaltungsstruktur
 * \date 20.10.94
 * \author Peter Kemper
 * \param element Zeiger auf das freizugebende Element
 * \return immer TRUE (besser void!)
 */
extern int list_free_delete_standard(void  *element );

/**
 * Freigabe der Verwaltungsstruktur (Diese Funktion macht gar nichts!)
 * \date 20.10.94
 * \author Peter Kemper
 * \param element Zeiger auf das freizugebende Element
 * \return immer TRUE (besser void!)
 */
extern int list_free_delete_dummy( void *element );

/**
 * Vergleicht, ob das Element schon in der Liste ist 	
 * \date 20.10.94
 * \author Peter Kemper
 * \param key ???
 * \param element ???
 * \return TRUE (0) wenn key == element, ansonsten FALSE (1)
 */
extern int list_compare_element( void *key, void *element );

/**
 * Freigeben einer Liste.
 * Nach dem Freigeben zeigt list auf den Kopf einer leeren Liste ( NIL ).
 * list_free laesst eine leere Liste zurueck, d.h. der Kopf einer leeren
 * Liste ( das Verwaltungselement ) besteht noch. Dies wird mit list_kill
 * ebenfalls freigegeben.
 * Fazit: nach list_kill muss ein list_new erfolgen, nach list_free nicht.
 * \date 08.11.90
 * \author Peter Kemper
 * \param list ???
 * \param del ???
 * \return immer TRUE (besser void!)
 */
extern int list_free( List **list, int (*del)() );

/**
 * list_free + loeschen des Verwaltungselementes.
 * list_free laesst eine leere Liste zurueck, d.h. der Kopf einer leeren
 * Liste ( das Verwaltungselement ) besteht noch. Dies wird mit list_kill
 * ebenfalls freigegeben.
 * Fazit: nach list_kill muss ein list_new erfolgen, nach list_free nicht.
 * \date 10.6.92
 * \author Peter Kemper
 * \param list ???
 * \param del ???
 * \return immer TRUE (besser waere void!)
 */
extern int list_kill( List **list, int (*del)() );

/**
 * Element in unsortierter Liste finden und ausgeben
 * \date 08.11.90
 * \author Peter Kemper
 * \param list Liste, in der gesucht werden soll
 * \param adr Adresse des gefundenen Elementes
 * \param key Suchkriterium fuer gesuchtes Element
 * \param comp Suchfunktion mit Aufruf (*comp)( key, element )
 * \return TRUE	Element gefunden, ansonsten FALSE
 */
extern int list_find( List **list, void *(*adr), void *key, int  (*comp)() );

/**
 * Einfuegen in eine sortierte Liste.
 * Sofern das einzufuegende Element schon vorhanden ist, wird das einzufuegende
 * Element nicht eingefuegt, ueber adr ein Zeiger auf das gefundene Element aus-
 * gegeben und der Returnwert ist FALSE!
 * \date 17.12.90
 * \author Peter Kemper
 * \param list Liste, in die eingefuegt werden soll
 * \param adr Zeiger auf Element, falls einzufuegendes Element schon vorhanden
 * \param key Sortierkriterium des einzufuegenden Elementes
 * \param elem einzufuegendes Element
 * \param comp Vergleichsfunktion mit Aufruf comp( key, element )
 * \return TRUE	Element erfolgreich eingefuegt
 * \return FALSE	sonst ( insbesondere bei Mehrfacheinfuegversuchen )
 */
extern int list_insert( List **list, void *(*adr), void *key, void *elem, 
		       int (*comp)() );

/**
 * Element an unsortierte Liste anhaengen
 * \date 08.11.90
 * \author Peter Kemper
 * \param list ???
 * \param elem ??? 
 * \return TRUE, wenn Element angehaengt werden konnte. Ansonsten FALSE.
 */
extern int list_append( List **list, void *elem);

/**
 * Element am Anfang einer unsortierten Liste einfuegen
 * \date 08.11.90
 * \author Peter Kemper
 * \param list ??? 
 * \param elem ???
 * \return TRUE, wenn das Element am Anfang eeingefuegt werden konnte. Ansonsten FALSE.
 */
extern int list_prepend( List **list, void *elem );

/**
 * erstes Element der Liste ausgeben ohne auszufuegen
 * \date 08.11.90
 * \author Peter Kemper
 * \param list Zeiger auf Liste
 * \return Zeiger auf Inhalt des ersten Elementes, falls Element vorhanden. NULL sonst.
 */
extern void    *list_first( List **list);

/**
 * Loeschen eines Elementes in der unsortierten Liste 
 * \date 08.11.90
 * \author Peter Kemper
 * \param list Zeiger auf die Liste
 * \param key ???
 * \param del ???
 * \param comp ???
 * \return TRUE falls Element geloescht wurde, ansonsten FALSE.
 */
extern int list_delete( List **list, void *key, int (*del)(), int (*comp)() );

/**
 * Bildschirmausgabe einer Liste
 * \date 17.12.90
 * \author Peter Kemper
 * \param list Zeiger auf Liste
 * \param print Ausgabefunktion
 * \return immer TRUE (besser void!)
 */
extern int list_print( List **list, int (*print)() ) ;

/**
 * Dateiausgabe einer Liste
 * \date 10.2.95
 * \author Peter Kemper
 * \param list Zeiger auf Liste
 * \param fp Ausgabefile
 * \param print Ausgabefunktion
 * \return immer TRUE (besser void!)
 */
extern int list_file_print( List **list, FILE *fp, int (*print)() );

/**
 * Loeschen bestimmter Elemente in der Liste 
 * \date 08.11.90
 * \author Peter Kemper
 * \param list Zeiger auf Liste
 * \param key ???
 * \param del ???
 * \param comp ???
 * \return TRUE wenn geloescht werden konnte, ansonsten FALSE 
 */
extern int list_delete_forall( List **list, void *key, int    (*del)(), 
			      int   (*comp)() );

/**
 * Liste in sortierte Liste transformieren.
 * Achtung die Listeneintraege werden NICHT kopiert, dh
 * bei copylist == TRUE entsteht eine zusaetzliche Liste mit Verweisen
 * auf die bisherigen Listeneintraege.
 * \date 1.10.99
 * \author Peter Kemper  
 * \param list Liste die sortiert werden soll
 * \param comp Vergleichsfunktion
 * \param copylist TRUE alte Listenstruktur wird beibehalten. FALSE alte Listenstruktur wird abgebaut.
 * \return sortierte Liste
 */
extern List **list_sort(List **list, int (*comp)(), short copylist) ;

/************************ Preprocessormacros ****************************/
#if LIST_PREPROC
#define list_new()    ( (List **)ecalloc( 1, sizeof( List *)) )
#define list_cont(A)  (( (!(A) || !*(A)) ? NULL : (*(A))->elem ))
#define list_empty(A) (( (!(A) || !*(A)) ? TRUE : FALSE ))
#define list_next(A)  (( (!(A) || !*(A) || !(*(A))->next) ? NULL : &((*(A))->next) ))
#else
#ifndef CC_COMP
extern List    **list_new();
extern void    *list_cont( List **list);
extern int list_empty( List **list );
extern List    **list_next( List **list );
#else
EXTERN	List	**list_new() ;
EXTERN	void	*list_cont() ;
EXTERN	List	**list_next() ;
EXTERN	int	list_empty() ;
#endif
#endif

/************************ globale Variablen ****************************/

#endif

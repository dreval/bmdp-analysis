/*
 * $Log: ememory.c,v $
 * Revision 1.41  2015/03/11 22:04:34  buchholz
 * *** empty log message ***
 *
 * Revision 1.44  2015/03/07 21:55:07  buchholz
 * *** empty log message ***
 *
 * Revision 1.40  2015/03/01 19:33:22  buchholz
 * *** empty log message ***
 *
 * Revision 1.30  2014/02/28 14:26:08  peter
 * *** empty log message ***
 *
 * Revision 1.38  2013/12/23 14:42:32  buchholz
 * *** empty log message ***
 *
 * Revision 1.26  2013/09/20 06:25:08  peter
 * *** empty log message ***
 *
 * Revision 1.36  2013/09/01 21:49:22  buchholz
 * *** empty log message ***
 *
 * Revision 1.20  2013/08/30 19:56:33  peter
 * *** empty log message ***
 *
 * Revision 1.35  2012/08/29 12:42:48  buchholz
 * *** empty log message ***
 *
 * Revision 1.17  2012/08/29 12:38:30  peter
 * *** empty log message ***
 *
 * Revision 1.16  2011/11/10 15:50:17  peter
 * *** empty log message ***
 *
 * Revision 1.33  2011/10/30 19:37:12  buchholz
 * *** empty log message ***
 *
 * Revision 1.15  2011/08/02 19:35:58  peter
 * *** empty log message ***
 *
 * Revision 1.28  2011/05/30 07:10:29  buchholz
 * *** empty log message ***
 *
 * Revision 1.13  2011/05/04 17:36:28  peter
 * *** empty log message ***
 *
 * Revision 1.25  2011/05/04 14:41:32  buchholz
 * *** empty log message ***
 *
 * Revision 1.28  2011/05/04 06:27:09  buchholz
 * *** empty log message ***
 *
 * Revision 1.24  2011/05/03 18:55:43  buchholz
 * *** empty log message ***
 *
 * Revision 1.12  2010/11/20 10:22:11  peter
 * *** empty log message ***
 *
 * Revision 1.16  2010/11/05 10:22:39  buchholz
 * *** empty log message ***
 *
 * Revision 1.11  2010/07/18 10:54:21  peter
 * *** empty log message ***
 *
 * Revision 1.7  2010/03/18 22:21:27  buchholz
 * *** empty log message ***
 *
 * Revision 1.10  2010/03/14 11:28:39  peter
 * *** empty log message ***
 *
 * Revision 1.6  2010/02/15 15:37:40  buchholz
 * *** empty log message ***
 *
 * Revision 1.2  2009/10/20 20:09:10  peter
 * *** empty log message ***
 *
 * Revision 1.2  2009/10/16 13:12:47  buchholz
 * *** empty log message ***
 *
 * Revision 1.2  2009/10/16 13:10:26  buchholz
 * *** empty log message ***
 *
 * Revision 1.1  2008/11/09 15:18:53  peter
 * Initial revision
 *
 * Revision 1.2  2000/03/08 15:19:23  kemper
 * update
 *
 * Revision 1.1  1999/12/13 14:54:29  tepper
 * Initial revision
 *
 */
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#define MODUL
#include	"ememory.h"
#undef MODUL
#ifndef NULL
#define NULL	0
#endif


#define TURBO TRUE

#if XENIX_MALLOC

#define P_Minfo(A)	(void)printf("A\t\t:\t\%d\n", miinfo.A )

/*******************
mem_info	##s	gebe TEST der Speicherallokation
input	Funktionsbezeichnung der letzten Speicherallokation
output
side
description
******************/
static void
mem_info(rout,adr)
char	*rout ;
char	*adr ;
{
    struct mallinfo minfo ;

    (void) printf ( "alloc von %s auf Adress %ld\n\n", rout, (long int) adr ) ;
    minfo = mallinfo ;
    P_Minfo(arena) ;
    P_Minfo(ordblks) ;
    P_Minfo(smblks) ;
    P_Minfo(hblkhd) ;
    P_Minfo(hblks) ;
    P_Minfo(usmblks) ;
    P_Minfo(fsmblks) ;
    P_Minfo(uordblks) ;
    P_Minfo(fordblks) ;
    P_Minfo(keepcost) ;
    putchar('\n') ;
} /* mem_info */

#else
#define mem_info(A,B);
#endif



/*******************
emalloc		##s	Speicherallokation durch Angabe der Groesse
input	size	Groesse
output	char	* Zeiger auf Speicherelement
side	Bei Fehler Abbruch
description
******************/
void	*
emalloc( size )
unsigned	size ;
{
    char	*cp ;
#if TURBO
    if ( (cp = (char *)malloc((size_t)size)) == NULL ) {
#else
    if ( (cp = malloc(size)) == NULL ) {
#endif
      printf( "Fataler Fehler : malloc (Try to allocate size %i)\n", size) ;
	exit ( -100 ) ;
    } /* then */
    mem_info("malloc",cp) ;
    return( cp ) ;
} /* emalloc */



/*******************
erealloc		##s	Speicherbereich wird veraendert
input	size	Groesse eines Elementes
	cp	Zeiger auf Speicherelement
output	char	* Zeiger auf Speicherelement
side	Bei Fehler Abbruch
description
******************/
void	*
erealloc( cp, size )
char		*cp ;
unsigned	size ;
{
    char	*prt ;

#if TURBO
    if ( (prt = (char *)realloc((void *)cp,(size_t)size)) == NULL ) {
#else
    if ( (prt = realloc(cp, size)) == NULL ) {
#endif
	printf( "Fataler Fehler : malloc\n" ) ;
	exit ( -100 ) ;
    } /* then */
    mem_info("realloc",cp) ;
    return( prt ) ;
} /* erealloc */



/*******************
ecalloc		##s	Speicherallokation durch Angabe der Elemente
input	size	Groesse eines Elementes
	nelem	Anzahl der Elemente
output	char	* Zeiger auf Speicherelement
side	Bei Fehler Abbruch
description
******************/
void	*
ecalloc( nelem, size )
unsigned	nelem ;
unsigned	size ;
{
    char	*cp ;

#if TURBO
    if ( (cp = (char *)calloc((size_t)nelem,(size_t)size)) == NULL ) {
#else
    if ( (cp = calloc(nelem, size)) == NULL ) {
#endif
      printf( "Fataler Fehler : malloc (try to allocate nelem %i size %i)\n", nelem, size ) ;
	exit ( -100 ) ;
    } /* then */
    mem_info("calloc",cp) ;
    return( cp ) ;
} /* ecalloc */



/*******************
efree		##s	Speicherfreigabe
input	char	* Zeiger auf Speicherelement
output	
side	
description
******************/
void	
efree( cp )
void	*cp ;
{
    if ( cp != NULL ) {
	mem_info("free",cp) ;
#if EFREE
#if TURBO
	free((void *)cp) ;
#else
	free(cp) ;
#endif
#else
#if EFREE_OUT
	printf( "free wird nicht ausgefuehrt! \n " ) ;
#endif
#endif
    } /* then */
} /* efree */

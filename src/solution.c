/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c (C) Copyright 1995 by Peter Buchholz
c     All Rights Reserved
c
c solution.c
c
c Loesung einer Markov Kette mittels Standardtechniken
c 
c
c       Datum           Modifikationens
c       ----------------------------------------------------------
c       29.5.95 Datei angelegt, 
c       12.7.95 JOR und SOR eingefuegt,
c       10.8.95 Anlegen des initialen Vektors bei ini>0
c       12.9.95 Parameter sol_direct_solution geaendert
c       13.09.95 measurement of real time included und 
c                SOR ohne Transponieren
c       25.09.95 Memory requirements included.
c       14.05.96 gcc Header
c       05.08.97 Funktionskoepfe vervollstaendigt
c       06.02.98 Funktion sol_normalise_vektor nach mat_double_vektor.c
c       12.05.98 Aenderung der Aufrufparameter von solve_sparse_system und
c                und Einfuegen von Aggregierungs-/Disaggregierungstechniken
c       10.06.98 kleinere Aenderungen bei der Speicherverwaltung
c       14.06.98 Einfuegen von GMRES
c       15.06.98 Einfuegen von Prekonditionierung fuer GMRES
c       31.08.98 Fehler bei ILU0 Praekonditionierung beseitigt
c       31.08.98 BiCGSTAB eingefuehrt
c       03.09.98 Power mit Praekonditionierung eingefuegt
c       05.11.98 Neue Parameter bei mat_sparse_matrix_mult_vektor
c       22.01.99 TFQMR eingefuegt
c       25.11.99 Konstante RES_DIST zur Testabstandbestimmung von residuen 
c                eingefuehrt
c       15.03.99 JOR und Power verbessert indem Vektoren nicht mehr kopiert werden
c       22.01.03 Einf�gen von sol_solve_general_system und sol_invert_matrix
c       07.05.03 Anpassen Projektionsmethoden an strukturierte Implementierungen
c       20.05.03 Effizientere Implementierung der ILUPraekonditionierer
c       22.07.03 in sol_sor_iteration und sol_jor_iteration zusaetzliche Parameter,
c                um Ausgaben abzuschalten.
c       28.07.03 Transponierte Matrix in sol_sor_iteration wir kopiert
c       26.07.04 Corrected two errors in lu_substitution and sol_direct_solution
c       16.06.05 Added routine modify_matrix
c       17.06.10 sol_general_bicgstab eingefuegt
c       23.06.10 sol_general_gmres eingefuegt
c       14.10.10 Euklidsiche Norm zum Abbruch eingef�hrt
c       30.05.12 ILUTH nutzt epsilon2 * max_eleme(q) statt epsilon2 direkt
c       01.06.12 Normierung in GMRES nur am Ende
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/

#include <math.h>
#include <values.h>
#include "str_proconst.h"
#include "str_global.h"
#include "mat_int_vektor.h"
#include "mat_double_vektor.h"
#include "mat_row.h" 
#include "mat_sparse_matrix.h"
#include "mat_time.h"
#include "solution.h"

#define SHOW_TESTX FALSE
#define SHOW_TESTY FALSE

#define NEW_VERSION TRUE  

#define TRC_VECTOR FALSE

#define SOL_TRC_ITERATION FALSE

#ifndef THRESHOLD
#define THRESHOLD 1.0e-50
#endif

/**********************
sol_matrix_dim 
Datum:  19.4.95
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: Funktionswert Spaltendiemsnion der Matrix
Side-Effects:
Description:
Die Funktion bestimmt den maximalen Spaltenindex eines Nichtnullelelements.
**********************/
#ifndef CC_COMP
int sol_matrix_dim(Sparse_matrix *q) 
#else
int sol_matrix_dim(q)
  Sparse_matrix *q ;
#endif
{
  size_t i, j, dim=0;
  
  for (i = 0; i < q->ord; i++)
  if (q->diagonals[i] != 0.0)
    dim = i + 1 ;
  for (i = 0; i < q->ord; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++) 
  if ((q->rowind[i]).colind[j] >= dim)
    dim = (q->rowind[i]).colind[j] + 1;

  return(dim) ;
}

/**********************
sol_copy_to_transpose
Datum:  19.4.95
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: Funktionswert transponierte Matrix wobei letzte Zeile durch
        den Einheitsvektor ersetzt wurde
Side-Effects:
Description:
Die Funktion transponiert eine spaerlich besetzte Matrix und ersetzt die
letzte Zeile durch den Einheitsvektor.
**********************/
#ifndef CC_COMP
Sparse_matrix *sol_copy_to_transpose(Sparse_matrix *q) 
#else
Sparse_matrix *sol_copy_to_transpose(q)
  Sparse_matrix *q ;
#endif
{
  Sparse_matrix *a        ;
  Int_vektor    *pos      ;
  size_t         i, j, dim;

  a = mat_sparse_matrix_new(sol_matrix_dim(q)) ;
  pos = mat_int_vektor_new(q->ord) ;
  for (i = 0; i < q->ord; i++)
    pos->vektor[i] = 0 ;

  /* Alle Zeile bis auf die letzte */
  for (i = 0; i < a->ord - 1; i++)
  {
    a->diagonals[i] = (i < q->ord) ? q->diagonals[i] : 0.0 ;
    dim = 0 ;
    for (j = 0; j < q->ord; j++)
    if (pos->vektor[j] < (q->rowind[j]).no_of_entries &&
        (q->rowind[j]).colind[pos->vektor[j]] == i      )
      dim++ ;
    (a->rowind[i]).no_of_entries = dim ;
    (a->rowind[i]).colind = (size_t *) ecalloc (dim, sizeof(size_t)) ;
    (a->rowind[i]).val = (double *) ecalloc (dim, sizeof(double)) ;
    dim = 0 ;
    for (j = 0; j < q->ord; j++)
    if (pos->vektor[j] < (q->rowind[j]).no_of_entries &&
       (q->rowind[j]).colind[pos->vektor[j]] == i       )
    {
      (a->rowind[i]).colind[dim] = j                               ;
      (a->rowind[i]).val[dim]    = (q->rowind[j]).val[pos->vektor[j]] ;
      pos->vektor[j]++ ;
      dim++ ;
    }
  }
  /* Ersetze die letzte Zeile durch den Einheitsvektor */
  (a->rowind[a->ord-1]).no_of_entries = a->ord - 1 ;
  (a->rowind[a->ord-1]).colind = (size_t *) ecalloc (a->ord - 1, sizeof(size_t)) ;
  (a->rowind[a->ord-1]).val = (double *) ecalloc (a->ord - 1, sizeof(double)) ;
  a->diagonals[a->ord-1] = 1.0 ;
  for (i = 0; i < a->ord-1; i++)
  {
    (a->rowind[a->ord-1]).colind[i] = i   ;
    (a->rowind[a->ord-1]).val[i]    = 1.0 ;
  }
  mat_int_vektor_free(pos) ;
  return(a) ;
}

/**********************
modify_matrix
Datum:  28.06.05
Autor:  Tugrul Dayar
Input:  q Sparse Matrix
Output: returns a new matrix which has q with its last column set to 1s
Side-Effects:
Description: 
**********************/
#ifndef CC_COMP
Sparse_matrix *modify_matrix(Sparse_matrix *q) 
#else
Sparse_matrix *modify_matrix(q)
  Sparse_matrix *q ;
#endif
{
  Sparse_matrix *a        ;
  int            j, k, dim, nonzeros; 
  short          have     ;

  a = mat_sparse_matrix_new(q->ord) ;

  for (k = 0; k < q->ord-1; k++)
  {
    have = 0;
    nonzeros = (q->rowind[k]).no_of_entries ;
    for (j = 0; j < nonzeros; j++)
        if ((q->rowind[k]).colind[j] == q->ord-1)
           have = 1 ;
    if (have)
       dim = nonzeros ;
    else 
       dim = nonzeros + 1 ;
    (a->rowind[k]).no_of_entries = dim ;
    (a->rowind[k]).colind = (size_t *) ecalloc (dim, sizeof(size_t)) ;
    (a->rowind[k]).val = (double *) ecalloc (dim, sizeof(double)) ;
    dim = 0 ;
    for (j = 0; j < nonzeros; j++) {
      (a->rowind[k]).colind[dim] = (q->rowind[k]).colind[dim] ;
      (a->rowind[k]).val[dim]    = (q->rowind[k]).val[dim] ;
      dim++ ;
    } 
    if (have) {
      (a->rowind[k]).val[dim-1]  = 1.0 ;
    }
    else {
      (a->rowind[k]).colind[dim] = a->ord-1 ;
      (a->rowind[k]).val[dim]    = 1.0 ;
    }
    a->diagonals[k] = q->diagonals[k] ;
  }
  nonzeros = (q->rowind[k]).no_of_entries ;
  dim = nonzeros ;
  (a->rowind[k]).no_of_entries = dim ;
  (a->rowind[k]).colind = (size_t *) ecalloc (dim, sizeof(size_t)) ;
  (a->rowind[k]).val = (double *) ecalloc (dim, sizeof(double)) ;
  dim = 0 ;
  for (j = 0; j < nonzeros; j++) {
    (a->rowind[k]).colind[dim] = (q->rowind[k]).colind[dim] ;
    (a->rowind[k]).val[dim]    = (q->rowind[k]).val[dim] ;
    dim++ ;
  } 
  a->diagonals[a->ord-1] = 1.0 ;

  return(a) ;
}

/**********************
sol_factorise ##e LU Faktorisierung einer Matrix
Datum:  19.4.95
Autor:  Peter Buchholz
Input:  a Sparse Matrix
Output: a LU Faktoren der Matrix
Side-Effects:
Description:
Die Funktion fuehrt eine LU Faktorisierung einer Sparse Matrix durch. Die
Dreiecksmatrizen L und U werden in die urspruengliche Datenstruktur
geschrieben. Tritt eine Fehler bei der Faktorisierung auf, so wird 1
zurueckgegeben, ansonsten NO_ERROR (= 0).
**********************/
#ifndef CC_COMP
int sol_factorise(Sparse_matrix *a)
#else
int sol_factorise(a)
  Sparse_matrix *a ;
#endif
{
  int            i, j, k, dim, lj ;
  Double_vektor *values           ;

  /* Die erste Zeile wird gesondert behandelt */
  if (a->diagonals[0] == 0.0)
    return(1) ;
  else
    a->diagonals[0] = 1.0 / a->diagonals[0] ;
  for (j = 0; j < (a->rowind[0]).no_of_entries; j++)
    (a->rowind[0]).val[j] =  (a->rowind[0]).val[j] * a->diagonals[0] ;

  values = mat_double_vektor_new (a->ord) ;
  /* nun die restlichen Zeilen */
  for (k = 1; k < a->ord; k++)
  {
    for (i = 0; i < a->ord; i++)
      values->vektor[i] = 0.0 ;
    for (i = 0; i < (a->rowind[k]).no_of_entries; i++)
      values->vektor[(a->rowind[k]).colind[i]] = (a->rowind[k]).val[i] ;
    values->vektor[k] = a->diagonals[k] ;
    for (i = 0; i < k; i++)
    if (values->vektor[i] != 0.0) 
    {
      j = 0 ;
      while (j < (a->rowind[i]).no_of_entries &&
             (a->rowind[i]).colind[j] <= i       ) 
        j++ ;
      while (j < (a->rowind[i]).no_of_entries)
      { 
        lj = (a->rowind[i]).colind[j] ;
        values->vektor[lj] = values->vektor[lj] - 
                (a->rowind[i]).val[j] * values->vektor[i] ;
        j++ ;
      }
    }
    if (values->vektor[k] == 0.0)
    {
      mat_double_vektor_free(values) ;
      return(1) ;
    }
    else
      values->vektor[k] = 1.0 / values->vektor[k] ;
    for (j = k + 1; j < a->ord; j++)
      values->vektor[j] = values->vektor[j] * values->vektor[k] ;
    a->diagonals[k]   = values->vektor[k] ;
    values->vektor[k] = 0.0               ;
    dim = 0 ;
    for (i = 0; i < a->ord; i++)
    if (values->vektor[i] != 0.0)
      dim++ ;
    efree ((a->rowind[k]).colind) ;
    efree ((a->rowind[k]).val)    ;
    (a->rowind[k]).no_of_entries = dim ;
    (a->rowind[k]).colind  = (size_t *) ecalloc (dim, sizeof(size_t)) ;
    (a->rowind[k]).val     = (double *) ecalloc (dim, sizeof(double)) ;
    j = 0 ;
    for (i = 0; i < a->ord; i++)
    if (values->vektor[i] != 0.0)
    {
      (a->rowind[k]).colind[j] = i                 ;
      (a->rowind[k]).val[j]    = values->vektor[i] ;
      j++ ;
    }
  }
  mat_double_vektor_free(values) ;
  return(NO_ERROR) ;
}

/**********************
sol_substitution 
Datum:  19.4.95
Autor:  Peter Buchholz
Input:  a LU Faktoren einer Sparse Matrix
Output: v Loesungsvektor
Side-Effects:
Description:
Die Funktion fuehrt die Ruecksubstitution  mit der faktorisierten Matrix
aus. 
**********************/
#ifndef CC_COMP
int sol_substitution(Sparse_matrix *a, Double_vektor *v)
#else
int sol_substitution(a, v) 
  Sparse_matrix *a  ;
  Double_vektor *v  ;
#endif
{
  int i,j;

  for (i = 0; i < a->ord - 1; i++)
    v->vektor[i] = 0.0 ;
  v->vektor[a->ord-1] = a->diagonals[a->ord-1] ;

  for (i = a->ord - 2; i >= 0; i--)
  {
    j = 0 ;
    while (j <  (a->rowind[i]).no_of_entries 
             && (a->rowind[i]).colind[j] < i )
      j++ ;
    while (j <  (a->rowind[i]).no_of_entries)
    {
      v->vektor[i] -= v->vektor[(a->rowind[i]).colind[j]] * 
                      (a->rowind[i]).val[j]                 ;
      j++ ;
    }
  }
  return(NO_ERROR) ;
}

/**********************
lu_substitution
Datum:  24.1.04
Autor:  Tugrul Dayar
Input:  The LU factors L and U in LU, the right-hand side b and solution x
Output: x is overwritten with b * inv(LU)
Side-Effects:
Description:
This code solves a nonsingular linear system x * LU = b for x and
differs from lu_solve in the types of the second and third parameters 
(i.e., they are Double_vektors, pointers to the first nonzero element 
are not used in each row of U, and the solution is returned separately).
The function computes b * inv(LU) in 2 steps:
1) Solve temp * U = b    for temp (x is used for temp )
2) Solve    x * L = temp for x    (temp is actually x from previous step)
It is assumed that L * U is a nonsingular matrix and LU has the strictly
lower triangular part of L, the reciprocal of the diagonal of U and the
strictly upper triangular part of U. Note that no temporary vectors are
allocated in solve_lu. 
***********************************************************************/
#ifndef CC_COMP
void lu_substitution (Sparse_matrix *LU, Double_vektor *b, 
                      Double_vektor *x)
#else
void lu_substitution (LU, b, x)
  Sparse_matrix *LU;
  Double_vektor *b;
  Double_vektor *x;
#endif
{
  int i, j, nz;
  int ord = LU->ord;

  for (i = 0; i < ord; i++)
      x->vektor[i] = b->vektor[i];
                                                                             
  // Step 1) Compute temp[i]
  for (i = 0; i < ord; i++) {

      // Multiplication rather than division is performed
      // since diagonal element is already reciprocated
      x->vektor[i] *= (LU->diagonals[i]);

      // Skip the elements in the strictly lower triangular part of row i
      j = 0 ;
      nz = (LU->rowind[i]).no_of_entries;
      while (j < nz && (LU->rowind[i]).colind[j] < i)
         j++;

      // If elements in the strictly upper triangular part of row i
      while (j < nz) {
         x->vektor[(LU->rowind[i]).colind[j]] -= 
            x->vektor[i] * (LU->rowind[i]).val[j];
         j++ ;
      }
  } // for-i

  // Note that x[(LU->ord)-1] remains as it is since L[(LU->ord)-1] = 1
  // Step 2) Compute x[i]
  for (i = ord-2; i >= 0; i--) {

      // Do the updates due to row i+1
      // Stop before the elements in the strictly upper triangular part 
      // of row (i+1)
      j = 0 ;
      nz = (LU->rowind[i+1]).no_of_entries;
      while (j < nz && (LU->rowind[i+1]).colind[j] < i+1) {
         x->vektor[(LU->rowind[i+1]).colind[j]] -=
            x->vektor[i+1] * (LU->rowind[i+1]).val[j];
         j++;
      }

      // Note that x[i] remains as it is since L[i] = 1
  } // for-i

  return;
} /* lu_substitution */

/**********************
lu_factorise_alt
Datum:  24.1.04
Autor:  Tugrul Dayar
Input:  The sparse matrix a
Output: a is overwritten with the LU factorization of a
Side-Effects:
Description:
The elements below the diagonal of L are stored in the strictly
lower triangular part of a, the reciprocal of the diagonal elements
of U are stored in the diagonal of a, and  the elements above the
diagonal of U are stored in the strictly upper-triangular part of a.
Note that we accomplish a Doolittle decomposition of a; hence,
L has a unit diagonal. 
**********************/
#ifndef CC_COMP
int lu_factorise_alt(Sparse_matrix *a)
#else
int lu_factorise_alt(a)
    Sparse_matrix *a;
#endif
{
   int            i, j, k, dim, lj, ord, nz, total_nz;
   Double_vektor *values;

   ord = a->ord;

#if TRC_VECTOR
   printf("Before factorizing the corrected and aggregated HLM matrix:\n");
   for (k = 0; k < ord; k++) {
       nz = (a->rowind[k]).no_of_entries;
       printf("Row %2i: ",k);
       for (i = 0; i < nz; i++)
           printf("%.3e ", (a->rowind[k]).val[i]);
       printf("  Diagonal: %.3e \n", a->diagonals[k]);
   }
   printf("\n\n");
#endif

   // Row 0 does not change except the diagonal element
   if (a->diagonals[0] == 0.0) return(1);
   else a->diagonals[0] = 1.0 / a->diagonals[0]; // Reciprocate pivot

   // Temporary vector to carry out the updates
   values = mat_double_vektor_new (ord);

   total_nz = (a->rowind[0]).no_of_entries;

   // Reduce rows 1 through (ord-1)
   for (k = 1; k < ord; k++) {

       // Clear temporary vector
       for (i = 0; i < ord; i++)
           values->vektor[i] = 0.0;

       // Copy row k to be updated to temporary vector including diag element
       nz = (a->rowind[k]).no_of_entries;
       for (i = 0; i < nz; i++)
           values->vektor[(a->rowind[k]).colind[i]] = (a->rowind[k]).val[i];

       values->vektor[k] = a->diagonals[k];

       // Carry out updates due to rows with smaller row indices
       for (i = 0; i < k; i++)

           // If pivot in row i is 0, there is no update due to row i
           if (values->vektor[i] != 0.0) {

              // The following statement is necessary if we use the code
              // with a onsingular matrix and do both forward and back solves
              // Compute and store multiplier
              values->vektor[i] *= a->diagonals[i];

              j = 0;

              // Skip the elements in the lower triangular part of row i
              nz = (a->rowind[i]).no_of_entries;
              while (j < nz && (a->rowind[i]).colind[j] <= i)
                 j++;

              // If elements in the strictly upper triangular part of row i
              while (j < nz) {
                 lj = (a->rowind[i]).colind[j];

                 // Update element in position (i,lj)
                 values->vektor[lj] = values->vektor[lj]
                                - values->vektor[i] * (a->rowind[i]).val[j];
                 j++;
              } // while
           }

       // If pivot in row k is 0, GE without pivoting breakdowns
       if (values->vektor[k] == 0.0) {
          mat_double_vektor_free(values);
          return(1);
       }
       else values->vektor[k] = 1.0 / values->vektor[k]; // Reciprocate pivot

       a->diagonals[k]   = values->vektor[k];    // Store reciprocated pivot
       values->vektor[k] = 0.0;                  // Pivot is in diag position
                                                 // and does not contribute to #
                                                 // of nonzeros in updated row
       dim = 0;
       for (i = 0; i < ord; i++)
           if (values->vektor[i] != 0.0) dim++;  // Count # of nonzeros
                                                                                
       efree ((a->rowind[k]).colind);            // a will be overwritten,
       efree ((a->rowind[k]).val);               // so free old vectors
                                                                                
       (a->rowind[k]).no_of_entries = dim;       // Set new # of nonzeros
       (a->rowind[k]).colind  = (size_t *) ecalloc ((unsigned int) dim, sizeof(size_t));
       (a->rowind[k]).val     = (double *) ecalloc ((unsigned int) dim, sizeof(double));
                                                                                
       total_nz += dim;
                                                                                
       j = 0;
       for (i = 0; i < ord; i++)
           if (values->vektor[i] != 0.0) {
              (a->rowind[k]).colind[j] = i;      // Set new column indices and
              (a->rowind[k]).val[j]    = values->vektor[i];  // nonzero values
              j++;
           }
   } // for-k
                                                                                
   a->n_of_nonzeros = total_nz;
                                                                                
   mat_double_vektor_free(values) ;
                                                                                
   return(NO_ERROR) ;
} /* lu_factorise_alt */

/**********************
sol_direct_solution ##e direkte Loesung des aggregierten Systems
Datum:  16.06.05
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  q Sparse Matrix als Koeffizientenmatrix
        transpose bei > 0 wird die Matrix transponiert und die letzte Zeile
                  der transponierten Matrix durch den Einheitsvektor ersetzt,
                  ansonsten wird das Gleichungssystem mit der gegebenen 
                  Koeffizientenmatrix geloest
        rhs right-hand side vector
Output: v Loesungsvektor muss ein Vektor ausreichender Laenge sein
Side-Effects:
Description:
Die Funktion loest ein Gleichungssystem mit Hilfe der LU Faktorisierung.
Tritt ein Fehler bei der Loesung auf, so wird ein gleichverteilter Vektor
zurueckgegeben und der Funktionswert auf NO_CONVERGENCE gesetzt. Ansonsten
wird der Loesungsvektor und Funktionswert NO_ERROR zurueckgegeben.
**********************/
#ifndef CC_COMP
int sol_direct_solution (Sparse_matrix *q, Double_vektor *v, int transpose,
                         Double_vektor *rhs)
#else
int sol_direct_solution (q, v, transpose, rhs) 
  Sparse_matrix   *q ; /* Aggregatmatrix */
  Double_vektor   *v ; /* Loesungsvektor */
  int              transpose ;
  Double_vektor *rhs ;
#endif
{
  int            failed = 0;
  size_t         i         ;
  double         val       ;
  Sparse_matrix *a         ;

  if (q->ord == 1)
  {
    // In any case, for a (1 x 1) matrix, the normalized solution must be 1
    v->vektor[0] = 1.0 ;
    if (q->diagonals[0] != 0.0)
       return(NO_ERROR);
    else
       return(NO_CONVERGENCE);
  }
  if (rhs == NULL)
     if (transpose) 
        a = sol_copy_to_transpose(q) ;
     else {
        a = mat_sparse_matrix_copy(q) ;
        efree((a->rowind[a->ord-1]).colind) ;    
        efree((a->rowind[a->ord-1]).val) ;
        (a->rowind[a->ord-1]).no_of_entries = a->ord - 1 ;
        (a->rowind[a->ord-1]).colind = 
           (size_t *) ecalloc (a->ord, sizeof(size_t)) ;
        (a->rowind[a->ord-1]).val = 
           (double *) ecalloc (a->ord, sizeof(double)) ;
        a->diagonals[a->ord - 1] = 1.0 ; 
        for (i = 0; i < a->ord - 1; i++) {
            (a->rowind[a->ord-1]).colind[i] = i ;
            (a->rowind[a->ord-1]).val[i]    = 1.0 ;
        }
     }
  // We do not transpose the matrix, and work with the complete matrix
  else 
     a = mat_sparse_matrix_copy(q) ;
  
  if (a->ord == q->ord)
  {
    // Singular matrix, zero right-hand side vector
    if (rhs == NULL) {
       failed = sol_factorise(a) ;
       /* printf("Fill in after factorization %i\n", mat_sparse_matrix_non_zeros(a)) ; */
       if (!(failed))
          sol_substitution (a, v)   ;
    }
    // Non-singular matrix, nonzero right-hand side vector
    else {
       failed = lu_factorise_alt(a);
       if (!(failed))
          lu_substitution(a, rhs, v);
    }
  }
  else
    failed = 1 ;
  mat_sparse_matrix_free(a) ;
  if (failed)
  {
    val = 1.0 / (double) v->no_of_entries ; 
    for (i = 0; i <  v->no_of_entries; i++)
        v->vektor[i] = val ;
    return(NO_CONVERGENCE) ;
  }
  // v should be a probability vector when rhs vector is zero
  if (rhs == NULL)
     return(mat_double_vektor_normalise(v)) ;
  // v should not be normalized
  else {
     return(NO_ERROR) ;
  }
}

/**********************
sol_solve_general_system
Datum:  21.01.03 
Autor:  Peter Buchholz
Input:  q faktoriserte Matrix 
        r rechtsseitiger Vektor
Output: v L�sungsvektor
Side-Effects:
Description:
Die Funktion berechnet die L�sung des Systems vQ=r mit einer direkten Methode.
q muss dazu faktorisiert und die l und u Werte gestezt sein.
Vorsicht: q wird ver�ndert!
***********************************************************************/
#ifndef CC_COMP
int sol_solve_general_system (Sparse_matrix *q, Double_vektor *r, Double_vektor *v)
#else
int sol_solve_general_system (q, r, v) 
  Sparse_matrix *q  ;
  Double_vektor *r  ;
  Double_vektor *v  ;
#endif
{
  int i,j;

  for (i = 0; i < q->ord; i++)
    v->vektor[i] = r->vektor[i] ;
  /* Forward substitution */
  for (i = 0; i < q->ord; i++)
  {
    for (j = q->u[i]; j < (q->rowind[i]).no_of_entries; j++) {
      v->vektor[(q->rowind[i]).colind[j]] -= v->vektor[i] * (q->rowind[i]).val[j] ;
    }
  }
  /* Back substitution to get the result */
  for (i = q->ord - 1; i >= 0; i--)
  {
    v->vektor[i] *= q->diagonals[i] ;
    for (j = 0; j < q->l[i]; j++)
      v->vektor[(q->rowind[i]).colind[j]] -= v->vektor[i] * (q->rowind[i]).val[j] ;
  }
  return(NO_ERROR) ;
} /* sol_solve_general_system */

/**********************
sol_invert_matrix
Datum:  21.1.03
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: Funktionswert invertierte Matrix
Side-Effects:
Description:
Die Funktion berechnet die Inverse einer Matrix unter Verwendung der
LU-Faktorisierung. Vorsicht q wird ver�ndert!!
***********************************************************************/
#ifndef CC_COMP
Sparse_matrix *sol_invert_matrix(Sparse_matrix *q) 
#else
Sparse_matrix *sol_invert_matrix(q)
  Sparse_matrix *q ; 
#endif
{
  Sparse_matrix *qi ;
  Double_vektor *v, *r  ;
  Row *help_row ;
  int i, j, k ;

#if SHOW_TEST2 
  printf("Matrix to invert\n") ;
  mat_sparse_matrix_output(stdout, q, TRUE) ;
#endif 
  if (sol_factorise(q) != NO_ERROR)
    return(NULL) ;
  mat_set_lud (q) ;
  qi = mat_sparse_matrix_new (q->ord) ;
  v  = mat_double_vektor_new (q->ord) ;
  r  = mat_double_vektor_new (q->ord) ;

  for (i = 0; i < q->ord; i++)
  {
    if (i > 0)
      r->vektor[i-1] = 0.0 ;
    r->vektor[i] = 1.0 ;
    sol_solve_general_system (q, r, v) ;
    qi->diagonals[i] = v->vektor[i] ;
    v->vektor[i] = 0.0 ;
    k = 0 ;
    for (j = 0; j < q->ord; j++)
    if (v->vektor[j] != 0.0)
      k++ ;
    help_row = mat_row_new(k) ;
    k = 0 ;
    for (j = 0; j < q->ord; j++) {
      if(v->vektor[j] != 0.0) {
	help_row->colind[k] = j ;
	help_row->val[k] = v->vektor[j] ;
	k++ ;
      }
    }
    (qi->rowind[i]).no_of_entries = help_row->no_of_entries ;
    (qi->rowind[i]).colind        = help_row->colind        ;
    (qi->rowind[i]).val           = help_row->val           ;
  }
#if SHOW_TEST2 
  printf("Inverse matrix\n") ;
  mat_sparse_matrix_output(stdout, qi, TRUE) ;
#endif  
  mat_double_vektor_free(v) ;
  mat_double_vektor_free(r) ;
  return(qi) ;
}

/**********************
sol_power_iteration
Datum:  19.01.04 
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  q Koeffizientenmatrix
        v1 Startvektor
        param Loesungsparameter
        trc bei >0 werden Tracemeldungen waehrend der Iteration ausgegeben
        rhs right-hand side vector
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der Power Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und v1 einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit und
die Fehlerschranke werdne in param angegeben.
**********************/
#ifndef CC_COMP
int sol_power_iteration
     (Sparse_matrix *q, Double_vektor *v1, Solution_param *param, int trc,
      Double_vektor *rhs)
#else
int sol_power_iteration(q, v1, param, trc, rhs) 
  Sparse_matrix   *q     ;
  Double_vektor   *v1    ;
  Solution_param  *param ;
  int              trc   ;
  Double_vektor   *rhs    ;
#endif
{
  Double_vektor *v2                      ;
  int            r, c, iter=0;
  size_t         *rc; 
  double         n_sum, n_max=1.0, n_two, comp_norm = 1.0e+6, alpha=0.0, val,  *rv ;
  double         starttime = mat_timeused() ;
  double         endtime ;

 
  v2 = mat_double_vektor_new(q->ord) ;
  for (r = 0; r < q->ord; r++)
    if (alpha > q->diagonals[r])
      alpha = q->diagonals[r] ; 

  if (alpha < 0.0)
    alpha = 0.999 / (- alpha) ; 
  else
    return(NO_CONVERGENCE) ; 

#if SOLUTION_TEST1
  if (trc)
    printf("Matrix structures for solution generated time used %.3e\n",
            mat_timeused() - starttime                                ) ;
#endif
  starttime = mat_timeused() ;
  endtime   = mat_timeused() ;
  while (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit                  )
  {
    // If rhs vector is not zero, initialize v2->vektor[r] = -rhs->vektor[r]
    if (rhs != NULL)
	for (r = 0; r < q->ord; r++)
	    v2->vektor[r] = -rhs->vektor[r];
    for (r = 0; r < q->ord; r++)
    {
      val = v1->vektor[r] ;
      rc = (q->rowind[r]).colind ;
      rv = (q->rowind[r]).val ;
      for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
        v2->vektor[rc[c]] += 
          val * rv[c] ;
    }
    for (r = 0; r < q->ord; r++)
      v2->vektor[r] += v1->vektor[r] * q->diagonals[r] ;
    iter++ ;
    if (iter % RES_DIST == 0 || iter == param->no_of_iterations)
    {
      // v1 should be a probability vector when rhs vector is zero
      if (rhs == NULL)
         mat_double_vektor_normalise(v1) ;
      // v2 has negated residual vector 
      // but this should not affect the convergence test
      mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
#if SOLUTION_TEST1
  if (trc == TRUE)
    printf("Residuals after %i iterations max %.5e, sum %.5e two: %.5e\n",
           iter, n_max, n_sum, n_two) ;
#endif
      endtime = mat_timeused () ;
    } 
    mat_add_vektor_skalar(v1, v2, alpha) ;
  }
#if SOLUTION_TEST1
  if (trc == TRUE && iter % RES_DIST != 0)
    printf("Residuals after %i iterations max %.5e, sum %.5e\n",
           iter, n_max, n_sum) ;
#endif

  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = endtime - starttime ;
  mat_double_vektor_free (v2) ;
  // v1 is probability vector iff rhs vector is zero
  if (rhs == NULL)
     mat_double_vektor_normalise(v1) ;
  return(NO_ERROR) ;
}

/**********************
sol_jor_iteration
Datum:  19.01.04
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  q Koeffizientenmatrix
        v1 Startvektor
        param Loesungsparameter
        outp TRUE um Ausgaben einzuschalten
        rhs right-hand side vector
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der JOR Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und v1 einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit, der
Relaxationsparameter und die Fehlerschranke werdne in param angegeben.
**********************/
#ifndef CC_COMP
int sol_jor_iteration
      (Sparse_matrix *q, Double_vektor *v1, Solution_param *param, short outp,
       Double_vektor *rhs)
#else
int sol_jor_iteration(q, v1, param, outp, rhs) 
  Sparse_matrix   *q     ;
  Double_vektor   *v1    ;
  Solution_param  *param ; 
  short            outp  ;
  Double_vektor   *rhs   ;
#endif
{
  Double_vektor *v2, *vh1, *vh           ;
  int            r, c, iter=0            ; 
  double         n_sum, n_max=1.0e+6, n_two, comp_norm=1.0e+6 ;
  double         starttime = mat_timeused() ;
  double         endtime ;
 
  v2 = mat_double_vektor_new(q->ord) ;
  vh1 = v1 ;
  for (r = 0; r < q->ord; r++)
  if (q->diagonals[r] != 0.0)
    q->diagonals[r] = 1.0 / -q->diagonals[r] ;
  else
  {
     fprintf(stderr, "Zero occured in diagonal of generator matrix\n") ;
     exit(-1) ;
  }
#if SOLUTION_TEST1
  if(outp)
      printf("Matrix structures for solution generated time used %.3e\n",
	     mat_timeused() - starttime                                ) ;
#endif
  starttime = mat_timeused() ;
  endtime   = mat_timeused() ;

  while (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit                  )
  {
    // If rhs vector is not zero, initialize v2 = -rhs
    if (rhs != NULL)
       for (r = 0; r < q->ord; r++)
           v2->vektor[r] = -rhs->vektor[r];
    for (r = 0; r < q->ord; r++)
    for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
      v2->vektor[(q->rowind[r]).colind[c]] += 
        v1->vektor[r] * (q->rowind[r]).val[c] ;
    for (r = 0; r < q->ord; r++)
      v2->vektor[r] = v2->vektor[r] * q->diagonals[r] ;
    iter++ ;
    if (param->relaxation != 1.0)
    {
      for (r = 0; r < v1->no_of_entries; r++)
        v1->vektor[r] = (1.0 - param->relaxation) * v1->vektor[r] +
                         param->relaxation * v2->vektor[r] ;
    }
    else
    {
      /* Geaendert 15.03.00 
      for (r = 0; r < v1->no_of_entries; r++)
        v1->vektor[r] = v2->vektor[r] ; */
      vh = v2 ;
      v2 = v1 ;
      v1 = vh ;
    }
    for (r = 0; r < v2->no_of_entries; r++)
      v2->vektor[r] = 0.0 ;
    if (iter % RES_DIST == 0 || iter == param->no_of_iterations)
    {
      // Residual norm is computed differently when rhs vector is zero 
      // v1 should be a probability vector
      if (rhs == NULL) 
         mat_double_vektor_normalise(v1) ;
      for (r = 0; r < q->ord; r++)
      for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
        v2->vektor[(q->rowind[r]).colind[c]] += 
          v1->vektor[r] * (q->rowind[r]).val[c] ;
      for (r = 0; r < q->ord; r++)
        v2->vektor[r] -= v1->vektor[r] / q->diagonals[r] ;
      // Residual norm is computed differently when rhs vector is not zero
      // Compute v2 = v2 - rhs (negated residual vector)
      if (rhs != NULL)
         for (r = 0; r < q->ord; r++)
             v2->vektor[r] -= rhs->vektor[r];
      mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
      for (r = 0; r < v2->no_of_entries; r++)
        v2->vektor[r] = 0.0 ;
      if (outp) {
#if SOLUTION_TEST1
   printf("Residuals after %i iterations max %.5e, sum %.5e two: %.5e\n",
	  iter, n_max, n_sum, n_two) ;
#endif
      }
      endtime = mat_timeused () ;
    } 
  }
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = endtime - starttime ;
  /* Geaendert 15.03.00 */
  if (v1 != vh1)
  {
    v2 = v1  ;
    v1 = vh1 ;
    for (r = 0; r < v1->no_of_entries; r++)
        v1->vektor[r] = v2->vektor[r] ;
  }
  mat_double_vektor_free (v2) ;
  // v1 is probability vector iff rhs vector is zero
  if (rhs == NULL)
     mat_double_vektor_normalise(v1) ;
  return(NO_ERROR) ;
}

/**********************
sol_sor_iteration
Datum:  19.01.04
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  q Koeffizientenmatrix
        v1 Startvektor
        param Loesungsparameter
        transpose TRUE falls Eingabematrix transpoiniert werden muss
        noout TRUE um Ausgaben abzuschalten
        rhs right-hand side vector
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
        outp TRUE um Ausgaben anzuschalten
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der SOR Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und v1 einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit, der
Relaxationsparameter und die Fehlerschranke werden in param angegeben.
**********************/
#ifndef CC_COMP
int sol_sor_iteration
     (Sparse_matrix **q, Double_vektor *v1, Solution_param *param, 
      short transpose, short outp, Double_vektor *rhs)
#else
int sol_sor_iteration(q, v1, param, transpose, outp, rhs) 
  Sparse_matrix  **q     ;
  Double_vektor   *v1    ;
  Solution_param  *param ;
  short            transpose ;
  short            outp     ;
  Double_vektor   *rhs   ;
#endif
{
  int            i, j, iter=0 ; 
  double         n_sum, n_max=1.0e+6, n_two, comp_norm = 1.0e+6     ;
  double         starttime = mat_timeused() ;
  double         endtime, val ;
  Sparse_matrix *qt           ;

  if (transpose) {
      qt = mat_sparse_matrix_transpose (*q, (*q)->ord - 1) ;
  }
  else {
      qt = *q ;
  }
  for (i = 0; i < qt->ord; i++)
  if (qt->diagonals[i] != 0.0)
    qt->diagonals[i] = 1.0 / -qt->diagonals[i] ;
  else
  {
     fprintf(stderr, "Zero occured in diagonal of generator matrix\n") ;
     exit(-1) ;
  }
#if SOLUTION_TEST1
  if (outp)
      printf("Matrix structures for solution generated time used %.3e\n",
           mat_timeused() - starttime                                ) ;
#endif
  starttime = mat_timeused() ;
  endtime   = mat_timeused() ;

  while (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit                  )
  {
    for (i = 0; i < qt->ord; i++)
    {
      // If rhs vector is zero, initialize val = 0
      if (rhs == NULL)
         val = 0.0 ;
      // If rhs vector is not zero, initialize val = -rhs->vektor[i]
      else
         val = -rhs->vektor[i];
      for (j = 0; j < (qt->rowind[i]).no_of_entries; j++)
        val += 
          (qt->rowind[i]).val[j] * v1->vektor[(qt->rowind[i]).colind[j]] ;
      if (param->relaxation != 1.0)
       v1->vektor[i] = (1.0 - param->relaxation) * v1->vektor[i] +
                       param->relaxation * val * qt->diagonals[i] ;
      else
        v1->vektor[i] = val * qt->diagonals[i] ;
    }
    iter++ ;
    if (iter % RES_DIST == 0 || iter == param->no_of_iterations)
    {
      // Residual norm is computed differently when rhs vector is zero
      // v1 should be a probability vector
      if (rhs == NULL)
         mat_double_vektor_normalise(v1) ;
      n_max = 0.0 ;
      n_sum = 0.0 ;
      n_two = 0.0 ;
      for (i = 0; i < qt->ord; i++)
      {
        val = 0.0 ;
        for (j = 0; j < (qt->rowind[i]).no_of_entries; j++)
          val += 
           (qt->rowind[i]).val[j] * v1->vektor[(qt->rowind[i]).colind[j]] ;
        val -= v1->vektor[i] / qt->diagonals[i] ; 
        // Residual norm is computed differently when rhs vector is not zero
        // Compute val = val - rhs[i] (negated residual)
        if (rhs != NULL)
           val -= rhs->vektor[i];
        n_sum += fabs(val) ;
        if (n_max < fabs(val))
          n_max = fabs(val) ;
	n_two += val * val ;
      }
      n_two = sqrt(n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
      if (outp) {
#if SOLUTION_TEST1
   printf("Residuals after %i iterations max %.5e, sum %.5e two: %.5e\n",
	  iter, n_max, n_sum, n_two) ;
#endif
      }
      endtime = mat_timeused () ;
    } 
  }
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = endtime - starttime ;
  // v1 is probability vector iff rhs vector is zero
  if (rhs == NULL)
     mat_double_vektor_normalise(v1) ;
  if (transpose) {
      mat_sparse_matrix_free(qt) ;
  }
  return(NO_ERROR) ;
}

/**********************
sol_gen_agg_matrix
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix des Gesamtsystems
        partition Blockstruktur der Matrix
Output: Funktionswert aggregierte Matrix
Side-Effects:
Description:
Die Funktion generiert die Basisstruktur fuer das aggrgierte System..
**********************/
#ifndef CC_COMP
Sparse_matrix *sol_gen_agg_mat(Sparse_matrix *q, Int_vektor *partition) 
#else
Sparse_matrix *sol_gen_agg_mat(q, partition) 
  Sparse_matrix   *q         ;
  Int_vektor      *partition ;
#endif
{
  Sparse_matrix *qa           ;
  Row           *help_row     ;
  int            i, j, k, l, r_start, r_stop, leng, *nz ;

  qa = mat_sparse_matrix_new(partition->no_of_entries-1) ;
  nz = (int *) ecalloc (qa->ord, sizeof(int)) ;

  for (i = 0; i < partition->no_of_entries-1; i++)
  {
    for (j = 0; j < qa->ord; j++)
      nz[j] = 0 ;
    leng = 0 ;
    r_start = partition->vektor[i]   ;
    r_stop  = partition->vektor[i+1] ;
    for (j = r_start; j < r_stop; j++)
    {
      l = 0 ;
      for (k = 0; k < (q->rowind[j]).no_of_entries; k++)
      {
        while ((q->rowind[j]).colind[k] >= partition->vektor[l+1])
          l++ ;
        if (nz[l] == 0 && l != i)
        {
          leng++ ;
          nz[l] = 1 ;
        }
      }
    }
    help_row = mat_row_new(leng) ;
    k = 0 ;
    for (l = 0; l < qa->ord; l++)
    if (nz[l] == 1)
    {
      help_row->colind[k] = l ;
      k++ ;
    }
    qa->rowind[i] = *help_row ;
  }
  efree(nz)  ;
  return(qa) ;
} /* sol_gen_agg_matrix */

/**********************
sol_fill_agg_matrix
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  qa Matrix des aggregierten Systems
        q Koeffizientenmatrix des Gesamtsystems
        v1 Iterationsvektor des Gesamtsystems
        partition Blockstruktur der Matrix
Output: qa aggregierte Matri xmit neu berechneten Elementen
Side-Effects:
Description:
Die Funktion berechnet die Elemente der aggregierten MAtrix bzgl. des aktuellen
Iterationsvektors v1.
**********************/
#ifndef CC_COMP
void sol_fill_agg_matrix
      (Sparse_matrix *qa, Sparse_matrix *q, Double_vektor *v1, Int_vektor *partition)
#else
void sol_fill_agg_matrix(qa, q, v1, partition)
  Sparse_matrix *qa        ;
  Sparse_matrix *q         ;
  Double_vektor *v1        ;
  Int_vektor    *partition ;
#endif
{
  int    i, j, k, l, r_start, r_stop ;
  double sum ;

  for (i = 0; i < partition->no_of_entries-1; i++)
  {
    for (j = 0; j < (qa->rowind[i]).no_of_entries; j++)
      (qa->rowind[i]).val[j] = 0.0 ;
    sum = 0.0 ;
    r_start = partition->vektor[i]   ;
    r_stop  = partition->vektor[i+1] ;
    for (j = r_start; j < r_stop; j++)
    {
      sum += v1->vektor[j] ;
      l = 0 ;
      for (k = 0; k < (q->rowind[j]).no_of_entries; k++)
      if ((q->rowind[j]).colind[k] <  partition->vektor[i] ||
          (q->rowind[j]).colind[k] >= partition->vektor[i+1]) 
      {
        while ((q->rowind[j]).colind[k] >= 
               partition->vektor[(qa->rowind[i]).colind[l]+1])
          l++ ;
        (qa->rowind[i]).val[l] += v1->vektor[j] * (q->rowind[j]).val[k] ;
      }
    }
    if (sum > 0.0)
      sum = 1.0 / sum ;
    else
      sum = 0.0 ;
    for (j = 0; j < (qa->rowind[i]).no_of_entries; j++)
      (qa->rowind[i]).val[j] *= sum ;
  }
  mat_sparse_matrix_set_diagonals(qa, TRUE) ;
} /* sol_fill_agg_matrix */

/**********************
sol_agg_system
Datum:  17.01.04
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  qa Matrix des aggregierten Systems
        v1 Iterationsvektor des Gesamtsystems
        partition Blockstruktur der Matrix
Output: va Loesungsvektor des aggregierten Systems
Side-Effects:
Description:
Die Funktion loest das aggrgeirte System. Fuer Matrizen einer Dimension < 500 wird ein
direktes Verfahren verwendet, fuer groessere Matrizen die Power Methode.
**********************/
#ifndef CC_COMP
void sol_agg_system
      (Sparse_matrix *qa, Double_vektor *va, Double_vektor *v1, Int_vektor *partition)
#else
void sol_sol_agg_system(qa, va, v1, partition)
  Sparse_matrix *qa        ;
  Double_vektor *va        ;
  Double_vektor *v1        ;
  Int_vektor    *partition ;
#endif
{
  int             i, j  ;
  Solution_param  param ;

  if (qa->ord > 500 || sol_direct_solution(qa, va, TRUE, NULL) != NO_ERROR)
  {
    if (qa->ord <= 500)
      printf("Direct solution failed\n") ;
    param.epsilon          = 0.0001 ;
    param.no_of_iterations = 100    ;
    param.time_limit       = 100.0  ;
    for (i = 0; i < partition->no_of_entries-1; i++)
    {
      va->vektor[i] = 0.0 ;
      for (j = partition->vektor[i]; j < partition->vektor[i+1]; j++)
        va->vektor[i] += v1->vektor[j] ; 
    }
    sol_power_iteration(qa, va, &param, FALSE, NULL) ;
  }
} /* sol_agg_system */

/**********************
sol_ad_step
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  qa Matrix des aggregierten Systems
        q Koeffizientenmatrix des Gesamtsystems
        va Loesungsvektor des aggregierten Systems
        v1 Iterationsvektor des Gesamtsystems
        partition Blockstruktur der Matrix
Output: v1 Iterationsvektor, der bzgl. des aggregierten Wahrscheinlichkeiten 
           berichtigt wurde.
Side-Effects:
Description:
Die Funktion fuehrt einen Aggregierungs-/Disaggregierungsschritt durch.
**********************/
#ifndef CC_COMP
void sol_ad_step(Sparse_matrix *qa, Sparse_matrix *q, Double_vektor *va, 
                 Double_vektor *v1, Int_vektor *partition) 
#else
void sol_ad_step(qa, q, va, v1, partition) 
  Sparse_matrix *qa        ;
  Sparse_matrix *q         ;
  Double_vektor *va        ;
  Double_vektor *v1        ;
  Int_vektor    *partition ;
#endif
{
  int             i, j  ;
  double          sum   ;

  sol_fill_agg_matrix(qa, q, v1, partition) ;
  sol_agg_system(qa, va, v1, partition) ;
  for (i = 0; i < partition->no_of_entries-2; i++)
  {
    sum = 0.0 ;
    for (j = partition->vektor[i]; j < partition->vektor[i+1]; j++)
      sum += v1->vektor[j] ;
    if (sum > 0.0)
    {
      sum = 1.0 / sum ;
      for (j = partition->vektor[i]; j < partition->vektor[i+1]; j++)   
        v1->vektor[j] *= sum * va->vektor[i] ;
    }
  }
} /* sol_ad_step */

/**********************
sol_power_ad_iteration
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        partition Blockstruktur der Matrix
        v1 Startvektor
        param Loesungsparameter
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der Power Methode und eingeschobenen a/d Schritten. Die maximale Iterationszahl, 
maximale CPU Zeit, der Relaxationsparameter und die Fehlerschranke werdne in param 
angegeben.
**********************/
#ifndef CC_COMP
void sol_power_ad_iteration (Sparse_matrix *q, Int_vektor *partition, 
                            Double_vektor *v1, Solution_param *param) 
#else
void sol_power_ad_iteration(q, partition, v1, param) 
  Sparse_matrix   *q         ;
  Int_vektor      *partition ;
  Double_vektor   *v1        ;
  Solution_param  *param     ;
#endif
{
  Sparse_matrix *qa                      ;
  Double_vektor *v2, *va                 ;
  int            r, c, iter=0, ad = 0    ; 
  double         n_sum, n_max=1.0, n_two, comp_norm = 1.0e+6, alpha=0.0 ;
  double         starttime = mat_timeused() ;
  double         endtime  = mat_timeused();

  v2 = mat_double_vektor_new(q->ord) ;
  for (r = 0; r < q->ord; r++)
  if (alpha > q->diagonals[r])
    alpha = q->diagonals[r] ;
  alpha = 0.999 / (- alpha)  ;
  qa = sol_gen_agg_mat(q, partition) ;
  va = mat_double_vektor_new(qa->ord) ;
#if SOLUTION_TEST1
  printf("Matrix structures for solution generated time used %.3e\n",
          mat_timeused() - starttime                                ) ;
#endif
  starttime = mat_timeused() ;
  endtime   = mat_timeused() ;

  while (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit                  )
  {
    for (r = 0; r < q->ord; r++)
    for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
      v2->vektor[(q->rowind[r]).colind[c]] += 
        v1->vektor[r] * (q->rowind[r]).val[c] ;
    for (r = 0; r < q->ord; r++)
      v2->vektor[r] += v1->vektor[r] * q->diagonals[r] ;
    iter++ ;
#if SOLUTION_TEST1
if (ad == 1)
{
  mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
  printf("Residuals after a/d step max %.5e, sum %.5e two: %.5e\n",
	 n_max, n_sum, n_two) ;
  ad = 0 ;
}
#endif 
    if (iter % RES_DIST == 0 || iter == param->no_of_iterations)
    {
      mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
#if SOLUTION_TEST1
  printf("Residuals after %i iterations max %.5e, sum %.5e two %.5e\n",
	 iter, n_max, n_sum, n_two) ;
#endif
      endtime = mat_timeused () ;
    } 
    mat_add_vektor_skalar(v1, v2, alpha) ;
    if (((iter <= 100 && iter % 10 == 0) ||
         (iter >  100 && iter % 50 == 0 )  ) &&
        (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit))
    {
      sol_ad_step(qa, q, va, v1, partition) ;
      ad = 1 ;
    }
  }
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = endtime - starttime ;
  mat_double_vektor_free (v2) ;
  mat_double_vektor_free (va) ;
  mat_sparse_matrix_free (qa) ;
  mat_double_vektor_normalise(v1) ;
} /* sol_power_ad_iteration */

/**********************
sol_jor_ad_iteration
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        partition Blockstruktur der Matrix
        v1 Startvektor
        param Loesungsparameter
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe von JOR und eingeschobenen a/d Schritten. Die maximale Iterationszahl, 
maximale CPU Zeit, der Relaxationsparameter und die Fehlerschranke werdne in param 
angegeben.
**********************/
#ifndef CC_COMP
void sol_jor_ad_iteration(Sparse_matrix *q, Int_vektor *partition, 
                      Double_vektor *v1, Solution_param *param)
#else
void sol_jor_ad_iteration(q, partition, v1, param) 
  Sparse_matrix   *q         ;
  Int_vektor      *partition ;
  Double_vektor   *v1        ;
  Solution_param  *param     ;
#endif
{
  Sparse_matrix *qa                      ;
  Double_vektor *v2, *va                 ;
  int            r, c, iter=0            ; 
  double         n_sum, n_max=1.0e+6, n_two, comp_norm=1.0e+6 ;
  double         starttime = mat_timeused() ;
  double         endtime  = mat_timeused();

  v2 = mat_double_vektor_new(q->ord) ;
  for (r = 0; r < q->ord; r++)
  if (q->diagonals[r] != 0.0)
    q->diagonals[r] = 1.0 / -q->diagonals[r] ;
  else
  {
     fprintf(stderr, "Zero occured in diagonal of generator matrix\n") ;
     exit(-1) ;
  }
  qa = sol_gen_agg_mat(q, partition) ;
  va = mat_double_vektor_new(qa->ord) ;
#if SOLUTION_TEST1
  printf("Matrix structures for solution generated time used %.3e\n",
          mat_timeused() - starttime                                ) ;
#endif
  starttime = mat_timeused() ;
  endtime   = mat_timeused() ;

  while (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit                  )
  {
    for (r = 0; r < q->ord; r++)
    for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
      v2->vektor[(q->rowind[r]).colind[c]] += 
        v1->vektor[r] * (q->rowind[r]).val[c] ;
    for (r = 0; r < q->ord; r++)
      v2->vektor[r] = v2->vektor[r] * q->diagonals[r] ;
    iter++ ;
    if (param->relaxation != 1.0)
    {
      for (r = 0; r < v1->no_of_entries; r++)
        v1->vektor[r] = (1.0 - param->relaxation) * v1->vektor[r] +
                         param->relaxation * v2->vektor[r] ;
    }
    else
    {
      for (r = 0; r < v1->no_of_entries; r++)
        v1->vektor[r] = v2->vektor[r] ;
    }
    for (r = 0; r < v2->no_of_entries; r++)
      v2->vektor[r] = 0.0 ;
    if (iter % RES_DIST == 0 || iter == param->no_of_iterations)
    {
      mat_double_vektor_normalise(v1) ;
      for (r = 0; r < q->ord; r++)
      for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
        v2->vektor[(q->rowind[r]).colind[c]] += 
          v1->vektor[r] * (q->rowind[r]).val[c] ;
      for (r = 0; r < q->ord; r++)
        v2->vektor[r] -= v1->vektor[r] / q->diagonals[r] ;
      mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
      for (r = 0; r < v2->no_of_entries; r++)
        v2->vektor[r] = 0.0 ;
#if SOLUTION_TEST1
   printf("Residuals after %i iterations max %.5e, sum %.5e\n",
           iter, n_max, n_sum) ;
#endif
      endtime = mat_timeused () ;
      if (((iter <= 100 && iter % 10 == 0) ||
           (iter >  100 && iter % 50 == 0 )  ) &&
          (comp_norm > param->epsilon && iter < param->no_of_iterations &&
           endtime - starttime < param->time_limit))
      {
        sol_ad_step(qa, q, va, v1, partition) ;
#if SOLUTION_TEST1
  mat_double_vektor_normalise(v1) ;
  for (r = 0; r < q->ord; r++)
  for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
    v2->vektor[(q->rowind[r]).colind[c]] += 
      v1->vektor[r] * (q->rowind[r]).val[c] ;
  for (r = 0; r < q->ord; r++)
    v2->vektor[r] -= v1->vektor[r] / q->diagonals[r] ;
  mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
  printf("Residuals after a/d step max %.3e, sum %.3e two %.3e\n",
	 n_max, n_sum, n_two) ;
  for (r = 0; r < v2->no_of_entries; r++)
    v2->vektor[r] = 0.0 ;
#endif
      }
    } 
  }
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = endtime - starttime ;
  mat_double_vektor_free (v2) ;
  mat_double_vektor_free (va) ;
  mat_sparse_matrix_free (qa) ;
  mat_double_vektor_normalise(v1) ;
} /* sol_jor_ad_iteration */

/**********************
sol_sor_ad_iteration
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        partition Blockstruktur der Matrix
        v1 Startvektor
        param Loesungsparameter
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe von SOR und eingeschobenen a/d Schritten. Die maximale Iterationszahl, 
maximale CPU Zeit, der Relaxationsparameter und die Fehlerschranke werdne in param 
angegeben.
**********************/
#ifndef CC_COMP
void sol_sor_ad_iteration(Sparse_matrix *q, Int_vektor *partition, 
                          Double_vektor *v1, Solution_param *param)
#else
void sol_sor_ad_iteration(q, partition, v1, param) 
  Sparse_matrix   *q         ;
  Int_vektor      *partition ;
  Double_vektor   *v1        ;
  Solution_param  *param     ;
#endif
{
  Sparse_matrix *qa, *qt                 ;
  Double_vektor *v2, *va                 ;
  int            r, c, iter=0            ; 
  double         n_sum, n_max=1.0e+6, n_two, comp_norm=1.0e+6     ;
  double         starttime = mat_timeused() ;
  double         endtime   = mat_timeused() ;
  double         val                     ;

  qt = mat_sparse_matrix_transpose(q, q->ord-1) ;
  v2 = mat_double_vektor_new(q->ord) ;
  for (r = 0; r < qt->ord; r++)
  if (qt->diagonals[r] != 0.0)
    qt->diagonals[r] = 1.0 / -qt->diagonals[r] ;
  else
  {
     fprintf(stderr, "Zero occured in diagonal of generator matrix\n") ;
     exit(-1) ;
  }
  qa = sol_gen_agg_mat(q, partition) ;
  va = mat_double_vektor_new(qa->ord) ;
#if SOLUTION_TEST1
  printf("Matrix structures for solution generated time used %.3e\n",
          mat_timeused() - starttime                                ) ;
#endif
  starttime = mat_timeused() ;
  endtime   = mat_timeused() ;

  while (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit                  )
  {
    for (r = 0; r < qt->ord; r++)
    {
      val = 0.0 ;
      for (c = 0; c < (qt->rowind[r]).no_of_entries; c++)
        val += 
         (qt->rowind[r]).val[c] * v1->vektor[(qt->rowind[r]).colind[c]] ;
      if (param->relaxation != 1.0)
       v1->vektor[r] = (1.0 - param->relaxation) * v1->vektor[r] +
                       param->relaxation * val * qt->diagonals[r] ;
      else
        v1->vektor[r] = val * qt->diagonals[r] ;
    }
    iter++ ;
    if (iter % RES_DIST == 0 || iter == param->no_of_iterations)
    {
      for (r = 0; r < v2->no_of_entries; r++)
        v2->vektor[r] = 0.0 ;
      mat_double_vektor_normalise(v1) ;
      for (r = 0; r < qt->ord; r++)
      for (c = 0; c < (qt->rowind[r]).no_of_entries; c++)
        v2->vektor[r] += 
          v1->vektor[(qt->rowind[r]).colind[c]] * (qt->rowind[r]).val[c] ;
      for (r = 0; r < qt->ord; r++)
        v2->vektor[r] -= v1->vektor[r] / qt->diagonals[r] ;
      mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
#if SOLUTION_TEST1
   printf("Residuals after %i iterations max %.5e, sum %.5e two %.5e\n",
	  iter, n_max, n_sum, n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
#endif
      endtime = mat_timeused () ;
      if (((iter <= 100 && iter % 10 == 0) ||
           (iter >  100 && iter % 50 == 0 )  ) &&
          (comp_norm > param->epsilon && iter < param->no_of_iterations &&
           endtime - starttime < param->time_limit))
      {
        sol_ad_step(qa, q, va, v1, partition) ;
#if SOLUTION_TEST1
  for (r = 0; r < v2->no_of_entries; r++)
    v2->vektor[r] = 0.0 ;
  mat_double_vektor_normalise(v1) ;
  for (r = 0; r < qt->ord; r++)
  for (c = 0; c < (qt->rowind[r]).no_of_entries; c++)
    v2->vektor[r] += 
      v1->vektor[(qt->rowind[r]).colind[c]] * (qt->rowind[r]).val[c] ;
  for (r = 0; r < qt->ord; r++)
    v2->vektor[r] -= v1->vektor[r] / qt->diagonals[r] ;
  mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
  printf("Residuals after a/d step max %.3e, sum %.3e two %.3e\n",
	 n_max, n_sum, n_two) ;
#endif 
      }
    } 
  }
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = endtime - starttime ;
  mat_double_vektor_free (v2) ;
  mat_double_vektor_free (va) ;
  mat_sparse_matrix_free (qa) ;
  mat_sparse_matrix_free (qt) ;
  mat_double_vektor_normalise(v1) ;
} /* sol_sor_ad_iteration */

/**********************
sol_extract_diag
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        row_start, row_start Blockgrenzen
Output: Funktionswert extrahierter Diagonalblock
Side-Effects:
Description:
Die Funktion den druch row_start und row_stop begrenzten Diagonalblock ader Matrix q.
**********************/
#ifndef CC_COMP
Sparse_matrix *sol_extract_diag(Sparse_matrix *q, int row_start, int row_stop)
#else
Sparse_matrix *sol_extract_diag(q, row_start, row_stop)
  Sparse_matrix *q         ;
  int            row_start ;
  int            row_stop  ;
#endif
{
  unsigned       i, j, k  ;
  unsigned       length   ;
  double        *values   ;
  Row           *help_row ;
  Sparse_matrix *q_sub    ;

  q_sub = mat_sparse_matrix_new (row_stop - row_start) ;
  values = (double *) ecalloc(row_stop-row_start, sizeof(double)) ;
  for (i = row_start; i < row_stop; i++)
  {
    q_sub->diagonals[i-row_start] = q->diagonals[i] ;
    q->diagonals[i]               = 0.0             ;
    for (k = 0; k < row_stop - row_start; k++)
      values[k] = 0.0 ;
    length = 0 ;
    j = 0 ;
    while (j < (q->rowind[i]). no_of_entries && 
           (q->rowind[i]).colind[j] < row_start)
      j++ ;
    while (j < (q->rowind[i]). no_of_entries && 
           (q->rowind[i]).colind[j] < row_stop)
    {
      length++ ;
      k = (q->rowind[i]).colind[j] - row_start ;
      values[k] =  (q->rowind[i]).val[j] ;
      (q->rowind[i]).val[j] = 0.0 ;
      j++ ;
    }
    (q_sub->rowind[i-row_start]).colind = (size_t *) ecalloc(length, sizeof(size_t)) ;
    (q_sub->rowind[i-row_start]).val = (double *) ecalloc(length, sizeof(double)) ;
    (q_sub->rowind[i-row_start]).no_of_entries = length ;
    k = 0 ;
    j = 0 ;
    help_row = mat_row_new ((q->rowind[i]).no_of_entries - length) ;
    while (k < length  && j < row_stop - row_start)
    {
      if (values[j] != 0.0)
      {
        (q_sub->rowind[i-row_start]).colind[k] = j         ;
        (q_sub->rowind[i-row_start]).val[k]    = values[j] ;
        k++ ;
      }
      j++ ;
    }
    help_row = mat_row_new((q->rowind[i]).no_of_entries - length) ;
    j = 0 ;
    k = 0 ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    if ((q->rowind[i]).val[j] != 0.0)
    {
       help_row->colind[k] = (q->rowind[i]).colind[j] ;
       help_row->val[k]    = (q->rowind[i]).val[j]    ;
      k++ ;
    }
    efree((q->rowind[i]).colind) ;
    efree((q->rowind[i]).val) ;
    (q->rowind[i]).colind        = help_row->colind        ;
    (q->rowind[i]).val           = help_row->val           ;
    (q->rowind[i]).no_of_entries = help_row->no_of_entries ;
  }
  efree(values) ;
  return(q_sub) ;
} /* sol_extract_diag */

/**********************
sol_iterative_solve
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        v1 Startvektor
        b rechseitiger Vektor
        epsilon Fehlerschranke
Output: v1 Loesungsvektor
Side-Effects:
Description:
Die Funktion loest das Gleichungssystem xA = b iterative.
**********************/
#ifndef CC_COMP
void sol_iterative_solve
      (Sparse_matrix *q , Double_vektor *v1, Double_vektor *b, double epsilon)
#else
void sol_iterative_solve(q , v1, b, epsilon) 
  Sparse_matrix *q       ;
  Double_vektor *v1      ;
  Double_vektor *b       ;
  double         epsilon ;
#endif
{
  int      r, c, iter=0        ; 
  double   n_sum, n_max=1.0e+6, n_two, comp_norm = 1.0e+6 ;
  double   val                 ;

  while ((comp_norm > epsilon) && (iter < 100)) 
  {
    for (r = 0; r < q->ord; r++)
    {
      val = 0.0 ;
      for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
        val += 
         (q->rowind[r]).val[c] * v1->vektor[(q->rowind[r]).colind[c]] ;
      v1->vektor[r] = (val - b->vektor[r]) * q->diagonals[r] ;
    }
    iter++ ;
    if ((iter == 1) || (iter % RES_DIST == 0))
    {
      n_sum = 0.0 ;
      n_max = 0.0 ;
      n_two = 0.0 ;
      for (r = 0; r < q->ord; r++)
      {
        val = 0.0 ;
        for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
          val += 
           (q->rowind[r]).val[c] * v1->vektor[(q->rowind[r]).colind[c]] ;
        val -= v1->vektor[r] / q->diagonals[r] + b->vektor[r] ;
        n_sum += val ;
        n_max = (fabs(val) > n_max) ? fabs(val) : n_max ;
	n_two += val * val ;
      } 
      n_two = sqrt(n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif 
    }
  }
} /* sol_iterative_solve */


/**********************
sol_ad_substitution
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  lu faktoriserte Koeffizientenmatrix
        b rechseitiger Vektor
Output: b Loesungsvektor
Side-Effects:
Description:
Die Funktion fuehrt die Ruecksubstitution fuer das Gleichungssystem xA = b durch, 
wobei A eine eine Faktorisierung ist.
**********************/
#ifndef CC_COMP
void sol_ad_substitution (Sparse_matrix *lu, Double_vektor *b)
#else
void sol_ad_substitution (lu, b)
  Sparse_matrix *lu ;
  Double_vektor *b  ;
#endif
{
  int  i, j  ;

  for (i = 0; i < lu->ord; i++)
  {
    j = 0 ;
    while ((j < (lu->rowind[i]).no_of_entries) && 
           ((lu->rowind[i]).colind[j] < i)      )
    {
      b->vektor[i] -= 
       b->vektor[(lu->rowind[i]).colind[j]] * (lu->rowind[i]).val[j] ;
      j++ ;
    }
    b->vektor[i] *= lu->diagonals[i] ;
  }
  for (i = lu->ord - 2; i >= 0; i--)
  {
    j = 0 ;
    while (j <  (lu->rowind[i]).no_of_entries 
             && (lu->rowind[i]).colind[j] < i )
      j++ ;
    while (j <  (lu->rowind[i]).no_of_entries)
    {
      b->vektor[i] -= b->vektor[(lu->rowind[i]).colind[j]] * 
                      (lu->rowind[i]).val[j]                ;
      j++ ;
    }
  }
} /* sol_ad_substitution */

/**********************
sol_solve_submatrix
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  qa Diagonalblock von q
        q Koeffizientenmatrix
        v1 Startvektor
        row_start, row_stop Blockgrenzen
        epsilon Genauigkeit der Loesung
        direct bei TRUE direkte, sonst iterative Loesung
Output: v1 Loesungsvektor
Side-Effects:
Description:
Die Funktion loest ein Gleichungssystem xA = b, wobei A eine Diagonalblock von Q ist
und b aus den Nebendiagonalbloecken und dem aktuellen Iterationsvektor v1 gebildet
wird.
**********************/
#ifndef CC_COMP
void sol_solve_submatrix (Sparse_matrix *qa, Sparse_matrix *q, Double_vektor *v1, 
                          int row_start, int row_stop, double epsilon, int direct)
#else
void sol_solve_submatrix (qa, q, v1, row_start, row_stop, epsilon, direct) 
  Sparse_matrix *qa        ;
  Sparse_matrix *q         ;
  Double_vektor *v1        ;
  int            row_start ;
  int            row_stop  ;
  double         epsilon   ;
  int            direct    ;
#endif
{
  Double_vektor *b, *v ;
  int            i, j  ;

  b = mat_double_vektor_new(qa->ord) ;
  /* compute the right hand vector */
  for (i = row_start; i < row_stop; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    b->vektor[i - row_start] += 
      -(q->rowind[i]).val[j] * v1->vektor[(q->rowind[i]).colind[j]] ;
  if (direct)
  {
    sol_ad_substitution (qa, b)   ;
    for (i  = 0; i < b->no_of_entries; i++)
      v1->vektor[i + row_start] = b->vektor[i] ;
  }
  else 
  {
    v = mat_double_vektor_new(qa->ord) ;
    for (i  = 0; i < v->no_of_entries; i++)
      v->vektor[i] = v1->vektor[i + row_start] ;
    sol_iterative_solve(qa, v, b, epsilon) ;
    for (i  = 0; i < v->no_of_entries; i++)
      v1->vektor[i + row_start] = v->vektor[i] ;
    mat_double_vektor_free(v) ; 
  }
  mat_double_vektor_free(b) ;
} /* sol_solve_submatrix */

/**********************
sol_kms
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        partition Blockstruktur der Matrix
        v1 Startvektor
        param Loesungsparameter
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der KMS Methode. Die maximale Iterationszahl, 
maximale CPU Zeit, der Relaxationsparameter und die Fehlerschranke werdne in param 
angegeben.
**********************/
#ifndef CC_COMP
void sol_kms (Sparse_matrix *q, Int_vektor *partition, 
              Double_vektor *v1, Solution_param *param)
#else
void sol_kms(q, partition, v1, param) 
  Sparse_matrix   *q         ;
  Int_vektor      *partition ;
  Double_vektor   *v1        ;
  Solution_param  *param     ;
#endif
{
  Sparse_matrix **dq, *qa, *qt              ;
  Double_vektor *v2, *va                    ;
  int           *direct                     ;
  int            i, r, c, iter=0            ; 
  double         n_sum, n_max=1.0e+6, n_two, comp_norm=1.0e+6  ;
  double         epsilon=param->epsilon     ;
  double         starttime = mat_timeused() ;
  double         endtime  = mat_timeused()  ;

  v2 = mat_double_vektor_new(q->ord)  ;
  qa = sol_gen_agg_mat(q, partition)  ;
  qt = mat_sparse_matrix_transpose(q, q->ord-1) ;
  va = mat_double_vektor_new(qa->ord) ;
  direct = (int *) ecalloc (qa->ord, sizeof(int)) ;
  for (i = 0; i < qa->ord; i++)
    direct[i] = FALSE ;
  dq = (Sparse_matrix **) ecalloc 
         (partition->no_of_entries, sizeof(Sparse_matrix *));
  starttime = mat_timeused() ;
  for (i = 0; i < partition->no_of_entries - 1; i++)
  {
    r = partition->vektor[i] ;
    c = partition->vektor[i+1] ;
    dq[i] = sol_extract_diag(qt, r, c) ;
    if (dq[i]->ord <= 500)
    {
      if (!(sol_factorise(dq[i])))
        direct[i] = TRUE ;
    }
    if (direct[i] == FALSE) 
    {
      for (r = 0; r < dq[i]->ord; r++)
      if (dq[i]->diagonals[r] != 0.0)
        dq[i]->diagonals[r] = 1.0 / -dq[i]->diagonals[r] ;
      else
      {
         fprintf
          (stderr, "Zero occured in diagonal of generator matrix\n") ;
         exit(-1) ;
      }
    }
  }
#if SOLUTION_TEST1
  printf("Matrix structures for solution generated time used %.3e\n",
          mat_timeused() - starttime                                ) ;
#endif
  endtime   = mat_timeused() ;
  while (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit                  )
  {
    sol_ad_step(qa, q, va, v1, partition) ; 
    for (i = 0;  i < partition->no_of_entries - 1; i++)
      sol_solve_submatrix (dq[i], qt, v1, partition->vektor[i],  
                           partition->vektor[i+1], epsilon, direct[i]) ;
    mat_double_vektor_normalise(v1) ;
    for (r = 0; r < v2->no_of_entries; r++)
      v2->vektor[r] = 0.0 ;
    for (r = 0; r < q->ord; r++)
    for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
      v2->vektor[(q->rowind[r]).colind[c]] += v1->vektor[r] 
                     * (q->rowind[r]).val[c] ;
    for (r = 0; r < q->ord; r++)
      v2->vektor[r] += v1->vektor[r] * q->diagonals[r] ;
    mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
    iter++ ;
#if SOLUTION_TEST1
   printf("Residuals after %i iterations max %.5e, sum %.5e two %.5e\n",
	  iter, n_max, n_sum, n_two) ;
#endif
    endtime = mat_timeused () ;
  }
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = endtime - starttime ;
  mat_double_vektor_free (v2) ;
  mat_double_vektor_free (va) ;
  mat_sparse_matrix_free (qa) ;
  mat_sparse_matrix_free (qt) ;
  for (i = 0; i < partition->no_of_entries; i++)
    mat_sparse_matrix_free(dq[i]) ;
  efree(dq) ;
  efree(direct) ;
} /* sol_kms */

/**********************
sol_half_matrix
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Matrix der feineren Ebene
        r Hilfsvektor 
Output: Funktionswert Aggregatmatrix
Side-Effects:
Description:
Die Funktion generiert die Aggregatmatrixstruktur der feineren Ebene, wobei jeweils
AGG_SIZE benachbarte Elemente aggregiert werden.
**********************/
#ifndef CC_COMP
Sparse_matrix *sol_half_matrix(Sparse_matrix *q, double *r)
#else
Sparse_matrix *sol_half_matrix(q, r)
  Sparse_matrix *q ;
  double        *r ;
#endif
{
  int            i, j, k, l, stop, col ;
  Sparse_matrix *qa                    ;

  qa = mat_sparse_matrix_new(q->ord / AGG_SIZE) ;
  for (i = 0; i < qa->ord; i++)
  {
    for (j = 0; j < qa->ord; j++)
      r[j] = 0.0 ;
    stop = FALSE ;
    k    = AGG_SIZE * i   ;
    while(stop == FALSE)
    {
      for (l = 0; l  < (q->rowind[k]).no_of_entries; l++)
      {
        col = (q->rowind[k]).colind[l] / AGG_SIZE ;
        if (col >= qa->ord) 
          col = qa->ord - 1 ;
        r[col] = 1.0 ;
      }
      if (k < (i+1) * AGG_SIZE - 1 || 
          (k < q->ord - 1 && k >= q->ord - AGG_SIZE))
        k++ ;
      else
        stop = TRUE ; 
    }
    k = 0 ;
    for (j = 0; j < qa->ord; j++)
    if (r[j] > 0.0 && j != i)
      k++ ;
    (qa->rowind[i]).colind = (size_t *) ecalloc (k, sizeof(size_t)) ;
    (qa->rowind[i]).val    = (double *) ecalloc (k, sizeof(double)) ;
    (qa->rowind[i]).no_of_entries = k ;
    k = 0 ;
    for (j = 0; j < qa->ord; j++)
    if (r[j] > 0.0 && j != i)
    {
      (qa->rowind[i]).colind[k] = j ;
      k++ ;
    }
  }
  return(qa) ;
} /* sol_half_matrix */

/**********************
sol_fill_ml_mat
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  qa Aggregatmatrixstruktur
        q Matrix der feineren Ebene
        p Iterationsvektor der feineren Ebene
        pa Iterationsvektor der aggregierten Ebene 
Output: qa Aggregatmatrix
Side-Effects:
Description:
Die Funktion besetzt die Aggregatmatri xbzgl. des aktuellen Iterationsvektors und der
Matrix der feineren Ebene.
**********************/
#ifndef CC_COMP
int sol_fill_ml_mat
      (Sparse_matrix *qa, Sparse_matrix *q, Double_vektor *p, Double_vektor *pa)
#else
int sol_fill_ml_mat(qa, q, p, pa)
  Sparse_matrix *qa ;
  Sparse_matrix *q  ;
  Double_vektor *p  ;
  Double_vektor *pa ;
#endif
{
  int i, j, k, l, col, stop ;

  for (i = 0; i < qa->ord; i++)
  {
    for (j = 0; j < (qa->rowind[i]).no_of_entries; j++)
      (qa->rowind[i]).val[j] = 0.0 ;
    stop = FALSE ;
    l    = AGG_SIZE * i ;
    while(stop == FALSE)
    {
      k = 0 ;
      j = 0 ;
      while (j < (qa->rowind[i]).no_of_entries &&
             k < (q->rowind[l]).no_of_entries  )
      {
        col = (q->rowind[l]).colind[k] / AGG_SIZE ;
        if (col >= qa->ord) 
          col = qa->ord - 1 ;
        if (col != i)
        {
          while (j < (qa->rowind[i]).no_of_entries &&
                 (qa->rowind[i]).colind[j] < col)
            j++ ;
          if (j < (qa->rowind[i]).no_of_entries &&
              pa->vektor[(qa->rowind[i]).colind[j]] > 0.0)
            (qa->rowind[i]).val[j] += 
             (p->vektor[(q->rowind[l]).colind[k]] /
              pa->vektor[(qa->rowind[i]).colind[j]]) *
              (q->rowind[l]).val[k]                  ;
           k++ ;
        }
        else
          k++ ;
      }
      if (l < (i+1) * AGG_SIZE - 1 || 
          (l < q->ord - 1 && l >= q->ord - AGG_SIZE))
        l++ ;
      else
        stop = TRUE ; 
    }
  }  
  for (i = 0; i < qa->ord; i++)
    qa->diagonals[i] = 0.0 ;
  for (i = 0; i < qa->ord; i++)
  for (j = 0; j < (qa->rowind[i]).no_of_entries; j++)
    qa->diagonals[(qa->rowind[i]).colind[j]] -= (qa->rowind[i]).val[j] ;
  return(NO_ERROR) ;
} /* sol_fill_ml_mat */

/**********************
sol_gs_iter
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Iterationsmatrix
        v_old Startvektor 
Output: Loesungsvektor
Side-Effects:
Description:
Die Funktion fuehrt LOCAL_ITER GS Iterationsschritte aus.
**********************/
#ifndef CC_COMP
int sol_gs_iter (Sparse_matrix *q, Double_vektor *v_old, Double_vektor *v_new)
#else
int sol_gs_iter (q, v_old, v_new)
  Sparse_matrix *q     ;
  Double_vektor *v_old ;
  Double_vektor *v_new ;
#endif
{
  int    iter, i, j ;
  double val        ;

  iter = 0 ;
  for (i = 0; i < q->ord; i++)
    v_new->vektor[i] = v_old->vektor[i] ;

  if (q->ord == 1)
    return(NO_ERROR) ;
  while (iter < LOCAL_ITER) 
  {
    for (i = 0; i < q->ord; i++)
    {
      val = 0.0 ;
      for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
        val += (q->rowind[i]).val[j] * 
                v_new->vektor[(q->rowind[i]).colind[j]] ;
      if (q->diagonals[i] < 0.0)
        v_new->vektor[i] = val / -q->diagonals[i] ;
      else
        v_new->vektor[i] = 0.0 ;
    }
    iter++ ;
  }
  mat_double_vektor_normalise(v_new) ; 
  return(NO_ERROR) ;
} /* sol_gs_iter */

/**********************
sol_restriction
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  p1 Iterationsvektor 
Output: p0 Projektion von p1 auf die darueber liegende Ebene
Side-Effects:
Description:
Die Funktion fuehrt die Restriktionsoperation aus.
**********************/
#ifndef CC_COMP
void sol_restriction(Double_vektor *p1,  Double_vektor *p0)
#else
void sol_restriction(p1,  p0)
  Double_vektor *p1 ;
  Double_vektor *p0 ;
#endif
{
  int i, j ;
  
  i = 0 ;
  while (i < p0->no_of_entries) 
  {
    p0->vektor[i]  = p1->vektor[i * AGG_SIZE]   ;
    j = 1 ;
    while (j < AGG_SIZE || 
           (i+1 >= p0->no_of_entries && 
            i * AGG_SIZE + j < p1->no_of_entries   ) )
    {
      p0->vektor[i] += p1->vektor[i * AGG_SIZE + j] ;
      j++ ;
    }
    i++ ;
  }
} /* sol_restriction */

/**********************
sol_interpol
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  p Gesamtloesungsvektor
        v_old Bisheriger Loesungsvektor
        v_new Neuer Loesungsvektor
Output: p bzgl. v_new/v_old interpoliert
Side-Effects:
Description:
Die Funktion fuehrt den Interpolationsschritt durch.
**********************/
#ifndef CC_COMP
void sol_interpol(Double_vektor *p, Double_vektor *v_new, Double_vektor *v_old)
#else
void sol_interpol(p, v_new, v_old) 
  Double_vektor *p     ;
  Double_vektor *v_new ;
  Double_vektor *v_old ;
#endif
{
  int    i, j, normalise = TRUE  ;
  double val ;

  i = 0 ;
  while (i < v_new->no_of_entries) 
  {
    if (v_old->vektor[i] > 0.0)
      val = v_new->vektor[i]  / v_old->vektor[i] ; 
    else
    {
      normalise = TRUE ;
      val = 0.0 ;
    }
    j = 0 ;
    while (j < AGG_SIZE || 
           (i+1  >= v_new->no_of_entries && 
            i * AGG_SIZE + j < p->no_of_entries   ) )
    {
      p->vektor[i * AGG_SIZE + j]   *= val ;
      j++ ;
    }
    i++ ;
  }
  if (normalise == TRUE) 
    mat_double_vektor_normalise(p) ;
} /* sol_interpol */

/**********************
sol_mss
Datum:  17.01.04
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  matrizen Koeffizientenmatrizen der verschiedenen Ebene
        v_old Bisherige Loesungsvektoren den Ebenen
        level Nummer der Ebene
Output: v_new Neue Loesungsvektoren den Ebenen
Side-Effects:
Description:
Die Funktion fuehrt den Rekursionschritt des Multi-Level Algorithmus durch.
**********************/
#ifndef CC_COMP
void sol_mss
     (Sparse_matrix **matrizen, Double_vektor **v_old, Double_vektor **v_new, int level)
#else
void sol_mss(matrizen, v_old, v_new, level)
  Sparse_matrix **matrizen ;
  Double_vektor **v_old    ;
  Double_vektor **v_new    ;
  int             level    ;
#endif
{
  if (level == 0)
  {
    if (sol_direct_solution(matrizen[0], v_new[0], FALSE, NULL) != NO_ERROR)
      sol_gs_iter(matrizen[0],v_old[0], v_new[0]) ; 
    else
      mat_double_vektor_normalise(v_new[0]) ;
  }
  else
  {
    sol_gs_iter (matrizen[level], v_old[level], v_new[level]) ;
    sol_restriction(v_new[level], v_old[level-1]) ;
    if (sol_fill_ml_mat
     (matrizen[level-1], matrizen[level], v_new[level], v_old[level-1]) 
        == NO_ERROR)
    {
      sol_mss(matrizen, v_old, v_new, level-1) ;
      sol_interpol(v_new[level], v_new[level-1], v_old[level-1]) ;
    }
    else
      printf("Stop refinement in level %i\n", level) ; 
  }
} /* sol_mss */

/**********************
sol_ml
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        partition Blockstruktur der Matrix
        p Startvektor
        param Loesungsparameter
Output: p Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der Multi-Level Methode. Die maximale Iterationszahl, 
maximale CPU Zeit, der Relaxationsparameter und die Fehlerschranke werdne in param 
angegeben.
**********************/
#ifndef CC_COMP
void sol_ml
     (Sparse_matrix *q, Int_vektor *partition, Double_vektor *v1, Solution_param *param)
#else
void sol_ml(q, partition, v1, param) 
  Sparse_matrix   *q         ;
  Int_vektor      *partition ;
  Double_vektor   *v1        ;
  Solution_param  *param     ;
#endif
{
  int             i, j, no, iter          ;
  double          n_max=1.0e+6, n_sum, n_two, comp_norm = 1.0e+6     ;
  double          starttime, endtime      ;
  double         *r, val                  ;
  Sparse_matrix **matrizen, *qh           ;
  Double_vektor **v_new, **v_old, *v_help ;

  starttime = mat_timeused() ;
  no = 1      ;
  i  = q->ord ;
  while (i > AGG_SIZE)
  {
    no++ ;
    i = i / AGG_SIZE ;
  }
  i = (q->ord / AGG_SIZE) + (q->ord % AGG_SIZE) ;
  r = (double *) ecalloc (i, sizeof(double)) ;
  matrizen = (Sparse_matrix **) ecalloc (no, sizeof(Sparse_matrix *)) ;
  v_new    = (Double_vektor **) ecalloc (no, sizeof(Double_vektor *)) ;
  v_old    = (Double_vektor **) ecalloc (no, sizeof(Double_vektor *)) ;
  matrizen[no-1] = mat_sparse_matrix_transpose(q, q->ord - 1) ;
  for (i = no - 2; i >= 0; i--)
    matrizen[i] = sol_half_matrix(matrizen[i+1], r) ;
  efree(r) ;
  for (i = 0; i < no; i++)
  {
    v_new[i]  = mat_double_vektor_new(matrizen[i]->ord) ;
    v_old[i]  = mat_double_vektor_new(matrizen[i]->ord) ; 
  }
  for (i = 0; i < v1->no_of_entries; i++)
    v_old[no-1]->vektor[i] = v1->vektor[i] ;
#if SOLUTION_TEST1
  printf("Matrix structures for solution generated time used %.3e\n",
          mat_timeused() - starttime                                ) ;
#endif
  starttime = mat_timeused() ;
  endtime   = mat_timeused() ;
  iter = 0 ;
  while (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit                  )
  {
    sol_mss(matrizen, v_old, v_new, no - 1) ;
    for (i = 0; i < no; i++)
    {
      v_help   = v_new[i] ;
      v_new[i] = v_old[i] ;
      v_old[i] = v_help   ;
    }
    iter++ ;

    if (iter % 5 == 0 || iter == param->no_of_iterations)
    {
      mat_double_vektor_normalise(v1) ;
      n_max = 0.0 ;
      n_sum = 0.0 ;
      n_two = 0.0 ;
      qh = matrizen[no-1] ;
      for (i = 0; i < qh->ord; i++)
      {
        val = 0.0 ;
        for (j = 0; j < (qh->rowind[i]).no_of_entries; j++)
        val += (qh->rowind[i]).val[j] * 
                v_old[no-1]->vektor[(qh->rowind[i]).colind[j]] ;
        val += v_old[no-1]->vektor[i] * qh->diagonals[i] ; 
        n_sum += fabs(val) ;
        if (n_max < fabs(val))
          n_max = fabs(val) ;
	n_two += val * val ;
      }
      n_two = sqrt(n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
#if SOLUTION_TEST1
   printf("Residuals after %i iterations max %.5e, sum %.5e\n",
           iter, n_max, n_sum) ;
#endif
      endtime = mat_timeused () ;
    } 
  }

  for (i =0; i < v1->no_of_entries; i++)
    v1->vektor[i] = v_old[no-1]->vektor[i] ;
  param->no_of_iterations = iter  ;
  param->epsilon          = n_max ;
  param->time_limit       = endtime - starttime ;
  for (i = 0; i < no; i++)
    mat_sparse_matrix_free(matrizen[i]) ;
  for (i = 0; i < no; i++)
  {
    mat_double_vektor_free(v_new[i]) ;
    mat_double_vektor_free(v_old[i]) ;
  }
  efree(matrizen) ;
  efree(v_new) ;
  efree(v_old) ;
} /* sol_ml */


/**********************
sol_sub_vektor
Datum:  14.06.98
Autor:  Peter Buchholz
Input:  w, v Double Vektoren (gleicherLaenge)
        h Skalar
Output: w Ergebnisvektor
Side-Effects:
Description:
Die Funktion berechnet w = w - h*v.
***********************************************************************/
#ifndef CC_COMP
int sol_sub_vektor(Double_vektor *w, Double_vektor *v, double h)
#else
int sol_sub_vektor(Double_vektor *w, Double_vektor *v, double h)
  Double_vektor *w   ;
  Double_vektor *v   ;
  double         h   ;
#endif
{
  int i ;

  for (i = 0; i < w->no_of_entries; i++)
    w->vektor[i] -= h * v->vektor[i] ;
  return(NO_ERROR) ;
} /* sol_sub_vektor */

/**********************
sol_inner_product
Datum:  14.06.98
Autor:  Peter Buchholz
Input: w, v strukturierte Vektoren (gleicher Laenge)
Output: res inneres Produkt der Vektoren
Side-Effects:
Description:
Die Funktion berechnet das innere Produkt der strukturierten vektoren v und w.
***********************************************************************/
#ifndef CC_COMP
int sol_inner_product
     (Double_vektor *w, Double_vektor *v, double *res) 
#else
int sol_inner_product (w, v, res)
  Double_vektor *w   ;
  Double_vektor *v   ;
  double        *res ;
#endif
{
  int i ;

  *res = 0.0 ;
  for (i = 0; i < w->no_of_entries; i++)
    *res += v->vektor[i] * w->vektor[i] ;
  return(NO_ERROR) ;
} /* sol_inner_product */

/**********************
sol_comp_preconditioner
Datum:  20.05.03
Autor:  Peter Buchholz
Input:  a Generatormatrix
        pc Auswahl Praekonditionierer (1 ILU0, 2 ILUTH)
        th Threshold fuer ILUTH
Output: Rueckgabewert unvollstanedige LU Zerlegung von a
Side-Effects:
Description:
Die Funktion berechnet unvollstaendige LU Faktoren fuer Matrix a. Es werden die
Praekonditionierer ILU0 und ILUTh mit Threshold th unteresteutzt.
***********************************************************************/
#ifndef CC_COMP
Sparse_matrix *sol_comp_preconditioner
     (Sparse_matrix *a, int pc, double th)
#else
Sparse_matrix *sol_comp_preconditioner (a, pc, th)
  Sparse_matrix *a  ;
  int            pc ;
  double         th ;
#endif
{
    size_t i, j, k, l, m;
    double *values, max_diag ;
    Sparse_matrix *lu ;

    /* Zur Skalierung wird bei ILUTh das maximale Diagonalelement ben�tigt */
    if (pc == 2) {
      /* max_diag = mat_sparse_matrix_max_diagonal (a) ;
	th *= max_diag ;
	printf("Threshold %.3e\n", th) ; */
    }
    lu = mat_sparse_matrix_copy(a) ;
    mat_sparse_matrix_insert_diagonals(lu) ;
    mat_set_lud(lu) ;
    if (pc == 1) {
        /* ILU0 Praekonditionierer */
#if NEW_VERSION
	for (k = 1; k < lu->ord; k++) {
	    for (i = 0; i < (lu->rowind[k]).no_of_entries && (lu->rowind[k]).colind[i] < k; i++) {
		l = (lu->rowind[k]).colind[i] ;
		if (lu->d[i] < 0 || (lu->rowind[i]).val[lu->d[i]] == 0.0) {
		    printf("Factorization of ILU-preconditioner failed\nSTOP EXECUTION\n") ;
		    exit(-1) ;
		}
		(lu->rowind[k]).val[i] /= (lu->rowind[i]).val[lu->d[i]] ;
		m = 0 ;
		for (j = lu->u[i]; j < (lu->rowind[i]).no_of_entries; j++) {
		    l = (lu->rowind[i]).colind[j] ;
		    while (m < (lu->rowind[k]).no_of_entries && (lu->rowind[k]).colind[m] < l) {
			m++ ;
		    }
		    if (m >= (lu->rowind[k]).no_of_entries)
			break ;
		    if ((lu->rowind[k]).colind[m] == l)
			(lu->rowind[k]).val[m] -= (lu->rowind[k]).val[i] * (lu->rowind[i]).val[j] ;
		}
	    }
	}
#else
	values = (double *) ecalloc (a->ord, sizeof(double)) ;
	for (k = 1; k < lu->ord; k++) {
	    for (i = 0; i < (lu->rowind[k]).no_of_entries; i++)
		values[(lu->rowind[k]).colind[i]] =  (lu->rowind[k]).val[i] ;
	    for (i = 0; i < k; i++) {
		if (values[i] != 0.0) {
		    if (lu->d[i] < 0 || (lu->rowind[i]).val[lu->d[i]] == 0.0) {
			printf("Factorization of ILU-preconditioner failed\nSTOP EXECUTION\n") ;
			exit(-1) ;
		    }
		    values[i] /= (lu->rowind[i]).val[lu->d[i]] ;
		    for (j = lu->u[i]; j < (lu->rowind[i]).no_of_entries; j++) {
			l = (lu->rowind[i]).colind[j] ;
			values[l] -= values[i] * (lu->rowind[i]).val[j] ;
		    }
		}
	    }
	    for (i = 0; i < (lu->rowind[k]).no_of_entries; i++) {
		(lu->rowind[k]).val[i] = values[(lu->rowind[k]).colind[i]] ;
	    }
	    if (k < lu->ord - 1) {
		for (i = 0; i < lu->ord; i++)
		    values[i] = 0.0 ;
	    }
	}
	efree(values) ;
#endif
    }
    else {
        /* ILUTH Praekonditionierer */
	values = (double *) ecalloc (a->ord, sizeof(double)) ;
	for (k = 1; k < lu->ord; k++) {
	    for (i = 0; i < (lu->rowind[k]).no_of_entries; i++)
		values[(lu->rowind[k]).colind[i]] =  (lu->rowind[k]).val[i] ;
	    for (i = 0; i < k; i++) {
		if (values[i] != 0.0) {
		    if (lu->d[i] < 0 || (lu->rowind[i]).val[lu->d[i]] == 0.0) {
			printf("Factorization of ILU-preconditioner failed\nSTOP EXECUTION\n") ;
			exit(-1) ;
		    }
		    values[i] /= (lu->rowind[i]).val[lu->d[i]] ;
		    if (fabs(values[i]) < th) {
			values[i] = 0.0 ;
		    }
		    else {
			for (j = lu->u[i]; j < (lu->rowind[i]).no_of_entries; j++) {
			    values[(lu->rowind[i]).colind[j]] -= values[i] * (lu->rowind[i]).val[j] ;
			}
		    }
		}
	    }
	    l = 0 ;
	    for (i = 0; i < lu->ord; i++) {
		if (fabs(values[i]) < th && i != k)
		    values[i] = 0.0 ;
		else
		    l++ ;
	    }
	    if (l > (lu->rowind[k]).no_of_entries) {
		efree((lu->rowind[k]).val) ;
		efree((lu->rowind[k]).colind) ;
        (lu->rowind[k]).val = (double *) ecalloc((unsigned int) l, sizeof(double)) ;
        (lu->rowind[k]).colind = (size_t *) ecalloc((unsigned int) l, sizeof(size_t)) ;
	    }
	    (lu->rowind[k]).no_of_entries = l ;
	    l = 0 ;
	    lu->d[k] = -1 ;
            lu->u[k] = (lu->rowind[k]).no_of_entries ;
	    lu->l[k] = 0 ;
	    for (i = 0; i < lu->ord; i++) {
		if (values[i] != 0.0) {
		    (lu->rowind[k]).val[l] = values[i] ;
		    (lu->rowind[k]).colind[l] = i ;
		    values[i] = 0.0 ;
		    if (i == k) 
			lu->d[k] = l ;
		    if (i >= k && l > 0 && (lu->rowind[k]).colind[l-1] < k)
			lu->l[k] = l ;
		    if (i > k && (l == 0 || (lu->rowind[k]).colind[l-1] <= k))
			lu->u[k] = l ;
		    l++ ;
		}
	    }
	}
	efree(values) ;
    }
    return(lu) ;
} /* sol_comp_preconditioner */

/**********************
sol_substitute_precond
Datum:  31.08.98
Autor:  Peter Buchholz
Input:  lu LU-Faktoren des Praekonditionierers
        p Vektor zur Multiplikation mit dem Praekonditionierer
Output: p Loesungsvektor

Side-Effects:
Description:
Die Funktion loest p (LU)^{-1}= p U^{-1}L^{-1}.
**********************/
#ifndef CC_COMP
void sol_substitute_precond (Sparse_matrix *lu, Double_vektor *p) 
#else
void sol_substitute_precond (lu, p)
Sparse_matrix *lu ;
Double_vektor *p  ;
#endif
{
  int i, j ;
 
  /* Zuerst yU = p berechnen und die Loesung in p speichern */ 
  for (i = 0; i < lu->ord; i++) {
      if (lu->d[i] < 0 || (lu->rowind[i]).val[lu->d[i]] == 0.0) {
	  printf("FATAL ERROR Zero appeared in the diagonal of the factor table\nSTOP EXECUTION\n") ;
	  exit(-1) ;
      }
      p->vektor[i] /= (lu->rowind[i]).val[lu->d[i]] ;
      for (j = lu->u[i]; j < (lu->rowind[i]).no_of_entries; j++) 
	  p->vektor[(lu->rowind[i]).colind[j]] -= p->vektor[i] * (lu->rowind[i]).val[j] ; 
  }
  /* Dann xL = y loesen und die Loesung in p speichern 
     Diagonalelemente von L sind 1.0 ! */
  for (i = lu->ord - 1; i >= 0; i--) {
      for (j = 0; j < lu->l[i]; j++)
	  p->vektor[(lu->rowind[i]).colind[j]] -= p->vektor[i] * (lu->rowind[i]).val[j] ; 
  }
} /* sol_substitute_Precond */

/**********************
sol_substitute_precond_transpose
Datum:  17.06.10
Autor:  Peter Buchholz
Input:  lu LU-Faktoren des Praekonditionierers
        p Vektor zur Multiplikation mit dem Praekonditionierer
Output: p Loesungsvektor

Side-Effects:
Description:
Die Funktion loest  (LU)^{-1}p=  U^{-1}L^{-1}p.
**********************/
#ifndef CC_COMP
void sol_substitute_precond_transpose (Sparse_matrix *lu, Double_vektor *p) 
#else
void sol_substitute_precond_transpose (lu, p)
Sparse_matrix *lu ;
Double_vektor *p  ;
#endif
{
  int i, j ;
 
  /* Zuerst Ly = p berechnen und die Loesung in p speichern */ 
  for (i = 0; i < lu->ord; i++) {
      for (j = 0; j < lu->l[i]; j++)
	p->vektor[i] -= p->vektor[(lu->rowind[i]).colind[j]] * (lu->rowind[i]).val[j] ;
  }
  /* Dann Ux = y loesen und die Loesung in p speichern 
     Diagonalelemente von L sind 1.0 ! */
  for (i = lu->ord-1; i >= 0; i--) {
    /* if (lu->d[i] < 0 || (lu->rowind[i]).val[lu->d[i]] == 0.0) {
	  printf("FATAL ERROR Zero appeared in the diagonal of the factor table\nSTOP EXECUTION\n") ;
	  exit(-1) ;
	  } */
     for (j = lu->u[i]; j < (lu->rowind[i]).no_of_entries; j++) 
        p->vektor[i] -= p->vektor[(lu->rowind[i]).colind[j]] * (lu->rowind[i]).val[j] ; 
    if (lu->d[i] < 0 || fabs((lu->rowind[i]).val[lu->d[i]]) < 1.0e-32) 
      p->vektor[i] = 0.0 ;
    else
      p->vektor[i] /= (lu->rowind[i]).val[lu->d[i]] ;
  }
} /* sol_substitute_precond_transpose */

/**********************
sol_gmres_arnoldi_process
Datum:  14.06.98
Autor:  Peter Buchholz
Input:  q Generatormatrix
        l, u LU-Faktorisierung des Praekonditionierers, NULL,NULL falls keine
             Praekonditionierung vorgenommen wird.
        res Residualvektor
        v Vektorarray zur Bildung des Krylov Unterraums
        h Orthogonalisierungsmatrix
        g Richtungsvektor
Output: res Ergebnisvektor
Side-Effects:
Description:
Die Funktion fuehrt den Arnoldi-Prozess und die Berechnung des neuen
Iterationsvektors im iterativen GMRES Verfahren durch. Es werden
SUBSPACE_DIM Schritte zur Orthogonalisierung durchgefuehrt. Falls die Matrizen 
l und u ungliehc NULL sind werden sie als Praekonditionierer verwendet.
***********************************************************************/
#ifndef CC_COMP
int sol_gmres_arnoldi_process
(Sparse_matrix *q, Sparse_matrix *lu,
      Double_vektor *w, Double_vektor *v[SUBSPACE_DIM], 
      double h[SUBSPACE_DIM+1][SUBSPACE_DIM], double g[SUBSPACE_DIM+1], short transpose)
#else
  int sol_gmres_arnoldi_process(q, lu, w, v, h, g, transpose)
  Sparse_matrix  *q                               ;
  Sparse_matrix  *lu                              ;
  Double_vektor  *w                               ;
  Double_vektor  *v[SUBSPACE_DIM]                 ;
  double          h[SUBSPACE_DIM+1][SUBSPACE_DIM] ;
  double          g[SUBSPACE_DIM+1]               ;
  short           transpose                       ;
#endif
{
  int i, j, x ;
  double beta, gamma, help ;
  double c[SUBSPACE_DIM], s[SUBSPACE_DIM] ;

  for (i = 0; i <= SUBSPACE_DIM; i++)
  {
    g[i] = 0.0 ;
    for (j = 0; j < SUBSPACE_DIM; j++)
        h[i][j] = 0.0 ;
  }
  beta = mat_double_vektor_tnorm (w) ;
  g[0] = beta ;
  beta = 1.0 / beta ;
  for (i = 0; i < w->no_of_entries; i++)
    v[0]->vektor[i] = w->vektor[i] * beta ;
  for (j = 0; j < SUBSPACE_DIM; j++)
  {
    /* Arnoldi process */
    if (transpose)
      mat_sparse_matrix_transposed_matrix_vektor_product(q, v[j], w) ;
    else
      mat_sparse_matrix_matrix_vektor_product(q, v[j], w) ;
    /* mat_double_vektor_scalar_mult(w, -1.0) ; */
    /* mat_sparse_matrix_mult_vektor(q, &v[j], &w, TRUE) ; 
       da die Funktion mat_sparse_matrix_mult_vektor den Ergebnisvektor in 
       &v[i] zurueckliefert, werden w und v[i] vertauscht. Anschliessend steht das
       Ergebnis in w und der urspruengliche Vektor wieder in v[j]!
    ph   = w    ;
    w    = v[j] ;
    v[j] = ph   ; */
    if (lu != NULL) {
      if (transpose)
	sol_substitute_precond_transpose(lu, w) ;
      else
	sol_substitute_precond(lu, w) ;
    }
#if SHOW_TESTX
 printf("Vector w\n") ;
 for (dd = 0; dd < w->no_of_entries; dd++)
   printf("%i: %.15e\n", dd, w->vektor[dd]) ;
 printf("\n") ;
#endif

    for (i = 0; i <= j; i++)
    {
      sol_inner_product(w, v[i], &h[i][j]) ;
      sol_sub_vektor(w, v[i], h[i][j]) ;
    }   
    h[j+1][j] = mat_double_vektor_tnorm (w) ;
    if (fabs(h[j+1][j]) > 1.0e-15)
    {
      if (j + 1 < SUBSPACE_DIM)
      for (x = 0; x < w->no_of_entries; x++)
        v[j+1]->vektor[x] = w->vektor[x] / h[j+1][j] ;
    }
    else
      h[j+1][j] = 0.0 ;
    /* Update factorization of H */
    for (i = 0; i < j; i++)
    {
      help      = c[i] * h[i][j] + s[i] * h[i+1][j] ;
      h[i+1][j] = c[i] * h[i+1][j] - s[i] * h[i][j] ; 
      h[i][j]   = help ;
    } 
    gamma   = sqrt(h[j][j] * h[j][j] + h[j+1][j] * h[j+1][j]) ;
    c[j]    = h[j][j] / gamma ;
    s[j]    = h[j+1][j] / gamma ;
    help    = c[j] * g[j] ;
    g[j+1]  = -s[j] * g[j] ;
    g[j]    = help ;
    h[j][j] = c[j] * h[j][j] + s[j] * h[j+1][j] ; 
    if (fabs(g[j+1]) < 1.0e-15)
    {
      printf("Stop Arnoldi process in step %i g equals %.5e\n",
              j, g[j+1]) ;
      break ;
    }
  }
  return(NO_ERROR) ;
} /* sol_gmres_arnoldi_process */

/**********************
sol_gmres_update
Datum:  14.06.98
Autor:  Peter Buchholz
Input:  h Orthogonalisierungsmatrix
        g Richtungsvektor
        p Iterationsvektor
        v Vektorarray zur Bildung des Krylov Unterraums
Output: p Ergebnisvektor
Side-Effects:
Description:
Die Funktion berechnet den neuen Iterationsvektor im itertaiven GMRES
Verfahren.
***********************************************************************/
#ifndef CC_COMP
int sol_gmres_update
     (double h[SUBSPACE_DIM+1][SUBSPACE_DIM], double g[SUBSPACE_DIM+1],        
      Double_vektor *p, Double_vektor *v[SUBSPACE_DIM]) 
#else
int sol_gmres_update (h, g, p, v) 
  double         h[SUBSPACE_DIM+1][SUBSPACE_DIM] ;
  double         g[SUBSPACE_DIM+1]               ;
  Double_vektor *p                               ;
  Double_vektor *v[SUBSPACE_DIM]                 ;
#endif 
{
  int    i, j ;
  double y[SUBSPACE_DIM] ;

  for (i = SUBSPACE_DIM - 1; i >= 0; i--)
  {
    y[i] = g[i] ;
    for (j = SUBSPACE_DIM - 1; j > i; j--)
      y[i] -= h[i][j] * y[j] ;
    if (h[i][i] != 0.0)
      y[i] /= h[i][i] ;
    else
      y[i] = 0.0 ;
  }
#if SHOW_TESTX
  printf("solution vector \n") ;
  for (i = 0; i < SUBSPACE_DIM; i++)
    printf("y[%i] %.5e \n", i, y[i]) ;
#endif
  for (i = 0; i < SUBSPACE_DIM; i++)
  if (y[i] != 0.0)
  for (j = 0; j < p->no_of_entries; j++)
    p->vektor[j] += v[i]->vektor[j] * y[i] ;
  return(NO_ERROR) ;
}  /* sol_gmres_update */


/**********************
solve_pre_power_iteration
Datum:  03.09.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        v1 Startvektor
        param Loesungsparameter
        pc bei 1 ILU0, 2 ILUTH mit Threshold param.epsilon_2
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der praekonditionierten Power Methode. Es wird engenommen, dass q eine Q-Matrix 
beinhaltet und v1 einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit und
die Fehlerschranke werdne in param angegeben.
**********************/
#ifndef CC_COMP
int sol_pre_power_iteration
     (Sparse_matrix *q, Double_vektor *v1, Solution_param *param, int pc)
#else
int sol_pre_power_iteration(q, v1, param, pc) 
  Sparse_matrix   *q     ;
  Double_vektor   *v1    ;
  Solution_param  *param ;
  int              trc   ;
#endif
{
  Sparse_matrix *lu ;
  Double_vektor *v2                      ;
  int            r, c, iter=0            ; 
  double         n_sum, n_max=1.0, n_two, comp_norm=1.0e+6, alpha=0.0 ;
  double         starttime = mat_timeused(), ptime ;
  double         endtime ;
 
  v2 = mat_double_vektor_new(q->ord) ;
  for (r = 0; r < q->ord; r++)
  if (alpha > q->diagonals[r])
    alpha = q->diagonals[r] ;

  if (alpha < 0.0)
    alpha = 0.999 / (- alpha) ; 
  else
    return(NO_CONVERGENCE) ;
  ptime = mat_timeused() ;
  if (pc == 1)
  {
    printf("ILU0 Preconditioner\n") ;
    lu = sol_comp_preconditioner(q, 1, 0.0) ;
  }
  else

  {
    if (pc == 2)
    {
	printf("ILUTH Preconditioner with treshold %.3e\n", param->epsilon_2) ;
	lu = sol_comp_preconditioner(q, 2, param->epsilon_2*mat_sparse_matrix_max_elem(q)) ;
    }
    else
    {
      printf("Unknown preconditioner! STOP EXECUTION\n") ;
      exit (-1) ;
    }
  }
  ptime = mat_timeused() - ptime ;
#if SOLUTION_TEST1
  printf("Times for preconditioning %.3e sec\n", ptime) ;
  printf("Matrix structures for solution generated time used %.3e\n",
          mat_timeused() - starttime                                ) ;
#endif
  starttime = mat_timeused() ;
  endtime   = mat_timeused() ;
  while (comp_norm > param->epsilon && iter < param->no_of_iterations &&
         endtime - starttime < param->time_limit                  )
  {
    iter++ ;
    if ((iter % 10 == 0 || iter == param->no_of_iterations) && iter > 0)
      mat_double_vektor_normalise(v1) ;
    for (r = 0; r < q->ord; r++)
      v2->vektor[r] = v1->vektor[r] * q->diagonals[r] ;
    for (r = 0; r < q->ord; r++)
    for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
      v2->vektor[(q->rowind[r]).colind[c]] += 
        v1->vektor[r] * (q->rowind[r]).val[c] ;
    if (iter % RES_DIST == 0 || iter == param->no_of_iterations)
    {
      mat_compute_all_norms(v2, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
#if SOLUTION_TEST1
      printf("Residuals after %i iterations max %.5e, sum %.5e two %.5e\n",
             iter, n_max, n_sum, n_two) ;
#endif
      endtime = mat_timeused () ;
    } 
    if (comp_norm > param->epsilon)
    {
      sol_substitute_precond(lu, v2) ;
      mat_add_vektor_skalar(v1, v2, -1.0) ;
    }
  }
  mat_double_vektor_normalise(v1) ;
#if SOLUTION_TEST1
  if (iter % RES_DIST != 0)
    printf("Residuals after %i iterations max %.5e, sum %.5e\n",
           iter, n_max, n_sum) ;
#endif
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = endtime - starttime ;
  mat_double_vektor_free (v2) ;
  mat_sparse_matrix_free(lu) ;
  mat_double_vektor_normalise(v1) ;
  return(NO_ERROR) ;
}

/**********************
sol_gmres
Datum:  12.7.95
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        p Startvektor
        param Loesungsparameter
        pc Auswahl des Praekonitionierer (0=ohne, )
Output: p Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der iterativen GMRES Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und p einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit, der
Relaxationsparameter und die Fehlerschranke werden in param angegeben. Der Paramter pc
dient zur Auswahl des verwendeten Praekonditionierers. Die Anzahl der lokalen
Iterationschritte (= Dimension des Krylov Unterraums) ist in der Konstanten
SUBSPACE_DIM definiert.
**********************/
#ifndef CC_COMP
int  sol_gmres(Sparse_matrix *q, Double_vektor *p, Solution_param *param, int pc)
#else
int sol_gmres(q, p, param, pc) 
  Sparse_matrix   *q     ;
  Double_vektor   *p     ;
  Solution_param  *param ;
  int              pc    ;   
#endif 
{
  Sparse_matrix *lu ;
  Double_vektor *v[SUBSPACE_DIM] ;
  Double_vektor *w ;
  double         h[SUBSPACE_DIM+1][SUBSPACE_DIM] ;
  double         g[SUBSPACE_DIM+1] ;
  double         n_max, n_sum, n_two, comp_norm;
  double         starttime = mat_timeused(), utime = mat_user_time(), ptime ;  
  int            iter = 0, stop = FALSE, i;

  w = mat_double_vektor_new(q->ord) ;
  for (i = 0; i < SUBSPACE_DIM; i++)
    v[i] = mat_double_vektor_new(q->ord) ;
  if (pc == 0)
  {
    printf("No Preconditioning!\n") ;
    lu = NULL ;
  }
  else
  {
    ptime = mat_timeused() ;
    if (pc == 1)
    {
      printf("ILU0 Preconditioner\n") ;
      lu = sol_comp_preconditioner(q, 1, 0.0) ;
    }
    else
    {
      if (pc == 2)
      {
	  printf("ILUTH Preconditioner with treshold %.3e\n", param->epsilon_2) ;
	  lu = sol_comp_preconditioner(q, 2, param->epsilon_2*mat_sparse_matrix_max_elem(q)) ;
      }
      else
      {
        printf("Unknown preconditioner! STOP EXECUTION\n") ;
        exit (-1) ;
      }
    }
    ptime = mat_timeused() - ptime ;
#if SOLUTION_TEST1
    printf("Times for preconditioning %.3e sec\n", ptime) ;
#endif
  }
#if SOLUTION_TEST1
  printf("Matrix structures for solution generated time used %.3e  (%.3e)\n",
          mat_timeused() - starttime, mat_user_time() - utime              ) ;
#endif
  starttime = mat_timeused() ;
  utime     = mat_user_time() ;

  while (!(stop))        
  {
    mat_sparse_matrix_matrix_vektor_product(q, p, w) ;
    for (i = 0; i < q->ord; i++)
      w->vektor[i] = -w->vektor[i] ;
    mat_compute_all_norms(w, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
    printf("Iteration %i residuals max: %.5e sum: %.5e two: %.5e\n",
	   iter, n_max, n_sum, n_two) ;
    if (comp_norm < param->epsilon  || 
        mat_timeused() - starttime > param->time_limit ||
        iter >= param->no_of_iterations)
       stop = TRUE ; 
    if (!(stop) && pc > 0) {
      sol_substitute_precond(lu, w) ;
    }
    if (!(stop))
    {    
      if (pc > 0)
        sol_gmres_arnoldi_process(q, lu, w, v, h, g, FALSE);
      else
        sol_gmres_arnoldi_process(q, NULL, w, v, h, g, FALSE);
      sol_gmres_update(h, g, p, v) ;
      iter = iter +  SUBSPACE_DIM ;
    }
    // Remove normalization after evry restart
    // if (!(stop))
    //  mat_double_vektor_normalise(p) ;
  }
  // Final Normailzation
  mat_double_vektor_normalise(p) ;
  mat_sparse_matrix_matrix_vektor_product(q, p, w) ;
  mat_compute_all_norms(w, &n_max, &n_sum, &n_two) ;
  printf("Residuals after normalization max: %.5e sum: %.5e two: %.5e\n",
	   n_max, n_sum, n_two) ;
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = mat_timeused() - starttime ;
  printf
   ("Solution procedure stops after %.3e sec CPU time (%.3e sec User time)\n",
     mat_timeused() - starttime, mat_user_time() - utime) ;

  for (i = 0; i < SUBSPACE_DIM; i++)
    mat_double_vektor_free(v[i]) ; 
  mat_double_vektor_free(w) ; 
  if (pc > 0)
  {
    mat_sparse_matrix_free(lu) ;
  }  
  return(NO_ERROR) ;
} /* sol_gmres */

/**********************
sol_bicgstab
Datum:  31.08.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        p Startvektor
        param Loesungsparameter
        pc Auswahl des Praekonitionierer (0=ohne, )
Output: p Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der iterativen BiCGSTAB Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und p einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit, der
Relaxationsparameter und die Fehlerschranke werden in param angegeben. Der Paramter pc
dient zur Auswahl des verwendeten Praekonditionierers. 
**********************/
#ifndef CC_COMP
int  sol_bicgstab(Sparse_matrix *q, Double_vektor *p_ini, Solution_param *param, int pc)
#else
int sol_bicgstab(q, p_ini, param, pc) 
  Sparse_matrix   *q     ;
  Double_vektor   *p_ini  ;
  Solution_param  *param ;
  int              pc    ;   
#endif 
{
  Sparse_matrix *lu ;
  Double_vektor *r, *rt, *p, *v, *s, *t;
  double         n_max, n_sum, n_two, comp_norm, rho, rho_old, alpha, beta, omega;
  double         starttime = mat_timeused(), utime = mat_user_time(), ptime ;  
  int            iter = 0, stop = FALSE, reset = TRUE, i;

  if (pc == 0)
  {
    printf("No Preconditioning!\n") ;
    lu = NULL ;
  }
  else
  {
    ptime = mat_timeused() ;
    if (pc == 1)
    {
	printf("ILU0 Preconditioner\n") ;
	lu = sol_comp_preconditioner(q, 1, 0.0) ;
    }
    else
    {
      if (pc == 2)
      {
	  printf("ILUTH Preconditioner with treshold %.3e\n", param->epsilon_2) ;
	  lu = sol_comp_preconditioner(q, 2, param->epsilon_2*mat_sparse_matrix_max_elem(q)) ;
      }
      else
      {
        printf("Unknown preconditioner! STOP EXECUTION\n") ;
        exit (-1) ;
      }
    }
    ptime = mat_timeused() - ptime ;
    printf("Times for preconditioning %.3e sec\n", ptime) ;
  }
  r  = mat_double_vektor_new(q->ord) ;
  rt = mat_double_vektor_new(q->ord) ;
  p  = mat_double_vektor_new(q->ord) ;
  v  = mat_double_vektor_new(q->ord) ;
  s  = mat_double_vektor_new(q->ord) ;
  t  = mat_double_vektor_new(q->ord) ;
#if SOLUTION_TEST1
  printf("Matrix structures for solution generated time used %.3e  (%.3e)\n",
          mat_timeused() - starttime, mat_user_time() - utime              ) ;
#endif
  starttime = mat_timeused() ;
  utime     = mat_user_time() ;
  mat_sparse_matrix_matrix_vektor_product(q, p_ini, r) ;
  mat_compute_all_norms(r, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
  printf("Iteration %i residuals max: %.5e sum: %.5e two: %.5e\n",
	 iter, n_max, n_sum, n_two) ;
  if (comp_norm < param->epsilon  || 
      mat_timeused() - starttime > param->time_limit ||
      iter >= param->no_of_iterations)
     stop = TRUE ; 
  if (!(stop))
  {
      if (pc > 0) {
        sol_substitute_precond(lu, r) ;
      } 
    for (i = 0; i < q->ord; i++)
    {
      r->vektor[i]  = -r->vektor[i] ; 
      rt->vektor[i] =  r->vektor[i] ;
    }
  }
  rho   = 1.0 ;
  alpha = 0.0 ;
  beta  = 0.0 ;
  omega = 0.0 ;

  while (!(stop))        
  {
    rho_old = rho ;
    rho = mat_double_vektor_inner_product(rt, r) ;
    if (fabs(rho) < THRESHOLD)
    {
      printf("Value rho < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    if (!(stop))
    {
      if (!(reset))
      {
	beta = (rho / rho_old) * (alpha/omega) ;
        for (i = 0; i < q->ord; i++)
          p->vektor[i] = r->vektor[i] + beta*(p->vektor[i]  - omega*v->vektor[i]) ;
      }
      else
      {
        for (i = 0; i < q->ord; i++)
          p->vektor[i] = r->vektor[i] ;
        reset = FALSE ;
      }
      mat_sparse_matrix_matrix_vektor_product(q, p, v) ;
      if (pc > 0) {
        sol_substitute_precond(lu, v) ;
      }
      alpha = mat_double_vektor_inner_product(rt, v) ;
    }
    if (!(stop) && fabs(alpha) < THRESHOLD)
    {
      printf("Value alpha < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    if (!(stop))
    {
      alpha = rho / alpha ;
      for (i = 0; i < q->ord; i++)
        s->vektor[i] = r->vektor[i] - alpha * v->vektor[i] ;
      mat_compute_all_norms(s, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
      if (comp_norm < param->epsilon)
      {
        for (i = 0; i < q->ord; i++)
          p_ini->vektor[i] += alpha * p->vektor[i] ;
        printf("Value s < epsilon stop iteration in step %i\n", iter) ;
        stop = TRUE ;
      }
      else
      {
        mat_sparse_matrix_matrix_vektor_product(q, s, t) ;
        if (pc > 0) {
         sol_substitute_precond(lu, t) ;
	}
        omega = mat_double_vektor_inner_product(t, s) ;
        /* beta can be used here, since it is set before used in th enext iteration */
        beta = mat_double_vektor_inner_product(t, t) ;
      }
    }
    if (!(stop) && fabs(beta) < THRESHOLD)
    {
      printf("Value beta < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    if (!(stop))
    {
      omega /= beta ;
      for (i = 0; i < q->ord; i++)
      {
        p_ini->vektor[i] += alpha * p->vektor[i] + omega * s->vektor[i] ;
        r->vektor[i] = s->vektor[i] - omega * t->vektor[i] ;
      }
      mat_compute_all_norms(r, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
    }
    if (!(stop) && fabs(omega) < THRESHOLD)
    {
      printf("Value omega < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    /* iter wird um 2 erhoeht, da immer zwei Vektor Matrix Produkte gebildet werden */
    iter += 2 ;

    /* printf("iter %i rho %.5e alpha %.5e beta %.5e omega %.5e\n", iter, rho, alpha, beta, omega) ; */

    if (iter % RES_DIST == 0 || iter >= param->no_of_iterations ||
        mat_timeused() - starttime > param->time_limit || stop )
    {
      printf("Iteration %i residuals max: %.5e sum: %.5e\n",
              iter, n_max, n_sum) ;
    }
    if (!(stop) &&
        (comp_norm < param->epsilon  || 
        mat_timeused() - starttime > param->time_limit ||
        iter >= param->no_of_iterations))
       stop = TRUE ; 
    /* alle 100 Iterationen wird der Loesungsvektor normalisiert und das
       Verfahren neu aufgesetzt. 
    if (!(stop) && iter > 0 && iter % 100 == 0)
    {
      mat_double_vektor_normalise (p_ini) ;
      mat_sparse_matrix_matrix_vektor_product(q, p_ini, r) ;
      if (pc > 0)
        sol_substitute_precond(l, u, r) ; 
      for (i = 0; i < q->ord; i++)
      {
        r->vektor[i]  = -r->vektor[i] ; 
        rt->vektor[i] =  r->vektor[i] ;
      }
      rho   = 1.0 ;
      alpha = 0.0 ;
      beta  = 0.0 ;
      omega = 0.0 ;
      reset = TRUE ;
      } */
  }
  mat_double_vektor_normalise (p_ini) ;
  /* Berechnung der wahren Residualnormen */
  mat_sparse_matrix_matrix_vektor_product(q, p_ini, r) ;
  mat_compute_all_norms(r, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
  printf("Norms after normalization max: %.5e sum: %.5e two: %.5e (%i iterations)\n", 
	 n_max, n_sum, n_two, iter) ;
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = mat_timeused() - starttime ;
  printf
   ("Solution procedure stops after %.3e sec CPU time (%.3e sec User time)\n",
     mat_timeused() - starttime, mat_user_time() - utime) ;

  mat_double_vektor_free(r) ; 
  mat_double_vektor_free(rt) ; 
  mat_double_vektor_free(p) ; 
  mat_double_vektor_free(v) ; 
  mat_double_vektor_free(s) ; 
  mat_double_vektor_free(t) ; 
  if (pc > 0)
  {
    mat_sparse_matrix_free(lu) ;
  }  
  return(NO_ERROR) ;
} /* sol_bicgstab */


/**********************
sol_tfqmr
Datum:  22.01.99
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        p Startvektor
        param Loesungsparameter
        pc Auswahl des Praekonitionierer (0=ohne, )
Output: p Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der iterativen QMRTF Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und p einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit, der
Relaxationsparameter und die Fehlerschranke werden in param angegeben. Der Paramter pc
dient zur Auswahl des verwendeten Praekonditionierers. 
**********************/
#ifndef CC_COMP
int  sol_tfqmr(Sparse_matrix *q, Double_vektor *p_ini, Solution_param *param, int pc)
#else
int sol_tfqmr(q, p_ini, param, pc) 
  Sparse_matrix   *q     ;
  Double_vektor   *p_ini  ;
  Solution_param  *param ;
  int              pc    ;   
#endif 
{
  Sparse_matrix *lu ;
  Double_vektor *d, *r, *v, *u_1, *y_1, *y_2,  *y_3;
  double         n_max, n_sum, n_two, comp_norm, c, rho, alpha, beta, sigma, eta, nu, tau;
  double         starttime = mat_timeused(), utime = mat_user_time(), ptime;  
  int            iter = 0, stop = FALSE, i;

  if (pc == 0)
  {
    printf("No Preconditioning!\n") ;
    lu = NULL ;
  }
  else
  {
    ptime = mat_timeused() ;
    if (pc == 1)
    {
      printf("ILU0 Preconditioner\n") ;
      lu = sol_comp_preconditioner(q, 1, 0.0) ;
    }
    else
    {
      if (pc == 2)
      {
        printf("ILUTH Preconditioner with treshold %.3e\n", param->epsilon_2) ;
	lu = sol_comp_preconditioner(q, 2, param->epsilon_2*mat_sparse_matrix_max_elem(q)) ;
      }
      else
      {
        printf("Unknown preconditioner! STOP EXECUTION\n") ;
        exit (-1) ;
      }
    }
    ptime = mat_timeused() - ptime ;
    printf("Times for preconditioning %.3e sec\n", ptime) ;
  }
  r   = mat_double_vektor_new(q->ord) ;
  u_1 = mat_double_vektor_new(q->ord) ;
  v   = mat_double_vektor_new(q->ord) ;
  d   = mat_double_vektor_new(q->ord) ;
  y_1 = mat_double_vektor_new(q->ord) ;
  y_2 = mat_double_vektor_new(q->ord) ;
  y_3 = mat_double_vektor_new(q->ord) ;
#if SOLUTION_TEST1
  printf("Matrix structures for solution generated time used %.3e  (%.3e)\n",
          mat_timeused() - starttime, mat_user_time() - utime              ) ;
#endif
  starttime = mat_timeused() ;
  utime     = mat_user_time() ;
  mat_sparse_matrix_matrix_vektor_product(q, p_ini, r) ;
  mat_compute_all_norms(r, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
  printf("Iteration %i residuals max: %.5e sum: %.5e two: %.5e\n",
	 iter, n_max, n_sum, n_two) ;    
  eta = 0.0 ;
  nu    = 0.0 ;
  rho   = 0.0 ;
  alpha = 0.0 ;
  beta  = 0.0 ;
  sigma = 0.0 ;
  tau  = 0.0 ;
  if (comp_norm < param->epsilon  || 
      mat_timeused() - starttime > param->time_limit ||
      iter >= param->no_of_iterations)
     stop = TRUE ; 
  if (!(stop))
  {
      if (pc > 0) {
        sol_substitute_precond(lu, r) ;
      }
    for (i = 0; i < q->ord; i++)
      r->vektor[i]   = -r->vektor[i] ;
    mat_double_vektor_copy(r, u_1) ; 
    mat_double_vektor_copy(r, y_1) ; 
    mat_sparse_matrix_matrix_vektor_product(q, y_1, v) ;
    if (pc > 0) {
      sol_substitute_precond(lu, v) ;
    } 
    mat_double_vektor_copy(v, y_3) ; 
    tau = mat_double_vektor_tnorm(u_1) ;    
    rho   = mat_double_vektor_inner_product(r,u_1) ;
  }
  while (!(stop))        
  {
    sigma = mat_double_vektor_inner_product(r, v) ;
    if (fabs(sigma) < THRESHOLD)
    {
      printf("Value sigma < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    else
      alpha = rho / sigma ;
    if (fabs(tau) < THRESHOLD)
    {
      printf("Value tau < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }    
    if (fabs(alpha) < THRESHOLD)
    {
      printf("Value alpha < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    if (!(stop))
    {
      for (i = 0; i < y_1->no_of_entries; i++)
        y_2->vektor[i] = y_1->vektor[i] - alpha * v->vektor[i] ;
      mat_double_vektor_add_vektor(u_1, y_3, -alpha) ;
      c = nu * nu * eta / alpha ;
      for (i = 0; i < d->no_of_entries; i++)
        d->vektor[i] = y_1->vektor[i] + c * d->vektor[i] ;
      nu = mat_double_vektor_tnorm(u_1) / tau ;
      c = 1.0 / (sqrt(1.0 + nu * nu)) ;
      tau *= nu * c ;
      eta = c * c * alpha ;
      mat_double_vektor_add_vektor(p_ini, d, eta) ;
      if (tau < param->epsilon)
        stop = TRUE ;
    }
    if (!(stop))
    {
      mat_sparse_matrix_matrix_vektor_product(q, y_2, y_3) ;
      if (pc > 0) {
        sol_substitute_precond(lu, y_3) ;
      }
      mat_double_vektor_add_vektor(u_1, y_3, -alpha) ;
      c = nu * nu * eta / alpha ;
      for (i = 0; i < d->no_of_entries; i++)
        d->vektor[i] = y_2->vektor[i] + c * d->vektor[i] ;
      nu = mat_double_vektor_tnorm(u_1) / tau ;
      c = 1.0 / (sqrt(1.0 + nu * nu)) ;
      tau *= nu * c ;
      eta = c * c * alpha ;
      mat_double_vektor_add_vektor(p_ini, d, eta) ;
      if (tau < param->epsilon)
	  stop = TRUE ;
      /* mat_compute_norms(d, &n_max, &n_sum) ;
      if (n_max < param->epsilon)
      reset = TRUE ; */
    }
    if (!(stop))
    {
      beta = 1.0 / rho ;
      rho = mat_double_vektor_inner_product(r, u_1) ;
      beta *= rho ;
      for (i = 0; i < y_1->no_of_entries; i++)
        y_1->vektor[i] = u_1->vektor[i] + beta * y_2->vektor[i] ;
      for (i = 0; i < y_1->no_of_entries; i++) 
        v->vektor[i] = beta *(y_3->vektor[i] + beta * v->vektor[i]) ;
      mat_sparse_matrix_matrix_vektor_product(q, y_1, y_3) ;
      if (pc > 0) {
        sol_substitute_precond(lu, y_3) ;
      }
      for (i = 0; i < y_1->no_of_entries; i++) 
        v->vektor[i] += y_3->vektor[i] ;
    }

    /* iter wird um 2 erhoeht, da immer zwei Vektor Matrix Produkte gebildet werden */
    iter += 2 ;
    if (iter % RES_DIST == 0 || iter >= param->no_of_iterations ||
        mat_timeused() - starttime > param->time_limit || stop )
    {
      printf("Iteration %i residuals max: %.5e\n",
              iter, tau) ;
    }
    if (!(stop) &&
         (mat_timeused() - starttime > param->time_limit ||
        iter >= param->no_of_iterations))
       stop = TRUE ; 
    /* spaetestens alle 100 Iterationen wird der Loesungsvektor normalisiert und das
       Verfahren neu aufgesetzt. 
    if (stop || reset || iter % 100 == 0)
    {
      mat_double_vektor_normalise (p_ini) ;
      mat_sparse_matrix_matrix_vektor_product(q, p_ini, r) ;
      mat_compute_norms(r, &n_max, &n_sum) ;
      printf("Norms after normalization max: %.5e sum: %.5e (%i iterations)\n", 
              n_max, n_sum, iter) ;
      if (n_max < param->epsilon)
        stop = TRUE ;
      if (!(stop))
      {
        if (pc > 0)
          sol_substitute_precond(l, u, r) ; 
        for (i = 0; i < q->ord; i++)
          r->vektor[i]   = -r->vektor[i] ;
        mat_double_vektor_copy(r, u_1) ; 
        mat_double_vektor_copy(r, y_1) ; 
        mat_sparse_matrix_matrix_vektor_product(q, y_1, v) ;
        if (pc > 0)
          sol_substitute_precond(l, u, v) ; 
        mat_double_vektor_copy(v, y_3) ; 
        tau = mat_double_vektor_tnorm(u_1) ;
        eta = 0.0 ;
        nu  = 0.0 ;
        rho   = mat_double_vektor_inner_product(r,u_1) ;
        alpha = 0.0 ;
        beta  = 0.0 ;
        sigma = 0.0 ;      
        reset = FALSE ;
      }
      } */
  }
  mat_double_vektor_normalise (p_ini) ;
  /* Berechnung der wahren Residualnormen */
  mat_sparse_matrix_matrix_vektor_product(q, p_ini, r) ;
  mat_compute_all_norms(r, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
  printf("Norms after normalization max: %.5e sum: %.5e two: %.5e (%i iterations)\n", 
	 n_max, n_sum, n_two, iter) ;
  param->no_of_iterations = iter  ;
  param->epsilon          = comp_norm ;
  param->time_limit       = mat_timeused() - starttime ;
  printf
   ("Solution procedure stops after %.3e sec CPU time (%.3e sec User time)\n",
     mat_timeused() - starttime, mat_user_time() - utime) ;

  mat_double_vektor_free(r)   ; 
  mat_double_vektor_free(u_1) ; 
  mat_double_vektor_free(v)   ; 
  mat_double_vektor_free(d)   ; 
  mat_double_vektor_free(y_1) ; 
  mat_double_vektor_free(y_2) ;
  mat_double_vektor_free(y_3) ;  
  if (pc > 0)
  {
    mat_sparse_matrix_free(lu) ;
  }  
  return(NO_ERROR) ;
} /* sol_tfqmr */


/**********************
sol_general_bicgstab
Datum:  17.06.10
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        lu Pr�konditinerer falls NULL ohne
        p Startvektor
        b rechte Seite
        max_iter maximale Anzahl iterationen
        epsilon Fehlerschranke
Output: p Loesungsvektor

Side-Effects:
Description:
Die Funktion loest das Gleichungssystem Q*p = b gegebenes Gleichungssystem mit
Hilfe der iterativen BiCGSTAB Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und p einen Startvektor. 
**********************/
#ifndef CC_COMP
int  sol_general_bicgstab(Sparse_matrix *q, Sparse_matrix *lu, Double_vektor *p_ini, Double_vektor *b, int max_iter, double epsilon, int *global_it)
#else
  int sol_general_bicgstab(q, lu, p_ini, b, max_iter, epsilon, global_it) 
  Sparse_matrix   *q     ;
  Sparse_matrix   *lu    ;
  Double_vektor   *p_ini  ;
  Double_vektor   *b      ;
  int              max_iter ;
  double           epsilon ;
  int             *global_it;
#endif 
{
  Double_vektor *r, *rt, *p, *v, *s, *t;
  double         n_max, n_sum, n_two, comp_norm, rho, rho_old, alpha, beta, omega;
  int            iter = 0, stop = FALSE, reset = TRUE, i;

  r  = mat_double_vektor_new(q->ord) ;
  rt = mat_double_vektor_new(q->ord) ;
  p  = mat_double_vektor_new(q->ord) ;
  v  = mat_double_vektor_new(q->ord) ;
  s  = mat_double_vektor_new(q->ord) ;
  t  = mat_double_vektor_new(q->ord) ;
  mat_sparse_matrix_transposed_matrix_vektor_product(q, p_ini, r) ;
  mat_double_vektor_add_vektor(r, b, -1.0) ; 
  mat_compute_all_norms(r, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
  if (comp_norm < epsilon  || iter >= max_iter)
     stop = TRUE ; 
  if (!(stop))
  {
      if (lu != NULL) {
        sol_substitute_precond_transpose(lu, r) ;
      } 
    for (i = 0; i < q->ord; i++)
    {
      r->vektor[i]  = -r->vektor[i]  ;  
      rt->vektor[i] =  r->vektor[i] ;
    }
  }
  rho   = 1.0 ;
  alpha = 0.0 ;
  beta  = 0.0 ;
  omega = 0.0 ;

  while (!(stop))        
  {
    rho_old = rho ;
    rho = mat_double_vektor_inner_product(rt, r) ;
    if (fabs(rho) < THRESHOLD)
    {
      printf("Value rho < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    if (!(stop))
    {
      if (!(reset))
      {
	beta = (rho / rho_old) * (alpha/omega) ;
        for (i = 0; i < q->ord; i++)
          p->vektor[i] = r->vektor[i] + beta*(p->vektor[i]  - omega*v->vektor[i]) ;
      }
      else
      {
        for (i = 0; i < q->ord; i++)
          p->vektor[i] = r->vektor[i] ;
        reset = FALSE ;
      }
      mat_sparse_matrix_transposed_matrix_vektor_product(q, p, v) ;
      if (lu != NULL) {
        sol_substitute_precond_transpose(lu, v) ;
      }
      alpha = mat_double_vektor_inner_product(rt, v) ;
    }
    if (!(stop) && fabs(alpha) < THRESHOLD)
    {
      printf("Value alpha < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    if (!(stop))
    {
      alpha = rho / alpha ;
      for (i = 0; i < q->ord; i++)
        s->vektor[i] = r->vektor[i] - alpha * v->vektor[i] ;
      mat_compute_all_norms(s, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
      if (comp_norm < epsilon)
      {
        for (i = 0; i < q->ord; i++)
          p_ini->vektor[i] += alpha * p->vektor[i] ;
        printf("Value s < epsilon stop iteration in step %i\n", iter) ;
        stop = TRUE ;
      }
      else
      {
        mat_sparse_matrix_transposed_matrix_vektor_product(q, s, t) ;
        if (lu != NULL) {
         sol_substitute_precond_transpose(lu, t) ;
	}
        omega = mat_double_vektor_inner_product(t, s) ;
        /* beta can be used here, since it is set before used in the next iteration */
        beta = mat_double_vektor_inner_product(t, t) ;
      }
    }
    if (!(stop) && fabs(beta) < THRESHOLD)
    {
      printf("Value beta < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    if (!(stop))
    {
      omega /= beta ;
      for (i = 0; i < q->ord; i++)
      {
        p_ini->vektor[i] += alpha * p->vektor[i] + omega * s->vektor[i] ;
        r->vektor[i] = s->vektor[i] - omega * t->vektor[i] ;
      }
      mat_compute_all_norms(r, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
    }
    if (!(stop) && fabs(omega) < THRESHOLD)
    {
      printf("Value omega < THRESHOLD stop iteration in step %i\n", iter) ;
      stop = TRUE ;
    }
    /* iter wird um 2 erhoeht, da immer zwei Vektor Matrix Produkte gebildet werden */
    iter += 2 ;

    /* printf("iter %i rho %.5e alpha %.5e beta %.5e omega %.5e\n", iter, rho, alpha, beta, omega) ; */


    if (!(stop) &&
        (comp_norm < epsilon  ||iter >= max_iter))
       stop = TRUE ; 
  }
  mat_double_vektor_free(r) ; 
  mat_double_vektor_free(rt) ; 
  mat_double_vektor_free(p) ; 
  mat_double_vektor_free(v) ; 
  mat_double_vektor_free(s) ; 
  mat_double_vektor_free(t) ; 
  printf("sol_general_bicgstab iter %i accuracy %.3e\n", iter, comp_norm) ;
  *global_it += iter ;
  return(NO_ERROR) ;
} /* sol_general_bicgstab */

/**********************
sol_general_gmres
Datum:  23.06.10
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        lu Pr�konditinerer falls NULL ohne
        p Startvektor
        b rechte Seite
        max_iter maximale Anzahl iterationen
        epsilon Fehlerschranke
Output: p Loesungsvektor

Side-Effects:
Description:
Die Funktion loest ein Gleichungssystem Qx = b mit
Hilfe der iterativen GMRES Methode. Es wird engenommen, dass q die Koeffizientenmatrix beinhaltet
und p den Startvektor. Die maximale Iterationszahl,  die Fehlerschranke sowie der Praekonditionierer
werden als Parameter uebergeben. Die Anzahl der lokalen
Iterationschritte (= Dimension des Krylov Unterraums) ist in der Konstanten
SUBSPACE_DIM definiert.
**********************/
#ifndef CC_COMP
int  sol_general_gmres(Sparse_matrix *q, Sparse_matrix *lu, Double_vektor *p, Double_vektor *b, int max_iter, double epsilon, int *global_it)
#else
  int sol_general_gmres(q, lu, p, b, max_iter, epsilon, global_it) 
  Sparse_matrix   *q     ;
  Sparse_matrix   *lu    ;
  Double_vektor   *p     ;
  Double_vektor   *b     ;
  int              max_iter;
  double           epsilon;
  int             *global_it;
#endif 
{
  Double_vektor *v[SUBSPACE_DIM] ;
  Double_vektor *w ;
  double         h[SUBSPACE_DIM+1][SUBSPACE_DIM] ;
  double         g[SUBSPACE_DIM+1] ;
  double         n_max, n_sum, n_two, comp_norm ;
  int            iter = 0, stop = FALSE, i;

  w = mat_double_vektor_new(q->ord) ;
  for (i = 0; i < SUBSPACE_DIM; i++)
    v[i] = mat_double_vektor_new(q->ord) ;

  while (!(stop))        
  {
    mat_sparse_matrix_transposed_matrix_vektor_product(q, p, w) ;
    mat_double_vektor_add_vektor(w, b, -1.0) ; 
    mat_double_vektor_scalar_mult(w, -1.0) ;
    mat_compute_all_norms(w, &n_max, &n_sum, &n_two) ;
#if STOP_TWO_NORM
      comp_norm = n_two ;
#else
      comp_norm = n_max ;
#endif
    if (comp_norm < epsilon  || iter >= max_iter)
       stop = TRUE ; 
    if (!(stop) && lu != NULL) {
      sol_substitute_precond_transpose(lu, w) ;
    }
    if (!(stop))
    {    
      sol_gmres_arnoldi_process(q, lu, w, v, h, g, TRUE);
      sol_gmres_update(h, g, p, v) ;
      iter = iter +  SUBSPACE_DIM ;
    }
  }
  for (i = 0; i < SUBSPACE_DIM; i++)
    mat_double_vektor_free(v[i]) ; 
  mat_double_vektor_free(w) ; 
#if SOLUTION_TEST1
  printf("sol_general_gmres iter % i accuracy %.3e\n", iter, comp_norm) ;
#endif
  *global_it += iter ;
  return(NO_ERROR) ;
} /* sol_general_gmres */

/**********************
solve_sparse_system
Datum:  17.01.04
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  param Loesungsparameter
        q Koeffizientenmatrix
        partition Blockstruktur auf der Matrix
        p initialer Vektor
        ini falls != 0 wird Speicherplatz fuer den Vektor allokiert
            und der Startvektor als Gleichverteilung gewaehlt, ansonsten wird 
           der uebergebene Vektor als Startvektor verwendet
Output: p Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest das durch die Matrix q definierte Gleichungssystem mittels
einer der Methoden LU-Faktorisierung, Power Methode, JOR oder SOR. Der
Loesungsalgorithmus und zuegehoerige Parameter werden in param uebergeben. 
Vorsicht: Bei Verwendung von SOR wird die Matrix q transponiert!!!
**********************/
#ifndef CC_COMP
void solve_sparse_system (Solution_param *param, Sparse_matrix *q, 
                          Int_vektor *partition, Double_vektor **p, int ini) 
#else
void solve_sparse_system (param, q, partition, p, ini)
  Solution_param  *param     ;
  Sparse_matrix   *q         ;
  Int_vektor      *partition ;
  Double_vektor  **p         ;
  int              ini       ;
#endif
{
  int    i   ;
  double val ;
  double starttime, utime;

  utime     = mat_user_time() ;
  starttime = mat_timeused()  ;
  if (ini != 0)
  {
    val = 1.0 / (double) q->ord ;
    (*p) = mat_double_vektor_new(q->ord) ;
    for (i = 0; i < q->ord; i++)
      (*p)->vektor[i] = val ;
  }
  printf("Conventional solution approach states %i non-zeros %i\n",
          q->ord, mat_sparse_matrix_non_zeros(q)) ;
  switch (param->method)
  {
    case  STD_LU :
      printf("Direct solution with LU-Factorisation\n") ;
      if (sol_direct_solution(q, *p, TRUE, NULL) == NO_ERROR)
        printf
          ("Direct solution computed perform some Power iteration steps\n") ;
      else
        printf("Direct solution failed try Power Iteration\n") ;
      sol_power_iteration(q, *p, param, FALSE, NULL) ;      
      printf("Solution requires %.3e seconds CPU time\n", 
              mat_timeused() - starttime) ;
      break ;

    case STD_POWER :
      printf("Iterative solution Power method\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e\n", param->time_limit) ;
      sol_power_iteration(q, *p, param, TRUE, NULL) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;

    case STD_ILU0_POWER :
      printf("Iterative solution Power method with preconditioning\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e\n", param->time_limit) ;
      sol_pre_power_iteration(q, *p, param, 1) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;

    case STD_ILUTH_POWER :
      printf("Iterative solution Power method with preconditioning\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e\n", param->time_limit) ;
      sol_pre_power_iteration(q, *p, param, 2) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;

    case STD_JOR :
      printf("Iterative solution JOR method\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_jor_iteration(q, *p, param, TRUE, NULL) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;      

    case STD_SOR :
      printf("Iterative solution SOR method\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_sor_iteration(&q, *p, param, TRUE, TRUE, NULL) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ; 

    case STD_POWER_AD :
      printf("Iterative solution Power method with A/D steps\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e\n", param->time_limit) ;
      sol_power_ad_iteration(q, partition, *p, param) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ; 

    case STD_JOR_AD :
      printf("Iterative solution JOR method with A/D steps\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_jor_ad_iteration(q, partition, *p, param) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ; 

    case STD_SOR_AD :
      printf("Iterative solution SOR method with A/D steps\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_sor_ad_iteration(q, partition, *p, param) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ; 

    case STD_KMS :
      printf("Iterative solution with the KMS algorithm\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_kms(q, partition, *p, param) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;   

    case STD_ML :
      printf("Iterative solution with the multi level algorithm\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      printf("iterations per step %i states to aggregate per step %i \n",
              LOCAL_ITER, AGG_SIZE) ;
      sol_ml(q, partition, *p, param) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;    

    case STD_GMRES :
      printf("Iterative solution with GMRES (m=%i)\n", SUBSPACE_DIM) ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_gmres(q, *p, param, 0) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;   

    case STD_ILU0_GMRES :
      printf("Iterative solution with GMRES (m=%i)\n", SUBSPACE_DIM) ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_gmres(q, *p, param, 1) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;         

    case STD_ILUTH_GMRES :
      printf("Iterative solution with GMRES (m=%i)\n", SUBSPACE_DIM) ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_gmres(q, *p, param, 2) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ; 

    case STD_BICGSTAB :
      printf("Iterative solution with BiCGSTAB\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_bicgstab(q, *p, param, 0) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;   

    case STD_ILU0_BICGSTAB :
      printf("Iterative solution with BiCGSTAB\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_bicgstab(q, *p, param, 1) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;         

    case STD_ILUTH_BICGSTAB :
      printf("Iterative solution with BICGSTAB\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_bicgstab(q, *p, param, 2) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ; 

    case STD_TFQMR :
      printf("Iterative solution with TFQMR\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_tfqmr(q, *p, param, 0) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;   

    case STD_ILU0_TFQMR :
      printf("Iterative solution with TFQMR\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_tfqmr(q, *p, param, 1) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ;         

    case STD_ILUTH_TFQMR :
      printf("Iterative solution with TFQMR\n") ;
      printf("Maximal number of iterations %i required accuracy %.3e",
              param->no_of_iterations, param->epsilon             ) ;
      printf(" maximum CPU time %.3e, relaxation param %.3e\n", 
              param->time_limit, param->relaxation) ;
      sol_tfqmr(q, *p, param, 2) ;
      printf("Solution requires %.3e seconds CPU time for %i iterations\n", 
              param->time_limit, param->no_of_iterations) ;
      printf(" estimated accuracy %.3e\n", param->epsilon) ;
      break ; 

    default:
      printf("Sorry method not yet implemented\n") ;
  }
  printf("Real time used: %.3e seconds\n", mat_user_time() - utime) ;
}


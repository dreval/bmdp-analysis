/***********************************
 *
 * Constants for tge optimization of BMDPs
 * Date: 1.3.15
 * Last chance:
 *
 ***********************************/

#ifndef BMDP_CONST_H
#define BMDP_CONST_H

/* Traces */
/* Output of some information about th eprogress of iterative solutions */
#define BMDP_TRC_ITER     FALSE
/* Print some intermediate results like vectors and matrices,
   should be sue donly for small examples */
#define BMDP_TRACE_LEVEL  FALSE
/* Traces the process of interseting new strategies in the
   list of strategies */
#define BMDP_TRACE_INSERT FALSE
/* trace level for Pareto optimization
   0 no output, 1 only stationary rewards,
   2 stationary rewards + policies,
   3  stationary rewards + policies + gain vectors */
#define BMDP_PARETO_PRT   1
/* if TRUE ist is checked whether rows exist where it is posisble to choose
   an action such that the process remain with probability 1 in the state
   the corrpesonding state number are printed */
#define BMDP_TEST_FOR_ABSORBING FALSE


/* Constants for the solution process */
/* Threshold of teh ILU precodnitioner, currently not used */
#define BMDP_ILUTH_TH 0.1
/* Threshold for the size of systems of equations that are solved by teh direct solver */
#define BMDP_SIZE_DIRECT 1000
/* Maximal number of itertaions */
#define BMDP_MAX_ITER    500
/* Maximal time for one itertaive solution */
#define BMDP_MAX_TIME    1000.0
/* error bound for itertaive solutions */
#define BMDP_LO_EPS      1.0e-8
/* Differences of the gain below this value are interpreted as zero */
#define BMDP_MIN_VAL     1.0e-5
/* Difference which is necessary to Pareto dominate a another policy */
#define BMDP_COMP_POLICY 1.0e-5
/* Allowed difference between 1 and the row sum of the matrix required in
   the minmax or maxmax optimization */
#define BMDP_ROW_VAL     1.0e-12
/* upper bound for the number of policies that are evaluated in the Pareto optimization */
#define BMDP_MAX_CHECKED 50000
/* upper bound for time used */
#define CMDP_MAX_TIME 5000.0

/* Available solution methods */
/* BMDP methods */
#define BMDP_CHECK_MATRICES  800
#define BMDP_COMPUTE_MIN     801
#define BMDP_COMPUTE_AVG     802
#define BMDP_COMPUTE_MAX     803
#define BMDP_COMPUTE_REW     804
#define BMDP_COMPUTE_PURE    805
#define BMDP_COMPUTE_WEIGHT  806
#define BMDP_COMPUTE_HULL    807
#define BMDP_COMPUTE_FRONT   808
#define BMDP_COMPUTE_PUREBFS 809
#define BMDP_COMPUTE_PUREIPT 810
#define BMDP_COMPUTE_PUREEVO 815
/* CMDP methods */
#define CMDP_CHECK_MATRICES  900
#define CMDP_OPT_IMGP        901
#define CMDP_OPT_IMPP        902
#define CMDP_OPT_QCLP        903
#define CMDP_OPT_MIP         904
#define CMDP_OPT_NLP         905
#define CMDP_OPT_NLP2        906
#define CMDP_TEST_ALL        999
/* SMDP methods */
#define SMDP_OPT             700

/* USe these switches to assure two dimensional optimization */
/* reward vector for the minimum is set to zero */
#define BMDP_RESET_MIN FALSE
/* reward vector for the average is set to zero */
#define BMDP_RESET_AVG FALSE
/* reward vector for the maximum is set to zero */
#define BMDP_RESET_MAX FALSE

enum weight_dimensions { min_avg, min_max, avg_max, min_max_avg };

#endif /* BMDP_CONST_H */

/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c (C) Copyright 1996 by Peter Buchholz
c     All Rights Reserved
c
c simple_io
c
c Baisis IO Funktionen
c 
c
c       Datum           Modifikationen
c       ----------------------------------------------------------
c       12.08.97 Datei aus Teilen von str_helpf.c erzeugt
c       25.02.98 long int Ueberlauf wird in den Funktionen
c                str_set_ui, str_set_li und str_set_si geprueft
c       19.03.98 einfuegen von Kommentaren beim Schreiben der Parameterdatei
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/

#include <stdio.h>
#include "global.h"
#include "basic_io.h"
#include "mat_int_vektor.h"
#include "simple_io.h"


/**********************
read_param
Datum:
Autor:  Peter Buchholz
Input:  fp Datei
        param Paramterdatenstruktur
Output: param Paramterdatenstruktur mit neu belegten Feldern
Side-Effects:
Description:
Die Funktion liest aus einer zum lesne geoeffneten Datei einen
Loesungsparamtersatz. Die Datenstruktur fuer den Parametersatz muss
uebergeben werden. Interne dynamische Arrays werden innerhalb der Funktion
allokiert und mit Werten besetzt. falls ein Lesefehler auftritt wird eine
Fehlermeldung ausgegeben und das Programm abgebrochen. Die Eingabedatei wir
din der Funktion nicht geschlossen.
**********************/
#ifndef CC_COMP
int read_param (FILE *fp, Solution_param *param)
#else
int read_param (fp, param)
  FILE      *fp    ;
  Solution_param *param ;
#endif
 
{
  int i ;

  param->n_proc           = read_integer(fp) ; 
  param->method           = read_integer(fp) ; 
  param->no_of_iterations = read_integer(fp) ; 
  param->epsilon          = read_double(fp)  ;
  param->epsilon_2        = read_double(fp)  ;
  param->epsilon_3        = read_double(fp)  ;
  param->time_limit       = read_double(fp)  ;
  param->relaxation       = read_double(fp)  ;
  param->overall_result   = read_integer(fp) ;
  param->subnet_results = mat_int_vektor_new(param->n_proc+1) ;
  for (i = 0; i <= param->n_proc; i++)
  {
    param->subnet_results->vektor[i] = read_integer(fp) ; 
  }
  param->initials         = (int *) ecalloc (param->n_proc+1, sizeof(int)) ;
  for (i = 0; i <= param->n_proc; i++)
  { 
    param->initials[i] = read_integer(fp) ;
  }
  return(NO_ERROR) ;
}

/**********************
write_param
Datum:
Autor:  Peter Buchholz
Input:  fp Datei
        param Paramterdatenstruktur
Output: 
Side-Effects:
Description:
Die Funktion schreibt in eine geoeffnete Datei einen
Loesungsparamtersatz. Die Ausgabedatei wird
in der Funktion nicht geschlossen.
**********************/

#ifndef CC_COMP
int write_param (FILE *fp, Solution_param *param)
#else
int write_param (fp, param)
  FILE      *fp    ;
  Solution_param *param ;
#endif
 
{
  int i ;

  fprintf(fp, "# Anzahl Submodelle\n%i\n", param->n_proc) ; 
  fprintf(fp, "# Lsg. Methode 111 = Str_Power, 112 = Str_JOR, 113 = Str_BSOR ...\n%i\n",
           param->method) ;
  fprintf(fp, "# Iterationszahl\n%i\n", param->no_of_iterations) ;
  fprintf(fp, "# Epsilon_1 (Maximumsnorm Residuum)\n%.3e\n", param->epsilon) ;
  fprintf(fp, "# Epsilon_2 (Maximumsnorm lokales Residuum oder treshold bei ILU_TH)\n%.3e\n",
          param->epsilon_2) ;
  fprintf(fp, "# Epsilon_3 (Maximumsnorm fuer die adaptive Auswahl von Komponenten in a/d-Verfahren\n%.3e\n",
          param->epsilon_3) ;
  fprintf(fp, "#  CPU Zeit Limit\n%.3e\n", param->time_limit) ;
  fprintf(fp, "# Relaxation Parameter\n%.3e\n", param->relaxation) ;
  fprintf(fp, "# Ausgabe Loesungsvektor\n%i\n", param->overall_result) ;
  fprintf(fp, "# Ausgabe HQPN Lsg. Vektor\n%i\n", param->subnet_results->vektor[0]) ; 
  for (i = 1; i <= param->n_proc; i++)
    fprintf(fp, "# Ausgabe LQPN %i Lsg. Vektor\n%i\n", i, param->subnet_results->vektor[i]) ; 
  fprintf(fp, "# Initialzustand HQPN oder -2 bei Gleichverteilung oder -1 zum Einlesen\n%i\n",
          param->initials[0]) ; 
  for (i = 1; i <= param->n_proc; i++)
    fprintf(fp, "# Initialzustand LQPN %i\n%i\n", i, param->initials[i]) ; 
  return(NO_ERROR) ;
}

/**********************
str_set_ui
Datum:  26.11.96
Autor:  Peter Buchholz
Input:  partition Partitionsgrenzen der Subnetzmatrizen
        relation  Beschreibung der HLQPN Zustaende durch Subnetzzustaende
        ord Anzahl HLQPN Zustaende
        n_proc Anzahl Subnetze
Output: Funktionswert Wert von li fuer all HLQPN Zustaende und all Subnetze
Side-Effects:
Description:
Die Prozedur generiert die Wert von ui (= Produkt der Submodellzustandsraum-
groessen von Subnetzen mit groesserem Index) fuer alle HLQPN Zustaende und
alle Subnetze.
***********************************************************************/
#ifndef CC_COMP
int *str_set_ui 
      (Int_vektor **partition, int *relation, int ord, int n_proc) 
#else
int *str_set_ui (partition, relation, ord, n_proc) 
  Int_vektor **partition ;
  int         *relation  ;
  int          ord       ;
  int          n_proc    ;
#endif
{
  int  i, j, val, ind ;
  int *ui   ;
  double d_val ;

  ui = (int *) ecalloc (ord * n_proc, sizeof(int)) ;
  for (i = 0; i < ord; i++)
  {
    d_val = 1.0 ;
    val = 1 ;
    ind = i * n_proc - 1 ;
    for (j = n_proc; j > 0; j--)
    {
      ui[ind+j] = val ;
      val *= (partition[j])->vektor[relation[ind+j] + 1] -
              (partition[j])->vektor[relation[ind+j]]  ;
      d_val *= (double) ((partition[j])->vektor[relation[ind+j] + 1] -
                (partition[j])->vektor[relation[ind+j]])  ;
    }
    if (d_val > (double) val)
    {
      printf("State space size %.8e exceeds long int\nSTOP EXECUTION\n", d_val) ;
      exit(-1) ;
    }
  }
  return (ui) ;
}

/**********************
str_set_li
Datum:  27.1.97
Autor:  Peter Buchholz
Input:  partition Partitionsgrenzen der Subnetzmatrizen
        relation  Beschreibung der HLQPN Zustaende durch Subnetzzustaende
        ord Anzahl HLQPN Zustaende
        n_proc Anzahl Subnetze
Output: Funktionswert Wert von li fuer all HLQPN Zustaende und alle Subnetze
Side-Effects:
Description:
Die Prozedur generiert die Wert von li (= Produkt der Submodellzustandsraum-
groessen von Subnetzen mit kleinerem Index) fuer all HLQPN Zustaende und
all Subnetze.
***********************************************************************/
#ifndef CC_COMP
int *str_set_li 
      (Int_vektor **partition, int *relation, int ord, int n_proc) 
#else
int *str_set_li (partition, relation, ord, n_proc) 
  Int_vektor **partition ;
  int         *relation  ;
  int          ord       ;
  int          n_proc    ;
#endif
{
  int  i, j, val, ind ;
  int *li   ;
  double d_val ;

  li = (int *) ecalloc (ord * n_proc, sizeof(int)) ;
  for (i = 0; i < ord; i++)
  {
    val = 1 ;
    d_val = 1.0 ;
    ind = i * n_proc - 1 ;
    for (j = 1; j <= n_proc; j++)
    {
      li[ind+j] = val ;
      val *= (partition[j])->vektor[relation[ind+j] + 1] -
              (partition[j])->vektor[relation[ind+j]]  ;
      d_val *= (double) ((partition[j])->vektor[relation[ind+j] + 1] -
                         (partition[j])->vektor[relation[ind+j]])  ;
    }
    if (d_val > (double) val)
    {
      printf("State space size %.8e exceeds long int\nSTOP EXECUTION\n", d_val) ;
      exit(-1) ;
    }
  }
  return (li) ;
}

/**********************
str_set_ni
Datum:  10.2.97
Autor:  Peter Buchholz
Input:  partition Partitionsgrenzen der Subnetzmatrizen
        relation  Beschreibung der HLQPN Zustaende durch Subnetzzustaende
        ord Anzahl HLQPN Zustaende
        n_proc Anzahl Subnetze
Output: Funktionswert Zustandsraumgroesse fuer alle Subnetze
Side-Effects:
Description:
Die Prozedur generiert die Zustandsraumgroessen fuer alle HLQPN Zustaende und
alle Subnetze.
***********************************************************************/
#ifndef CC_COMP
int *str_set_ni 
      (Int_vektor **partition, int *relation, int ord, int n_proc) 
#else
int *str_set_ni (partition, relation, ord, n_proc) 
  Int_vektor **partition ;
  int         *relation  ;
  int          ord       ;
  int          n_proc    ;
#endif
{
  int  i, j, ind ;
  int *ni   ;

  ni = (int *) ecalloc (ord * n_proc, sizeof(int)) ;
  for (i = 0; i < ord; i++)
  {
    ind = i * n_proc - 1 ;
    for (j = 1; j <= n_proc; j++)
    {
      ni[ind+j] = (partition[j])->vektor[relation[ind+j] + 1] -
                  (partition[j])->vektor[relation[ind+j]]      ;
    }
  }
  return (ni) ;
}

/**********************
str_set_si
Datum:  28.1.97
Autor:  Peter Buchholz
Input:  partition Partitionsgrenzen der Subnetzmatrizen
        relation  Beschreibung der HLQPN Zustaende durch Subnetzzustaende
        ord Anzahl HLQPN Zustaende
        n_proc Anzahl Subnetze
Output: Funktionswert Zustandszahlen fuer alle HLQPN Zustaende
Side-Effects:
Description:
Die Prozedur berechnet die Zustandszahlen fuer all Makrozustaende
***********************************************************************/
#ifndef CC_COMP
int *str_set_si 
      (Int_vektor **partition, int *relation, int ord, int n_proc) 
#else
int *str_set_si (partition, relation, ord, n_proc) 
  Int_vektor **partition ;
  int         *relation  ;
  int          ord       ;
  int          n_proc    ;
#endif
{
  int  i, j, ind ;
  int *si        ;
  double d_val   ;

  si = (int *) ecalloc (ord, sizeof(int)) ;
  for (i = 0; i < ord; i++)
  {
    si[i] = 1 ;
    ind = i * n_proc - 1 ;
    d_val = 1.0 ;
    for (j = 1; j <= n_proc; j++)
    {
      si[i] *= (partition[j])->vektor[relation[ind+j] + 1] -
               (partition[j])->vektor[relation[ind+j]]  ;
      d_val *= (double) ((partition[j])->vektor[relation[ind+j] + 1] -
                         (partition[j])->vektor[relation[ind+j]]) ;
      if (d_val > (double) si[i])
      {
        printf("State space size %.8e exceeds long int\nSTOP EXECUTION\n", d_val) ;
        exit(-1) ;
      }
    }
  }
  return (si) ;
}



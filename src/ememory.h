/********************
	Name	:	Peter Kemper
	Datum	:	4.11.90
	Datei	:	ememory.h
	Version	:	1.0
	C-		:	

	Beschreibung :	Header Datei fuer ememory.h 

	Modifikationen :

	Datum					Beschreibung
	-------------------------------------------------------
	11.6.91		Modifikationsbeschreibungen entfernt			

			
********************/
#ifndef EMEMORY
#define EMEMORY
#ifndef MODUL
#define EXTERN extern
#else
#define EXTERN 
#endif

#ifndef TRUE
#define	TRUE	1
#endif

#ifndef	FALSE
#define	FALSE	0
#endif

/*************** Schalter fuer Modulverhalten ************************
EFREE :		TRUE	efree gibt Speicher frei
		FALSE	efree gibt keinen Speicher frei
EFREE_OUT :	TRUE	MELDUNG falls efree() nur dummy
		FALSE	keine Meldung
****************/
#define EFREE		TRUE	
#define EFREE_OUT	FALSE

/*************** Funktionen *****************************************/
#ifdef MODUL
extern	void	*emalloc () ;
extern	void	efree (void *cp) ;
extern	void	*erealloc () ;
extern	void	*ecalloc (unsigned  nelem, unsigned size) ;
#endif

EXTERN	void	*emalloc () ;
EXTERN	void	efree (void *cp) ;
EXTERN	void	*erealloc () ;
EXTERN	void	*ecalloc (unsigned  nelem, unsigned size) ;

#undef EXTERN
#endif


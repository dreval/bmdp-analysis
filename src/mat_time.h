/*********************
	Datei:	mat_time.h
	Datum: 	1.8.97
	Autor:	Peter Kemper
	Inhalt:	Funktionen zur Zeiterfassung


	Datum 	Aenderung
----------------------------------------------------------------------
1.8.97 Datei angelegt, Funktionen entstammen matrix.c/matrix.h

**********************/
#ifndef MAT_TIME_H
#define MAT_TIME_H

/*Include system constant for clock ticks per second */
#include <limits.h>

/******************* Defines	 **********************/
#ifndef CLK_TCK
#define CLK_TCK	CLOCKS_PER_SEC
#endif 
#ifndef time_to_seconds_factor
#define time_to_seconds_factor (10000.0 / CLK_TCK) 
#endif

/******************* Data structures	 **********************/

/**************** Charvektor ******************************/

/******************* Functions	 **********************/

/**********************
mat_user_time ##e bestimmt die Realzeit
Datum:
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
extern double mat_user_time() ;

/**********************
mat_timeused ##e bestimmt die CPU Zeit, die der Prozess verbraucht hat
Datum:
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
extern double mat_timeused () ;

#endif

/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c fuer die angegebenen Funktionen
c (C) Copyright 1995 by Peter Buchholz
c (C) Copyright 1993, 94 by Peter Kemper
c
c     All Rights Reserved
c
c mat_time.c
c
c Aufgabe ist Funktionen zur Zeiterfassung
c 
c
c	Datum		Modifikationen
c	----------------------------------------------------------
c 1.8.97 Datei angelegt, Funktionen aus matrix.c entnommen
c 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/
#include <sys/types.h>
#include <sys/times.h> 
#include <time.h>
#include "mat_time.h"

/******************************************************************************
               Zeiterfassung
*******************************************************************************

******************************************************************************/
/**********************
mat_user_time ##e bestimmt die Realzeit
Datum:
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
double mat_user_time()
#else
double mat_user_time()
#endif
{
  time_t t ;
 
  time(&t) ;
  return((double) t) ;
}

/**********************
mat_timeused ##e bestimmt die CPU Zeit, die der Prozess verbraucht hat
Datum:
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
double mat_timeused ()
#else
double mat_timeused ()
#endif
{
  struct tms t ;

  times (&t) ;
  return (((double) t.tms_utime + t.tms_stime) * time_to_seconds_factor) ; 
}

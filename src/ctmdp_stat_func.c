/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c (C) Copyright 2015 by Peter Buchholz
c
c
c     All Rights Reserved
c
c ctmdp_statt_func.c
c
c Funktionen fuer die transiente Analyse von CTMDPs
c
c
c       Datum           Modifikationen
c       ----------------------------------------------------------
c       01.03.15 Datei angelegt aus tst_ctmdp_stat.c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/

#include "ctmdp_stat_func.h"

#define USE_SVD FALSE /* Use SVD for direct solution in policy iteration */

/**********************
mdp_new_strategy
Datum:  17.06.11
Autor:  Peter Buchholz
Input:  mdp Mdp Matrix 
        h Vektor der Reward-Werte
        rew Reward Vektoren
        
Output: Funktionswert neue Ordnung TRUE, sonst FALSE
        ind Index der Elemente f�r die neue optimale Strategie
Side-Effects:
Description:
Die Funktion bestimmt eine Strategie in Abh�ngigkeit vom Wertevektor h.
***********************************************************************/
short mdp_new_strategy(Mdp_matrix *mdp, double *hh, int *ind)
{
  short news = FALSE ;
  int i, j, k, l ;
  double val, mval ;

  for (i = 0; i < mdp->ord; i++) {
    mval = -1.0e+12 ;
    l = ind[i] ;
    for (k = 0; k < mdp->rows[i]->pord; k++) {
      val = hh[i] * mdp->rows[i]->diagonals[k]  ;
      for (j = 0; j < mdp->rows[i]->no_of_entries[k]; j++)
	val += hh[mdp->rows[i]->colind[k][j]] * mdp->rows[i]->values[k][j]  ;
      if (val > mval) {
	mval = val ;
	l = k ;
      }
    }
    if (l != ind[i]) {
      news = TRUE ;
      ind[i] = l ;
    }
  } /* for i = 0, ... ord-1 */
  return(news) ;
} /* mdp_new_strategy */

/**********************
mdp_power_gs_eval_policy
Datum:  15.06.10
Autor:  Peter Buchholz
Input:  pp Policy Matrix
        rew Reward Vektor

Output: Funktionswert average reward der policy
        hv zustandsspezifische Rewards
Side-Effects:
Description:
Die Funktion wertet eine Politik iterativ mit GS aus.
Aus bo_power_gs_eval_policy kopiert!
***********************************************************************/
double mdp_power_gs_eval_policy(Sparse_matrix *pp, Double_vektor *b, double *hv, int max_iter, 
				double max_eps, double rho, int *global_it) 
{
  int i, j, iter;
  double res, eps, val, hval;
  Double_vektor *h1;

  /* Vektoren allokieren */
  h1 = mat_double_vektor_new(pp->ord) ;
  for (i = 0; i < pp->ord; i++)
    h1->vektor[i] = hv[i] ;
  res = rho ;
  for (iter = 0; iter < max_iter; iter++) {
    for (i = 0; i < pp->ord; i++) {
      hval = b->vektor[i] + h1->vektor[i] * pp->diagonals[i] ;
      for (j = 0; j < (pp->rowind[i]).no_of_entries; j++) {
	  hval += (pp->rowind[i]).val[j] * h1->vektor[(pp->rowind[i]).colind[j]] ;
      }
      h1->vektor[i] = hval ;
    }
    eps = 0.0 ;
    for (i = 0; i < pp->ord; i++)
      eps += h1->vektor[i] ;
    eps /= (double) pp->ord ;
    for (i = 0; i < pp->ord; i++) {
      b->vektor[i]  -= eps ;
      h1->vektor[i] -= eps ;
    }
    res += eps ;
    val = (fabs(res) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(res) ;
    if (fabs(eps) / val < max_eps)
      break ;
  }
  for (i = 0; i < pp->ord; i++)
    hv[i] = h1->vektor[i] ;
  /* Vektoren freigeben */
  mat_double_vektor_free(h1) ;
  *global_it += iter ;
  return(res) ;
} /* mdp_power_gs_eval_policy */

/**********************
mdp_power_eval_policy
Datum:  21.06.11
Autor:  Peter Buchholz
Input:  pp Policy Matrix
        rew Reward Vektor

Output: Funktionswert average reward der policy
        hv zustandsspezifische Rewards
Side-Effects:
Description:
Die Funktion wertet eine Politik iterativ mit der Power Methode aus aus.
Aus bo_power_eval_policy kopiert!
***********************************************************************/
double mdp_power_eval_policy(Sparse_matrix *pp, Double_vektor *b, double *hv, int max_iter, double max_eps, 
			     double rho, int *global_it) 
{
  int i, j, iter;
  double res, eps, val;
  Double_vektor *h1, *h2, *hh ;

  /* Vektoren allokieren */
  h1 = mat_double_vektor_new(pp->ord) ;
  h2 = mat_double_vektor_new(pp->ord) ;
  for (i = 0; i < pp->ord; i++)
    h1->vektor[i] = hv[i] ;
  res = rho ;
  for (iter = 0; iter < max_iter; iter++) {
    for (i = 0; i < pp->ord; i++)
      h2->vektor[i] = b->vektor[i] + h1->vektor[i] * pp->diagonals[i] ;
    for (i = 0; i < pp->ord; i++) {
      for (j = 0; j < (pp->rowind[i]).no_of_entries; j++) {
	  h2->vektor[i] += (pp->rowind[i]).val[j] * h1->vektor[(pp->rowind[i]).colind[j]] ;
      }
    }
    eps = 0.0 ;
    for (i = 0; i < pp->ord; i++)
      eps += h2->vektor[i] ;
    eps /= (double) pp->ord ;
    for (i = 0; i < pp->ord; i++) {
      b->vektor[i]  -= eps ;
      h2->vektor[i] -= eps ;
    }
    res += eps ;
    hh = h1 ;
    h1 = h2 ;
    h2 = hh ;
    val = (fabs(res) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(res) ;
    if (fabs(eps) / val < max_eps)
      break ;
  }
  for (i = 0; i < pp->ord; i++)
    hv[i] = h1->vektor[i] ;
  /* Vektoren freigeben */
  mat_double_vektor_free(h1) ;
  mat_double_vektor_free(h2) ;
  *global_it += iter ;
  return(res) ;
} /* mdp_power_eval_policy */

/**********************
mdp_mixed_iteration
Datum: 21.06.11
Autor:  Peter Buchholz
Input:  mdp MDP Matrix
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maximale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke für den Reward mit Hilfe einer Kombination
aus value und policy Iteration.
***********************************************************************/
double mdp_mixed_iteration(Mdp_matrix *mdp, Double_vektor *rew, int *ind, int max_iter,double ti_max, 
			  double epsilon, short max, int local_it, double local_eps, int method)
{
  short stop =FALSE, found=FALSE ;
  int iter, i, global_it ;
  int *old_ind ;
  double rho, old_rho=1.0e+5, val, starttime, *hh;
  Double_vektor *b ;
  Sparse_matrix *pp ;
  short dynamic=FALSE ;
  
  /* initialize rho */
  rho = 0.0 ;
  for (i = 0; i < rew->no_of_entries; i++)
    rho += rew->vektor[i] ;
  rho /= (double) rew->no_of_entries ;
  iter = global_it = 0 ;
  pp = mdp_gen_sparse_matrix_structure (mdp)  ;
  b = mat_double_vektor_new(mdp->ord) ;
  old_ind = (int *) ecalloc(mdp->ord, sizeof(int)) ;
  hh = (double *) ecalloc(mdp->ord, sizeof(double)) ;
  for ( i = 0; i < mdp->ord; i++)
    hh[i] = rew->vektor[i] - rho ;
  if (local_eps < 0.0) {
    local_eps = 10.0*epsilon ;
    dynamic = TRUE ;
  }
  starttime = mat_timeused() ;
  while (!(stop)) {
    iter++ ;
    /* compute and set a new policy */
    found = mdp_new_strategy(mdp, hh, ind) ;
    val = (fabs(rho) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(rho) ;
    if (iter > 1 && fabs(old_rho - rho)/val < epsilon) {
      found = FALSE ;
      stop = TRUE ;
    }
    else {
      if (dynamic)
	local_eps = 0.1*val ;
    }
    for (i = 0; i < pp->ord; i++) {
      if (ind[i] != old_ind[i] || iter == 1) {
	stop = FALSE ;
	found = TRUE ;
      }
      old_ind[i] = ind[i] ;
    }
    if (!(stop)) {
      old_rho = rho ;
      if (found == TRUE)  {
	mdp_set_strategy(mdp, pp, ind) ;
      }
      /* set the right hand side */
      for (i = 0; i < b->no_of_entries; i++) {
	b->vektor[i] = rew->vektor[i] - rho ;
      }
      if (method == BO_POWER) {
	rho = mdp_power_eval_policy(pp, b, hh, local_it, local_eps, rho, &global_it) ;
      }  
      else {      
	if (method == BO_GS_POWER) {
	  rho = mdp_power_gs_eval_policy(pp, b, hh, local_it, local_eps, rho, &global_it) ;
	}
	else {
	  printf("Unknown method\nSTOP EXECUTION\n") ;
	  exit(-1) ;
	}
      }
    }
    if (iter >= max_iter || mat_timeused()-starttime > ti_max) {
      stop = TRUE ;
    }
#if MDP_TRC_ITER
    if (!stop && iter % 1 == 0) {
      printf("Iteration %i rho %.3e difference %.3e (%.3e seconds)\n", 
	     iter, rho, fabs(rho - old_rho)/val, mat_timeused()-starttime) ;
    }
#endif
  } /* while */
  if (!found)
    printf("Mixed iteration stopped after %i (global %i) iterations with the optimal policy (%.3e seconds)\n", 
	   iter, global_it, mat_timeused()-starttime) ;
  else
    printf("Mixed iteration stopped after %i (global %i) iterations due to iteration/time limit (%.3e seconds)\n", 
	   iter, global_it, mat_timeused()-starttime) ;
  efree(old_ind) ;
  mat_sparse_matrix_free(pp) ;
  mat_double_vektor_free(b) ;
  return(rho) ;
} /* mdp_mixed_iteration */

/**********************
mdp_iterative_eval_policy
Datum:  13.04.10
Autor:  Peter Buchholz
Input:  pp Policy Matrix
        rew Reward Vektor

Output: Funktionswert average reward der policy
        hv zustandsspezifische Rewards
Side-Effects:
Description:
Die Funktion wertet eine Politik ierativ aus.
Z.Z. wird dei einfache Power Method everwendet, diese sollt enoch ersetzt werden!!
Aus bo_iterative_eval_policy kopiert!
***********************************************************************/
double mdp_iterative_eval_policy(Sparse_matrix *pp, Double_vektor *rew, double *hv, int max_iter, double max_eps) 
{
  int i, j, iter;
  double res, eps, n_sum, n_max ;
  Double_vektor *h1, *h2, *hh ;

  /* Vektoren allokieren */
  h1 = mat_double_vektor_new(pp->ord) ;
  h2 = mat_double_vektor_new(pp->ord) ;
  for (i = 1; i < pp->ord; i++)
    h1->vektor[i] = hv[i] ;
  h1->vektor[0] = 0.0 ;
  for (iter = 0; iter < max_iter; iter++) {
    for (i = 0; i < pp->ord; i++)
      h2->vektor[i] = rew->vektor[i] + h1->vektor[i] * pp->diagonals[i] ;
    for (i = 0; i < pp->ord; i++) {
      for (j = 0; j < (pp->rowind[i]).no_of_entries; j++) {
	  h2->vektor[i] += (pp->rowind[i]).val[j] * h1->vektor[(pp->rowind[i]).colind[j]] ;
      }
      if (i > 0)
	h2->vektor[i] -= h2->vektor[0] ;
    }
    h2->vektor[0] = 0.0 ;
    mat_double_vektor_norm_diff(h1, h2, &n_max, &n_sum) ;
    mat_compute_norms(h2, &n_max, &eps) ;
    if (eps < MDP_MIN_VAL)
      eps = MDP_MIN_VAL ;
    eps = n_sum / eps ;
    hh = h1 ;
    h1 = h2 ;
    h2 = hh ;
    if (eps < max_eps)
      break ;
  }
  for (i = 0; i < pp->ord; i++)
    hv[i] = h1->vektor[i] ;
  res = rew->vektor[0] ;
  for (i = 0; i < (pp->rowind[0]).no_of_entries; i++)
    res += (pp->rowind[0]).val[i] * hv[(pp->rowind[0]).colind[i]] ;
  /* Vektoren freigeben */
  efree(h1) ;
  efree(h2) ;
  return(res) ;
} /* mdp_iterative_eval_policy */

/**********************
mdp_direct_eval_policy
Datum:  12.04.10
Autor:  Peter Buchholz
Input:  pp Policy Matrix
        rew Reward Vektor

Output: Funktionswert average reward der policy
        hv zustandsspezifische Rewards
Side-Effects:
Description:
Die Funktion wertet eine Politik mit einem direkten Verfahren aus.
Aus der Funktion bo_direct_eval_policy kopiert!
***********************************************************************/
double mdp_direct_eval_policy(Sparse_matrix *pp, Double_vektor *rew, double *hv) 
{
  int i, j ;
  double res ;
  gsl_vector *b = gsl_vector_calloc (pp->ord+1);
  gsl_vector *h = gsl_vector_calloc (pp->ord+1);
  gsl_matrix *m = gsl_matrix_calloc (pp->ord+1, pp->ord+1);
#if USE_SVD
  gsl_matrix *v = gsl_matrix_calloc (pp->ord+1, pp->ord+1);
  gsl_vector *s = gsl_vector_calloc (pp->ord+1);
  gsl_vector *work = gsl_vector_calloc (pp->ord+1);
#else
  gsl_permutation *p = gsl_permutation_alloc (pp->ord+1);
#endif

  /* Vektorelemente initialisieren */
  for (i = 0; i < pp->ord; i++) {
    gsl_vector_set (b, i, rew->vektor[i]);
  }
  /* Matrixelemente initialisieren */
  for (i = 0; i < pp->ord; i++) {
    gsl_matrix_set (m, i, i, 1.0 - pp->diagonals[i]);
    gsl_matrix_set (m, i, pp->ord, 1.0) ;
    for (j = 0; j < (pp->rowind[i]).no_of_entries; j++)
      gsl_matrix_set (m, i, (pp->rowind[i]).colind[j], -(pp->rowind[i]).val[j]);
  }
  gsl_matrix_set(m, pp->ord, 0, 1.0) ;
  /* Matrix faktorisieren und Lösung berechnen */
#if USE_SVD
  gsl_linalg_SV_decomp(m, v, s, work) ;
  j = 0 ;
  for (i = pp->ord; i >= 0; i--) {
    res = gsl_vector_get(s, i) ;
    if (res < 1.0e-12)
      j++ ;
  }
  printf("%i of %i singular values < 1.0e-12 smallest %.5e\n",
	 j, pp->ord,  gsl_vector_get(s, pp->ord)) ;
  gsl_linalg_SV_solve(m, v, s, b, h) ;
#else
  gsl_linalg_LU_decomp (m, p, &i);
  gsl_linalg_LU_solve (m, p, b, h);
#endif
  for (i = 0; i < pp->ord; i++)
    hv[i] =  gsl_vector_get (h, i) ;
  res = gsl_vector_get (h, pp->ord) ;
  gsl_vector_free (h) ;
  gsl_vector_free (b) ;
  gsl_matrix_free (m);
#if USE_SVD
  gsl_matrix_free (v);
  gsl_vector_free (s) ;
  gsl_vector_free (work) ;
#else
  gsl_permutation_free(p) ;
#endif
  return(res) ;
} /* mdp_direct_eval_policy */

/**********************
bo_policy_iteration
Datum:  25.03.10
Autor:  Peter Buchholz
Input:  pl, pu Bounding matrices
        ds Vektor der Zeilensummen
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maixmale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke für den Reward mit Hilfe der policy-Iteration.
***********************************************************************/
double mdp_policy_iteration(Mdp_matrix *mdp, Double_vektor *rew, int *ind, int max_iter, 
			    double ti_max, double epsilon, short max, int local_it, double local_eps) 
{
  short stop =FALSE, found=FALSE ;
  int iter, i;
  double rho, old_rho, val, starttime, *hh;
  Sparse_matrix *pp ;
  
  rho = old_rho = 0.0 ;
  iter= 0 ;
  pp = mdp_gen_sparse_matrix_structure (mdp) ;
  hh = (double *) ecalloc(mdp->ord, sizeof(double)) ;
  for ( i = 0; i < mdp->ord; i++)
    hh[i] = rew->vektor[i] ;
  starttime = mat_timeused() ;
  while (!(stop)) {
    /* compute and set a new policy */
    if (iter > 0)
      found = mdp_new_strategy(mdp, hh, ind) ;
    val = (fabs(rho) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(rho) ;
    if (iter > 1 && ((fabs(old_rho - rho)/val < epsilon && !(found)) || (old_rho > rho))) {
      if (old_rho > rho)
	printf("Values in policy iteration are increasing old %.3e new %.3e\n", old_rho, rho) ;
      else
	stop = TRUE ;
    }
    if (!(stop)) {
      iter++ ;
      old_rho = rho ;
      mdp_set_strategy(mdp, pp, ind) ;
      /* Evaluatethe policy */
      if (pp->ord <= MDP_SIZE_DIRECT)
	rho = mdp_direct_eval_policy(pp, rew, hh) ;
      else {
	rho = mdp_iterative_eval_policy(pp, rew, hh, local_it, local_eps) ;
      }
      if (iter >= max_iter || mat_timeused()-starttime > ti_max) {
	stop = TRUE ;
      }
#if MDP_TRC_ITER
      if (!stop && iter % 1 == 0) {
	if (!(found)) {
	  printf("Iteration %i rho %.3e difference %.3e (%.3e seconds) (policy remains)\n", 
		 iter, rho, fabs(rho - old_rho)/val, mat_timeused()-starttime) ;
	}
	else {
	  printf("Iteration %i rho %.3e difference %.3e (%.3e seconds) (new policy)\n", 
		 iter, rho, fabs(rho - old_rho)/val, mat_timeused()-starttime) ;
	}
      }
#endif
    }
  } /* while */
  if (!(found))
    printf("Policy iteration stopped after %i iterations with the optimal policy (%.3e seconds)\n", iter, mat_timeused()-starttime) ;
  else
    printf("Policy iteration stopped after %i iterations due to iteration/time limit (%.3e seconds)\n", iter, mat_timeused()-starttime) ;
  mat_sparse_matrix_free(pp) ;
  efree(hh) ;
  return(rho) ;
} /* mdp_policy_iteration */

/**********************
mdp_value_iteration
Datum:  17.06.11
Autor:  Peter Buchholz
Input:  mdp MDP-Matrix
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maixmale Laufzeit
        epsilon Fehlerschranke
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke für den Reward mit Hilfe der value-Iteration.
***********************************************************************/
double mdp_value_iteration(Mdp_matrix *mdp, Double_vektor *rew, int *ind, int max_iter, double ti_max, double epsilon, short max) 
{
  short stop =FALSE, found=FALSE ;
  int iter, i;
  double eps, res, starttime, n_max, n_sum;
  Double_vektor *h1, *h2;
  Sparse_matrix *pp ;

  starttime = mat_timeused() ;
  eps = 1.0e+30 ;
  iter= 0 ;
  pp = mdp_gen_sparse_matrix_structure (mdp) ;
  h1 = mat_double_vektor_new(pp->ord) ;
  h2 = mat_double_vektor_new(pp->ord) ;
  /* determine the minimal reward to initialize the gain vector */
  n_max = 1.0e+12 ;
  for (i = 0; i <  rew->no_of_entries; i++) {
    if (n_max < rew->vektor[i]) 
      n_max = rew->vektor[i] ;
  }
  for (i = 0; i <  rew->no_of_entries; i++)
    h2->vektor[i] = n_max ;
  while (!(stop)) {
    mat_double_vektor_copy(h2, h1) ;
    if (iter > 0)
      found = mdp_new_strategy(mdp, h1->vektor, ind) ;
    iter++ ;
    if (iter == 1 || found)
      mdp_set_strategy(mdp, pp, ind) ;
    /* Compute h2 = r + pp * h1 */
    mat_double_vektor_copy(rew, h2) ;
    mat_sparse_transposed_matrix_vektor_mult (pp, h1, h2, TRUE) ;
    mat_double_vektor_scalar_add(h2, -h2->vektor[0]) ;
    mat_double_vektor_norm_diff(h1, h2, &n_max, &n_sum) ;
    mat_compute_norms(h2, &n_max, &eps) ;
    if (eps < MDP_MIN_VAL)
      eps = MDP_MIN_VAL ;
    eps = n_sum / eps ;
    if (!found && (eps <= epsilon || iter >= max_iter || mat_timeused()-starttime > ti_max)) {
      stop = TRUE ;
    }
#if MDP_TRC_ITER
    if (!stop && iter % 10 == 0) {
      res = rew->vektor[0] ;
      res += pp->diagonals[0] * h2->vektor[0] ; 
      for (i = 0; i < (pp->rowind[0]).no_of_entries; i++)
	res += (pp->rowind[0]).val[i] * h2->vektor[(pp->rowind[0]).colind[i]] ;
      printf("Iteration %i relative difference %.3e (%.3e seconds, res %.5e)\n", 
	     iter, eps, mat_timeused()-starttime, res) ;
    }
#endif
  } /* while !stop */
  printf("Value iteration stopped after %i iterations with relative difference %.3e (%.3e seconds)\n", 
	 iter, eps, mat_timeused()-starttime) ;
  /* Now we have to compute the result */
  res = rew->vektor[0] ;
  res += pp->diagonals[0] * h2->vektor[0] ;
  for (i = 0; i < (pp->rowind[0]).no_of_entries; i++)
    res += (pp->rowind[0]).val[i] * h2->vektor[(pp->rowind[0]).colind[i]] ;
  mat_double_vektor_free(h1) ;
  mat_double_vektor_free(h2) ;
  mat_sparse_matrix_free(pp) ;
  return(res) ;
} /* mdp_value_iteration */

/**********************
mdp_pre_power_eval_policy
Datum:  21.06.11
Autor:  Peter Buchholz
Input:  pp Policy Matrix
        rew Reward Vektor

Output: Funktionswert average reward der policy
        hv zustandsspezifische Rewards
Side-Effects:
Description:
Die Funktion wertet eine Politik iterativ mit Power + 
Präkonditionierung aus.
As bo_pre_power_eval_policy kopiert.
***********************************************************************/
double mdp_pre_power_eval_policy(Sparse_matrix *pp, Sparse_matrix *lu, Double_vektor *b, double *hv, 
				 int max_iter, double max_eps, int *global_it) 
{
  int i, j, iter;
  double res, res_old, val, omega;
  Double_vektor *h1, *h2;

  /* Vektoren allokieren */
  h1 = mat_double_vektor_new(pp->ord) ;
  h2 = mat_double_vektor_new(pp->ord) ;
  for (i = 0; i < pp->ord; i++)
    h1->vektor[i] = hv[i] ;
  res = hv[pp->ord-1] ;
  res_old = 0.0 ;
  for (iter = 0; iter < max_iter; iter++) {
    for (i = 0; i < pp->ord; i++)
      h2->vektor[i] = h1->vektor[i] * pp->diagonals[i] - b->vektor[i] ;
    for (i = 0; i < pp->ord; i++) {
      for (j = 0; j < (pp->rowind[i]).no_of_entries; j++) {
	  h2->vektor[i] += (pp->rowind[i]).val[j] * h1->vektor[(pp->rowind[i]).colind[j]] ;
      }
    }
    sol_substitute_precond_transpose(lu, h2) ;
    omega = mat_double_vektor_tnorm(h2) ;
    res = omega ;
    if (omega > 1.0)
      omega = 1.0 / omega ;
    else
      omega = 1.0 ;
    for (i = 0; i < pp->ord; i++) {
      h1->vektor[i] -= omega * h2->vektor[i] ; 
    }
    val = (fabs(res) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(res) ;
    if (fabs(res_old-res) / val < max_eps)
      break ;
    res_old = res ;
  }
  for (i = 0; i < pp->ord; i++)
    hv[i] = h1->vektor[i] ;
  /* Vektoren freigeben */
  mat_double_vektor_free(h1) ;
  mat_double_vektor_free(h2) ;
  *global_it += iter ;
  return(hv[pp->ord-1]) ;
} /* mdp_pre_power_eval_policy */

/**********************
mdp_precond_iteration
Datum:  15.06.10
Autor:  Peter Buchholz
Input:  mdp MDp matrix
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maximale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke für den Reward mit Hilfe einer iterativen Technik
und ILU Präkonditionierung.
***********************************************************************/
double mdp_precond_iteration(Mdp_matrix *mdp, Double_vektor *rew, int *ind, int max_iter, double ti_max, 
			    double epsilon, short max, int local_it, double local_eps, int method) 
{
  short stop =FALSE, found=FALSE ;
  int iter, i, global_it;
  int *old_ind ;
  double rho, old_rho, val, threshold, starttime, *hh;
  Double_vektor *b, hv ;
  Sparse_matrix *pp, *lu=NULL ;
  short dynamic = FALSE ;

  iter = global_it = 0 ;
  if (local_eps < 0.0) {
    dynamic = TRUE ;
    local_eps = 10.0 * epsilon ;
  }
  pp = mdp_gen_precond_structure (mdp) ;
  b = mat_double_vektor_new(mdp->ord+1) ;
  old_ind = (int *) ecalloc(mdp->ord, sizeof(int)) ;
  hh = (double *) ecalloc(mdp->ord+1, sizeof(double)) ;
  for ( i = 0; i < mdp->ord; i++) {
    b->vektor[i] = rew->vektor[i] ;
    hh[i] = rew->vektor[i] ;
    hh[mdp->ord] += rew->vektor[i] ;
  }
  b->vektor[mdp->ord] = 0.0 ;
  hh[mdp->ord] /= (double) rew->no_of_entries ;
  rho = old_rho = hh[mdp->ord] ;
  if (method == BO_BICGSTAB || method == BO_BICGSTAB_ILU0 || method == BO_BICGSTAB_ILUTH ||
      method == BO_GMRES || method == BO_GMRES_ILU0 || method == BO_GMRES_ILUTH) {
    hv.no_of_entries = pp->ord ;
    hv.vektor = hh ;
  }
  starttime = mat_timeused() ;
  while (!(stop)) {
    /* compute and set a new policy */
    if (iter > 0)
      found = mdp_new_strategy(mdp, hh, ind) ;
    iter++ ;
    val = (fabs(rho) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(rho) ;
    if (iter > 1) {
      val = fabs(old_rho - rho)/val ;
      if (val < epsilon) {
	found = FALSE ;
	stop = TRUE ; 
      }
      else {
	if (dynamic)
	  local_eps = 0.1*val ;
      }
    }
    else
      found = TRUE ;
    for (i = 0; i < mdp->ord; i++) {
      if (ind[i] != old_ind[i]) {
	stop = FALSE ;
	found = TRUE ;
      }
      old_ind[i] = ind[i] ;
    }
    if (!(stop)) {
      old_rho = rho ;
      if (found == TRUE)  {
	mdp_set_precond_strategy(mdp, pp, ind) ;
	if (method == BO_GS_ILU0 || method == BO_BICGSTAB_ILU0 || method == BO_GMRES_ILU0) {
	  if (lu != NULL)
	    mat_sparse_matrix_free(lu) ;
	  lu = sol_comp_preconditioner (pp, 1, 0.0) ; 
	  mat_sparse_matrix_eliminate_small_values(lu, 1.0e-12) ;
	}
	if (method == BO_GS_ILUTH || method == BO_BICGSTAB_ILUTH || method == BO_GMRES_ILUTH) {
	  if (lu != NULL)
	    mat_sparse_matrix_free(lu) ;
	  threshold = mat_sparse_matrix_max_elem(pp) * MDP_ILUTH_TH ;
	  lu = sol_comp_preconditioner (pp, 2, threshold) ;
	  mat_sparse_matrix_eliminate_small_values(lu, 1.0e-12) ;
	}
      }
      if (method == BO_GS_ILU0 || method == BO_GS_ILUTH) {
	rho = mdp_pre_power_eval_policy(pp, lu, b, hh, local_it, local_eps, &global_it) ;
      }
      else {
	  if (method == BO_BICGSTAB || method == BO_BICGSTAB_ILU0 || method == BO_BICGSTAB_ILUTH) {
	    sol_general_bicgstab(pp, lu, &hv, b, local_it, local_eps, &global_it) ;
	    rho = hh[pp->ord-1] ;
	  }
	  else {
	    if (method == BO_GMRES || method == BO_GMRES_ILU0 || method == BO_GMRES_ILUTH) {
	      sol_general_gmres(pp, lu, &hv, b, local_it, local_eps, &global_it) ;
	      rho = hh[pp->ord-1] ; 
	    }
	    else {
	      printf("Unknown method\nSTOP EXECUTION\n") ;
	      exit(-1) ;
	    }
	  }
      }
    }
    if (iter >= max_iter || mat_timeused()-starttime > ti_max) {
      stop = TRUE ;
    }
#if BO_TRC_ITER
    if (!stop && iter % 1 == 0) {
      printf("Iteration %i rho %.3e difference %.3e (%.3e seconds)\n", 
	     iter, rho, fabs(rho - old_rho)/val, mat_timeused()-starttime) ;
    }
#endif
  } /* while */
  if (!found)
    printf("Mixed iteration stopped after %i (global %i) iterations with the optimal policy (%.3e seconds)\n", 
	   iter, global_it, mat_timeused()-starttime) ;
  else
    printf("Mixed iteration stopped after %i (global %i) iterations due to iteration/time limit (%.3e seconds)\n", 
	   iter, global_it, mat_timeused()-starttime) ;
  efree(old_ind) ;
  efree(hh) ;
  mat_sparse_matrix_free(pp) ;
  mat_double_vektor_free(b) ;
  if (lu != NULL)
    mat_sparse_matrix_free(lu) ;
  return(rho) ;
} /* mdp_precond_iteration */

/*********************
	Datei:	mat_int_vektor.h
	Datum: 	1.8.97
	Autor:	Peter Kemper
	Inhalt:	Funktionen zur Behandlung von Vektoren


	Datum 	Aenderung
----------------------------------------------------------------------
1.8.97 Datei angelegt, Funktionen entstammen matrix.c/matrix.h

**********************/
#ifndef MAT_INT_VEKTOR_H
#define MAT_INT_VEKTOR_H

#include <stdio.h>
#include "basic_io.h" 

/******************* Defines	 **********************/


/******************* Data structures	 **********************/

/**************** Intvektor ******************************/
typedef struct int_vektor
{
  unsigned no_of_entries ; /* gibt Laenge des Vektors an */
  int *vektor ;            /* Positionen 0,1, ... no_of_entries -1 */
} Int_vektor ; 

/******************* Functions	 **********************/
/**********************
mat_int_vektor_new ##e Konstruktor
Datum:  14.2.95
Autor:  Peter Kemper
Input:  unsigned anzahl, falls 0 < anzahl Laenge des Vektors
Output: neues Element
Side-Effects:
Description:
Es wird ein neues Element vom Typ Int_vektor auf eigenem
Speicherplatz erzeugt, 
falls eine Vektorlaenge angegeben ist (anzahl > 0) wird 
zusaetzlich ein Vektor entsprechender Laenge fuer int Eintraege
allokiert
anderenfalls liegt kein Vektor vor.
**********************/
extern Int_vektor *mat_int_vektor_new(unsigned anzahl);

/**********************
mat_int_vektor_free ##e Destruktor
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
Der Speicherplatz fuer v wird freigegeben, dies gilt ebenso fuer 
die Komponente vektor.
**********************/
extern int mat_int_vektor_free(Int_vektor *v);

/**********************
mat_int_vektor_print_file ##e Ausgabefunktion (mit Kommentar, je 10 Eintraege 1 Zeile)
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege des Vektors wird in das angegebene File ausgegeben.
Annahme: fp zeigt auf eine Datei, die bereits zum Schreiben geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
Ausgabeformat: Kommentar zur Anzahl Eintraege
dann jeweils 10 Eintraege pro Zeile
**********************/
extern int mat_int_vektor_print_file(FILE *fp, Int_vektor *v) ;

/**********************
mat_int_vektor_output ##e Ausgabefunktion (ohne Kommentar, je Eintrag 1 Zeile)
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege des Vektors wird in das angegebene File ausgegeben.
Annahme: fp zeigt auf eine Datei, die bereits zum Schreiben geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
Ausgabeformat: Anzahl Eintraege \n Wert1 \n Wert2 ....
**********************/
extern int mat_int_vektor_output(FILE *fp, Int_vektor *v);

/**********************
mat_int_vektor_input ##e Einlesefunktion
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege eines Vektors werden aus dem angegebenen File 
eingelesen und in v gespeichert.
Es werden genau so viele Eintraege eingelesen, wie in v->no_of_entries
angegeben ist. 
Annahme: fp zeigt auf eine Datei, die bereits zum Lesen geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
**********************/
extern int mat_int_vektor_input(FILE *fp, Int_vektor *v);

/**
 * Einlesen eines Int_vektors aus fp ohne Kenntnis der Laenge.
 * \date 4.5.95
 * \param fp Zeiger auf File
 * \return Der gefuellte Vektor 
 * \author Peter Buchholz
 */
extern Int_vektor *mat_int_vektor_input_1(FILE *fp) ;

/**********************
mat_int_vektor_realloc ##e verlaengert Vektor auf Vektorlaenge + Anzahl
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
extern Int_vektor *mat_int_vektor_realloc(Int_vektor *v, unsigned anzahl) ;

/**
 * Setzt Wert an bestimmte Position im Vektor  
 * \date 18.11.1999  
 * \param v Zeiger auf Vektor, in dem der Wert an eine bestimmte Position gesetzt werden soll
 * \param pos Wert soll an diese Position gesetzt werden
 * \param val Dieser Wert soll gesetzt werden.
 * \return TRUE (immer)
 * \author Carsten Tepper  
 */
extern int mat_int_vektor_set_pos(Int_vektor *v, int pos, int val);
    
/**
 * Liefert den Wert einer bestimmten Position im Vektor.
 * \date 25.11.1999  
 * \param pos Position des Wertes, der geliefert werden soll
 * \param v Zeiger auf Vektor, aus dem ein Wert einer bestimmten Position geholt werden soll
 * \return Den Wert an der angegebenen Position
 * \author Carsten Tepper  
 */
extern int mat_int_vektor_get_pos(Int_vektor *v, int pos);

/**
 * Setzt Wert fuer Teilvektor [start,stop[
 * \date 30.11.1999  
 * \author Carsten Tepper
 * \param v Zeiger auf Vektor, in dem der Teilvektor mit dem angegebenen Wert gesetzt werden soll
 * \param start Startposition von der an der Wert gesetzt wird 
 * \param stop Stopposition bis wohin der Wert gesetzt wird (Position stop wird nicht gesetzt)
 * \param val Wert der in den Teilvektor geschrieben werden soll 
 * \return TRUE (immer)
 */
extern int mat_int_vektor_set_interval(Int_vektor *v, int start, int stop, short val);

/**
 * Addiert zwei Vektoren (a = a + b).
 * Das Resultat der Addition steht im Vektor a.
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param a Zeiger auf Vektor a
 * \param b Zeiger auf Vektor b
 */
extern void mat_int_vektor_plus(Int_vektor *a, Int_vektor *b);

/**
 * Subtrahiert zwei Vektoren (a = a - b).
 * Das Resultat der Subtraktion steht im Vektor a.
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param a Zeiger auf Vektor a
 * \param b Zeiger auf Vektor b
 */
extern void mat_int_vektor_minus(Int_vektor *a, Int_vektor *b);

/**
 * Modulo zweier Vektoren (a = a % b).
 * Das Ergebnis steht im Vektor a. 
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param a Zeiger auf Vektor a
 * \param b Zeiger auf Vektor b
 */
extern void mat_int_vektor_modulo(Int_vektor *a, Int_vektor *b);

/**
 * Division zweier Vektoren (a = a / b).
 * Das Ergebnis steht im Vektor a.
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param a Zeiger auf Vektor a
 * \param b Zeiger auf Vektor b
 */
extern void mat_int_vektor_div(Int_vektor *a, Int_vektor *b);

/**
 * Multiplikation zweier Vektoren (a = a * b).
 * Das Ergebnis steht im Vektor a.
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param Zeiger auf Vektor a
 * \param Zeiger auf Vektor b
 */
extern void mat_int_vektor_mul(Int_vektor *a, Int_vektor *b);


#endif

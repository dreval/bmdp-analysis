/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c (C) Copyright 2015 by Peter Buchholz
c
c
c     All Rights Reserved
c
c bmdp_pareto_func.c
c
c Functions to compute Pareto optimal solution for BMDPs
c
c
c       Datum           Modifikationen
c       ----------------------------------------------------------
c       13.03.15 Implementtaion started
c   bis 16.03.15 Fertigstellung und Fehlerbeseitigung
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/
#include <stddef.h>
#include "bmdp_pareto_func.h"
#include "bloom.h"
#include <omp.h>
#include <time.h>
#include <sys/time.h>
#include <stddef.h>
#include <stdlib.h>

#define is_smaller(x, y, th) (y - x > th ? 1 : (x - y > th ? 2 : 0))
#define is_larger(x, y, th) (x - y > th ? 1 : (y - x > th ? 2 : 0))

void check_set(Pareto_vector_set *my_set) {
    int i = 1;
    List **le;

    printf("Entries in list %i\n", my_set->no_entries);
    le = my_set->list;
    while (!list_empty(le)) {
        printf("Element %i in list\n", i++);
        le = list_next(le);
    }
}

/**********************
bmpar_pareto_vector_element_new
Datum:  13.03.15
Autor:  Peter Buchholz
Input: ord size of the vectors
Output: return values pointer to the new element
Side-Effects:
Description:
The function generates a new Pareto element.
Vectors are initialized with NULL!!
**********************/
Pareto_vector_element *bmpar_pareto_vector_element_new(size_t ord) {
    Pareto_vector_element *pareto;

    pareto = (Pareto_vector_element *) ecalloc(1, sizeof(Pareto_vector_element));
    pareto->seen1 = FALSE;
    pareto->seen2 = FALSE;
    pareto->ord = ord;
    pareto->policy = NULL;
    pareto->gain_min = NULL;
    pareto->gain_avg = NULL;
    pareto->gain_max = NULL;
    return (pareto);
} /* bmpar_pareto_vector_element_new */

/**********************
bmpar_pareto_vector_element_free
Datum:  13.03.15
Autor:  Peter Buchholz
Input: pareto element
Output: 
Side-Effects:
Description:
The function deallocates the space for a Pareto element.
**********************/
int bmpar_pareto_vector_element_free(Pareto_vector_element *pareto) {
    if (pareto->policy != NULL)
        efree(pareto->policy);
    if (pareto->gain_min != NULL)
        efree(pareto->gain_min);
    if (pareto->gain_avg != NULL)
        efree(pareto->gain_avg);
    if (pareto->gain_max != NULL)
        efree(pareto->gain_max);
    efree(pareto);
    return (NO_ERROR);
} /* bmpar_pareto_vector_element_free */

/**********************
bmpar_pareto_vector_set_new
Datum:  13.03.15
Autor:  Peter Buchholz
Input: 
Output: return values pointer to the new empty set
Side-Effects:
Description:
The function generates an empty Pareto set.
**********************/
Pareto_vector_set *bmpar_pareto_vector_set_new() {
    Pareto_vector_set *my_set;

    my_set = (Pareto_vector_set *) ecalloc(1, sizeof(Pareto_vector_set));
    my_set->no_entries = 0;
    my_set->list = list_new();
    return (my_set);
} /* bmpar_pareto_vector_set_new */

/**********************
bmpar_pareto_vector_set_free
Datum:  13.03.15
Autor:  Peter Buchholz
Input: 
Output: return values pointer to the new empty set
Side-Effects:
Description:
The function deallocates the memory for a Pareto set.
**********************/
int bmpar_pareto_vector_set_free(Pareto_vector_set *my_set) {
    if (!(my_set))
        return (NO_ERROR);
    list_free(my_set->list, bmpar_pareto_vector_element_free);
    efree(my_set);
    return (NO_ERROR);
} /* bmpar_pareto_vector_set_free */

/**********************
bmpar_vector_compare
Datum:  13.03.15
Autor:  Peter Buchholz
Input: elem1, elem2 Pareto elements to compare
Output: 
Side-Effects:
Description:
The function compares the value of vectors of two Pareto
elements and return 1 if elem1 is larger, 2 if elem2 is larger,
0 if they are equal and -1 if they are uncomparable. 
All comparisions are made up to BMDP_MIN_VAL. 
**********************/
int bmpar_vector_compare(Pareto_vector_element *elem1, Pareto_vector_element *elem2) {
    size_t i;
    int ret = 0, cur;

    if (elem1->ord != elem2->ord) {
        printf("Try to compare Pareto elments with different dimensions\nSTOP EXECUTION\n");
        exit(-1);
    }
    for (i = 0; i < elem1->ord; i++) {
        cur = is_larger(elem1->gain_min[i], elem2->gain_min[i], BMDP_COMP_POLICY);
        if (ret == 0)
            ret = cur;
        else {
            if (cur != 0 && cur != ret)
                return (-1);
        }
        cur = is_larger(elem1->gain_avg[i], elem2->gain_avg[i], BMDP_COMP_POLICY);
        if (ret == 0)
            ret = cur;
        else {
            if (cur != 0 && cur != ret)
                return (-1);
        }
        cur = is_larger(elem1->gain_max[i], elem2->gain_max[i], BMDP_COMP_POLICY);
        if (ret == 0)
            ret = cur;
        else {
            if (cur != 0 && cur != ret)
                return (-1);
        }
    } /* for i=0...ord-1 */
    return (ret);
} /* bmpar_vector_compare */

/**********************
bmpar_get_first
Datum:  13.03.15
Autor:  Peter Buchholz
Input: my_set set of policies with gain vectors
Output: return value paarteo vector element and NULL if set empty
Side-Effects:
Description:
The function removes the first element form a set of 
Pareto elements. For an empty list return 0.
**********************/
Pareto_vector_element *bmpar_get_first(Pareto_vector_set *my_set) {
    Pareto_vector_element *my_element;

    if (my_set->no_entries == 0)
        return (NULL);
    else {
        my_element = (Pareto_vector_element *) list_cont(my_set->list);
        my_set->list = list_next(my_set->list);
        my_set->no_entries--;
    }
    return (my_element);
} /* bmpar_get_first */

/**********************
bmpar_in_list
Datum:  13.03.15
Autor:  Peter Buchholz
Input: my_set set of policies with gain vectors
       my_elem element to be added to the set
Output: return value TRUE, if element in lsit FALSE else
Side-Effects:
Description:
The function adds an element to my_set, if no larger element is already in the set
and it removes all elements from the set that are dominated by the new elementchecks whetehr an element is still n the list. 
**********************/
short bmpar_in_list(Pareto_vector_set *my_set, Pareto_vector_element *my_elem) {
    Pareto_vector_element *comp_elem;
    List **le;

    le = my_set->list;
    while (!list_empty(le)) {
        comp_elem = (Pareto_vector_element *) list_cont(le);
        if (comp_elem == my_elem)
            return (TRUE);
        le = list_next(le);
    } /* while list nnot empty */
    return (FALSE);
} /* bmpar_in_list */

/**********************
bmpar_add_remove_element
Datum:  13.03.15
Autor:  Peter Buchholz
Input: my_set set of policies with gain vectors
       my_elem element to be added to the set
Output: return value Pareteo vector element and NULL if set empty
Side-Effects:
Description:
The function adds an element to my_set, if no larger element is already in the set
and it removes all elements from the set that are dominated by the new element. 
**********************/
int bmpar_add_remove_element(Pareto_vector_set *my_set, Pareto_vector_element *my_elem) {
    int comp;
    Pareto_vector_element *comp_elem;
    List **le, **prev;

#if BMDP_TRACE_INSERT
  printf("Call add_remove_element for list with %i entries\n", my_set->no_entries) ;
#endif
    le = my_set->list;
    prev = NULL;
    while (!list_empty(le)) {
        comp_elem = (Pareto_vector_element *) list_cont(le);
        comp = bmpar_vector_compare(my_elem, comp_elem);
        if (comp == 2 || comp == 0) {
            /* we are done since we found a larger or identical element */
            bmpar_pareto_vector_element_free(my_elem);
#if BMDP_TRACE_INSERT
      printf("Found larger or equal element %i\n", comp) ;
#endif
            return (FALSE);
        }
        else {
            if (comp == 1) {
                /* remove the element from the list, since the new one is larger */
                if (prev == NULL) {
                    /* we remove the first element */
                    my_set->list = list_next(my_set->list);
                    if (my_set->no_entries == 1)
                        my_set->list = list_new();
                    le = my_set->list;
                }
                else {
                    (*prev)->next = (*le)->next;
                    prev = le;
                    le = list_next(le);
                }
                my_set->no_entries--;
                bmpar_pareto_vector_element_free(comp_elem);
#if BMDP_TRACE_INSERT
	printf("Remove one element from list\n") ;
#endif
            }
            else {
#if BMDP_TRACE_INSERT
	printf("Incomparable element in list\n") ;
#endif
                prev = le;
                le = list_next(le);
            }
        }
    } /* while */
    list_prepend(my_set->list, (void *) my_elem);
    my_set->no_entries++;
#if BMDP_TRACE_INSERT
  printf("Add element to list\n") ;
#endif
    return (TRUE);
} /* bmpar_add_remove_element */

/**********************
bmpar_smaller_vector
Datum:  13.03.15
Autor:  Peter Buchholz
Input: policy1, policy2 policies to be compared
       ord length of the policy vectors
Output: return value 
        -1 if the first policy is lexicographically larger
        1 if the first policy is lexicographically smaller
        0 if both policies are equal
Side-Effects:
Description:
The function compares two policie s(integer vectors). 
**********************/
int bmpar_smaller_vector(size_t *policy1, size_t *policy2, size_t ord) {
    size_t i, index = ord;
    short ret;
    short stop = FALSE;
#if 0

    #pragma omp parallel shared(policy1, policy2, ord, stop, ret, index) /* private(i) */
    {
        /* let's implement plain multi-threading... */
        int segment_start, segment_stop;
        const int segment_size = ord / omp_get_num_threads();
        const int current_thread = omp_get_thread_num();

        /* #pragma omp critical */
        {
            if(segment_size > 0) {
                /* task subdivision */
                segment_start = current_thread * segment_size;
                segment_stop = (current_thread + 1) * segment_size;
                if(current_thread == omp_get_num_threads() - 1) {
                    segment_stop = ord;
                }
            } else {
                segment_start = current_thread;
                segment_stop = current_thread + 1;
                if(segment_stop > ord) {
                    segment_stop = ord;
                }
            }
        }

        for(i = segment_start; i < segment_stop && i < index; ++i) {
            if (policy1[i] < policy2[i]) {
                #pragma omp critical
                {
                    if(i < index) {
                        index = i;
                        ret = 1;
                        stop = TRUE;
                    }
                }
            } else if(policy1[i] > policy2[i]) {
                #pragma omp critical
                {
                    if(i < index) {
                        index = i;
                        ret = 2;
                        stop = TRUE;
                    }
                }
            }
        }
    }
#else
    for (i = 0; i < ord && !stop; i++) {
        if (policy1[i] < policy2[i]) {
            return (1);
        }
        if (policy1[i] > policy2[i]) {
            return (2);
        }
    }
#endif
    if(stop) {
        return ret;
    } else {
        return (0);
    }
}

void bmpar_show_list(List **my_list, int ord) {
    int i, *comp_policy;
    List **le;

    printf("Show list list:");
    le = my_list;
    while (!list_empty(le)) {
        comp_policy = (int *) list_cont(le);
        printf("(");
        for (i = 0; i < ord; i++)
            printf("%i ", comp_policy[i]);
        printf(")");
        le = list_next(le);
    }
    printf("\n");
}

/**********************
bmpar_add_hash
Datum:  13.03.15
Autor:  Peter Buchholz
Input: my_list list of analyzed policies
       my_policy currnet policy
       ord leingth of a policy
Output: return value 
        TRUE policy added, FALSE policy already available
Side-Effects:
Description:
The function tries to add a policy to set a set of polices. 
**********************/
short bmdp_add_hash(List **my_list, size_t *my_policy, size_t ord, bloom_filter bloom_set) {
    int comp;
    size_t *comp_policy;
    List **le, **prev, *l;

    le = my_list;
    prev = NULL;

    /* consider the Bloom filter. If the element is not inside, then insert it at once */
#if 1
    if(!bloom_filter_query(bloom_set, (void *) my_policy, ord)){
        bloom_filter_add(bloom_set, (void *) my_policy, ord);
        list_prepend(my_list, (void *) my_policy);
        return (TRUE);
    } else {
        /* search for the entry */
        while(!list_empty(le)) {
            comp_policy = (size_t *) list_cont(le);
            comp = bmpar_smaller_vector(my_policy, comp_policy, ord);
            if(!comp) {
                return(FALSE);
            }
            le = list_next(le);
        }
        /* not found -- insert */
        list_prepend(my_list, (void *) my_policy);
        return (TRUE);
    }
#endif /* 0 */
    while (!list_empty(le)) {
        comp_policy = (size_t *) list_cont(le);
        comp = bmpar_smaller_vector(my_policy, comp_policy, ord);
        if (comp == 0) {
            /* element already in list */
            return (FALSE);
        }
        if (comp == 1) {
            if (prev == NULL) {
                /* add as first element */
                list_prepend(my_list, (void *) my_policy);
                return (TRUE);
            }
            else {
                /* add element before current element */
                l = l_neu((void *) my_policy);
                l->next = *le;
                (*prev)->next = l;
                return (TRUE);
            }
        }
        /* go on to the next element */
        prev = le;
        le = list_next(le);
    }
    /* Add new element as last element to the list */
    list_append(my_list, (void *) my_policy);
    return (TRUE);
} /* bmdp_add_hash */

tree *make_tree(size_t *elem, size_t ord) {
    tree *tr = ecalloc(1, sizeof(tree));
    tr->ord = ord;
    tr->elem = elem;
    tr->left_child = tr->right_child = NULL;
    return tr;
}

short tree_insert(tree *tr, size_t *elem, size_t ord) {
    assert(ord == tr->ord);
    int comparison = bmpar_smaller_vector(elem, tr->elem, ord);
    if(comparison == 0) {
        /* elements are equal */
        return 0;
    } else if(comparison == 1) {
        /* elem is smaller */
        if(tr->left_child != NULL) {
            return tree_insert(tr->left_child, elem, ord);
        } else {
            tr->left_child = make_tree(elem, ord);
            return 1;
        }
    } else {
        /* elem is greater */
        if(tr->right_child != NULL) {
            return tree_insert(tr->right_child, elem, ord);
        } else {
            tr->right_child = make_tree(elem, ord);
            return 1;
        }
    }
}

short tree_test(tree *tr, size_t *elem, size_t ord) {
    if(tr == NULL) {
        return 0;
    }
    assert(ord == tr->ord);
    int comparison = bmpar_smaller_vector(elem, tr->elem, ord);

    if(comparison == 0) {
        return 1;
    } else if(comparison == 1) {
        return tree_test(tr->left_child, elem, ord);
    } else {
        return tree_test(tr->right_child, elem, ord);
    }
}

void tree_free(tree *tr) {
    if(tr->left_child != NULL) {
        tree_free(tr->left_child);
        efree(tr->left_child);
    }
    if(tr->right_child != NULL) {
        tree_free(tr->right_child);
        efree(tr->right_child);
    }
    efree(tr->elem);
}

/**********************
bmpar_first_in_list
Datum:  13.03.15
Autor:  Peter Buchholz
Input: my_set Pareto set 
       which 1 or 2 depending which flag should be set 
Output: returns a pointer to the first element in a list of 
        Pareto elements where the parameter seen is FALSE 
Side-Effects:
Description:
The function returns a pointer to the first element in a list of 
Pareto optimal policies and sets the value seen to TRUE for this element! 
**********************/
Pareto_vector_element *bmpar_first_in_list(Pareto_vector_set *my_set, short which) {
    Pareto_vector_element *my_elem = NULL;
    List **le;

    le = my_set->list;
    while (!list_empty(le)) {
        my_elem = (Pareto_vector_element *) list_cont(le);
        le = list_next(le);
        if (which == 1) {
            if (!(my_elem->seen1)) {
                my_elem->seen1 = TRUE;
                return (my_elem);
            }
        }
        else {
            if (which == 2) {
                if (!(my_elem->seen2)) {
                    my_elem->seen2 = TRUE;
                    return (my_elem);
                }
            }
        }
    }
    return (NULL);
} /* bmpar_first_in_list */

/**********************
bmpar_reset_seen
Datum:  14.03.15
Autor:  Peter Buchholz
Input: my_set Pareto set 
Output: none 
Side-Effects:
Description:
The function resets the value seen to FALSE for all elements in the
set my_set. 
**********************/
void bmpar_reset_seen(Pareto_vector_set *my_set, int which) {
    Pareto_vector_element *my_elem = NULL;
    List **le;

    le = my_set->list;
    while (!list_empty(le)) {
        my_elem = (Pareto_vector_element *) list_cont(le);
        le = list_next(le);
        if (which == 1)
            my_elem->seen1 = FALSE;
        else {
            if (which == 2)
                my_elem->seen2 = FALSE;
        }
    }
} /* bmpar_reset_seen */

/**********************
bmpar_find_pareto
Datum:  14.03.15
Autor:  Peter Buchholz
Input: my_elem Pareto element for which a Pareto better policy should be found
       (mdp_.., rew_..) BMDP
       gamma discount factor
       comp_max maximum or minimum 
Output: new policy
Side-Effects:
Description:
The function computes a Pareto dominant policy for a given policy if such a
a policy exists, otherwise it returns NULL.
**********************/
size_t *bmpar_find_pareto(Pareto_vector_element *my_elem, Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg,
        Mdp_matrix *mdp_max, Double_vektor **rew_min, Double_vektor **rew_avg,
        Double_vektor **rew_max, double gamma, short comp_max) {
    short found = FALSE, is_new = FALSE, stop = FALSE;
    size_t i, j, k, ord, *ind, **indarray;
    double val, diag, *vv;
    Row *hrow;
    ord = mdp_min->ord;

    /* hrow = mat_row_new(ord); */
    /* vv = (double *) ecalloc(ord, sizeof(double)); */
    ind = (size_t *) ecalloc(ord, sizeof(size_t));
    indarray = (size_t **) ecalloc(ord, sizeof(size_t *));
    for (i = 0; i < ord; i++) {
        ind[i] = my_elem->policy[i];
        indarray[i] = (size_t *) ecalloc(ord, sizeof(size_t));
        for(j = 0; j < ord; ++j) {
            indarray[i][j] = my_elem->policy[j];
        }
    }
    #pragma omp parallel shared(is_new) \
    private(found, stop, i, j, k, val, vv, hrow, diag) \
    firstprivate(ord, ind, my_elem, mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, gamma, comp_max) \
    // num_threads(10)
    {
        #pragma omp for nowait
        for (i = 0; i < ord; i++) {
            found = FALSE;
            double diag;
            vv = (double *) ecalloc(ord, sizeof(double));
            hrow = mat_row_new(ord);

            for (k = 0; k < mdp_min->rows[i]->pord; k++) {

                if (k != ind[i] && (mdp_min->rows[i]->diagonals[k] > 0.0 || mdp_min->rows[i]->no_of_entries[k] > 0)) {
                    stop = FALSE;
                    /* first test the average reward */
                    val = rew_avg[k]->vektor[i] + gamma * my_elem->gain_avg[i] * mdp_avg->rows[i]->diagonals[k];
                    for (j = 0; j < mdp_avg->rows[i]->no_of_entries[k]; j++) {
                        val += gamma * my_elem->gain_avg[mdp_avg->rows[i]->colind[k][j]] * mdp_avg->rows[i]->values[k][j];
                    }
                    if (comp_max) {
                        if (is_smaller(val, my_elem->gain_avg[i], BMDP_MIN_VAL) == 1)
                            stop = TRUE;
                        else {
                            if (is_smaller(val, my_elem->gain_avg[i], BMDP_MIN_VAL) == 2)
                                found = TRUE;
                        }
                    }
                    else {
                        if (is_smaller(val, my_elem->gain_avg[i], BMDP_MIN_VAL) == 2)
                            stop = TRUE;
                        else {
                            if (is_smaller(val, my_elem->gain_avg[i], BMDP_MIN_VAL) == 1)
                                found = TRUE;
                        }
                    } /* else comp_max */
                    if (!(stop)) {
                        /* Now analyze the minimum */
                        bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], hrow, &diag, i, k, my_elem->gain_min, vv, TRUE);
                        val = rew_min[k]->vektor[i] + gamma * my_elem->gain_min[i] * diag;
                        for (j = 0; j < mdp_max->rows[i]->no_of_entries[k]; j++) {
                            val += gamma * my_elem->gain_min[hrow->colind[j]] * hrow->val[j];
                        }
                        if (comp_max) {
                            if (is_smaller(val, my_elem->gain_min[i], BMDP_MIN_VAL) == 1) {
                                found = FALSE;
                                stop = TRUE;
                            }
                            else {
                                if (is_smaller(val, my_elem->gain_min[i], BMDP_MIN_VAL) == 2)
                                    found = TRUE;
                            }
                        }
                        else {
                            if (is_smaller(val, my_elem->gain_min[i], BMDP_MIN_VAL) == 2) {
                                found = FALSE;
                                stop = TRUE;
                            }
                            else {
                                if (is_smaller(val, my_elem->gain_min[i], BMDP_MIN_VAL) == 1)
                                    found = TRUE;
                            }
                        } /* else comp_max */
                    } /* ! stop */
                    if (!(stop)) {
                        /* Now analyze the maximum */
                        bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], hrow, &diag, i, k, my_elem->gain_max, vv, FALSE);
                        val = rew_max[k]->vektor[i] + gamma * my_elem->gain_max[i] * diag;
                        for (j = 0; j < mdp_max->rows[i]->no_of_entries[k]; j++) {
                            val += gamma * my_elem->gain_max[hrow->colind[j]] * hrow->val[j];
                        }
                        if (comp_max) {
                            if (is_smaller(val, my_elem->gain_max[i], BMDP_MIN_VAL) == 1) {
                                found = FALSE;
                            }
                            else {
                                if (is_smaller(val, my_elem->gain_max[i], BMDP_MIN_VAL) == 2)
                                    found = TRUE;
                            }
                        }
                        else {
                            if (is_smaller(val, my_elem->gain_max[i], BMDP_MIN_VAL) == 2) {
                                found = FALSE;
                            }
                            else {
                                if (is_smaller(val, my_elem->gain_max[i], BMDP_MIN_VAL) == 1)
                                    found = TRUE;
                            }
                        } /* else comp_max */
                    } /* ! stop  */
                    if (found)
                        break;
                } /* k != ind[i] */
            } /* for k = 0,...,act-1 */
            if (found) {
                #pragma omp atomic write
                is_new = TRUE;
                #pragma omp atomic write
                ind[i] = k;
            }
            efree(vv);
            mat_row_free(hrow);
        } /* for i = 0,...,pord-1 */
    }

    /* efree(vv); */
    /* mat_row_free(hrow); */
    if (is_new) {
        return (ind);
    }
    else {
        efree(ind);
        return (NULL);
    }
} /* bmpar_find_pareto */

/**********************
bmpar_is_better
Datum:  14.03.15
Autor:  Peter Buchholz
Input: pol New policy
        my_elem Pareto element for which a potentially better policy should be found
       (mdp_.., rew_..) BMDP
       gamma discount factor
       comp_max maximum or minimum 
Output: new policy
Side-Effects:
Description:
The function checks whether a policy is better in at least one component than another policy.
**********************/
int bmpar_is_better(size_t k, size_t i, Pareto_vector_element *my_elem, Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg,
        Mdp_matrix *mdp_max, Double_vektor **rew_min, Double_vektor **rew_avg,
        Double_vektor **rew_max, double gamma, short comp_max) {
    size_t j, ord = mdp_min->ord;
    double val, diag, *vv;
    Row *hrow;

    /* first test the average reward */
    val = rew_avg[k]->vektor[i] + gamma * my_elem->gain_avg[i] * mdp_avg->rows[i]->diagonals[k];
    for (j = 0; j < mdp_avg->rows[i]->no_of_entries[k]; j++)
        val += gamma * my_elem->gain_avg[mdp_avg->rows[i]->colind[k][j]] * mdp_avg->rows[i]->values[k][j];
    if (comp_max) {
        if (is_smaller(val, my_elem->gain_avg[i], BMDP_MIN_VAL) == 2) {
            return (TRUE);
        }
    }
    else {
        if (is_smaller(val, my_elem->gain_avg[i], BMDP_MIN_VAL) == 1) {
            return (TRUE);
        }
    } /* else comp_max */
    vv = (double *) ecalloc(ord, sizeof(double));
    hrow = mat_row_new(ord);
    /* Now analyze the minimum */
    bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], hrow, &diag, i, k, my_elem->gain_min, vv, TRUE);
    val = rew_min[k]->vektor[i] + gamma * my_elem->gain_min[i] * diag;
    for (j = 0; j < mdp_max->rows[i]->no_of_entries[k]; j++)
        val += gamma * my_elem->gain_min[hrow->colind[j]] * hrow->val[j];
    if (comp_max) {
        if (is_smaller(val, my_elem->gain_min[i], BMDP_MIN_VAL) == 2) {
            efree(vv);
            mat_row_free(hrow);
            return (TRUE);
        }
    }
    else {
        if (is_smaller(val, my_elem->gain_min[i], BMDP_MIN_VAL) == 1) {
            efree(vv);
            mat_row_free(hrow);
            return (TRUE);
        }
    } /* else comp_max */
    /* Now analyze the maximum */
    bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], hrow, &diag, i, k, my_elem->gain_max, vv, FALSE);
    val = rew_max[k]->vektor[i] + gamma * my_elem->gain_max[i] * diag;
    for (j = 0; j < mdp_max->rows[i]->no_of_entries[k]; j++)
        val += gamma * my_elem->gain_max[hrow->colind[j]] * hrow->val[j];
    if (comp_max) {
        if (is_smaller(val, my_elem->gain_max[i], BMDP_MIN_VAL) == 2) {
            efree(vv);
            mat_row_free(hrow);
            return (TRUE);
        }
    }
    else {
        if (is_smaller(val, my_elem->gain_max[i], BMDP_MIN_VAL) == 1) {
            efree(vv);
            mat_row_free(hrow);
            return (TRUE);
        }
    } /* else comp_max */
    efree(vv);
    mat_row_free(hrow);
    return (FALSE);
}

/* bmpar_is_better */

int bmpar_free_int(void *pointer) {
    if (pointer != NULL)
        efree(pointer);
    return (NO_ERROR);
}

/**
  * Use a heuristic to compute pure non-dominated policies.
  *
  * NOTE This is a heuristic, so do not expect the complete search space. Furthermore, do not expect completeness.
  *
  * The idea here is simple: Given a set of policies, we consider possible changes (one action in one state) to each of them.
  * If there is a change that yields another non-dominated policy, then we accept it. If not, then we are done.
  */
Pareto_vector_element **bmdp_pure_opt_ipt_heuristic(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                                    Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                                    double gamma, short comp_max, size_t *no_policies) {
    Pareto_vector_set *pareto_front;
    double starttime = mat_timeused();
    const size_t ord = mdp_avg->ord;
    size_t s, a, s_prime;
    size_t *old_policy;
    Pareto_vector_element **results, *elem, *new_elem;
    short stop = FALSE;
    int success;
    int iteration = 0;

    /* first, let us compute an initial policy */
    elem = bmpar_pareto_vector_element_new(ord);
    elem->policy = (size_t *) ecalloc(ord, sizeof(size_t));
    elem->gain_min = bmdp_compute_extreme_case(mdp_min, mdp_max, rew_min, elem->policy, BMDP_MAX_ITER, BMDP_MAX_TIME, BMDP_LO_EPS, 300, 1.0e-8, BO_GMRES_ILU0, gamma, TRUE, comp_max, FALSE);
    elem->gain_avg = bmdp_evaluate_avg_policy(mdp_avg, elem->policy, rew_avg, gamma, FALSE);
    elem->gain_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, elem->policy, rew_max, gamma, FALSE, FALSE);

    /* initialize stuff */
    pareto_front = bmpar_pareto_vector_set_new();
    bmpar_add_remove_element(pareto_front, elem);
    old_policy = (size_t *) ecalloc(ord, sizeof(size_t));

    while(!stop) {
        stop = TRUE;
        while(NULL != (elem = bmpar_first_in_list(pareto_front, 1))) {
            /* this is a safeguard. It may happen that there is a neighboring dominating policy, which may cause the removal of the parent. */
            for(s_prime = 0; s_prime < ord; ++s_prime) {
                old_policy[s_prime] = elem->policy[s_prime];
            }

            for(s = 0; s < ord; ++s) {
                for(a = 0; a < mdp_avg->rows[s]->pord; ++a) {
                    if(old_policy[s] != a) {
                        new_elem = bmpar_pareto_vector_element_new(ord);
                        new_elem->policy = (size_t *) ecalloc(ord, sizeof(size_t));

                        for(s_prime = 0; s_prime < ord; ++s_prime) {
                            new_elem->policy[s_prime] = old_policy[s_prime];
                        }
                        new_elem->policy[s] = a;

                        /* evaluate policy */
                        new_elem->gain_min = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, new_elem->policy, rew_min, gamma, TRUE, FALSE);
                        new_elem->gain_avg = bmdp_evaluate_avg_policy(mdp_avg, new_elem->policy, rew_avg, gamma, FALSE);
                        new_elem->gain_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, new_elem->policy, rew_max, gamma, FALSE, FALSE);

                        success = bmpar_add_remove_element(pareto_front, new_elem);
                        if(success) {
                            stop = FALSE;
                        }
                    }
                }
            }
        }
        iteration++;
        printf("%f: %i iterations, %i policies", mat_timeused() - starttime, iteration, pareto_front->no_entries);
    }

    efree(old_policy);
    *no_policies = pareto_front->no_entries;
    results = ecalloc(pareto_front->no_entries, sizeof(Pareto_vector_element *));
    for(s = 0; s < *no_policies; ++s) {
        elem = bmpar_get_first(pareto_front);
        results[s] = elem;
    }
    return results;
}

/**
  * Compute all pure non-dominated policies with the BFS approach.
  *
  * NOTE This is a proof of concept! It should be fast, but as of now I have no idea if it really is fast.
  */
Pareto_vector_element **bmdp_pure_opt_bfs(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                          Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                          double gamma, short comp_max, size_t *no_policies) {
    /* initialize several policy lists */
    Pareto_vector_set *p_prev, *p_next;
    Pareto_vector_set *pareto_front;
    double starttime = mat_timeused();
    const size_t ord = mdp_min->ord;
    size_t i, j, k, d;
    int policy_counter;
    Pareto_vector_element **results, *elem, *new_elem;
    short is_unseen;
    short is_not_dominated;
    tree *seen_policies;
    size_t *temp_policy;
    struct timeval start_time, end_time;

    gettimeofday(&start_time, NULL);

    /* first, let us compute an initial policy */
    elem = bmpar_pareto_vector_element_new(ord);
    elem->policy = (size_t *) ecalloc(ord, sizeof(int));
    for(i = 0; i < ord; ++i) {
        elem->policy[i] = 0;
    }
    elem->gain_min = bmdp_compute_extreme_case(mdp_min, mdp_max, rew_min, elem->policy, BMDP_MAX_ITER, BMDP_MAX_TIME, BMDP_LO_EPS, 300, 1.0e-8, BO_GMRES_ILU0, gamma, TRUE, comp_max, FALSE);
    elem->gain_avg = bmdp_evaluate_avg_policy(mdp_avg, elem->policy, rew_avg, gamma, FALSE);
    elem->gain_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, elem->policy, rew_max, gamma, FALSE, FALSE);

    seen_policies = make_tree(elem->policy, ord);

    /* initialize stuff */
    pareto_front = bmpar_pareto_vector_set_new();
    p_prev = bmpar_pareto_vector_set_new();
    p_next = bmpar_pareto_vector_set_new();

    bmpar_add_remove_element(pareto_front, elem);
    bmpar_add_remove_element(p_prev, elem);
    policy_counter = 1;

    for(d = 1; d <= 2 * ord; ++d) {
        if(p_next->list == NULL) {
            p_next->list = list_new();
        }

        /* first step of the algorithm: compute all non-dominated neighbors */
        do {
            elem = bmpar_get_first(p_prev);

            if(elem != NULL) {
                for(i = 0; i < ord; ++i) {
                    for(j = 0; j < mdp_avg->rows[i]->pord; ++j) {
                        if(elem->policy[i] != j) {
                            /* init policy */
                            temp_policy = (size_t *) ecalloc(ord, sizeof(size_t));
                            for(k = 0; k < ord; ++k) {
                                temp_policy[k] = elem->policy[k];
                            }
                            temp_policy[i] = j;
                            is_unseen = !tree_test(seen_policies, temp_policy, ord);

                            /* check gradient */
                            if(is_unseen) {
                                is_not_dominated = bmpar_is_better(j, i, elem, mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, gamma, comp_max);
                            }

                            if(is_unseen && is_not_dominated) {

                                /* check if we have already seen this policy */
                                is_unseen = tree_insert(seen_policies, temp_policy, ord);

                                if(is_unseen) {
                                    /* create new element */
                                    new_elem = bmpar_pareto_vector_element_new(ord);
                                    new_elem->policy = temp_policy;

                                    /* evaluate policy */
                                    new_elem->gain_min = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, new_elem->policy, rew_min, gamma, TRUE, FALSE);
                                    new_elem->gain_avg = bmdp_evaluate_avg_policy(mdp_avg, new_elem->policy, rew_avg, gamma, FALSE);
                                    new_elem->gain_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, new_elem->policy, rew_max, gamma, FALSE, FALSE);

                                    list_prepend(p_next->list, new_elem);
                                    p_next->no_entries++;

                                    policy_counter++;
                                } else {
                                    efree(temp_policy);
                                }
                            }
                        }
                    }
                }
            }
        } while(elem != NULL);
        /* second step of the algorithm: switch lists and add stuff into the Pareto front */
        p_prev->list = list_new();
        do {
            elem = bmpar_get_first(p_next);
            if(elem != NULL) {
                /* insert element into p_i */
                list_prepend(p_prev->list, elem);
                p_prev->no_entries++;

                /* update Pareto front */
                Pareto_vector_element *elem_copy = bmpar_pareto_vector_element_new(ord);
                elem_copy->policy = (size_t *) ecalloc(ord, sizeof(size_t));
                elem_copy->gain_min = (double *) ecalloc(ord, sizeof(double));
                elem_copy->gain_avg = (double *) ecalloc(ord, sizeof(double));
                elem_copy->gain_max = (double *) ecalloc(ord, sizeof(double));

                for(i = 0; i < ord; ++i) {
                    elem_copy->policy[i] = elem->policy[i];
                    elem_copy->gain_min[i] = elem->gain_min[i];
                    elem_copy->gain_avg[i] = elem->gain_avg[i];
                    elem_copy->gain_max[i] = elem->gain_max[i];
                }

                bmpar_add_remove_element(pareto_front, elem_copy);
            }
        } while(elem != NULL);

        /* limit the computation time */
        gettimeofday(&end_time, NULL);
        long delta_seconds = end_time.tv_sec - start_time.tv_sec;
        if(delta_seconds > BMDP_MAX_TIME || policy_counter > 2 * BMDP_MAX_CHECKED) {
            break;
        }
    }
    /* now we should have all policies */
    *no_policies = pareto_front->no_entries;
    results = ecalloc(pareto_front->no_entries, sizeof(Pareto_vector_element *));
    for(i = 0; i < (*no_policies); ++i) {
        elem = bmpar_get_first(pareto_front);
        results[i] = elem;
    }
    return results;
}

/**********************
bmpar_pure_opt
Datum:  14.03.15
Autor:  Peter Buchholz
Input: (mdp_.., rew_..) BMDP
       gamma discount factor
       comp_max maximum or minimum 
Output: return value pointer to teh policies plus corresponding vectors
        no_policies number of Pareto optimla polcies that have been computed 
Side-Effects:
Description:
The function computes the Pareto optimal policies for a BMDP.
**********************/
Pareto_vector_element **bmdp_pure_opt(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
        Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
        double gamma, short comp_max, size_t *no_policies) {
    short go_on, deleted;
    size_t ord, i, j, k, *pol, checked = 0;
    double starttime = mat_timeused();
    Pareto_vector_element **my_results, *my_elem, *new_elem;
    Pareto_vector_set *my_set;
    List **le;
    tree *seen_policies;
    //long bloom_set_size = 134217728; /* 2^{27} */
    //bloom_filter bloom_set;

    ord = mdp_min->ord;
    assert(mdp_min->ord == mdp_avg->ord && mdp_avg->ord == mdp_max->ord);

    //init_hash_functions(mdp_avg->ord);
    //bloom_set = bloom_filter_new(num_hash_functions, hash_functions, bloom_set_size);

    my_set = bmpar_pareto_vector_set_new();
    /* Start with the policies for minimal, average and maximal reward */
    my_elem = bmpar_pareto_vector_element_new(ord);
    my_elem->policy = (size_t *) ecalloc(ord, sizeof(size_t));
    my_elem->gain_min = bmdp_compute_extreme_case(mdp_min, mdp_max, rew_min, my_elem->policy, BMDP_MAX_ITER, BMDP_MAX_TIME, BMDP_LO_EPS, 300,
            1.0e-8, BO_GMRES_ILU0, gamma, TRUE, comp_max, FALSE);
    my_elem->gain_avg = bmdp_evaluate_avg_policy(mdp_avg, my_elem->policy, rew_avg, gamma, FALSE);
    my_elem->gain_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, my_elem->policy, rew_max, gamma, FALSE, FALSE);
    /* we have to copy the policy vector, since the policy element might be later deleted */
    pol = (size_t *) ecalloc(ord, sizeof(size_t));
    /* #pragma omp parallel shared(ord, pol, my_elem) private(i) */
    {
        /* #pragma omp for */
        for (i = 0; i < ord; i++) {
            pol[i] = my_elem->policy[i];
        }
    }

    seen_policies = make_tree(pol, ord);
    bmpar_add_remove_element(my_set, my_elem);

    my_elem = bmpar_pareto_vector_element_new(ord);
    my_elem->policy = (size_t *) ecalloc(ord, sizeof(size_t));
    if (ord < BMDP_SIZE_DIRECT)
        my_elem->gain_avg = bmdp_policy_iteration(mdp_avg, rew_avg, my_elem->policy, BMDP_MAX_ITER, BMDP_MAX_TIME, BMDP_LO_EPS,
                gamma, comp_max, FALSE);
    else
        my_elem->gain_avg = bmdp_precond_iteration(mdp_avg, rew_avg, my_elem->policy, BMDP_MAX_ITER, BMDP_MAX_TIME, BMDP_LO_EPS,
                300, 1.0e-8, BO_GMRES_ILU0, gamma, comp_max, FALSE);
    my_elem->gain_min = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, my_elem->policy, rew_min, gamma, TRUE, FALSE);
    my_elem->gain_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, my_elem->policy, rew_max, gamma, FALSE, FALSE);
    pol = (size_t *) ecalloc(ord, sizeof(size_t));

    /* #pragma omp parallel shared(ord, pol, my_elem) private(i) */
    {
        /* #pragma omp for */
        for (i = 0; i < ord; i++) {
            pol[i] = my_elem->policy[i];
        }
    }
    if (tree_insert(seen_policies, pol, ord)) {
        bmpar_add_remove_element(my_set, my_elem);
    }
    else {
        efree(pol);
    }
    my_elem = bmpar_pareto_vector_element_new(ord);
    my_elem->policy = (size_t *) ecalloc(ord, sizeof(size_t));
    my_elem->gain_max = bmdp_compute_extreme_case(mdp_min, mdp_max, rew_max, my_elem->policy, BMDP_MAX_ITER, BMDP_MAX_TIME, BMDP_LO_EPS, 300,
            1.0e-8, BO_GMRES_ILU0, gamma, FALSE, comp_max, FALSE);
    my_elem->gain_avg = bmdp_evaluate_avg_policy(mdp_avg, my_elem->policy, rew_avg, gamma, FALSE);
    my_elem->gain_min = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, my_elem->policy, rew_min, gamma, TRUE, FALSE);
    pol = (size_t *) ecalloc(ord, sizeof(size_t));

    /* #pragma omp parallel shared(ord, pol, my_elem) private(i) */
    {
        /* #pragma omp for */
        for (i = 0; i < ord; i++) {
            pol[i] = my_elem->policy[i];
        }
    }
    if (tree_insert(seen_policies, pol, ord)) {
        bmpar_add_remove_element(my_set, my_elem);
    }
    else {
        efree(pol);
    }
    checked = 3;
    /* Now we can start the process to look for new policies */
    go_on = TRUE;
    while (go_on) {
        go_on = FALSE;
        /* find new policies that dominate available policies
           run through all policies in the set that have not been
           seen yet */
        do {
            my_elem = bmpar_first_in_list(my_set, 1);
            if (my_elem != NULL) {
                pol = bmpar_find_pareto(my_elem, mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, gamma, comp_max);
                if (pol != NULL && tree_insert(seen_policies, pol, ord)) {
                    new_elem = bmpar_pareto_vector_element_new(ord);
                    new_elem->policy = (size_t *) ecalloc(ord, sizeof(size_t));
                    for (i = 0; i < ord; i++) {
                        new_elem->policy[i] = pol[i];
                    }
                    int idx;
                    #pragma omp parallel firstprivate(new_elem, mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, gamma) private(idx) num_threads(3)
                    {
                        double **gain_min = &(new_elem->gain_min);
                        double **gain_avg = &(new_elem->gain_avg);
                        double **gain_max = &(new_elem->gain_max);
                        #pragma omp for nowait
                        for(idx = 0; idx < 3; ++idx) {
                            double *value_vector;
                            if(idx == 0) {
                                value_vector = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, new_elem->policy, rew_min, gamma, TRUE, FALSE);
                                /* #pragma omp atomic write */
                                *gain_min = value_vector;
                            } else if(idx == 1) {
                                value_vector = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, new_elem->policy, rew_max, gamma, FALSE, FALSE);
                                /* #pragma omp atomic write */
                                *gain_max = value_vector;
                            } else /* if(idx == 2) */ {
                                value_vector = bmdp_evaluate_avg_policy(mdp_avg, new_elem->policy, rew_avg, gamma, FALSE);
                                /* #pragma omp atomic write */
                                *gain_avg = value_vector;
                            }
                        }
                    }
                    if (bmpar_add_remove_element(my_set, new_elem)) {
                        go_on = TRUE;
                        checked++;
                    }
                    else {
                        printf("Find a Pareto optimal policy that is not added to the set of policies\nSTOP EXECUTION\n");
                        /* exit(-1) ; */
                    }
                }
                else {
                    if (pol != NULL)
                        efree(pol);
                } /* else */
            } /* my_elem != NULL */
        } while (my_elem != NULL);
        do {
            my_elem = bmpar_first_in_list(my_set, 2);
            if (my_elem != NULL) {
                deleted = FALSE;
                pol = (size_t *) ecalloc(ord, sizeof(size_t));
                for (i = 0; i < ord; i++) {
                    pol[i] = my_elem->policy[i];
                }
                /* Try all possibilities to improve the policy */
                for (i = 0; i < ord; i++) {
                    for (k = 0; k < mdp_min->rows[i]->pord; k++) {
                        if (k != my_elem->policy[i] && (mdp_min->rows[i]->diagonals[k] > 0.0 || mdp_min->rows[i]->no_of_entries[k] > 0)) {
                            pol[i] = k;
                            if (tree_insert(seen_policies, pol, ord)) {
                                if (bmpar_is_better(pol[i], i, my_elem, mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, gamma, comp_max)) {
                                    new_elem = bmpar_pareto_vector_element_new(ord);
                                    new_elem->policy = (size_t *) ecalloc(ord, sizeof(size_t));
                                    /* #pragma omp parallel for private(j) shared(new_elem, pol) */
                                    for (j = 0; j < ord; j++) {
                                        new_elem->policy[j] = pol[j];
                                    }
                                    /* parallelize */
                                    int idx;
                                    #pragma omp parallel firstprivate(new_elem, mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, gamma) private(idx) num_threads(3)
                                    {
                                        #pragma omp for nowait
                                        for(idx = 0; idx < 3; ++idx) {
                                            double *value_vector;
                                            if(idx == 0) {
                                                value_vector = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, new_elem->policy, rew_min, gamma, TRUE, FALSE);
                                                /* #pragma omp atomic write */
                                                new_elem->gain_min = value_vector;
                                            } else if(idx == 1) {
                                                value_vector = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, new_elem->policy, rew_max, gamma, FALSE, FALSE);
                                                /* #pragma omp atomic write */
                                                new_elem->gain_max = value_vector;
                                            } else /* if(idx == 2) */ {
                                                value_vector = bmdp_evaluate_avg_policy(mdp_avg, new_elem->policy, rew_avg, gamma, FALSE);
                                                /* #pragma omp atomic write */
                                                new_elem->gain_avg = value_vector;
                                            }
                                        }
                                    }
                                    if (bmpar_add_remove_element(my_set, new_elem)) {
                                        go_on = TRUE;
                                    }
                                    checked++;
                                }
                                if (bmpar_in_list(my_set, my_elem)) {
                                    pol = (size_t *) ecalloc(ord, sizeof(size_t));
                                    for (j = 0; j < ord; j++)
                                        pol[j] = my_elem->policy[j];
                                }
                                else {
                                    deleted = TRUE;
                                    break;
                                }
                            } /* new and potentially better */
                        } /* if k != my_elem->policy[i] */
                    } /* for k */
                    if (deleted) {
                        deleted = FALSE;
                        break;
                    }
                    else
                        pol[i] = my_elem->policy[i];
                    if (checked > BMDP_MAX_CHECKED)
                        break;
                } /* for i */
            } /* my_elem != NULL */
            if(checked % 1000 == 0) {
                int unused = 0;
            }
            if (checked > BMDP_MAX_CHECKED)
                break;
        } while (my_elem != NULL);
        if (checked > BMDP_MAX_CHECKED) {
            printf("Stop due to maximal number of analyzed polcies (%zu policies checked)\n", checked);
            go_on = FALSE;
        }
    } /* while go_on */
    /* Copy the resulting policies in an array of policies */
    size_t np = my_set->no_entries;
    my_results = (Pareto_vector_element **) calloc(np, sizeof(Pareto_vector_element *));
    le = my_set->list;
    i = 0;
    while (!list_empty(le)) {
        my_results[i] = (Pareto_vector_element *) list_cont(le);
        le = list_next(le);
        if (i == np - 1)
            break;
        else
            i++;
    }
    if (i < np - 1) {
        printf("Expected %zu policies found only %zu policies\n", np, i + 1);
        np = i + 1;
    }
    // list_free(pl, bmpar_free_int);
    tree_free(seen_policies);
    efree(seen_policies);
    //efree(pl);
    efree(my_set);
#if BMDP_TRC_ITER
    printf("Checked %zu policies found %zu Pareto optimal policies\n", checked, np);
    printf("Computation requires %.3e seconds\n", mat_timeused() - starttime);
#endif
    *no_policies = np;
    return (my_results);
} /* bmpar_pure_opt */


void bmpar_print_pareto_values(char *base_name, Pareto_vector_element **results, size_t no_policies) {
    FILE *fp;
    size_t i;
    size_t j;
    char *fname = (char *) ecalloc(100, sizeof(char));
    fname = strcpy(fname, base_name);
    fname = strcat(fname, ".pareto_vals");

    if((fp = fopen(fname, "w")) == NULL) {
        fprintf(stderr, "Cannot open file %s for writing", fname);
        efree(fname);
        exit(EXIT_FAILURE);
    }
    for(i = 0; i < no_policies; ++i) {
        Pareto_vector_element *elem = results[i];
        fprintf(fp, "%5zu", i);
        for(j = 0; j < elem->ord; ++j) {
            fprintf(fp, " %10e %10e %10e %zu", elem->gain_min[j], elem->gain_avg[j], elem->gain_max[j], elem->policy[j]);
        }
        fprintf(fp, "\n");
    }
    fclose(fp);
    efree(fname);
}

/**********************
bmpar_print_pareto_results
Datum:  14.03.15
Autor:  Peter Buchholz
Input: base_name praefix of teh file names
       my_results result vectors of Pareto optimal policies
       no_policies number ofPareto optimal policies
     prt_ind 0 nothing, 1 only stationay, 2 stationary and policies, 3 everything
Output: 
Side-Effects:
Description:
The function writes the policy vectors and the gain vectors of a Pareto optimization to a file.
**********************/
void bmpar_print_pareto_results(char *base_name, Pareto_vector_element **my_results, size_t no_policies, short prt_id) {
    FILE *fp_min, *fp_avg, *fp_max, *fp_policy, *fp_stat;
    char *fil_name = (char *) ecalloc(100, sizeof(char));
    size_t i;
    size_t j;

    if (prt_id > 0) {
        fil_name = strcpy(fil_name, base_name);
        fil_name = strcat(fil_name, ".pareto_stat");
        if ((fp_stat = fopen(fil_name, "w")) == NULL) {
            fprintf(stderr, " Can't open output file %s for stationary results\n", fil_name);
            efree(fil_name);
            exit(-1);
        }
        fprintf(fp_stat, "# Number of stationary results %lu\n", no_policies);
        fprintf(fp_stat, "# Number min avg max wmin wavg wmax\n");
        if (prt_id > 1) {
            fil_name = strcpy(fil_name, base_name);
            fil_name = strcat(fil_name, ".pareto_policy");
            if ((fp_policy = fopen(fil_name, "w")) == NULL) {
                fprintf(stderr, " Can't open output file %s for the policies\n", fil_name);
                efree(fil_name);
                exit(-1);
            }
            fprintf(fp_policy, "# Number of policies:\n%lu\n", no_policies);
            if (prt_id > 2) {
                fil_name = strcpy(fil_name, base_name);
                fil_name = strcat(fil_name, ".pareto_gain_min");
                if ((fp_min = fopen(fil_name, "w")) == NULL) {
                    fprintf(stderr, " Can't open output file %s for the minimal gain\n", fil_name);
                    efree(fil_name);
                    exit(-1);
                }
                fil_name = strcpy(fil_name, base_name);
                fil_name = strcat(fil_name, ".pareto_gain_max");
                if ((fp_max = fopen(fil_name, "w")) == NULL) {
                    fprintf(stderr, " Can't open output file %s for the maximal gain\n", fil_name);
                    efree(fil_name);
                    exit(-1);
                }
                fil_name = strcpy(fil_name, base_name);
                fil_name = strcat(fil_name, ".pareto_gain_avg");
                if ((fp_avg = fopen(fil_name, "w")) == NULL) {
                    fprintf(stderr, " Can't open output file %s for the average gain\n", fil_name);
                    efree(fil_name);
                    exit(-1);
                }
                fprintf(fp_min, "# Number of gain vectors:\n%lu\n", no_policies);
                fprintf(fp_max, "# Number of gain vectors:\n%lu\n", no_policies);
                fprintf(fp_avg, "# Number of gain vectors:\n%lu\n", no_policies);

            } /* prt_id > 2 */
        } /* prt_id > 1*/
        for (i = 0; i < no_policies; i++) {
            fprintf(fp_stat, "%zu  %.10e  %.10e  %.10e  %.10e  %.10e  %.10e\n", i,
                    my_results[i]->stat_min, my_results[i]->stat_avg, my_results[i]->stat_max,
                    my_results[i]->weight_min, my_results[i]->weight_avg, my_results[i]->weight_max);
            if (prt_id > 1) {
                fprintf(fp_policy, "# Policy %zu:\n%zu\n", i, my_results[i]->ord);
                for (j = 0; j < my_results[i]->ord; j++) {
                    fprintf(fp_policy, "%zu\n", my_results[i]->policy[j]);
                }
                if (prt_id > 2) {
                    fprintf(fp_min, "# Gain vector %zu:\n%zu\n", i, my_results[i]->ord);
                    fprintf(fp_max, "# Gain vector %zu:\n%zu\n", i, my_results[i]->ord);
                    fprintf(fp_avg, "# Gain vector %zu:\n%zu\n", i, my_results[i]->ord);
                    for (j = 0; j < my_results[i]->ord; j++) {
                        fprintf(fp_min, "%.5e\n", my_results[i]->gain_min[j]);
                        fprintf(fp_max, "%.5e\n", my_results[i]->gain_max[j]);
                        fprintf(fp_avg, "%.5e\n", my_results[i]->gain_avg[j]);
                    }
                } /* prt_id > 2 */
            } /* prt_id > 1 */
        } /* for all policies */
        fclose(fp_stat);
        if (prt_id > 1) {
            fclose(fp_policy);
            if (prt_id > 2) {
                fclose(fp_min);
                fclose(fp_max);
                fclose(fp_avg);
            }
        }
    } /* prt_id > 0 */
    efree(fil_name);
} /* bmpar_print_pareto_results */


void bmpar_print_nonst_pareto_results(char *base_name, Pareto_vector_element *my_results, size_t no_policies) {
    FILE *fp;

    char* ending = ".pareto_nonst";
    char *fname = (char *) ecalloc(strlen(base_name) + strlen(ending), sizeof(char));
    sprintf(fname, "%s%s", base_name, ending);

    if((fp = fopen(fname, "w")) == NULL) {
        fprintf(stderr, "Cannot open output file %s", fname);
        exit(EXIT_FAILURE);
    }
    fprintf(fp, "# Number of nonstationary results: \n%lu\n", no_policies);
    fprintf(fp, "# id min avg max");

    int i;
    for(i = 0; i < no_policies; ++i) {
        double vmin = 0, vavg = 0, vmax = 0;
        Pareto_vector_element el = my_results[i];
        size_t j;

        for(j = 0; j < el.ord; ++j) {
            vmin += el.gain_min[j];
            vavg += el.gain_avg[j];
            vmax += el.gain_max[j];
        }

        fprintf(fp, "%i %.5e %.5e %.5e\n", i, vmin, vavg, vmax);
    }
    fclose(fp);
}

/**********************
bmpar_stationary_analysis
Datum:  16.03.15
Autor:  Peter Buchholz
Input: (mdp_..) BMDP matrices
       gamma discount factor
       my_results Pareto optimal policies
       no_policies number ofPareto optimal policies
Output: 
Side-Effects:
Description:
The function writes the policy vectors and the gain vectors of a Pareto optimization to a file.
**********************/
void bmpar_stationary_analysis(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
        double gamma, Pareto_vector_element **my_results, size_t no_policies) {
    int i, j;
    double min_val, avg_val, max_val, val, starttime = mat_timeused(), *vv, *hh;
    Double_vektor *b, rh;
    Sparse_matrix *pp;
    Solution_param param;

    pp = bmdp_gen_precond_structure(mdp_min);
    b = mat_double_vektor_new(mdp_min->ord);
    vv = (double *) ecalloc(mdp_min->ord, sizeof(double));
    hh = (double *) ecalloc(mdp_min->ord, sizeof(double));

    printf("Stationary results for Pareto optimal policies\n");
    for (i = 0; i < no_policies; i++) {
        bmdp_set_extreme_values(mdp_min, mdp_max, pp, my_results[i]->policy, my_results[i]->gain_min, vv, gamma, TRUE);
        mat_sparse_matrix_mult_scalar(pp, -1.0 / gamma);
        val = (gamma - 1.0) / gamma;
        int err;
        for (j = 0; j < pp->ord; j++)
            pp->diagonals[j] -= val;
        if (pp->ord < BMDP_SIZE_DIRECT) {
            if ((err = sol_direct_solution(pp, b, TRUE, NULL)) == NO_ERROR)
                printf("Stationary solution successful\n");
            else {
                FILE *fp_matrix, *fp_vector;
                fp_matrix = fopen("problematic_matrix", "w");
                fp_vector = fopen("problematic_vector", "w");
                mat_sparse_matrix_print_file(fp_matrix, pp);
                mat_double_vektor_print_file(fp_vector, b);
                fclose(fp_vector);
                fclose(fp_matrix);
                printf("Stationary solution failed: %d\n", err);
            }
        }
        else {
            param.n_proc = 1;
            /* param.method = STD_ILU0_GMRES  ; */
            /* param.method = STD_ILU0_BICGSTAB ; */
            param.method = STD_SOR;
            param.no_of_iterations = 500;
            param.epsilon = 1.0e-8;
            param.epsilon_2 = 0.1;
            param.epsilon_3 = 0.0;
            param.time_limit = 500.0;
            param.relaxation = 0.98;
            param.initials = NULL;
            param.overall_result = 0;
            param.subnet_results = NULL;
            val = 1.0 / (double) pp->ord;
            for (j = 0; j < pp->ord; j++)
                b->vektor[j] = val;
            /* sol_gmres(pp, b, &param, 1) ; */
            /* sol_bicgstab(pp, b, &param, 0) ; */
            sol_sor_iteration(&pp, b, &param, TRUE, TRUE, NULL);
        }
        rh.no_of_entries = my_results[i]->ord;
        rh.vektor = my_results[i]->gain_min;
        min_val = mat_double_vektor_inner_product(b, &rh);
        bmdp_set_extreme_values(mdp_min, mdp_max, pp, my_results[i]->policy, my_results[i]->gain_max, vv, gamma, FALSE);
        mat_sparse_matrix_mult_scalar(pp, -1.0 / gamma);
        val = (gamma - 1.0) / gamma;
        for (j = 0; j < pp->ord; j++)
            pp->diagonals[j] -= val;
        if (pp->ord < BMDP_SIZE_DIRECT) {
            if (sol_direct_solution(pp, b, TRUE, NULL) == NO_ERROR)
                printf("Stationary solution successful\n");
            else {
                printf("Stationary solution failed\n");
            }
        }
        else {
            param.n_proc = 1;
            /* param.method = STD_ILU0_GMRES  ; */
            /* param.method = STD_ILU0_BICGSTAB ; */
            param.method = STD_SOR;
            param.no_of_iterations = 500;
            param.epsilon = 1.0e-8;
            param.epsilon_2 = 0.1;
            param.epsilon_3 = 0.0;
            param.time_limit = 500.0;
            param.relaxation = 0.98;
            param.initials = NULL;
            param.overall_result = 0;
            param.subnet_results = NULL;
            val = 1.0 / (double) pp->ord;
            for (j = 0; j < pp->ord; j++)
                b->vektor[j] = val;
            /* sol_gmres(pp, b, &param, 1) ; */
            /* sol_bicgstab(pp, b, &param, 0) ; */
            sol_sor_iteration(&pp, b, &param, TRUE, TRUE, NULL);
        }
        rh.no_of_entries = my_results[i]->ord;
        rh.vektor = my_results[i]->gain_max;
        max_val = mat_double_vektor_inner_product(b, &rh);
        bmdp_set_strategy(mdp_avg, pp, my_results[i]->policy, gamma);
        mat_sparse_matrix_mult_scalar(pp, -1.0 / gamma);
        val = (gamma - 1.0) / gamma;
        for (j = 0; j < pp->ord; j++)
            pp->diagonals[j] -= val;
        if (mdp_avg->ord < BMDP_SIZE_DIRECT) {
            if (sol_direct_solution(pp, b, TRUE, NULL) == NO_ERROR)
                printf("Stationary solution successful\n");
            else {
                printf("Stationary solution failed\n");
            }
        }
        else {
            param.n_proc = 1;
            /* param.method = STD_ILU0_GMRES  ; */
            /* param.method = STD_ILU0_BICGSTAB ; */
            param.method = STD_SOR;
            param.no_of_iterations = 500;
            param.epsilon = 1.0e-8;
            param.epsilon_2 = 0.1;
            param.epsilon_3 = 0.0;
            param.time_limit = 500.0;
            param.relaxation = 0.98;
            param.initials = NULL;
            param.overall_result = 0;
            param.subnet_results = NULL;
            val = 1.0 / (double) pp->ord;
            for (j = 0; j < pp->ord; j++)
                b->vektor[j] = val;
            /* sol_gmres(pp, b, &param, 1) ; */
            /* sol_bicgstab(pp, b, &param, 0) ; */
            sol_sor_iteration(&pp, b, &param, TRUE, TRUE, NULL);
        }
        rh.no_of_entries = my_results[i]->ord;
        rh.vektor = my_results[i]->gain_avg;
        avg_val = mat_double_vektor_inner_product(b, &rh);
        my_results[i]->stat_min = min_val;
        my_results[i]->stat_avg = avg_val;
        my_results[i]->stat_max = max_val;
        printf("Policy %i:min %.5e avg %.5e max %.5e\n", i, min_val, avg_val, max_val);
        min_val = avg_val = max_val = 0.0;
        for(j = 0; j < pp->ord; j++){
            min_val += my_results[i]->gain_min[j];
            avg_val += my_results[i]->gain_avg[j];
            max_val += my_results[i]->gain_max[j];
        }
        printf("Policy %i (sum):min %.5e avg %.5e max %.5e\n", i, min_val, avg_val, max_val);
        printf("Policy %i (weight):min %.5e avg %.5e max %.5e\n", i, my_results[i]->weight_min, my_results[i]->weight_avg, my_results[i]->weight_max);
    } /* for all policies */
    mat_sparse_matrix_free(pp);
    mat_double_vektor_free(b);
    efree(vv);
    efree(hh);
#if BMDP_TRC_ITER
    printf("Stationary analysis requires %.3e seconds\n", mat_timeused() - starttime);
#endif
} /* bmpar_stationary_analysis */


short dominates(tri_objective x, tri_objective y, short comp_max) {
    short almost_dominates = 0, is_better = 0;
    if(comp_max) {
        almost_dominates = (x.min >= y.min) && (x.avg >= y.avg) && (x.max >= y.max);
        is_better = (x.min > y.min) || (x.avg > y.avg) || (x.max > y.max);
    } else {
        almost_dominates = (x.min <= y.min) && (x.avg <= y.avg) && (x.max <= y.max);
        is_better = (x.min < y.min) || (x.avg < y.avg) || (x.max < y.max);
    }
    return (almost_dominates && is_better);
}

Pareto_vector_element *bmdp_pareto_opt_step(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                            Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                            double gamma, short comp_max, int *no_policies, Pareto_vector_element *value_vectors,
                                            long num_value_vectors, tri_objective *max_value, size_t max_policies) {
    /* initialize helper vectors */
    double *vv_min = (double *) ecalloc(mdp_avg->ord * 2, sizeof(double));
    double *vv_max = vv_min + mdp_avg->ord; /* dirty hacking to avoid unneccesary malloc's */

    Row *hrow = mat_row_new(mdp_avg->ord);
    double diag;

    List ***tuple_lists = ecalloc(mdp_avg->ord, sizeof(List **));
    long *num_triples = ecalloc(mdp_avg->ord, sizeof(long));
    size_t j;
    int i;
    size_t state = 0;

    for(state = 0; state < mdp_avg->ord; ++state) {
        tuple_lists[state] = list_new();
        num_triples[state] = 0;
        for(i = 0; i < num_value_vectors; ++i) {
            size_t action;
            for(action = 0; action < mdp_avg->rows[state]->pord; ++action) {
                /* compute action results */
                tri_objective triple;
                hrow->no_of_entries = mdp_min->rows[state]->no_of_entries[action];

                double *hh_avg = value_vectors[i].gain_avg;
                double *hh_min = value_vectors[i].gain_min;
                double *hh_max = value_vectors[i].gain_max;

                triple.avg = rew_avg[action]->vektor[state] + gamma * mdp_avg->rows[state]->diagonals[action] * hh_avg[state];
                for(j = 0; j < mdp_avg->rows[state]->no_of_entries[action]; j++)
                    triple.avg += gamma * hh_avg[mdp_avg->rows[state]->colind[action][j]] * mdp_avg->rows[state]->values[action][j];

                /* do the sorting and ordering stuff... */
                /* compute the pessimistic bound */

                bmdp_set_one_minmax_row(mdp_min->rows[state], mdp_max->rows[state], hrow, &diag, state, action, hh_min, vv_min, TRUE);
                triple.min = rew_min[action]->vektor[state] + gamma * hh_min[state] * diag;

                for (j = 0; j < mdp_max->rows[state]->no_of_entries[action]; j++)
                    triple.min += gamma * hh_min[hrow->colind[j]] * hrow->val[j];

                /* compute the optimistic bound */
                bmdp_set_one_minmax_row(mdp_min->rows[state], mdp_max->rows[state], hrow, &diag, state, action, hh_max, vv_max, FALSE);
                triple.max = rew_max[action]->vektor[state] + gamma * hh_max[state] * diag;

                for (j = 0; j < mdp_max->rows[state]->no_of_entries[action]; j++)
                    triple.max += gamma * hh_max[hrow->colind[j]] * hrow->val[j];

                /* at this point, we have computed the triple. Now we should insert it, if it is non-dominated */
                /* iterate over the list */
                short dominated = FALSE;
                List **pv = tuple_lists[state];
                while(!list_empty(pv)) {
                    tri_objective *elem = (tri_objective *) (*pv)->elem;
                    pv = list_next(pv);

                    double max_diff = fmax(fmax(fabs(triple.min - elem->min), fabs(triple.avg - elem->avg)), fabs(triple.max - elem->max));

                    if(dominates(*elem, triple, comp_max) || max_diff < BMDP_LO_EPS) {
                        dominated = TRUE;
                        break;
                    } else if(dominates(triple, *elem, comp_max)) {
                        list_delete(tuple_lists[state], elem, list_free_delete_standard, list_compare_element);
                        num_triples[state]--;
                    }
                }
                if(!dominated) {
                    tri_objective *to_insert = ecalloc(1, sizeof(tri_objective));
                    to_insert->min = triple.min;
                    to_insert->avg = triple.avg;
                    to_insert->max = triple.max;

                    list_prepend(tuple_lists[state], to_insert);
                    num_triples[state]++;

                    max_value->max = (max_value->max > triple.max) ? max_value->max : triple.max;
                    max_value->avg = (max_value->avg > triple.avg) ? max_value->avg : triple.avg;
                    max_value->min = (max_value->min > triple.min) ? max_value->min : triple.min;
                }
            }
        }
    }
    efree(vv_min);
    mat_row_free(hrow);

    /* concatenate triples */
    size_t num_vectors = 1;
    for(state = 0; state < mdp_avg->ord; ++state) {
        num_vectors *= num_triples[state];
    }
    efree(num_triples);

    num_vectors = num_vectors > max_policies ? max_policies : num_vectors;

    Pareto_vector_element *elements = ecalloc(num_vectors, sizeof(Pareto_vector_element));
    List ***list_heads = ecalloc(mdp_avg->ord, sizeof(List **));
    for(state = 0; state < mdp_avg->ord; ++state) {
        list_heads[state] = tuple_lists[state];
    }

    j = 0;
    while(!list_empty(list_heads[mdp_avg->ord - 1]) && j < num_vectors) {
        // do some dirty hacking
        elements[j].ord = mdp_avg->ord;
        elements[j].gain_min = (double *) ecalloc(mdp_min->ord * 3, sizeof(double));
        elements[j].gain_avg = elements[j].gain_min + mdp_min->ord;
        elements[j].gain_max = elements[j].gain_avg + mdp_min->ord;

        for(state = 0; state < mdp_avg->ord; ++state) {
            tri_objective *triple = (*list_heads[state])->elem;

            elements[j].gain_avg[state] = triple->avg;
            elements[j].gain_max[state] = triple->max;
            elements[j].gain_min[state] = triple->min;
        }
        /* set the pointer to next triple */
        short at_end = FALSE;
        i = 0;
        do {
            list_heads[i] = list_next(list_heads[i]);
            at_end = list_empty(list_heads[i]);
            if(at_end && i < mdp_avg->ord - 1) {
                list_heads[i] = tuple_lists[i];
            }
            ++i;
        } while(i < mdp_avg->ord && at_end);
        ++j;
    }
    *no_policies = num_vectors;

    /* free lists */
    for(state = 0; state < mdp_avg->ord; ++state) {

        list_kill(tuple_lists[state], list_free_delete_standard);
    }
    efree(list_heads);
    efree(tuple_lists);

    return elements;
}

extern Pareto_vector_element* bmdp_pareto_opt(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                              Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                              double gamma, double maxerr, short comp_max, size_t *no_policies) {
    /*
     * In pseudo-code:
     * Start with a zero value vector
     * Compute all possible actions on it
     * Choose only non-dominated results
     * Construct consolidated value vectors
     */
    Pareto_vector_element *new_elements = ecalloc(1, sizeof(Pareto_vector_element));
    /* initalize the set with a zero vector */
    new_elements->gain_min = ecalloc(mdp_avg->ord, sizeof(double));
    new_elements->gain_avg = ecalloc(mdp_max->ord, sizeof(double));
    new_elements->gain_max = ecalloc(mdp_min->ord, sizeof(double));
    int num_policies = 0;
    *no_policies = 1;

    size_t max_policies = (1 << 20);

    tri_objective max_value;
    max_value.min = 0;
    max_value.avg = 0;
    max_value.max = 0;
    double error = maxerr + 1;

    int k = 1;
    while(error > maxerr && k < 7) {
        long old_policies = num_policies;
        Pareto_vector_element *old_elements = new_elements;
        new_elements = bmdp_pareto_opt_step(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, gamma, comp_max, &num_policies, old_elements, *no_policies, &max_value, max_policies);

        /* free old elements */
        int j = 0;
        for(; j < old_policies; ++j) {
            efree(old_elements[j].gain_min);
        }
        efree(old_elements);

        double gamma_k = pow(gamma, k);
        *no_policies = num_policies;
        double relative_error = gamma_k / (1.0 - gamma_k);

        double error_max = max_value.max * relative_error;
        double error_min = max_value.min * relative_error;
        double error_avg = max_value.avg * relative_error;

        error = error_max;

        printf("step %i new policies: %i, relative error: %f maximal absolute error: %f\n", k, num_policies, relative_error, error);
        ++k;
    }
    return new_elements;
}

/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c (C) Copyright 1998 by Peter Buchholz
c     All Rights Reserved
c
c transient.c
c
c Basisfunktion zur transienten Analyse von CTMCs
c 
c
c       Datum           Modifikationen
c       ----------------------------------------------------------
c       06.02.98 Datei angelegt,  
c       05.11.98 Neue Paramter bei mat_sparse_matrix_mult_vektor
c       06.11.98 Sonderfall alpha = 0 in tr_randomization und tr_acc_randomization
c                abfangen
c       24.08.99 tr_gen_step_matrix eingefuegt
c       30.08.99 Fehler in  tr_gen_step_matrix beseitigt
c       31.08.99 Verwendung von mat_sparse_matrix_matrix_vektor_product
c                statt mat_sparse_matrix_mult_vektor in tr_randomization und
c                tr_acc_randomization
c       28.02.00 tr_adaptive_acc_randomization eingef�hrt
c       21.07.11 tr_acc_randomization Vektor ph enthaelt bei Terminierung
c                den unormierten akkumulierten Vektor
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/

#include <math.h>

#include "hc_cons.h"
#include "ememory.h"
#include "mat_int_vektor.h"
#include "mat_double_vektor.h"
#include "mat_row.h"
#include "mat_sparse_matrix.h"
#include "transient.h"

#define DETECT_STATIONARY FALSE
#define STAT_EPSILON 1.0e-8

#define TRC_TRANSIENT FALSE 

/**********************
tr_add_to_vector
Datum:
Autor:  Peter Buchholz
Input:  p Iterationsvektor
        pi Kummulationsvektor
        d  Skalar
Output: 
Side-Effects:
Description:
Die Funktion berechnet pi = pi + d * p
**********************/
#ifndef CC_COMP
void tr_add_to_vector (Double_vektor *p, Double_vektor *pi, double d) 
#else
void tr_add_to_vector (p, pi, d) 
  Double_vektor *p  ;
  Double_vektor *pi ;
  doubel         d  ; 
#endif
{
  int i ;

  if (p == NULL || pi == NULL || p->no_of_entries != pi->no_of_entries)
  {
    printf("Wrong parameter in tr_add_to_vector\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  for (i = 0; i < p->no_of_entries; i++)
    pi->vektor[i] += p->vektor[i] * d ;
} /* tr_add_to_vector */

/**********************
tr_poisson_params
Datum:
Autor:  Peter Buchholz
Input:  qt Wert alpha * Zeithorizont
        epsilon Fehlerschranke
Output: left Startpunkt der Iteration
        right Endpunkt der Iteration
        scale Skalierungsfaktor
        startval Initialer Wert
Side-Effects:
Description:
Die Funktion berechnet die Werte der Poisson Verteilung mit Parameter
qt in numerisch stabiler Weise. Dabei sind left und right die Iterationsgrenzen,
startval der initiale Werte und scale der Skalierungswert. 
Es gilt also : Poisson(k) = 0 falls k < left oder k > right und
ansonsten (startval * qt^(k-left) / (k - left)!) / scale 
**********************/
#ifndef CC_COMP
void tr_poisson_params (double qt, double epsilon, double *scale, 
                        double *startval, int *left, int *right) 
#else
void tr_poisson_params (qt, epsilon, scale, startval, left, right) 
  double  qt       ;
  double  epsilon  ;
  double *scale    ;
  double *startval ;
  int    *left     ;
  int    *right    ;
#endif
{
  int    i, j          ;
  double val, sum, eps ;

  /* Berechne den signifikaten Bereich links von qt */
  j = (int) qt ;
  i = j        ;
  sum = 1.0 ;
  val = 1.0 ;
  while (i > 0 && sum + val > sum)
  {
    val *= (double) i / qt ;
    sum += val ;
    i-- ;
  }
  *left = i ;
  *startval = val ;
  /* Berechne den signifikaten Bereich rechts von qt */
  val = 1.0 ;
  i   = j   ;
  while (val + sum > sum)
  {
    i++ ;
    val *= qt / (double) i ;
    sum += val ;
  }
  *right  = i   ;
  *scale  = sum ;

  eps = epsilon * sum  ;
  /* Rechts kann epsilon weggelassen werden */
  sum = 0.0 ;
  i = *right ;
  if (val > 0.0)
  while (sum + val < eps)
  {
    sum += val ;
    val *= (double) i / qt ;
    i-- ;
  }
  *right = i ;
  /* *scale -= sum ; */
} /* tr_poisson_params */

/**********************
tr_randomization
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor (initialer Vektor)
        ph Hilfsvektor
        pt Transienter Vektor
        alpha Randomisierungsparameter
        t Zeitintervall
        epsilon Fehlerschranke
Output: pt transienter Loesungsvektor zum Zeitpunkt t
        p auf 1.0 normierter Vektor pt.
Side-Effects:
Description:
Die Funktion berechnet den transienten Loesungsvktor zum Zeitpunkt t ausgehend
vom initialen Vektor p mittels Randomization. Dabei wird die Anzahl der Iterationen so
gewaehlt, dass der Fehler kleiner epsilon ist. Weiterhin wird der unormierte und der
auf 1.0 normierte Verteilungsvektor zurueckgegeben. 
**********************/
#ifndef CC_COMP
int tr_randomization 
      (Sparse_matrix *q, Double_vektor **p, Double_vektor **ph, Double_vektor **pt,
       double alpha, double t, double epsilon)
#else
int tr_randomization (q, p, ph, pt, alpha, t, epsilon)
  Sparse_matrix  *q       ;
  Double_vektor **p       ;
  Double_vektor **ph      ;
  Double_vektor **pt      ;
  double          alpha   ;
  double          t       ;
  double          epsilon ;
#endif
{
  int i, j, left, right ;
  double val, scale, beta, qt ;

  if (q == NULL || *p == NULL || *ph == NULL || *pt == NULL ||
      q->ord != (*p)->no_of_entries || q->ord != (*ph)->no_of_entries ||
      q->ord != (*pt)->no_of_entries                                    )
  {  
    printf("Wrong parameter in tr_randomization\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  qt = alpha * t ;
  /* printf("Alpha %.3e t %.3e at %.3e\n", alpha, t, qt) ; */
  if (qt == 0.0)
  {
    for (i = 0; i < (*pt)->no_of_entries; i++)
      (*pt)->vektor[i] = (*p)->vektor[i] ;
  }
  else
  {
    for (i = 0; i < (*pt)->no_of_entries; i++)
      (*pt)->vektor[i] = 0.0 ;
    tr_poisson_params (qt, epsilon, &scale, &val, &left, &right) ;
    i = 0 ;
    while (i <= right)
    {
      if (i >= left)
      {
        beta = val / scale ;
        tr_add_to_vector (*p, *pt, beta) ; 
        val *= qt / (double) (i + 1) ;
      }
      i++ ;
      if (i <= right)
      {
        mat_sparse_matrix_matrix_vektor_product (q, *p, *ph) ;
#if ADAPTIVE_COMP
  mat_double_vektor_normalise(*ph) ;
#endif
        for (j = 0; j < (*p)->no_of_entries; j++)
          (*p)->vektor[j] = (*ph)->vektor[j] ;
      }
    }
  }
  val = 0.0 ;
  for (i = 0; i < (*pt)->no_of_entries; i++)
  {
    val += (*pt)->vektor[i] ;
    (*p)->vektor[i] = (*pt)->vektor[i] ;
  }
  if (fabs(1.0 -val) > 1.0e-3)
  {
    val = 1.0 / val ;
    for (i = 0; i < (*pt)->no_of_entries; i++)
      (*p)->vektor[i] *= val ;
  }
  return(right) ;
} /* tr_randomization */
 
/**********************
tr_acc_randomization
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor (initialer Vektor)
        ph Hilfsvektor
        pt Transienter Vektor
        pl akkumulierter Vektor
        alpha Randomisierungsparameter
        t Zeitintervall
        epsilon Fehlerschranke
Output: pt transienter Loesungsvektor zum Zeitpunkt t
        p auf 1.0 normierter Vektor pt
        pl Vektor der mittlere Zustandsverweilzeiten im Intervall [0,t) 
        ph unormierter Vektor pl
Side-Effects:
Description:
Die Funktion berechnet den transienten Loesungsvktor zum Zeitpunkt t und den
Vektor der mittleren Zustandsverweilzeiten im Intervall [0,t) ausgehend
vom initialen Vektor p mittels Randomization. Dabei wird die Anzahl der Iterationen so
gewaehlt, dass der Fehler kleiner epsilon ist. Weiterhin wird der unormierte und der
auf 1.0 normierte Verteilungsvektor zurueckgegeben. 
**********************/
#ifndef CC_COMP
void tr_acc_randomization 
      (Sparse_matrix *q, Double_vektor **p, Double_vektor **ph, Double_vektor **pt,
       Double_vektor **pl, double alpha, double t, double epsilon)
#else
void tr_acc_randomization (q, p, ph, pt, pl, alpha, t, epsilon)
  Sparse_matrix  *q       ;
  Double_vektor **p       ;
  Double_vektor **ph      ;
  Double_vektor **pt      ;
  Double_vektor **pl      ;
  double          alpha   ;
  double          t       ;
  double          epsilon ;
#endif
{
  short n_iter=1 ;
  int i, j, left, right ;
  double val, scale, beta, qt, sum ;
#if DETECT_STATIONARY
  double n_sum, sum_t = 0.0  ;
  int k ;
#endif

#if TRC_TRANSIENT
  printf("tr_acc_randomization with alpha %.3e t %.3e\n", alpha, t) ;
  printf("Initial vector\n") ;
  mat_double_vektor_print_file(stdout, *p) ;
#endif

  if (q == NULL || *p == NULL || *ph == NULL || *pt == NULL ||
      q->ord != (*p)->no_of_entries  || q->ord != (*ph)->no_of_entries ||
      q->ord != (*pt)->no_of_entries || q->ord != (*pl)->no_of_entries   )
  {
    printf("Wrong parameter in tr_acc_randomization\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  qt = alpha * t ;
  if (qt == 0.0)
  {
    for (i = 0; i < (*pt)->no_of_entries; i++)
    {
      (*pt)->vektor[i] = (*p)->vektor[i] ;
      (*pl)->vektor[i] = (*p)->vektor[i] ;
    }
  }
  else
  {
    for (i = 0; i < (*pt)->no_of_entries; i++)
    {
      (*pt)->vektor[i] = 0.0 ;
      (*pl)->vektor[i] = 0.0 ;
    }
    tr_poisson_params (qt, epsilon, &scale, &val, &left, &right) ;
    i = 0 ;
    sum = 0.0 ;
    while (i <= right)
    {

      if (i >= left)
      {
        sum += val ;
        beta = val / scale ;
        tr_add_to_vector (*p, *pt, beta) ; 
        val *= qt / (double) (i + 1) ;
        /* Der Fall left = right muss gesondert behandelt werden !!!
         In diesem Fall gilt pl = t*p, da Vektor p mit hoher Wahrscheinlichkeit
         (> epsilon) das ganze Intervall beibehalten wird */
        if (right > left)
        {
          if (scale > sum)
          {
            beta = (1.0 - (sum / scale)) / alpha ;
            tr_add_to_vector (*p, *pl, beta) ;
#if DETECT_STATIONARY
  sum_t += beta ;
#endif
          }
        }
        else
          tr_add_to_vector (*p, *pl, t) ;
      }
      else
      {
        tr_add_to_vector (*p, *pl, 1.0 / alpha) ;
#if DETECT_STATIONARY
  sum_t += 1.0 / alpha ;
#endif
      }
      if (i <= right && n_iter > 0)
      {
        mat_sparse_matrix_matrix_vektor_product (q, *p, *ph) ;
#if DETECT_STATIONARY
  if (i > 0 && (i == 1 || i % 10 == 0) && i < right)
  {
    n_sum = 0.0 ;
    for (k = 0; k < (*p)->no_of_entries; k++)
      n_sum += fabs((*p)->vektor[k] - (*ph)->vektor[k]) ;
    if (n_sum < STAT_EPSILON)
    {
       tr_add_to_vector (*ph, *pt, (scale-sum)/scale) ;
       tr_add_to_vector (*ph, *pl, t - sum_t) ;
       i = right ;
    }
  }
#endif
        if (i < right)
        for (j = 0; j < (*p)->no_of_entries; j++)
          (*p)->vektor[j] = (*ph)->vektor[j] ;
      }
      i++ ;
    }
  }
  /* Normiere pt */
  val = 0.0 ;
  for (i = 0; i < (*pt)->no_of_entries; i++)
  {
    val += (*pt)->vektor[i] ;
    (*p)->vektor[i] = (*pt)->vektor[i] ;
  }
  if (fabs(val) < 1.0e-20)
  {
    printf("Transient vector too small cannot normalize set to zero\n") ;
    for (i = 0; i < (*p)->no_of_entries; i++)
      (*p)->vektor[i] = 0.0 ;
  }
  if (fabs(1.0 - val) > 1.0e-20)
  {
    val = 1.0 / val ;
    for (i = 0; i < (*p)->no_of_entries; i++)
      (*p)->vektor[i] *= val ;
  }
  /* Bestimme den Verteilungsvektor fuer das Intervall [0, t) */
  val = 0.0 ;
  for (i = 0; i < (*pl)->no_of_entries; i++) {
    val += (*pl)->vektor[i] ;
    (*ph)->vektor[i] = (*pl)->vektor[i] ;
  }
  if (fabs(val) < 1.0e-20)
  {
    printf("Accumulated vector too small cannot normalize set to zero\n") ;
    /* printf("val %.3e qt %.3e left %i right %i scale %.3e\n",
       val, qt, left, right, scale) ; */
    for (i = 0; i < (*pl)->no_of_entries; i++)
      (*pl)->vektor[i] = 0.0 ;
  }
  else
  {
    if (fabs(1.0 - val) > 1.0e-5)
    {
      val = 1.0 / val ;
      for (i = 0; i < (*pl)->no_of_entries; i++)
        (*pl)->vektor[i] *= val ;
    }
  }
#if TRC_TRANSIENT
  printf("Vector p\n") ;
  mat_double_vektor_print_file(stdout, *p) ;
  printf("Vector pl\n") ;
  mat_double_vektor_print_file(stdout, *pl) ;
#endif
} /* tr_acc_randomization */

/**********************
tr_adaptive_acc_randomization
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor (initialer Vektor)
        ph Hilfsvektor
        pt Transienter Vektor
        pl akkumulierter Vektor
        beta_sum Randomisierungsparameter
        beta Werte der einzelnen Matrizen
        t Zeitintervall
        epsilon Fehlerschranke
Output: pt transienter Loesungsvektor zum Zeitpunkt t
        p auf 1.0 normierter Vektor pt
        pl Vektor der mittlere Zustandsverweilzeiten im Intervall [0,t) 
Side-Effects:
Description:
Die Funktion berechnet den transienten Loesungsvktor zum Zeitpunkt t und den
Vektor der mittleren Zustandsverweilzeiten im Intervall [0,t) ausgehend
vom initialen Vektor p mittels Randomization �ber mehrere Matrizen, wie f�r
die adaptive Berechnung bei der hybriden SImulation ben�tigt. 
Dabei wird die Anzahl der Iterationen so gewaehlt, dass der Fehler 
kleiner epsilon ist. Weiterhin wird der unormierte und der
auf 1.0 normierte Verteilungsvektor zurueckgegeben. 
**********************/
#ifndef CC_COMP
void tr_adaptive_acc_randomization 
      (Sparse_matrix **q, int no_mat, Double_vektor **p, Double_vektor **ph, Double_vektor **pt,
       Double_vektor **pl, double beta_sum, double *beta, double t, double epsilon)
#else
void tr_adaptive_acc_randomization (q, no_mat, p, ph, pt, pl, beta_sum, beta, t, epsilon)
  Sparse_matrix **q       ;
  int             no_mat  ;
  Double_vektor **p       ;
  Double_vektor **ph      ;
  Double_vektor **pt      ;
  Double_vektor **pl      ;
  double          beta_sum;
  double         *beta    ;
  double          t       ;
  double          epsilon ;
#endif
{
  short n_iter=1 ;
  int i, j, k, left, right ;
  double val, scale, gamma, qt, sum, dh ;

#if DETECT_STATIONARY
  double n_sum, sum_t = 0.0  ;
#endif

#if TRC_TRANSIENT
  printf("tr_adaptive_acc_randomization with alpha %.3e t %.3e\n", beta_sum, t) ;
  printf("Initial vector\n") ;
  mat_double_vektor_print_file(stdout, *p) ;
#endif


  if (q[0] == NULL || *p == NULL || *ph == NULL || *pt == NULL ||
      q[0]->ord != (*p)->no_of_entries  || q[0]->ord != (*ph)->no_of_entries ||
      q[0]->ord != (*pt)->no_of_entries || q[0]->ord != (*pl)->no_of_entries   )
  {
    printf("Wrong parameter in tr_adaptive_acc_randomization\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  qt = beta_sum * t ;
  if (qt == 0.0)
  {
    for (i = 0; i < (*pt)->no_of_entries; i++)
    {
      (*pt)->vektor[i] = (*p)->vektor[i] ;
      (*pl)->vektor[i] = (*p)->vektor[i] ;
    }
  }
  else
  {
    for (i = 0; i < (*pt)->no_of_entries; i++)
    {
      (*pt)->vektor[i] = 0.0 ;
      (*pl)->vektor[i] = 0.0 ;
    }
    tr_poisson_params (qt, epsilon, &scale, &val, &left, &right) ;
    i = 0 ;
    sum = 0.0 ;
    while (i <= right)
    {

      if (i >= left)
      {
        sum += val ;
        gamma = val / scale ;
        tr_add_to_vector (*p, *pt, gamma) ; 
        val *= qt / (double) (i + 1) ;
        /* Der Fall left = right muss gesondert behandelt werden !!!
         In diesem Fall gilt pl = t*p, da Vektor p mit hoher Wahrscheinlichkeit
         (> epsilon) das ganze Intervall beibehalten wird */
        if (right > left)
        {
          if (scale > sum)
          {
            gamma = (1.0 - (sum / scale)) / beta_sum ;
            tr_add_to_vector (*p, *pl, gamma) ;
#if DETECT_STATIONARY
  sum_t += gamma ;
#endif
          }
        }
        else
          tr_add_to_vector (*p, *pl, t) ;
      }
      else
      {
        tr_add_to_vector (*p, *pl, 1.0 / beta_sum) ;
#if DETECT_STATIONARY
  sum_t += 1.0 / beta_sum ;
#endif
      }
      if (i <= right && n_iter > 0)
      {
        /* Zuerst die Matrix der lokalen Transitionen */
        mat_sparse_matrix_matrix_vektor_product (q[0], *p, *ph) ;
        dh = beta[0] / beta_sum ;
        if (dh + 0.1 * STAT_EPSILON < 1.0)
        for (j = 0; j < (*ph)->no_of_entries; j++)
          (*ph)->vektor[j] *= dh ;
	/* Nun die synchronisierten Matrizen, die bzgl. der Komponente 
           randomisiert werden */
	for (k = 1; k <= no_mat; k++)
	if (beta[k]/beta_sum > 1.0e-5 && q[k] != NULL)
	{
          dh = beta[k] / beta_sum ;
          for (j = 0; j < (*ph)->no_of_entries; j++)
	  if (q[k]->diagonals[j] == 0.0)
	    (*ph)->vektor[j] += dh * (*p)->vektor[j] ;
	}
        /* Nun noch den Vektor normalisieren */
        if (beta[0] + 0.1 * STAT_EPSILON  < beta_sum)
          mat_double_vektor_normalise(*ph) ;

#if DETECT_STATIONARY
  if (i > 0 && (i == 1 || i % 10 == 0) && i < right)
  {
    n_sum = 0.0 ;
    for (k = 0; k < (*p)->no_of_entries; k++)
      n_sum += fabs((*p)->vektor[k] - (*ph)->vektor[k]) ;
    if (n_sum < STAT_EPSILON)
    {
       tr_add_to_vector (*ph, *pt, (scale-sum)/scale) ;
       tr_add_to_vector (*ph, *pl, t - sum_t) ;
       i = right ;
    }
  }
#endif
        if (i < right)
        for (j = 0; j < (*p)->no_of_entries; j++)
          (*p)->vektor[j] = (*ph)->vektor[j] ;
      }
      i++ ;
    }
  }
  /* Normiere pt */
  val = 0.0 ;
  for (i = 0; i < (*pt)->no_of_entries; i++)
  {
    val += (*pt)->vektor[i] ;
    (*p)->vektor[i] = (*pt)->vektor[i] ;
  }
  if (fabs(val) < 1.0e-20)
  {
    printf("Transient vector too small cannot normalize set to zero\n") ;
    for (i = 0; i < (*p)->no_of_entries; i++)
      (*p)->vektor[i] = 0.0 ;
  }
  if (fabs(1.0 - val) > 1.0e-20)
  {
    val = 1.0 / val ;
    for (i = 0; i < (*p)->no_of_entries; i++)
      (*p)->vektor[i] *= val ;
  }
  /* Bestimme den Verteilungsvektor fuer das Intervall [0, t) */
  val = 0.0 ;
  for (i = 0; i < (*pl)->no_of_entries; i++)
    val += (*pl)->vektor[i] ;
  if (fabs(val) < 1.0e-20)
  {
    printf("Accumulated vector too small cannot normalize\nSet to zero\n") ;
    printf("val %.3e qt %.3e left %i right %i scale %.3e\n",
           val, qt, left, right, scale) ;
    for (i = 0; i < (*pl)->no_of_entries; i++)
      (*pl)->vektor[i] = 0.0 ;
  }
  else
  {
    if (fabs(1.0 - val) > 1.0e-5)
    {
      val = 1.0 / val ;
      for (i = 0; i < (*pl)->no_of_entries; i++)
        (*pl)->vektor[i] *= val ;
    }
  }
#if TRC_TRANSIENT
  printf("Vector p\n") ;
  mat_double_vektor_print_file(stdout, *p) ;
  printf("Vector pl\n") ;
  mat_double_vektor_print_file(stdout, *pl) ;
#endif
} /* tr_adaptive_acc_randomization */



/**********************
tr_gen_step_matrix
Datum:
Autor:  Peter Buchholz
Input:  ti Intervallbreite, fuer die Q[ti] geenriert werden soll
        alpha Randomisierungsrate
        q Generatormatrix
Output: qt Matrix Q[ti]
        ql Matrix \int Q[ti]
Side-Effects:
Description:
Die Funktion berechnet Matrix qt = exp(q*ti) und die Matrix der akkumulierten Zeit in
den einzelnen Zust�nden. 
**********************/
#ifndef CC_COMP
void tr_gen_step_matrix
   (double ti, double alpha, Sparse_matrix *q, Sparse_matrix *qt, Sparse_matrix *ql) 
#else
void tr_gen_step_matrix(ti, alpha, q, qv, ql)
  double         ti    ;
  double         alpha ;
  Sparse_matrix *q     ;
  Sparse_matrix *qt    ;
  Sparse_matrix *ql    ;
#endif
{
  int i, j, k ;
  Double_vektor *p, *ph, *pt, *pl ;

  p  = mat_double_vektor_new (q->ord) ;
  ph = mat_double_vektor_new (q->ord) ;
  pt = mat_double_vektor_new (q->ord) ;
  pl = mat_double_vektor_new (q->ord) ;
  for (i = 0; i < q->ord; i++) 
  {
    p->vektor[i] = 1.0 ;
    tr_acc_randomization (q, &p, &ph, &pt, &pl, alpha, ti, STAT_EPSILON) ;
    /* Transformiere den Vektor in eine Matrixzeile */
    k = 0 ;
    qt->diagonals[i] = p->vektor[i] ;
    p->vektor[i] = 0.0 ;
    /* Generiere pt */
    for (j = 0; j < q->ord; j++)
    if (p->vektor[j] > 0.0)
      k++ ;
    (qt->rowind[i]).colind = (size_t *) ecalloc (k, sizeof(size_t))    ;
    (qt->rowind[i]).val = (double *) ecalloc (k, sizeof(double)) ;
    (qt->rowind[i]).no_of_entries = k ;
    k = 0 ;
    for (j = 0; j < q->ord; j++)
    if (p->vektor[j] > 0.0)
    {
      (qt->rowind[i]).colind[k] = j ;
      (qt->rowind[i]).val[k] = p->vektor[j] ;
      p->vektor[j] = 0.0 ;
      k++ ;
      if (k >= (qt->rowind[i]).no_of_entries)
        break ; 
    }
    /* Generiere ql */
    k = 0 ;
    ql->diagonals[i] = pl->vektor[i] * ti ;
    pl->vektor[i] = 0.0 ;
    for (j = 0; j < q->ord; j++)
    if (pl->vektor[j] > 0.0)
      k++ ;
    (ql->rowind[i]).colind = (size_t *) ecalloc (k, sizeof(size_t));
    (ql->rowind[i]).val = (double *) ecalloc (k, sizeof(double)) ;
    (ql->rowind[i]).no_of_entries = k ;
    k = 0 ;
    for (j = 0; j < q->ord; j++)
    if (pl->vektor[j] > 0.0)
    {
      (ql->rowind[i]).colind[k] = j ;
      (ql->rowind[i]).val[k] = pl->vektor[j] * ti ;
      pl->vektor[j] = 0.0 ;
      k++ ;
      if (k >= (ql->rowind[i]).no_of_entries)
        break ; 
    }
  }
  mat_double_vektor_free(p)  ;
  mat_double_vektor_free(ph) ;
  mat_double_vektor_free(pt) ;
  mat_double_vektor_free(pl) ;
} /* tr_gen_step_matrix */


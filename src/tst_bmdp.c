/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c (C) Copyright 2015 by Peter Buchholz
c
c
c     All Rights Reserved
c
c bmdp_main.c
c
c Hauptprogram fuer die Analyse von BMDPs
c
c
c       Datum           Modifikationen
c       ----------------------------------------------------------
c       01.03.15 Datei angelegt
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/

//#include <tix.h>
#include "tst_bmdp.h"
#include "bmdp_pareto_evo.h"
#include "bmdp_weight_func.h"
#include "continuous_weight_func.h"
#include "ctmdp_func.h"
#include "utils.h"
#include <error.h>
#include <omp.h>

/**********************
bmdp_read_one_reward_vector
Datum: 01.03.15
Autor:  Peter Buchholz
Input:  fp File to read from
        size required size of the matrix, if < 0 no size required
Output: return value pointer to the vector

Side-Effects:
Description:
The function reads a rewrd vector from a diagonal matrix in the file.
***********************************************************************/
Double_vektor *bmdp_read_one_reward_vector(FILE *fp, int size)
{
    int i, j, k = 0;
    Double_vektor *vec;

    /* Read the number of partitions and the boundaries, the last is the size of
     * the matrix */
    i = read_integer(fp);
    for (j = 0; j <= i; j++)
        k = read_integer(fp);
    if (size < 0 && k != i) {
        printf("Inconsistent size of vectors  (%i - %i)\n", k, size);
        exit(-1);
    }
    vec = mat_double_vektor_new(size);
    for (i = 0; i < size; i++) {
        j = read_integer(fp);
        if (j != 0) {
            printf("Reward vector not in diagonal matrix: column %d (value "
                   "%f)!\nSTOP EXECUTION",
                   j, read_double(fp));
            exit(-1);
        }
        vec->vektor[i] = read_double(fp);
    }
    return (vec);
} /* bmdp_read_one_reward_vector */

/**********************
bmdp_check_rewards
Datum: 01.03.15
Autor:  Peter Buchholz
Input:  rew_low, rew_avg, rew_up lower, average and upper reward vectors
Output: return value NO ERROR values are constent
                     INPUT_ERRO else

Side-Effects:
Description:
The function checks whether rew_low <= rew_avg <= rew_up.
***********************************************************************/
int bmdp_check_rewards(Double_vektor *rew_low, Double_vektor *rew_avg, Double_vektor *rew_up)
{
    int i, ord = rew_low->no_of_entries;

    for (i = 0; i < ord; i++) {
        if (rew_low->vektor[i] >= rew_avg->vektor[i] + BMDP_MIN_VAL
            || rew_avg->vektor[i] >= rew_up->vektor[i] + BMDP_MIN_VAL) {
            printf("Reward low %.le avg %.le up %.le\n", rew_low->vektor[i], rew_avg->vektor[i],
                   rew_up->vektor[i]);
            return (INPUT_ERROR);
        }
    }
    return (NO_ERROR);
} /* bmdp_check_rewards */

/**********************
bmdp_read_one_matrix
Datum: 01.03.15
Autor:  Peter Buchholz
Input:  fp File to read from
        size required size of the matrix, if < 0 no size required
Output: return value pointer to the matrix

Side-Effects:
Description:
The function reads sparse matrix form the file.
***********************************************************************/
Sparse_matrix *bmdp_read_one_matrix(FILE *fp, int size)
{
    int i, j, k = 0;
    Sparse_matrix *mat;

    /* Read the number of partitions and the boundaries, the last is the size of
     * the matrix */
    i = read_integer(fp);
    for (j = 0; j <= i; j++)
        k = read_integer(fp);
    if (size < 0)
        size = k;
    else {
        if (size > 0 && k != size) {
            printf("Inconsistent size of matrices (%i - %i)\n", k, size);
            exit(-1);
        }
    }
    mat = mat_sparse_matrix_new(size);
    mat_sparse_matrix_input(fp, mat, TRUE);
    mat_sparse_matrix_sort_reduce_elems(mat);
    return (mat);
} /* bmdp_read_one_matrix */

/**********************
bmdp_check_matrices
Datum: 01.03.15
Autor:  Peter Buchholz
Input:  mat_low, mat_avg, mat_up lower, average and upper matrices
Output: return value NO ERROR matrices are consistent
                     INPUT_ERRO else

Side-Effects:
Description:
The function checks whetehr mat_low <= mat_avg <= mat_up.
***********************************************************************/
int bmdp_check_matrices(Sparse_matrix *mat_low, Sparse_matrix *mat_avg, Sparse_matrix *mat_up)
{
    int i, j;
    double val;
    Double_vektor *rs;

    if (!(mat_sparse_matrix_is_smaller(mat_low, mat_avg, BMDP_MIN_VAL, TRUE))) {
        printf("Lower bound matrix larger than average matrix\n");
        return (INPUT_ERROR);
    }
    if (!(mat_sparse_matrix_is_smaller(mat_avg, mat_up, BMDP_MIN_VAL, TRUE))) {
        printf("Average matrix larger than upper bound matrix\n");
        return (INPUT_ERROR);
    }
    rs = mat_double_vektor_new(mat_avg->ord);
    mat_sparse_matrix_row_sum(mat_avg, rs, TRUE);
    for (i = 0; i < mat_avg->ord; i++) {
        if (fabs(rs->vektor[i] - 1.0) > BMDP_MIN_VAL && fabs(rs->vektor[i]) > BMDP_MIN_VAL) {
            if (fabs(rs->vektor[i] - 1.0) > 1.0e-5) {
                mat_double_vektor_free(rs);
                printf("Average matrix not stochastic (row sum row %i: %.5e\n", i, rs->vektor[i]);
#if BMDP_TRACE_LEVEL
                printf("Row %i with row sum %.5e\n", i, rs->vektor[i]);
                printf("Diagonal: %.3e \n", mat_avg->diagonals[i]);
                for (j = 0; j < (mat_avg->rowind[i]).no_of_entries; j++) {
                    printf("%i: %.3e ", (mat_avg->rowind[i]).colind[j],
                           (mat_avg->rowind[i]).val[j]);
                    if (j > 0 && (j + 1) % 5 == 0)
                        printf("\n");
                }
                printf("val\n");
#endif
                return (INPUT_ERROR);
            } else {
                if (fabs(rs->vektor[i]) > BMDP_MIN_VAL) {
                    /* normalize row */
                    if ((1.0 - rs->vektor[i]) > 1.0e-5)
                        printf("Normalize row with row %i 1-sum %.3e\n", i, 1.0 - rs->vektor[i]);
                    val = 1.0 / rs->vektor[i];
                    mat_avg->diagonals[i] *= val;
                    for (j = 0; j < (mat_avg->rowind[i]).no_of_entries; j++)
                        (mat_avg->rowind[i]).val[j] *= val;
                }
            }
        }
    }
    mat_double_vektor_free(rs);

    /* We need identical non-zero structures, this has to be checked
       and if necessary induced by adding zero elements */
    for (i = 0; i < mat_up->ord; i++) {
        /* Check if the upper bound contains more non-zeros than the average values
         */
        if ((mat_up->rowind[i]).no_of_entries > (mat_avg->rowind[i]).no_of_entries) {
            j = 0;
            while (j < (mat_up->rowind[i]).no_of_entries) {
                if (j < (mat_avg->rowind[i]).no_of_entries
                    && (mat_up->rowind[i]).colind[j] == (mat_avg->rowind[i]).colind[j])
                    j++;
                else {
                    mat_sparse_matrix_add_elem(mat_avg, i, (mat_up->rowind[i]).colind[j], 0.0,
                                               TRUE);
                    j = 0;
                }
            } /* while j in row i */
        } /* Add elements to the average values */
        /* Check if the average values contain more non-zeros than the lower bound
         */
        if ((mat_avg->rowind[i]).no_of_entries > (mat_low->rowind[i]).no_of_entries) {
            j = 0;
            while (j < (mat_avg->rowind[i]).no_of_entries) {
                if (j < (mat_low->rowind[i]).no_of_entries
                    && (mat_avg->rowind[i]).colind[j] == (mat_low->rowind[i]).colind[j])
                    j++;
                else {
                    mat_sparse_matrix_add_elem(mat_low, i, (mat_avg->rowind[i]).colind[j], 0.0,
                                               TRUE);
                    j = 0;
                }
            } /* while j in row i */
        } /* Add elements to the average values */
    } /* for i = 0  */

    return (NO_ERROR);
} /* bmdp_check_matrices */

/**********************
bmdp_read_process
Datum: 01.03.15
Autor:  Peter Buchholz
Input:  base_name Name of teh file containing the BMDP description
Output: ord size of teh state space
        act number of actions
        m_low, m_avg, m_up lower bound, average, upper bound MDP matrices
        r_low, r_avg, r_up lower bound, average, upper bound reward vectors
Side-Effects:
Description:
The function reads a BMDP description form a file and checks teh consistency of
matrices
and reward vectors.
***********************************************************************/
int bmdp_read_process(char *base_name, size_t *size, size_t *act, Mdp_matrix **m_low,
                      Double_vektor ***r_low, Mdp_matrix **m_avg, Double_vektor ***r_avg,
                      Mdp_matrix **m_up, Double_vektor ***r_up)
{
    int ord, no_act, no_mat, i;
    Double_vektor **rew_low, **rew_avg, **rew_up;
    Sparse_matrix **mat_low, **mat_avg, **mat_up;
    Mdp_matrix *mdp_low, *mdp_avg, *mdp_up;
    FILE *fp;
    char *fil_name = (char *)ecalloc(100, sizeof(char));

    /* Open the input file */
    fil_name = strcpy(fil_name, base_name);
    //fil_name = strcat(fil_name, ".mdpu");
    if ((fp = fopen(fil_name, "r")) == NULL) {
        fprintf(stderr, " Can't open input file with matrices (%s)\n", fil_name);
        exit(-1);
    }
    /* Read the matrices and reward vectors */
    no_mat = read_integer(fp);
    /* The number of actions correpsonds to teh number of matrices divied by 6 */
    no_act = no_mat / 6;
    /* Allocate space for matrices and vectors */
    rew_low = (Double_vektor **)ecalloc(no_act, sizeof(Double_vektor *));
    rew_avg = (Double_vektor **)ecalloc(no_act, sizeof(Double_vektor *));
    rew_up = (Double_vektor **)ecalloc(no_act, sizeof(Double_vektor *));
    mat_low = (Sparse_matrix **)ecalloc(no_act, sizeof(Sparse_matrix *));
    mat_avg = (Sparse_matrix **)ecalloc(no_act, sizeof(Sparse_matrix *));
    mat_up = (Sparse_matrix **)ecalloc(no_act, sizeof(Sparse_matrix *));
    ord = -1;
    /* For every action the lower, average and upper matrices and reweard vectors
     * have to be read */
    for (i = 0; i < no_act; i++) {
        /* Read the lower, average and upper matrix */
        mat_low[i] = bmdp_read_one_matrix(fp, ord);
#if BMDP_TEST_FOR_ABSORBING
        for (j = 0; j < mat_low[i]->ord; j++) {
            if (fabs(mat_low[i]->diagonals[j] - 1.0) < 1.0e-6
                && (mat_low[i]->rowind[j]).no_of_entries == 0)
                printf("Row %i in lower bound matrix for action %i is absorbing\n", j, i);
        }
#endif
        /* if ord is not set, set it to the size of teh matrix */
        if (ord < 0)
            ord = mat_low[i]->ord;
        mat_avg[i] = bmdp_read_one_matrix(fp, ord);
#if BMDP_TEST_FOR_ABSORBING
        for (j = 0; j < mat_avg[i]->ord; j++) {
            if (fabs(mat_avg[i]->diagonals[j] - 1.0) < 1.0e-6
                && (mat_avg[i]->rowind[j]).no_of_entries == 0)
                printf("Row %i in average matrix for action %i is absorbing\n", j, i);
        }
#endif
        mat_up[i] = bmdp_read_one_matrix(fp, ord);
#if BMDP_TEST_FOR_ABSORBING
        for (j = 0; j < mat_up[i]->ord; j++) {
            if (fabs(mat_up[i]->diagonals[j] - 1.0) < 1.0e-6
                && (mat_up[i]->rowind[j]).no_of_entries == 0)
                printf("Row %i in upper bound matrix for action %i is absorbing\n", j, i);
        }
#endif
        if (bmdp_check_matrices(mat_low[i], mat_avg[i], mat_up[i]) != NO_ERROR) {
            printf("Matrix bounds are not consistent for action %i\nSTOP EXECUTION\n", i);
            exit(-1);
        }
        /* Read the rewards */
        rew_low[i] = bmdp_read_one_reward_vector(fp, ord);
        rew_avg[i] = bmdp_read_one_reward_vector(fp, ord);
        rew_up[i] = bmdp_read_one_reward_vector(fp, ord);
#if BMDP_TRACE_LEVEL
        printf("Matrices for action %i\n", i);
        printf("Lower bound\n");
        mat_sparse_matrix_prt_small_matrix(mat_low[i]);
        printf("Average\n");
        mat_sparse_matrix_prt_small_matrix(mat_avg[i]);
        printf("Upper bound\n");
        mat_sparse_matrix_prt_small_matrix(mat_up[i]);
#endif
        /*Check the rewards */
        if (bmdp_check_rewards(rew_low[i], rew_avg[i], rew_up[i]) != NO_ERROR) {
            printf("Reward bounds are not consistent for action %i\nSTOP EXECUTION\n", i);
            exit(-1);
        }
    } /* for i = 0, ..., no_act-1 */
    /* Close the input file */
    fclose(fp);
    /* Transform the matrices in MDP Matrices */
    mdp_low = mdp_matrix_generate_discrete(no_act, ord, mat_low);
    mdp_avg = mdp_matrix_generate_discrete(no_act, ord, mat_avg);
    mdp_up = mdp_matrix_generate_discrete(no_act, ord, mat_up);
    /* Deallocat ememory */
    for (i = 0; i < no_act; i++) {
        efree(mat_low[i]->rowind);
        efree(mat_low[i]->diagonals);
        efree(mat_low[i]);
        efree(mat_avg[i]->rowind);
        efree(mat_avg[i]->diagonals);
        efree(mat_avg[i]);
        efree(mat_up[i]->rowind);
        efree(mat_up[i]->diagonals);
        efree(mat_up[i]);
    }
    efree(mat_low);
    efree(mat_avg);
    efree(mat_up);
    efree(fil_name);
/* If two dimensional optimization is used reset some rewards */
#if BMDP_RESET_MIN
    for (i = 0; i < no_act; i++)
        mat_double_vektor_reset(rew_low[i]);
#endif
#if BMDP_RESET_AVG
    for (i = 0; i < no_act; i++)
        mat_double_vektor_reset(rew_avg[i]);
#endif
#if BMDP_RESET_MAX
    for (i = 0; i < no_act; i++)
        mat_double_vektor_reset(rew_up[i]);
#endif
    /* Set the return values */
    *size = ord;
    *act = no_act;
    *m_low = mdp_low;
    *m_avg = mdp_avg;
    *m_up = mdp_up;
    *r_low = rew_low;
    *r_avg = rew_avg;
    *r_up = rew_up;
    return (NO_ERROR);
} /* bmdp_read_process */

/**********************
bmdp_prt_gain
Datum: 13.03.15
Autor:  Peter Buchholz
Input:  base_name prefix of teh file name
        post_name postfix of teh file name
        ord number of lemenets
        hh gain vector

Output:
Side-Effects:
Description:
The function writes a gain vector in a file.
***********************************************************************/
int bmdp_prt_gain(char *base_name, char *post_name, int ord, double *hh)
{
    FILE *fp;
    char *fil_name = (char *)ecalloc(100, sizeof(char));
    int i;

    sprintf(fil_name, "%s.gain_%s", base_name, post_name);
    if ((fp = fopen(fil_name, "w")) == NULL) {
        fprintf(stderr, " Can't open output file for the gain vector (%s)\n", fil_name);
        return (-1);
    }
    fprintf(fp, "# number of elements\n%i\n", ord);
    for (i = 0; i < ord; i++) {
        fprintf(fp, "%.5e\n", hh[i]);
    }
    fclose(fp);
    return (NO_ERROR);
} /* bmdp_prt_gain */

/**********************
bmdp_prt_gain_and_policy
Datum: 02.03.15
Autor:  Peter Buchholz
Input:  base_name prefix of teh file name
        post_name postfix of teh file name
        ord number of lemenets
        hh gain vector
        ind policy
Output:
Side-Effects:
Description:
The function writes a gain vector and a policy in files.
***********************************************************************/
int bmdp_prt_gain_and_policy(char *base_name, char *post_name, int ord, double *hh, size_t *ind)
{
    FILE *fp;
    char *fil_name = (char *)ecalloc(100, sizeof(char));
    int i;

    sprintf(fil_name, "%s.gain_%s", base_name, post_name);
    if ((fp = fopen(fil_name, "w")) == NULL) {
        fprintf(stderr, " Can't open output file for the gain vector (%s)\n", fil_name);
        return (-1);
    }
    fprintf(fp, "# number of elements\n%i\n", ord);
    for (i = 0; i < ord; i++) {
        fprintf(fp, "%.5e\n", hh[i]);
    }
    fclose(fp);
    sprintf(fil_name, "%s.policy_%s", base_name, post_name);
    if ((fp = fopen(fil_name, "w")) == NULL) {
        fprintf(stderr, " Can't open output file for the policy (%s)\n", fil_name);
        return (-1);
    }
    fprintf(fp, "# number of elements\n%i\n", ord);
    for (i = 0; i < ord; i++)
        fprintf(fp, "%zu\n", ind[i]);
    fclose(fp);
    return (NO_ERROR);
} /* bmdp_prt_gain_and_policy */

/**********************
bmdp_read_policy
Datum: 13.03.15
Autor:  Peter Buchholz
Input:  base_name prefix of the file name
Output:
Side-Effects:
Description:
The function read a policy from the file "base_name.policy".
***********************************************************************/
size_t *bmdp_read_policy(char *base_name, size_t ord)
{
    FILE *fp;
    char *fil_name = (char *)ecalloc(100, sizeof(char));
    size_t *ind, i;

    sprintf(fil_name, "%s.policy", base_name);
    if ((fp = fopen(fil_name, "r")) == NULL) {
        fprintf(stderr, " Can't open input file with policy (%s)\nSTOP EXECUTION\n", fil_name);
        exit(-1);
    }
    i = read_integer(fp);
    if (i != ord) {
        printf("Dimension of policy %zu required dimension %zu\nSTOPE EXECUTION\n", i, ord);
        exit(-1);
    }
    ind = (size_t *)ecalloc(ord, sizeof(size_t));
    for (i = 0; i < ord; i++)
        ind[i] = (size_t)read_integer(fp);
    fclose(fp);
    return (ind);
} /* bmdp_read_policy */

/**********************
main # Hauptprogramm steuert den gesamten Ablauf des Verfahrens.
Datum:
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/

int main(int argc, char *argv[])
{
    char *base_name, *mstring;
    double starttime, comptime, gamma, *hh;
    short comp_max = TRUE, comp_stat = TRUE;
    size_t i, act, ord, method, no_policies, *ind;
    Double_vektor **rew_low, **rew_avg, **rew_up;
    Mdp_matrix *mdp_low, *mdp_avg, *mdp_up;
    Pareto_vector_element **my_results;
    Pareto_vector_element *elems;
    double max_weight, min_weight;
    List **solutions;

    /* perform some OpenMP calculations... */
    printf("[OpenMP] %d devices, %d possible threads\n", omp_get_num_devices(),
           omp_get_max_threads());
    omp_set_num_threads(6);
    printf("[OpenMP] nested: %d, dynamic: %d\n", omp_get_nested(), omp_get_dynamic());
    omp_set_dynamic(1);
    /*omp_set_nested(1);*/

    stdlog_file(NULL); // "bmdp-analysis.log"
    stdlog_quiet(0);

    starttime = mat_timeused();
    if (argc <= 3) {
        printf("Call BMDPanalysis <name> <method> <gamma> (<comp_max> <comp_stat>)\n");
        printf("Available methods are %i TST_MAT. %i LOWER_VAL %i AVG_VAL %i "
               "UPPER_VAL %i COMP_GAIN %i COMP_PURE %i COMP_WEIGHT %i COMP_HULL %i "
               "COMP_FRONT %i COMP_PURE_BFS %i COMP_PURE_IPT %i COMP_PURE_EVO\n",
               BMDP_CHECK_MATRICES, BMDP_COMPUTE_MIN, BMDP_COMPUTE_AVG, BMDP_COMPUTE_MAX,
               BMDP_COMPUTE_REW, BMDP_COMPUTE_PURE, BMDP_COMPUTE_WEIGHT, BMDP_COMPUTE_HULL,
               BMDP_COMPUTE_FRONT, BMDP_COMPUTE_PUREBFS, BMDP_COMPUTE_PUREIPT,
               BMDP_COMPUTE_PUREEVO);
        exit(-1);
    }
    sscanf(argv[2], "%zi", &method);
    if (method >= CMDP_CHECK_MATRICES && method <= CMDP_TEST_ALL) {
        exit(cmdp_main(argc, argv));
    }
    if (method >= SMDP_OPT && method < BMDP_CHECK_MATRICES) {
        exit(smdp_main(argc, argv));
    }
    if (method < BMDP_CHECK_MATRICES || method > BMDP_COMPUTE_PUREEVO
        || (method < BMDP_COMPUTE_PUREEVO && method > BMDP_COMPUTE_PUREIPT)) {
        printf("Only methods %i .. %i, %i are implemented\nSTOP EXECUTION\n", BMDP_CHECK_MATRICES,
               BMDP_COMPUTE_PUREBFS, BMDP_COMPUTE_PUREEVO);
        exit(-1);
    }
    sscanf(argv[3], "%le", &gamma);
    if (gamma < 0.0 || gamma > 1.0 - BMDP_MIN_VAL) {
        printf("gamma (%.4f) from [0,1) required\nSTOP EXECUTION\n", gamma);
        exit(EXIT_FAILURE);
    }
    int extra_arg = 4;
    if (method == BMDP_COMPUTE_WEIGHT) {
        // NOTE this is actually bad programming style.
        extra_arg = 6;
        if (argc <= 5) {
            printf("method %i expects at least two arguments, min_weight and "
                   "max_weight\nSTOP EXECUTION\n",
                   BMDP_COMPUTE_WEIGHT);
            exit(EXIT_FAILURE);
        }
        sscanf(argv[4], "%le", &min_weight);
        sscanf(argv[5], "%le", &max_weight);
    }

    if (argc > extra_arg) {
        sscanf(argv[extra_arg], "%zi", &comp_max);
        if (comp_max == 0)
            comp_max = FALSE;
        else
            comp_max = TRUE;
    }
    extra_arg++;
    if (argc > extra_arg) {
        sscanf(argv[extra_arg], "%zi", &comp_stat);
        if (comp_stat == 0)
            comp_stat = FALSE;
        else
            comp_stat = TRUE;
    }

    printf("BMDP analysis with ");
    if (comp_max)
        printf("maximum ");
    else
        printf("minimum ");
    printf("with ");
    if (method == BMDP_CHECK_MATRICES)
        mstring = "matrix check";
    if (method == BMDP_COMPUTE_MIN)
        mstring = "optimization according to the minimum";
    if (method == BMDP_COMPUTE_MAX)
        mstring = "optimization according to the maximum";
    if (method == BMDP_COMPUTE_AVG)
        mstring = "optimization according to the average";
    if (method == BMDP_COMPUTE_REW)
        mstring = "compute rewards for policy vector";
    if (method == BMDP_COMPUTE_PURE)
        mstring = "compute Pareto optimal policies";
    if (method == BMDP_COMPUTE_WEIGHT)
        mstring = "compute a weight-optimal policy";
    if (method == BMDP_COMPUTE_HULL)
        mstring = "compute all weight-optimal policies";
    if (method == BMDP_COMPUTE_FRONT)
        mstring = "compute the Pareto front (with possibly nonstationary policies)";
    if (method == BMDP_COMPUTE_PUREBFS)
        mstring = "compute Pareto optimal policies with the BFS method";
    if (method == BMDP_COMPUTE_PUREIPT)
        mstring = "Pareto optimal policies by the IPT heuristic";
    if (method == BMDP_COMPUTE_PUREEVO)
        mstring = "compute Pareto optimal policies with an evolutionary strategy";

    printf("%s with gamma %le\n", mstring, gamma);

    base_name = argv[1];
    if (bmdp_read_process(base_name, &ord, &act, &mdp_low, &rew_low, &mdp_avg, &rew_avg, &mdp_up,
                          &rew_up)
        == NO_ERROR)
        printf("Everything read after %.3e seconds Number of states %zu number of "
               "actions %zu)\n",
               mat_timeused() - starttime, ord, act);
    else {
        printf("Error reading input file\nSTOP EXECUTION\n");
    }
#if BMDP_TRACE_LEVEL
    mdp_matrix_prt(mdp_avg);
#endif

    const int name_len = strlen(base_name) + 5;
    char *name = (char *)ecalloc(name_len, sizeof(char));
    sprintf(name, "%s_%3zu", base_name, method);

    comptime = mat_timeused();
    switch (method) {
    case BMDP_COMPUTE_AVG:
        printf("Compute average reward\n");
        ind = (size_t *)ecalloc(mdp_avg->ord, sizeof(size_t));
        if (ord < BMDP_SIZE_DIRECT)
            hh = bmdp_policy_iteration(mdp_avg, rew_avg, ind, BMDP_MAX_ITER, BMDP_MAX_TIME,
                                       BMDP_LO_EPS, gamma, comp_max, comp_stat);
        else
            hh = bmdp_precond_iteration(mdp_avg, rew_avg, ind, BMDP_MAX_ITER, BMDP_MAX_TIME,
                                        BMDP_LO_EPS, 300, 1.0e-8, BO_GMRES_ILU0, gamma, comp_max,
                                        comp_stat);
        printf("Time for result computation %.3e sec\n", mat_timeused() - comptime);
        mstring = "avg";
        bmdp_prt_gain_and_policy(base_name, mstring, mdp_avg->ord, hh, ind);
        break;

    case BMDP_COMPUTE_MIN:
        printf("Compute worst case reward\n");
        ind = (size_t *)ecalloc(mdp_avg->ord, sizeof(size_t));
        hh = bmdp_compute_extreme_case(mdp_low, mdp_up, rew_low, ind, BMDP_MAX_ITER, BMDP_MAX_TIME,
                                       BMDP_LO_EPS, 300, 1.0e-8, BO_GMRES_ILU0, gamma, TRUE,
                                       comp_max, comp_stat);
        printf("Time for result computation %.3e sec\n", mat_timeused() - comptime);
        mstring = "min";
        bmdp_prt_gain_and_policy(base_name, mstring, mdp_avg->ord, hh, ind);
        break;

    case BMDP_COMPUTE_MAX:
        printf("Compute best case reward\n");
        ind = (size_t *)ecalloc(mdp_avg->ord, sizeof(size_t));
        hh = bmdp_compute_extreme_case(mdp_low, mdp_up, rew_up, ind, BMDP_MAX_ITER, BMDP_MAX_TIME,
                                       BMDP_LO_EPS, 300, 1.0e-8, BO_GMRES_ILU0, gamma, FALSE,
                                       comp_max, comp_stat);
        printf("Time for result computation %.3e sec\n", mat_timeused() - comptime);
        mstring = "max";
        bmdp_prt_gain_and_policy(base_name, mstring, mdp_avg->ord, hh, ind);
        break;

    case BMDP_COMPUTE_REW:
        printf("Compute gain vectors for policy\n");
        ind = bmdp_read_policy(base_name, (unsigned)mdp_low->ord);
        mstring = "min";
        if (comp_stat)
            printf("Minimal ");
        hh = bmdp_evaluate_minmax_policy(mdp_low, mdp_up, ind, rew_low, gamma, TRUE, comp_stat);
        bmdp_prt_gain(base_name, mstring, (unsigned)mdp_low->ord, hh);
        mstring = "avg";
        if (comp_stat)
            printf("Average ");
        hh = bmdp_evaluate_avg_policy(mdp_avg, ind, rew_avg, gamma, comp_stat);
        bmdp_prt_gain(base_name, mstring, (unsigned)mdp_avg->ord, hh);
        mstring = "max";
        if (comp_stat)
            printf("Maximal ");
        hh = bmdp_evaluate_minmax_policy(mdp_low, mdp_up, ind, rew_up, gamma, FALSE, comp_stat);
        bmdp_prt_gain(base_name, mstring, (unsigned)mdp_up->ord, hh);
        break;

    case BMDP_COMPUTE_PURE:
        printf("Compute pure Pareto optimal policies\n");
        my_results = bmdp_pure_opt(mdp_low, mdp_avg, mdp_up, rew_low, rew_avg, rew_up, gamma,
                                   comp_max, &no_policies);
        if (comp_stat)
            bmpar_stationary_analysis(mdp_low, mdp_avg, mdp_up, gamma, my_results, no_policies);
        bmpar_print_pareto_results(name, my_results, no_policies, BMDP_PARETO_PRT);
        bmpar_print_pareto_values(name, my_results, no_policies);
        break;

    case BMDP_COMPUTE_PUREBFS:
        printf("Compute pure Pareto optimal policies with the BFS method\n");
        my_results = bmdp_pure_opt_bfs(mdp_low, mdp_avg, mdp_up, rew_low, rew_avg, rew_up, gamma,
                                       comp_max, &no_policies);
        if (comp_stat)
            bmpar_stationary_analysis(mdp_low, mdp_avg, mdp_up, gamma, my_results, no_policies);
        bmpar_print_pareto_results(name, my_results, no_policies, BMDP_PARETO_PRT);
        bmpar_print_pareto_values(name, my_results, no_policies);
        break;

    case BMDP_COMPUTE_PUREIPT:
        printf("Compute pure Pareto optimal policies with the IPT heuristic\n");
        my_results = bmdp_pure_opt_ipt_heuristic(mdp_low, mdp_avg, mdp_up, rew_low, rew_avg, rew_up,
                                                 gamma, comp_max, &no_policies);
        if (comp_stat)
            bmpar_stationary_analysis(mdp_low, mdp_avg, mdp_up, gamma, my_results, no_policies);
        bmpar_print_pareto_results(name, my_results, no_policies, BMDP_PARETO_PRT);
        break;

    case BMDP_COMPUTE_PUREEVO:
        printf("Compute pure Pareto optimal policies with an evolutionary strategy\n");
        my_results = bmdp_pure_opt_evo(mdp_low, mdp_avg, mdp_up, rew_low, rew_avg, rew_up, gamma,
                                       comp_max, &no_policies);
        if (comp_stat)
            bmpar_stationary_analysis(mdp_low, mdp_avg, mdp_up, gamma, my_results, no_policies);
        bmpar_print_pareto_results(name, my_results, no_policies, BMDP_PARETO_PRT);
        bmpar_print_pareto_values(name, my_results, no_policies);
        break;

    case BMDP_COMPUTE_WEIGHT:
        ind = (size_t *)ecalloc(mdp_avg->ord, sizeof(size_t));
        size_t *ind_proof = (size_t *)ecalloc(mdp_avg->ord, sizeof(size_t));
        gain_vectors_t gain = bmdp_compute_weight_case(
            mdp_low, mdp_avg, mdp_up, rew_low, rew_avg, rew_up, ind, BMDP_MAX_ITER, BMDP_MAX_TIME,
            BMDP_LO_EPS, 300, 1.0e-8, BO_GMRES_ILU0, gamma, min_weight, max_weight, comp_max,
            comp_stat);
        printf("Time for result computation %.3e sec\n", mat_timeused() - comptime);
        mstring = "weight_x.xx_x.xx_x.xxXXXXXX";
        char buf[100];
        snprintf(buf, (6 + 3 * 5), "weight_%.2f_%.2f_%.2f", min_weight,
                 (1 - min_weight - max_weight), max_weight);
        bmdp_prt_gain_and_policy(base_name, buf, mdp_avg->ord, gain.gain_avg, ind);
        short validate = FALSE;
        // hh = bmdp_policy_iteration(mdp_avg, rew_avg, ind_proof, BMDP_MAX_ITER,
        // BMDP_MAX_TIME, BMDP_LO_EPS, gamma, comp_max, comp_stat);
        // lower bound
        if (min_weight == 1.0) {
            hh = bmdp_compute_extreme_case(mdp_low, mdp_up, rew_low, ind_proof, BMDP_MAX_ITER,
                                           BMDP_MAX_TIME, BMDP_LO_EPS, 300, 1.0e-8, BO_GMRES_ILU0,
                                           gamma, TRUE, comp_max, comp_stat);
            validate = TRUE;
        } else if (max_weight == 1.0) {
            hh = bmdp_compute_extreme_case(mdp_low, mdp_up, rew_up, ind_proof, BMDP_MAX_ITER,
                                           BMDP_MAX_TIME, BMDP_LO_EPS, 300, 1.0e-8, BO_GMRES_ILU0,
                                           gamma, FALSE, comp_max, comp_stat);
            validate = TRUE;
        } else if (min_weight == 0.0 && max_weight == 0.0) {
            hh = bmdp_policy_iteration(mdp_avg, rew_avg, ind_proof, BMDP_MAX_ITER, BMDP_MAX_TIME,
                                       BMDP_LO_EPS, gamma, comp_max, comp_stat);
            validate = TRUE;
        }
        // bmdp_prt_gain_and_policy(base_name, "avg", mdp_avg->ord, hh, ind_proof);
        // verify

        short valid = TRUE;
        int i;
        double maxerr = 0.0;

        double min_val = 0.0, avg_val = 0.0, max_val = 0.0;
        for (i = 0; i < mdp_avg->ord; ++i) {
            min_val += gain.gain_min[i];
            max_val += gain.gain_max[i];
            avg_val += gain.gain_avg[i];

            if (validate) {
                double error = 0.0;
                if (min_weight == 1.0) {
                    error = hh[i] - gain.gain_min[i];
                } else if (max_weight == 1.0) {
                    error = hh[i] - gain.gain_max[i];
                } else if (min_weight == 0.0 && max_weight == 0.0) {
                    error = hh[i] - gain.gain_avg[i];
                }
                if (ind[i] != ind_proof[i]) {
                    // whoops, something went wrong...
                    printf("erroneous policy at state %i: should be %zu, is %zu\n", i, ind_proof[i],
                           ind[i]);
                    valid = FALSE;
                }
                if (fabs(error) > 0.01) {
                    printf("value differences (%f) at state %i\n", error, i);
                }
                maxerr += fabs(error);
            }
        }
        printf("maxerr is %f\n", maxerr);
        printf("values are %.5f %.5f %.5f\n", min_val / mdp_avg->ord, avg_val / mdp_avg->ord,
               max_val / mdp_avg->ord);
        break;
    case BMDP_COMPUTE_HULL:
        solutions = bmdp_weight_opt3d_alt(mdp_low, mdp_avg, mdp_up, rew_low, rew_avg, rew_up,
                                          BMDP_MAX_ITER * 10, BMDP_MAX_TIME, BMDP_LO_EPS,
                                          BO_GMRES_ILU0, gamma, comp_max, min_max_avg);
        if (!list_empty(solutions)) {
            int len = 0;
            List **it = solutions;
            while (!list_empty(it)) {
                it = list_next(it);
                len++;
            }
            my_results = (Pareto_vector_element **)ecalloc(len, sizeof(Pareto_vector_element *));
            it = solutions;
            len = 0;
            while (!list_empty(it)) {
                my_results[len++] = (Pareto_vector_element *)(*it)->elem;
                it = list_next(it);
            }
            // compute some auxiliary values
            bmpar_stationary_analysis(mdp_low, mdp_avg, mdp_up, gamma, my_results, len);
            bmpar_print_pareto_results(base_name, my_results, len, BMDP_PARETO_PRT);
        }
        break;
    case BMDP_COMPUTE_FRONT:
        elems = bmdp_pareto_opt(mdp_low, mdp_avg, mdp_up, rew_low, rew_avg, rew_up, gamma,
                                BMDP_LO_EPS, comp_max, &no_policies);
        bmpar_print_nonst_pareto_results(base_name, elems, no_policies);
        break;

    default:
        printf(" ");
    }
    mdp_matrix_free(mdp_low);
    mdp_matrix_free(mdp_avg);
    mdp_matrix_free(mdp_up);
    for (i = 0; i < act; i++) {
        mat_double_vektor_free(rew_low[i]);
        mat_double_vektor_free(rew_avg[i]);
        mat_double_vektor_free(rew_up[i]);
    }
    efree(rew_low);
    efree(rew_avg);
    efree(rew_up);
    if (my_results != NULL) {
        for (i = 0; i < no_policies; ++i) {
            bmpar_pareto_vector_element_free(my_results[i]);
        }
        efree(my_results);
    }
    printf("Everything done after %.3e second\n", mat_timeused() - starttime);
    return (0);
} /* main */

void write_cmdp_solution(const value_vectors vectors, double **policy, size_t act,
                         const char *filename)
{
    size_t filename_length = strlen(filename);
    char *basename = (char *)calloc(filename_length + 32, sizeof(char));
    char *occurence = strstr(filename, ".cmdp");
    size_t len = occurence - filename;
    if (occurence == NULL) {
        len = filename_length;
    }
    strncpy(basename, filename, len);
    sprintf(basename, "%s%s", basename, ".cmdp_vals");
    FILE *fp = fopen(basename, "w");

    for (int s = 0; s < vectors.ord; ++s) {
        for (int k = 0; k < vectors.scenarios; ++k) {
            fprintf(fp, "%.5f ", vectors.gains[k][s]);
        }
        for (int a = 0; a < act; ++a) {
            fprintf(fp, "%.5f ", policy[s][a]);
        }
        fprintf(fp, "\n");
    }

    fclose(fp);
}

int cmdp_main(int argc, char **argv)
{
    /* call format: basename method gamma [comp_max] */
    size_t method;
    sscanf(argv[2], "%lu", &method);
    printf("CMDP optimization branch: %lu\n", method);

    /* double starttime = mat_timeused(); */
    double gamma = 0.0;
    sscanf(argv[3], "%le", &gamma);

    char *basename = argv[1];

    size_t print_solutions = 0;
    if (argc > 4) {
        sscanf(argv[4], "%lu", &print_solutions);
    }

    size_t comp_max = 1;
    if (argc > 5) {
        sscanf(argv[4], "%lu", &comp_max);
    }

    cmdp_problem prob = concurrent_read_problem(basename);
    /* initialize optimization context */
    cmdp_context ctx;
    prob.gamma = gamma;
    prob.comp_max = comp_max;
    ctx.problem = &prob;
    ctx.policy = NULL;
    value_vectors ret;
    concurrent_mdp cmdp = prob.mdp;

    if (method == CMDP_CHECK_MATRICES) {
        if (cmdp_check_matrices(cmdp)) {
            printf("Valid CMDP of order %lu with %lu scenarios found\n", cmdp.matrices[0]->ord,
                   cmdp.scenarios);
        } else {
            printf("Inconsistencies found!\n");
        }
    } else {
        switch (method) {
        case CMDP_OPT_IMGP:
            printf("Using IMGP\n");
            ctx.method = CMDP_METHOD_IMGP;
            break;
        case CMDP_OPT_IMPP:
            printf("Using IMPP\n");
            ctx.method = CMDP_METHOD_IMPP;
            break;
        case CMDP_OPT_MIP:
            printf("Using MIP\n");
            ctx.method = CMDP_METHOD_MIP;
            break;
        case CMDP_OPT_NLP:
            printf("Using NLP/Ipopt\n");
            ctx.method = CMDP_METHOD_NLP;
            break;
        case CMDP_OPT_NLP2:
            printf("Using NLP/NLopt\n");
            ctx.method = CMDP_METHOD_NLP2;
            break;
        case CMDP_OPT_QCLP:
            printf("Using QCLP\n");
            ctx.method = CMDP_METHOD_QCLP;
            break;
        default:
            printf("Unknown method %zu\n", method);
            return -1;
        }
        ret = cmdp_optimize(&ctx);
        const size_t actions = prob.mdp.matrices[0]->rows[0]->pord;
        /* write result somewhere */
        printf("%.5e\n", ctx.result);

        /* do we need to compare solutions? */
        if (print_solutions) {
            write_cmdp_solution(ret, ctx.policy, actions, basename);
            /* compute for each scenario the optimal policy and write it out, too */
            for (size_t k = 0; k < prob.mdp.scenarios; ++k) {
                fprintf(stderr, "[BMDPanalysis] Computing solution for scenario %zu\n", k);
                /* clear weights */
                for (size_t kprime = 0; kprime < prob.mdp.scenarios; ++kprime) {
                    prob.weights.weights[kprime] = 0.0;
                }
                prob.weights.weights[k] = 1.0;
                ctx.method = CMDP_METHOD_MIP;
                value_vectors vecs = cmdp_optimize(&ctx);
                fprintf(stderr, "[BMDPanalysis] Got a solution for scenario %zu\n", k);

                /* write the solution somewhere where we can see it */
                size_t filename_length = strlen(basename);
                char *compare_basename = (char *)calloc(filename_length + 32, sizeof(char));
                sprintf(compare_basename, "%s", basename);
                char *ptr = strstr(compare_basename, ".cmdp");
                if (ptr == NULL) {
                    sprintf(compare_basename, "%s_scenario_%zu.cmdp", basename, k);
                } else {
                    sprintf(ptr, "_scenario_%zu.cmdp", k);
                }
                fprintf(stderr, "[BMDPanalysis] filename: %s\n", compare_basename);
                write_cmdp_solution(vecs, ctx.policy, actions, compare_basename);

                /* cleanup */
                for (size_t kprime = 0; kprime < prob.mdp.scenarios; ++kprime) {
                    free(vecs.gains[kprime]);
                }
                free(compare_basename);
                fprintf(stderr, "[BMDPanalysis] Saved solution for scenario %zu\n", k);
            }
        }

        for (size_t k = 0; k < prob.mdp.scenarios; ++k) {
            free(ret.gains[k]);
        }
        free(ret.gains);
        for (size_t s = 0; s < prob.mdp.matrices[0]->ord; ++s) {
            free(ctx.policy[s]);
        }
        free(ctx.policy);
    }
    concurrent_delete_problem(prob);
    /* printf("done after %.3e seconds\n", mat_timeused() - starttime); */

    return 0;
}

int smdp_main(int argc, char **argv)
{
    stdlog_level(LOG_INFO);
    double gamma = 0.0;
    sscanf(argv[3], "%le", &gamma);
    char *basename = argv[1];
    size_t max_subproblems = 5;
    if (argc > 3) {
        sscanf(argv[4], "%zu", &max_subproblems);
    }

    stdlog(LOG_TRACE, "smdp_main", "gamma = %f", gamma);
    stdlog(LOG_TRACE, "smdp_main", "basename = %s", basename);
    stdlog(LOG_TRACE, "smdp_main", "max subproblems = %d", max_subproblems);

    size_t ord;
    size_t actions;
    Double_vektor **rew_low, **rew_avg, **rew_up;
    continuous_optimization_problem problem;
    /* read BMDP */
    bmdp_read_process(basename, &ord, &actions, &(problem.mdp_lower), &rew_low, &(problem.mdp_mean),
                      &rew_avg, &(problem.mdp_upper), &rew_up);
    /* we have to check some assertions first… */
    short warning = FALSE;
    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            /* validate reward structure */
            if (fabs(rew_avg[a]->vektor[s] - rew_avg[0]->vektor[s]) > 1e-7) {
                if (!warning) {
                    stdlog(LOG_WARNING, "smdp_main", "Rewards are action-dependent! This is not supported (yet), assuming reward for action 0");
                    warning = TRUE;
                }
                rew_avg[a]->vektor[s] = rew_avg[0]->vektor[s];
                rew_low[a]->vektor[s] = rew_avg[0]->vektor[s];
                rew_up[a]->vektor[s] = rew_avg[0]->vektor[s];
            }
            /* validate sparsity structure */
            size_t lower_entries = problem.mdp_lower->rows[s]->no_of_entries[a];
            size_t avrge_entries = problem.mdp_mean->rows[s]->no_of_entries[a];
            size_t upper_entries = problem.mdp_upper->rows[s]->no_of_entries[a];

            assert(lower_entries == avrge_entries && avrge_entries == upper_entries);

            for (size_t i = 0; i < avrge_entries; ++i) {
                size_t lower_colind = problem.mdp_lower->rows[s]->colind[a][i];
                size_t avrge_colind = problem.mdp_mean->rows[s]->colind[a][i];
                size_t upper_colind = problem.mdp_upper->rows[s]->colind[a][i];

                assert(lower_colind == avrge_colind && avrge_colind == upper_colind);
            }
        }
    }

    continuous_optimization_options options;
    options.inner_mode = LOCAL_PURE;
    options.max_subproblems = max_subproblems;
    options.precision = 1e-7;
    problem.gamma = gamma;
    problem.initial_vector = calloc(ord, sizeof(double));
    problem.ord = ord;
    for (size_t s = 0; s < ord; ++s) {
        problem.initial_vector[s] = 0.0;
    }
    problem.initial_vector[0] = 1.0;
    problem.rewards = rew_avg;

    continuous_optimization_solution solution;
    solution.policy = calloc(ord, sizeof(size_t));

    stdlog(LOG_INFO, "smdp_main", "Call to SMDP optimizer…");

    continuous_weight_optimize(problem, options, &solution);
    mdp_matrix_free(problem.mdp_lower);
    mdp_matrix_free(problem.mdp_upper);
    mdp_matrix_free(problem.mdp_mean);
    free(problem.initial_vector);
    for(size_t a = 0; a < actions; ++a) {
        mat_double_vektor_free(rew_low[a]);
        mat_double_vektor_free(rew_avg[a]);
        mat_double_vektor_free(rew_up[a]);
    }
    free(rew_low);
    free(rew_avg);
    free(rew_up);
    free(solution.policy);

    return 0;
}

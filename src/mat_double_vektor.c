/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c fuer die angegebenen Funktionen
c (C) Copyright 1995 by Peter Buchholz
c (C) Copyright 1993, 94 by Peter Kemper
c
c     All Rights Reserved
c
c mat_double_vektor.c
c
c Aufgabe ist Funktionen zur Erzeugung und Behandlung von 
c Vektoren mit Eintraegen von Typ double
c 
c
c	Datum		Modifikationen
c	----------------------------------------------------------
c 1.8.97 Datei angelegt, Funktionen aus matrix.c entnommen
c 06.02.98 Funktion mat_double_vektor_normalise eingefuehrt
c 14.06.98 mat_double_vektor_reset und mat_double_vektor_tnorm eingefuehrt
c 11.08.98 Kommentare werden mit der Funktion read_comment ueberlesen
c 31.08.98 mat_double_vektor_inner_product eingefuegt
c 22.01.99 mat_double_vektor_copy und mat_double_vektor_add_vektor eingefuegt
c 07.12.01 mat_double_vektor_new mit Parameter 0 liefert NULL Pointer
c 07.02.03 mat_double_vektor_norm_diff eingef�gt
c 25.02.10 mat_double_vektor_tensor_product eingefuehrt
c 25.03.10 mat_double_vektor_scalar_add eingefuehrt
c 12.10.10 mat_compute_all_norms eingefuehrt
c 30.05.12 in mat_double_vektor_normalize Werte fabs(v(i)) < 1.0e-100 auf 0 gesetzt 
c 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/
#include <math.h>
#include "list.h"
#include "global.h" 
#include "ememory.h"
#include "mat_double_vektor.h"

/**********************
mat_double_vektor_new ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
Double_vektor *mat_double_vektor_new(unsigned anzahl)
#else
Double_vektor *mat_double_vektor_new(anzahl)
     unsigned anzahl ;
#endif
{
  Double_vektor *v = (Double_vektor *)ecalloc(1,sizeof(Double_vektor)) ;
  
  v->no_of_entries = anzahl ;
  if (0 < anzahl)
    v->vektor = (double *)ecalloc(anzahl,sizeof(double)) ; 
  else
    v->vektor = NULL ;
  return(v) ;
}
    
/**********************
mat_double_vektor_free ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_double_vektor_free(Double_vektor *v)
#else
int mat_double_vektor_free(v)
     Double_vektor *v ;
#endif
{
  if (!v)
    return(NO_ERROR);
  
  efree(v->vektor) ;
  efree(v) ; 
  return(NO_ERROR) ;
}
    
/**********************
mat_double_vektor_print_file ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_double_vektor_print_file(FILE *fp, Double_vektor *v)
#else
int mat_double_vektor_print_file(fp, v)
     FILE *fp ;
     Double_vektor *v ;
#endif
{
  unsigned i ;
  unsigned j ;
  
  if (!fp)
    return(OUTPUT_ERROR) ;
  if (!v) 
  {
    fprintf(fp,"Double_vektor ist leer\n") ;
    return(NO_ERROR) ;

  }
  fprintf(fp,"\nAusgabe eines Double-Vektors mit %i Eintraegen\n", 
	  v->no_of_entries) ;
  fprintf(fp," (10 Eintraege pro Zeile):\n") ;
  for (i=j=0 ;i<v->no_of_entries ; i++, j++) 
  {
    if (10 == j ) 
    {
      j=0 ;
      fprintf(fp,"\n %E", v->vektor[i]) ; 
    }
    else
      fprintf(fp," %E", v->vektor[i]) ; 
  }
  fprintf(fp,"\nEnde des Vektors \n") ;    
  return(NO_ERROR) ;
}

/**********************
mat_double_vektor_output ##e 
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_double_vektor_output(FILE *fp, Double_vektor *v)
#else
int mat_double_vektor_output(fp, v)
     FILE *fp ;
     Double_vektor *v ;
#endif
{
  unsigned i ;
  
  if (!fp || !v)
    return(OUTPUT_ERROR) ;
  fprintf(fp, "%i\n", v->no_of_entries) ;
  for (i=0 ;i<v->no_of_entries ; i++) 
    fprintf(fp,"%.15e\n", v->vektor[i]) ; 
  fprintf(fp,"\n") ;    
  return(NO_ERROR) ;
}

/**********************
mat_double_vektor_input ##e 
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_double_vektor_input(FILE *fp, Double_vektor *v)
#else
int mat_double_vektor_input(fp, v)
     FILE *fp ;
     Double_vektor *v ;
#endif
{
  int i ;
  
  if (!fp || !v)
    return FALSE ;
  fp = read_comment(fp) ;
  if (fscanf(fp," %i\n", &i)  == EOF)
    return(INPUT_ERROR) ;
  if (i != v->no_of_entries) {
    efree(v->vektor) ;
    v->vektor = (double *) ecalloc(i+1, sizeof(double)) ;
    v->no_of_entries = i ;
  }
  for (i=0 ;i<v->no_of_entries ; i++) 
  {
    fp = read_comment(fp) ;
    if (fscanf(fp," %le\n", &v->vektor[i])  == EOF)
      return(INPUT_ERROR) ; 
  }
  return(NO_ERROR) ;  
}

/**********************
mat_double_vektor_sum ##e 
Datum:  13.6.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
double mat_double_vektor_sum (Double_vektor *v) 
#else
double mat_double_vektor_sum (v) 
  Double_vektor *v ;
#endif
{
  unsigned i       ;
  double   sum=0.0 ;

  for (i = 0; i < v->no_of_entries; i++)
    sum += v->vektor[i] ;
  return (sum) ;
}

/**********************
mat_double_vektor_scalar_mult
Datum:  03.03.05
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Multipliziert einen Vektor mit einem Skalar.
**********************/
#ifndef CC_COMP
int mat_double_vektor_scalar_mult(Double_vektor *v, double s)
#else
int mat_double_vektor_scalar_mult(v, s)
  Double_vektor  *v ; 
  double          s ;
#endif
{
  int i ;

  for (i = 0; i < v->no_of_entries; i++) {
      v->vektor[i] *= s ;
  }
  return(NO_ERROR) ;
}

/**********************
mat_double_vektor_scalar_add
Datum:  03.03.05
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Addiert einen Skalar zu einem Vektor.
**********************/
#ifndef CC_COMP
int mat_double_vektor_scalar_add(Double_vektor *v, double s)
#else
int mat_double_vektor_scalar_add(v, s)
  Double_vektor  *v ; 
  double          s ;
#endif
{
  int i ;

  for (i = 0; i < v->no_of_entries; i++) {
      v->vektor[i] += s ;
  }
  return(NO_ERROR) ;
}



/**********************
mat_double_vektor_norm_diff 
Datum:  07.02.03
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Berechnung der Summen- und Maximumsnorm der Differenz zweier Vektoren.
**********************/
#ifndef CC_COMP
int mat_double_vektor_norm_diff(Double_vektor *v1, Double_vektor *v2, double *n_max, double *n_sum)
#else
int mat_double_vektor_norm_diff(v1, v2, n_max, n_sum)
  Double_vektor  *v1    ; 
  Double_vektor  *v2    ;
  double         *n_max ;
  double         *n_sum ;
#endif
{
  int i ;
  double d ;

  (*n_max) = fabs(v1->vektor[0]-v2->vektor[0]) ;
  (*n_sum) = fabs(v1->vektor[0]-v2->vektor[0]) ;
  for (i = 1; i < v1->no_of_entries && i < v2->no_of_entries; i++)
  {
    d = fabs(v1->vektor[i]-v2->vektor[i]) ;
    (*n_sum) += d ;
    if ((*n_max) < d)
      (*n_max) = d ;
  }
  return(NO_ERROR) ;
}

/**********************
mat_compute_norms ##e Berechnung der Maximum- und Summennorm
Datum:  24.3.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
#ifndef CC_COMP
int mat_compute_norms(Double_vektor *v, double *n_max, double *n_sum)
#else
int mat_compute_norms(v, n_max, n_sum)
  Double_vektor  *v     ; 
  double         *n_max ;
  double         *n_sum ;
#endif
{
  int i ;
  double d ;

  (*n_max) = fabs(v->vektor[0]) ;
  (*n_sum) = fabs(v->vektor[0]) ;
  for (i = 1; i < v->no_of_entries; i++)
  {
    d = fabs(v->vektor[i]) ;
    (*n_sum) += d ;
    if ((*n_max) < d)
      (*n_max) = d ;
  }
  return(NO_ERROR) ;
}

/**********************
mat_compute_norms ##e Berechnung der Maximum-, Summen-
                     und Euklisdischen-Norm
Datum:  24.3.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
#ifndef CC_COMP
int mat_compute_all_norms(Double_vektor *v, double *n_max, double *n_sum, double *n_two)
#else
  int mat_compute_all_norms(v, n_max, n_sum, n_two)
  Double_vektor  *v     ; 
  double         *n_max ;
  double         *n_sum ;
  double         *n_two ;
#endif
{
  int i ;
  double d ;

  (*n_max) = fabs(v->vektor[0]) ;
  (*n_sum) = fabs(v->vektor[0]) ;
  (*n_two) = v->vektor[0] * v->vektor[0] ;
  for (i = 1; i < v->no_of_entries; i++)
  {
    d = fabs(v->vektor[i]) ;
    (*n_sum) += d ;
    if ((*n_max) < d)
      (*n_max) = d ;
    *n_two += v->vektor[i] * v->vektor[i] ;
  }
  *n_two = sqrt(*n_two) ;
  return(NO_ERROR) ;
}

/**********************
mat_double_vektor_tnorm ##e Berechnung der 2 Norm fuer Vektoren
Datum:  24.3.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
#ifndef CC_COMP
double mat_double_vektor_tnorm(Double_vektor *v)
#else
double mat_double_vektor_tnorm(v)
  Double_vektor  *v     ; 
#endif
{
  int i ;
  double d = 0.0;

  d += v->vektor[0] * v->vektor[0] ;
  for (i = 1; i < v->no_of_entries; i++)
    d += v->vektor[i] * v->vektor[i] ;
  d = sqrt(d) ;
  return(d) ;
}

/**********************
mat_double_vektor_inner_product ##e Berechnung des Produkts zweier Vektoren
Datum:  31.08.98
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
#ifndef CC_COMP
double mat_double_vektor_inner_product(Double_vektor *v1, Double_vektor *v2)
#else
double mat_double_vektor_inner_product(v1, v2)
  Double_vektor  *v1    ;
  Double_vektor  *v2    ; 
#endif
{
  int i ;
  double d ;

  if (v1 == NULL || v2 == NULL || (v1->no_of_entries != v2->no_of_entries))
    return(0.0) ;
  d = v1->vektor[0] * v2->vektor[0] ;
  for (i = 1; i < v1->no_of_entries; i++) 
    d += v1->vektor[i] * v2->vektor[i] ;
  return(d) ;
}

/**********************
mat_add_vektor_skalar 
Datum:  24.3.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Adds v2 to v1 and sets v2 to 0
**********************/
#ifndef CC_COMP
int mat_add_vektor_skalar(Double_vektor *v1, Double_vektor *v2, double alpha)
#else
int mat_add_vektor_skalar(v1, v2, alpha)
  Double_vektor *v1   ;
  Double_vektor *v2   ;
  double         alpha ;
#endif
{
  int i ;

  if (v1->no_of_entries != v2->no_of_entries)
  {
    printf("Fatal error wrong vector dimensions\n") ;
    exit(1) ;
  }
  for (i = 0; i < v1->no_of_entries; i++)
  {
    v1->vektor[i] += alpha * v2->vektor[i] ;
    v2->vektor[i]  = 0.0 ;
  }
  return(NO_ERROR) ;
}

/**********************
mat_double_vektor_normalise ##e Normalisiert einen Vektor auf 1.0
Datum:  24.3.95
Autor:  Peter Buchholz
Input:  v Double Vektor 
Output: v Vektor mit auf 1.0 normalisierten Elementen
Side-Effects:
Description:
Die Funktion normiert die Summe der Vektorelemente eines double Vektors auf
1.0. Dazu werden alle Elemente < 1.0e-100 auf 0 gesetzt. Falls die Summe der
vektorelemente nach dieser Operation 0 ist, so wird der Vektor mit einer
Gleichverteilung belegt.
**********************/
#ifndef CC_COMP
int mat_double_vektor_normalise(Double_vektor *v) 
#else
int mat_double_vektor_normalise(v)
  Double_vektor  *v ;
#endif 
{
  int i;
  double sum = 0.0 ;
  
  for (i = 0; i < v->no_of_entries; i++)
  {
    if (fabs(v->vektor[i]) <= 1.0e-100)
      v->vektor[i] = 0.0 ;
    else
     sum += v->vektor[i] ;
  }
  if (sum <= 1.0e-20)
  {
    sum = 1.0 / (double) v->no_of_entries ;
    for (i = 0; i < v->no_of_entries; i++)
      v->vektor[i] = sum ;
    printf("Zero or negative vector reset to uniform distribution\n") ;
    return(NO_CONVERGENCE) ;
  }
  else
  {
    if (fabs(1.0 - sum) > 1.0e-20)
    {
      sum = 1.0 / sum ;
      for (i = 0; i < v->no_of_entries; i++)
        v->vektor[i] *= sum ;
    }
  }
  return(NO_ERROR) ;
}

/**********************
mat_double_vektor_reset ##e Belegt einen double_vektor mit 0.0
Datum:  14.06.98
Autor:  Peter Buchholz
Input:  v Double Vektor 
Output: v Vektor mit auf 0.0 belegt
Side-Effects:
Description:
Die Funktion belegt die Elemente eines Vektors mit 0.0.
**********************/
#ifndef CC_COMP
int mat_double_vektor_reset(Double_vektor *v) 
#else
int mat_double_vektor_reset(v)
  Double_vektor  *v ;
#endif 
{
  int i;

  for (i = 0; i < v->no_of_entries; i++)
    v->vektor[i] = 0.0 ;
  return(NO_ERROR) ;
} /* mat_double_vektor_reset */

/**********************
mat_double_vektor_copy 
Datum:  22.01.99
Autor:  Peter Buchholz
Input:  v Double Vektor
        w Double Vektor 
Output: w Vektor mit dem Inhalt von v
Side-Effects:
Description:
Die Funktion kopiert die Elemente von v nach w, wobei v und w
gleiche Laenge haben muessen.
**********************/
#ifndef CC_COMP
int mat_double_vektor_copy(Double_vektor *v, Double_vektor *w) 
#else
int mat_double_vektor_copy(v, w)
  Double_vektor  *v ;
  Double_vektor  *w ;
#endif 
{
  int i;

  if (v == NULL || w == NULL || v->no_of_entries != w->no_of_entries){
      printf("Can not copy vectors of different length\n") ;
    return(INPUT_ERROR) ;
  }
  for (i = 0; i < v->no_of_entries; i++)
    w->vektor[i] = v->vektor[i] ;
  return(NO_ERROR) ;
} /* mat_double_vektor_copy */

/**********************
mat_double_vektor_add_vektor 
Datum:  22.01.99
Autor:  Peter Buchholz
Input:  v Double Vektor
        w Double Vektor 
        a Skalar
Output: v = v + a * w
Side-Effects:
Description:
Die Funktion berechnet v = v + a*w. Im Gegensatz zur Funktion mat_add_vektor_skalar,
wird Vektor w nicht veraendert. Vektoren muessen gleiche Laenge haben.
**********************/
#ifndef CC_COMP
int mat_double_vektor_add_vektor(Double_vektor *v, Double_vektor *w, double a) 
#else
int mat_double_vektor_add_vektor(v, w, a)
  Double_vektor  *v ;
  Double_vektor  *w ;
  double          a ;
#endif 
{
  int i;

  if (v == NULL || w == NULL || v->no_of_entries != w->no_of_entries)
    return(INPUT_ERROR) ;
  for (i = 0; i < v->no_of_entries; i++)
    v->vektor[i] += a * w->vektor[i] ;
  return(NO_ERROR) ;
} /* mat_double_vektor_add_vektor */

/**********************
mat_double_vektor_eliminate_small_values
Datum:  09.02.03
Autor:  Peter Buchholz
Input:  v Double Vektor
        sval kleiner Wert, ab dem Elemente gel�scht werden sollen
Output: v, wobei Elemente < sval gel�scht wurden
Side-Effects:
Description:
Die Funktion setzt alle Werte aus V, die kleiner als sval sind auf 0.
**********************/
#ifndef CC_COMP
int mat_double_vektor_eliminate_small_values(Double_vektor *v, double sval)
#else
int mat_double_vektor_eliminate_small_values(v, sval)
Double_vektor *v   ;
double         sval;
#endif
{
    int i, changed = 0 ;

 
    for (i = 0; i < v->no_of_entries; i++) {
	if (v->vektor[i] < sval) { 
	    v->vektor[i] = 0.0 ;
	    changed = 1 ;
	}
    }
    return(changed) ;
} /* mat_double_vektor_eliminate_small_values */

/**********************
mat_double_vektor_tensor_product
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  p1, p2 Vektoren 
Output: Funktionswert
Side-Effects:
Description:
Die Funktion berechnet das Kronecker Produkt zweier Vektoren.
**********************/
Double_vektor *mat_double_vektor_tensor_product(Double_vektor *p1, Double_vektor *p2) 
{
  int i, j, k ;
  Double_vektor *p0 ;

  p0 = mat_double_vektor_new(p1->no_of_entries * p2->no_of_entries) ;
  k = 0 ;
  for (i = 0; i < p1->no_of_entries; i++) {
    for (j = 0; j < p2->no_of_entries; j++) {
      p0->vektor[k] = p1->vektor[i] * p2->vektor[j] ;
      k++ ;
    }
  }
  return(p0) ;
} /* mat_double_vektor_tensor_product */

/*********************
        Datei:  transient.h
        Datum:  06.02.98
        Autor:  Peter Buchholz
        Inhalt: 
 
Side-effects: 
 
********************************************************/

#ifndef TRANSIENT_H
#define TRANSIENT_H

/***** exportierte Funktionen *****/
 
#ifndef CC_COMP
/**********************
tr_poisson_params
Datum:
Autor:  Peter Buchholz
Input:  qt Wert alpha * Zeithorizont
        epsilon Fehlerschranke
Output: left Startpunkt der Iteration
        right Endpunkt der Iteration
        scale Skalierungsfaktor
        startval Initialer Wert
Side-Effects:
Description:
Die Funktion berechnet die Werte der Poisson Verteilung mit Parameter
qt in numerisch stabiler Weise. Dabei sind left und right die Iterationsgrenzen,
startval der initiale Werte und scale der Skalierungswert. 
Es gilt also : Poisson(k) = 0 falls k < left oder k > right und
ansonsten (startval * qt^(k-left) / (k - left)!) / scale 
**********************/
extern void tr_poisson_params (double qt, double epsilon, double *scale, 
                        double *startval, int *left, int *right) ;

/**********************
tr_power_iter
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Hilfsvektor
Output: 
Side-Effects:
Description:
Die Funktion berechnet einen Iterationsschritt der Power Methode
p = p*q. Das Ergebnis wrid im Vektor p zurueckgeliefetr, der auch den
Iterationsvektor als Eingabeparameter beihaltet. 
q muss eine n x n Matrix sein!!
**********************/
extern void tr_power_iter (Sparse_matrix *q, Double_vektor **p, Double_vektor **ph) ;

/**********************
tr_randomization
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor (initialer Vektor)
        ph Hilfsvektor
        pt Transienter Vektor
        alpha Randomisierungsparameter
        t Zeitintervall
        epsilon Fehlerschranke
Output:  pt transienter Loesungsvektor zum Zeitpunkt t
        p auf 1.0 normierter Vektor pt.
Side-Effects:
Description:
Die Funktion berechnet den transienten Loesungsvktor zum Zeitpunkt t ausgehend
vom initialen Vektor p mittels Randomization. Dabei wird die Anzahl der Iterationen so
gewaehlt, dass der Fehler kleiner epsilon ist. Weiterhin wird der unormierte und der
auf 1.0 normierte Verteilungsvektor zurueckgegeben. 
**********************/
extern int tr_randomization 
      (Sparse_matrix *q, Double_vektor **p, Double_vektor **ph, Double_vektor **pt,
       double alpha, double t, double epislon) ;

/**********************
tr_acc_randomization
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor (initialer Vektor)
        ph Hilfsvektor
        pt Transienter Vektor
        pl akkumulierter Vektor
        alpha Randomisierungsparameter
        t Zeitintervall
        epsilon Fehlerschranke
Output: pt transienter Loesungsvektor zum Zeitpunkt t
        p auf 1.0 normierter Vektor pt
        pl Vektor der mittlere Zustandsverweilzeiten im Intervall [0,t) 
Side-Effects:
Description:
Die Funktion berechnet den transienten Loesungsvktor zum Zeitpunkt t und den
Vektor der mittleren Zustandsverweilzeiten im Intervall [0,t) ausgehend
vom initialen Vektor p mittels Randomization. Dabei wird die Anzahl der Iterationen so
gewaehlt, dass der Fehler kleiner epsilon ist. Weiterhin wird der unormierte und der
auf 1.0 normierte Verteilungsvektor zurueckgegeben. 
**********************/
extern void tr_acc_randomization 
      (Sparse_matrix *q, Double_vektor **p, Double_vektor **ph, Double_vektor **pt,
       Double_vektor **pl, double alpha, double t, double epislon) ;

/**********************
tr_adaptive_acc_randomization
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor (initialer Vektor)
        ph Hilfsvektor
        pt Transienter Vektor
        pl akkumulierter Vektor
        beta_sum Randomisierungsparameter
        beta Werte der einzelnen Matrizen
        t Zeitintervll
        epsilon Fehlerschranke
Output: pt transienter Loesungsvektor zum Zeitpunkt t
        p auf 1.0 normierter Vektor pt
        pl Vektor der mittlere Zustandsverweilzeiten im Intervall [0,t) 
Side-Effects:
Description:
Die Funktion berechnet den transienten Loesungsvktor zum Zeitpunkt t und den
Vektor der mittleren Zustandsverweilzeiten im Intervall [0,t) ausgehend
vom initialen Vektor p mittels Randomization �ber mehrere Matrizen, wie f�r
die adaptive Berechnung bei der hybriden SImulation ben�tigt. 
Dabei wird die Anzahl der Iterationen so gewaehlt, dass der Fehler 
kleiner epsilon ist. Weiterhin wird der unormierte und der
auf 1.0 normierte Verteilungsvektor zurueckgegeben. 
**********************/
extern void tr_adaptive_acc_randomization 
      (Sparse_matrix **q, int no_mat, Double_vektor **p, Double_vektor **ph, Double_vektor **pt,
       Double_vektor **pl, double beta_sum, double *beta, double t, double epsilon) ;

/**********************
tr_gen_step_matrix
Datum:
Autor:  Peter Buchholz
Input:  ti Intervallbreite, fuer die Q[ti] geenriert werden soll
        alpha Randomisierungsrate
        q Generatormatrix
Output: qt Matrix Q[ti]
        ql Matrix \int Q[ti]
Side-Effects:
Description:
Die Funktion berechnet Matrix qt = exp(q*ti) und die Matrix der akkumulierten Zeit in
den einzelnen Zust�nden. 
**********************/
extern void tr_gen_step_matrix
   (double ti, double alpha, Sparse_matrix *q, Sparse_matrix *qt, Sparse_matrix *ql) ;

#else

/**********************
tr_poisson_params
Datum:
Autor:  Peter Buchholz
Input:  qt Wert alpha * Zeithorizont
        epsilon Fehlerschranke
Output: left Startpunkt der Iteration
        right Endpunkt der Iteration
        scale Skalierungsfaktor
        startval Initialer Wert
Side-Effects:
Description:
Die Funktion berechnet die Werte der Poisson Verteilung mit Parameter
qt in numerisch stabiler Weise. Dabei sind left und right die Iterationsgrenzen,
startval der initiale Werte und scale der Skalierungswert. 
Es gilt also : Poisson(k) = 0 falls k < left oder k > right und
ansonsten (startval * qt^(k-left) / (k - left)!) / scale 
**********************/
extern void tr_poisson_params () ;

/**********************
tr_power_iter
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Hilfsvektor
Output: 
Side-Effects:
Description:
Die Funktion berechnet einen Iterationsschritt der Power Methode
p = p*q. Das Ergebnis wrid im Vektor p zurueckgeliefetr, der auch den
Iterationsvektor als Eingabeparameter beihaltet. 
q muss eine n x n Matrix sein!!
**********************/
extern void tr_power_iter () ;

/**********************
tr_randomization
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor (initialer Vektor)
        ph Hilfsvektor
        pt Transienter Vektor
        alpha Randomisierungsparameter
        t Zeitintervall
        epsilon Fehlerschranke
Output: pt transienter Loesungsvektor zum Zeitpunkt t
        p auf 1.0 normierter Vektor pt.
Side-Effects:
Description:
Die Funktion berechnet den transienten Loesungsvktor zum Zeitpunkt t ausgehend
vom initialen Vektor p mittels Randomization. Dabei wird die Anzahl der Iterationen so
gewaehlt, dass der Fehler kleiner epsilon ist. Weiterhin wird der unormierte und der
auf 1.0 normierte Verteilungsvektor zurueckgegeben. 
**********************/
extern void tr_randomization() ;

/**********************
tr__acc_randomization
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor (initialer Vektor)
        ph Hilfsvektor
        pt Transienter Vektor
        pl akkumulierter Vektor
        alpha Randomisierungsparameter
        t Zeitintervall
        epsilon Fehlerschranke
Output: pt transienter Loesungsvektor zum Zeitpunkt t
        p auf 1.0 normierter Vektor pt
        pl Vektor der mittlere Zustandsverweilzeiten im Intervall [0,t) 
Side-Effects:
Description:
Die Funktion berechnet den transienten Loesungsvktor zum Zeitpunkt t und den
Vektor der mittleren Zustandsverweilzeiten im Intervall [0,t) ausgehend
vom initialen Vektor p mittels Randomization. Dabei wird die Anzahl der Iterationen so
gewaehlt, dass der Fehler kleiner epsilon ist. Weiterhin wird der unormierte und der
auf 1.0 normierte Verteilungsvektor zurueckgegeben. 
**********************/
extern void tr_acc_randomization() ;

/**********************
tr_adaptive_acc_randomization
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor (initialer Vektor)
        ph Hilfsvektor
        pt Transienter Vektor
        pl akkumulierter Vektor
        beta_sum Randomisierungsparameter
        beta Werte der einzelnen Matrizen
        t Zeitintervall
        epsilon Fehlerschranke
Output: pt transienter Loesungsvektor zum Zeitpunkt t
        p auf 1.0 normierter Vektor pt
        pl Vektor der mittlere Zustandsverweilzeiten im Intervall [0,t) 
Side-Effects:
Description:
Die Funktion berechnet den transienten Loesungsvktor zum Zeitpunkt t und den
Vektor der mittleren Zustandsverweilzeiten im Intervall [0,t) ausgehend
vom initialen Vektor p mittels Randomization �ber mehrere Matrizen, wie f�r
die adaptive Berechnung bei der hybriden SImulation ben�tigt. 
Dabei wird die Anzahl der Iterationen so gewaehlt, dass der Fehler 
kleiner epsilon ist. Weiterhin wird der unormierte und der
auf 1.0 normierte Verteilungsvektor zurueckgegeben. 
**********************/
extern void tr_adaptive_acc_randomization () ;

/**********************
tr_gen_step_matrix
Datum:
Autor:  Peter Buchholz
Input:  ti Intervallbreite, fuer die Q[ti] geenriert werden soll
        alpha Randomisierungsrate
        q Generatormatrix
Output: qt Matrix Q[ti]
        ql Matrix \int Q[ti]
Side-Effects:
Description:
Die Funktion berechnet Matrix qt = exp(q*ti) und die Matrix der akkumulierten Zeit in
den einzelnen Zust�nden. 
**********************/
extern void tr_gen_step_matrix() ;

#endif

#endif


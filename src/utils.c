#include "utils.h"
#include "ememory.h"
#include <stdarg.h>
#include <stdio.h>
#include <time.h>

double **binary_vectors_init(int num_vectors, int ord) {
  int i, s;
  double **vectors = (double **)ecalloc(num_vectors, sizeof(double *));
  for (i = 0; i < num_vectors; ++i) {
    vectors[i] = (double *)ecalloc(ord, sizeof(double));
    for (s = 0; s < ord; ++s) {
      vectors[i][s] = ((s == 0) ? 1.0 : 0.0);
    }
  }
  return vectors;
}

short binary_vectors_at_end(int num_vectors, int ord, double **vectors) {
  short at_end = TRUE;
  int i;
  for (i = 0; i < num_vectors; ++i) {
    at_end = (at_end && (vectors[i][ord - 1] == 1.0));
  }
  return at_end;
}

void binary_vectors_next(int num_vectors, int ord, double **vectors) {
  int i, s;
  for (i = 0; i < num_vectors; ++i) {
    for (s = 0; s < ord; ++s) {
      if (vectors[i][s] == 1.0) {
        if (s < ord - 1) {
          vectors[i][s] = 0.0;
          vectors[i][s + 1] = 1.0;
          return;
        } else {
          vectors[i][s] = 0.0;
          vectors[i][0] = 1.0;
        }
      }
    }
  }
}

void binary_vectors_remove(int num_vectors, double **vectors) {
  int i;
  for (i = 0; i < num_vectors; ++i) {
    efree(vectors[i]);
  }
  efree(vectors);
}

int next_subset(int subset) {
  /* Gosper's hack. */
  int least_significant_bit = subset & -subset;
  int flipped = subset + least_significant_bit;
  int result = (((subset ^ flipped) >> 2) / least_significant_bit) | flipped;
  return result;
}

/// A variable which stores the string values for the log level names
static const char *log_level_names[] = {"TRACE", "DEBUG", "INFO",
                                        "WARN",  "ERROR", "FATAL"};

// Use colors
/// A list of matching colors
static const char *level_colors[] = {"\x1b[94m", "\x1b[36m", "\x1b[32m",
                                     "\x1b[33m", "\x1b[31m", "\x1b[35m"};

static struct {
  FILE *fp;
  enum log_level level;
  int quiet;
} L;

void stdlog_quiet(int quiet) { L.quiet = quiet; }

void stdlog_file(FILE *fp) { L.fp = fp; }

void stdlog_level(enum log_level level) { L.level = level; }

void stdlog(enum log_level level, const char *src, const char *fmt, ...) {
  /* Get current time */
  time_t t = time(NULL);
  struct tm *lt = localtime(&t);

  char buf[16];
  buf[strftime(buf, sizeof(buf), "%H:%M:%S", lt)] = '\0';
  fprintf(stderr, "%s %s%-5s\x1b[0m \x1b[90m%s:\x1b[0m ", buf,
          level_colors[level], log_level_names[level], src);

  if (level >= L.level) {
    if (!L.quiet) {
      va_list args;
      va_start(args, fmt);
      vfprintf(stderr, fmt, args);
      va_end(args);
      fprintf(stderr, "\n");
      fflush(stderr);
    }

    if (L.fp != NULL) {
      va_list args;
      char buf[32];
      buf[strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", lt)] = '\0';
      fprintf(L.fp, "%s %-5s %s: ", buf, log_level_names[level], src);
      va_start(args, fmt);
      vfprintf(L.fp, fmt, args);
      va_end(args);
      fprintf(L.fp, "\n");
      fflush(L.fp);
    }
  }
  return;
}

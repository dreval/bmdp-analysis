#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>

double **binary_vectors_init(int num_vectors, int ord);

short binary_vectors_at_end(int num_vectors, int ord, double **vectors);

void binary_vectors_next(int num_vectors, int ord, double **vectors);

void binary_vectors_remove(int num_vectors, double **vectors);

int next_subset(int subset);

enum log_level { LOG_TRACE = 0, LOG_DEBUG = 1, LOG_INFO = 2, LOG_WARNING = 3, LOG_FATAL = 4 };

/// A function for logging things
void stdlog(enum log_level level, const char *src, const char *fmt, ...);

/// Logger control
/// Control stderr output
void stdlog_quiet(int quiet);
/// Control logfile
void stdlog_file(FILE *fp);
/// Control logfile
void stdlog_level(enum log_level level);

#endif // UTILS_H

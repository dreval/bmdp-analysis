#include "bmdp_pareto_evo.h"
#include "bmdp_pareto_func.h"
#include <omp.h>
#include <sys/time.h>
#include <stddef.h>

size_t random_interval(size_t min, size_t max) {
    int r;
    const size_t range = max - min;
    const size_t buckets = RAND_MAX / range;
    const size_t limit = buckets * range;
    do {
        r = rand();
    } while(r >= limit);

    return min + r / buckets;
}

void initialize_strength_and_fitness(List **list) {
    List **pv = list;

    while(!list_empty(pv)) {
        Pareto_vector_element *i_elem = (Pareto_vector_element *) ((*pv)->elem);
        i_elem->strength = 0;
        i_elem->fitness = 0.0;

        pv = list_next(pv);
    }
}

void compute_relative_strength(List **first_list, List **second_list, int comp_max) {
    List **pv = first_list;
    const int first_vector_dominates = comp_max ? 1 : 2;
    if(list_empty(first_list) || list_empty(second_list)) {
        return;
    }

    while(!list_empty(pv)) {
        Pareto_vector_element *i_elem = (Pareto_vector_element *) ((*pv)->elem);
        i_elem->strength = 0;

        /* Compute strength of the element */
        List **pv_prime = second_list;
        while(!list_empty(pv_prime)) {
            Pareto_vector_element *j_elem = (Pareto_vector_element *) ((*pv_prime)->elem);
            if(bmpar_vector_compare(i_elem, j_elem) == first_vector_dominates) {
                i_elem->strength++;
            }
            pv_prime = list_next(pv_prime);
        }
        pv = list_next(pv);
    }
}

void compute_relative_fitness(List **first_list, List **second_list, int comp_max) {
    List **pv = first_list;
    const int first_vector_dominates = comp_max ? 1 : 2;
    if(list_empty(first_list) || list_empty(second_list)) {
        return;
    }

    while(!list_empty(pv)) {
        Pareto_vector_element *i_elem = (Pareto_vector_element *) ((*pv)->elem);
        i_elem->fitness = 0.0;

        /* Compute strength of the element */
        List **pv_prime = second_list;
        while(!list_empty(pv_prime)) {
            Pareto_vector_element *j_elem = (Pareto_vector_element *) ((*pv_prime)->elem);
            if(bmpar_vector_compare(j_elem, i_elem) == first_vector_dominates) {
                i_elem->fitness += j_elem->strength;
            }
            pv_prime = list_next(pv_prime);
        }
        pv = list_next(pv);
    }
}

int compare_doubles(const void *a, const void *b) {
    const double *da = (const double *) a;
    const double *db = (const double *) b;

    return (*da > *db) - (*da < *db);
}

int compare_elements_by_fitness(const void *a, const void *b) {
    const Pareto_vector_element **elem1 = (const Pareto_vector_element **) a;
    const Pareto_vector_element **elem2 = (const Pareto_vector_element **) b;

    return ((*elem1)->fitness > (*elem2)->fitness) - ((*elem1)->fitness < (*elem2)->fitness);
}

double bmpar_vector_distance(Pareto_vector_element const * restrict elem1, Pareto_vector_element const * restrict elem2) {
    int i;
    double distance;
    assert(elem1->ord == elem2->ord);
    for(i = 0; i < elem1->ord; ++i) {
        distance += pow(elem1->gain_min[i] - elem2->gain_min[i], 2);
        distance += pow(elem1->gain_avg[i] - elem2->gain_avg[i], 2);
        distance += pow(elem1->gain_max[i] - elem2->gain_max[i], 2);
    }
    distance = pow(distance, 0.5);
    return distance;
}

Pareto_vector_element **adjust_knn_fitness(List **archive, List **population, int archive_size, int population_size) {
    const int n = archive_size + population_size;
    const double k_double = pow(n, 0.5);
    const int k = ceil(k_double);
    int i = 0, j;

    /* copy everything into an array */
    Pareto_vector_element **all_elements = (Pareto_vector_element **) ecalloc(n, sizeof(Pareto_vector_element *));

    List **pv = archive;
    while(!list_empty(pv)) {
        Pareto_vector_element *elem = (Pareto_vector_element *) ((*pv)->elem);
        all_elements[i] = elem;
        pv = list_next(pv);
        ++i;
    }

    pv = population;
    while(!list_empty(pv)) {
        Pareto_vector_element *elem = (Pareto_vector_element *) ((*pv)->elem);
        all_elements[i] = elem;
        pv = list_next(pv);
        ++i;
    }

    /* create an array for distances */
    #pragma omp parallel for private(j) firstprivate(all_elements)
    for(i = 0; i < n; ++i) {
        double *distances = (double *) ecalloc(n, sizeof(double));
        for(j = 0; j < n; ++j) {
            distances[j] = bmpar_vector_distance(all_elements[i], all_elements[j]);
        }
        qsort(distances, n, sizeof(double), compare_doubles);
        double sigma_k_i = distances[k];
        all_elements[i]->fitness += 1.0 / (sigma_k_i + 2);
        efree(distances);
    }

    qsort(all_elements, n, sizeof(Pareto_vector_element *), compare_elements_by_fitness);
    return all_elements;
}

Pareto_vector_element **bmdp_pure_opt_evo(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                                 Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                                 double gamma, short comp_max, size_t *no_policies) {
    /*
     * This is an implementation of the popular SPEA2 algorithm.
     *
     * In the beginning, we clarify the algorithm as presented in Zitzler, Laumanns, Thiele (2001).
     *
     * Step 1: Create an initial population P_0, empty archive A_0, t = 0
     * Step 2: Calcualte fitness
     * Step 3: Copy all non-dominated individuals (at most N_A) from P_t, A_t to A_{t + 1}, if there is still space, copy some dominated individuals from P_t
     * Step 4: Terminate, if t ≥ T or some other termination condition has been reached
     * Step 5: Tournament selection with replacement from A_{t + 1} to mating pool M
     * Step 6: Apply recombination and mutation to M to get P_{t + 1}, go to Step 2
     */

    struct timeval starttime, endtime;
    gettimeofday(&starttime, NULL);

    /* declare variables */
    const size_t ord = mdp_avg->ord;
    short stop = FALSE;
    List **population = list_new();
    List **archive = list_new();
    int t = 0;
    int i, j, k;
    size_t s;
    int archive_size = 0;

    /* Create an initial population of fixed size */
    const int initial_size = 100;
    int population_size = initial_size;
    // #pragma omp parallel for
    for(i = 0; i < initial_size; ++i) {
        size_t *policy = (size_t *) ecalloc(ord, sizeof(size_t));

        for(s = 0; s < ord; ++s) {
            policy[s] = random_interval(0, mdp_avg->rows[s]->pord);
        }
        Pareto_vector_element *elem = bmpar_pareto_vector_element_new(ord);
        elem->policy = policy;
        elem->gain_min = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, policy, rew_min, gamma, TRUE, FALSE);
        elem->gain_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, policy, rew_max, gamma, FALSE, FALSE);
        elem->gain_avg = bmdp_evaluate_avg_policy(mdp_avg, policy, rew_avg, gamma, FALSE);

        list_prepend(population, elem);
    }

    while(!stop) {
        /* TODO: Calculate fitness */

        List **lists[2] = {archive, population};
        initialize_strength_and_fitness(archive);
        initialize_strength_and_fitness(population);

        /* Part one: strength calcualtion */
        // #pragma omp parallel for private(j) num_threads(2)
        for(i = 0; i < 2; ++i) {
            for(j = 0; j < 2; ++j) {
                compute_relative_strength(lists[i], lists[j], comp_max);
            }
        }

        /* Part two: fitness calculation */
        // #pragma omp parallel for private(j) num_threads(2)
        for(i = 0; i < 2; ++i) {
            for(j = 0; j < 2; ++j) {
                compute_relative_fitness(lists[i], lists[j], comp_max);
            }
        }

        Pareto_vector_element **sorted_elements = adjust_knn_fitness(archive, population, archive_size, population_size);

        list_free(archive, list_free_delete_dummy);
        list_free(population, list_free_delete_dummy);

        for(i = 0; i < fmin(BMDP_MAX_CHECKED, archive_size + population_size); ++i) {
            list_prepend(archive, sorted_elements[i]);
        }
        int old_size = archive_size + population_size;
        archive_size = i;

        /* free the rest */
        for(; i < old_size; ++i) {
            bmpar_pareto_vector_element_free(sorted_elements[i]);
        }

        gettimeofday(&endtime, NULL);
        int delta_seconds = endtime.tv_sec - starttime.tv_sec;
        /* termination check */
        if(t > 100 || delta_seconds > BMDP_MAX_TIME) {
            stop = TRUE;
        } else {
            /* Mating selection */
            int pool_size = initial_size;
            Pareto_vector_element **pool = (Pareto_vector_element **) ecalloc(pool_size, sizeof(Pareto_vector_element *));
            Pareto_vector_element *tournament[2];
            const double selection_pressure = 0.8;

            for(i = 0; i < pool_size; ++i) {
                tournament[0] = sorted_elements[random_interval(0, archive_size)];
                tournament[1] = sorted_elements[random_interval(0, archive_size)];

                if(tournament[0]->fitness > tournament[1]->fitness) {
                    /* swap elements */
                    Pareto_vector_element *temp = tournament[0];
                    tournament[0] = tournament[1];
                    tournament[1] = temp;
                }

                Pareto_vector_element *selected;
                if(random() <= selection_pressure * RAND_MAX) {
                    selected = tournament[0];
                } else {
                    selected = tournament[1];
                }

                pool[i] = selected;
            }

            /* do some variation */

            for(i = 0; i < pool_size; ++i) {
                /* each element has a child by mutation only and a child from recombination */

                /* mutation only child */
                Pareto_vector_element *mutation_child = bmpar_pareto_vector_element_new(ord);
                size_t *mutation_policy = (size_t *) ecalloc(ord, sizeof(size_t));
                mutation_child->policy = mutation_policy;

                for(s = 0; s < ord; ++s) {
                    /* let's invent something. Probability of mutation is 1/ord */
                    size_t action_remains = random_interval(0, ord);
                    if(!action_remains) {
                        mutation_policy[s] = random_interval(0, mdp_avg->rows[s]->pord);
                    } else {
                        mutation_policy[s] = pool[i]->policy[s];
                    }
                }
                /* evaluate */
                mutation_child->gain_min = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, mutation_policy, rew_min, gamma, TRUE, FALSE);
                mutation_child->gain_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, mutation_policy, rew_max, gamma, FALSE, FALSE);
                mutation_child->gain_avg = bmdp_evaluate_avg_policy(mdp_avg, mutation_policy, rew_avg, gamma, FALSE);

                list_prepend(population, mutation_child);

                /* recombination child */
                Pareto_vector_element *recombination_child = bmpar_pareto_vector_element_new(ord);
                size_t *recombination_policy = (size_t *) ecalloc(ord, sizeof(size_t));
                recombination_child->policy = recombination_policy;
                int other_parent = random_interval(0, pool_size);

                for(s = 0; s < ord; ++s) {
                    /* flip a coin. If 0, take the current parent, if 1, take the other parent */
                    if(random_interval(0, 2)) {
                        recombination_policy[s] = pool[other_parent]->policy[s];
                    } else {
                        recombination_policy[s] = pool[i]->policy[s];
                    }
                }
                /* evaluate */
                recombination_child->gain_min = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, recombination_policy, rew_min, gamma, TRUE, FALSE);
                recombination_child->gain_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, recombination_policy, rew_max, gamma, FALSE, FALSE);
                recombination_child->gain_avg = bmdp_evaluate_avg_policy(mdp_avg, recombination_policy, rew_avg, gamma, FALSE);

                list_prepend(population, recombination_child);
            }
            population_size = pool_size * 2;

            ++t;

            efree(pool);
        }
        efree(sorted_elements);
    }
    /* return the archive */
    Pareto_vector_element **pool = (Pareto_vector_element **) ecalloc(archive_size, sizeof(Pareto_vector_element *));
    List **pv = archive;
    i = 0;
    while(!list_empty(pv)) {
        Pareto_vector_element *elem = (Pareto_vector_element *) (*pv)->elem;
        if(elem->fitness < 1.0) {
            pool[i] = elem;
            ++i;
        }
        pv = list_next(pv);
    }
    *no_policies = i;
    list_free(archive, list_free_delete_dummy);

    /* compute the time used */
    gettimeofday(&endtime, NULL);
    double delta_seconds = endtime.tv_sec - starttime.tv_sec;
    double delta_usec = endtime.tv_usec - starttime.tv_usec;
    double delta = delta_seconds + 1e-6 * delta_usec;
    printf("Computation required %.3e seconds", delta);
    return pool;
}

#include "bmdp_const.h"
#include "concurrent_weight_func.h"
#include "bmdp_func.h"
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multimin.h>
#include <nlopt.h>
#include <coin/IpStdCInterface.h>
#include <math.h>

#define NLOPT
//#undef NLOPT

typedef struct nlp_params_t {
    concurrent_mdp *mdps;
    weight_tuple *weights;
    double gamma;
    short comp_max;
} nlp_params;

typedef struct nlp_constraint_params_t {
    size_t ord;
    size_t actions;
    size_t s;
} nlp_constraint_params;

double nlp_constraint_func(const gsl_vector *x, void *params, gsl_vector *g)
{
    nlp_params *nparams = (nlp_params *) params;
    const size_t ord = nparams->mdps->matrices[0]->ord;
    const size_t actions = nparams->mdps->matrices[0]->rows[0]->pord;
    
    double value = 0.0;
    const double penalty = 1.0e3;
    
    for (size_t s = 0; s < ord; ++s) {
        double policy_mass = 1.0;
        for(size_t a = 0; a < actions; ++a) {
            policy_mass -= gsl_vector_get(x, s * actions + a);
        }
        double lambda_s = gsl_vector_get(x, ord * actions + s);
        value += penalty * lambda_s * policy_mass;
        if (g != NULL) {
            gsl_vector_set(g, ord * actions + s, policy_mass);
            for(size_t a = 0; a < actions; ++a) {
                double gradient = gsl_vector_get(g, s * actions + a);
                gradient -= penalty * lambda_s;
                gsl_vector_set(g, s * actions + a, gradient);
            }
        }
    }
    return value;
}

double nlp_helper_func_f(const gsl_vector *x, void *params)
{
    /* Evaluates the objective function and returns the result */
    nlp_params *nparams = (nlp_params *) params;
    const size_t ord = nparams->mdps->matrices[0]->ord;
    const size_t actions = nparams->mdps->matrices[0]->rows[0]->pord;

    double **policy = calloc(ord, sizeof(double *));
    double *pv = calloc(ord * actions, sizeof(double));
    double ret = 0.0;
    gsl_vector *tv = gsl_vector_alloc(ord);
    gsl_vector_view iniv = gsl_vector_view_array(nparams->mdps->iniv, ord);

    for (size_t s = 0; s < ord; ++s) {
        policy[s] = &(pv[s * actions]);
        for(size_t a = 0; a < actions; ++a) {
            policy[s][a] = gsl_vector_get(x, s * actions + a);
        }
    }

    for (size_t k = 0; k < nparams->mdps->scenarios; ++k) {
        gsl_matrix *cbar_k = mdp_identity_minus_gamma_inv_matrix(nparams->mdps->matrices[k], policy, nparams->gamma);
        gsl_vector_view rv = gsl_vector_view_array(nparams->mdps->rewards[k][0]->vektor, ord);
        gsl_blas_dgemv(CblasNoTrans, 1.0, cbar_k, &rv, 0.0, tv);
        double product = 0.0;
        gsl_blas_ddot(&iniv, tv, &product);

        ret += nparams->weights->weights[k] * product;

        gsl_matrix_free(cbar_k);
    }
    free(pv);
    free(policy);
    gsl_vector_free(tv);
    double constraint_penalty = 0.0;
#ifndef NLOPT
    if (nparams->comp_max) {
        ret = -ret;
    }
    constraint_penalty = nlp_constraint_func(x, params, NULL);
#endif /* only for GSL */
    return ret + constraint_penalty;
}

double nlp_helper_for_df(nlp_params *nparams, gsl_matrix *cbar_k, size_t k, size_t s, size_t a)
{
    /*
     * Computes the product q C_k(\bullet s) (\vec{e}_s - \gamma P_k^a(s \bullet)) C_k r
     * See Eq. 3.7 in the thesis
     */
    const size_t ord = nparams->mdps->matrices[0]->ord;
    
    double q_c_product = 0.0;
    gsl_vector_view cbar_ks = gsl_matrix_column(cbar_k, s);
    gsl_vector_view iniv = gsl_vector_view_array(nparams->mdps->iniv, ord);
    gsl_blas_ddot(&iniv, &cbar_ks, &q_c_product);

    gsl_vector_view rv = gsl_vector_view_array(nparams->mdps->rewards[k][0]->vektor, ord);
    gsl_vector *cbar_r = gsl_vector_alloc(ord);
    gsl_blas_dgemv(CblasNoTrans, 1.0, cbar_k, &rv, 0.0, cbar_r);
    
    Mdp_row *row = nparams->mdps->matrices[k]->rows[s];
    double diag = (1.0 - nparams->gamma * row->diagonals[a]) * gsl_vector_get(cbar_r, s);
    double imgp_cbar_r_product = diag;
    for (size_t i = 0; i < row->no_of_entries[a]; ++i) {
        double prod = - nparams->gamma * row->values[a][i] * gsl_vector_get(cbar_r, row->colind[a][i]);
        imgp_cbar_r_product += prod;
    }

    double p = q_c_product * imgp_cbar_r_product;
    
    gsl_vector_free(cbar_r);

    return p;
}

void nlp_helper_func_df(const gsl_vector *x, void *params, gsl_vector *g)
{
    /* Evaluates the objective function and returns the derivative */
    nlp_params *nparams = (nlp_params *) params;
    const size_t ord = nparams->mdps->matrices[0]->ord;
    const size_t actions = nparams->mdps->matrices[0]->rows[0]->pord;

    double **policy = calloc(ord, sizeof(double *));
    double *pv = calloc(ord * actions, sizeof(double));

    for (size_t s = 0; s < ord; ++s) {
        policy[s] = &(pv[s * actions]);
        for(size_t a = 0; a < actions; ++a) {
            policy[s][a] = gsl_vector_get(x, s * actions + a);
        }
    }
    gsl_vector_set_all(g, 0.0);

    for (size_t k = 0; k < nparams->mdps->scenarios; ++k) {
        gsl_matrix *cbar_k = mdp_identity_minus_gamma_inv_matrix(nparams->mdps->matrices[k], policy, nparams->gamma);
        for (size_t s = 0; s < ord; ++s) {
            for (size_t a = 0; a < actions; ++a) {
                const size_t index = s * actions + a;
                double g_sa = gsl_vector_get(g, index);
                double p = nlp_helper_for_df(params, cbar_k, k, s, a);
                gsl_vector_set(g, index, g_sa - nparams->weights->weights[k] * p);
            }
        }
        gsl_matrix_free(cbar_k);
    }
    /* add the gradient from the constraints */
#ifndef NLOPT
    nlp_constraint_func(x, params, g);
#endif /* GSL */

    free(pv);
    free(policy);
    return;
}

void nlp_helper_func(const gsl_vector *x, void *params, double *f, gsl_vector *g)
{
    /* Evaluates the objective function and returns the derivative */
    *f = nlp_helper_func_f(x, params);
    nlp_helper_func_df(x, params, g);
}

double nlp_linear_constraint(unsigned n, const double *x, double *grad, void *constraint_data)
{
    nlp_constraint_params *params = (nlp_constraint_params *) constraint_data;
    const size_t actions = params->actions;

    double ret = -1.0;
    
    for (size_t a = 0; a < actions; ++a) {
        const size_t d_s_a_index = params->s * actions + a;
        ret += x[d_s_a_index];
    }
    return ret;
}

double nlp_neg_linear_constraint(unsigned n, const double *x, double *grad, void *constraint_data)
{
    return -nlp_linear_constraint(n, x, grad, constraint_data);
}

double nlp_fdf(unsigned n, const double *x, double *grad, void *params)
{
    nlp_params *p = (nlp_params *) params;
    
    gsl_vector *g = gsl_vector_alloc(n);
    gsl_vector *gl = gsl_vector_alloc(n);
    gsl_vector *xv = gsl_vector_alloc(n);
    gsl_vector_set_all(g, 0.0);
    
    for (size_t i = 0; i < n; ++i) {
        gsl_vector_set(xv, i, x[i]);
    }
    nlp_helper_func_df(xv, params, g);
    //double lagrange = nlp_constraint_func(xv, params, gl);
    //gsl_vector_add(g, gl);
    if (grad != NULL) {
        for (size_t i = 0; i < n; ++i) {
            grad[i] = gsl_vector_get(g, i);
        }
    }
    double f = nlp_helper_func_f(xv, params);
    
    gsl_vector_free(xv);
    gsl_vector_free(g);
    
    return f;
}

Bool eval_f(Index n, Number *x, Bool new_x, Number *obj_value, UserDataPtr user_data)
{
    nlp_params *p = (nlp_params *) user_data;
    const size_t ord = p->mdps->matrices[0]->ord;
    const size_t actions = p->mdps->matrices[0]->rows[0]->pord;
    
#ifdef NATIVE_EVAL
    double **policy = (double **) calloc(ord, sizeof(double *));
    double *pv = (double *) calloc(ord * actions, sizeof(double));
    
    for (size_t s = 0; s < ord; ++s) {
        policy[s] = &(pv[s * ord]);
        for (size_t a = 0; a < actions; ++a) {
            policy[s][a] = x[s * actions + a];
        }
    }
    
    *obj_value = 0.0;
    
    for (size_t k =0; k < p->mdps->scenarios; ++k) {
        double *gain = bmdp_evaluate_avg_stationary_policy(p->mdps->matrices[k], policy, p->mdps->rewards[k], p->gamma, FALSE);
        for (size_t s = 0; s < ord; ++s) {
            *obj_value += p->weights->weights[k] * p->mdps->iniv[s] * gain[s];
        }
        free(gain);
    }
#else
    gsl_vector_view xv = gsl_vector_view_array(x, n);
    *obj_value = nlp_helper_func_f(&xv, user_data);
#endif    
    return 1;
}

Bool eval_grad_f(Index n, Number *x, Bool new_x, Number *grad_f, UserDataPtr user_data)
{
    nlp_params *p = (nlp_params *) user_data;
    gsl_vector *g = gsl_vector_alloc(n);
    gsl_vector *xv = gsl_vector_alloc(n);
    
    for (size_t i = 0; i < n; ++i) {
        gsl_vector_set(xv, i, x[i]);
    }
    
    nlp_helper_func_df(xv, user_data, g);
    
    for (size_t i = 0; i < n; ++i) {
        grad_f[i] = gsl_vector_get(g, i);
    }
    
    gsl_vector_free(g);
    gsl_vector_free(xv);
    
    return 1;
}

Bool eval_g(Index n, Number *x, Bool new_x, Index m, Number *g, UserDataPtr user_data)
{
    nlp_params *p = (nlp_params *) user_data;
    const size_t actions = p->mdps->matrices[0]->rows[0]->pord;
    
    for (size_t s = 0; s < m; ++s) {
        double sum_d_s = 0.0;
        for (size_t a = 0; a < actions; ++a) {
            const size_t d_s_a_index = s * actions + a;
            const double d_s_a = x[d_s_a_index];
            sum_d_s += d_s_a;
        }
        g[s] = sum_d_s;
    }
    
    return 1;
}

Bool eval_jac_g(Index n, Number *x, Bool new_x, Index m, Index nele_jac, Index *iRow, Index *jCol, Number *values, UserDataPtr user_data)
{
    /* evaluate Jacobian of the constraints, that is, dg_{i}/dx_{j} */
    nlp_params *p = (nlp_params *) user_data;
    const size_t actions = p->mdps->matrices[0]->rows[0]->pord;
    
    if (values == NULL) {
        for (size_t s = 0; s < m; ++s) {
            for (size_t a = 0; a < actions; ++a) {
                const size_t var_index = s * actions + a;
                const size_t run_index = s * actions + a;
                const size_t constraint_index = s;
                iRow[run_index] = constraint_index;
                jCol[run_index] = var_index;
            }
        }
    } else {
        for (size_t s = 0; s < m; ++s) {
            for (size_t a = 0; a < actions; ++a) {
                const size_t var_index = s * actions + a;
                const size_t run_index = s * actions + a;
                const size_t constraint_index = s;
                values[run_index] = 1.0; /* d g_{i} / d x_{j} */
            }
        }
    }    
    return 1;
}

Bool eval_h(Index n, Number *x, Bool new_x, Number obj_factor, Index m, Number *lambda, Bool new_lambda, Index nele_hess, Index *iRow, Index *jCol, Number *values, UserDataPtr user_data)
{
    /* this function will not be implemented beyond "return false" */
    return 0;
}

value_vectors concurrent_nlp_ipopt(concurrent_mdp mdps, weight_tuple weights, double **policy, double gamma, short comp_max, double *value, short presolved)
{
    /* An NLP formulation. Solved with Ipopt. Should be slow. */
    const size_t ord = mdps.matrices[0]->ord;
    const size_t actions = mdps.matrices[0]->rows[0]->pord;
    const size_t ipopt_num_variables = ord * actions;

    value_vectors gain;
    gain.scenarios = weights.scenarios;
    gain.ord = ord;
    gain.gains = calloc(weights.scenarios, sizeof(double *));

    /* use Ipopt */
    Index n = (int) ipopt_num_variables; /* number of variables */
    Index m = (int) ord; /* number of constraints */
    Number *x_L = (Number *) calloc(n, sizeof(Number));
    Number *x_U = (Number *) calloc(n, sizeof(Number));
    Number *g_L = (Number *) calloc(m, sizeof(Number));
    Number *g_U = (Number *) calloc(m, sizeof(Number));

    IpoptProblem nlp = NULL;
    enum ApplicationReturnStatus ipopt_status;

    Number *xv = (Number *) calloc(n, sizeof(Number));
    Number *mult_x_L = (Number *) calloc(n, sizeof(Number));
    Number *mult_x_U = (Number *) calloc(n, sizeof(Number));
    Number *mult_g = (Number *) calloc(m, sizeof(Number));

    for (Index i = 0; i < n; ++i) {
        x_L[i] = 0.0;
        x_U[i] = 1.0;
        if (presolved) {
            xv[i] = policy[(size_t) i / actions][(size_t) i % actions];
        } else {
            xv[i] = 1.0 / actions;
        }
    }

    for (Index i = 0; i < m; ++i) {
        g_L[i] = 1.0;
        g_U[i] = 1.0;
    }

    nlp = CreateIpoptProblem(n, x_L, x_U, m, g_L, g_U, ord * actions, n * (n + m), 0, eval_f, eval_g, eval_grad_f, eval_jac_g, eval_h);
    free(x_L);
    free(x_U);
    free(g_L);
    free(g_U);
    
    nlp_params params;
    params.mdps = &mdps;
    params.weights = &weights;
    params.gamma = gamma;
    params.comp_max = comp_max;
    
    AddIpoptNumOption(nlp, "tol", 1e-7);
    AddIpoptStrOption(nlp, "mu_strategy", "adaptive");
    AddIpoptStrOption(nlp, "output_file", "ipopt.out");
    AddIpoptStrOption(nlp, "hessian_approximation", "limited-memory");
    AddIpoptStrOption(nlp, "check_derivatives_for_naninf", "yes");
    AddIpoptNumOption(nlp, "max_cpu_time", CMDP_MAX_TIME);
    AddIpoptIntOption(nlp, "max_iter", 10000.0);
    AddIpoptIntOption(nlp, "print_level", 3);
    
    if(comp_max) {
        SetIpoptProblemScaling(nlp, -1.0, NULL, NULL);
        AddIpoptStrOption(nlp, "nlp_scaling_method", "user-scaling");
    }
    ipopt_status = IpoptSolve(nlp, xv, NULL, value, mult_g, mult_x_L, mult_x_U, &params);
    
    if (ipopt_status != Solve_Succeeded) {
        fprintf(stderr, "ipopt was not successful for reason %d (value = %f)\n", ipopt_status, *value);
    }
    
    /* read values */
    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            policy[s][a] = xv[s * actions + a];
        }
    }
    
    gsl_vector_view iniv = gsl_vector_view_array(mdps.iniv, ord);
    double check_value = 0.0;
    for (size_t k =0; k < mdps.scenarios; ++k) {
        gain.gains[k] = bmdp_evaluate_avg_stationary_policy(mdps.matrices[k], policy, mdps.rewards[k], gamma, FALSE);

        double dot;
        gsl_vector_view value = gsl_vector_view_array(gain.gains[k], ord);
        gsl_blas_ddot(&iniv, &value, &dot);
        check_value += weights.weights[k] * dot;
        //fprintf(stderr, "value in scenario %zu: %f\n", k, dot);
    }
    *value = check_value;
    //fprintf(stderr, "value: %f\n", check_value);
    
    FreeIpoptProblem(nlp);
    free(xv);
    free(mult_g);
    free(mult_x_L);
    free(mult_x_U);

    return gain;
}

value_vectors concurrent_nlp(concurrent_mdp mdps, weight_tuple weights, double **policy, double gamma, short comp_max, double *value, __attribute__((unused)) short presolved)
{
    /* An NLP formulation. Solved with GSL/NLOPT. Should be slow. */
    const size_t ord = mdps.matrices[0]->ord;
    const size_t actions = mdps.matrices[0]->rows[0]->pord;
    const size_t num_variables = ord * actions;// + ord;
 

    /* use GSL */
    gsl_multimin_function_fdf fdf;
    const gsl_multimin_fdfminimizer_type *T = gsl_multimin_fdfminimizer_vector_bfgs2;
    nlp_params params;
    params.mdps = &mdps;
    params.weights = &weights;
    params.gamma = gamma;
    params.comp_max = comp_max;
    fdf.params = &params;
    fdf.n = num_variables;
    fdf.f = &nlp_helper_func_f;
    fdf.df = &nlp_helper_func_df;
    fdf.fdf = &nlp_helper_func;
    
    value_vectors gain;
    gain.scenarios = weights.scenarios;
    gain.ord = ord;
    gain.gains = calloc(weights.scenarios, sizeof(double *));

#ifdef NLOPT
    /* initialize minimizer */
    nlopt_opt optimizer = nlopt_create(NLOPT_LD_SLSQP, num_variables);
    
    nlopt_opt local_opt = nlopt_create(NLOPT_LD_SLSQP, num_variables);
    nlopt_set_xtol_rel(local_opt, 1e-7);
    nlopt_set_ftol_rel(local_opt, 1e-7);
    nlopt_set_initial_step1(local_opt, 1e-7);
    
    // nlopt_set_local_optimizer(optimizer, local_opt);
    
    /* add linear equality constraints */
    nlp_constraint_params *lineq_params = calloc(ord, sizeof(nlp_constraint_params));
    for (size_t s = 0; s < ord; ++s) {
        lineq_params[s].actions = actions;
        lineq_params[s].ord = ord;
        lineq_params[s].s = s;
        nlopt_add_equality_constraint(optimizer, nlp_linear_constraint, (void *) &(lineq_params[s]), 1e-8);
        //nlopt_add_inequality_constraint(optimizer, nlp_linear_constraint, (void *) &(lineq_params[s]), 1e-8);
        //nlopt_add_inequality_constraint(optimizer, nlp_neg_linear_constraint, (void *) &(lineq_params[s]), 1e-8);
    }
    
    if (comp_max) {
        nlopt_set_max_objective(optimizer, nlp_fdf, (void *) &params);
    } else {
        nlopt_set_min_objective(optimizer, nlp_fdf, (void *) &params);
    }
    
    nlopt_set_lower_bounds1(optimizer, 0.0);
    nlopt_set_upper_bounds1(optimizer, 1.0);
    
    /* set convergence criteria */
    nlopt_set_xtol_rel(optimizer, 1e-7);
    
    /* optimize */
    /* create an initial point */
    double *x = calloc(num_variables, sizeof(double));
    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            x[s * actions + a] = 1.0 / actions;
        }
    }
    
    /* call to the optimizer */
    nlopt_result status = nlopt_optimize(optimizer, x, value);
    if (status < 0) {
        if (status == NLOPT_ROUNDOFF_LIMITED) {
            fprintf(stderr, "nlopt reached its precision.\n");
        } else {
            fprintf(stderr, "nlopt failed!\n");
        }
    }
    
    /* read values */
    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            policy[s][a] = x[s * actions + a];
        }
    }
    nlopt_destroy(optimizer);
    nlopt_destroy(local_opt);

#else /* use GSL */
    gsl_multimin_fdfminimizer *minimizer = gsl_multimin_fdfminimizer_alloc(T, num_variables);
    /* starting point */
    gsl_vector *x = gsl_vector_alloc(num_variables);
    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            double b = (a == 0) ? 1.0 : 0.0;
            gsl_vector_set(x, s * actions + a, b);
        }
    }
    gsl_multimin_fdfminimizer_set(minimizer, &fdf, x, 1e-1, 1e-4);

    int status;
    size_t iter = 0;

    do {
        iter++;
        status = gsl_multimin_fdfminimizer_iterate(minimizer);

        if (status) {
            break;
        }

        status = gsl_multimin_test_gradient(minimizer->gradient, 1e-5);

        if (status == GSL_SUCCESS) {
            printf("Optimum found at:\n");
        }

        *value = minimizer->f;
        if (comp_max) {
            *value = -(*value);
        }
        /* printf("%5lu %10.5f\n", iter, *value); */

    } while (status == GSL_CONTINUE && iter < 1000);

    gain.scenarios = weights.scenarios;
    gain.ord = ord;
    gain.gains = calloc(weights.scenarios, sizeof(double *));

    for (size_t s = 0; s < ord; ++s) {
        double policy_mass = 0.0;
        for(size_t a = 0; a < actions; ++a) {
            const size_t index = s * actions + a;
            policy_mass += gsl_vector_get(minimizer->x, index);
        }
        for(size_t a = 0; a < actions; ++a) {
            const size_t index = s * actions + a;
            policy[s][a] = gsl_vector_get(minimizer->x, index) / policy_mass;
        }
    }
    gsl_multimin_fdfminimizer_free(minimizer);
#endif /* NLOPT */
    
    for (size_t k =0; k < mdps.scenarios; ++k) {
        gain.gains[k] = bmdp_evaluate_avg_stationary_policy(mdps.matrices[k], policy, mdps.rewards[k], gamma, FALSE);
    }

    return gain;
}

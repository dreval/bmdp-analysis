/*********************
        Datei:  bmdp_func.h
        Datum:  1.3.15
        Autor:  Peter Buchholz
        Inhalt: Externe Deklarationen fuer bmdp_func.c
        Aenderungen:

**********************/
#ifndef BMDP_FUNC_H
#define BMDP_FUNC_H

#include "tst_ctmdp_stat.h"


/* declaration of functions */

extern void bmdp_initial_policy(size_t *ind, Mdp_matrix *mdp);
extern double bmdp_direct_eval_policy(Sparse_matrix *, Double_vektor *, double *);
extern double bmdp_precond_eval_policy(int method, Sparse_matrix *pp, Sparse_matrix **lu2, Double_vektor *b, double *hh,
        short found, int local_it, double local_eps, int *global_it);

double *bmdp_evaluate_avg_stationary_policy(Mdp_matrix *mdp, double **policy, Double_vektor **rew, double gamma, short comp_stat);

double *bmdp_low_level_evaluate_avg_stationary_policy(size_t ord, size_t *num_actions, Row ***rows, double **diags, Double_vektor **rew,
                                                      double **policy, double gamma);

/**********************
bmdp_gen_precond_structure
Datum:  03.03.15
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a Sparse_matrix data structure that can be used to include
a transition matrix resulting from map_matrix mdp and an arbitrary decision vector
for preconditioned iteration techniques that contains an additional state for the 
normalization condition. 
**********************/
extern Sparse_matrix *bmdp_gen_precond_structure (Mdp_matrix const * restrict mdp) ;

/**********************
bmdp_set_reward
Datum:  02.03.15
Autor:  Peter Buchholz
Input:  rew Vektors with policy abh�ngigen Rewards
        ind Indizes der Aktionen in dne Zust�nden
        
Output: hh Vektor mit zustandsahb�ngigen Rewards
Side-Effects:
Description:
Die Funktion bestimmt die Rewards in dne Zust�nden in Abh�ngigkeit von der gew�hlten 
Aktion. 
***********************************************************************/
extern int bmdp_set_reward(Double_vektor const ** restrict rew, double *hh, size_t *ind) ;

/**********************
bmdp_set_extreme_values(
Datum:  03.03.15
Autor:  Peter Buchholz
Input:  mdp_min, mdp_max Bounding MDP matrices
        hh gain vector
        ind politics
        hh auxiliary vectors
        compmin if TRUE compute the worst case, else the best case
Output: pp matrix of the politics
Side-Effects:
Description:
The function computes the worst/best case matrix for a politics.
***********************************************************************/
extern void bmdp_set_extreme_values(Mdp_matrix const * restrict mdp_min, Mdp_matrix const * restrict mdp_max, Sparse_matrix *pp, size_t const * restrict ind,
                    double const * restrict hh, double *vv, double gamma, short compmin) ;

 /**********************
bmdp_set_strategy
Datum:  02.03.15
Autor:  Peter Buchholz
Input:  mdp MDP matrix
        ind decision vector
        gamma Diskontierungsfaktor
Output: pp Matrix resulting from decision vector
Side-Effects:
Description:
The function generates the matrix for a given decision vector for preconditioned
iteration methods. Matrix pp has to adequately allocated e.g. by the function 
bmdp_gen_precond_structure. 
**********************/
extern void bmdp_set_strategy(const Mdp_matrix * restrict mdp, Sparse_matrix *pp, size_t const * restrict ind, double gamma) ;

/**********************
bmdp_set_minmax_row
Datum:  03.03.15
Autor:  Peter Buchholz
Input:  min_row max_row Bounding mdp rows
        my_row index of teh current row
        pol current politics
        hh gain vector
        vv help vector
        compmin true for worst fals for best case
Output: prow non diagonal entries of teh row
        diag diagonal entry of teh new row
Side-Effects:
Description:
The function computes a new ploictis and th ecorrepsonding matrix and reward vector
for a BMDP. one row for polictis pol.
***********************************************************************/
extern void bmdp_set_one_minmax_row(Mdp_row const * restrict min_row, Mdp_row const * restrict max_row, Row *prow, double *diag, size_t my_row,
                    size_t pol, double const * restrict hh, double *vv, short compmin) ;


/**********************
bmdp_policy_iteration
Datum:  25.03.10
Autor:  Peter Buchholz
Input:  pl, pu Bounding matrices
        ds Vektor der Zeilensummen
        rew Rewardvektoren
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maixmale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet den durchschnittlichen abgezinsten Gewinn mit einem dirketen Verfahren.
***********************************************************************/
extern double *bmdp_policy_iteration(const Mdp_matrix *mdp, const Double_vektor **rew, size_t *ind, int max_iter,
				     double ti_max, double epsilon, double gamma, short comp_max, short comp_stat) ;

/**********************
bmdp_precond_iteration
Datum:  02.02.15
Autor:  Peter Buchholz
Input:  mdp MDp matrix
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maximale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke für den Reward mit Hilfe einer iterativen Technik
und ILU Pr�konditionierung.
***********************************************************************/
extern double *bmdp_precond_iteration(const Mdp_matrix *mdp, const Double_vektor **rew, size_t *ind, int max_iter, double ti_max,
				      double epsilon, int local_it, double local_eps, int method, double gamma, short comp_max, short comp_stat) ;

/**********************
bmdp_evaluate_avg_policy
Datum:  13.03.15
Autor:  Peter Buchholz
Input:  mdp MDP matrices
        ind Policy
        reward vectors for different actions
        gamma discount factor
        comp_stat TRUE if stationary result should be computed
        avg TRUE if teh average matrix is used oterhwhise bounding matrices are used
Output: return value gain vector
Side-Effects:
Description:
The function evaluates the policy ind and returns the average gain vector. If 
comp_stat is set a stationary analysis is performed and the stationary gain is printed.
***********************************************************************/
extern double *bmdp_evaluate_avg_policy(Mdp_matrix const * restrict mdp, size_t const * restrict ind, Double_vektor const ** restrict rew, double gamma, short comp_stat) ;

/**********************
bmdp_compute_extreme_case
Datum:  03.03.15
Autor:  Peter Buchholz
Input:  mdp_min, mdp_max Bounding MDP matrices
        rew reward vector
        ind politics
        max_iter, local maximal number of global/local iterations
        ti_max, local_it maximal local/global solution time
        epsilon, local_eps epsilon global/local
        method solution method if size > BMDP_SIZE_DIRECT
        gamma discount factor
        compmin if TRUE compute the worst case, else the best case
Output: return value gain vector
        ind optimal policy
Side-Effects:
Description:
The function computes the worst/best case gain vector and policy for a BMDP.
***********************************************************************/
extern double *bmdp_compute_extreme_case(Mdp_matrix *mdp_min, Mdp_matrix *mdp_max, const Double_vektor **rew, size_t *ind, int max_iter,
        double ti_max, double epsilon, int local_it, double local_eps, int method, double gamma,
        short compmin, short comp_max, short comp_stat) ;


/**********************
bmdp_evaluate_avg_policy
Datum:  13.03.15
Autor:  Peter Buchholz
Input:  mdp MDP matrices
        ind Policy
        reward vectors for different actions
        gamma discount factor
        comp_stat TRUE if stationary result should be computed
        avg TRUE if teh average matrix is used oterhwhise bounding matrices are used
Output: return value gain vector
Side-Effects:
Description:
The function evaluates the policy ind and returns the average gain vector. If 
comp_stat is set a stationary analysis is performed and the stationary gain is printed.
***********************************************************************/
extern double *bmdp_evaluate_minmax_policy(Mdp_matrix const *restrict mdp_min, Mdp_matrix const *restrict mdp_max, size_t const *restrict ind, Double_vektor const **restrict rew,
                                           double gamma, short compmin, short comp_stat);

#endif /* BMDP_FUNC_H */

/*
 * A tiny Bloom filter implementation
 * Nothing very special, just a fast data structure for more or less sparse sets
 */
#ifndef BLOOM_H
#define BLOOM_H
#include <math.h>
#include <stddef.h>

#define BYTE_WIDTH 8 // assume a sane machine

typedef size_t (*hash_function) (void *, size_t);

/**
  * A structure for Bloom filters
  * Contains (pointers to) hash functions and a bitmap.
  **/
typedef struct bloom_filter_t {
    size_t num_hash_functions;
    hash_function *hash_functions; // resort to void * since declaring a pointer to function pointers seems hard
    char *bitmap;
} bloom_filter;

/** Creates a Bloom filter.
 * @brief bloom_filter_new Creates a Bloom filter.
 * @param num_hash_functions The number of hashing functions involved in the filter.
 * @param hash_functions An array of hashing functions. They should have type int hash(void *obj) and the index should always fit into the bitmap size
 * @param size The size of the bitmap
 * @return The Bloom filter structure
 */
bloom_filter bloom_filter_new(size_t num_hash_functions, hash_function *hash_functions, size_t size);

/** Creates a Bloom filter with automatic size.
 * @brief bloom_filter_new_auto Creates a Bloom filter with automatic size.
 * @param num_hash_functions The number of hashing functions involved in the filter.
 * @param hash_functions An array of hashing functions. They should have type int hash(void *obj) and the index should always fit into the bitmap size
 * @param num_items The size of the set we would like to represent
 * @param false_positive_rate The desired false positive rate.
 * @return
 */
bloom_filter bloom_filter_new_auto(size_t num_hash_functions, hash_function *hash_functions, size_t num_items, double false_positive_rate);

/**
 * @brief bloom_filter_add Add an item to a Bloom filter.
 * @param filter The filter data structure.
 * @param item The item that should be added
 */
void bloom_filter_add(bloom_filter filter, void *item, size_t ord);

/**
 * @brief bloom_filter_query Query a Bloom filter for an item
 * @param filter The filter data structure
 * @param item The item to be queried for
 * @return 1, if the item (probabilisitcally) is contained in the structure, 0 otherwise
 */
int bloom_filter_query(bloom_filter filter, void *item, size_t ord);

/**
 * @brief bit_query Query a bit in a bitmap
 * @param bitmap The bitmap, represented as a character array
 * @param index The index of the bit
 * @return The bit value
 */
int bit_query(char *bitmap, size_t index);

/**
 * @brief bit_set Set a bit in a bitmap
 * @param bitmap The bitmap, represented as a character array
 * @param index The index of the bit
 * @param value The bit value
 */
void bit_set(char *bitmap, size_t index, int value);

/**
 * @brief bitmap_index Helper function to determine the correct bit
 * @param bitmap The bitmap structure, as a character array
 * @param index The bit index
 * @param mask return parameter: The bit query mask
 * @return The byte index
 */
size_t bitmap_index(char *bitmap, size_t index, char *mask);

static size_t num_hash_functions = 16l;

static size_t **hash_coeff;

hash_function *hash_functions;

void init_hash_functions(size_t ord);

size_t universal_hash_function(size_t *policy, size_t ord, size_t index);
#endif /* BLOOM_H */

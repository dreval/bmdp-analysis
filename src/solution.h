/*********************
        Datei:  solution.h
        Datum:  17.01.04
        Autor:  Peter Buchholz (modified by Tugrul Dayar)
        Inhalt: Datenstrukturen
                Funktionskoepfe
                globale Variablen
                fuer die Analyse von SANs 

Side-effects: 

**********************/
#ifndef SOLUTION_H
#define SOLUTION_H

#include "global.h"

/*** lokale Konstanten fuer das ML Verfahren ***/
#define LOCAL_ITER 5  /* Anzahl Iterationen in jeder Ebene */
#define AGG_SIZE   5  /* Anzahl aggregierte Zustanede */
 

/***** exportierte Funktionen *****/

/**********************
sol_matrix_dim 
Datum:  19.4.95
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: Funktionswert Spaltendiemsnion der Matrix
Side-Effects:
Description:
Die Funktion bestimmt den maximalen Spaltenindex eines Nichtnullelelements.
**********************/
#ifndef CC_COMP
extern int sol_matrix_dim(Sparse_matrix *q) ;
#else
extern int sol_matrix_dim() ;
#endif

/**********************
modify_matrix
Datum:  16.06.05
Autor:  Tugrul Dayar
Input:  q Sparse Matrix
Output: returns a new matrix which has q with its last column set to 1s
Side-Effects:
Description: 
**********************/
#ifndef CC_COMP
extern Sparse_matrix *modify_matrix(Sparse_matrix *q) ;
#else
extern Sparse_matrix *modify_matrix() ;
#endif

/**********************
sol_copy_to_transpose
Datum:  19.4.95
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: Funktionswert transponierte Matrix wobei letzte Zeile durch
        den Einheitsvektor ersetzt wurde
Side-Effects:
Description:
Die Funktion transponiert eine spaerlich besetzte Matrix und ersetzt die
letzte Zeile durch den Einheitsvektor.
**********************/
#ifndef CC_COMP
extern Sparse_matrix *sol_copy_to_transpose(Sparse_matrix *q) ;
#else
extern Sparse_matrix *sol_copy_to_transpose() ;
#endif

/**********************
sol_factorise ##e LU Faktorisierung einer Matrix
Datum:  19.4.95
Autor:  Peter Buchholz
Input:  a Sparse Matrix
Output: a LU Faktoren der Matrix
Side-Effects:
Description:
Die Funktion fuehrt eine LU Faktorisierung einer Sparse Matrix durch. Die
Dreiecksmatrizen L und U werdne ind die urspruengliche Datenstruktur
geschrieben. Tritt eine Fehler bei der Faktorisierung auf, so wird 1
zurueckgegeben, ansonsten NO_ERROR (= 0).
**********************/
#ifndef CC_COMP
extern int sol_factorise(Sparse_matrix *a) ;
#else
extern int sol_factorise() ;
#endif

/**********************
sol_substitution 
Datum:  19.4.95
Autor:  Peter Buchholz
Input:  a LU Faktoren einer Sparse Matrix
Output: v Loesungsvektor
Side-Effects:
Description:
Die Funktion fuehrt die Ruecksubstitution  mit der faktorisierten Matrix
aus. 
**********************/
#ifndef CC_COMP
extern int sol_substitution(Sparse_matrix *a, Double_vektor *v) ;
#else
extern int sol_substitution() ;
#endif

/**********************
sol_direct_solution ##e direkte Loesung des aggregierten Systems
Datum:  17.01.04
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  q Sparse Matrix als Koeffizientenmatrix
        transpose bei > 0 wird die Matrix transponiert und die letzte Zeile
                  der transponierten Matrix durch den Einheitsvektor ersetzt,
                  ansonsten wird das Gleichungssystem mit der gegebenen 
                  Koeffizientenmatrix geloest
        rhs right-hand side vector
Output: v Loesungsvektor muss ein Vektor ausreichender Laenge sein
Side-Effects:
Description:
Die Funktion loest ein Gleichungssystem mit Hilfe der LU Faktorisierung.
Tritt ein Fehler bei der Loesung auf, so wird ein gleichverteilter Vektor
zurueckgegeben und der Funktionswert auf NO_CONVERGENCE gesetzt. Ansonsten
wird der Loesungsvektor und Funktionswert NO_ERROR zurueckgegeben.
**********************/
#ifndef CC_COMP
extern int sol_direct_solution 
            (Sparse_matrix *q, Double_vektor *v, int transpose, 
             Double_vektor *rhs) ;
#else
extern int sol_direct_solution () ;
#endif

/**********************
sol_solve_general_system
Datum:  21.01.03 
Autor:  Peter Buchholz
Input:  q faktoriserte Matrix 
        r rechtsseitiger Vektor
Output: v Lösungsvektor
Side-Effects:
Description:
Die Funktion berechnet die Lösung des Systems vQ=r mit einer direkten Methode.
q muss dazu faktorisiert und die l und u Werte gestezt sein.
Vorsicht: q wird verändert!
***********************************************************************/
#ifndef CC_COMP
extern int sol_solve_general_system (Sparse_matrix *q, Double_vektor *r, Double_vektor *v) ;
#else
extern int sol_solve_general_system () ;
#endif

/**********************
sol_invert_matrix
Datum:  21.1.03
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: Funktionswert invertierte Matrix
Side-Effects:
Description:
Die Funktion berechnet die Inverse einer Matrix unter Verwendung der
LU-Faktorisierung. Vorsicht q wird verändert!!
***********************************************************************/
#ifndef CC_COMP
extern Sparse_matrix *sol_invert_matrix(Sparse_matrix *q) ;
#else
extern Sparse_matrix *sol_invert_matrix() ;
#endif

/**********************
sol_power_iteration
Datum:  19.01.04
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  q Koeffizientenmatrix
        v1 Startvektor
        param Loesungsparameter
        trc bei >0 werden Tracemeldungen waehrend der Iteration ausgegeben
        rhs right-hand side vector
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der Power Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und v1 einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit und
die Fehlerschranke werdne in param angegeben.
**********************/
#ifndef CC_COMP
extern int sol_power_iteration
     (Sparse_matrix *q, Double_vektor *v1, Solution_param *param, int trc,
      Double_vektor *rhs) ;
#else
extern int sol_power_iteration () ;
#endif

/**********************
sol_jor_iteration
Datum:  19.01.04
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  q Koeffizientenmatrix
        v1 Startvektor
        param Loesungsparameter
        outp TRUE um Ausgaben einzuschalten
        rhs right-hand side vector
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der JOR Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und v1 einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit, der
Relaxationsparameter und die Fehlerschranke werdne in param angegeben.
**********************/
#ifndef CC_COMP
extern int sol_jor_iteration
   (Sparse_matrix *q, Double_vektor *v1, Solution_param *param, short outp,
    Double_vektor *rhs) ;
#else
extern int sol_jor_iteration() ;
#endif

/**********************
sol_sor_iteration
Datum:  19.01.04
Autor:  Peter Buchholz (modified by Tugrul Dayar)
Input:  q Koeffizientenmatrix
        v1 Startvektor
        param Loesungsparameter
        transpose TRUE falls Eingabematrix transpoiniert werden muss
        outp TRUE um Ausgaben einzuschalten
        rhs right-hand side vector
Output: v1 Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der SOR Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und v1 einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit, der
Relaxationsparameter und die Fehlerschranke werden in param angegeben.
**********************/
#ifndef CC_COMP
extern int sol_sor_iteration
  (Sparse_matrix **q, Double_vektor *v1, Solution_param *param, 
   short transpose, short outp, Double_vektor *rhs) ;
#else
extern int sol_sor_iteration() ;
#endif

/**********************
sol_comp_preconditioner
Datum:  20.05.03
Autor:  Peter Buchholz
Input:  a Generatormatrix
        pc Auswahl Praekonditionierer (1 ILU0, 2 ILUTH)
        th Threshold fuer ILUTH
Output: Rueckgabewert unvollstanedige LU Zerlegung von a
Side-Effects:
Description:
Die Funktion berechnet unvollstaendige LU Faktoren fuer Matrix a. Es werden die
Praekonditionierer ILU0 und ILUTh mit Threshold th unteresteutzt.
***********************************************************************/
#ifndef CC_COMP
extern Sparse_matrix *sol_comp_preconditioner
(Sparse_matrix *a, int pc, double th) ;
#else
extern Sparse_matrix *sol_comp_preconditioner () ;
#endif

/**********************
sol_substitute_precond
Datum:  31.08.98
Autor:  Peter Buchholz
Input:  lu LU-Faktoren des Praekonditionierers
        p Vektor zur Multiplikation mit dem Praekonditionierer
Output: p Loesungsvektor

Side-Effects:
Description:
Die Funktion loest p (LU)^{-1}= p U^{-1}L^{-1}.
**********************/
#ifndef CC_COMP
extern void sol_substitute_precond (Sparse_matrix *lu, Double_vektor *p)  ;
#else
extern void sol_substitute_precond () ;
#endif

/**********************
sol_substitute_precond_transpose
Datum:  17.06.10
Autor:  Peter Buchholz
Input:  lu LU-Faktoren des Praekonditionierers
        p Vektor zur Multiplikation mit dem Praekonditionierer
Output: p Loesungsvektor

Side-Effects:
Description:
Die Funktion loest  (LU)^{-1}p=  U^{-1}L^{-1}p.
**********************/
#ifndef CC_COMP
extern void sol_substitute_precond_transpose (Sparse_matrix *lu, Double_vektor *p) ;
#else
extern void sol_substitute_precond_transpose () ;
#endif

/**********************
sol_gmres
Datum:  12.7.95
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        p Startvektor
        param Loesungsparameter
        pc Auswahl des Praekonitionierer (0=ohne, )
Output: p Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der iterativen GMRES Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und p einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit, der
Relaxationsparameter und die Fehlerschranke werden in param angegeben. Der Paramter pc
dient zur Auswahl des verwendeten Praekonditionierers. Die Anzahl der lokalen
Iterationschritte (= Dimension des Krylov Unterraums) ist in der Konstanten
SUBSPACE_DIM definiert.
**********************/
#ifndef CC_COMP
extern int  sol_gmres(Sparse_matrix *q, Double_vektor *p, Solution_param *param, int pc) ;
#else
extern int sol_gmres() ;
#endif

/**********************
sol_bicgstab
Datum:  31.08.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        p Startvektor
        param Loesungsparameter
        pc Auswahl des Praekonitionierer (0=ohne, )
Output: p Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest ein durch die Matrix q gegebenes Gleichungssystem mit
Hilfe der iterativen BiCGSTAB Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und p einen Startvektor. Die maximale Iterationszahl, maximale CPU Zeit, der
Relaxationsparameter und die Fehlerschranke werden in param angegeben. Der Paramter pc
dient zur Auswahl des verwendeten Praekonditionierers. 
**********************/
#ifndef CC_COMP
extern int  sol_bicgstab(Sparse_matrix *q, Double_vektor *p_ini, Solution_param *param, int pc) ;
#else
extern  int sol_bicgstab() ;
#endif

/**********************
solve_sparse_system
Datum:  29.5.95
Autor:  Peter Buchholz
Input:  param Loesungsparameter
        q Koeffizientenmatrix
        partition Blockstruktur auf der Matrix
        p initialer Vektor
        ini falls != 0 wird Speicherplatz fuer den Vektor allokiert
            und der Startvektor als Gleichverteilung gewaehlt, ansonsten wird 
           der uebergebene Vektor als Startvektor verwendet
Output: p Loesungsvektor
        param Anzahl ausgefuehrter Iterationen, verbrauchte CPU-Zeit und
              Maximumnorm des Residualvektors.
Side-Effects:
Description:
Die Funktion loest das durch die Matrix q definierte Gleichungssystem mittels
einer der Methoden LU-Faktorisierung, Power Methode, JOR oder SOR. Der
Loesungsalgorithmus und zuegehoerige Parameter werden in param uebergeben. 
Vorsicht: Bei Verwendung von SOR wird die Matrix q transponiert!!!
**********************/
#ifndef CC_COMP
extern void solve_sparse_system (Solution_param *param, Sparse_matrix *q, 
				 Int_vektor *partition, Double_vektor **p, int ini) ; 
#else
extern void solve_sparse_system () ;
#endif

/**********************
sol_iterative_solve
Datum:  12.05.98
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        v1 Startvektor
        b rechseitiger Vektor
        epsilon Fehlerschranke
Output: v1 Loesungsvektor
Side-Effects:
Description:
Die Funktion loest das Gleichungssystem xA = b iterative.
**********************/
#ifndef CC_COMP
extern void sol_iterative_solve
             (Sparse_matrix *q , Double_vektor *v1, Double_vektor *b, double epsilon) ;
#else
extern void sol_iterative_solve() ;
#endif

/**********************
sol_general_bicgstab
Datum:  17.06.10
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        lu Präkonditinerer falls NULL ohne
        p Startvektor
        b rechte Seite
        max_iter maximale Anzahl iterationen
        epsilon Fehlerschranke
Output: p Loesungsvektor

Side-Effects:
Description:
Die Funktion loest das Gleichungssystem Q*p = b gegebenes Gleichungssystem mit
Hilfe der iterativen BiCGSTAB Methode. Es wird engenommen, dass q eine Q-Matrix beinhaltet
und p einen Startvektor. 
**********************/
#ifndef CC_COMP
extern int  sol_general_bicgstab(Sparse_matrix *q, Sparse_matrix *lu, Double_vektor *p_ini, Double_vektor *b, int max_iter, double epsilon, int *global_it) ;
#else
extern int sol_general_bicgstab() ;
#endif

/**********************
sol_general_gmres
Datum:  23.06.10
Autor:  Peter Buchholz
Input:  q Koeffizientenmatrix
        lu Präkonditinerer falls NULL ohne
        p Startvektor
        b rechte Seite
        max_iter maximale Anzahl iterationen
        epsilon Fehlerschranke
Output: p Loesungsvektor

Side-Effects:
Description:
Die Funktion loest ein Gleichungssystem Qx = b mit
Hilfe der iterativen GMRES Methode. Es wird engenommen, dass q die Koeffizientenmatrix beinhaltet
und p den Startvektor. Die maximale Iterationszahl,  die Fehlerschranke sowie der Praekonditionierer
werden als Parameter uebergeben. Die Anzahl der lokalen
Iterationschritte (= Dimension des Krylov Unterraums) ist in der Konstanten
SUBSPACE_DIM definiert.
**********************/
#ifndef CC_COMP
int  sol_general_gmres(Sparse_matrix *q, Sparse_matrix *lu, Double_vektor *p, Double_vektor *b, int max_iter, double epsilon, int *global_it);
#else
  int sol_general_gmres() ;
#endif

/************* Schalter fuer Testausgaben ********/
#define SOLUTION_TEST1 FALSE 

#define SOLUTION_TEST2 FALSE

#define SOLUTION_TEST3 FALSE 

#endif

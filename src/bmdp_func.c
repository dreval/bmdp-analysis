/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c (C) Copyright 2015 by Peter Buchholz
c
c
c     All Rights Reserved
c
c bmdp_func.c
c
c Functions to analyze discounted rewards for BMDPs
c
c
c       Datum           Modifikationen
c       ----------------------------------------------------------
c       01.03.15 Datei angelegt aus tst_ctmdp_stat.c
c       02.03.15 Entscheidungsabh�ngige Rewards eingef�hrt
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/

#include "bmdp_const.h"
#include "ctmdp_stat_func.h"
#include "bmdp_func.h"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_linalg.h>
#include <omp.h>

#define USE_SVD FALSE /* Use SVD for direct solution in policy iteration */

/**********************
bmdp_gen_precond_structure
Datum:  03.03.15
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a Sparse_matrix data structure that can be used to include
a transition matrix resulting from map_matrix mdp and an arbitrary decision vector
for preconditioned iteration techniques that contains an additional state for the 
normalization condition. 
**********************/
Sparse_matrix *bmdp_gen_precond_structure(Mdp_matrix const * restrict mdp) {
    size_t i, j, k;
    Sparse_matrix *q;

    q = mat_sparse_matrix_new((unsigned) mdp->ord);
    for (i = 0; i < mdp->ord; i++) {
        k = 0;
        for (j = 0; j < mdp->rows[i]->pord; j++) {
            if (mdp->rows[i]->no_of_entries[j] > k)
                k = mdp->rows[i]->no_of_entries[j];
        } /* j = 0 ... pord-1 */
        (q->rowind[i]).val = (double *) ecalloc((unsigned) (k + 1), sizeof(double));
        (q->rowind[i]).colind = (size_t *) ecalloc((unsigned) (k + 1), sizeof(size_t));
        (q->rowind[i]).no_of_entries = k + 1;
    } /* for i = 0 ...ord-1 */
    return (q);
} /* bmdp_gen_precond_structure */

/**********************
bmdp_set_reward
Datum:  02.03.15
Autor:  Peter Buchholz
Input:  rew Vektors with policy abh�ngigen Rewards
        ind Indizes der Aktionen in dne Zust�nden
        
Output: hh Vektor mit zustandsahb�ngigen Rewards
Side-Effects:
Description:
Die Funktion bestimmt die Rewards in dne Zust�nden in Abh�ngigkeit von der gew�hlten 
Aktion. 
***********************************************************************/
int bmdp_set_reward(Double_vektor const ** restrict rew, double *hh, size_t *ind) {
    size_t i;

    for (i = 0; i < rew[0]->no_of_entries; i++)
        hh[i] = rew[ind[i]]->vektor[i];
    return (NO_ERROR);
} /* bmdp_set_reward */

/**********************
bmdp_new_strategy
Datum:  02.03.15
Autor:  Peter Buchholz
Input:  mdp Mdp Matrix 
        h Vektor der Reward-Werte
        rew Reward Vektoren
        gamma Diskontierungsfaktor
        
Output: Funktionswert neue Ordnung TRUE, sonst FALSE
        ind Index der Elemente f�r die neue optimale Strategie
Side-Effects:
Description:
Die Funktion bestimmt eine Strategie in Abh�ngigkeit vom Wertevektor h.
***********************************************************************/
short bmdp_new_strategy(Mdp_matrix *mdp, Double_vektor **rew, double *hh, size_t *ind, double gamma, short comp_max) {
    short news = FALSE;
    size_t i, j, k, l;
    double val, mval;

    for (i = 0; i < mdp->ord; i++) {
        if (comp_max)
            mval = -1.0e+12;
        else
            mval = 1.0e+12;
        l = ind[i];
        for (k = 0; k < mdp->rows[i]->pord; k++) {
            if (mdp->rows[i]->diagonals[k] > 0.0 || mdp->rows[i]->no_of_entries[k] > 0) {
                val = rew[k]->vektor[i] + gamma * hh[i] * mdp->rows[i]->diagonals[k];
                for (j = 0; j < mdp->rows[i]->no_of_entries[k]; j++)
                    val += gamma * hh[mdp->rows[i]->colind[k][j]] * mdp->rows[i]->values[k][j];
                if (comp_max) {
                    if (val > mval || (k == ind[i] && val + BMDP_MIN_VAL > mval)) {
                        mval = val;
                        l = k;
                    }
                }
                else {
                    if (val < mval || (k == ind[i] && val - BMDP_MIN_VAL < mval)) {
                        mval = val;
                        l = k;
                    }
                }
            } /* non_zero row */
        } /* for all actions k */
        if (l != ind[i]) {
            news = TRUE;
            ind[i] = l;
        }
    } /* for i = 0, ... ord-1 */
    return (news);
} /* bmdp_new_strategy */

/**********************
bmdp_set_strategy
Datum:  02.03.15
Autor:  Peter Buchholz
Input:  mdp MDP matrix
        ind decision vector
        gamma Diskontierungsfaktor
Output: pp Matrix resulting from decision vector
Side-Effects:
Description:
The function generates the matrix for a given decision vector for preconditioned
iteration methods. Matrix pp has to adequately allocated e.g. by the function 
bmdp_gen_precond_structure. 
**********************/
void bmdp_set_strategy(const Mdp_matrix * restrict mdp, Sparse_matrix *pp, size_t const * restrict ind, double gamma) {
    size_t i, j;

    for (i = 0; i < mdp->ord; i++) {
        pp->diagonals[i] = mdp->rows[i]->diagonals[ind[i]];
        (pp->rowind[i]).no_of_entries = mdp->rows[i]->no_of_entries[ind[i]];
        for (j = 0; j < mdp->rows[i]->no_of_entries[ind[i]]; j++) {
            (pp->rowind[i]).val[j] = mdp->rows[i]->values[ind[i]][j];
            (pp->rowind[i]).colind[j] = mdp->rows[i]->colind[ind[i]][j];
        }
    } /* for i = 0,... ord- 1 */
    /* We need I-gamma*P */
    for (i = 0; i < mdp->ord; i++) {
        pp->diagonals[i] = 1.0 - gamma * pp->diagonals[i];
        for (j = 0; j < (pp->rowind[i]).no_of_entries; j++) {
            (pp->rowind[i]).val[j] = -gamma * (pp->rowind[i]).val[j];
        }
    }
} /* bmdp_set_strategy */


/**********************
bmdp_initial_policy
Datum:  02.03.15
Autor:  Peter Buchholz
Input:  mdp Matrix
Output: ind initiale Politik
Side-Effects:
Description:
Die Funktion berechnet eine initiale politik.
***********************************************************************/
void bmdp_initial_policy(size_t *ind, Mdp_matrix *mdp) {
    size_t i, j;
    short found = FALSE;

    for (i = 0; i < mdp->ord; i++) {
        found = FALSE;
        for (j = 0; j < mdp->rows[i]->pord; j++) {
            if (mdp->rows[i]->diagonals[j] > 0.0 || mdp->rows[i]->no_of_entries[j] > 0) {
                found = TRUE;
                ind[i] = j;
                break;
            }
        }
        if (found == FALSE) {
            printf("Found no valid action in state %zu\n", i);
            exit(-1);
        }
    }
} /* bmdp_initial_policy */

/**********************
bmdp_direct_eval_policy
Datum:  02.03.15
Autor:  Peter Buchholz
Input:  pp Policy Matrix
        rew Reward Vektoren

Output: hv zustandsspezifische Gewinn
Side-Effects:
Description:
Die Funktion wertet eine Politik mit einem direkten Verfahren aus.
***********************************************************************/
double bmdp_direct_eval_policy(Sparse_matrix *pp, Double_vektor *rew, double *hv) {
    size_t i, j;
    int k;
    double sum = 0.0;
    gsl_vector *b = gsl_vector_calloc(pp->ord);
    gsl_vector *h = gsl_vector_calloc(pp->ord);
    gsl_matrix *m = gsl_matrix_calloc(pp->ord, pp->ord);
#if USE_SVD
  double res ;
  gsl_matrix *v = gsl_matrix_calloc (pp->ord, pp->ord);
  gsl_vector *s = gsl_vector_calloc (pp->ord);
  gsl_vector *work = gsl_vector_calloc (pp->ord);
#else
    gsl_permutation *p = gsl_permutation_alloc(pp->ord);
#endif


#if BMDP_TRACE_LEVEL
  printf("Matrix A:\n") ;
  mat_sparse_matrix_prt_small_matrix (pp);
  printf("Reward vector\n") ;
  for (i = 0; i < pp->ord; i++)
    printf("%.3e ", rew->vektor[i]) ;
  printf("\n") ;
#endif
    /* Vektorelemente initialisieren */
    /* #pragma omp parallel shared(pp, b, rew) private(i) */
    {
        /* #pragma omp for */
        //for (i = 0; i < pp->ord; i++) {
        //}
    }
    /* Matrixelemente initialisieren */
    //#pragma omp parallel firstprivate(rew, b, pp, m) private(i, j)
    {
        //#pragma omp for nowait
        for (i = 0; i < pp->ord; i++) {
            gsl_vector_set(b, i, rew->vektor[i]);

            gsl_matrix_set(m, i, i, pp->diagonals[i]);
            for (j = 0; j < (pp->rowind[i]).no_of_entries; j++) {
                gsl_matrix_set(m, i, (pp->rowind[i]).colind[j], (pp->rowind[i]).val[j]);
            }
        }
    }
    /* Matrix faktorisieren und L�sung berechnen */
#if USE_SVD
  gsl_linalg_SV_decomp(m, v, s, work) ;
  j = 0 ;
  for (i = pp->ord-1; i >= 0; i--) {
    res = gsl_vector_get(s, i) ;
    if (res < 1.0e-12)
      j++ ;
  }
  printf("%i of %i singular values < 1.0e-12 smallest %.5e\n",
	 j, pp->ord,  gsl_vector_get(s, pp->ord)) ;
  gsl_linalg_SV_solve(m, v, s, b, h) ;
#else
    gsl_linalg_LU_decomp(m, p, &k);
    gsl_linalg_LU_solve(m, p, b, h);
#endif
    for (i = 0; i < pp->ord; i++) {
        hv[i] = gsl_vector_get(h, i);
        sum += hv[i];
    }
#if BMDP_TRACE_LEVEL
  printf("Result vector\n") ;
  for (i = 0; i < pp->ord; i++)
    printf("%.3e ", hv[i]) ;
  printf("\n") ;
#endif
    gsl_vector_free(h);
    gsl_vector_free(b);
    gsl_matrix_free(m);
#if USE_SVD
  gsl_matrix_free (v);
  gsl_vector_free (s) ;
  gsl_vector_free (work) ;
#else
    gsl_permutation_free(p);
#endif
    return (sum);
} /* bmdp_direct_eval_policy */


/**********************
bmdp_policy_iteration
Datum:  25.03.10
Autor:  Peter Buchholz
Input:  pl, pu Bounding matrices
        ds Vektor der Zeilensummen
        rew Rewardvektoren
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maixmale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet den durchschnittlichen abgezinsten Gewinn mit einem dirketen Verfahren.
***********************************************************************/
double *bmdp_policy_iteration(const Mdp_matrix *mdp, const Double_vektor **rew, size_t *ind, int max_iter,
        double ti_max, double epsilon, double gamma, short comp_max, short comp_stat) {
    short stop = FALSE, found = FALSE;
    size_t i;
    int iter;
    double rho, old_rho, val, starttime, *hh;
    Double_vektor *rh, r2;
    Sparse_matrix *pp;

    rho = old_rho = 0.0;
    iter = 0;
    pp = mdp_gen_sparse_matrix_structure(mdp);
    hh = (double *) ecalloc((unsigned) mdp->ord, sizeof(double));
    rh = mat_double_vektor_new((unsigned) mdp->ord);
    starttime = mat_timeused();
    bmdp_initial_policy(ind, mdp);
    while (!(stop)) {
        /* compute and set a new policy */
        if (iter > 0)
            found = bmdp_new_strategy(mdp, rew, hh, ind, gamma, comp_max);
        val = (fabs(rho) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(rho);
        if (iter > 1 && ((fabs(old_rho - rho) / val < epsilon && !(found)) || (old_rho > rho))) {
            if (old_rho > rho)
                printf("Values in policy iteration are decreasing old %.3e new %.3e\n", old_rho, rho);
            else
                stop = TRUE;
        }
        if (!(stop)) {
            iter++;
            old_rho = rho;
            bmdp_set_strategy(mdp, pp, ind, gamma);
            bmdp_set_reward(rew, rh->vektor, ind);
            /* Evaluatethe policy */
            rho = bmdp_direct_eval_policy(pp, rh, hh);
            if (iter >= max_iter || mat_timeused() - starttime > ti_max) {
                stop = TRUE;
            }
#if MDP_TRC_ITER
            if (!stop && iter % 1 == 0) {
                if (!(found)) {
                    printf("Iteration %i rho %.3e difference %.3e (%.3e seconds) (policy remains)\n",
                            iter, rho, fabs(rho - old_rho) / val, mat_timeused() - starttime);
                }
                else {
                    printf("Iteration %i rho %.3e difference %.3e (%.3e seconds) (new policy)\n",
                            iter, rho, fabs(rho - old_rho) / val, mat_timeused() - starttime);
                }
            }
#endif
        }
    } /* while */
    if (!(found))
        printf("Policy iteration stopped after %i iterations with the optimal policy (%.3e seconds) rho %.5e\n", iter, mat_timeused() - starttime, rho);
    else
        printf("Policy iteration stopped after %i iterations due to iteration/time limit (%.3e seconds) rho %.5e\n", iter, mat_timeused() - starttime, rho);
    if (comp_stat) {
        printf("Compute stationary solution\n");
        starttime = mat_timeused();
        mat_sparse_matrix_mult_scalar(pp, -1.0 / gamma);
        val = (gamma - 1.0) / gamma;
        for (i = 0; i < pp->ord; i++)
            pp->diagonals[i] -= val;
#if BMDP_TRACE_LEVEL
    printf("Matrix for stationary analysis\n") ;
    mat_sparse_matrix_prt_small_matrix (pp) ;
#endif
        int ret;
        if ((ret = sol_direct_solution(pp, rh, TRUE, NULL)) == NO_ERROR)
            printf("Stationary solution successful\n");
        else {
            printf("Stationary solution failed: %d\n", ret);
        }
        r2.vektor = hh;
        r2.no_of_entries = pp->ord;
        printf("Stationary reward %.3e\n", mat_double_vektor_inner_product(rh, &r2));
        printf("Time used to compute stationary vector %.3e\n", mat_timeused() - starttime);
    }
    mat_sparse_matrix_free(pp);
    mat_double_vektor_free(rh);
    return (hh);
} /* bmdp_policy_iteration */

/**********************
bmdp_precond_eval_policy
Datum:  02.03.15
Autor:  Peter Buchholz
Input:  pp Policy Matrix
        rew Reward Vektoren

Output: hv zustandsspezifische Gewinn
Side-Effects:
Description:
Die Funktion wertet eine Politik mit einem iterativem Verfahren mit Voirkonditionierung aus.
***********************************************************************/
double bmdp_precond_eval_policy(int method, Sparse_matrix *pp, Sparse_matrix **lu2, Double_vektor *b, double *hh, short found, int local_it, double local_eps, int *global_it) {
    size_t i;
    double threshold, sum = 0.0;
    Sparse_matrix *lu;
    Double_vektor hv;

    lu = *lu2;
    if (method == BO_BICGSTAB || method == BO_BICGSTAB_ILU0 || method == BO_BICGSTAB_ILUTH ||
            method == BO_GMRES || method == BO_GMRES_ILU0 || method == BO_GMRES_ILUTH) {
        hv.no_of_entries = pp->ord;
        hv.vektor = hh;
    }
    if (found == TRUE) {
        if (method == BO_GS_ILU0 || method == BO_BICGSTAB_ILU0 || method == BO_GMRES_ILU0) {
            if (lu != NULL)
                mat_sparse_matrix_free(lu);
            lu = sol_comp_preconditioner(pp, 1, 0.0);
            mat_sparse_matrix_eliminate_small_values(lu, 1.0e-12);
            mat_set_lud(lu);
        }
        if (method == BO_GS_ILUTH || method == BO_BICGSTAB_ILUTH || method == BO_GMRES_ILUTH) {
            if (lu != NULL)
                mat_sparse_matrix_free(lu);
            threshold = mat_sparse_matrix_max_elem(pp) * MDP_ILUTH_TH;
            lu = sol_comp_preconditioner(pp, 2, threshold);
            mat_sparse_matrix_eliminate_small_values(lu, 1.0e-12);
            mat_set_lud(lu);
        }
    } /* found == TRUE */
    if (method == BO_GS_ILU0 || method == BO_GS_ILUTH) {
        mdp_pre_power_eval_policy(pp, lu, b, hh, local_it, local_eps, global_it);
    }
    else {
        if (method == BO_BICGSTAB || method == BO_BICGSTAB_ILU0 || method == BO_BICGSTAB_ILUTH) {
            sol_general_bicgstab(pp, lu, &hv, b, local_it, local_eps, global_it);
        }
        else {
            if (method == BO_GMRES || method == BO_GMRES_ILU0 || method == BO_GMRES_ILUTH) {
                sol_general_gmres(pp, lu, &hv, b, local_it, local_eps, global_it);
            }
            else {
                printf("Unknown method\nSTOP EXECUTION\n");
                exit(-1);
            }
        }
    }
    for (i = 0; i < pp->ord; i++)
        sum += hh[i];
    return (sum);
} /*  bmdp_precond_eval_policy */

/**********************
bmdp_precond_iteration
Datum:  02.02.15
Autor:  Peter Buchholz
Input:  mdp MDp matrix
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maximale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke für den Reward mit Hilfe einer iterativen Technik
und ILU Pr�konditionierung.
***********************************************************************/
double *bmdp_precond_iteration(const Mdp_matrix *mdp, const Double_vektor **rew, size_t *ind, int max_iter, double ti_max,
        double epsilon, int local_it, double local_eps, int method, double gamma, short comp_max, short comp_stat) {
    short stop = FALSE, found = FALSE;
    int iter, global_it;
    size_t i;
    size_t *old_ind;
    double rho, old_rho, val, starttime, *hh;
    Double_vektor *b, r2;
    Sparse_matrix *pp, *lu = NULL;
    short dynamic = FALSE;
    Solution_param param;

    iter = global_it = 0;
    if (local_eps < 0.0) {
        dynamic = TRUE;
        local_eps = 10.0 * epsilon;
    }
    pp = bmdp_gen_precond_structure(mdp);
    b = mat_double_vektor_new(mdp->ord);
    old_ind = (size_t *) ecalloc(mdp->ord, sizeof(int));
    hh = (double *) ecalloc(mdp->ord, sizeof(double));
    val = 0.0;
    bmdp_initial_policy(ind, mdp);
    bmdp_set_reward(rew, hh, ind);
    for (i = 0; i < mdp->ord; i++) {
        val += hh[i];
    }
    rho = old_rho = val;

    starttime = mat_timeused();
    while (!(stop)) {
        /* compute and set a new policy */
        if (iter > 0)
            found = bmdp_new_strategy(mdp, rew, hh, ind, gamma, comp_max);
        iter++;
        val = (fabs(rho) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(rho);
        if (iter > 1) {
            val = fabs(old_rho - rho) / val;
            if (val < epsilon) {
                found = FALSE;
                stop = TRUE;
            }
            else {
                if (dynamic)
                    local_eps = 0.1 * val;
            }
            for (i = 0; i < mdp->ord; i++) {
                if (ind[i] != old_ind[i]) {
                    stop = FALSE;
                    found = TRUE;
                }
                old_ind[i] = ind[i];
            }
        }
        else
            found = TRUE;
        if (!(stop)) {
            old_rho = rho;
            bmdp_set_reward(rew, b->vektor, ind);
            if (found == TRUE) {
                bmdp_set_strategy(mdp, pp, ind, gamma);
#if BMDP_TRACE_LEVEL
	printf("Policy\n") ;
	for (i = 0; i < pp->ord; i++)
	  printf("%i ", ind[i]) ;
	printf("\n") ;
	printf("Matrix A:\n") ;
	mat_sparse_matrix_prt_small_matrix (pp);
	printf("Reward vector\n") ;
	for (i = 0; i < pp->ord; i++)
	  printf("%.3e ", b->vektor[i]) ;
	printf("\n") ;
#endif
            }
            rho = bmdp_precond_eval_policy(method, pp, &lu, b, hh, found, local_it, local_eps, &global_it);
        } /* not stop */
        if (iter >= max_iter || mat_timeused() - starttime > ti_max) {
            stop = TRUE;
        }
#if MDP_TRC_ITER
        if (!stop && iter % 1 == 0) {
            printf("Iteration %i rho %.3e difference %.3e (%.3e seconds)\n",
                    iter, rho, fabs(rho - old_rho) / val, mat_timeused() - starttime);
        }
#endif
    } /* while */
    if (!found)
        printf("Mixed iteration stopped after %i (global %i) iterations with the optimal policy (%.3e seconds) rho %.5e\n",
                iter, global_it, mat_timeused() - starttime, rho);
    else
        printf("Mixed iteration stopped after %i (global %i) iterations due to iteration/time limit (%.3e seconds) rho %.5e\n",
                iter, global_it, mat_timeused() - starttime, rho);
    efree(old_ind);
    if (comp_stat) {
        printf("Compute stationary solution\n");
        starttime = mat_timeused();
        mat_sparse_matrix_mult_scalar(pp, -1.0 / gamma);
        val = (gamma - 1.0) / gamma;
        for (i = 0; i < pp->ord; i++)
            pp->diagonals[i] -= val;
        param.n_proc = 1;
        /* param.method = STD_ILU0_GMRES  ; */
        /* param.method = STD_ILU0_BICGSTAB ; */
        param.method = STD_SOR;
        param.no_of_iterations = 1000;
        param.epsilon = 1.0e-8;
        param.epsilon_2 = 0.1;
        param.epsilon_3 = 0.0;
        param.time_limit = 500.0;
        param.relaxation = 0.98;
        param.initials = NULL;
        param.overall_result = 0;
        param.subnet_results = NULL;
        val = 1.0 / (double) pp->ord;
        for (i = 0; i < pp->ord; i++)
            b->vektor[i] = val;
#if BMDP_TRACE_LEVEL
    printf("Matrix for stationary analysis\n") ;
    mat_sparse_matrix_prt_small_matrix (pp) ;
#endif
        /* sol_gmres(pp, b, &param, 1) ; */
        /* sol_bicgstab(pp, b, &param, 0) ; */
        sol_sor_iteration(&pp, b, &param, TRUE, FALSE, NULL);
        r2.vektor = hh;
        r2.no_of_entries = pp->ord;
        printf("Stationary reward %.3e\n", mat_double_vektor_inner_product(b, &r2));
        printf("Time used to compute stationary vector %.3e\n", mat_timeused() - starttime);
    }

    mat_sparse_matrix_free(pp);
    mat_double_vektor_free(b);
    if (lu != NULL)
        mat_sparse_matrix_free(lu);
    return (hh);
} /* bmdp_precond_iteration */

/**********************
bmdp_evaluate_avg_policy
Datum:  13.03.15
Autor:  Peter Buchholz
Input:  mdp MDP matrices
        ind Policy
        reward vectors for different actions
        gamma discount factor
        comp_stat TRUE if stationary result should be computed
        avg TRUE if the average matrix is used oterhwhise bounding matrices are used
Output: return value gain vector
Side-Effects:
Description:
The function evaluates the policy ind and returns the average gain vector. If 
comp_stat is set a stationary analysis is performed and the stationary gain is printed.
***********************************************************************/
double *bmdp_evaluate_avg_policy(Mdp_matrix const * restrict mdp, size_t const * restrict ind, Double_vektor const ** restrict rew, double gamma, short comp_stat) {
    size_t i;
    int global_it = 0;
    double *hh, sum = 0, val;
    Sparse_matrix *pp, *lu = NULL;
    Double_vektor *b, r2;
    Solution_param param;
    int ret = 0;

    pp = bmdp_gen_precond_structure(mdp);
    b = mat_double_vektor_new(mdp->ord);
    hh = (double *) ecalloc(mdp->ord, sizeof(double));
    bmdp_set_reward(rew, b->vektor, ind);
    for (i = 0; i < mdp->ord; i++) {
        sum += b->vektor[i];
        hh[i] = b->vektor[i];
    }
    bmdp_set_strategy(mdp, pp, ind, gamma);
    if (mdp->ord < BMDP_SIZE_DIRECT)
        sum = bmdp_direct_eval_policy(pp, b, hh);
    else
        sum = bmdp_precond_eval_policy(BO_GMRES_ILU0, pp, &lu, b, hh, TRUE, 300, 1.0e-8, &global_it);
    if (comp_stat) {
        mat_sparse_matrix_mult_scalar(pp, -1.0 / gamma);
        val = (gamma - 1.0) / gamma;
        for (i = 0; i < pp->ord; i++)
            pp->diagonals[i] -= val;
        if (mdp->ord < BMDP_SIZE_DIRECT) {
            if ((ret = sol_direct_solution(pp, b, TRUE, NULL)) == NO_ERROR)
                printf("Stationary solution successful\n");
            else {
                printf("Stationary solution failed: %d\n", ret);
            }
        }
        else {
            param.n_proc = 1;
            /* param.method = STD_ILU0_GMRES  ; */
            /* param.method = STD_ILU0_BICGSTAB ; */
            param.method = STD_SOR;
            param.no_of_iterations = 1000;
            param.epsilon = 1.0e-8;
            param.epsilon_2 = 0.1;
            param.epsilon_3 = 0.0;
            param.time_limit = 500.0;
            param.relaxation = 0.98;
            param.initials = NULL;
            param.overall_result = 0;
            param.subnet_results = NULL;
            val = 1.0 / (double) pp->ord;
            for (i = 0; i < pp->ord; i++)
                b->vektor[i] = val;
            /* sol_gmres(pp, b, &param, 1) ; */
            /* sol_bicgstab(pp, b, &param, 0) ; */
            sol_sor_iteration(&pp, b, &param, TRUE, FALSE, NULL);
        }
        r2.vektor = hh;
        r2.no_of_entries = pp->ord;
        val = mat_double_vektor_inner_product(b, &r2);
        printf("sum of gain values %.5e stationary gain value %.5e\n", sum, val);
    }
    mat_double_vektor_free(b);
    mat_sparse_matrix_free(pp);
    if (lu != NULL)
        mat_sparse_matrix_free(lu);

    return (hh);
} /* bmdp_evaluate_avg_policy */

double *bmdp_evaluate_avg_stationary_policy(Mdp_matrix *mdp, double **policy, Double_vektor **rew, double gamma, short comp_stat) {
    /* let's create some stuff… */
    Sparse_matrix *pp = mat_sparse_matrix_new((unsigned) mdp->ord);
    Sparse_matrix *lu = NULL;
    Double_vektor *b = mat_double_vektor_new((unsigned) mdp->ord);
    Double_vektor *unity = mat_double_vektor_new((unsigned) mdp->ord);
    Double_vektor *temp = mat_double_vektor_new((unsigned) mdp->ord);
    size_t s, a, s_prime, i;
    int global_it;

    for(s = 0; s < mdp->ord; ++s) {
        for(a = 0; a < mdp->rows[s]->pord; ++a) {
            b->vektor[s] += policy[s][a] * rew[a]->vektor[s];

            unity->vektor[s] = -gamma * policy[s][a];
            /* cleanup temp vector */
            for(s_prime = 0; s_prime < mdp->ord; ++s_prime) {
                temp->vektor[s_prime] = 0.0;
            }
            temp->vektor[s] = mdp->rows[s]->diagonals[a];
            for(i = 0; i < mdp->rows[s]->no_of_entries[a]; ++i) {
                temp->vektor[mdp->rows[s]->colind[a][i]] = mdp->rows[s]->values[a][i];
            }

            Sparse_matrix *result = mat_transform_vectors_to_matrix(unity, temp);
            mat_sparse_matrix_add_matrices(pp, result);
            mat_sparse_matrix_free(result);
        }
        unity->vektor[s] = 0.0;
    }

    double *hh = (double *) ecalloc((unsigned) mdp->ord, sizeof(double));

    for(s = 0; s < mdp->ord; ++s) {
        pp->diagonals[s] += 1.0;
        for(i = 0; i < pp->rowind[s].no_of_entries; ++i) {
            if(pp->rowind[s].colind[i] == s) {
                /* double diag = pp->rowind[s].val[i]; */
                pp->rowind[s].val[i] += 1.0;
                /* pp->diagonals[s] += diag; */
            }
        }
    }

    if(mdp->ord < BMDP_SIZE_DIRECT) {
        bmdp_direct_eval_policy(pp, b, hh);
    } else {
        bmdp_precond_eval_policy(BO_GMRES_ILU0, pp, &lu, b, hh, TRUE, 300, 1.0e-8, &global_it);
    }

    mat_sparse_matrix_free(pp);
    mat_sparse_matrix_free(lu);
    mat_double_vektor_free(b);
    mat_double_vektor_free(temp);
    mat_double_vektor_free(unity);

    return hh;
}

double *bmdp_low_level_evaluate_avg_stationary_policy(size_t ord, size_t *num_actions, Row ***rows, double **diags, Double_vektor **rew, double **policy, double gamma) {
    /* let's create some stuff… */
    Sparse_matrix *pp = mat_sparse_matrix_new((unsigned) ord);
    Sparse_matrix *lu = NULL;
    Double_vektor *b = mat_double_vektor_new((unsigned) ord);
    Double_vektor *unity = mat_double_vektor_new((unsigned) ord);
    Double_vektor *temp = mat_double_vektor_new((unsigned) ord);
    size_t s, a, s_prime, i;
    int global_it;

    for(s = 0; s < ord; ++s) {
        for(a = 0; a < num_actions[s]; ++a) {
            b->vektor[s] += policy[s][a] * rew[a]->vektor[s];

            unity->vektor[s] = -gamma * policy[s][a];
            /* cleanup temp vector */
            for(s_prime = 0; s_prime < ord; ++s_prime) {
                temp->vektor[s_prime] = 0.0;
            }
            temp->vektor[s] = diags[s][a];
            for(i = 0; i < rows[s][a]->no_of_entries; ++i) {
                temp->vektor[rows[s][a]->colind[i]] = rows[s][a]->val[i];
            }

            Sparse_matrix *result = mat_transform_vectors_to_matrix(unity, temp);
            mat_sparse_matrix_add_matrices(pp, result);
            mat_sparse_matrix_free(result);
        }
        unity->vektor[s] = 0.0;
    }

    double *hh = (double *) ecalloc(ord, sizeof(double));

    for(s = 0; s < ord; ++s) {
        pp->diagonals[s] += 1.0;
        for(i = 0; i < pp->rowind[s].no_of_entries; ++i) {
            if(pp->rowind[s].colind[i] == s) {
                pp->rowind[s].val[i] += 1.0;
            }
        }
    }

    if(ord < BMDP_SIZE_DIRECT) {
        bmdp_direct_eval_policy(pp, b, hh);
    } else {
        bmdp_precond_eval_policy(BO_GMRES_ILU0, pp, &lu, b, hh, TRUE, 300, 1.0e-8, &global_it);
    }

    mat_sparse_matrix_free(pp);
    mat_sparse_matrix_free(lu);
    mat_double_vektor_free(b);
    mat_double_vektor_free(temp);
    mat_double_vektor_free(unity);

    return hh;
}

/**********************
bmdp_set_extreme_values
Datum:  03.03.15
Autor:  Peter Buchholz
Input:  mdp_min, mdp_max Bounding MDP matrices
        hh gain vector
        ind politics
        hh auxiliary vectors
        compmin if TRUE compute the worst case, else the best case
Output: pp matrix of the politics
Side-Effects:
Description:
The function computes the worst/best case matrix for a politics.
***********************************************************************/
void bmdp_set_extreme_values(Mdp_matrix const * restrict mdp_min, Mdp_matrix const * restrict mdp_max, Sparse_matrix *pp, size_t const * restrict ind,
        double const * restrict hh, double *vv, double gamma, short compmin) {
    size_t i, j;

    double *vv_global = (double *) ecalloc(mdp_min->ord * mdp_min->ord, sizeof(double));

    #pragma omp parallel firstprivate(pp, mdp_min, mdp_max, ind, hh, compmin, vv_global) private(i, j)
    {
        size_t ord = pp->ord;
        int num_threads = omp_get_max_threads() / 2;
        #pragma omp for nowait schedule(static, ord / num_threads)
        for (i = 0; i < ord; i++) {
            double *vv_local = vv_global + (i * ord);
            bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], &(pp->rowind[i]), &pp->diagonals[i], i, ind[i], hh, vv_local, compmin);

            pp->diagonals[i] = 1.0 - gamma * pp->diagonals[i];
            for (j = 0; j < (pp->rowind[i]).no_of_entries; j++)
                (pp->rowind[i]).val[j] *= -gamma;
        }
    }

    efree(vv_global);
    /* Compute PP = I - gamma*PP */
    /* #pragma omp parallel shared(pp, gamma) private(i, j) */
    {
        /* #pragma omp parallel for private(j) */ /* parallel for: 0.98% */
        /*for (i = 0; i < pp->ord; i++) {
        }*/
    }
} /* bmdp_set_extreme_values */


/**********************
bmdp_set_minmax_row
Datum:  03.03.15
Autor:  Peter Buchholz
Input:  min_row max_row Bounding mdp rows
        my_row index of teh current row
        pol current politics
        hh gain vector
        vv help vector
        compmin true for worst fals for best case
Output: prow non diagonal entries of teh row
        diag diagonal entry of teh new row
Side-Effects:
Description:
The function computes a new ploictis and the corresponding matrix and reward vector
for a BMDP. one row for polictis pol.
***********************************************************************/
void bmdp_set_one_minmax_row(Mdp_row const * restrict min_row, Mdp_row const * restrict max_row, Row *prow, double *diag, size_t my_row,
        size_t pol, double const * restrict hh, double *vv, short compmin) {
    size_t i, j, k, len;
    double sum, val;
    gsl_vector_view v;
    gsl_permutation *perm;

    sum = min_row->diagonals[pol];
    *diag = min_row->diagonals[pol];
    prow->no_of_entries = min_row->no_of_entries[pol];
    for (i = 0; i < min_row->no_of_entries[pol]; i++) {
        prow->colind[i] = min_row->colind[pol][i];
        prow->val[i] = min_row->values[pol][i];
        sum += prow->val[i];
    }
    if (1.0 - sum > BMDP_ROW_VAL) {
        len = min_row->no_of_entries[pol] + 1;
        for (j = 0; j < min_row->no_of_entries[pol]; j++) {
            vv[j] = hh[min_row->colind[pol][j]];
        }
        vv[len - 1] = hh[my_row];
        v = gsl_vector_view_array(vv, len);
        perm = gsl_permutation_alloc(len);
        gsl_sort_vector_index(perm, &v.vector);
        for (j = 0; j < len; j++) {
            if (compmin == TRUE)
                k = gsl_permutation_get(perm, j);
            else
                k = gsl_permutation_get(perm, len - 1 - j);
            if (k < len - 1)
                val = max_row->values[pol][k] - min_row->values[pol][k];
            else
                val = max_row->diagonals[pol] - min_row->diagonals[pol];
            if (val + sum > 1.0)
                val = 1.0 - sum;
            if (k < len - 1)
                prow->val[k] += val;
            else
                *diag += val;
            sum += val;
            if (1.0 - sum < BMDP_ROW_VAL)
                break;
        }
        gsl_permutation_free(perm);
    }
} /* bmdp_set_one_minmax_row */


/**********************
bmdp_find_and_set_new_strategy
Datum:  03.03.15
Autor:  Peter Buchholz
Input:  mdp_min, mdp_max Bounding MDP matrices
        hh gain vector
        ind old politics
        hh auxiliary vectors
        compmin if TRUE compute the worst case, else the best case
Output: pp matrix of the politics
        ind new polictis
        b reward vector of new polictis
        return value true i fnew politics has bene found
Side-Effects:
Description:
The function computes a new ploictis and the correpsonding matrix and reward vector
for a BMDP. 
***********************************************************************/
short bmdp_find_and_set_new_strategy(Mdp_matrix *mdp_min, Mdp_matrix *mdp_max, Sparse_matrix *pp, size_t *ind, Double_vektor **rew,
        Double_vektor *b, double *hh, double *vv, double gamma, short compmin, short comp_max) {
    short news = FALSE;
    size_t i, j, k, l, count = 0;
    double val, mval, diag;
    Row *hrow;

    hrow = mat_row_new(pp->ord);
    for (i = 0; i < pp->ord; i++) {
        if (comp_max)
            mval = -1.0e+12;
        else
            mval = 1.0e+12;
        l = ind[i];
        for (k = 0; k < mdp_max->rows[i]->pord; k++) {
            if (mdp_max->rows[i]->diagonals[k] > 0.0 || mdp_max->rows[i]->no_of_entries[k] > 0) {
                hrow->no_of_entries = mdp_max->rows[i]->no_of_entries[k];
                bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], hrow, &diag, i, k, hh, vv, compmin);
                val = rew[k]->vektor[i] + gamma * hh[i] * diag;
                for (j = 0; j < mdp_max->rows[i]->no_of_entries[k]; j++)
                    val += gamma * hh[hrow->colind[j]] * hrow->val[j];
                if (comp_max) {
                    if (val > mval || (k == ind[i] && val + BMDP_MIN_VAL > mval)) {
                        mval = val;
                        l = k;
                    }
                }
                else {
                    if (val < mval || (k == ind[i] && val - BMDP_MIN_VAL < mval)) {
                        mval = val;
                        l = k;
                    }
                }
            } /* non_zero row */
        }
        if (ind[i] != l) {
            count++;
            ind[i] = l;
            news = TRUE;
            b->vektor[i] = rew[l]->vektor[i];
        }
        /* We have to reset the row because even if the policy remains the row may have been changed due to
           a different gain vector */
        bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], &(pp->rowind[i]), &pp->diagonals[i], i, ind[i], hh, vv, compmin);
        pp->diagonals[i] = 1.0 - gamma * pp->diagonals[i];
        for (j = 0; j < (pp->rowind[i]).no_of_entries; j++)
            (pp->rowind[i]).val[j] *= -gamma;
    } /* for i = 0, ... ord-1 */
    mat_row_free(hrow);
    return (news);
} /* bmdp_find_and_set_new_strategy */

/**********************
bmdp_compute_extreme_case
Datum:  03.03.15
Autor:  Peter Buchholz
Input:  mdp_min, mdp_max Bounding MDP matrices
        rew reward vector
        ind politics
        max_iter, local maximal number of global/local iterations
        ti_max, local_it maximal local/global solution time
        epsilon, local_eps epsilon global/local
        method solution method if size > BMDP_SIZE_DIRECT
        gamma discount factor
        compmin if TRUE compute the worst case, else the best case
Output: return value gain vector
        ind optimal policy
Side-Effects:
Description:
Th efunction computes the worst/best cae gain vector and policy for a BMDP.
***********************************************************************/
double *bmdp_compute_extreme_case(Mdp_matrix *mdp_min, Mdp_matrix *mdp_max, const Double_vektor **rew, size_t *ind, int max_iter,
        double ti_max, double epsilon, int local_it, double local_eps, int method, double gamma,
        short compmin, short comp_max, short comp_stat) {
    short stop = FALSE, found = FALSE;
    int iter, global_it;
    size_t i;
    size_t *old_ind;
    double rho, old_rho, val, starttime, *hh, *vv;
    Double_vektor *b, r2;
    Sparse_matrix *pp, *lu = NULL;
    short dynamic = FALSE;
    Solution_param param;

    iter = global_it = 0;
    if (local_eps < 0.0) {
        dynamic = TRUE;
        local_eps = 10.0 * epsilon;
    }
    pp = bmdp_gen_precond_structure(mdp_max);
    b = mat_double_vektor_new((unsigned) mdp_max->ord);
    old_ind = (size_t *) ecalloc((unsigned) mdp_max->ord, sizeof(size_t));
    hh = (double *) ecalloc((unsigned) mdp_max->ord, sizeof(double));
    val = 0.0;
    bmdp_initial_policy(ind, mdp_max);
    bmdp_set_reward(rew, hh, ind);
    for (i = 0; i < mdp_max->ord; i++) {
        val += hh[i];
    }
    rho = old_rho = val;
    vv = (double *) ecalloc(pp->ord, sizeof(double));

    starttime = mat_timeused();
    while (!(stop)) {
        /* compute and set a new policy */
        if (iter > 0)
            found = bmdp_find_and_set_new_strategy(mdp_min, mdp_max, pp, ind, rew, b, hh, vv, gamma, compmin, comp_max);
        else {
            /* bmdp_set_strategy(mdp_min, pp, ind, gamma) ; */
            bmdp_set_extreme_values(mdp_min, mdp_max, pp, ind, hh, vv, gamma, compmin);
            bmdp_set_reward(rew, b->vektor, ind);
        }
        iter++;
        val = (fabs(rho) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(rho);
        if (iter > 1) {
            val = fabs(old_rho - rho) / val;
            if (val < epsilon) {
                found = FALSE;
                stop = TRUE;
            }
            else {
                if (dynamic)
                    local_eps = 0.1 * val;
            }
            for (i = 0; i < mdp_max->ord; i++) {
                if (ind[i] != old_ind[i]) {
                    stop = FALSE;
                }
                old_ind[i] = ind[i];
            }
        }
        else
            found = TRUE;
        if (!(stop)) {
            old_rho = rho;
            if (found == TRUE) {
#if BMDP_TRACE_LEVEL
	printf("Policy\n") ;
	for (i = 0; i < pp->ord; i++)
	  printf("%i ", ind[i]) ;
	printf("\n") ;
	printf("Matrix A:\n") ;
	mat_sparse_matrix_prt_small_matrix (pp);
	printf("Reward vector\n") ;
	for (i = 0; i < pp->ord; i++)
	  printf("%.3e ", b->vektor[i]) ;
	printf("\n") ;
#endif
            }
            if (pp->ord < BMDP_SIZE_DIRECT) {
                rho = bmdp_direct_eval_policy(pp, b, hh);
            }
            else
                rho = bmdp_precond_eval_policy(method, pp, &lu, b, hh, found, local_it, local_eps, &global_it);
            val = (fabs(rho) < MDP_MIN_VAL) ? MDP_MIN_VAL : fabs(rho);
#if BMDP_TRACE_LEVEL
      printf("gain vector: ") ;
      for (i = 0; i < pp->ord; i++)
	printf(".3e ", hh[i]) ;
      printf("\n") ;
#endif
        } /* not stop */
        if (iter >= max_iter || mat_timeused() - starttime > ti_max) {
            stop = TRUE;
        }
#if MDP_TRC_ITER
        if (!stop && iter % 1 == 0) {
            if (!(found)) {
                printf("Iteration %i rho %.3e difference %.3e (%.3e seconds) (policy remains)\n",
                        iter, rho, fabs(rho - old_rho) / val, mat_timeused() - starttime);
            }
            else {
                printf("Iteration %i rho %.3e difference %.3e (%.3e seconds) (new policy)\n",
                        iter, rho, fabs(rho - old_rho) / val, mat_timeused() - starttime);
            }
        }
#endif
    } /* while */
    if (!found) {
        if (BMDP_TRC_ITER) {
            printf("Minmax iteration stopped after %i (global %i) iterations with the optimal policy (%.3e seconds) rho %.5e\n",
                    iter, global_it, mat_timeused() - starttime, rho);
        }
    } else {
        printf("Minmax iteration stopped after %i (global %i) iterations due to iteration/time limit (%.3e seconds) rho %.5e\n",
                iter, global_it, mat_timeused() - starttime, rho);
    }
    efree(old_ind);
    efree(vv);
    int ret;

    if (comp_stat) {
        printf("Compute stationary solution\n");
        starttime = mat_timeused();
        mat_sparse_matrix_mult_scalar(pp, -1.0 / gamma);
        val = (gamma - 1.0) / gamma;
        for (i = 0; i < pp->ord; i++)
            pp->diagonals[i] -= val;
        if (pp->ord < BMDP_SIZE_DIRECT) {
            if ((ret = sol_direct_solution(pp, b, TRUE, NULL)) == NO_ERROR)
                printf("Stationary solution successful\n");
            else {
                printf("Stationary solution failed: %d\n", ret);
            }
        }
        else {
            param.n_proc = 1;
            /* param.method = STD_ILU0_GMRES  ; */
            /* param.method = STD_ILU0_BICGSTAB ; */
            param.method = STD_SOR;
            param.no_of_iterations = 1000;
            param.epsilon = 1.0e-8;
            param.epsilon_2 = 0.1;
            param.epsilon_3 = 0.0;
            param.time_limit = 500.0;
            param.relaxation = 0.98;
            param.initials = NULL;
            param.overall_result = 0;
            param.subnet_results = NULL;
            val = 1.0 / (double) pp->ord;
            for (i = 0; i < pp->ord; i++)
                b->vektor[i] = val;
            /* sol_gmres(pp, b, &param, 1) ; */
            /* sol_bicgstab(pp, b, &param, 0) ; */
            sol_sor_iteration(&pp, b, &param, TRUE, FALSE, NULL);
        }
        r2.vektor = hh;
        r2.no_of_entries = pp->ord;
        printf("Stationary reward %.3e\n", mat_double_vektor_inner_product(b, &r2));
        printf("Time used to compute stationary vector %.3e\n", mat_timeused() - starttime);
    }
    mat_sparse_matrix_free(pp);
    mat_double_vektor_free(b);
    if (lu != NULL)
        mat_sparse_matrix_free(lu);
    return (hh);
} /* bmdp_compute_extreme_case */

/**********************
bmdp_evaluate_minmax_policy
Datum:  13.03.15
Autor:  Peter Buchholz
Input:  mdp MDP matrices
        ind Policy
        reward vectors for different actions
        gamma discount factor
        comp_stat TRUE if stationary result should be computed
        avg TRUE if teh average matrix is used oterhwhise bounding matrices are used
Output: return value gain vector
Side-Effects:
Description:
The function evaluates the policy ind and returns the average gain vector. If 
comp_stat is set a stationary analysis is performed and the stationary gain is printed.
***********************************************************************/
double *bmdp_evaluate_minmax_policy(Mdp_matrix const * restrict mdp_min, Mdp_matrix const * restrict mdp_max, size_t const * restrict ind, Double_vektor const ** restrict rew,
        double gamma, short compmin, short comp_stat) {
    short stop = FALSE;
    size_t i;
    int global_it = 0;
    double *hh, *gg, *vv, sum, val, n_max, n_sum;
    Sparse_matrix *pp, *lu = NULL;
    Double_vektor *b, r1, r2, r3;
    Solution_param param;

    pp = bmdp_gen_precond_structure(mdp_min);
    b = mat_double_vektor_new(mdp_min->ord);
    hh = (double *) ecalloc(mdp_min->ord, sizeof(double));
    gg = (double *) ecalloc(mdp_min->ord, sizeof(double));
    vv = (double *) ecalloc(mdp_min->ord, sizeof(double));
    bmdp_set_reward(rew, b->vektor, ind);
    for (i = 0; i < mdp_min->ord; i++) {
        sum += b->vektor[i];
        hh[i] = b->vektor[i];
    }
    r1.vektor = hh;
    r2.vektor = gg;
    r1.no_of_entries = r2.no_of_entries = mdp_min->ord;
    while (!(stop)) {
        bmdp_set_extreme_values(mdp_min, mdp_max, pp, ind, r1.vektor, vv, gamma, compmin);
        if (pp->ord < BMDP_SIZE_DIRECT)
            sum = bmdp_direct_eval_policy(pp, b, r1.vektor);
        else
            sum = bmdp_precond_eval_policy(BO_GMRES_ILU0, pp, &lu, b, r1.vektor, TRUE, 300, 1.0e-8, &global_it);
        val = mat_double_vektor_norm_diff(&r1, &r2, &n_max, &n_sum);
        sum = mat_double_vektor_sum(&r1);
        if (fabs(sum) < BMDP_ROW_VAL)
            sum = BMDP_ROW_VAL;
        if (n_sum / fabs(sum) < BMDP_ROW_VAL)
            stop = TRUE;
        else {
            r3.vektor = r1.vektor;
            r1.vektor = r2.vektor;
            r2.vektor = r3.vektor;
        }
    } /* while !stop */
#if BMDP_TRACE_LEVEL
  printf("compmin %i\n", compmin) ;
  printf("Policy\n") ;
  for (i = 0; i < pp->ord; i++)
    printf("%i ", ind[i]) ;
  printf("\n") ;
  printf("Matrix A:\n") ;
  mat_sparse_matrix_prt_small_matrix (pp);
  printf("Reward vector\n") ;
  for (i = 0; i < pp->ord; i++)
    printf("%.3e ", b->vektor[i]) ;
  printf("\n") ;
  printf("gain vector: ") ;
  for (i = 0; i < pp->ord; i++)
    printf("%.3e ", hh[i]) ;
  printf("\n") ;
#endif
    int ret;
    if (comp_stat) {
        mat_sparse_matrix_mult_scalar(pp, -1.0 / gamma);
        val = (gamma - 1.0) / gamma;
        for (i = 0; i < pp->ord; i++)
            pp->diagonals[i] -= val;
        if (pp->ord < BMDP_SIZE_DIRECT) {
            if ((ret = sol_direct_solution(pp, b, TRUE, NULL)) == NO_ERROR)
                printf("Stationary solution successful\n");
            else {
                printf("Stationary solution failed: %d\n", ret);
            }
        }
        else {
            param.n_proc = 1;
            /* param.method = STD_ILU0_GMRES  ; */
            /* param.method = STD_ILU0_BICGSTAB ; */
            param.method = STD_SOR;
            param.no_of_iterations = 1000;
            param.epsilon = 1.0e-8;
            param.epsilon_2 = 0.1;
            param.epsilon_3 = 0.0;
            param.time_limit = 500.0;
            param.relaxation = 0.98;
            param.initials = NULL;
            param.overall_result = 0;
            param.subnet_results = NULL;
            val = 1.0 / (double) pp->ord;
            for (i = 0; i < pp->ord; i++)
                b->vektor[i] = val;
            /* sol_gmres(pp, b, &param, 1) ; */
            /* sol_bicgstab(pp, b, &param, 0) ; */
            sol_sor_iteration(&pp, b, &param, TRUE, FALSE, NULL);
        }
        val = mat_double_vektor_inner_product(b, &r1);
        printf("sum of gain values %.5e stationary gain value %.5e\n", sum, val);
    }
    mat_double_vektor_free(b);
    mat_sparse_matrix_free(pp);
    if (lu != NULL)
        mat_sparse_matrix_free(lu);
    efree(vv);
    efree(r2.vektor);
    return (r1.vektor);
} /* bmdp_evaluate_minmax_policy */

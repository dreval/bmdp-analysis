/*********************
        Datei:  ctmdp_stat_func.h
        Datum:  1.3.15
        Autor:  Peter Buchholz
        Inhalt: Externe Deklarationen fuer ctmdp_stat_func.c
        Aenderungen:

**********************/
#ifndef CTMDP_STAT_FUNC_H
#define CTMDP_STAT_FUNC_H
#include "tst_ctmdp_stat.h"



/* declaration of functions */

/**********************
mdp_direct_eval_policy
Datum:  12.04.10
Autor:  Peter Buchholz
Input:  pp Policy Matrix
        rew Reward Vektor

Output: Funktionswert average reward der policy
        hv zustandsspezifische Rewards
Side-Effects:
Description:
Die Funktion wertet eine Politik mit einem direkten Verfahren aus.
Aus der Funktion bo_direct_eval_policy kopiert!
***********************************************************************/
extern double mdp_direct_eval_policy(Sparse_matrix *pp, Double_vektor *rew, double *hv) ;

 /**********************
mdp_pre_power_eval_policy
Datum:  21.06.11
Autor:  Peter Buchholz
Input:  pp Policy Matrix
        rew Reward Vektor

Output: Funktionswert average reward der policy
        hv zustandsspezifische Rewards
Side-Effects:
Description:
Die Funktion wertet eine Politik iterativ mit Power + 
Präkonditionierung aus.
As bo_pre_power_eval_policy kopiert.
***********************************************************************/
double mdp_pre_power_eval_policy(Sparse_matrix *pp, Sparse_matrix *lu, Double_vektor *b, double *hv, 
				 int max_iter, double max_eps, int *global_it) ;

/**********************
mdp_value_iteration
Datum:  17.06.11
Autor:  Peter Buchholz
Input:  mdp MDP-Matrix
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maixmale Laufzeit
        epsilon Fehlerschranke
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke für den Reward mit Hilfe der value-Iteration.
***********************************************************************/
extern double mdp_value_iteration(Mdp_matrix *mdp, Double_vektor *rew, int *ind, int max_iter, double ti_max, double epsilon, short max) ;

/**********************
bo_policy_iteration
Datum:  25.03.10
Autor:  Peter Buchholz
Input:  pl, pu Bounding matrices
        ds Vektor der Zeilensummen
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maixmale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke für den Reward mit Hilfe der policy-Iteration.
***********************************************************************/
extern double mdp_policy_iteration(Mdp_matrix *mdp, Double_vektor *rew, int *ind, int max_iter, 
				   double ti_max, double epsilon, short max, int local_it, double local_eps) ;

/**********************
mdp_precond_iteration
Datum:  15.06.10
Autor:  Peter Buchholz
Input:  mdp MDp matrix
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maximale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke f�r den Reward mit Hilfe einer iterativen Technik
und ILU Präkonditionierung.
***********************************************************************/
extern double mdp_precond_iteration(Mdp_matrix *mdp, Double_vektor *rew, int *ind, int max_iter, double ti_max, 
				    double epsilon, short max, int local_it, double local_eps, int method) ;


/**********************
mdp_mixed_iteration
Datum: 21.06.11
Autor:  Peter Buchholz
Input:  mdp MDP Matrix
        rew Rewardvektor
        ind Startpolitik
        max_iter maximale Iterationszahl
        ti_max maximale Laufzeit
Output: Funktionswert maximaler Reward
        ind optimale Politik
Side-Effects:
Description:
Die Funktion berechnet die obere Schranke für den Reward mit Hilfe einer Kombination
aus value und policy Iteration.
***********************************************************************/
extern double mdp_mixed_iteration(Mdp_matrix *mdp, Double_vektor *rew, int *ind, int max_iter,double ti_max, 
				  double epsilon, short max, int local_it, double local_eps, int method);

#endif /* CTMDP_STAT_FUNC_H */

/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c fuer die angegebenen Funktionen
c (C) Copyright 1995 by Peter Buchholz
c (C) Copyright 1993, 94 by Peter Kemper
c
c     All Rights Reserved
c
c mat_int_vektor.c
c
c Aufgabe ist Funktionen zur Erzeugung und Behandlung von 
c Vektoren mit Eintraegen von Typ int
c 
c
c	Datum		Modifikationen
c	----------------------------------------------------------
c 1.8.97 Datei angelegt, Funktionen aus matrix.c entnommen
c 11.08.98 Kommentare werden mit der Funktion read_comment ueberlesen
c 27.08.13 Erg�nzungen f�r sgspn2nsolve
c 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/
#include <assert.h>
#include "list.h"
#include "global.h" 
#include "ememory.h"
#include "mat_int_vektor.h"
/**********************
mat_int_vektor_new ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
Int_vektor *mat_int_vektor_new(unsigned anzahl)
#else
Int_vektor *mat_int_vektor_new(anzahl)
     unsigned anzahl ;
#endif
{
  Int_vektor *v = (Int_vektor *)ecalloc(1,sizeof(Int_vektor)) ;
  
  if (0 < anzahl) 
  {
    v->no_of_entries = anzahl ;
    v->vektor = (int *)ecalloc(anzahl,sizeof(int)) ; 
  }
  return(v) ;
}

/**********************
mat_int_vektor_free ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_int_vektor_free(Int_vektor *v)
#else
int mat_int_vektor_free(v)
     Int_vektor *v ;
#endif
{
  if (!v)
    return(NO_ERROR);
  
  efree(v->vektor) ;
  efree(v) ; 
  return(NO_ERROR) ;
}
/**********************
mat_int_vektor_output ##e 
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_int_vektor_output(FILE *fp, Int_vektor *v)
#else
int mat_int_vektor_output(fp, v)
     FILE *fp ;
     Int_vektor *v ;
#endif
{
  unsigned i ;
  
  if (!fp || !v)
    return(OUTPUT_ERROR) ;
  fprintf(fp,"%i\n", v->no_of_entries) ;
  for (i=0 ;i<v->no_of_entries ; i++) 
    fprintf(fp,"%i\n", v->vektor[i]) ;    
  return(NO_ERROR) ; 
}

/**********************
mat_int_vektor_input ##e 
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_int_vektor_input(FILE *fp, Int_vektor *v)
#else
int mat_int_vektor_input(fp, v)
     FILE *fp ;
     Int_vektor *v ;
#endif
{
  unsigned i ;
  
  if (!fp || !v)
    return(INPUT_ERROR) ;
  fp = read_comment(fp) ;
  if (fscanf(fp," %i\n", &i)  == EOF || i != v->no_of_entries)
    return(INPUT_ERROR) ;
  for (i=0 ;i<v->no_of_entries ; i++) 
  {
   fp = read_comment(fp) ;
    if (fscanf(fp," %i\n", &v->vektor[i])  == EOF)
      return FALSE ; 
  }
  return(NO_ERROR) ;  
}   

/**
 * Einlesen eines Int_vektors aus fp ohne Kenntnis der Laenge.
 * \date 4.5.95
 * \param fp Zeiger auf File
 * \return Der gefuellte Vektor 
 * \author Peter Buchholz
 */
Int_vektor *mat_int_vektor_input_1(FILE *fp)
{
  unsigned i ;
  Int_vektor *v ;

  if ((!fp) || (fscanf(fp," %i\n", &i)  == EOF))
    return(NULL) ;
  v = mat_int_vektor_new(i) ;
  for (i=0 ;i<v->no_of_entries ; i++) 
    if (fscanf(fp," %i\n", &v->vektor[i])  == EOF)
      return(NULL) ; 
  return(v) ;  
}

/**********************
mat_int_vektor_realloc ##e verlaengert Vektor auf Vektorlaenge + Anzahl
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
Int_vektor *mat_int_vektor_realloc(Int_vektor *v, unsigned anzahl)
#else
Int_vektor *mat_int_vektor_realloc(v, anzahl)
     Int_vektor *v ;
     unsigned anzahl ;
#endif
{
  assert( 1 < anzahl ) ;
  v->no_of_entries = v->no_of_entries + anzahl ;
  v->vektor = (int *)erealloc((char *)v->vektor,
			      (unsigned)(v->no_of_entries*sizeof(int))) ;
  return(v) ;
}   
/**********************
mat_int_vektor_print_file ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_int_vektor_print_file(FILE *fp, Int_vektor *v)
#else
int mat_int_vektor_print_file(fp, v)
     FILE *fp ;
     Int_vektor *v ;
#endif
{
  unsigned i ;
  unsigned j ;
  
  if (!fp)
    return FALSE ;
  if (!v) 
  {
    fprintf(fp,"Int_vektor ist leer\n") ;
    return TRUE ; 
  }
  fprintf(fp,"\nAusgabe eines Int-Vektors mit %i Eintraegen\n", 
	  v->no_of_entries) ;
  fprintf(fp," (10 Eintraege pro Zeile):\n") ;
  for (i=j=0 ;i<v->no_of_entries ; i++, j++) 
  {
    if (10 == j ) 
    {
      j=0 ;
      fprintf(fp,"\n %i", v->vektor[i]) ; 
    }
    else
      fprintf(fp," %i", v->vektor[i]) ; 
  }
  fprintf(fp,"\nEnde des Vektors \n") ;    
  return TRUE ;  
}
    
/**
 * Setzt Wert an bestimmte Position im Vektor  
 * \date 18.11.1999  
 * \param v Zeiger auf Vektor, in dem der Wert an eine bestimmte Position gesetzt werden soll
 * \param pos Wert soll an diese Position gesetzt werden
 * \param val Dieser Wert soll gesetzt werden.
 * \return TRUE (immer)
 * \author Carsten Tepper  
 */
int mat_int_vektor_set_pos(Int_vektor *v, int pos, int val)
{
  assert(v && v->vektor);
  assert(0 <= pos && pos < v->no_of_entries) ;
  v->vektor[pos] = val; 
  return TRUE;
} 
    
/**
 * Liefert den Wert einer bestimmten Position im Vektor.
 * \date 25.11.1999  
 * \param pos Position des Wertes, der geliefert werden soll
 * \param v Zeiger auf Vektor, aus dem ein Wert einer bestimmten Position geholt werden soll
 * \return Den Wert an der angegebenen Position
 * \author Carsten Tepper  
 */
int mat_int_vektor_get_pos(Int_vektor *v, int pos)
{
  int val;
  assert(v && v->vektor);
  assert(0 <= pos && pos < v->no_of_entries);
  val = v->vektor[pos];
  return val; 
} 

/*
 * Setzt Wert fuer Teilvektor [start,stop[
 * \date 30.11.1999  
 * \author Carsten Tepper
 * \param v Zeiger auf Vektor, in dem der Teilvektor mit dem angegebenen Wert gesetzt werden soll
 * \param start Startposition von der an der Wert gesetzt wird 
 * \param stop Stopposition bis wohin der Wert gesetzt wird (Position stop wird nicht gesetzt)
 * \param val Wert der in den Teilvektor geschrieben werden soll 
 * \return TRUE (immer)
 */
int mat_int_vektor_set_interval(Int_vektor *v, int start, int stop, short val)
{
  while (start < stop)
    {
      v->vektor[start] = val;
      start++;
    }

  return TRUE;
}

/**
 * Addiert zwei Vektoren (a = a + b).
 * Das Resultat der Addition steht im Vektor a.
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param a Zeiger auf Vektor a
 * \param b Zeiger auf Vektor b
 */
void mat_int_vektor_plus(Int_vektor *a, Int_vektor *b)
{
  int i ;
  for (i=0 ; i < a->no_of_entries ; i++)
    a->vektor[i] = a->vektor[i] + b->vektor[i];
}

/**
 * Subtrahiert zwei Vektoren (a = a - b).
 * Das Resultat der Subtraktion steht im Vektor a.
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param a Zeiger auf Vektor a
 * \param b Zeiger auf Vektor b
 */
void mat_int_vektor_minus(Int_vektor *a, Int_vektor *b)
{
  int i ;
  for (i=0 ; i < a->no_of_entries ; i++)
    a->vektor[i] = a->vektor[i] - b->vektor[i];
}

/**
 * Modulo zweier Vektoren (a = a % b).
 * Das Ergebnis steht im Vektor a. 
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param a Zeiger auf Vektor a
 * \param b Zeiger auf Vektor b
 */
void mat_int_vektor_modulo(Int_vektor *a, Int_vektor *b)
{
  int i ;
  for (i=0 ; i < a->no_of_entries ; i++)
    a->vektor[i] = a->vektor[i] % b->vektor[i]; 
}

/**
 * Division zweier Vektoren (a = a / b).
 * Das Ergebnis steht im Vektor a.
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param a Zeiger auf Vektor a
 * \param b Zeiger auf Vektor b
 */
void mat_int_vektor_div(Int_vektor *a, Int_vektor *b)
{
  int i ;
  for (i=0 ; i < a->no_of_entries ; i++)
    a->vektor[i] = a->vektor[i] / b->vektor[i];
}

/**
 * Multiplikation zweier Vektoren (a = a * b).
 * Das Ergebnis steht im Vektor a.
 * \date 03.11.1999
 * \author Carsten Tepper 
 * \param Zeiger auf Vektor a
 * \param Zeiger auf Vektor b
 */
void mat_int_vektor_mul(Int_vektor *a, Int_vektor *b)
{
  int i ;
  for (i=0 ; i < a->no_of_entries ; i++)
    a->vektor[i] = a->vektor[i] * b->vektor[i];
}

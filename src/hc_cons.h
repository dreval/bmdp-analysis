/*********************
        Datei:  ccons.h
        Datum:  1.8.97
        Autor:  Peter Buchholz
        Inhalt: globale Konstanten
 
        Datum   Aenderung
----------------------------------------------------------------------
19.12.99 Datei angelegt,
 
**********************/

#ifndef HCCONS_H
#define HCCONS_H  

#define ADAPTIVE_COMP FALSE
#define ALPHA_FACTOR 0.0
#define ADAPTIVE_EPSILON 1.0e-12
#define ADAPTIVE_ITER   2000

#endif 


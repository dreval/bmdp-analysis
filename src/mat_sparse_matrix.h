/*********************
	Datei:	mat_sparse_matrix.h
	Datum: 	1.8.97
	Autor:	Peter Kemper
	Inhalt:	Funktionen zur Behandlung von spaerlich besetzten
	Matrizen, dieses Modul greift auf mat_row.c/h zurueck


	Datum 	Aenderung
----------------------------------------------------------------------
1.8.97 Datei angelegt, Funktionen entstammen matrix.c/matrix.h
06.02.98 Einfuegen der Funktionen mat_sparse_matrix_to_p, mat_sparse_matrix_normalize
         und mat_sparse_matrix_mult_vektor
23.10.98 Einfuegen der Funktion mat_sparse_matrix_extract
14.02.99 Aenderung der Datenstruktur durch einfuegen der Arrays l, u und d
         Funktion zum setzen der Arrays
10.01.03 Funktionsk�pfe f�r neue Funktionen eingef�gt
07.02.03 Neue Funktionsk�pfe eingef�gt
12.02.10 Funktion mat_sparse_matrix_row_sum eingefuegt
15.02.10 Funktion mat_sparse_matrix_add_eps eingefuegt
09.03.11 Funktion mat_sparse_matrix_output_with_comments
28.08.12 Funktion mat_sparse_matrix_extract_with_diags
28.08.13 �nderungen f�r sgspn2nsolve
01.03.15 Neue Funktion mat_sparse_matrix_is_smaller

**********************/
#ifndef MAT_SPARSE_MATRIX_H
#define MAT_SPARSE_MATRIX_H

#include <stdio.h>
#include "basic_io.h"
#include "mat_row.h"
#include "mat_double_vektor.h"

/******************* Defines	 **********************/


/******************* Data structures	 **********************/
typedef struct 
{
  unsigned ord;            /* matrix dimension */
  Row      *rowind;        /* array of matrix rows, !!! anders bei PB 
			      dort Row **rowind */
  double   *diagonals;     /* separate Speicherung der Diagonalelemente */
  unsigned n_of_nonzeros;  /* number of non-zeros in the matrix */
  unsigned bandwidth;      /* maximal bandwidth of a matrix row */
  double   diagmax;        /* maximum diagonal element */
  double   diagmin;        /* minimum diagonal element */
  short    active ;        /* nicht verwendet, wg Kompatibilitaet */ 
  int      *l, *u, *d ;    /* Arrays zur Lokalisierung der unteren und oberen
                             Dreiecksmatrix un der Diagonalen */
} Sparse_matrix ;

/******************* Functions	 **********************/
/**********************
mat_sparse_matrix_new ##e Konstruktor
Datum:  14.2.95
Autor:  Peter Kemper
Input:  unsigned anzahl, falls 0 < anzahl Laenge des Vektors
Output: neues Element
Side-Effects:
Description:
Es wird ein neues Element vom Typ Sparse_Matrix auf eigenem
Speicherplatz erzeugt, 
falls eine Vektorlaenge angegeben ist (anzahl > 0) wird 
zusaetzlich ein Vektor entsprechender Laenge fuer double Eintraege
als Diagonaleintraege und Row Eintraege fuer duennbesetzte Zeilen
allokiert
anderenfalls liegen keine Vektoren vor.
**********************/
extern Sparse_matrix *mat_sparse_matrix_new(unsigned anzahl);

/**
 * Achtung: assignes q->n_of_nonzeros the appropriate value
 * \date 4.10.99
 * \author Peter Kemper
 * \param q Zeiger auf Sparse-Matrix
 * \return ???
 */
extern int mat_sparse_matrix_set_n_of_nonzeros (Sparse_matrix *q) ; 

/**********************
mat_sparse_matrix_free ##e Destruktor
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
Der Speicherplatz fuer v wird freigegeben, dies gilt ebenso fuer 
die Komponente vektor.
**********************/
extern int mat_sparse_matrix_free(Sparse_matrix *v);

/**********************
mat_sparse_matrix_set_diagonals ##e 
Datum:  14.2.95             (Aenderung 27.3.95 PB)
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description: fuer die Matrix q werden die Zeilensummen berechnet und
gesetzt, bei negative == 0 mit positivem Vorzeichen, sonst mit negativem
Vorzeichen.
**********************/
extern int mat_sparse_matrix_set_diagonals (Sparse_matrix *q, int negative) ;

/**********************
mat_sparse_non_zeros  ##= Anzahl Nonzeros (inclusive Diagonaleintraege) bestimmen
Datum:  9.7.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
extern int mat_sparse_matrix_non_zeros (Sparse_matrix *q) ;


/**********************
mat_sparse_matrix_output ##e Ausgabefunktion (ohne Kommentar, je Eintrag 1 Zeile)
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege der Matrix werden in das angegebene File ausgegeben.
Falls write_diag == TRUE werden Diagonaleintraege mit ausgegeben.
Annahme: fp zeigt auf eine Datei, die bereits zum Schreiben geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
Ausgabeformat: Anzahl Eintraege \n Wert1 \n Wert2 ....
**********************/
extern int mat_sparse_matrix_output(FILE *fp, Sparse_matrix *v, short write_diag);

/**********************
mat_sparse_matrix_output ##e Ausgabefunktion (ohne Kommentar, je Eintrag 1 Zeile)
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege der Matrix werden in das angegebene File ausgegeben.
Falls write_diag == TRUE werden Diagonaleintraege mit ausgegeben.
Annahme: fp zeigt auf eine Datei, die bereits zum Schreiben geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
Ausgabeformat: Anzahl Eintraege \n Wert1 \n Wert2 ....
**********************/
extern int mat_sparse_matrix_output_with_comments(FILE *fp, Sparse_matrix *v, short write_diag);

/**********************
mat_sparse_matrix_input ##e Einlesefunktion
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege einer Matrix werden aus dem angegebenen File 
eingelesen und in v gespeichert.
Es werden genau so viele Eintraege eingelesen, wie in v->no_of_entries
angegeben ist. 
Annahme: fp zeigt auf eine Datei, die bereits zum Lesen geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
**********************/
extern int mat_sparse_matrix_input(FILE *fp, Sparse_matrix *v, short read_diag);

/**********************
mat_sparse_matrix_copy ##e Matrix auf neu allokierten Speicher kopieren 
Datum:  1.9.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Es wird Speicher neu allokiert und die Werte aus q werden uebertragen.
**********************/
extern Sparse_matrix *mat_sparse_matrix_copy (Sparse_matrix *q) ;

/**********************
mat_sparse_set_lud ##e 
Datum:  14.02.99
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Setzt die Arrays l, u und d, um die obere und untere Dreiecksmatrix sowie die
Diagonalelemente in der Matrix zu identifizieren. l und u enthalten des letzte und
erste Element der Zeilen der unteren und oberen Dreiecksmatrix.
Falls eine Zeile der unteren Dreiecksmatrix leer ist, wird der zugehoerige Wert von l
mit 0 belegt, falls eine Zeile der oberen Dreiecksmatrix leer ist, so wird der 
Wert in u auf no_of_entries gesetzt. 
d enthaelt den Index eines evtl. Diagonalmatrix in der Matrix.
**********************/
extern void  mat_set_lud(Sparse_matrix *q) ; 

/**********************
mat_sparse_matrix_transpose ##e Erzeugen einer transponierten Matrix
Datum:  9.8..95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: 
Fuer die transponierte Matrix wird eigener Speicher allokiert.
**********************/
extern Sparse_matrix *mat_sparse_matrix_transpose (Sparse_matrix *q, int col) ;

/**********************
mat_sparse_matrix_max_elem ##e berechnet das betragsmaessig maximale Element einer Matrix
Datum:  5.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
extern double mat_sparse_matrix_max_elem(Sparse_matrix *q) ;

/**********************
mat_sparse_matrix_insert_diagonals ##e vormals mat_insert_diagonals
Datum:  8.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Ordnet die Diagonalelemente direkt in die Zeilen ein
**********************/
extern int mat_sparse_matrix_insert_diagonals (Sparse_matrix *q)  ;

/**********************
mat_sparse_matrix_extract_diagonals ##e
Datum:  8.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Entfernt Diagonalelemente aus einer Zeile und speichert 
             sie separat
**********************/
extern int mat_sparse_matrix_extract_diagonals (Sparse_matrix *q) ;
 
/**********************
mat_sparse_matrix_max_diagonal ##e
Datum:  20.6.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Compute the absolute value of the maximum diagonal element
**********************/
extern double mat_sparse_matrix_max_diagonal(Sparse_matrix *q) ;

/**********************
mat_sparse_matrix_reset ##e
Datum:  13.02.97
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Set all elements to zero
**********************/
extern int mat_sparse_matrix_reset(Sparse_matrix *q) ;

/**********************
mat_sparse_matrix_check_zero ##e
Datum:  13.02.97
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: The procedure yields FALSE if one value is zero and
TRUE otherwise
**********************/
extern int mat_sparse_matrix_check_zero(Sparse_matrix *q) ;

/**********************
mat_sparse_matrix_add_matrices
Datum:  22.01.97
Autor:  Peter Buchholz
Input:  q1, q2 Eingabematrizen gleicher Dimension!
Output: q1 Summe der beiden Eingabematrizen
Side-Effects:
Description:
Die Prozedur addiert Matrix q2 zu q1.
***********************************************************************/
extern int mat_sparse_matrix_add_matrices
            (Sparse_matrix *q1, Sparse_matrix *q2) ;

/**********************
mat_sparse_matrix_add_mult_matrices
Datum:  22.01.97
Autor:  Peter Buchholz
Input:  q1, q2 Eingabematrizen gleicher Dimension!
Output: q1 Summe der beiden Eingabematrizen
Side-Effects:
Description:
Die Prozedur berechnet q1 = q1 + s*q2, wobei q1, q2 Matrizen sind und s ein Skalar ist.
***********************************************************************/
extern int mat_sparse_matrix_add_mult_matrices
            (Sparse_matrix *q1, Sparse_matrix *q2, double s) ;

/**********************
mat_sparse_matrix_mult ##e
Datum:  01.07.97
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: The procedure multiplies two sparse_matrices, the
result is the product. Diagonal elements have to be inserted in the rows.
See function for insertion and extraction of diagonal elements.
If trans > 0, then the second matrix is transposed, otherwise it is assumed that the
matrix is already transposed.
**********************/
extern Sparse_matrix *mat_sparse_matrix_mult
                       (Sparse_matrix *q1, Sparse_matrix *qi, int trans) ;


/**********************
mat_sparse_matrix_print_file ##e Ausgabefunktion (mit Kommentar)
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege der Matrix werden in das angegebene File ausgegeben.
Annahme: fp zeigt auf eine Datei, die bereits zum Schreiben geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen. 
Die Funktion greift auf mat_row_print_file zurueck.
**********************/
extern int mat_sparse_matrix_print_file(FILE *fp, Sparse_matrix *v) ;

/**********************
mat_sparse_matrix_sort_reduce_elems    
Datum:  29.3.95
Autor:  Peter Buchholz
Input:  q Sparse_matrix
Output: q Sparse_matrix mit zeilenweise sortierten Elementen
Side-Effects:
Description:
Die Funktion sortiert die Elemente in den Zeilen einer Sparse_matrix nach
ihren Spaltenindizes und addiert Raten von Transitionen mit
gleichem Spaltenidenx
**********************/
extern int mat_sparse_matrix_sort_reduce_elems(Sparse_matrix *q) ;

/**********************
mat_sparse_matrix_mult_vektor
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Hilfsvektor
        diag Flag, ob die Diagonalelemente mit einbezogen werden sollen
Output: 
Side-Effects:
Description:
Die Funktion berechnet einen Iterationsschritt der Power Methode
p = p*q. Das Ergebnis wrid im Vektor p zurueckgeliefert, der auch den
Iterationsvektor als Eingabeparameter beihaltet. Falls diag=TRUE ist, wird auch
mit den Diagonalelementen multipliziert, bei diag=FALSE, werden die Diagonalelemente
weggelassen.
Die Laenge des Vektor ph wird nicht ueberprueft, da die Funktion auch fuer 
n x m Matrizen verwendet werden kann. 
**********************/
extern void mat_sparse_matrix_mult_vektor 
      (Sparse_matrix *q, Double_vektor **p, Double_vektor **ph, short diag) ;


/**********************
mat_diagonal_vektor_mult
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Zielvektor
Output: 
Side-Effects:
Description:
Die Funktion berechnet das Produkt
ph = p*q.diag. Das Ergebnis wird im Vektor ph zurueckgeliefert. 
Der Vektor ph wird nicht initialisiert, sondern die neuen Werten zu den 
bisherigen Werten dazu addirt.
**********************/
extern void mat_diagonal_vektor_mult (Sparse_matrix *q, Double_vektor *p, Double_vektor *ph) ;

/**********************
mat_sparse_matrix_vektor_mult
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Zielvektorvektor
        diag Flag, ob die Diagonalelemente mit einbezogen werden sollen
Output: 
Side-Effects:
Description:
Die Funktion berechnet das Produkt
ph = p*q. Das Ergebnis wird im Vektor ph zurueckgeliefert. Falls diag=TRUE ist, wird auch
mit den Diagonalelementen multipliziert, bei diag=FALSE, werden die Diagonalelemente
weggelassen. Der Vektor ph wird nicht initialisiert, sondern die neuen Werten zu den 
bisherigen Werten dazu addirt.
Die Laenge des Vektor ph wird nicht ueberprueft, da die Funktion auch fuer 
n x m Matrizen verwendet werden kann. 
**********************/
extern void mat_sparse_matrix_vektor_mult 
(Sparse_matrix *q, Double_vektor *p, Double_vektor *ph, short diag) ; 

/**********************
mat_sparse_matrix_vektor_mult
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Zielvektorvektor
        diag Flag, ob die Diagonalelemente mit einbezogen werden sollen
Output: 
Side-Effects:
Description:
Die Funktion berechnet das Produkt
ph = q*p. Das Ergebnis wird im Vektor ph zurueckgeliefert. Falls diag=TRUE ist, wird auch
mit den Diagonalelementen multipliziert, bei diag=FALSE, werden die Diagonalelemente
weggelassen. Der Vektor ph wird nicht initialisiert, sondern die neuen Werten zu den 
bisherigen Werten dazu addiert.
Die Laenge des Vektor ph wird nicht ueberprueft, da die Funktion auch fuer 
n x m Matrizen verwendet werden kann. 
**********************/
extern void mat_sparse_transposed_matrix_vektor_mult 
   (Sparse_matrix *q, Double_vektor *p, Double_vektor *ph, short diag) ;


/**********************
mat_sparse_matrix_matrix_vektor_product
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        v Iterationsvektor
Output: w Ergebnisvektor
Side-Effects:
Description:
Die Funktion berechnet das Produkt w = v q. w und v muessen die geliche Laenge haben!
**********************/
extern void mat_sparse_matrix_matrix_vektor_product 
      (Sparse_matrix *q, Double_vektor *v, Double_vektor *w) ;

/**********************
mat_sparse_matrix_transposed_matrix_vektor_product
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        v Iterationsvektor
Output: w Ergebnisvektor
Side-Effects:
Description:
Die Funktion berechnet das Produkt w = q*v. v und q muessen die gleiche L�nge haben!
**********************/
extern void mat_sparse_matrix_transposed_matrix_vektor_product 
               (Sparse_matrix *q, Double_vektor *v, Double_vektor *w)  ;


/**********************
mat_sparse_matrix_to_p
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
Output: q transformierte stochastische Matrix
        alpha Wert des absolut groessten Diagonalelements
Side-Effects:
Description:
Die Funktion transformiert eine Generatormatrix mittels Randomization in
eine stochastische Matrix. Falls die Matrix negative Elemente enthaelt,
so bleibt so wird WRONG_MATRIX als Funktionswert 
zurueckgegebem, im Erfolgsfall wird NO_ERROR zurueckgegeben.
ACHTUNG: Diagonalelemente in der Eingabematrix werden nicht beachtet.
**********************/
extern int mat_sparse_matrix_to_p (Sparse_matrix *q, double *alpha) ;

/**********************
mat_sparse_matrix_normalize
Datum:
Autor:  Peter Buchholz
Input:  q Eingabematrix
        diag falls diag=TRUE werden in NUllzeilen die Diagonalelemente auf 1 gesetzt
Output: q transformierte Eingabematrix
Side-Effects:
Description:
Die Funktion normalisiert die Zeilen einer nicht negativen Matrix so, dass
die Zeilensumme 1 ergibt. Falls diag FALSE ist, werden NUllzeilen nicht veraendert, 
bei diag TRUE werden die Diagonalelemente der Nullzeilen auf 1 gesetzt.
ACHTUNG: Wenn die Matrix negative Elemente beinhaltet, so bricht die Funktion mit
Rueckgabewert WRONG_MATRIX ab, ansonsten liefert sie NO_ERROR.
**********************/
extern int mat_sparse_matrix_normalize  (Sparse_matrix *q, short diag) ;

/**********************
mat_sparse_matrix_mult_scalar
Datum:  08.05.98
Autor:  Peter Buchholz
Input:  q Eingabematrix
        s Skalarwert
Output: q wobei alle Elemente mit s multipliziert wurden
Side-Effects:
Description:
Die Funktion multipliziert alle Elemente von q mit s.
**********************/
extern void mat_sparse_matrix_mult_scalar (Sparse_matrix *q, double s) ;

/**********************
mat_sparse_matrix_extract
Datum:  12.6.95
Autor:  Peter Buchholz
Input:  q Subnetzmatrix
        row_start Erste Zeile der Submatrix
        row_stop Letzte Zeile + 1 der Submatrix
        col_start Erste Zeile der Submatrix
        col_stop Letzte Zeile + 1 der Submatrix
Output: Funktionswert liefert die Untermatrix, die zum angegebenen Block
        gehoert.
Side-Effects:
Description:
Die Funktion extrahiert eine Submatrix mit den angegbenen Blockgrenzen aus 
einer Sparse_amtrix. Die Submatrix wird als Sparse_matrix zurueckgegeben. Die
urspruengliche Sparse_matrix wird nicht veraendert.
***********************************************************************/
extern Sparse_matrix *mat_sparse_matrix_extract
  (Sparse_matrix *q, int row_start, int row_stop, int col_start, int col_stop) ;

/**********************
mat_sparse_matrix_extract_with_diagonals
Datum:  27.08.12
Autor:  Peter Buchholz
Input:  q Subnetzmatrix
        row_start Erste Zeile der Submatrix
        row_stop Letzte Zeile + 1 der Submatrix
        col_start Erste Zeile der Submatrix
        col_stop Letzte Zeile + 1 der Submatrix
Output: Funktionswert liefert die Untermatrix, die zum angegebenen Block
        gehoert.
Side-Effects:
Description:
Die Funktion extrahiert eine Submatrix mit den angegbenen Blockgrenzen aus 
einer Sparse_matrix. Die Submatrix wird als Sparse_matrix zurueckgegeben. Die
urspruengliche Sparse_matrix wird nicht veraendert. Falls die Submatrix keine
Elemente enthaelt wird NULL zurueckgegeben!! Im Gegensatz zur
Funktion mat_sparse_matrix_extract werden die Diagonalelemente aus den Diagonalsubmatrizen
�bernommen und sonst auf 0 belassen.
***********************************************************************/
extern Sparse_matrix *mat_sparse_matrix_extract_with_diags
(Sparse_matrix *q, int row_start, int row_stop, int col_start, int col_stop) ;

/**********************
mat_sparse_matrix_gen_identity
Datum:  20.02.02
Autor:  Peter Buchholz
Input:  ord Dimenion der Matrix
        in_row falls true Werte in Zeile + Diagonalen bei false nur in der Diagonalen
Output: Funktionswert liefert eine EInheitsmatrix der Dimesnion ord
Side-Effects:
Description:
Die Funktion generiert eine Einheitsmatrix der Diemsnion ord.
***********************************************************************/
extern Sparse_matrix *mat_sparse_matrix_gen_identity (int ord, short in_row) ;

/**********************
mat_sparse_matrix_compute_bandwidth
Datum:  14.11.02
Autor:  Peter Buchholz
Input:  q Matrix
        diag falls diag > 0 wird angenommen, dass das Diagonalelement != 0 ist
             falls diaf = 0 wird das Diagonalelement nicht beachtet!!
Output: Funktionswert liefert die Bandbreite, die auch im Parameter
        bandwidth der Matrix gesetzt wird.
Side-Effects:
Description:
Die Funktion berechnet die maximale Bandbreite einer Matrix.
Es wird davon ausgegangen, dass die Elemente in den Matrixzeilen geordnet sind!
***********************************************************************/
extern int mat_sparse_matrix_compute_bandwidth (Sparse_matrix *q, short diag) ;

/**********************
mat_sparse_matrix_permute_matrices
Datum:  12.08.97 (urspr�ngliche Realisierung in matorder)
Autor:  Peter Buchholz
Input:  matrices Matrizenarray
        perm Zustandspermutation
        n Anzahl Matrizen in matrices
Output: matrices Matrizenarray in dme die Zeilen und Spalten
                aller Matrizen wie in perm beschrieben permutiert wurden
Side-Effects:
Description:
Die Funktion permutiert die Zeilen und Spalten der Matrizen in matrices,
wie in der Permutation perm angegeben.
***********************************************************************/
extern int mat_sparse_matrix_permute_matrices(Sparse_matrix **matrices, int *perm, int n) ;

/**********************
mat_sparse_matrix_prt_small_matrix
Datum:  25.08..05
Autor:  Peter Buchholz
Input:  q Matrix
Output: 
Side-Effects:
Description:
Die Funktion druckt eine Matrix als dichtbesetzte Matrix aus
***********************************************************************/
extern void mat_sparse_matrix_prt_small_matrix (Sparse_matrix *q) ;

/**********************
mat_prt_small_matrix_without_diagonals
Datum:  01.03.10
Autor:  Peter Buchholz
Input:  q Matrix
Output: 
Side-Effects:
Description:
Die Funktion druckt eine Matrix als dichtbesetzte Matrix aus und geht davon aus, dass
die Diagonalelements in dne Zielne eingeordnet sind.
***********************************************************************/
extern void mat_prt_small_matrix_without_diagonals(Sparse_matrix *q, int co) ;

/**********************
mat_sparse_matrix_prt_structure
Datum:  15.11.02
Autor:  Peter Buchholz
Input:  q Matrix
        diag 0 keine ber�cksichtigung der Diagonalaelemente
             1 Ber�cksichtigung der Diagonalelemente
            -1 Diagonalelemenet als ungleich 0 angenommen
Output: 
Side-Effects:
Description:
Die Funktion permutiert die Zeilen und Spalten der Matrizen in matrices,
wie in der Permutation perm angegeben.Druckt die STruktur der Nichtnullelemente 
einer Matrix aus.
***********************************************************************/
extern void mat_sparse_matrix_prt_structure (Sparse_matrix *q, int diag) ;

/**********************
mat_sparse_matrix_norm
Datum:  10.01.03
Autor:  Peter Buchholz
Input:  q1, q2 Matrizen
Output: n_sum, n_max Summen- und Maximumsnorm der Matrizen
Side-Effects:
Description:
Die Funktion berechent die Normen der Matrix |q1 -q2|. Die Matrizen m�ssen gleiche 
Dimension haben. 
***********************************************************************/
#ifndef CC_COMP
extern int mat_sparse_matrix_norm (Sparse_matrix *q1, Sparse_matrix *q2,
				   double *max_norm, double *sum_norm)  ;
#else
extern int mat_sparse_matrix_norm () ;
#endif

/**********************
mat_sparse_matrix_overwrite
Datum:  10.01.03
Autor:  Peter Buchholz
Input:  q1, q2 Matrizen
Output: q1 enth�lt Kopie von q2
Side-Effects:
Description:
Die Funktion �berschreibt q1 mit q2. Die Matrizen m�ssen gleiche 
Dimension und identische Nichtnullelemente haben. 
***********************************************************************/
#ifndef CC_COMP
extern int mat_sparse_matrix_overwrite(Sparse_matrix *q0, Sparse_matrix *q1) ;
#else
extern int mat_sparse_matrix_overwrite() ;
#endif

/**********************
mat_column_vector_to_matrix
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  p
Output: R�ckgabewert erzeugte Matrix
Side-Effects:
Description:
Die Funktion transformiert einen Spaltenvektor in eine Matrix.
***********************************************************************/
#ifndef CC_COMP
extern Sparse_matrix *mat_column_vector_to_matrix (Double_vektor *p) ;
#else
extern Sparse_matrix *mat_column_vector_to_matrix () ;
#endif

/**********************
mat_row_vector_to_matrix
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  p
Output: R�ckgabewert erzeugte Matrix
Side-Effects:
Description:
Die Funktion transformiert einen Zeilenvektor in eine Matrix.
***********************************************************************/
extern Sparse_matrix *mat_row_vector_to_matrix (Double_vektor *p) ;

/**********************
mat_sparse_matrix_eliminate_small_values
Datum:  16.01.03
Autor:  Peter Buchholz
Input:  q Matrix, deren Element gl�scht werdne sollen
        threshold Grenze, ab der Elemente gel�scht werden
Output: q modifizierte Matrix
Side-Effects:
Description:
Die Funktion l�scht alle ELemente < threshold aus einer Matrix.
***********************************************************************/
#ifndef CC_COMP
extern short mat_sparse_matrix_eliminate_small_values(Sparse_matrix *q, double threshold) ;
#else
extern short mat_sparse_matrix_eliminate_small_values() ;
#endif

/**********************
mat_transform_matrix_to_vector
Datum:  07.02.03
Autor:  Peter Buchholz
Input:  p1 substochastische Eingabematrix
Output: pv Vektor der Zeilensummen der Matrix
        pi auf 1 normierter Vektor der Spaltensummen
        R�ckgabewert 1, falls p1 = pv^T pi, 0 falls p1=0 und -1 sonst
Side-Effects:
Description:
Die Funktion berechnet aus einer Matrix p den Vektor der Zeilensummen und
der normierten Spaltensummen. 
Diagoinalelemente werden nicht ber�cksichtigt!!
Der R�ckgabewert zeigt an, ob P=pv^T * pi gilt.
***********************************************************************/
#ifndef CC_COMP
extern int mat_transform_matrix_to_vector(Sparse_matrix *p1, Double_vektor **pv, Double_vektor **pi); 
#else
int mat_transform_matrix_to_vector() ;
#endif

/**********************
mat_transform_vectors_to_matrix
Datum:  09.02.03
Autor:  Peter Buchholz
Input:  p1, p2 Eingabevektoren
Output: R�ckgabewert erzeugte Matrix
Side-Effects:
Description:
Die Funktion berechnet P = p1^T * p2 und gibt resultierende Matrix als Funktionswert zur�ck.
***********************************************************************/
#ifndef CC_COMP
extern Sparse_matrix *mat_transform_vectors_to_matrix (Double_vektor *p1, Double_vektor *p2) ;
#else
extern Sparse_matrix *mat_transform_vectors_to_matrix () ;
#endif

/**********************
mat_expand_matrix 
Datum:  7.5.98
Autor:  Peter Buchholz
Input:  q Sparse Matrix
        col Spaltendimension
        l, u Dimension der linken, rechtenIdentitaetsmatrix 
Output: Funktionswert expandierte Matrix
Side-Effects:
Description:
Die Funktion expandiert eine Matrix q zu I_l \otimes q \times I_u.
***********************************************************************/
#ifndef CC_COMP
extern Sparse_matrix *mat_expand_matrix (Sparse_matrix *q, int cols, int l, int u) ; 
#else
extern Sparse_matrix *mat_expand_matrix () ;
#endif

/**********************
mat_sparse_matrix_row_sum
Datum:  12.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
        diag treu falls Diagonalelement ber�cksichtigt werden
Output: rs row sums
Side-Effects:
Description:
Die Funktion berechnet die Zielensummen der Matrix q. Falls diag=true,
werden Diagonalelemente ber�cksichtigt, ansonsten nicht.
Die Summe werden in den Vektor rs geschrieben, der die richtige
L�nge haben muss.
***********************************************************************/
extern void mat_sparse_matrix_row_sum (Sparse_matrix *q, Double_vektor *rs, short diag) ;

/**********************
mat_sparse_matrix_upper_triangular
Datum:  12.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: TRUE q is upper triangular, 
        FALSE otherwhise
Side-Effects:
Description:
Die Funktion �berpr�ft, ob q eine obere Dreiecksmatrix ist. 
***********************************************************************/
extern short mat_sparse_matrix_upper_triangular(Sparse_matrix *q) ;

/**********************
mat_sparse_matrix_transform_stochastic
Datum:  12.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: q transformierte Matrix
Side-Effects:
Description:
Die Funktion transformiert die Matrix q in eine (sub)stochastische Matrix mit Hilfe von alpha.
Falls alpha nicht gro� genug gew�hlt wurde, wird der Wert auf das betragsm��ig gr��te
Diagonalelement gestezt. 
***********************************************************************/
extern void mat_sparse_matrix_transform_stochastic (Sparse_matrix *q, double *alpha) ;


/**********************
mat_sparse_matrix_transform_rate
Datum:  12.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: q transformierte Matrix
Side-Effects:
Description:
Die Funktion transformiert die (sub)stochastische Matrix q in eine Ratenmatrix.
***********************************************************************/
extern void mat_sparse_matrix_transform_rate (Sparse_matrix *q, double alpha) ;

/**********************
mat_sparse_matrix_add_eps
Datum:  15.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
        eps Elemnt, das dazu addiert wird
        diag_eps Element, das zur Diagonalen dazu addiert wird
Output: q transformierte Matrix
Side-Effects:
Description:
Die Funktion addiert eps zu allen Nichtdiagonalelemente und diag_eps zu allen
Diagonalelementen
***********************************************************************/
extern void mat_sparse_matrix_add_eps (Sparse_matrix *q, double eps, double diag_eps) ;

/**********************
mat_add_submatrix
Datum:  5.7.95
Autor:  Peter Buchholz
Input:  q Ausgangsmatrix
        qh zu addierende Submatrix
        ro, co Spalten und Zeilenoffset
        s Skalarwert
Output: q mit addiertem Block s*qh
Side-Effects:
Description:
Die Funktion addiert die Untermatrix qh multipliziert mit dem Skalar s zu Matrix q. 
Die Position wird durch den Zeilenoffset ro und Spaltenoffset co gegeben.
VorsichtDiagonalelemente muessen einsortiert sein.
***********************************************************************/
#ifndef CC_COMP
extern void mat_add_submatrix(Sparse_matrix *q, Sparse_matrix *qh, int ro, int co, double s) ;
#else
extern void mat_add_submatrix() ;
#endif

/**********************
mat_sparse_matrix_tensor_product
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  q1, q2 Sparse Matrix
Output: Kroneckerprodukt von q1 und q2
Side-Effects:
Description:
Die Funktion berechnet das Kroneckerprodukt zweier Matrizen.
Die Diagonalelemente muessen in die Zeilen einsortiert sein!
***********************************************************************/
extern Sparse_matrix *mat_sparse_matrix_tensor_product (Sparse_matrix *q1, Sparse_matrix *q2, int q2col) ;

/**********************
mat_sparse_matrix_add_elem
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
        ri, ci Zeilen- und Spaltenindex
        val Wert
        diag bei TRUE werden die Diagonalelement in diagonals gespeichert, sonst in der Zeile
Output: Matrix mit Q(ri, ci) += val 
Side-Effects:
Description:
Die Funktion addiert val zum Element ri,ci der Matrix. Falls das Element nicht vorhanden ist, wir es neu erzeugt.
***********************************************************************/
extern void mat_sparse_matrix_add_elem (Sparse_matrix *q, int ri, int ci, double val, short diag) ;

/**********************
mat_sparse_matrix_add_identity
Datum:  21.11.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix


Output: 
Side-Effects:
Description:
Die Funktion ersetzt die letzte Spalte von q durch den Einheitsvektor.
***********************************************************************/
extern void mat_sparse_matrix_add_identity(Sparse_matrix *q) ;

/**********************
mat_sparse_matrix_identical
Datum:  21.11.10
Autor:  Peter Buchholz
Input:  q1, q2 Sparse Matrix
        threshold maximaler Unterschied in den Elementen

Output: Rueckgabewert TRUE or FALSE
Side-Effects:
Description:
Die Funktion untersucht, ob zwei Matrizen idnetisch sind..
***********************************************************************/
extern int mat_sparse_matrix_identical(Sparse_matrix *q1, Sparse_matrix *q2, double threshold) ;


/**********************
mat_sparse_matrix_is_smaller
Datum:  1.3.15
Autor:  Peter Buchholz
Input:  q1, q2 Sparse Matrix
        threshold maximaler Unterschied in den Elementen

Output: Rueckgabewert TRUE or FALSE
Side-Effects:
Description:
Die Funktion untersucht, ob q1 - threshold <= q2 ist.
***********************************************************************/
extern int mat_sparse_matrix_is_smaller(Sparse_matrix *q1, Sparse_matrix *q2, double threshold, short with_diag) ;

extern int mat_sparse_matrix_diagmax_diagmin_bandwidth(Sparse_matrix *q_nm, 
                                                double *diagmax, 
                                                double *diagmin, 
						       unsigned *bandwidth) ;

/**
 * fuehrt Reindizierung und Verkleinerung durch.
 * dient der Verkleinerung von Matrizen bzgl erreichbarer Teilraueme
 * Achtung: relation muss ordnungsvertraeglich sein, Bildmenge muss mit kleinerer
 * Indexmenge auskommen.
 * \date 6.3.00
 * \author Peter Kemper
 * \param q zu modifizierende Matrix
 * \param relation Vektor der Laenge der Anzahl Matrixzeilen
 * \param new_dim Anzahl Zeilen der gekuerzten Matrix
 * \return ???
 */
extern int mat_sparse_matrix_update_index(Sparse_matrix *q, int *relation, int new_dim) ;

/**
 * \date 7.4.99
 * \author Peter Kemper
 * \param q Sparse_matrix
 * \return  Space used for complete data structurs
 */
extern int mat_sparse_matrix_space(Sparse_matrix *q) ;
#endif

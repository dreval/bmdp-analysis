/*********************
        Datei:  str_proconst.h
        Datum:  02.07.97
        Autor:  Peter Buchholz
        Inhalt: Konstanten fuer Projektionstechniken

Side-effects: 

**********************/

/* Local Constants */
/* Subspace dimension for GMRES */
#define SUBSPACE_DIM  20


/* Subspace dimension for DQGMRES */
#define MAX_DIM       30
/* Used dimension for DQGMRES */
#define USED_DIM      15 


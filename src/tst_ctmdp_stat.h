/*********************
        Datei:  tst_ctmdp_stat.h
        Datum:  17.6.11
        Autor:  Peter Buchholz
        Inhalt: Externe Deklarationen fuer mdp_ctmdp_statt.c
        Aenderungen:

**********************/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "basic_io.h"
#include "global.h"
#include "str_global.h"
#include "ememory.h"
#include "mat_time.h"
#include "mat_int_vektor.h"
#include "mat_row.h"
#include "mat_sparse_matrix.h"
#include "simple_io.h"
#include "ctmdp_func.h"
#include "solution.h"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_linalg.h>

#ifndef CTMDP_STAT
#define CTMDP_STAT TRUE

#define MDP_TRC_ITER     FALSE
#define MDP_PRT_STRATEGY FALSE

#define MDP_ILUTH_TH 0.1

#define MDP_SIZE_DIRECT 5000
#define MDP_MAX_ITER    500
#define MDP_LO_EPS      1.0e-8
#define MDP_MIN_VAL     1.0e-12
#endif

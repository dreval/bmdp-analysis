#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H
#include <stddef.h>

/**
 * A small library to deal with polynomials in vector representation. Supported operations are summation and product, which should be sufficient.
 * 
 * Shame on GSL for not providing me with the relevant functions :-/
 */

/**
 * @brief A data structure for polynomials
 * Nothing fancy, just an array of coefficients.
 */
typedef struct polynomial_t {
    size_t ord;
    double *coeffs;
} polynomial;

/**
 * @brief polynomial_add Adds two polynomials and stores the result in the first argument.
 * @param a The first operand where the result is stored
 * @param b The second operand
 */
void polynomial_add(polynomial *a, polynomial *b);

/**
 * @brief polynomial_multiply Computes the product of two polynomials and stores the result in the first argument.
 * @param a The first operand where the result is stored
 * @param b The second operand
 */
void polynomial_multiply(polynomial *a, polynomial *b);

/**
 * @brief polynomial_multiply_scalar Computes the product of a polynomial and a scalar and stores the result in the first argument.
 * @param a The first operand where the result is stored
 * @param b The scalar
 */
void polynomial_multiply_scalar(polynomial *a, double b);

/**
 * @brief Creates a polynomial of order zero, p = 1
 * @return The polynomial structure
 */
extern polynomial make_polynomial();

void polynomial_compress(polynomial *p);

#endif /* POLYNOMIAL_H */

/*********************
        Datei:  bmdp_func.h
        Datum:  25.3.15
        Autor:  Dimitri Scheftelowitsch
        Inhalt: Externe Deklarationen fuer bmdp_weight_func.c
        Aenderungen:

**********************/
#ifndef BMDP_WEIGHT_FUNC_H
#define BMDP_WEIGHT_FUNC_H

#include <stddef.h>
#include "tst_ctmdp_stat.h"
#include "bmdp_pareto_func.h"
#include "bmdp_const.h"
#include "bloom.h"
#include <glpk.h>

#define X_MIN_COL_NAME_PATTERN "x_min[%zu][%zu]"
#define X_AVG_COL_NAME_PATTERN "x_avg[%zu][%zu]"
#define X_MAX_COL_NAME_PATTERN "x_max[%zu][%zu]"
#define X_COL_NAME_PATTERN "x_%zu_%zu"
#define D_COL_NAME_PATTERN "d[%zu][%zu]"
#define D_CONSTRAINT_PATTERN "d_%zu constraint row"
#define MU_MIN_COL_NAME_PATTERN "mu_min[%zu]"
#define MU_AVG_COL_NAME_PATTERN "mu_avg[%zu]"
#define MU_MAX_COL_NAME_PATTERN "mu_max[%zu]"
#define MU_COL_NAME_PATTERN "mu_%zu"
#define MU_MIN_CONSTRAINT_ROW "mu_min constraint row"
#define MU_AVG_CONSTRAINT_ROW "mu_avg constraint row"
#define MU_MAX_CONSTRAINT_ROW "mu_max constraint row"
#define X_MIN_CONSTRAINT_ROW "x_min constraints, row %zu"
#define X_AVG_CONSTRAINT_ROW "x_avg constraints, row %zu"
#define X_MAX_CONSTRAINT_ROW "x_max constraints, row %zu"

#define MIN_IDX 0
#define AVG_IDX 1
#define MAX_IDX 2

#define WEIGHT_LP_ITERATION
#ifndef WEIGHT_LP_ITERATION
#define WEIGHT_POLICY_ITERATION
#endif /* WEIGHT_LP_ITERATION */

typedef struct {
        double *gain_min;
        double *gain_avg;
        double *gain_max;
} gain_vectors_t;

typedef struct weight_triple_t {
    double weight_min;
    double weight_avg;
    double weight_max;
    double priority;
} weight_triple;

/**********************
bmdp_compute_weight_case
Datum:  25.03.15
Autor:  Dimitri Scheftelowitsch
Input:  mdp_min, mdp_max Bounding MDP matrices
        rew reward vector
        ind politics
        max_iter, local maximal number of global/local iterations
        ti_max, local_it maximal local/global solution time
        epsilon, local_eps epsilon global/local
        method solution method if size > BMDP_SIZE_DIRECT
        gamma discount factor
        min_weight weight of the worst case
        max_weight weight of the best case
Output: return value gain vector
        ind optimal policy
Side-Effects:
Description:
The function computes a weighted worst-best-average case gain vector and policy for a BMDP.
***********************************************************************/
extern gain_vectors_t bmdp_compute_weight_case(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                               Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                               size_t *ind, int max_iter, double ti_max, double epsilon, int local_it,
                                               double local_eps, int method, double gamma, double min_weight,
                                               double max_weight, short comp_max, short comp_stat);

extern void eval_weight_in_place(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                 Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                 int max_iter, double ti_max, double epsilon, int method, double gamma,
                                 Pareto_vector_element *pair, short comp_max);

extern Pareto_vector_element *pareto_element_new_with_weights(size_t ord, double min_weight, double max_weight);

double bmdp_weight_value_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                   size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                   double *hh_min, double *hh_avg, double *hh_max, double *vv_min, double *vv_max, double gamma,
                                   double weight_min, double weight_max, short comp_max);

double **bmdp_lp_max_w_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                 size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                 double *hh_min, double *hh_max, double *vv_min, double *vv_max,
                                 double gamma, double weight_min, double weight_max, short comp_max);

double compute_optimal_weight_policy(size_t num_of_mdps, Mdp_matrix *mdp, Row ****rows, double ***diags, Double_vektor ***rew,
                                   double *weight, size_t *num_actions, int **decision_variable_index, double **hh,
                                   double gamma, double **policy, short pure, short comp_max);

double **bmdp_lp_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                         size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                         double *hh_min, double *hh_max, double *vv_min, double *vv_max,
                         double gamma, double weight_min, double weight_max, short comp_max);

double bmdp_lp_iteration_case1a(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                double **policy, double **hh, Double_vektor ***rew, double gamma,
                                double *weight, short comp_max, int **x, size_t *num_actions,
                                Row ****rows, double ***diags);

double bmdp_lp_iteration_case1b(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                double **policy, double **hh,
                                Double_vektor ***rew, double gamma, double *weight,
                                short comp_max, size_t case_index, int **x, size_t *num_actions,
                                Row ****rows, double ***diags);

double bmdp_lp_iteration_case2(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                               size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                               double gamma, double* weight, short comp_max);

double *bmdp_evaluate_minmax_stationary_policy(Mdp_matrix *mdp_min, Mdp_matrix *mdp_max,
                                               Double_vektor **rew, double gamma, double **policy,
                                               short compmin);

void compute_extreme_rows_and_diags(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                    Row ****rows, double ***diags, double **hh, double *vv_min, double *vv_max, double **policy,
                                    double gamma, Double_vektor ***rew);

int add_mdp_constraints(glp_prob *lp, Row ***rows, double **diags, double gamma, Mdp_matrix *mdp, int dimension, int **x, double *initial_distribution);

void update_rows_and_diags(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                           Row ****rows, double ***diags, double **hh, double *vv_min, double *vv_max);

int init_lp(size_t ord, size_t *num_actions, double gamma, glp_prob *lp, char *buf, int **x, short pure);

#if 0
double bmdp_weight_value_iteration_alt(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
        size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
        double *hh_min, double *hh_avg, double *hh_max, double *vv_min, double *vv_max, double gamma,
        double weight_min, double weight_max, short comp_max);
#endif

double bmdp_weight_policy_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                    size_t *ind, double **policy, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                    bloom_filter bloom_set, double gamma, double weight_min, double weight_max, short comp_max);

extern short bmdp_find_and_set_new_weight_strategy(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                                   Sparse_matrix *pp_min, Sparse_matrix *pp_avg, Sparse_matrix *pp_max,
                                                   size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg,
                                                   Double_vektor **rew_max,
                                                   Double_vektor *b_min, Double_vektor *b_avg, Double_vektor *b_max,
                                                   double *hh_min, double *hh_avg, double *hh_max,
                                                   double *vv_min, double *vv_max, double gamma,
                                                   double weight_min, double weight_max, short comp_max);

double bmdp_alternative_goal_value_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                             Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                             double *vv_min, double *vv_max, double **new_hh,
                                             size_t *ind, double *hh, double gamma,
                                             double weight_min, double weight_max, short comp_max);

extern List **bmdp_weight_opt3d(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                int max_iter, double ti_max, double epsilon, int method, double gamma, short comp_max, enum weight_dimensions dim);

extern List **bmdp_weight_opt3d_alt(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                    Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                    int max_iter, double ti_max, double epsilon, int method, double gamma, short comp_max, enum weight_dimensions dim);

/*
 * bmdp_weight_error_bound
 * Datum: 16.02.2016
 * Autor: Dimitri Scheftelowitsch
 * Input: last_goal_value last value of the goal function
 *        current_goal_value current value of the goal_function
 *        gamma discount factor
 * Output: error bound
 * Description: This function computes the error bound according to Theorem 3 in the main paper
 */
extern double bmdp_weight_error_bound(double last_goal_value, double current_goal_value, double gamma);

#endif /* BMDP_WEIGHT_FUNC_H */

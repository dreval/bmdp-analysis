#ifndef BMDP_PARETO_EVO_H
#define BMDP_PARETO_EVO_H

#include "bmdp_func.h"
#include "bmdp_pareto_func.h"

/* An evolutionary algorithm as a benchmark */
extern Pareto_vector_element **bmdp_pure_opt_evo(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                                 Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                                 double gamma, short comp_max, size_t *no_policies);

#endif /* BMDP_PARETO_EVO_H */

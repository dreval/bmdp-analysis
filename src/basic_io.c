/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c (C) Copyright 1998 by Peter Buchholz
c     All Rights Reserved
c
c basic_io
c
c Baisis IO Funktionen
c 
c
c       Datum           Modifikationen
c       ----------------------------------------------------------
c       03.02.98 Datei aus Teilen von simple_io.c/sim_io.c erzeugt
c       11.08.98 Funktion read_comment eingefuegt
c       11.08.98 Kommentare werden mit der Funktion read_comment ueberlesen
c       11.08.98 Funktionen zur Ausgabe von Resultatwerten eingefuegt
c       16.01.06 readc_int und readc_double eingef�gt
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ememory.h"
#include "basic_io.h"
#ifndef NO_ERROR
#define NO_ERROR 0 
#endif

/**********************
bg_copy_file
Datum:  29.11.02
Autor:  Peter Buchholz
Input:  fp_in Filedeskriptor zum Lesen ge�ffnet
        fp_out Filedeskriptor zum Schreiben ge�ffnet
Output: Kopie der Datei in fp_out
Side-Effects:
Description:
Die Funktion kopiert die Datei fp_in zeilenweise nach fp_out
***********************************************************************/
#ifndef CC_COMP
int bg_copy_file(FILE *fp_in, FILE *fp_out) 
#else
int bg_copy_file(fp_in, fp_out)
  FILE *fp_in  ;
  FILE *fp_out ;
#endif
{
 char  cp  ;

 while (fscanf(fp_in, "%c", &cp) != EOF) {
   fprintf(fp_out, "%c", cp) ;
 }
 return (NO_ERROR) ;
} /* bo_copy_file */

/********************
read_comment	##s Einlesen von Kommentaren (werden uebersprungen)
Change:	17.3.98
Input:	
Output:	TRUE	ok
	FALSE	Fehler
Side:	
Description: Verwendung: fp = read_comment(fp) 
             fp muss beim Aufruf geoeffnet sein.
********************/
FILE *read_comment(FILE *fp)
{    
  char c ;
  int r;

  while ((r = fgetc(fp)) != EOF)
    {
      c = (char) r;
      if (c == '\n' || c == '\t' || c == ' ')
	continue ; 
      if (c == '#')
	{
	  /* Kommentarzeilen ignorieren */
	  char dummybuf[4096];
	  fgets(dummybuf, 4096, fp);
	  /* printf("Skip comment: %s \n", dummybuf) ;*/
	}
      else
	{
	  ungetc(c,fp) ;
	  return(fp) ;
	}
    }
  return(fp) ;
}

/**********************
input_error
Datum:  9.6.95
Autor:  Peter Buchholz
Input:  
Output: F
Side-Effects:
Description:
Die Funktion druckt eine Fehlermeldung und bricht das Programmab.. 
***********************************************************************/
void input_error ()  
{
  printf("Error reading input file\n") ;
  printf("stop execution!!!\n\n") ;
  exit(1) ;
}

/**********************
readc_double
Datum:  16.01.06
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen double Wert.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein double in der Zeile
gefunden wird, so wird der Wert DOUBLE_STOP zur�ckgegeben. 
Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
double readc_double (FILE *fp) 
#else
double readc_double (fp)
  FILE *fp ;
#endif
{
  double d     ;

  fp =read_comment(fp) ;
  if (fscanf(fp, "%le", &d) == EOF)
    return(DOUBLE_STOP) ;
  return(d) ;
} /* readc_double */


/**********************
read_integer
Datum:  16.01.06
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen integer Wert.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein integer in der Zeile
gefunden wird, so wird der Wert INT_STOP zur�ckgegeben. 
Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
int readc_integer (FILE *fp)
#else
int readc_integer (fp)
  FILE *fp ;
#endif
{
  int  i     ;

  fp =read_comment(fp) ;
  if (fscanf(fp, "%i", &i) == EOF)
    return(INT_STOP) ;
  return(i) ;
} /* read_cint */

/**********************
read_double
Datum:  9.6.95
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen double Wert.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein double in der Zeile
gefunden wird, so wird das Programm abgebrochen. 
Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
double read_double (FILE *fp) 
#else
double read_double (fp)
  FILE *fp ;
#endif
{
  double d     ;

  fp =read_comment(fp) ;
  if (fscanf(fp, "%le", &d) == EOF)
    input_error() ;
  return(d) ;
}


/**********************
read_integer
Datum:  9.6.95
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen integer Wert.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein integer in der Zeile
gefunden wird, so wird das Programm abgebrochen.   
Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
int read_integer (FILE *fp)
#else
int read_integer (fp)
  FILE *fp ;
#endif
{
  int  i     ;

  fp =read_comment(fp) ;
  if (fscanf(fp, "%i", &i) == EOF)
    input_error() ;
  return(i) ;
}

/**********************
read_string
Datum:  19.05.11
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen String.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein String in der Zeile
gefunden wird, so wird das Programm abgebrochen.   
Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
char *read_string (FILE *fp)
#else
char *read_string (fp)
  FILE *fp ;
#endif
{
  char  buf[1024] ; /* temporary buffer to read strings from the input trace, WARNING: buffer overflow possible */
  char *s ;

  fp =read_comment(fp) ;
  if (fscanf(fp, "%s", buf) == EOF)
    input_error() ;
  s = (char *)ecalloc(strlen(buf)+1,sizeof(char)) ;
  s = strcpy(s,buf) ;
  return(s) ;
}


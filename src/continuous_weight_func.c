#include "continuous_weight_func.h"
#include "bmdp_func.h"
#include "utils.h"
#include "concurrent_weight_func.h"
#include <cblas.h>
#include <time.h>
#include <math.h>

void continuous_weight_optimize(continuous_optimization_problem problem,
                                continuous_optimization_options options,
                                continuous_optimization_solution *solution)
{
    const size_t ord = problem.ord;
    const size_t actions = problem.mdp_mean->rows[0]->pord;
    const double gamma = problem.gamma;

    int done = FALSE;
    size_t problems = 1;

    Mdp_matrix **scenario_matrices = calloc(problems, sizeof(Mdp_matrix *));
    scenario_matrices[0] = mdp_copy_matrix(problem.mdp_mean);
    Double_vektor ***scenario_rewards = calloc(problems, sizeof(Double_vektor **));
    scenario_rewards[0] = problem.rewards;

    concurrent_mdp cmdp;
    cmdp.iniv = problem.initial_vector;
    cmdp_problem cmdp_problem;
    cmdp_problem.comp_max = 1;
    cmdp_problem.gamma = problem.gamma;
    cmdp.scenarios = 1;
    cmdp.matrices = scenario_matrices;
    cmdp.rewards = scenario_rewards;
    cmdp_problem.weights.scenarios = 1;
    cmdp_problem.mdp = cmdp;

    cmdp_context cmdp_ctx;
    cmdp_ctx.problem = &cmdp_problem;
    cmdp_ctx.policy = NULL;
    cmdp_ctx.presolved = FALSE;
    switch (options.inner_mode) {
    case GLOBAL_MIXED:
        cmdp_ctx.method = CMDP_METHOD_NLP;
        break;
    case GLOBAL_PURE:
        cmdp_ctx.method = CMDP_METHOD_MIP;
        break;
    case LOCAL_MIXED:
        cmdp_ctx.method = CMDP_METHOD_IMGP;
        break;
    case LOCAL_PURE:
        cmdp_ctx.method = CMDP_METHOD_IMPP;
        break;
    }

    Mdp_matrix **bmdp_models_lower_matrices = calloc(1, sizeof(Mdp_matrix *));
    Mdp_matrix **bmdp_models_upper_matrices = calloc(1, sizeof(Mdp_matrix *));
    bmdp_models_lower_matrices[0] = mdp_copy_matrix(problem.mdp_lower);
    bmdp_models_upper_matrices[0] = mdp_copy_matrix(problem.mdp_upper);
    double *models_weights = calloc(1, sizeof(double));
    models_weights[0] = 1;
    size_t *policy = calloc(problem.ord, sizeof(size_t));
    cmdp_problem.weights.weights = models_weights;
    double *helper = calloc(ord, sizeof(double));
    size_t *temp_lower_policy = calloc(ord, sizeof(size_t));
    size_t *temp_upper_policy = calloc(ord, sizeof(size_t));

    srand(options.random_seed);

    double rmax = 0.0;
    for (size_t s = 0; s < ord; ++s) {
        rmax = fmax(rmax, problem.rewards[0]->vektor[s]);
    }

    while (!done) {
        // solve the CMDP problem
        stdlog(LOG_INFO, "cwo", "Iteration %d: Computing compromise policy", problems);
        struct timespec t0;
        if (problems > 1) {
            cmdp_ctx.presolved = TRUE;
        }
        timespec_get(&t0, TIME_UTC);
        value_vectors subvectors = cmdp_optimize(&cmdp_ctx);
        struct timespec t1;
        timespec_get(&t1, TIME_UTC);
        double dt = (t1.tv_sec - t0.tv_sec) * 1000 + (t1.tv_nsec - t0.tv_nsec) * 1e-6;
        stdlog(LOG_INFO, "cwo", "%d scenarios, %.5f msec", problems, dt);
        // extract deterministic policy
        for (size_t s = 0; s < ord; ++s) {
            policy[s] = 0;
            for (size_t a = 0; a < actions; ++a) {
                if (cmdp_ctx.policy[s][a] > 0.0) {
                    policy[s] = a;
                }
            }
        }
        stdlog(LOG_INFO, "cwo", "Estimate: %f", cmdp_ctx.result);

        // compute error bounds
        size_t problem_to_split = 0;
        size_t split_state = 0;
        size_t split_action = 0;
        int split_param = -1;
        double maxdelta = 0.0;
        double total_error = 0.0;

        for (size_t k = 0; k < problems; ++k) {
            // compute \eta_{N,i}
            double eta_Ni = 1.0;
            size_t local_action = 0;
            size_t local_state = 0;
            int local_parameter = -1;

            for (size_t a = 0; a < actions; ++a) {
                for (size_t s = 0; s < ord; ++s) {
                    Mdp_row *lower_row = bmdp_models_lower_matrices[k]->rows[s];
                    Mdp_row *upper_row = bmdp_models_upper_matrices[k]->rows[s];

                    if (upper_row->diagonals[a] > 0.0) {
                        double eta = lower_row->diagonals[a] / upper_row->diagonals[a];
                        if (eta < eta_Ni) {
                            //stdlog(LOG_TRACE, "cwo:error", "new eta: %f, old eta: %f", eta, eta_Ni);
                            local_state = s;
                            local_action = a;
                            local_parameter = -1;
                            eta_Ni = eta;
                        }
                    }

                    for (size_t i = 0; i < lower_row->no_of_entries[a]; ++i) {
                        // assume the order does not change
                        assert(lower_row->colind[a][i] == upper_row->colind[a][i]);
                        if (upper_row->values[a][i] > 0.0) {
                            double eta = lower_row->values[a][i] / upper_row->values[a][i];
                            if (eta < eta_Ni) {
                                //stdlog(LOG_TRACE, "cwo:error", "new eta: %f, old eta: %f", eta, eta_Ni);
                                local_state = s;
                                local_action = a;
                                local_parameter = (int) i;
                                eta_Ni = eta;
                            }
                        }
                    }
                }
            }

            double relative_error = gamma * (1 - eta_Ni) / ((1 - gamma) * (1 - gamma * eta_Ni));
            double delta = relative_error * rmax * models_weights[k];
            total_error += delta;

//            stdlog(LOG_TRACE, "cwo", "error bound for subproblem %3d: %f (relative: %f, eta: %f), weight: %f",
//                   k, relative_error * rmax, relative_error, eta_Ni, models_weights[k]);

            if (delta > maxdelta) {
                problem_to_split = k;
                maxdelta = delta;
                split_state = local_state;
                split_action = local_action;
                split_param = local_parameter;
            }
        }

        printf("cwo:Error at iteration %4zu: %f\n", problems, total_error);

        // split the problem into subproblems
        //stdlog(LOG_DEBUG, "cwo", "Would split problem %zu in state %zu, action %zu",
        //       problem_to_split, split_state, split_action);
        // re-calculate upper bounds to make them somewhat realistic…
        Mdp_row* upper_row = bmdp_models_upper_matrices[problem_to_split]->rows[split_state];
        Mdp_row* lower_row = bmdp_models_lower_matrices[problem_to_split]->rows[split_state];
        fix_row_bounds(lower_row, upper_row, split_action);

        ++problems;
        /* first, reallocate all needed data structures… */
        models_weights = realloc(models_weights, problems * sizeof(double));
        cmdp_problem.weights.weights = models_weights;
        Mdp_matrix **ptr = realloc(bmdp_models_lower_matrices, problems * sizeof(Mdp_matrix *));
        check_realloc_ptr(ptr);
        bmdp_models_lower_matrices = ptr;

        ptr = realloc(bmdp_models_upper_matrices, problems * sizeof(Mdp_matrix *));
        check_realloc_ptr(ptr);
        bmdp_models_upper_matrices = ptr;

        scenario_matrices = realloc(scenario_matrices, problems * sizeof(Mdp_matrix *));
        scenario_rewards = realloc(scenario_rewards, problems * sizeof(Double_vektor **));
        /* now, compute the new scenario */
        /* first, copy matrices */
        scenario_matrices[problems - 1] = mdp_copy_matrix(scenario_matrices[problem_to_split]);
        bmdp_models_lower_matrices[problems - 1] =
            mdp_copy_matrix(bmdp_models_lower_matrices[problem_to_split]);
        bmdp_models_upper_matrices[problems - 1] =
            mdp_copy_matrix(bmdp_models_upper_matrices[problem_to_split]);
        /* then create new bounds */
        double *subvalue = subvectors.gains[problem_to_split];

//        stdlog(LOG_TRACE, "cwo:split", "splitting parameter %d", split_param);

        double old_problem_weight = models_weights[problem_to_split];

        if (split_param == -1) {
            double local_upper = bmdp_models_upper_matrices[problem_to_split]
                                     ->rows[split_state]
                                     ->diagonals[split_action];
            double local_lower = bmdp_models_lower_matrices[problem_to_split]
                                     ->rows[split_state]
                                     ->diagonals[split_action];
            double split_value = (local_upper + local_lower) / 2;
            bmdp_models_upper_matrices[problem_to_split]
                ->rows[split_state]
                ->diagonals[split_action] = split_value;
            bmdp_models_lower_matrices[problems - 1]->rows[split_state]->diagonals[split_action] =
                split_value;
//            stdlog(LOG_TRACE, "cwo:split:diag", "total lower bound: %f",
//                   problem.mdp_lower->rows[split_state]->diagonals[split_action]);
//            stdlog(LOG_TRACE, "cwo:split:diag", "total upper bound: %f",
//                   problem.mdp_upper->rows[split_state]->diagonals[split_action]);
//            stdlog(LOG_TRACE, "cwo:split:diag", "split value: %f", split_value);
        } else {
            double local_lower = bmdp_models_lower_matrices[problem_to_split]
                                     ->rows[split_state]
                                     ->values[split_action][split_param];
            double local_upper = bmdp_models_upper_matrices[problem_to_split]
                                     ->rows[split_state]
                                     ->values[split_action][split_param];
            double split_value = (local_upper + local_lower) / 2;
            bmdp_models_upper_matrices[problem_to_split]
                ->rows[split_state]
                ->values[split_action][split_param] = split_value;
            bmdp_models_lower_matrices[problems - 1]
                ->rows[split_state]
                ->values[split_action][split_param] = split_value;
//            stdlog(LOG_TRACE, "cwo:split:nondiag", "total lower bound: %f",
//                   problem.mdp_lower->rows[split_state]->values[split_action][split_param]);
//            stdlog(LOG_TRACE, "cwo:split:nondiag", "total upper bound: %f",
//                   problem.mdp_upper->rows[split_state]->values[split_action][split_param]);
//            stdlog(LOG_TRACE, "cwo:split:nondiag", "split value: %f", split_value);
        }

        for (size_t s = 0; s < ord; ++s) {
            Mdp_row *old_row = scenario_matrices[problem_to_split]->rows[s];
            Mdp_row *new_row = scenario_matrices[problems - 1]->rows[s];

            for (size_t a = 0; a < old_row->pord; ++a) {
                memset(helper, 0, ord * sizeof(double));
                set_minmax_row(bmdp_models_lower_matrices[problem_to_split]->rows[s],
                               bmdp_models_upper_matrices[problem_to_split]->rows[s], old_row, s, a,
                               subvalue, helper, TRUE);
                double mass = old_row->diagonals[a];
                for (size_t i = 0; i < old_row->no_of_entries[a]; ++i) {
                    mass += old_row->values[a][i];
                }
                if (mass - 1.0 > 1e-7) {
                    stdlog(LOG_FATAL, "cwo:splitting", "minimal mass is %f, greater than 1", mass);
                }
                assert(mass <= 1.0 + 1e-7);
                memset(helper, 0, ord * sizeof(double));
                set_minmax_row(bmdp_models_lower_matrices[problems - 1]->rows[s],
                               bmdp_models_upper_matrices[problems - 1]->rows[s], new_row, s, a,
                               subvalue, helper, FALSE);
                mass = new_row->diagonals[a];
                for (size_t i = 0; i < new_row->no_of_entries[a]; ++i) {
                    mass += new_row->values[a][i];
                }
                if (mass - 1.0 > 1e-7) {
                    stdlog(LOG_FATAL, "cwo:splitting", "minimal mass is %f, greater than 1", mass);
                }
                assert(mass <= 1.0 + 1e-7);
            }
        }

        double old_weight = compute_subproblem_mass(bmdp_models_lower_matrices[problem_to_split]->rows[split_state],
                                                    bmdp_models_upper_matrices[problem_to_split]->rows[split_state],
                                                    split_action, split_param);
        double new_weight = compute_subproblem_mass(bmdp_models_lower_matrices[problems - 1]->rows[split_state],
                                                    bmdp_models_upper_matrices[problems - 1]->rows[split_state],
                                                    split_action, split_param);
        double weight_sum = old_weight + new_weight;
        old_weight /= weight_sum;
        new_weight /= weight_sum;
//        stdlog(LOG_TRACE, "cwo:split:weights",
//               "new mass: %f, old mass: %f, total mass: %f, weight: %f", new_weight,
//               old_weight, weight_sum, old_problem_weight);
        models_weights[problems - 1] = old_problem_weight * new_weight;
        models_weights[problem_to_split] = old_problem_weight * old_weight;
        cmdp_problem.weights.weights = models_weights;
        cmdp_problem.weights.scenarios = problems;

        scenario_rewards[problems - 1] = scenario_rewards[problem_to_split];
        cmdp.matrices = scenario_matrices;
        cmdp.rewards = scenario_rewards;
        cmdp.scenarios = problems;
        cmdp_problem.mdp = cmdp;

        for (size_t k = 0; k < problems - 1; ++k) {
            free(subvectors.gains[k]);
        }
        free(subvectors.gains);

        solution->value = cmdp_ctx.result;
        solution->error = total_error;

        done = (total_error < options.precision)
            || (problems > options.max_subproblems); // delta < options.precision;
    }

    solution->policy = policy;

    for (size_t k = 0; k < problems; ++k) {
        mdp_matrix_free(scenario_matrices[k]);
        mdp_matrix_free(bmdp_models_lower_matrices[k]);
        mdp_matrix_free(bmdp_models_upper_matrices[k]);
    }

    for (size_t s = 0; s < ord; ++s) {
        free(cmdp_ctx.policy[s]);
    }
    free(cmdp_ctx.policy);

    free(helper);
    free(scenario_matrices);
    free(scenario_rewards);
    free(models_weights);
    free(bmdp_models_lower_matrices);
    free(bmdp_models_upper_matrices);
    free(temp_upper_policy);
    free(temp_lower_policy);

    return;
}

Mdp_matrix *mdp_copy_matrix(Mdp_matrix *src)
{
    Mdp_matrix *dest = mdp_matrix_new(src->ord);
    for (size_t s = 0; s < dest->ord; ++s) {
        // copy rows...
        const size_t actions = src->rows[s]->pord;
        dest->rows[s] = mdp_row_new(actions);
        for (size_t a = 0; a < actions; ++a) {
            dest->rows[s]->diagonals[a] = src->rows[s]->diagonals[a];
            dest->rows[s]->no_of_entries[a] = src->rows[s]->no_of_entries[a];
            const size_t entries = dest->rows[s]->no_of_entries[a];
            dest->rows[s]->colind[a] = calloc(entries, sizeof(size_t));
            dest->rows[s]->values[a] = calloc(entries, sizeof(double));
            for (size_t i = 0; i < entries; ++i) {
                dest->rows[s]->colind[a][i] = src->rows[s]->colind[a][i];
                dest->rows[s]->values[a][i] = src->rows[s]->values[a][i];
            }
        }
    }
    return dest;
}

void set_minmax_row(const Mdp_row *restrict lower, const Mdp_row *restrict upper, Mdp_row *row,
                    size_t state, size_t action, const double *restrict values, double *helper,
                    short lower_bound)
{
    Row sparse_row;
    sparse_row.colind = row->colind[action];
    sparse_row.no_of_entries = row->no_of_entries[action];
    sparse_row.val = row->values[action];
    bmdp_set_one_minmax_row(lower, upper, &sparse_row, &(row->diagonals[action]), state, action,
                            values, helper, lower_bound);
    return;
}

void fix_row_bounds(Mdp_row *lower_row, Mdp_row *upper_row, size_t split_action) {
//    stdlog(LOG_TRACE, "frb", "Fixing row bounds, action %zu…", split_action);
    short changed = FALSE;
    do {
        changed = FALSE;
        // Fix upper bounds
        double sum_lower_bound = lower_row->diagonals[split_action];
        double sum_upper_bound = upper_row->diagonals[split_action];
        for (size_t i = 0; i < lower_row->no_of_entries[split_action]; ++i) {
            sum_lower_bound += lower_row->values[split_action][i];
            sum_upper_bound += upper_row->values[split_action][i];
        }
        double free_mass = sum_upper_bound - sum_lower_bound;
        double diag_upper_bound = 1.0 - (sum_lower_bound - lower_row->diagonals[split_action]);
        if (upper_row->diagonals[split_action] - diag_upper_bound > 1.0e-7) {
            changed = TRUE;
//            stdlog(LOG_TRACE, "frb", "changed upper bound in diagonal from %f to %f",
//                   upper_row->diagonals[split_action],
//                   diag_upper_bound);
            assert(diag_upper_bound > 0.0);
            double delta = upper_row->diagonals[split_action] - diag_upper_bound;
            free_mass -= delta;
            sum_upper_bound -= delta;
            upper_row->diagonals[split_action] = diag_upper_bound;
        }
        double nondiagonal_residual_mass = free_mass - (upper_row->diagonals[split_action] - lower_row->diagonals[split_action]);
        double diagonal_mass_deficit = 1.0 - (sum_lower_bound + nondiagonal_residual_mass);
        if (diagonal_mass_deficit > 1.0e-7) {
            changed = TRUE;
//            stdlog(LOG_TRACE, "frb", "changed lower bound in diagonal from %f to %f",
//                   lower_row->diagonals[split_action],
//                   lower_row->diagonals[split_action] + diagonal_mass_deficit);
            assert(lower_row->diagonals[split_action] + diagonal_mass_deficit - 1.0 <= 1e-7);
            lower_row->diagonals[split_action] += diagonal_mass_deficit;
            free_mass -= diagonal_mass_deficit;
            sum_lower_bound += diagonal_mass_deficit;
        }

        // Fix non-diagonal elements
        for (size_t i = 0; i < upper_row->no_of_entries[split_action]; ++i) {
            double upper_bound = 1.0 - (sum_lower_bound - lower_row->values[split_action][i]);
            if (upper_row->values[split_action][i] - upper_bound > 1.0e-7) {
                changed = TRUE;
//                stdlog(LOG_TRACE, "frb", "changed upper bound in nonzero parameter %zu from %f to %f",
//                       i,
//                       upper_row->values[split_action][i],
//                       diag_upper_bound);
                double delta = upper_row->values[split_action][i] - upper_bound;
                upper_row->values[split_action][i] = upper_bound;
                free_mass -= delta;
                sum_upper_bound -= delta;
                assert(upper_bound > 0.0);
            }
            double residual_mass = free_mass - (upper_row->values[split_action][i] - lower_row->values[split_action][i]);
            double mass_deficit = 1.0 - (sum_lower_bound + residual_mass);
            if (mass_deficit > 1.0e-7) {
                changed = TRUE;
                //stdlog(LOG_TRACE, "frb", "changed lower bound in nonzero parameter %zu from %f to %f",
                //       i,
                //       lower_row->values[split_action][i],
                //       lower_row->values[split_action][i] + mass_deficit);
                free_mass -= mass_deficit;
                lower_row->values[split_action][i] += mass_deficit;
                assert(lower_row->values[split_action][i] - 1.0 <= 1e-7);
                sum_lower_bound += mass_deficit;
            }
        }
    } while (changed);
    //stdlog(LOG_TRACE, "frb", "Fixed row bounds…");
}

double compute_subproblem_mass(Mdp_row* lower_row, Mdp_row* upper_row, size_t action, int parameter) {
    size_t samples = 256;
    const size_t nonzeros = lower_row->no_of_entries[action];
    samples = samples << nonzeros;
    //stdlog(LOG_TRACE, "csm", "free parameters: %zu, samples: %zu", nonzeros, samples);
    //trace_row_bounds(lower_row, upper_row, action, parameter);
    double computed_mass = 0.0;

    double lower_bound, upper_bound;
    if (parameter == -1) {
        lower_bound = lower_row->diagonals[action];
        upper_bound = upper_row->diagonals[action];
    } else {
        lower_bound = lower_row->values[action][parameter];
        upper_bound = upper_row->values[action][parameter];
    }
    const double mass = upper_bound - lower_bound;

    for (size_t i = 0; i < samples; ++i) {
        double probability = lower_row->diagonals[action];
        double delta = upper_row->diagonals[action] - lower_row->diagonals[action];
        if (parameter != -1) {
            probability += delta * rand() / (double) RAND_MAX;
        }

        for (size_t j = 0; j < nonzeros; ++j) {
            double local_lower_bound = lower_row->values[action][j];
            double local_upper_bound = upper_row->values[action][j];
            probability += local_lower_bound;

            double delta = local_upper_bound - local_lower_bound;
            double random_mass = delta * rand() / (double) RAND_MAX;
            if (parameter == -1 || (size_t) parameter != j) {
                probability += random_mass;
            }
        }

        if (probability <= 1.0 && 1.0 <= probability + mass) {
            computed_mass += 1.0;
        }
    }
    //stdlog(LOG_TRACE, "csm", "mass: %f", computed_mass);
    return computed_mass;
}

void trace_row_bounds(Mdp_row *lower_row, Mdp_row *upper_row, size_t action, int parameter) {
    stdlog(LOG_TRACE, "PRINTF_DEBUG", "BEGIN");
    //char *string = (char *) calloc(lower_row->no_of_entries[action] * 4, sizeof(char));
    stdlog(LOG_TRACE, "PRINTF_DEBUG", "  d [%.4f %.4f]", lower_row->diagonals[action], upper_row->diagonals[action]);
    for (size_t i = 0; i < lower_row->no_of_entries[action]; ++i) {
        stdlog(LOG_TRACE, "PRINTF_DEBUG", "%3zu [%.4f %.4f]", i, lower_row->values[action][i], upper_row->values[action][i]);
    }
    stdlog(LOG_TRACE, "PRINTF_DEBUG", "END");
}

void check_realloc_ptr(void *ptr) {
  if (ptr == NULL) {
    stdlog(LOG_FATAL, "cwo", "Out of memory!");
    exit(-ENOMEM);
  }
}

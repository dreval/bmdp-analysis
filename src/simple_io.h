/*********************
        Datei:  simple_io.h
        Datum:  14.11.96
        Autor:  Peter Buchholz
        Inhalt: 

Side-effects: 

********************************************************/

#ifndef SIMPLEIO_H
#define SIMPLEIO_H

#include "global.h"

/***** exportierte Funktionen *****/


/**********************
read_param
Datum:
Autor:  Peter Buchholz
Input:  fp Datei
        param Paramterdatenstruktur
Output: param Paramterdatenstruktur mit neu belegten Feldern
Side-Effects:
Description:
Die Funktion liest aus einer zum lesne geoeffneten Datei einen
Loesungsparamtersatz. Die Datenstruktur fuer den Parametersatz muss
uebergeben werden. Interne dynamische Arrays werden innerhalb der Funktion
allokiert und mit Werten besetzt. falls ein Lesefehler auftritt wird eine
Fehlermeldung ausgegeben und das Programm abgebrochen. Die Eingabedatei wir
din der Funktion nicht geschlossen.
**********************/
#ifndef CC_COMP
extern int read_param (FILE *fp, Solution_param *param) ;
#else
extern int read_param () ;
#endif

/**********************
write_param
Datum:
Autor:  Peter Buchholz
Input:  fp Datei
        param Paramterdatenstruktur
Output: 
Side-Effects:
Description:
Die Funktion schreibt in eine geoeffnete Datei einen
Loesungsparamtersatz. Die Ausgabedatei wird
in der Funktion nicht geschlossen.
**********************/
#ifndef CC_COMP
extern int write_param (FILE *fp, Solution_param *param) ;
#else
extern int write_param () ;
#endif

/**********************
str_set_ui
Datum:  26.11.96
Autor:  Peter Buchholz
Input:  partition Partitionsgrenzen der Subnetzmatrizen
        relation  Beschreibung der HLQPN Zustaende durch Subnetzzustaende
        ord Anzahl HLQPN Zustaende
        n_proc Anzahl Subnetze
Output: Funktionswert Wert von ui fuer alle HLQPN Zustaende und alel Subnetze
Side-Effects:
Description:
Die Prozedur generiert die Wert von ui (= Produkt der Submodellzustandsraum-
groessen von Subnetzen mit groesserem Index) fuer alle HLQPN Zustaende und
alle Subnetze.
***********************************************************************/
#ifndef CC_COMP
extern int *str_set_ui 
      (Int_vektor **partition, int *relation, int ord, int n_proc) ;
#else
extern int *str_set_ui () ;
#endif

/**********************
str_set_li
Datum:  27.1.97
Autor:  Peter Buchholz
Input:  partition Partitionsgrenzen der Subnetzmatrizen
        relation  Beschreibung der HLQPN Zustaende durch Subnetzzustaende
        ord Anzahl HLQPN Zustaende
        n_proc Anzahl Subnetze
Output: Funktionswert Wert von li fuer alle HLQPN Zustaende und alle Subnetze
Side-Effects:
Description:
Die Prozedur generiert die Wert von li (= Produkt der Submodellzustandsraum-
groessen von Subnetzen mit kleinerem Index) fuer alle HLQPN Zustaende und
alle Subnetze.
***********************************************************************/
#ifndef CC_COMP
extern int *str_set_li 
      (Int_vektor **partition, int *relation, int ord, int n_proc) ;
#else
extern int *str_set_li() ;
#endif

/**********************
str_set_ni
Datum:  10.2.97
Autor:  Peter Buchholz
Input:  partition Partitionsgrenzen der Subnetzmatrizen
        relation  Beschreibung der HLQPN Zustaende durch Subnetzzustaende
        ord Anzahl HLQPN Zustaende
        n_proc Anzahl Subnetze
Output: Funktionswert Zustandsraumgroesse fuer alle Subnetze
Side-Effects:
Description:
Die Prozedur generiert die Zustandsraumgroessen fuer alle HLQPN Zustaende und
alle Subnetze.
***********************************************************************/
#ifndef CC_COMP
extern int *str_set_ni 
      (Int_vektor **partition, int *relation, int ord, int n_proc) ;
#else
extern int *str_set_ni() ;
#endif

/**********************
str_set_si
Datum:  28.1.97
Autor:  Peter Buchholz
Input:  partition Partitionsgrenzen der Subnetzmatrizen
        relation  Beschreibung der HLQPN Zustaende durch Subnetzzustaende
        ord Anzahl HLQPN Zustaende
        n_proc Anzahl Subnetze
Output: Funktionswert Zustandszahlen fuer alle HLQPN Zustaende
Side-Effects:
Description:
Die Prozedur berechnet die Zustandszahlen fuer all Makrozustaende
***********************************************************************/
#ifndef CC_COMP
extern int *str_set_si 
      (Int_vektor **partition, int *relation, int ord, int n_proc) ;
#else
extern int *str_set_si() ;
#endif

/**********************
str_trans_ini_state
Datum:  19.11.96
Autor:  Peter Buchholz
Input:  ini Nummer des Initialzustandes
        partition Makrostruktur fuer das Subnetz
Output: Funktionswert transformierter Initialzustand
Side-Effects:
Description:
Die Funktion berechnet aus der obsoluten Nummer des Initialzustands die
Nummer relativ vom naechsten Blockanfang. 
***********************************************************************/
#ifndef CC_COMP
extern int str_trans_ini_state (int ini, Int_vektor *partition) ;
#else
extern int str_trans_ini_state () ;
#endif

#endif

/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c fuer die angegebenen Funktionen
c (C) Copyright 1995 by Peter Buchholz
c (C) Copyright 1993, 94 by Peter Kemper
c
c     All Rights Reserved
c
c mat_row.c
c
c Aufgabe ist Funktionen zur Erzeugung und Behandlung von 
c spaerlich besetzen Matrixzeilen == Vektoren mit Eintraegen von Typ double
c 
c
c	Datum		Modifikationen
c	----------------------------------------------------------
c 1.8.97 Datei angelegt, Funktionen aus matrix.c entnommen
c 11.08.98 Kommentare werden mit der Funktion read_comment ueberlesen
c 27.08.13 mat_row_update aus sgspn2nsolve übernommen
c 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/
#include <assert.h>
#include "list.h"
#include "global.h" 
#include "ememory.h"
#include "mat_row.h"

/**********************
mat_row_new ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:h
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
Row *mat_row_new(unsigned anzahl)
#else
Row *mat_row_new(anzahl)
     unsigned anzahl ;
#endif
{
  Row *v = (Row *)ecalloc(1,sizeof(Row)) ;
  
  if (0 < anzahl) 
  {
    v->no_of_entries = anzahl ;
    v->colind = (size_t *)ecalloc(anzahl,sizeof(size_t)) ; 
    v->val    = (double *)ecalloc(anzahl,sizeof(double)) ; 
  }
  return(v) ;
}
  
/**********************
mat_row_free ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_row_free(Row *v)
#else
int mat_row_free(v)
     Row *v ;
#endif
{
  if (!v)
    return(NO_ERROR);
  
  efree(v->colind) ;
  efree(v->val) ;
  efree(v) ; 
  return(NO_ERROR) ; 
}
    
    
/**********************
mat_row_output ##e 
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_row_output(FILE *fp, Row *v)
#else
int mat_row_output(fp, v)
     FILE   *fp ;
     Row    *v  ;
#endif
{
  unsigned i ;
  
  if (!fp || !v)
    return(OUTPUT_ERROR) ;
  for (i=0 ; i < v->no_of_entries ; i++) 
    fprintf(fp,"%lu %.15e\n", v->colind[i], v->val[i]) ;    
  return(NO_ERROR) ;  
}

/**********************
mat_row_input ##e 
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_row_input(FILE *fp, Row *v)
#else
int mat_row_input(fp, v)
     FILE   *fp ;
     Row    *v  ;
#endif
{
  unsigned i ;
  
  if (!fp || !v)
    return(INPUT_ERROR); 
  for (i=0 ; i < v->no_of_entries ; i++) 
  {
    fp = read_comment(fp) ;
    if (fscanf(fp,"%lu %le", &v->colind[i], &v->val[i]) == EOF)
      return FALSE ;
  }    
  return(NO_ERROR) ;  
}
 
   
/**********************
mat_row_print_file ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_row_print_file(FILE *fp, Row *v)
#else
int mat_row_print_file(fp, v)
     FILE *fp ;
     Row *v ;
#endif
{
  unsigned i ;
  unsigned j ;
  
  if (!fp)
    return FALSE ;
  if (!v) 
  {
    fprintf(fp,"Row ist leer\n") ;
    return TRUE ; 
  }
  fprintf(fp,"\nAusgabe eines Row-Objektes mit %lu Eintraegen\n", 
	  v->no_of_entries) ;
  fprintf(fp," (4 Eintraege (Spaltennummer, Wert) pro Zeile):\n") ;
  for (i=j=0 ;i<v->no_of_entries ; i++, j++) 
  {
    if (4 == j ) 
    {
      j=0 ;
      fprintf(fp,"\n (%lu,%f)", v->colind[i],v->val[i]) ; 
    }
    else
      fprintf(fp," (%lu,%f)", v->colind[i],v->val[i]) ; 
  }
  fprintf(fp,"\nEnde des Row-Objektes \n") ;    
  return TRUE ;  
}
    
/**
 * Aktualisieren und ggfs verkuerzen einer Matrixzeile gemaess Relation.
 * \date 7.3.00
 * \author Peter Kemper
 * \param r Matrixzeile
 * \param dim ???
 * \param relation Realtion
 * \param new_dim ???
 */
int mat_row_update(Row *r, int dim, int *relation, int new_dim)
{
  int i = 0 ; /* zaehlt Originalzeileneintraege durch */
  int free = 0 ; /* bestimmt jeweils den ersten Eintrag der ueberschrieben werden darf */

  /* leere Zeilen werden automatisch uebersprungen */
  while (i < r->no_of_entries)
    {
      /* in Teilmatrizen aus Kroneckerprodukten koennen
	 lokal Zustanduebergaenge moeglich sein, deren 
	 lokale Zielzustaende global verhindert werden,
	 daher kann der Fall unerreichbarer Zielzustaende
	 auftreten 
      */
      if (dim != relation[r->colind[i]])
	{
	  /* Zustandsindex r->colind[i] gemaess relation umbenennen */
	  /* falls Matrixeintraege verschoben werden muessen,
	     colind und val kopieren */
	  r->colind[free] = relation[r->colind[i]] ;
	  if (free != i)
	    {
	      r->val[free] = r->val[i] ; 
	      free++ ; /* free wurde gebraucht, naechstes Element betrachten
			diese koennte ebenfalls frei sein, dann ist ueberschreiben
			ohnehin ok, 
			dies koennte relevant sein, dann wuerde es durch i bereits behandelt
			sein, weil free i nicht mehr einholen kann, sobald einmal eine Luecke
			aufgetreten ist */
	    } 
	  else /* falls i == free, mit beiden fortschreiten */
	    free++ ;
	  /* auf jeden Fall mit i fortschreiten */
	}
      /*  else 	Zielzustand ist nicht erreichbar, 
	  Matrixeintrag soll ueberschrieben werden,
	  i inkrementieren, free belassen */
      i++ ;
      assert(free <= i) ;
    }
  /* nach Abschluss ist i == r->no_of_entries, free == 1. ungenutzte Position im Array,
     daher ist die Anzahl Zeileneintraege == free */
  assert(r->no_of_entries >= free) ;
  r->no_of_entries = free ;
  return(TRUE) ;
}

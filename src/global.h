/*********************
	Datei:	global.h
	Datum: 	17.01.90
	Autor:	Peter Kemper
	Inhalt:	defines
		Datenstrukturen
 		Funktionskoepfe
		globale Variablen
		die fuer alle Module verwendet werden

memo:

	Datum 	Aenderung
----------------------------------------------------------------------
	30.07.  T_list zusaetzliche Komponente int timed eingefuegt
		dient zur Unterscheidung von TIMED/IMMEDIATE Transitionen
	24.09.	REK_SIMPLE durch EQUAL_CONFLICT ersetzt
	01.10.	Korrektur property LIVE_NEC == LIVE_DTB == 25 ,
		LIVE_CTB auf 26 gesetzt
		LB_SMD == 27 eingefuegt
	4.6.	STR_CON in property eingefuegt
		SMD_NO.. Ergebniswerte eingefuegt

include <strings.h> Versuchsweise ersetzt wegen strdup 
includes auskommentiert
**********************/
#ifndef GLOBAL_H
#define GLOBAL_H
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/times.h>
#include <string.h>
#include <assert.h>
#include "defines.h"
#include "ememory.h"
#include "list.h"
#include "mat_time.h"
#include "mat_int_vektor.h"
#include "str_global.h"

/******************* Defines	 **********************/
#ifndef NULL
#define NULL	0
#endif
#ifndef TRUE
#define TRUE	1
#define FALSE	0
#endif

#define SUMME   0
#define PRODUKT 1



#define DIRLAENGE	100	/* max Namenslaenge des Directories fuer wspn */
#define STRLAENGE       100     /* max Stringlaenge fuer Namen, zb. Farbnamen */

/**** Error constants *****/
#ifndef NO_ERROR
#define NO_ERROR                     0
#endif
#define STORAGE_OVERFLOW             1
#define WRONG_MATRIX                 2
#define WRONG_INITIAL_DISTRIBUTION   3 
#define NO_CONVERGENCE               4
#define METHOD_NOT_IMPLEMENTED       5
#define SUBMATRIX_NOT_FOUND          6
#ifndef INPUT_ERROR
#define INPUT_ERROR                  7
#endif
#ifndef OUTPUT_ERROR
#define OUTPUT_ERROR                 8
#endif
#define FACTORIZATION_ERROR          9
/* JACOBI */
/* PS: elementweise mit perfect Shuffle (B. Plateau) */
#define STR_JOR_SHUFFLE 45 /*       SGSPN */
/* PS: elementweise, einzeln (P. Buchholz) */
#define STR_JOR_ELEM    41 /*       SGSPN */
/* PS: zeilenweise, nur wenn value != 0.0 */
#define STR_JOR_ROW     51 /*       SGSPN */
/* PS: zeilen und spaltenweise */
#define STR_JOR_ROWCOL  61 /*       SGSPN */
/* PS/TRS: shuffle ala plateau mit permutationsvektor */
#define STR_JOR_SHUFFLE_PERM  62 /*       SGSPN */
/* TRS: zeilenweise mit TRS-Permutation */
#define STR_JOR_PERM    47 /*       SGSPN */
/* TRS: zeilen/spalten gemischt, mit Suchbaum (G.Ciardo) */
#define STR_JOR_STREE   52 /*       SGSPN */
/* TRS: zeilen/spalten gemischt, mit gefaltetem Suchbaum */
#define STR_JOR_FOLD   54 /*       SGSPN */
/* TRS: zeilenweise, mit Suchbaum (G.Ciardo) */
#define STR_JOR_STREE1   58 /*       SGSPN */

/* GAUSS-SEIDEL */
/* PS: spaltenweise */
#define STR_SOR_COL     42 /*       SGSPN */
/* TRS: spaltenweise mit TRS-Permutation */
#define STR_SOR_PERM    48 /*       SGSPN */
/* TRS: spaltenweise mit Suchbaum (G.Ciardo) */
#define STR_SOR_STREE   53 /*       SGSPN */
/* TRS: zeilen/spalten gemischt, mit gefaltetem Suchbaum */
#define STR_SOR_FOLD   55 /*       SGSPN */

/* transiente Analyse */
/* TRS: RSN-Algorithmus mit TRS-Permutation */
#define STR_RSN 56 /* SGSPN */
/* TRS: Uniformization mit TRS-Permutation */
#define STR_UNI 57 /* SGSPN */
/* TRS: RSN-Algorithmus mit TRS-Permutation */
#define STR_ARSN 59 /* SGSPN */
/* TRS: Uniformization mit TRS-Permutation */
#define STR_AUNI 60 /* SGSPN */

#define HLQPN 1
#define FLAT  2

/****** Solution methods SGSPNs ************/
/* ALTE DEFINES */
/* without Permutation */
#define PWR 40
#define JOR 41
#define SOR 42
#define UNI 43
#define RSN 44
#define SJOR 45 /* Shuffle */
/* with Permutation */
#define PPWR 46
#define PJOR 47
#define PSOR 48
#define PUNI 49
#define PRSN 50

/***************** Schalter fuer Versionen ************/
#define SHOW_TEST1   FALSE   
#define SHOW_TEST2   FALSE    
#define SHOW_TEST3   FALSE 
#define SHOW_TEST4   FALSE    

#define TRACE_ALL    FALSE 

#define CHECK_PROG   FALSE


/******************* Positionen im Property-array **********************/
#define	PURE		0
#define	EINFACH		1
#define	HOMOGEN		2
#define	UNBLOCKED	3
#define	MARKED_GRAPH	4
#define	STATE_MACHINE	5
#define	FREE_CHOICE	6
#define	E_FREE_CHOICE	7
#define	SIMPLE		8
#define	E_SIMPLE	9
#define	EQUAL_CONFLICT	10
#define	NZF		11
#define	STATION		12
#define	NO_ISOLATED	13
#define	NO_MULTIPLE	14
#define	BOUNDED		15	/* Reachability Analysis: Boundedness TRUE/FALSE */
#define	LIVE		16	/* Reachability Analysis: Liveness TRUE/FALSE
				   bzw. Falls live und Anzahl Zustaende > 0, 
				   dann Anzahl Zustaende */
#define	NO_DEAD		17	/* Reachability Analysis: No Deadlock TRUE/FALSE */
#define	S_ANZAHL	18
#define	WS_ANZAHL	19
#define	T_ANZAHL	20
#define	C_ANZAHL	21
#define	SZK		22      /* Anzahl Strongly Connected Components im 
				   Reachability Graph der qualitativen Analyse */
#define	HEURISTIK	23
#define	BOUNDED_SUF	24
#define	LIVE_NEC	25
#define	LIVE_DTB	26
#define LB_SMD		27	/* s.u. defines fuer SMD Ergebnisse */
#define STR_CON		28	/* 0  : Netz ist nicht szh */
				/* >0 : Anzahl szh Teilnetze */
#define	WS_LAENGE_OK	29	/* >0 : Netz enthaelt FCFS/LCFS Stellen */
				/* 0  : Netz enthaelt keine FCFS/LCFS Stellen */
				/* 0  : Initialisierung */
				/* 0  : WS Laengen sind bekannt */

/******************* Positionen im Ergebnis-array **********************/
/* unused, just for erg_stelle etc. */
#define AUSLASTUNG              0
#define AUSLASTUNG_GESAMT       1
#define AUSLASTUNG_FARBEN       2
#define AUSLASTUNG_MITTEL       3
#define AUSLASTUNG_VARIANZ      4
#define AUSLASTUNG_VERTEILUNG   5
#define POPULATION              6
#define POPULATION_GESAMT       7
#define POPULATION_FARBEN       8
#define POPULATION_MITTEL       9
#define POPULATION_VARIANZ      10
#define POPULATION_VERTEILUNG   11
#define WSLAENGE                12
#define WSLAENGE_GESAMT         13
#define WSLAENGE_FARBEN         14
#define WSLAENGE_MITTEL         15
#define WSLAENGE_VARIANZ        16
#define WSLAENGE_VERTEILUNG     17
#define DURCHSATZ               18
#define DURCHSATZ_GESAMT        19
#define DURCHSATZ_FARBEN        20
#define DURCHSATZ_MITTEL        21
#define DURCHSATZ_VARIANZ       22
#define DURCHSATZ_VERTEILUNG    23
#define VERWEILZEIT             24
#define VERWEILZEIT_GESAMT      25
#define VERWEILZEIT_FARBEN      26
#define WARTEZEIT               27
#define WARTEZEIT_GESAMT        28
#define WARTEZEIT_FARBEN        29

/********************* Positionen im Analyse Array ***************************/
/* Achtung: Numerierung ist auf Menu_Strings in wspn.h und zugehoerige
	    Fehlermeldungen in error/M_wspn.h abgestimmt 
*/
#define	A_REACH		0 /* RS exporation NO,BASIC,IMP */
#define	A_PART		1 /* Partition NO,GIVEN,AUTO,MIXED */
#define	A_PINV  	2 /* P-invariants: TRUE, FALSE */
#define	A_TINV   	3 /* T-invariants: TRUE, FALSE */
#define	A_SHOW		4 /* output: INV,PART,TRS,PI,ALL */
#define	A_MAXSTATE	5 /* Limit TRS^i */

/* Werte im  Analyse Array */
#define NO    0

#define BASIC 1
#define IMP   2
#define LEVEL 3

#define GIVEN 2
#define AUTO  3
#define MIXED 4
#define DYNAMIC 4

#define INV   1
#define PINV  2
#define TINV  3

#define PART  4
#define TRS   5
#define PI    6 
#define ALL   7
#define STD_REWARDS 12

/***************** Scheduling Strategien *********************************/
#define FCFS			0
#define LCFS			1
#define PS			2
#define IS			3
#define HBFIFO                  4

/**************** Transitionstypen ***************************************/
/* Prefix wegen Namensgleichheit TIMED zu Attribut in qpn.gdl ************/
/* Change (10.5.96) Kemper according to Def GSPN 
   before #define T_IMMEDIATE 0, #define T_TIMED 1, #define T_SYNCHRONIZED 2 */
#define T_PRIORITIES TRUE
#define T_SYNCHRONIZED -1
#define T_TIMED     0
#define T_IMMEDIATE 1

/***************** Schalter fuer Farkas Algo ( set_of_invariants ) *******/
#define	F_S_INVARIANT		0
#define	F_T_INVARIANT		1
#define	F_DEADLOCK		2
#define	F_TRAP			3
#define F_S_APPROX              4 /* lediglich approx. mittels Floyd */

/****** Solution parameter ********/
typedef struct
{ 
  int          n_proc   ;            /* Number of submodels */
  int          method;            /* Solution method */
  int          no_of_iterations ; /* maximum number of iterations */
  double       epsilon ;          /* required accuracy */
  double       epsilon_2 ;        /* Error bound for aggregation/disaggregation of the HLQPN*/
  double       epsilon_3 ;        /* Error bound for aggregation/disaggregation of the LLQPN*/
  double       time_limit ;       /* Maximum CPU time */
  double       relaxation ;       /* relaxation parameter */
  int         *initials   ;       /* Store the initial states */
  int          overall_result ;   /* == 1 overall vector to be computed */
  struct int_vektor *subnet_results ;   /* indicates which subnet results have to
be computed */ 
} Solution_param ;


/***************** Stellenobjekt *******************/
typedef struct {
int     id ;                    /* Nummer der Stelle */
int	m0 ;			/* Startwert */
char	*name_g ;		/* Name der Stelle, von GSS erzeugt*/
char	*name_b ;		/* Name der Stelle,vom Benutzer erzeugt*/
char	*farbe_b ;		/* Name der Farbe,vom Benutzer erzeugt */
/* additional part for SGSPNs */
    int     gspn_id ;           /* Id des Teilnetzes (evtl. automatisch partitioniert)
				   derived from gspn_name or automatically
                                   value space: -1 unknown, default value for complete net
                                                0,1,2,... in partition */
    char    *gspn_name ;        /* Stringbezeichner des Teilnetzes (optional, usr given)
				   stores information from parser */ 
    int     local_id ;          /* id einer Stelle in isoliertem GSPN 
				   value space: 0,1, ... property[S_ANZAHL]-1 */ 
    int     cap ;               /* capacity, value space: 0,1, ... 0 == infinity */
    int     xpos ;              /* x-Position, values: 0,1, ... */
    int     ypos ;              /* y-Position, values: 0,1, ... */

} Stelle ;

/***************** Ergebnisanforderungen fuer Stellen, unused *******************/
typedef struct {
char	*name_g ;		/* Name der Stelle, von GSS erzeugt*/
				/* Ergebnisanforderungen */
int	ergebnis[POPULATION_VERTEILUNG - POPULATION] ;	
List	**farben ;		/* Liste mit Farbenid's,
                                   Elemente sind direkt aus stellennamen_list */
} Erg_stelle ;

/***************** Warteschlangenstellenobjekt, unused *******************/
typedef struct {
char	*name_g ;		/* Name der Stelle, von GSS erzeugt*/
int	sched ;			/* Schedulingstrategie */
int	ergebnis[WARTEZEIT_FARBEN + 1] ;	/* Ergebnisanforderungen */
int	anz_farben ;		/* Anzahl Farben */
int	max_laenge ;		/* maximale Markenzahl aller Farben */
int	anz_bed ;		/* Anzahl der Bediener */
List	**farben ;		/* Liste mit Spezifikation der Farben */
char	**schlange ;		/* Array mit Positionsnamen der WS-Schlange 
                                   Laenge 0 .. max_laenge 
                                   fuer FCFS, LCFS 
                                   (Usenum Schnittstelle) */
} Ws_stelle ;

typedef struct {
int	id ;			/* Nummer der Stelle */
int	m0 ;			/* Startwert */
int	rang ;			/* Rangfolge der Farben an dieser Stelle 
                                   dieser Wert wird innerhalb u_insert_fcfs.lcfs
                                   normiert, d.h. unter der Annahme, dass die Farben
                                   nach ihren rangwerten geordnet vorliegen, werden
                                   die Rangwerte auf 1,2,3 ... abgebildet
                                   Grund: Codierung der Phasenzahlen rang*100+i
                                   Bei HB-FIFO zeigt rang < 0 holdbacktoken an,
                                   wobei relation holdback token zu token durch +- rang
                                   wiedergegeben wird */
double	e ;			/* Erwartungswert des Bedienwunsches der Farbe */
double	vk ;			/* Variationskoef. des Bedienwunsches der Farbe */
int	anz_phasen ;		/* Anzahl Bedienphasen */
double	rate1 ;			/* Rate des Bedienwunsches 1. Phase */
double	rate2 ;			/* Rate des Bedienwunsches anderer Phasen */
double	prob ;			/* Abgangswahrscheinlichkeit nach der 1. Phase */
char	*name_in ;		/* Name der Eingangsstelle (Usenum Schnittstelle)
                                   auf diesen String wird ebenfalls durch in_vektor[id]
                                   in use_schnitt.h verwiesen
				   Sonderfall HB-FIFO: name_in == NULL */
char	*name_out ;		/* Name der Ausgangsstelle (Usenum Schnittstelle)
                                   auf diesen String wird ebenfalls durch out_vektor[id]
                                   in use_schnitt.h verwiesen */
char	**phasen ;		/* Array mit Phasennamen   (Usenum Schnittstelle)
                                   Laenge 0 .. anz_phasen 
                                   fuer IS, PS */
} Farben ;

/***************** Aktivierungsmatrix *******************/
typedef struct stell_list {
int     id ;                    /* Nummer der Stelle */
int     anz ;                   /* Anzahl Marken auf der Stelle */
struct  stell_list      *next ;
} S_list ;

typedef struct trans_list {
int	id ;			/* Nummer der Transition, bisher 
				   Begann die Indizierung bei 0, ab 21.1.97
				   auf 1,... abgeaendert */
int	timed ;			/* T_IMMEDIATE, T_TIMED, T_SYNCHRONIZED */
double	prob ;			/* Feuerungshaeufigkeit oder Mittelwert */
char	*name_g ;		/* Name der Transition, von GSS erzeugt*/
char	*name_b ;		/* Name der Transition,vom Benutzer erzeugt*/
/* char	*farbe_g ;		Name der Farbe, von GSS erzeugt, unnotig geworden  */
char	*farbe_b ;		/* Name der Farbe,vom Benutzer erzeugt */
S_list	*w_plus ;		/* Inzidenzliste fuer w+ */
S_list	*w_minus ;		/* Inzidenzliste fuer w- */
S_list	*w_inhib ;              /* Inhibitorkanten, Eintraege haben bzgl anz Wertebereich >= 0 */
struct 	trans_list	*next ;	/* Nachfolger */
int xpos ;                      /* x-Position, values: 0,1, ... */
int ypos ;                      /* y-Position, values: 0,1, ... */
} T_list ;


  
/**************** HQPN ***********************************/
typedef struct 
{
  int gspn ;                     /* Identifier des GSPNs */
    char *netname ;                /* Name des GSPNs (22.6.98 eingefuegt) */
  int           *limits ;               /* Array mit Obergrenzen der Tokenanzahl an Stellen */
  List		**stellenid_list ;	/* Zugriff auf Stelle-Objekte mit id */
  List		**stellennamen_list ;	/* Zugriff auf Stelle-Objekte mit name_g,farbe_b
					   Lex-Liste ist sortiert nach (name_g,farbe_b) */
  T_list	*matrix ;		/* Inzidenzmatrix, sortiert, so dass
					   immediate transitions vor timed transitions
					   sind */
  T_list *fst_timed ;                   /* Tail mit timed transitions aus matrix
					   Achtung: wird erst in f_gen_init_ds gesetzt */
  short flag_matrix_array ;             /* TRUE: matrix zeigt auf array mit T_list Elementen
                                           FALSE: matrix zeigt auf T_list Elemente, die ueber 
                                           ihren next-Zeiger verkettet sind  */
  List		**ws_list ;		/* Liste der Warteschlangen-Objekte */
  List		**erg_list ;		/* Ergebnisanforderungen Ordinary Stellen */
  List          **sub_list ;            /* Liste mit Subnetzstellen, fuer HQPNs 
					   Elemente sind vom Typ Subnet */ 
  int	property[WS_LAENGE_OK + 1] ;	/* Array mit Netzeigenschaften */
  struct int_vektor *input_trans ;             /* Vektor mit ids von Input Transitionen, unsortiert */ 
} HQPN_net ;

typedef struct 
{
  char	*name_g ;		/* Name der Stelle, von GSS erzeugt*/
  int	ergebnis[WARTEZEIT_FARBEN + 1] ;	/* Ergebnisanforderungen */
  int	anz_farben ;		/* Anzahl Farben */
  List	**farben ;		/* Liste mit Spezifikation der Farben */
  HQPN_net  *net ;    /* Zeiger auf Subnetzbeschreibung */
  int  agg ;     /* flag, gibt an ob aggregiert werden soll in AD-Verfahren
		    Wertebereich TRUE/FALSE */ 
} Subnet ;

    
/**************** zusaetzliche Variable fuer SGSPNs *******************/
/* List **map_list ;  */              /* Liste fuer Partition der Stellen auf die GSPNs,
				    aufsteigend sortiert nach Komponente gspn, local_id
				    */
/* List **net_list ;   */             /* Liste der einzelnen GSPNs */
                                 /* synchronisierte Variable tauchen mehrfach auf */
                                 /* Stellen und Transitionen sind neu numeriert */
/* HQPN_net *top_net ;    */                /* Speichert kontext fuer gesamtnetz */
/* HQPN_net *current_net ; */              /* stellt das aktuelle Netz zur Verfuegung
				    durch diese Buendelung vormals globaler Variablen
				    wird ein einfacher Kontextwechsel ermoeglicht
				    */
/***************** globale Variablen **********************/
/*char		*directory ;	*/	/* aktuelles Install-Directory fuer wspn */

#ifndef time_to_seconds_factor
#define time_to_seconds_factor (1.0/60.0)
#endif
#ifdef MODUL_GLOBAL_C
/**************** globale Variablen zur Performance *************/
double zeit_cpu_start ;                 /* wird in gen_matrix gesetzt */
double zeit_cpu_component_exploration ; /* wird in gen_matrix gesetzt */
double zeit_cpu_trs_exploration ;       /* wird in gen_permutation_vektor,bzw
				       gen2_permutation_vektor gesetzt */
double zeit_cpu_iteration_step ;        /* bisher nicht gesetzt */

double zeit_user_start ;                 /* wird in gen_matrix gesetzt */
double zeit_user_component_exploration ; /* wird in gen_matrix gesetzt */
double zeit_user_trs_exploration ;       /* wird in gen_permutation_vektor,bzw
				       gen2_permutation_vektor gesetzt */
double zeit_user_iteration_step ;        /* bisher nicht gesetzt */

/* Groesse des Models, Speicheranforderungen */
int    zeit_ps ;                    /* wird in gen_matrix gesetzt */
int    zeit_trs ;                   /* wird in gen_matrix gesetzt */
int    zeit_sum_trs_i ;             /* wird in gen_reach gesetzt */
int    zeit_sum_q_i ;               /* wird in gen_transform_graph gesetzt */
int    zeit_q_i ; /* ungenutzt */

/* Aufwand fuer binary search mit Permutation */
double zeit_perm_binary_max ;
double zeit_perm_binary_sum ;
int  zeit_perm_binary_call ;
#else
/* wichtig, damit globale Variablen nur einmal existieren */
extern double zeit_cpu_start ;
extern double zeit_cpu_component_exploration ;
extern double zeit_cpu_trs_exploration ; 
extern double zeit_cpu_iteration_step ; 
extern double zeit_user_start ;      
extern double zeit_user_component_exploration ;
extern double zeit_user_trs_exploration ; 
extern double zeit_user_iteration_step ;
extern int    zeit_ps ;   
extern int    zeit_trs ;   
extern int    zeit_sum_trs_i ;   
extern int    zeit_sum_q_i ;   
extern int    zeit_q_i ;
extern double zeit_perm_binary_max ;
extern double zeit_perm_binary_sum ;
extern int    zeit_perm_binary_call ; 
#endif

#ifndef CC_COMP
/* global.c */
/* Zeit */
extern void   gen_zeit_init() ;
extern void   gen_zeit_print() ;
/* HQPN_net */
extern HQPN_net *g_hqpn_net_new( int gspn_id ) ;
extern int       g_hqpn_net_vgl_gspn( int *key, HQPN_net *elem ) ;
/**
 * lexikographische Sortierung mit Duplikaten.
 * Ein fehlender netname wird als kleinstes Element der Ordnung
 * angenommen, ferner sind Duplikate in der Liste zulaessig und es wird
 * jeweils an das Ende der Teilliste aus Duplikaten eingefuegt, um eine 
 * reihenfolgentreue Abbildung zu erleichtern.
 * gibt -1 zurueck falls key < elem->netname
 * gibt 1 sonst
 * \author Peter Kemper
 * \date 14.10.99
 * \param key ???
 * \param elem Objekt vom Typ HQPN_net
 */
extern int g_hqpn_net_vgl_dup_gspn_name( char *key, HQPN_net *elem ) ;
/**
 * Vergleichsfkt: HQPN_net, Komponente gspn
 * \author Peter Kemper
 * \date 4.10.99
 * \param key Objekt vom Typ HQPN_net
 * \param elem Objekt vom Typ HQPN_net
 * \return ???
 */
extern int g_hqpn_net_vgl_gspn_fallend( HQPN_net *key, HQPN_net *elem ) ;
/**
 * Ausgabefunktion fuer Inzidenzfunktion
 * \date 17.10.96
 * \author Peter Kemper
 * \param fp Ausgabefile
 * \param net Objekt vom Typ HQPN_net
 * \param t Objekt vom Typ T_list
 * \param s Objekt vom Typ S_list
 * \param arcid ???
 * \param w_flag: -1 w_minus, 0 w_inhib, 1 w_plus
 */
extern int g_incidence_print_apnnfile(FILE *fp, HQPN_net *net,T_list *t, S_list *s,int *arcid, short w_flag);
/**
 * Ausgabefunktion fuer HQPN_net
 * \date 17.10.96
 * \author Peter Kemper
 * \param fp Ausgabefile
 * \param net Objekt vom Typ HQPN_net
 * \param netid ID des Netzes
 */
extern void g_hqpn_net_print_apnnfile_with_correspondid(FILE *fp, HQPN_net *net, char *netid);
/**
 * Sonderfall, Transitionen in Array abgelegt!
 * Innerhalb der Invariantenanalyse werden nur hqpn_nets betrachtet
 * deren ws_list, erg_list und sub_list leer sind.
 * Falls Transitionen innerhalb eines Arrays angeordnet wurden, darf natuerlich
 * nicht jede Transition einzeln, sondern abschliessend nur das gesamte Array
 * freigegeben werden!
 * \date 2.5.97
 * \author Peter Kemper
 * \param hqpn_net Objekt vom typ HQPN_net
 */  
extern void      g_hqpn_net_free_trans_array( HQPN_net *hqpn_net ) ;
/**
 * Abbauen einer HQPN_net DS wobei matrix als LISTE, resp Array abgearbeitet wird.
 * Innerhalb der Invariantenanalyse werden nur hqpn_nets betrachtet
 * deren ws_list, erg_list und sub_list leer sind.
 * Achtung: Transitionen sind EINZELN allokiert worden -> sonst g_hqpn_free_trans_array
 * \date 2.5.97
 * \author Peter Kemper
 * \param hqpn_net Objekt vom Typ HQPN_net
 */
extern void      g_hqpn_net_free( HQPN_net *hqpn_net ) ;
/**
 * Ausgabefunktion fuer HQPN_net
 * \date 17.10.96
 * \author Peter Kemper
 * \param fp Ausgabefile
 * \param net Objekt vom Typ HQPN_net
 * \param netid ID des Netzes
 */
extern void      g_hqpn_net_print_apnnfile(FILE *fp, HQPN_net *net,char *netid) ;
/**
 * ???
 * \date 8.11.99
 * \author Peter Kemper  
 * \param net Objekt vom Typ HQPN_net
 * \return ???
 */
extern int g_hqpn_net_get_max_prio(HQPN_net *net) ;
/**
 * ???
 * \date 30.9.99
 * \author Peter Kemper  
 * \param net Objekt vom Typ HQPN_net
 * \return ???
 */
extern char *g_hqpn_net_get_partitionname(HQPN_net *net) ;

/* Stelle */
/**
 * Erzeugen und Initialisieren  einer Stelle.
 * Strings werden auf eigenen, neuen Speicher kopiert !
 * \date 5.5.97
 * \author Peter Kemper
 * \param id ???
 * \param mo ???
 * \param name_g ???
 * \param name_b ???
 * \param farbe_b ???
 * \param gspn_name ???
 * \param gspn_id ???
 * \param local_id ???
 * \param cap ???
 * \return Objekt vom Typ Stelle
 */
extern Stelle   *g_stelle_new(int id, int m0, char *name_g, char *name_b, char *farbe_b,
			    char *gspn_name, int gspn_id, int local_id, int cap) ; 
/**
 * Erzeugen einer vollstaendigen Kopie auf eigenem Speicherbereich
 * \date 17.2.95
 * \author Peter Kemper
 * \param s_old Objekt vom Typ Stelle
 * \return neues Objekt vom Typ Stelle
 */
extern Stelle   *g_stelle_copy(Stelle *s_old) ;
/**
 * Ein fehlender gspn_name wird als kleinstes Element der Ordnung
 * angenommen, ferner sind Duplikate in der Liste zulaessig und es wird
 * jeweils an das Ende der Teilliste aus Duplikaten eingefuegt, um eine 
 * reihenfolgentreue Abbildung zu erleichtern.
 * gibt -1 zurueck falls key < elem->gspn_name
 * gibt 1 sonst
 * lexikographische Sortierung mit Duplikaten
 * \author Peter Kemper
 * \date 14.10.99
 * \param key ???
 * \param elem ???
 * \return ???
 */
extern int g_stelle_vgl_dup_gspn_name( char *key, Stelle *elem ) ;
/**
 * Vergleichsfunktion fuer Stelle, Komponente id
 * \date 17.12.90
 * \param key ???
 * \param elem ???
 * \return -1 key < elem->key; 0 key = elem->key; 1 key > elem->key
 */
extern int       g_stelle_vgl_id(int     *key , Stelle  *elem ) ;
/**
 * Vergleichsfunktion fuer Stelle, Komponente id
 * \date 17.12.90
 * \param key ???
 * \param elem ???
 * \return -1 key < elem->key; 0 key = elem->key; 1 key > elem->key
 */
extern int       g_stelle_vgl_id_unsorted(int     *key , Stelle  *elem ) ;
/**
 * Vergleichsfunktion fuer Stelle, Komponente id
 * \date 17.12.90
 * \param key ???
 * \param elem ???
 * \return -1 key < elem->key; 0 key = elem->key; 1 key > elem->key
 */
extern int       g_stelle_vgl_namen(Stelle  *key, Stelle  *elem ) ;
/**
 * Vergleichsfunktion fuer Stelle, Komponente (char *)name_g, 
 * \date 27.2.97
 * \param key ???
 * \param elem ???
 * \return -1 key < elem->key; 0 key = elem->key; 1 key > elem->key
 */
extern int       g_stelle_vgl_name_g(char *key, Stelle *elem) ; 
/**
 * Vergleichsfunktion fuer Stelle, name_g.farbe_b
 * \date 17.12.98
 * \param key ???
 * \param elem ???
 * \return 0 key = elem->key; 1	key != elem->key
 */
extern int g_stelle_vgl_name_g_dot_farbe_b_unsorted( char *key, Stelle	*elem) ;
/**
 * Vergleichsfunktion fuer Stelle, name_g.farbe_b
 * \date 17.12.98
 * \param key ???
 * \param elem ???
 * \return 0 key = elem->key; 1	key != elem->key
 */
extern int g_stelle_vgl_name_g_dot_farbe_b_unsorted( char *key, Stelle	*elem) ;
/**
 * Vergleichsfunktion fuer Stelle, name_g.farbe_b
 * \date 17.12.98
 * \param key ???
 * \param elem ???
 * \return 0 key = elem->key; 1	key != elem->key
 */
extern int g_stelle_vgl_name_g_dot3_name_g_dot_farbe_b_unsorted( char *key, Stelle *elem ) ;
/**
 * Ausgabefunktion fuer Stelle
 * \date 17.10.96
 * \author Peter Kemper
 * \param fp Ausgabefile 
 * \param s Objekt vom Typ Stelle
 */
extern void g_stelle_print_file(FILE *fp, Stelle *s) ;
/**
 * Ausgabefunktion fuer Stelle
 * \date 17.10.96
 * \author Peter Kemper
 * \param fp Ausgabefile 
 * \param s Objekt vom Typ Stelle
 */
extern void      g_stelle_print_file_name_g_local_id(FILE *fp, Stelle *s) ; 
/**
 * Ausgabefunktion fuer Stelle.
 * Ausgabeformat: name_g.name_b.farbe_b, bei fehlenden Eintraegen P.N.C
 * name-g wird ggfs um _farbe suffix bereinigt
 * \date 4.12.98
 * \author Peter Kemper
 * \param fp Ausgabefile
 * \param s Objekt vom Typ Stelle
 */
extern void      g_stelle_print_file_name_g_dot3_local_id(FILE *fp, Stelle *s) ;

/**
 * Ausgabefunktion fuer Stelle als einfarbiges Netz
 * \date 17.10.96
 * \author Peter Kemper
 * \param fp Ausgabefile
 * \param s Objekt vom Typ Stelle
 */
extern void g_stelle_print_apnnfile(FILE *fp, Stelle *s) ;
/**
 * Ausgabefunktion fuer Stelle als einfarbiges Netz
 * \date 17.10.96
 * \author Peter Kemper
 * \param fp Ausgabefile
 * \param s Objekt vom Typ Stelle
 */
extern void g_stelle_print_apnnfile_with_correspondid(FILE *fp, Stelle *s);
/**
 * mit free auf name_g, name_b und farbe_b
 * \date 17.8.95
 * \author Peter Kemper
 * \param s Objekt vom Typ Stelle
 * \return immer TRUE!
 */
extern int       g_stelle_free(Stelle *s ) ;
/**
 * ???
 * \date 27.10.98
 * \author Peter Kemper
 * \param s Objekt vom Typ Stelle
 * \param x X-Position der Stelle
 * \param y Y-Position der Stelle
 * \return immer TRUE!
 */
extern int       g_stelle_set_pos(Stelle *s, int x, int y ) ;
/**
 * ???
 * \date 27.10.98
 * \author Peter Kemper
 * \param s Objekt vom Typ Stelle
 * \param x Output: X-Position der Stelle
 * \param y Output: Y-Position der Stelle
 * \return immer TRUE!
 */
extern int       g_stelle_get_pos(Stelle *s, int *x, int *y ) ;
/**
 * ???
 * \author Peter Kemper
 * \param s Objekt vom Typ Stelle
 * \return Name der Partition
 */
extern char *g_stelle_get_partitionname(Stelle *s) ;


/* T_list */
/**
 * Ausgabefunktion fuer Transition
 * \date 17.10.96
 * \author Peter Kemper
 * \param fp Ausgabefile
 * \param t Objekt vom Typ T_list
 */
extern void g_t_list_print_file(FILE *fp, T_list *t) ;
/**
 * Vergleichsfunktion fuer T_list, Komponente id.
 * Ann.: Die Liste ist nach den Id's der Transitionsfarben sortiert. 
 * \date 17.12.90
 * \param key ???
 * \param elem ???
 * \return -1 key < elem->key; 0 key = elem->key; 1 key > elem->key
 */
extern int       g_t_list_vgl_id( int *key,T_list *elem) ;
/**
 * Element vom Typ T_list erzeugen und initialisieren.
 * WARNUNG: timed Attribut wird direkt gesetzt
 * \date 5.5.97
 * \param id ???
 * \param timed ???
 * \param prob ???
 * \param name_g ???
 * \param name_b ???
 * \param farbe_b ???
 * \param w_plus ???
 * \param w_minus ???
 * \param w_inhib ???
 * \param next ???
 * \return Objekt vom Typ T_list
 */
extern T_list *g_t_list_new(int id, int timed, double prob, char *name_g,
		     char *name_b, char *farbe_b, S_list *w_plus,
		     S_list *w_minus, S_list *w_inhib, T_list *next)  ;
/**
 * Element vom Typ T_list erzeugen und Inhalt hineinkopieren ohne next.
 * Es wird eine Kopie erzeugt, wobei Strings eigenen Speicher bekommen, die 
 * Incidencefunktionen kopiert werden. Lediglich der next-Zeiger wird auf NULL gesetzt.
 * WARNUNG: timed Attribut wird direkt gesetzt
 * \date 5.5.97
 * \param t Element vom Typ T_list
 * \return neues Element vom Typ T_list
 */
extern T_list *g_t_list_copy(T_list *t) ;
/**
 * Element vom Typ T_list freigeben inclusive Inzidenzlisten
 * \date 5.5.97
 * \param t Element vom Typ T_list
 */
extern void g_t_list_free(T_list *t) ;
/**
 * ???
 * \date 27.10.98
 * \author Peter Kemper
 * \param s Objekt vom Typ T_list 
 * \param x X-Position 
 * \param y Y-Position
 * \return immer TRUE
 */
extern int g_t_list_set_pos(T_list *t, int x, int y ) ;
/**
 *  Liefert die X- und Y-Position 
 * \date 27.10.98
 * \author Peter Kemper
 * \param s Objekt vom Typ T_list
 * \param x Output: X-Position
 * \param y Output: Y-Position
 * \return immer TRUE
 */
extern int g_t_list_get_pos(T_list *t, int *x, int *y ) ;
/**
 * Liefert den Namen der Transition
 * \date 30.9.99
 * \param t Element vom Typ T_list	
 * \return Name der Tarnsition (Format T.N.C)
 */
extern char *g_t_list_get_transitionname(T_list *t) ;

/**
 * Transition is synchronized -> t/f
 * \date 27.9.99
 * \author Peter Kemper
 * \param t Element vom Typ T_list 
 * \return TRUE if transition is synchronized, FALSE otherwise
 */
extern int g_t_list_is_sync(T_list *t) ;

/**
 * ???
 * \date 30.9.99
 * \author Peter Kemper  
 * \param net Objekt vom Typ HQPN_net
 * \return ???
 */
extern int g_hqpn_net_get_number_of_sync(HQPN_net *net) ;

/**
 * ermittelt Transition
 * \date 5.11.99
 * \author Peter Kemper  
 * \param net Objekt vom Typ HQPN_net
 * \param id ???
 * \return Objekt vom Typ T_list
 */
extern T_list *g_hqpn_net_find_transition(HQPN_net *net, int id) ;

/**
 * gives priority in {0,1,2,...}
 * internal priority representation is
 * not synchronized: 
 * 0 = timed, 1,2,... = immediate
 * synchronized
 * -1 = timed, -2, -3, ... = immediate with prio 1,2,...
 * \date 27.9.99
 * \author Peter Kemper
 * \param t Element vom Typ T_list 
 * \return prio in {0,1,2,...}	
 */
extern int g_t_list_get_priority(T_list *t) ;

/**
 * sets priority of synchronized/not sync
 * internal priority representation is
 * not synchronized: 
 * 0 = timed, 1,2,... = immediate
 * synchronized
 * -1 = timed, -2, -3, ... = immediate with prio 1,2,...
 * \date 27.9.99
 * \author Peter Kemper
 * \param t Element vom Typ T_list * 
 * \param synchronized TRUE/FALSE
 * \param priority ???
 * \return TRUE ok, FALSE otherwise
 */
extern int g_t_list_set_priority_sync(T_list *t, int synchronized, unsigned int priority) ;

/**
 * sets priority of transition
 * internal priority representation is
 * not synchronized: 
 * 0 = timed, 1,2,... = immediate
 * synchronized
 * -1 = timed, -2, -3, ... = immediate with prio 1,2,...
 * \date 27.9.99
 * \author Peter Kemper
 * \param t Element vom typ T_list 
 * \param priority ???
 * \return TRUE ok
 */
extern int g_t_list_set_priority(T_list *t, unsigned int priority) ;

/**
 * sets transition as synchronized/not sync	
 * internal priority representation is
 * not synchronized: 
 * 0 = timed, 1,2,... = immediate
 * synchronized
 * -1 = timed, -2, -3, ... = immediate with prio 1,2,...
 * \date 27.9.99
 * \author Peter Kemper
 * \param t Element vom Typ T_list, 
 * \param synchronized TRUE/FALSE 
 * \retrun TRUE ok
 */
extern int g_t_list_set_sync(T_list *t, int synchronized);

/**
 * sucht nach Transition in Array gemaess id
 * \date 5.11.99
 * \author Peter Kemper
 * \param t Eleemnt vom Typ T_list
 * \param len Laenge
 * \param id ???
 * \return NULL Element nicht gefunden, ansonsten Zeiger auf Element
 */
extern T_list *g_t_list_find_by_id_in_array(T_list *t, int len, int id) ;

/**
 * sucht nach Transition in Array gemaess namen
 * \date 5.11.99
 * \author Peter Kemper
 * \param t Element vom Typ T_list
 * \param len Laenge
 * \param name_g muss angegeben sein 
 * \param name_b kann NULL sein == Wildcard
 * \param farbe_b kann NULL sein == Wildcard
 * \return NULL Element nicht gefunden, Zeiger auf Element dass bzgl name_g, name_b und farbe_b uebereinstimmt
 */
extern T_list *g_t_list_find_by_names_in_array(T_list *t, int len, char *name_g, char *name_b, char *farbe_b) ;

/**
 * sucht nach Transition in Array gemaess id
 * \date 5.11.99
 * \author Peter Kemper
 * \param t Element vom Typ T_list
 * \param len Laenge
 * \param id ???
 * \retrun NULL Element nicht gefunden, Zeiger auf Element
 */
extern T_list *g_t_list_find_by_id_in_list(T_list *t, int id) ;

/**
 * sucht nach Transition in Array gemaess Namen
 * \date 5.11.99
 * \author Peter Kemper
 * \param t Element vom Typ T_list
 * \param len Laenge
 * \param name_g muss angegeben sein 
 * \param name_b kann NULL sein == Wildcard
 * \param farbe_b kann NULL sein == Wildcard
 * \return NULL Element nicht gefunden, Zeiger auf Element dass bzgl name_g, name_b und farbe_b uebereinstimmt
 */
extern T_list *g_t_list_find_by_names_in_list(T_list *t, char *name_g, char *name_b, char *farbe_b) ;

/* S_list */
/**
 * Erzeugen und Initialisieren eines S_list Elementes
 * \date 5.5.97
 * \param id ???
 * \param anz ???
 * return Objekt vom Typ S_list
 */
extern S_list *g_s_list_new( int id, int anz) ; 
/**
 * loesche Liste mit objekten vom Typ s_list 
 * \date 14.02.90
 * \param elem Objekt vom Typ S_list
 */
extern void g_s_list_free_list(S_list *elem) ;
/**
 * Einfuegen eines Elementes in sortierte S_list Liste.
 * S-list nach ids aufsteigend sortiert
 * Idee bei **s Konstruktion ist, dass *s jeweils die Adresse des next-Zeigers
 * des aktuellen S_list-Listenelementes ist.
 * \date 5.5.97
 * \param sl Zeiger auf Objekt vom Typ S_list
 * \param elem Objekt vom Typ S_list
 * \param adr Zeiger auf Objekt vom Typ S_list
 * \return FALSE bei Fehler, ansonsten TRUE
 */
extern int g_s_list_insert_elem( S_list **sl, S_list *elem, S_list **adr) ;

/**
 * Kopie einer Liste erzeugen
 * \date 14.02.90
 * \param elem Objekt vom Typ S_list
 * \return neues Objekt vom Typ S_list
 */
extern S_list *g_s_list_duplicate_list( S_list *elem) ;

/*

#else
extern double mat_user_time() ;
extern double mat_timeused () ;
extern void gen_zeit_init() ;
extern void gen_zeit_print() ;
extern HQPN_net *g_hqpn_net_new() ;
extern int       g_hqpn_net_vgl_gspn() ;
extern Stelle *g_stelle_copy() ;
extern int g_stelle_print_file( ) ;
extern void g_s_list_free_list() ;
extern int g_t_list_print_file( ) ; */

/**************** Funktionskoepfe aus qpn.c *************************/
extern int 	g_stelle_vgl_id() ;
extern int      g_stelle_vgl_namen() ;
extern int      q_vgl_map_global_id() ;
extern int      del_ws() ;
extern int      del_stelle() ;
extern int      del_erg() ;
extern int      del_net() ;

/**************** Funktionskoepfe aus classify.c *************************/
extern	int	c_str_con() ;

/**************** Funktionskoepfe aus ausgabe.c *************************/
extern int      aus_map_file() ;
extern int      aus_net_file() ;


/**************** Funktionskoepfe *************************/
extern FILE	*w_fopen() ;
#endif
#endif

/********************
	Name	:	Peter Kemper
	Datum	:	08.11.1990
	Datei	:	list.c
	Version	:	0.1
	C-	:	
                        28.11.2011 
                        Peter Buchholz 
                        Funktion list_remove_elem eingefügt
	Beschreibung :	Routinen fuer die Verwaltung beliebiger Objekte in
			linearen Listen ( sortiert und unsortiert )

	Modifikationen :
	Datum		Beschreibung
	-------------------------------------------------------
        10.2.95         neue Funktion list_file_print
	7.11.97         err_print ausgebaut

Uebersicht:
    Operationen auf Listen :
	List	**list_new() ;
	int	list_free( List **list, int (*del)( void *elem ) ) ;
	int	list_kill( List **list, int (*del)( void *elem ) ) ;
	int	list_find(List **list,void **adr,void *key,int (*comp)()) ;
	int	list_insert(List **list,void **adr,void *key,void *elem,
			    int(*comp)()) ;
	int	list_append( List **list, void *elem ) ;
	int	list_prepend( List **list, void *elem) ;
	void	*list_first( List **list) ;
	List	**list_next( List **list) ;
	int	list_delete( List **list,void *key, int (*del)(), int (*comp)())
	void	*list_cont( List **list) ;
	int	list_empty( List **list ) ;
	int	list_print( List **list, int (*print)( void *elem ) ) ;
	int	list_file_print( List **list, FILE *fp, int (*print)( void *elem ) ) ;
	int	list_delete_forall( List **list,void *key, int (*del)(), 
					int (*comp)())
        int     list_compare_element(void *key, void *element )
				       
        int     list_free_delete_standard(void *element)
        int     list_free_delete_dummy(void *element)
Hilfsfunktionen:
	List	*l_neu( void *elem ) ;


Annahmen fuer Funktion comp	
1.	int	comp( void *, void * )
2.	comp( key, elem2 ) <  0   	falls key < elem2->key
3.	comp( key, elem2 ) == 0		falls key = elem2->key
4.	comp( key, elem2 ) >  0		falls key > elem2->key
5. 	Alle Routinen fuer diese Liste arbeiten mit demselben comp ! 

Annahmen fuer Funktion del
1.	int	del( void * )
2.	Die Funktion wird mit einem Zeiger auf ein verwaltetes Objekt oder Null
	aufgerufen.
			
Annahmen fuer Funktion print in list_file_print
1.      int print( fp, elem )			
********************/
#include <stdio.h>

#include "list.h"

#if QPN
#include        "ememory.h"
#else
#include <malloc.h>
#endif



/**********************
list_new     ##e 	Erzeugen einer neuen Liste     
Datum:  08.11.90
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#if LIST_PREPROC
#else
#ifndef CC_COMP
List	**list_new()
#else
List	**list_new()
#endif
{
    List	**l = ( List **)ecalloc( 1, sizeof( List * ) ) ;

    return l ;
} /* list_new */
#endif

/**********************
list_free_delete_standard     ##e 	Freigabe der Verwaltungsstruktur
Datum:  20.10.94
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_free_delete_standard(void  *element )
#else
int list_free_delete_standard( element )
     void *element ; 
#endif
{
  if (element)
    efree( element ) ;
  return TRUE ;
} /* list_free_delete_standard */

/**********************
list_free_delete_dummy     ##e 	Freigabe der Verwaltungsstruktur
Datum:  20.10.94
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_free_delete_dummy( void *element )
#else
int list_free_delete_dummy( element )
     void *element ; 
#endif
{
  return TRUE ;
} /* list_free_delete_dummy */

/**********************
list_compare_element     ##e 	
Datum:  20.10.94
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_compare_element( void *key, void *element )
#else
int list_compare_element( void *key, void *element )
     void *key ; 
     void *element ; 
#endif
{
  if (key == element)
    return 0 ;
  return 1 ;
  
} /* list_compare_element */


/**********************
l_neu     ##s 	Listenelement erzeugen     
Datum:  08.11.90
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
List	* l_neu( void *elem )
#else
List	* l_neu( elem )
     void	*elem ;
#endif
{
    List	*l = ( List *)ecalloc( 1, sizeof( List ) ) ;

    l->elem = elem ;
    return l ;
} /* l_neu */


/**********************
list_free      ##e	Freigeben einer Liste 
Datum:  08.11.90
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
Nach dem Freigeben zeigt list auf den Kopf einer leeren Liste ( NIL ).
list_free laesst eine leere Liste zurueck, d.h. der Kopf einer leeren
Liste ( das Verwaltungselement ) besteht noch. Dies wird mit list_kill
ebenfalls freigegeben.
Fazit: nach list_kill muss ein list_new erfolgen, nach list_free nicht.
**********************/
#ifndef CC_COMP
int list_free( List **list, int	(*del)() )
#else
int list_free( list, del )
List	**list ;
int	(*del)() ;
#endif
{
    List	*l ;

    if ( !list ) {
	fprintf(stderr,"Null value in function list_free for parameter list\n" ) ;
	return FALSE ;
    } /* then */
    if ( !del ) {
	fprintf(stderr,"Null value in function list_free for parameter delete_funktion\n" ) ;
	return FALSE ;
    } /* then */
    if ( !(l = *list) )
	return TRUE ;
    while ( l ) {
	*list = l->next ;
	del( l->elem ) ;
	efree( l ) ;
	l = *list ;
    } /* while */
    return TRUE ;
} /* list_free */


/**********************
list_kill     ##e 	list_free + loeschen des Verwaltungselementes     
Datum:  10.6.92
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
list_free laesst eine leere Liste zurueck, d.h. der Kopf einer leeren
Liste ( das Verwaltungselement ) besteht noch. Dies wird mit list_kill
ebenfalls freigegeben.
Fazit: nach list_kill muss ein list_new erfolgen, nach list_free nicht.
**********************/
#ifndef CC_COMP
int list_kill( List **list, int	(*del)() )
#else
int list_kill( list, del )
List	**list ;
int	(*del)() ;
#endif
{
    list_free( list, del ) ;
    efree( list ) ;
    return TRUE ;
} /* list_kill */


/**********************
list_find      ##e	Element in unsortierter Liste finden und ausgeben
Datum:  08.11.90
Autor:  Peter Kemper
Input:  list	Liste, in der gesucht werden soll
	adr	Adresse des gefundenen Elementes
	key	Suchkriterium fuer gesuchtes Element
	comp	Suchfunktion mit Aufruf (*comp)( key, element )
Output:	TRUE	Element gefunden
	FALSE	sonst
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_find( List **list, void **adr, void *key, int	(*comp)() )
#else
int list_find( list, adr, key, comp )
     List 	**list ;
     void	**adr ;
     void	*key ;
     int	(*comp)() ;
#endif
{
    List	*l ;

    if ( !list ) {
	fprintf(stderr,"Null value in function list_find for parameter list\n" ) ;
	return FALSE ;
    } /* then */
    if ( !adr ) {
	fprintf(stderr,"Null value in function list_find for parameter adr\n" ) ;
	return FALSE ;
    } /* then */
    if ( !key ) {
	fprintf(stderr,"Null value in function list_find for parameter key\n" ) ;
	return FALSE ;
    } /* then */
    if ( !comp ) {
	fprintf(stderr,"Null value in function list_find for parameter compare_funktion\n" ) ;
	return FALSE ;
    } /* then */
    if ( !( l = *list) ) 		/* Liste leer */
	return FALSE ;
    while ( l ) {
	if ( !( (*comp)( key, l->elem ) ) ) {	/* Element gefunden */
	    *adr = l->elem ;
	    return TRUE ;
	} /* then */
	l = l->next ;
    } /* while */
    return FALSE ;			/* Suche erfolglos */
} /* list_find */



/**********************
list_insert      ##e    Einfuegen in eine sortierte Liste  
Datum:  17.12.90
Autor:  Peter Kemper
Input:  list	Liste, in die eingefuegt werden soll
	adr	Zeiger auf Element, falls einzufuegendes Element schon vorhanden
	key	Sortierkriterium des einzufuegenden Elementes
	elem	einzufuegendes Element
	comp	Vergleichsfunktion mit Aufruf comp( key, element )
Output: TRUE	Element erfolgreich eingefuegt
	FALSE	sonst ( insbesondere bei Mehrfacheinfuegversuchen )
Side:	
Description:
Sofern das einzufuegende Element schon vorhanden ist, wird das einzufuegende
Element nicht eingefuegt, ueber adr ein Zeiger auf das gefundene Element aus-
gegeben und der Returnwert ist FALSE !
**********************/
#ifndef CC_COMP
int list_insert( List **list, void **adr, void *key, void *elem, int	(*comp)() )
#else
int list_insert( list, adr, key, elem, comp )
     List	**list ;
     void	**adr ;
     void	*key ;
     void	*elem ;
     int	(*comp)() ;
#endif
{
    List	**l ;
    List	*neu ;
    int		i ;

    if ( !( l = list ) ) {
	fprintf(stderr,"Null value in function list_insert for parameter list\n" ) ;
	return FALSE ;
    } /* then */
    if ( !key ) {
	fprintf(stderr,"Null value in function list_insert for parameter key\n" ) ;
	return FALSE ;
    } /* then */
    if ( !elem ) {
	fprintf(stderr,"Null value in function list_insert for parameter elem\n" ) ;
	return FALSE ;
    } /* then */
    if ( !comp ) {
	fprintf(stderr,"Null value in function list_insert for parameter compare_funktion\n" ) ;
	return FALSE ;
    } /* then */

    while ( *l ) {
	i = (*comp)( key, (*l)->elem ) ;

/* neue Version Vergleichsfunktion liefert Werte <0, =0 oder >0 */
	if (0 > i) 
	{
	    neu = l_neu( elem ) ;	/* neues Element vor elem einfuegen */
	    neu->next = *l ;
	    *l = neu ;
	    return TRUE ;	  
	  }
	if (0 == i) {
	    *adr = (*l)->elem ;
	    return FALSE ;		/* Element schon vorhanden */
	  }    
	if (0 < i) 
	{
	  l = &((*l)->next) ;		/* Zeiger weitersetzen */
	}
	
/* alte Version, Vergleichsfunktion liefert -1,0 oder 1 
	switch ( i ) {
	case -1 :
	    neu = l_neu( elem ) ;	
	    neu->next = *l ;
	    *l = neu ;
	    return TRUE ;
	    break ;
	case 0 :
	    *adr = (*l)->elem ;
	    return FALSE ;		
	    break ;
	case 1 :
	    l = &((*l)->next) ;		
	    break ;
	default :
	    return FALSE ;
	} 
*/
    } /* while */
    *l = l_neu( elem ) ;	/* Einfuegen in leere Liste oder Listenende */
    return TRUE ;
} /* list_insert */



/**********************
list_append     ##e	Element an unsortierte Liste anhaengen
Datum:  08.11.90
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_append( List **list, void *elem)
#else
int list_append( list, elem)
     List	**list ;
     void	*elem ;
#endif
{
    List	**l ;

    if ( !( l = list ) ) {
	fprintf(stderr,"Null value in function list_append for parameter list\n" ) ;
	return FALSE ;
    } /* then */
    if ( !elem ) {
	fprintf(stderr,"Null value in function list_append for parameter elem\n" ) ;
	return FALSE ; 
    } /* then */

    /* Correction 17.3.2015 PB */
    if (!(*l))
      *l = l_neu( elem ) ;
    else {
      while ((*l)->next)
	l = &((*l)->next) ;
      (*l)->next = l_neu( elem ) ;
    }
      /* Old version */
    /*  while ( *l ) */		/* Zeiger bis ans Ende der Liste setzen */
    /*	l = &((*l)->next) ; */
    /* *l = l_neu( elem ) ; */	/* Element anhaengen */
    return TRUE ;
} /* list_append */



/**********************
list_prepend      ##e	Element am Anfang einer unsortierten Liste einfuegen
Datum:  08.11.90
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_prepend( List **list, void *elem )
#else
int list_prepend( list, elem )
     List	**list ;
     void	*elem ;
#endif
{
    List	*l ;

    if ( !list ) {
	fprintf(stderr,"Null value in function list_prepend for parameter list\n" ) ;
	return FALSE ;
    } /* then */
    if ( !elem ) {
	fprintf(stderr,"Null value in function list_prepend for parameter elem\n" ) ;
	return FALSE ;
    } /* then */

    l = l_neu( elem ) ;
    l->next = *list ;
    *list = l ;
    return TRUE ;
} /* list_prepend */



/**********************
list_first      ##e	erstes Element der Liste ausgeben ohne auszufuegen
Datum:  08.11.90
Autor:  Peter Kemper
Input:  
Output: Zeiger auf Inhalt des ersten Elementes, falls Element vorhanden
	NULL sonst
Side:	
Description:
**********************/
#ifndef CC_COMP
void	* list_first( List **list)
#else
void	* list_first( list)
     List **list ;
#endif
{
    if ( !list ) {
	fprintf(stderr,"Null value in function list_first for parameter list\n" ) ;
	return NULL ;
    } /* then */
    if ( !(*list) ) 
	return NULL ;
    return (*list)->elem ;
} /* list_first */




/**********************
list_next      ##e	nachfolgendes Element in der Liste
Datum:  08.11.90
Autor:  Peter Kemper
Input:  Zeiger auf Liste
Output: Zeiger auf Nachfolger falls vorhanden
	NULL	sonst
Side:		
Description:
**********************/
#if LIST_PREPROC
#else
#ifndef CC_COMP
List	** list_next( List **list )
#else
List	** list_next( list )
     List **list ;
#endif
{
    if ( !list ) {
	fprintf(stderr,"Null value in function list_next for parameter list\n" ) ;
	return NULL ;
    } /* then */
    if ( !*list ) 
	return NULL ;
    if ( !( (*list)->next ) )
	return NULL ;
    return &((*list)->next) ;
} /* list_next */
#endif


/**********************
list_delete      ##e	Loeschen eines Elementes in der unsortierten Liste 
Datum:  08.11.90
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_delete( List **list, void *key, int	(*del)(), int	(*comp)() )
#else
int list_delete( list, key, del, comp )
     List	**list ;
     void	*key ;
     int	(*del)() ;
     int	(*comp)() ;
#endif
{
    List	**l ;
    List	*loesch ;

    if ( !( l = list ) ) {
	fprintf(stderr,"Null value in function list_delete for parameter list\n" ) ;
	return FALSE ;
    } /* then */
    if ( !key ) {
	fprintf(stderr,"Null value in function list_delete for parameter key\n" ) ;
	return FALSE ;
    } /* then */
    if ( !del ) {
	fprintf(stderr,"Null value in function list_delete for parameter delete_funktion\n" ) ;
	return FALSE ;
    } /* then */
    if ( !comp ) {
	fprintf(stderr,"Null value in function list_delete for parameter compare_funktion\n" ) ;
	return FALSE ;
    } /* then */

    while ( *l ) {
	if ( !( (*comp)( key, (*l)->elem ) ) ) {
	    loesch = *l ;		/* Element gefunden */
	    *l = (*l)->next ;		/* Element ausfuegen */
	    (*del)( loesch->elem ) ;
	    efree( loesch ) ;
	    return TRUE ;
	} /* then */
	l = &((*l)->next) ; 		/* Zeiger weitersetzen */
    } /* while */
    return FALSE ;
} /* list_delete */



/**********************
list_cont      ##e	Inhalt eines Listenelementes ausgeben 
Datum:  08.11.90
Autor:  Peter Kemper
Input:  Zeiger auf Liste  
Output: Zeiger auf verwaltetes Listenelement, falls vorhanden
	NULL	sonst
Side:	
Description:
**********************/
#if LIST_PREPROC
#else
#ifndef CC_COMP
void	* list_cont( List **list)
#else
void	* list_cont( list)
List **list ;
#endif
{
    if ( !list ) {
/*	fprintf(stderr,"Null value in function list_cont for parameter list\n" ) ; */
	return NULL ;
    } /* then */
    if ( !*list ) {
/*	wegen nerviger und ueberfluessiger Fehlermeldung auskommentiert
	fprintf(stderr,"Null value in function list_cont for parameter list\n" ) ;
*/
	return NULL ;
    } /* then */
    return (*list)->elem ;
} /* list_cont */
#endif

/**********************
list_empty      ##e	Liste leer ? -> TRUE/FALSE
Datum:  17.12.90
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#if LIST_PREPROC
#else
#ifndef CC_COMP
int list_empty( List **list ) 
#else
int list_empty( list ) 
     List **list ;
#endif
{
    if ( !list ) {
/* wegen Benutzung als Schleifenabbruchkriterum ist auch dieser Fall ok 
	fprintf(stderr,"Null value in function list_empty for parameter list\n" ) ;
*/
	return TRUE ;
    } /* then */
    if ( !*list ) 
	return TRUE ;
    else 
	return FALSE ;
} /* list_empty */
#endif


/**********************
list_print      ##e	Bildschirmausgabe einer Liste
Datum:  17.12.90
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_print( List **list, int (*print)() ) 
#else
int list_print( list, print ) 
     List	**list ;
     int	(*print)() ;
#endif
{
    List	*l ;

    if ( !list ) {
	fprintf(stderr,"Null value in function list_print for parameter list\n" ) ;
	return TRUE ;
    } /* then */
    if ( !print ) {
	fprintf(stderr,"Null value in function list_print for parameter print\n" ) ;
	return TRUE ;
    } /* then */
    l = *list ;
    if ( !l )
	fprintf(stderr,"Null value in function list_print for parameter Liste ist leer \n" ) ; 
    while ( l ) {
	(*print)( l->elem ) ;
	l = l->next ;
    } /* while */
    return TRUE ;
} /* list_print */


/**********************
list_file_print      ##e	Dateiausgabe einer Liste
Datum:  10.2.95
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_file_print( List **list, FILE *fp, int (*print)() ) 
#else
int list_file_print( list, fp, print ) 
     List	**list ;
     FILE    *fp ; 
     int	(*print)() ;
#endif
{
    List	*l ;

    if ( !list ) {
	fprintf(stderr,"Null value in function list_file_print for parameter list\n" ) ;
	return TRUE ;
    } /* then */
    if ( !fp ) {
	fprintf(stderr,"Null value in function list_file_print for parameter fp\n" ) ;
	return TRUE ;
    } /* then */    
    if ( !print ) {
	fprintf(stderr,"Null value in function list_file_print for parameter print\n" ) ;
	return TRUE ;
    } /* then */
    l = *list ;
    if ( !l )
	fprintf(stderr,"Null value in function list_file_print for parameter Liste ist leer \n" ) ; 
    while ( l ) {
	(*print)( fp, l->elem ) ;
	l = l->next ;
    } /* while */
    return TRUE ;
} /* list_file_print */


/**********************
list_delete_forall ##e	Loeschen bestimmter Elemente in der Liste 
Datum:  08.11.90
Autor:  Peter Kemper
Input:  
Output: 
Side:	
Description:
**********************/
#ifndef CC_COMP
int list_delete_forall( List **list, void *key, int	(*del)(), int	(*comp)() )
#else
int list_delete_forall( list, key, del, comp )
     List	**list ;
     void	*key ;
     int	(*del)() ;
     int	(*comp)() ;
#endif
{
    List	**l ;
    List	*loesch ;

    if ( !( l = list ) ) {
	fprintf(stderr,"Null value in function list_delete_forall for parameter list\n" ) ;
	return FALSE ;
    } /* then */
    if ( !key ) {
	fprintf(stderr,"Null value in function list_delete_forall for parameter key\n" ) ;
	return FALSE ;
    } /* then */
    if ( !del ) {
	fprintf(stderr,"Null value in function list_delete_forall for parameter delete_funktion\n" ) ;
	return FALSE ;
    } /* then */
    if ( !comp ) {
	fprintf(stderr,"Null value in function list_delete_forall for parameter compare_funktion\n" ) ;
	return FALSE ;
    } /* then */

    while ( *l ) {
	if ( !( (*comp)( key, (*l)->elem ) ) ) {
	    loesch = *l ;		/* Element gefunden */
	    *l = (*l)->next ;		/* Element ausfuegen */
	    (*del)( loesch->elem ) ;
	    efree( loesch ) ;
	} /* then */
	else
	    l = &((*l)->next) ; 		/* Zeiger weitersetzen */
    } /* while */
    return TRUE ;
} /* list_delete_forall */



List **list_remove_elem(List **list, void *key, int (*comp)())
{
  List **list_pre, **list_first ;

  if ( !( list_pre = list ) ) {
    fprintf(stderr,"Null value in function list_remove_elem for parameter list\n" ) ;
    exit(-1) ;
  } /* then */
  if ( !key ) {
    fprintf(stderr,"Null value in function list_remove_elem for parameter key\n" ) ;
    exit(-1) ;
  } /* then */
  if ( !comp ) {
    fprintf(stderr,"Null value in function list_remove_elem for parameter compare_funktion\n" ) ;
    exit(-1) ;
  } /* then */
  list_first = list ;
  if (!( (*comp)( key, (*list)->elem))) {
    list_first = list_next(list) ;
    return(list_first) ;
  }
  list = list_next(list) ;
  while (list) {
    if (!( (*comp)( key, (*list)->elem))) {
      (*list_pre)->next = (*list)->next ;
      return(list_first) ;
    }
    else {
      list_pre = list ;
      list = list_next(list) ;
    }
  } 
  return(list_first) ;
} /* list_remove_elem */


/**
 * Liste in sortierte Liste transformieren.
 * Achtung die Listeneintraege werden NICHT kopiert, dh
 * bei copylist == TRUE entsteht eine zusaetzliche Liste mit Verweisen
 * auf die bisherigen Listeneintraege.
 * \date 1.10.99
 * \author Peter Kemper  
 * \param list Liste die sortiert werden soll
 * \param comp Vergleichsfunktion
 * \param copylist TRUE alte Listenstruktur wird beibehalten. FALSE alte Listenstruktur wird abgebaut.
 * \return sortierte Liste
 */
List **list_sort(List **list, int (*comp)(), short copylist)
{
  List **l= list ;
  List **new = list_new() ;
  void *elem = NULL ;
  while (!list_empty(l))
    {
      if (!list_insert(new,&elem,list_cont(l), list_cont(l),comp))
	fprintf(stderr,"WARNING: list_sort drops duplicate element\n") ;
      l = list_next(l) ;
    }

  if (!copylist)
    list_kill(list,list_free_delete_dummy) ;

  return(new) ;
}


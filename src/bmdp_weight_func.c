#include <stddef.h>
#include "bmdp_weight_func.h"
#include "bmdp_func.h"
#include "bmdp_const.h"
#include "bmdp_pareto_func.h"
#include "ctmdp_func.h"
#include "utils.h"
#include <glpk.h>
#include <setoper.h>
/* #define GMPRATIONAL */
#include <cdd.h>
/* #include <scip/scip.h> */

#define FX_CONSTRAINT 0

/**********************
bmdp_compute_weight_case
Datum:  25.03.15
Autor:  Dimitri Scheftelowitsch
Input:  mdp_min, mdp_max Bounding MDP matrices
        rew reward vector
        ind politics
        max_iter, local maximal number of global/local iterations
        ti_max, local_it maximal local/global solution time
        epsilon, local_eps epsilon global/local
        method solution method if size > BMDP_SIZE_DIRECT
        gamma discount factor
        min_weight weight of the worst case
        max_weight weight of the best case
Output: return value gain vector
        ind optimal policy
Side-Effects:
Description:
The function computes a weighted worst-best-average case gain vector and policy for a BMDP.
***********************************************************************/

gain_vectors_t bmdp_compute_weight_case(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
        Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max, size_t *ind,
        int max_iter, double ti_max, double epsilon, int local_it, double local_eps, int method, double gamma,
        double min_weight, double max_weight, short comp_max, short comp_stat) {
    /* initialize stopping conditions */
    short stop = FALSE, found = FALSE;
    double avg_weight = 1.0 - min_weight - max_weight;
    short condition = (min_weight >= -BMDP_LO_EPS) && (max_weight >= -BMDP_LO_EPS) && (avg_weight >= -BMDP_LO_EPS);
    gain_vectors_t triple;
    triple.gain_min = triple.gain_avg = triple.gain_max = NULL;

    if (!condition) {
        printf("Parameters out of bounds: min_weight is %f, max_weight is %f, avg_weight id %f\n",
                min_weight, max_weight, avg_weight);
        return triple;
    }

    short dynamic = FALSE;
    if (local_eps < 0.0) {
        dynamic = TRUE;
        local_eps = 10.0 * epsilon;
    }

    /* initialization */
    Sparse_matrix *pp_max = bmdp_gen_precond_structure(mdp_max);
    Sparse_matrix *pp_avg = bmdp_gen_precond_structure(mdp_avg);
    Sparse_matrix *pp_min = bmdp_gen_precond_structure(mdp_min);

    double *hh_min;
    double *hh_max;
    double *hh_avg;

    Double_vektor *b_min = mat_double_vektor_new(mdp_max->ord);
    Double_vektor *b_avg = mat_double_vektor_new(mdp_max->ord);
    Double_vektor *b_max = mat_double_vektor_new(mdp_max->ord);
    double *vv_min = (double *) ecalloc(pp_avg->ord, sizeof(double));
    double *vv_max = (double *) ecalloc(pp_avg->ord, sizeof(double));


    /* start iteration */
    double starttime = mat_timeused();

    bmdp_initial_policy(ind, mdp_avg);
    double **policy;

    hh_avg = bmdp_evaluate_avg_policy(mdp_avg, ind, rew_avg, gamma, FALSE);
    hh_min = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, ind, rew_min, gamma, TRUE, FALSE);
    hh_max = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, ind, rew_max, gamma, FALSE, FALSE);
    double *hh = (double *) ecalloc(mdp_avg->ord, sizeof(double));
    double *temp_hh[3] = {hh_min, hh_avg, hh_max};

    double val = 0.0;
    size_t i;

    for (i = 0; i < mdp_max->ord; i++) {
        val += hh_min[i];
    }

    /* stopping conditions etc. */
    int iteration = 0;
    double last_difference = comp_max ? (-DBL_MAX) : DBL_MAX;

    /* already seen policies */
    size_t bloom_set_size = 134217728; /* 2^{27} */
    bloom_filter bloom_set;

    const size_t ord = mdp_avg->ord;

    init_hash_functions(ord);
    bloom_set = bloom_filter_new(num_hash_functions, hash_functions, bloom_set_size);

#undef WEIGHT_LP_ITERATION
#ifdef WEIGHT_LP_ITERATION
    /*
    policy = bmdp_lp_iteration(mdp_min, mdp_avg, mdp_max, ind, rew_min, rew_avg, rew_max,
                               hh_min, hh_max, vv_min, vv_max, gamma, min_weight, max_weight, comp_max);
    */
    policy = bmdp_lp_max_w_iteration(mdp_min, mdp_avg, mdp_max, ind, rew_min, rew_avg, rew_max,
                               hh_min, hh_max, vv_min, vv_max, gamma, min_weight, max_weight, comp_max);
#else
    while(!stop) {
        double difference;
        difference = bmdp_alternative_goal_value_iteration(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max,
                                                           vv_min, vv_max, temp_hh, ind, hh, gamma,
                                                           min_weight, max_weight, comp_max);

        double delta = bmdp_weight_error_bound(0, difference, gamma);
        if(delta < epsilon) {
            stop = TRUE;
        }
        last_difference = difference;

        if (iteration >= max_iter /* || mat_timeused() - starttime > ti_max */) {
            stop = TRUE;
        }
        ++iteration;
    }
#endif /* WEIGHT_LP_ITERATION */

    /* cleanup */
    mat_double_vektor_free(b_min);
    mat_double_vektor_free(b_avg);
    mat_double_vektor_free(b_max);
    efree(vv_min);
    efree(vv_max);

    mat_sparse_matrix_free(pp_min);
    mat_sparse_matrix_free(pp_avg);
    mat_sparse_matrix_free(pp_max);

    triple.gain_min = hh_min;
    triple.gain_avg = hh_avg;
    triple.gain_max = hh_max;

    return triple;
}

double bmdp_weight_error_bound(double last_goal_value, double current_goal_value, double gamma) {
    return 2 * fabs(last_goal_value - current_goal_value) * gamma / (1 - gamma);
}

double bmdp_alternative_goal_value_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                             Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                             double *vv_min, double *vv_max, double **new_hh,
                                             size_t *ind, double *hh, double gamma,
                                             double weight_min, double weight_max, short comp_max) {
    /*
     * An alternative weighted problem. We assume that IN EACH TRANSITION, the probability of switching according to the upper bound is weight_max etc.
     * This is easier and works with a single value vector.
     */
    int i = 0, j = 0, a = 0, s = 0;
    const int ord = mdp_avg->ord;
    const double weight_avg = (1.0 - weight_min - weight_max);
    double weight[3] = {weight_min, weight_avg, weight_max};
    Double_vektor **rew[3] = {rew_min, rew_avg, rew_max};
    int *num_actions = ecalloc(ord, sizeof(int));
    double *hh_triple[3] = {hh, hh, hh};
    double max_value = -DBL_MAX;

    Row ***rows[3];
    double **diags[3];
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        rows[i] = ecalloc(ord, sizeof(Row **));
        diags[i] = ecalloc(ord, sizeof(double *));
        for(s = 0; s < ord; ++s) {
            num_actions[s] = mdp_avg->rows[s]->pord;
            rows[i][s] = ecalloc(num_actions[s], sizeof(Row *));
            diags[i][s] = ecalloc(num_actions[s], sizeof(double));

            for(a = 0; a < num_actions[s]; ++a) {
                rows[i][s][a] = mat_row_new(ord);
            }
        }
    }
    update_rows_and_diags(mdp_min, mdp_avg, mdp_max, rows, diags, hh_triple, vv_min, vv_max);

    /* local optimization, update policy */
    for(s = 0; s < ord; ++s) {
        double opt_value = comp_max ? -DBL_MAX : DBL_MAX;
        for(a = 0; a < num_actions[s]; ++a) {
            double value = 0.0;
            double v[3];
            for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                v[i] = rew[i][a]->vektor[s] + gamma * diags[i][s][a] * hh[s];
                for(j = 0; j < rows[i][s][a]->no_of_entries; ++j) {
                    v[i] += gamma * rows[i][s][a]->val[j] * hh[rows[i][s][a]->colind[j]];
                }
                value += weight[i] * v[i];
            }
            short more_optimal = (value > opt_value) == comp_max;
            if(more_optimal) {
                ind[s] = a;
                opt_value = value;
                for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                    new_hh[i][s] = v[i];
                }
            }
        }
    }

    for(s = 0; s < ord; ++s) {
        for(a = 0; a < num_actions[s]; ++a) {
            for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                mat_row_free(rows[i][s][a]);
            }
        }
        double new_value = 0.0;
        for(i = MIN_IDX; i <= MAX_IDX; ++i) {
            efree(rows[i][s]);
            efree(diags[i][s]);

            new_value += new_hh[i][s] * weight[i];
        }
        double delta = fabs(hh[s] - new_value);
        max_value = fmax(delta, max_value);
        hh[s] = new_value;
    }
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        efree(rows[i]);
        efree(diags[i]);
    }
    efree(num_actions);

    return max_value;
}

double bmdp_weight_policy_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                    size_t *ind, double **policy,
                                    Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                    bloom_filter bloom_set, double gamma, double weight_min, double weight_max, short comp_max) {
    /* An alternative approach to the weighted problem. Actually, a policy iteration. */
    size_t i = 0, j = 0, a = 0, s = 0;
    const size_t ord = mdp_avg->ord;
    const double weight_avg = (1.0 - weight_min - weight_max);
    double weight[3] = {weight_min, weight_avg, weight_max};
    size_t s_star, a_star;
    /* we shall need a bloom filter-like structure */

    /* iterate over the states and acctions and look if there is something better. N'est-ce pas? */
    double max_value = -DBL_MAX;
    if(!comp_max) {
        max_value = DBL_MAX;
    }
    for(s = 0; s < ord; ++s) {
        const size_t num_actions = mdp_avg->rows[i]->pord;
        const size_t old_action = ind[s];
        for(a = 0; a < num_actions; ++a) {
            ind[s] = a;
            short already_seen = bloom_filter_query(bloom_set, ind, ord);
            if(already_seen) {
                ind[s] = old_action;
                continue;
            } else {
                bloom_filter_add(bloom_set, ind, ord);
            }
            /* evaluate policies */
            double value = 0.0;
            /* compute the value */
            /* let's consider the sum first, then the max-norm */
            for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                double *hh;
                if(i == MIN_IDX) {
                    hh = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, ind, rew_min, gamma, TRUE, FALSE);
                } else if(i == AVG_IDX) {
                    hh = bmdp_evaluate_avg_policy(mdp_avg, ind, rew_avg, gamma, FALSE);
                } else {
                    hh = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, ind, rew_max, gamma, FALSE, FALSE);
                }
                for(j = 0; j < ord; ++j) {
                    value += hh[j] * weight[i];
                }
                efree(hh);
            }
            value /= ord;
            if(value > max_value) {
                s_star = s;
                a_star = a;
                max_value = value;
            }
        }
        ind[s] = old_action;
        if(s % 40 == 0) {
            printf("%zu/%zu…\n", s, ord);
        }
    }
    ind[s_star] = a_star;
    return max_value;
}

double **bmdp_lp_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                           size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                           double *hh_min, double *hh_max, double *vv_min, double *vv_max,
                           double gamma, double weight_min, double weight_max, short comp_max) {
    /*
     * use the idea from the discussion on 22.2.2016 with F. Bökler / D. Kurz
     *
     * In short, we shall consider the dual LP formulation. We know that the primal problem is a cone,
     * hence, the dual must be a cone, too. Then we consider the (nonlinear) problem for $k$ MDPs: We seek
     * for frequency vectors that are equivalent up to a common factor. This yields several cases:
     *
     * Case 1. The frequency vectors are equivalent. Then the problem is a linear program with the intersection
     * of the cones as constraint.
     *
     * Case 2. The frequency vectors are not equivalent. Then we can improve our solution by going into the
     * direction of the optimal solution for some of the MDPs. For at least one MDP, the gradient will be
     * positive. This case can be generalized to $k$ MDPs by taking the intersection of $l < k$ dual LPs.
     */

    double max_value, delta = DBL_MAX;
    size_t i = 0, j = 0, s = 0, a = 0;
    const size_t ord = mdp_avg->ord;
    size_t *pi_temp = ecalloc(ord, sizeof(size_t));

    const double weight_avg = (1.0 - weight_min - weight_max);
    double weight[3] = {weight_min, weight_avg, weight_max};
    Double_vektor **rew[3] = {rew_min, rew_avg, rew_max};
    double *hh[3];
    hh[MIN_IDX] = hh_min;
    hh[MAX_IDX] = hh_max;
    hh[AVG_IDX] = (double *) ecalloc(ord, sizeof(double));

    double **policy = (double **) ecalloc(ord, sizeof(double *));
    double **temp_policy = (double **) ecalloc(ord, sizeof(double *));

    /* Case 2: An optimal solution for one of the MDPs */
    max_value = bmdp_lp_iteration_case2(mdp_min, mdp_avg, mdp_max, ind, rew_min, rew_avg, rew_max, gamma, weight, comp_max);
    for(s = 0; s < ord; ++s) {
        policy[s] = (double *) ecalloc(mdp_avg->rows[s]->pord, sizeof(double));
        policy[s][ind[s]] = 1.0;
    }

    /* Case 1b: An intersection of two MDPs */
    size_t *num_actions = ecalloc(ord, sizeof(int));

    /* do some preprocessing */
    Row ***rows[3];
    double **diags[3];
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        rows[i] = ecalloc(ord, sizeof(Row **));
        diags[i] = ecalloc(ord, sizeof(double *));
        for(s = 0; s < ord; ++s) {
            num_actions[s] = mdp_avg->rows[s]->pord;
            rows[i][s] = ecalloc(num_actions[s], sizeof(Row *));
            diags[i][s] = ecalloc(num_actions[s], sizeof(double));
            if(!i) {
                policy[s] = (double *) ecalloc(num_actions[s], sizeof(double));
                temp_policy[s] = (double *) ecalloc(num_actions[s], sizeof(double));
            }

            for(a = 0; a < num_actions[s]; ++a) {
                rows[i][s][a] = mat_row_new(ord);
            }
        }
    }
    update_rows_and_diags(mdp_min, mdp_avg, mdp_max, rows, diags, hh, vv_min, vv_max);

    /* create indexing structure that translates state and action indices to the variable index in the LP */
    int **x = ecalloc(ord, sizeof(int *));
    int row_index = 1;

    for(s = 0; s < ord; ++s) {
        x[s] = ecalloc(num_actions[s], sizeof(int));
        for(a = 0; a < num_actions[s]; ++a) {
            x[s][a] = row_index++;
        }
    }

    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        /* iterate until fixed point */
        delta = DBL_MAX;
        while(delta > BMDP_LO_EPS) {
            delta = bmdp_lp_iteration_case1b(mdp_min, mdp_avg, mdp_max, temp_policy, hh, rew, gamma, weight, comp_max, i, x, num_actions, rows, diags);
            compute_extreme_rows_and_diags(mdp_min, mdp_avg, mdp_max, rows, diags, hh, vv_min, vv_max, temp_policy, gamma, rew);
        }
        /* evaluate policies */
        double value = 0.0;

        const int left_out_index = (i + 2) % 3;
        double *hh_temp;
        if(left_out_index == AVG_IDX) {
            hh_temp = bmdp_evaluate_avg_stationary_policy(mdp_avg, temp_policy, rew_avg, gamma, FALSE);
        } else {
            short compmin = (left_out_index == MIN_IDX);
            hh_temp = bmdp_evaluate_minmax_stationary_policy(mdp_min, mdp_max, rew[left_out_index], gamma, temp_policy, compmin);
        }
        for(s = 0; s < ord; ++s) {
            hh[left_out_index][s] = hh_temp[s];
        }
        efree(hh_temp);

        for(s = 0; s < ord; ++s) {
            for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                value += hh[i][s];
            }
        }

        if(value > max_value) {
            max_value = value;
            for(s = 0; s < ord; ++s) {
                for(a = 0; a < num_actions[s]; ++a) {
                    policy[s][a] = temp_policy[s][a];
                }
            }
        }
    }

    /* Case 1a: An intersection of all three MDPs */
    delta = DBL_MAX;
    while(delta > BMDP_LO_EPS) {
        delta = bmdp_lp_iteration_case1a(mdp_min, mdp_avg, mdp_max, temp_policy, hh, rew, gamma, weight, comp_max, x, num_actions, rows, diags);
        compute_extreme_rows_and_diags(mdp_min, mdp_avg, mdp_max, rows, diags, hh, vv_min, vv_max, temp_policy, gamma, rew);
    }
    double value = 0.0;
    for(s = 0; s < ord; ++s) {
        for(i = MIN_IDX; i <= MAX_IDX; ++i) {
            value += hh[i][s];
        }
    }

    if(value > max_value) {
        max_value = value;
        for(s = 0; s < ord; ++s) {
            for(a = 0; a < num_actions[s]; ++a) {
                policy[s][a] = temp_policy[s][a];
            }
        }
    }
    /* cleanup */
    efree(pi_temp);
    for(s = 0; s < ord; ++s) {
        for(a = 0; a < num_actions[s]; ++a) {
            for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                mat_row_free(rows[i][s][a]);
            }
        }
        efree(x[s]);
        for(i = MIN_IDX; i <= MAX_IDX; ++i) {
            efree(rows[i][s]);
            efree(diags[i][s]);
        }
        efree(temp_policy[s]);
    }
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        efree(rows[i]);
        efree(diags[i]);
    }
    efree(x);
    efree(num_actions);
    efree(pi_temp);
    efree(temp_policy);

    return policy;
}

double **bmdp_lp_max_w_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                 size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                 double *hh_min, double *hh_max, double *vv_min, double *vv_max,
                                 double gamma, double weight_min, double weight_max, short comp_max) {
    /*
     * This algorithm uses the same idea as \ref bmdp_lp_iteration. However, we use the algorithm described in the paper.
     *
     * Our idea, hence, will be a glorified policy iteration
     */

    /* initialization */
    size_t i = 0, j = 0, s = 0, a = 0;
    const size_t ord = mdp_avg->ord;

    double delta = DBL_MAX, value, old_value = DBL_MAX;

    const double weight_avg = (1.0 - weight_min - weight_max);
    double weight[3] = {weight_min, weight_avg, weight_max};
    Double_vektor **rew[3] = {rew_min, rew_avg, rew_max};
    double *hh[3];
    hh[MIN_IDX] = hh_min;
    hh[MAX_IDX] = hh_max;
    hh[AVG_IDX] = (double *) ecalloc(ord, sizeof(double));

    double **policy = (double **) ecalloc(ord, sizeof(double *));

    for(s = 0; s < ord; ++s) {
        policy[s] = (double *) ecalloc(mdp_avg->rows[s]->pord, sizeof(double));
    }

    size_t *num_actions = ecalloc(ord, sizeof(int));

    /* do some preprocessing */
    Row ***rows[3];
    double **diags[3];
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        rows[i] = ecalloc(ord, sizeof(Row **));
        diags[i] = ecalloc(ord, sizeof(double *));
        for(s = 0; s < ord; ++s) {
            num_actions[s] = mdp_avg->rows[s]->pord;
            rows[i][s] = ecalloc(num_actions[s], sizeof(Row *));
            diags[i][s] = ecalloc(num_actions[s], sizeof(double));
            if(!i) {
                policy[s] = (double *) ecalloc(num_actions[s], sizeof(double));
            }

            for(a = 0; a < num_actions[s]; ++a) {
                rows[i][s][a] = mat_row_new(ord);
            }
        }
    }
    update_rows_and_diags(mdp_min, mdp_avg, mdp_max, rows, diags, hh, vv_min, vv_max);

    /* create indexing structure that translates state and action indices to the variable index in the LP */
    int **x = ecalloc(ord, sizeof(int *));
    int row_index = 1;

    for(s = 0; s < ord; ++s) {
        x[s] = ecalloc(num_actions[s], sizeof(int));
        for(a = 0; a < num_actions[s]; ++a) {
            x[s][a] = row_index++;
        }
    }

    /* iterate until fixed point is reached */
    while(delta > BMDP_LO_EPS) {
        value = compute_optimal_weight_policy(3, mdp_avg, rows, diags, rew, weight, num_actions, x, hh, gamma, policy, FALSE, TRUE);
        compute_extreme_rows_and_diags(mdp_min, mdp_avg, mdp_max, rows, diags, hh, vv_min, vv_max, policy, gamma, rew);
        double error = bmdp_weight_error_bound(old_value, value, gamma);
        delta = fabs(value - old_value);
        old_value = value;
    }

    short pure = TRUE;
    for(s = 0; s < ord; ++s) {
        for(a = 0; a < num_actions[s]; ++a) {
            if(policy[s][a] > BMDP_LO_EPS && policy[s][a] < 1 - BMDP_LO_EPS) {
                pure = FALSE;
            }
        }
    }
    if(pure) {
        printf("[bmdp_lp_max_w_iteration] A wonder, the policy is pure!\n");
    }

    return policy;
}

double compute_optimal_weight_policy(size_t num_of_mdps, Mdp_matrix *mdp, Row ****rows, double ***diags, Double_vektor ***rew,
                                   double *weight, size_t *num_actions, int **decision_variable_index, double **hh,
                                   double gamma, double **policy, short pure, short comp_max) {
    /* we are going to write a fully general implementation, so lots of code will be written in a generic manner */
    /* iterate over all possible initial distributions */
    const size_t ord = mdp->ord;
    double max_value = comp_max ? -DBL_MAX : DBL_MAX;
    char buf[256];
    double **temp_policy = (double **) ecalloc((unsigned) ord, sizeof(double *));
    size_t s;
    double **temp_hh = (double **) ecalloc((unsigned) num_of_mdps, sizeof(double *));

    for(s = 0; s < ord; ++s) {
        temp_policy[s] = (double *) ecalloc( (unsigned)num_actions[s], sizeof(double));
    }

    /* iterate over possible subset size */
    size_t i;
    for(i = 0; i < num_of_mdps; ++i) {
        const size_t subset_size = (num_of_mdps - i);
        double **initial_distribution = binary_vectors_init(subset_size, ord);
        /* for(; !binary_vectors_at_end(subset_size, ord, initial_distribution); binary_vectors_next(subset_size, ord, initial_distribution)) { */
            /* iterate over all possible subsets of given size */
            int subset = (1 << subset_size) - 1; /* ...1 1 in binary. We shall use Gosper's hack to generate the next subset */
            while(subset < (1 << num_of_mdps)) {
                /* create LP */
                glp_prob *lp = glp_create_prob();
                int problem_dimension = init_lp(ord, num_actions, gamma, lp, buf, decision_variable_index, pure);

                /* extract the MDPs */
                size_t j, s, a;
                for(j = 0; j < num_of_mdps; ++j) {
                    const size_t index = 1 << j;
                    if(subset & index) {
                        add_mdp_constraints(lp, rows[j], diags[j], gamma, mdp, problem_dimension, decision_variable_index, NULL /* initial_distribution[j] */);
                    }
                }
                for(s = 0; s < ord; ++s) {
                    for(a = 0; a < num_actions[s]; ++a) {
                        double coef = 0.0;
                        for(j = 0; j < num_of_mdps; ++j) {
                            const int index = 1 << j;
                            if(subset & index) {
                                const double part = rew[j][a]->vektor[s] * weight[j];
                                coef += part;
                            }
                        }
                        glp_set_obj_coef(lp, decision_variable_index[s][a], coef);
                    }
                }

                /* solve the LP */
                glp_iocp parm;
                glp_init_iocp(&parm);
                parm.presolve = GLP_ON;
                parm.msg_lev = GLP_MSG_ALL;
                parm.fp_heur = GLP_OFF;
                parm.gmi_cuts = GLP_ON;
                parm.mir_cuts = GLP_ON;
                parm.cov_cuts = GLP_ON;
                parm.clq_cuts = GLP_ON;
                parm.mip_gap = 0.0;
                parm.tm_lim = INT_MAX;
                parm.cb_func = NULL;
                parm.cb_info = NULL;
                parm.cb_size = 0;
                parm.binarize = GLP_ON;
                glp_write_lp(lp, NULL, "current.lp");
                glp_intopt(lp, &parm);

                /* retrieve solution */
                int status = glp_mip_status(lp);
                /* we probably have to trust that the status is GLP_OPT at least once */
                double value = glp_mip_obj_val(lp);

                /* first, retrieve policy */
                for(s = 0; s < ord; ++s) {
                    double sum = 0.0;
                    for(a = 0; a < num_actions[s]; ++a) {
                        const double freq = glp_mip_col_val(lp, decision_variable_index[s][a]);
                        sum += freq;
                        temp_policy[s][a] = 0.0;
                    }
                    if(sum > 0) {
                    for(a = 0; a < num_actions[s]; ++a) {
                        const double freq = glp_mip_col_val(lp, decision_variable_index[s][a]);
                            temp_policy[s][a] = freq / sum;
                    }
                    } else {
                        temp_policy[s][0] = 1.0;
                    }
                }

                /* evaluate other MDPs */
                for(j = 0; j < num_of_mdps; ++j) {
                    const int index = 1 << j;
                    if(!(subset & index)) {
                        temp_hh[j] = bmdp_low_level_evaluate_avg_stationary_policy(ord, num_actions, rows[j], diags[j], rew[j], temp_policy, gamma);
                        /* compute maximum norm */
                        double val = temp_hh[j][0];
                        for(s = 0; s < ord; ++s) {
                            val = fmax(val, temp_hh[j][s]);
                        }
                        value += weight[j] * val;
                    }
                }

                short better_solution = (value > max_value && comp_max) || (value < max_value && !comp_max);
                if(better_solution) {
                    max_value = value;
                    /* retrieve values */
                    for(s = 0; s < ord; ++s) {
                        for(j = 0; j < num_of_mdps; ++j) {
                            if((1 << j) & subset) {
                                hh[j][s] = 0.0;
                            } else {
                                hh[j][s] = temp_hh[j][s];
                            }
                        }
                        for(a = 0; a < num_actions[s]; ++a) {
                            policy[s][a] = temp_policy[s][a];
                            const double freq = glp_mip_col_val(lp, decision_variable_index[s][a]);
                            for(j = 0; j < num_of_mdps; ++j) {
                                hh[j][s] += freq * rew[j][a]->vektor[s];
                            }
                        }
                    }
                }

                /* deallocate temporary memory */
                glp_delete_prob(lp);

                for(j = 0; j < num_of_mdps; ++j) {
                    if(!(subset & (1 << j))) {
                        efree(temp_hh[j]);
                    }
                }

                subset = next_subset(subset);
            }
        /* } */
        binary_vectors_remove(subset_size, initial_distribution);
    }

    /* free memory */
    for(s = 0; s < ord; ++s) {
        efree(temp_policy[s]);
    }
    efree(temp_policy);

    return max_value;
}

void compute_extreme_rows_and_diags(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                    Row ****rows, double ***diags, double **hh, double *vv_min, double *vv_max, double **policy,
                                    double gamma, Double_vektor ***rew) {
    double delta = DBL_MAX;
    double *new_hh[3];
    int s, a, i;

    while(delta > BMDP_LO_EPS) {
        delta = 0.0;
        update_rows_and_diags(mdp_min, mdp_avg, mdp_max, rows, diags, hh, vv_min, vv_max);
        new_hh[MIN_IDX] = bmdp_evaluate_minmax_stationary_policy(mdp_min, mdp_max, rew[MIN_IDX], gamma, policy, TRUE);
        new_hh[AVG_IDX] = bmdp_evaluate_avg_stationary_policy(mdp_avg, policy, rew[AVG_IDX], gamma, FALSE);
        new_hh[MAX_IDX] = bmdp_evaluate_minmax_stationary_policy(mdp_min, mdp_max, rew[MAX_IDX], gamma, policy, TRUE);

        for(i = MIN_IDX; i <= MAX_IDX; ++i) {
            for(s = 0; s < mdp_avg->ord; ++s) {
                delta = fmax(delta, fabs(new_hh[i][s] - hh[i][s]));
                hh[i][s] = new_hh[i][s];
            }
            efree(new_hh[i]);
        }
    }
}

void update_rows_and_diags(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                           Row ****rows, double ***diags, double **hh, double *vv_min, double *vv_max) {
    int s, a, i, j;
    const int ord = mdp_avg->ord;
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        for(s = 0; s < ord; ++s) {
            for(a = 0; a < mdp_avg->rows[s]->pord; ++a) {
                if(i == AVG_IDX) {
                    rows[i][s][a]->no_of_entries = mdp_avg->rows[s]->no_of_entries[a];
                    for(j = 0; j < rows[i][s][a]->no_of_entries; ++j) {
                        rows[i][s][a]->val[j] = mdp_avg->rows[s]->values[a][j];
                        rows[i][s][a]->colind[j] = mdp_avg->rows[s]->colind[a][j];
                    }
                    diags[i][s][a] = mdp_avg->rows[s]->diagonals[a];
                } else {
                    short comp_min = (i == MIN_IDX);
                    if(comp_min) {
                        bmdp_set_one_minmax_row(mdp_min->rows[s], mdp_max->rows[s], rows[i][s][a], &(diags[i][s][a]), s, a, hh[i], vv_min, comp_min);
                    } else {
                        bmdp_set_one_minmax_row(mdp_min->rows[s], mdp_max->rows[s], rows[i][s][a], &(diags[i][s][a]), s, a, hh[i], vv_max, comp_min);
                    }
                }
            }
        }
    }
}

int init_lp(size_t ord, size_t *num_actions, double gamma, glp_prob *lp, char *buf, int **x, short pure) {
    size_t s, a;
    int problem_dimension = 0;

    glp_set_obj_dir(lp, GLP_MAX);
    for(s = 0; s < ord; ++s) {
        problem_dimension += num_actions[s];
        for(a = 0; a < num_actions[s]; ++a) {
            glp_add_cols(lp, 1);
            sprintf(buf, X_COL_NAME_PATTERN, s, a);
            glp_set_col_name(lp, x[s][a], buf);
            glp_set_col_bnds(lp, x[s][a], GLP_DB, 0.0, 1.0 / (1.0 - gamma));
        }
    }
    /* we can add integrality constraints */
    int singleton_indices[3];
    double singleton_values[3] = {0.0, 1.0, -(1.0 / (1.0 - gamma))};

    for(s = 0; s < ord; ++s) {
        int *d_row_indices = (int *) ecalloc(num_actions[s] + 1, sizeof(int));
        double *d_row_values = (double *) ecalloc(num_actions[s] + 1, sizeof(double));

        for(a = 0; a < num_actions[s]; ++a) {
            singleton_indices[1] = x[s][a];
            singleton_indices[2] = x[s][a] + problem_dimension;

            d_row_indices[a + 1] = x[s][a] + problem_dimension;
            d_row_values[a + 1] = 1.0;

            int d_index = glp_add_cols(lp, 1);
            sprintf(buf, D_COL_NAME_PATTERN, s, a);
            glp_set_col_name(lp, x[s][a] + problem_dimension, buf);
            glp_set_col_kind(lp, x[s][a] + problem_dimension, GLP_BV); /* set to GLP_BV if you want binary constraints */

            int row_index = glp_add_rows(lp, 1);
            glp_set_mat_row(lp, row_index, 2, singleton_indices, singleton_values);
            glp_set_row_bnds(lp, row_index, GLP_UP, 0.0, 0.0);
        }
        int row_index = glp_add_rows(lp, 1);
        sprintf(buf, D_CONSTRAINT_PATTERN, s);
        glp_set_row_name(lp, row_index, buf);
        glp_set_mat_row(lp, row_index, num_actions[s], d_row_indices, d_row_values);
        glp_set_row_bnds(lp, row_index, pure ? GLP_FX : GLP_FR, 1.0, 1.0);

        efree(d_row_indices);
        efree(d_row_values);
    }

    return problem_dimension;
}

double bmdp_lp_iteration_case1a(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                double **policy, double **hh, Double_vektor ***rew, double gamma,
                                double *weight, short comp_max, int **x, size_t *num_actions,
                                Row ****rows, double ***diags) {
    /*
     * Case 1a of the LP iteration procedure.
     *
     * Here we run with the hypothesis that the optimal policy is the policy that results
     * from the intersection of three linear programs for all cases. We work iteratively.
     *
     * First, we compute a policy that is optimal with respect to given MDPs. Then, we
     * change the MDPs and compute new value vectors.
     * This should give us a better approximation of the worst and best case MDPs.
     */
    /* variables */
    size_t i, s, a;
    char buf[256];\
    const size_t ord = mdp_avg->ord;

    /* init LP */
    glp_prob *lp = glp_create_prob();
    int problem_dimension = init_lp(ord, num_actions, gamma, lp, buf, x, FALSE);

    /* set constraints */
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        add_mdp_constraints(lp, rows[i], diags[i], gamma, mdp_avg, problem_dimension, x, NULL);
    }

    /* set goal function */
    /* set objective coefficients */
    for(s = 0; s < ord; ++s) {
        for(a = 0; a < num_actions[s]; ++a) {
            double obj_coef = 0.0;
            for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                obj_coef += weight[i] * rew[i][a]->vektor[s];
            }
            glp_set_obj_coef(lp, x[s][a], obj_coef);
        }
    }

    glp_write_lp(lp, NULL, "current.lp"); /* debugging purposes, mostly */
    glp_iocp params;
    glp_init_iocp(&params);
    glp_intopt(lp, &params);
    /* retrieve objective value */
    double value = glp_mip_obj_val(lp);

    /* retrieve policy and value vector */
    for(s = 0; s < ord; ++s) {
        double sum = 0.0;
        for(i = MIN_IDX; i <= MAX_IDX; ++i) {
            hh[i][s] = 0.0;
        }
        for(a = 0; a < num_actions[s]; ++a) {
            const double freq = glp_mip_col_val(lp, x[s][a]);
            sum += freq;
            for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                hh[i][s] += freq * rew[i][a]->vektor[s];
            }
        }
        for(a = 0; a < num_actions[s]; ++a) {
            const double freq = glp_mip_col_val(lp, x[s][a]);
            policy[s][a] = freq / sum;
        }
    }
    glp_delete_prob(lp);

    return value;
}

double bmdp_lp_iteration_case1b(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                double **policy, double **hh,
                                Double_vektor ***rew, double gamma, double *weight,
                                short comp_max, size_t case_index, int **x, size_t *num_actions,
                                Row ****rows, double ***diags) {
    /*
     * Case 1b of the LP iteration procedure.
     *
     * Here we run with the hypothesis that the optimal policy is the policy that results
     * from the intersection of two linear programs for two cases. We work iteratively.
     *
     * First, we compute a policy that is optimal with respect to given MDPs. Then, we
     * change the MDPs and compute new value vectors.
     * This should give us a better approximation of the worst and best case MDPs.
     *
     * case_index refers to the case we are acting in.
     * 0 means min, avg
     * 1 means avg, max
     * 2 means max, min
     */
    const size_t first_index = case_index;
    const int second_index = (first_index + 1) % 3;
    const size_t ord = mdp_avg->ord;
    char buf[256];
    double max_delta = 0.0;
    size_t s, a;

    glp_prob *lp = glp_create_prob();

    int problem_dimension = init_lp(ord, num_actions, gamma, lp, buf, x, FALSE);

    add_mdp_constraints(lp, rows[first_index], diags[first_index], gamma, mdp_avg, problem_dimension, x, NULL);
    add_mdp_constraints(lp, rows[second_index], diags[second_index], gamma, mdp_avg, problem_dimension, x, NULL);

    /* set objective coefficients */
    for(s = 0; s < ord; ++s) {
        for(a = 0; a < num_actions[s]; ++a) {
            glp_set_obj_coef(lp, x[s][a],
                             weight[first_index] * rew[first_index][a]->vektor[s] + weight[second_index] * rew[second_index][a]->vektor[s]);
        }
    }

    /* define MILP parameters */
    glp_iocp params;
    glp_init_iocp(&params);
    params.presolve = GLP_ON;
    params.msg_lev = GLP_MSG_ALL;
    params.br_tech = GLP_BR_DTH;
    params.bt_tech = GLP_BT_BLB;
    params.pp_tech = GLP_PP_ALL;
    params.fp_heur = GLP_OFF;
    params.gmi_cuts = GLP_ON;
    params.mir_cuts = GLP_ON;
    params.cov_cuts = GLP_ON;
    params.clq_cuts = GLP_ON;
    params.tol_int = 1e-5;
    params.tol_obj = 1e-7;
    params.mip_gap = 0.0;
    params.tm_lim = INT_MAX;
    params.out_frq = 5000;
    params.out_dly = 10000;
    params.cb_func = NULL;
    params.cb_info = NULL;
    params.cb_size = 0;
    params.binarize = GLP_ON;
    /* print LP in MPS format */
    glp_write_lp(lp, NULL, "current.lp");
    /* solve */
    glp_intopt(lp, &params);

    /* retrieve solution */
    double value = glp_mip_obj_val(lp);

    for(s = 0; s < ord; ++s) {
        double sum = 0.0;
        for(a = 0; a < num_actions[s]; ++a) {
            const double freq = glp_mip_col_val(lp, x[s][a]);
            sum += freq;
        }
        for(a = 0; a < num_actions[s]; ++a) {
            const double freq = glp_mip_col_val(lp, x[s][a]);
            policy[s][a] = freq / sum;
        }
    }

    /* compute extreme case */
    double *first_values;
    double *second_values;
    if(first_index != AVG_IDX) {
        short compmin = (first_index == MIN_IDX);
        first_values = bmdp_evaluate_minmax_stationary_policy(mdp_min, mdp_max, rew[first_index], gamma, policy, compmin);
    } else {
        first_values = bmdp_evaluate_avg_stationary_policy(mdp_avg, policy, rew[first_index], gamma, FALSE);
    }
    if(second_index != AVG_IDX) {
        short compmin = (second_index == MIN_IDX);
        second_values = bmdp_evaluate_minmax_stationary_policy(mdp_min, mdp_max, rew[second_index], gamma, policy, compmin);
    } else {
        second_values = bmdp_evaluate_avg_stationary_policy(mdp_avg, policy, rew[second_index], gamma, FALSE);
    }

    for(s = 0; s < ord; ++s) {
        double delta = fabs(hh[first_index][s] - first_values[s]);
        delta = fmax(delta, fabs(hh[second_index][s] - second_values[s]));
        max_delta = fmax(delta, max_delta);
        hh[first_index][s] = first_values[s];
        hh[second_index][s] = second_values[s];
    }
    efree(first_values);
    efree(second_values);

    glp_delete_prob(lp);

    return max_delta;
}

double *bmdp_evaluate_minmax_stationary_policy(Mdp_matrix *mdp_min, Mdp_matrix *mdp_max,
                                               Double_vektor **rew, double gamma, double **policy,
                                               short compmin) {
    const size_t ord = mdp_max->ord;
    double *hh = (double *) ecalloc(ord, sizeof(double));
    double *hh_new = (double *) ecalloc(ord, sizeof(double));
    double *vv = (double *) ecalloc(ord, sizeof(double));
    size_t s, a, j;
    Row *hrow = mat_row_new(ord);

    double max_delta = DBL_MAX;

    /* clear arrays */
    for(s = 0; s < ord; ++s) {
        hh[s] = hh_new[s] = 0.0;
    }

    while(max_delta > BMDP_LO_EPS) {
        max_delta = 0.0;
        for(s = 0; s < ord; ++s) {
            const double old_value = hh[s];

            for(a = 0; a < mdp_max->rows[s]->pord; ++a) {
                double v = rew[a]->vektor[s];
                double diag;
                bmdp_set_one_minmax_row(mdp_min->rows[s], mdp_max->rows[s], hrow, &diag, s, a, hh, vv, compmin);

                v += gamma * diag * hh[s];
                for(j = 0; j < hrow->no_of_entries; ++j) {
                    v += gamma * hrow->val[j] * hh[hrow->colind[j]];
                }

                hh_new[s] += policy[s][a] * v;
            }
            const double delta = fabs(old_value - hh_new[s]);
            if(delta > max_delta) {
                max_delta = delta;
            }
        }

        for(s = 0; s < ord; ++s) {
            hh[s] = hh_new[s];
            hh_new[s] = 0;
        }
    }
    efree(hh_new);
    efree(vv);
    mat_row_free(hrow);

    return hh;
}

double bmdp_lp_iteration_case2(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                               size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                               double gamma, double* weight, short comp_max) {
    const size_t ord = mdp_avg->ord;
    size_t *pi_temp = ecalloc(ord, sizeof(size_t));

    double *hh[3];
    double max_value = comp_max ? -DBL_MAX : DBL_MAX;
    size_t i, j, s;

    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        double value = 0.0;

        if(i == MIN_IDX) {
            hh[i] = bmdp_compute_extreme_case(mdp_min, mdp_max, rew_min, pi_temp, BMDP_MAX_ITER, BMDP_MAX_TIME, BMDP_LO_EPS,
                                              BMDP_MAX_TIME, BMDP_LO_EPS, BO_GMRES_ILU0, gamma, TRUE, comp_max, FALSE);
            hh[AVG_IDX] = bmdp_evaluate_avg_policy(mdp_avg, pi_temp, rew_avg, gamma, FALSE);
            hh[MAX_IDX] = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, pi_temp, rew_max, gamma, FALSE, FALSE);
        } else if(i == AVG_IDX) {
            hh[i] = bmdp_policy_iteration(mdp_avg, rew_avg, pi_temp, BMDP_MAX_ITER, BMDP_MAX_TIME, BMDP_LO_EPS, gamma, comp_max, FALSE);
            hh[MIN_IDX] = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, pi_temp, rew_min, gamma, TRUE, FALSE);
            hh[MAX_IDX] = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, pi_temp, rew_max, gamma, FALSE, FALSE);
        } else {
            hh[i] = bmdp_compute_extreme_case(mdp_min, mdp_max, rew_max, pi_temp, BMDP_MAX_ITER, BMDP_MAX_TIME, BMDP_LO_EPS,
                                              BMDP_MAX_TIME, BMDP_LO_EPS, BO_GMRES_ILU0, gamma, FALSE, comp_max, FALSE);
            hh[MIN_IDX] = bmdp_evaluate_minmax_policy(mdp_min, mdp_max, pi_temp, rew_min, gamma, TRUE, FALSE);
            hh[AVG_IDX] = bmdp_evaluate_avg_policy(mdp_avg, pi_temp, rew_avg, gamma, FALSE);
        }
        for(j = MIN_IDX; j <= MAX_IDX; ++j) {
            for(s = 0; s < ord; ++s) {
                value += hh[j][s] * weight[j];
            }
            efree(hh[j]);
        }
        if(value > max_value) {
            /* copy maximal value */
            max_value = value;
            /* copy policy */
            for(s = 0; s < ord; ++s) {
                ind[s] = pi_temp[s];
            }
        }
    }
    return max_value;
}

int add_mdp_constraints(glp_prob *lp, Row ***rows, double **diags, double gamma, Mdp_matrix *mdp, int dimension, int **x, double *initial_distribution) {
    size_t s, a, j, s_prime;
    const size_t ord = mdp->ord;
    int ret = 0;
    double *row_values = (double *) ecalloc(dimension + 2, sizeof(double));
    int *row_indices = (int *) ecalloc(dimension + 2, sizeof(int));
    char buf[256];
    int *mu_indices = (int *) ecalloc(ord + 1, sizeof(int));
    double *mu_values = (double *) ecalloc(ord + 1, sizeof(double));

    for(s = 0; s < ord; ++s) {
        for(a = 0; a < mdp->rows[s]->pord; ++a) {
            row_indices[x[s][a]] = x[s][a];
        }

        int column_index = glp_add_cols(lp, 1);
        sprintf(buf, MU_COL_NAME_PATTERN, (size_t) column_index);
        glp_set_col_kind(lp, column_index, GLP_CV);
        glp_set_col_name(lp, column_index, buf);
        glp_set_obj_coef(lp, column_index, 0.0);
        if(initial_distribution == NULL) {
            /* this is the case with free initial distribution */
            glp_set_col_bnds(lp, column_index, GLP_DB, 0.0, 1.0);
        } else {
            double bound = initial_distribution[s];
            glp_set_col_bnds(lp, column_index, GLP_FX, bound, bound);
        }
        mu_indices[s + 1] = column_index;
        mu_values[s + 1] = 1.0;
    }

    int mu_row_index = glp_add_rows(lp, 1);
    glp_set_mat_row(lp, mu_row_index, ord, mu_indices, mu_values);
    glp_set_row_bnds(lp, mu_row_index, GLP_FX, 1.0, 1.0);

    row_values[dimension + 1] = -1.0;

    for(s = 0; s < ord; ++s) {
        row_indices[dimension + 1] = mu_indices[s + 1];
        int row_index = glp_add_rows(lp, 1);
        if(s == 0) {
            ret = row_index;
        }
        /* cleanup row */
        for(s_prime = 0; s_prime < ord; ++s_prime) {
            for(a = 0; a < mdp->rows[s_prime]->pord; ++a) {
                row_values[x[s_prime][a]] = 0.0;
            }
        }

        /* fill the row with diagonal elements */
        for(a = 0; a < mdp->rows[s]->pord; ++a) {
            row_values[x[s][a]] = 1.0 - gamma * diags[s][a];
        }

        for(s_prime = 0; s_prime < ord; ++s_prime) {
            for(a = 0; a < mdp->rows[s_prime]->pord; ++a) {
                Row *row = rows[s_prime][a];

                for(j = 0; j < row->no_of_entries; ++j) {
                    if(row->colind[j] == s) {
                        row_values[x[s_prime][a]] = -gamma * row->val[j];
                    }
                }
            }
        }

        glp_set_mat_row(lp, row_index, dimension + 1, row_indices, row_values);
#if FX_CONSTRAINT
        glp_set_row_bnds(lp, row_index, GLP_FX, 0.0, 0.0);
#else
        glp_set_row_bnds(lp, row_index, GLP_UP, 0.0, 0.0);
#endif
        sprintf(buf, "constraint row %d for x_%zu", row_index, s);
        glp_set_row_name(lp, row_index, buf);
    }
    efree(row_indices);
    efree(row_values);
    efree(mu_indices);
    efree(mu_values);

    return ret;
}

#if 0
double bmdp_weight_value_iteration_alt(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
        size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
        double *hh_min, double *hh_avg, double *hh_max, double *vv_min, double *vv_max, double gamma,
        double weight_min, double weight_max, short comp_max) {
    Row *hrow = mat_row_new(mdp_avg->ord);
    size_t i = 0, j = 0, a = 0, s = 0;
    size_t s_star_min = 0, s_star_avg = 0, s_star_max = 0;
    size_t a_star_min = 0, a_star_avg = 0, a_star_max = 0;
    double optimal_value = 0.0;
    double diag = 0.0;
    const size_t ord = mdp_avg->ord;
    const double weight_avg = (1.0 - weight_min - weight_max);
    char buf[256];
    const double x_upper_bound = (1.0 / (1.0 - gamma));

    double **q_min = ecalloc(ord, sizeof(double *));
    double **q_avg = ecalloc(ord, sizeof(double *));
    double **q_max = ecalloc(ord, sizeof(double *));
    double weight[3] = {weight_min, weight_avg, weight_max};
    double **q[3] = {q_min, q_avg, q_max};

    Row ***rows[3];
    double **diags[3];
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        rows[i] = ecalloc(ord, sizeof(Row **));
        diags[i] = ecalloc(ord, sizeof(double *));
        for(s = 0; s < ord; ++s) {
            const size_t num_actions = mdp_avg->rows[i]->pord;
            rows[i][s] = ecalloc(num_actions, sizeof(Row *));
            diags[i][s] = ecalloc(num_actions, sizeof(double));
            for(a = 0; a < num_actions; ++a) {
                rows[i][s][a] = mat_row_new(ord);
                if(i == AVG_IDX) {
                    rows[i][s][a]->no_of_entries = mdp_avg->rows[s]->no_of_entries[a];
                    for(j = 0; j < rows[i][s][a]->no_of_entries; ++j) {
                        rows[i][s][a]->val[j] = mdp_avg->rows[s]->values[a][j];
                        rows[i][s][a]->colind[j] = mdp_avg->rows[s]->colind[a][j];
                    }
                    diags[i][s][a] = mdp_avg->rows[s]->diagonals[a];
                } else {
                    short comp_min = (i == MIN_IDX);
                    if(comp_min) {
                        bmdp_set_one_minmax_row(mdp_min->rows[s], mdp_max->rows[s], rows[i][s][a], &(diags[i][s][a]), s, a, hh_min, vv_min, comp_min);
                    } else {
                        bmdp_set_one_minmax_row(mdp_min->rows[s], mdp_max->rows[s], rows[i][s][a], &(diags[i][s][a]), s, a, hh_max, vv_max, comp_min);
                    }
                }
            }
        }
    }

    /* construct nonlinear (projective) program */
    SCIP *scip = NULL;
    SCIPcreate(&scip);
    SCIP_RETCODE ret = SCIPcreateProbBasic(scip, "NLP formulation for weighted BMDP problem");
    SCIPsetObjsense(scip, SCIP_OBJSENSE_MAXIMIZE);

    /* create vars */
    SCIP_VAR ***x_s_a_min = ecalloc(ord, sizeof(SCIP_VAR **));
    SCIP_VAR ***x_s_a_avg = ecalloc(ord, sizeof(SCIP_VAR **));
    SCIP_VAR ***x_s_a_max = ecalloc(ord, sizeof(SCIP_VAR **));

    SCIP_VAR **mu_s_min = ecalloc(ord, sizeof(SCIP_VAR*));
    SCIP_VAR **mu_s_avg = ecalloc(ord, sizeof(SCIP_VAR*));
    SCIP_VAR **mu_s_max = ecalloc(ord, sizeof(SCIP_VAR*));

    SCIP_VAR **mu_vars[3] = {mu_s_min, mu_s_avg, mu_s_max};
    SCIP_VAR ***x_vars[3] = {x_s_a_min, x_s_a_avg, x_s_a_max};

    for(s = 0; s < ord; ++s) {
        const size_t num_actions = mdp_avg->rows[s]->pord;
        x_s_a_min[s] = ecalloc(num_actions, sizeof(SCIP_VAR *));
        x_s_a_avg[s] = ecalloc(num_actions, sizeof(SCIP_VAR *));
        x_s_a_max[s] = ecalloc(num_actions, sizeof(SCIP_VAR *));

        sprintf(buf, MU_MIN_COL_NAME_PATTERN, s);
        ret = SCIPcreateVarBasic(scip, &(mu_s_min[s]), buf, 0.0, 1.0, 0.0, SCIP_VARTYPE_CONTINUOUS);

        sprintf(buf, MU_AVG_COL_NAME_PATTERN, s);
        ret = SCIPcreateVarBasic(scip, &(mu_s_avg[s]), buf, 0.0, 1.0, 0.0, SCIP_VARTYPE_CONTINUOUS);

        sprintf(buf, MU_MAX_COL_NAME_PATTERN, s);
        ret = SCIPcreateVarBasic(scip, &(mu_s_max[s]), buf, 0.0, 1.0, 0.0, SCIP_VARTYPE_CONTINUOUS);

        for(a = 0; a < num_actions; ++a) {
            const double x_s_a_min_weight = weight_min * rew_min[a]->vektor[s];
            const double x_s_a_avg_weight = weight_avg * rew_avg[a]->vektor[s];
            const double x_s_a_max_weight = weight_max * rew_max[a]->vektor[s];

            sprintf(buf, X_MIN_COL_NAME_PATTERN, s, a);
            ret = SCIPcreateVar(scip, &(x_s_a_min[s][a]), buf, 0.0, x_upper_bound, x_s_a_min_weight, SCIP_VARTYPE_CONTINUOUS, TRUE, FALSE,
                                SCIPdelVar, SCIPtransformVar, NULL, NULL,
                                NULL);
            sprintf(buf, X_AVG_COL_NAME_PATTERN, s, a);
            ret = SCIPcreateVarBasic(scip, &(x_s_a_avg[s][a]), buf, 0.0, x_upper_bound, x_s_a_avg_weight, SCIP_VARTYPE_CONTINUOUS);
            sprintf(buf, X_MAX_COL_NAME_PATTERN, s, a);
            ret = SCIPcreateVarBasic(scip, &(x_s_a_max[s][a]), buf, 0.0, x_upper_bound, x_s_a_max_weight, SCIP_VARTYPE_CONTINUOUS);
        }
    }

    /* create constraints for mu */
    SCIP_ROW *mu_rows[3];
    char *names[3] = {MU_MIN_CONSTRAINT_ROW, MU_AVG_CONSTRAINT_ROW, MU_MAX_CONSTRAINT_ROW};
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        SCIPcreateEmptyRow(scip, &(mu_rows[i]), names[i], 0.0, 1.0, FALSE, FALSE, FALSE);
        for(s = 0; s < ord; ++s) {
            SCIPaddVarToRow(scip, mu_rows[i], mu_vars[i][s], 1.0);
        }
    }

    /* create constraints for x */
    names[MIN_IDX] = X_MIN_CONSTRAINT_ROW;
    names[AVG_IDX] = X_AVG_CONSTRAINT_ROW;
    names[MAX_IDX] = X_MAX_CONSTRAINT_ROW;
    SCIP_ROW **x_s_rows[3];
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        x_s_rows[i] = ecalloc(ord, sizeof(SCIP_ROW *));
        for(s = 0; s < ord; ++s) {
            sprintf(buf, names[i], s);
            SCIPcreateEmptyRow(scip, &(x_s_rows[i][s]), buf, -DBL_MAX, 0.0, FALSE, FALSE, FALSE);
            SCIPaddVarToRow(scip, x_s_rows[i][s], mu_vars[i][s], -1.0);

            const size_t num_actions = mdp_avg->rows[s]->pord;

            for(a = 0; a < num_actions; ++a) {
                SCIPaddVarToRow(scip, x_s_rows[i][s], x_vars[i][s][a], 1.0 - gamma * diags[i][s][a]);
            }

            size_t s_prime;
            for(s_prime = 0; s_prime < ord; ++s_prime) {
                for(a = 0; a < mdp_avg->rows[s_prime]->pord; ++a) {
                    /* iterate over the row */
                    Row *row = rows[i][s_prime][a];
                    for(j = 0; j < row->no_of_entries; ++j) {
                        if(row->colind[j] == s) {
                            SCIPaddVarToRow(scip, x_s_rows[i][s], x_vars[i][s_prime][a], - gamma * row->val[j]);
                        }
                    }
                }
            }
        }
    }

    /* bind x */
    SCIP_VAR *alpha[2];
    SCIPcreateVarBasic(scip, &(alpha[0]), "alpha", 0.0, DBL_MAX, 0.0, SCIP_VARTYPE_CONTINUOUS);
    SCIPcreateVarBasic(scip, &(alpha[1]), "beta", 0.0, DBL_MAX, 0.0, SCIP_VARTYPE_CONTINUOUS);

    SCIPincludeConshdlrQuadratic(scip);

    SCIP_CONS ***x_bind_cons[3];
    for(i = MIN_IDX; i < MAX_IDX; ++i) {
        x_bind_cons[i] = ecalloc(ord, sizeof(SCIP_CONS **));
        for(s = 0; s < ord; ++s) {
            const size_t num_actions = mdp_avg->rows[s]->pord;
            x_bind_cons[i][s] = ecalloc(num_actions, sizeof(SCIP_CONS *));
            for(a = 0; a < num_actions; ++a) {
                sprintf(buf, "bind 0 and %zu, state %zu, action %zu", i, s, a);
                SCIPcreateConsBasicQuadratic(scip, &(x_bind_cons[i][s][a]), buf, 0, NULL, NULL, 0, NULL, NULL, NULL, 0.0, 0.0);
                SCIPaddLinearVarQuadratic(scip, x_bind_cons[i][s][a], x_vars[MAX_IDX][s][a], -1.0);
                SCIPaddBilinTermQuadratic(scip, x_bind_cons[i][s][a], x_vars[i][s][a], alpha[i], 1.0);
                SCIPaddCons(scip, x_bind_cons[i][s][a]);
            }
        }
    }

    /* solve */
    SCIPsolve(scip);
    optimal_value = SCIPgetNLPObjval(scip);

    /* collect result */
    double *hh[3] = {hh_min, hh_avg, hh_max};
    Double_vektor **rew[3] = {rew_min, rew_avg, rew_max};
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        for(s = 0; s < ord; ++s) {
            hh[i][s] = 0.0;
            for(a = 0; a < mdp_avg->rows[s]->pord; ++a) {
                double x_s_a = SCIPgetVarSol(scip, x_vars[i][s][a]);
                if(x_s_a > 0) {
                    ind[s] = a;
                    hh[i][s] += x_s_a * rew[i][a]->vektor[s];
                }
            }
        }
    }

    /* cleanup */
    unsigned int del;
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        for(s = 0; s < ord; ++s) {
            const size_t num_actions = mdp_avg->rows[s]->pord;
            for(a = 0; a < num_actions; ++a) {
                SCIPdelVar(scip, x_vars[i][s][a], &del);
                if(i < MAX_IDX) {
                    SCIPdelCons(scip, x_bind_cons[i][s][a]);
                }
                mat_row_free(rows[i][s][a]);
            }
            efree(q[i][s]);
            efree(x_vars[i][s]);
            efree(rows[i][s]);
            SCIPdelVar(scip, mu_vars[i][s], &del);
            if(i < MAX_IDX) {
                efree(x_bind_cons[i][s]);
            }
        }
        efree(q[i]);
        efree(x_vars[i]);
        efree(mu_vars[i]);
        efree(x_s_rows[i]);
        efree(rows[i]);
        if(i < MAX_IDX) {
            efree(x_bind_cons[i]);
        }
    }
    mat_row_free(hrow);

    /* return the result */
    return optimal_value;
}
#endif /* 0 */

double bmdp_weight_value_iteration(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
        size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
        double *hh_min, double *hh_avg, double *hh_max, double *vv_min, double *vv_max, double gamma,
        double weight_min, double weight_max, short comp_max) {
    Row *hrow = mat_row_new(mdp_avg->ord);
    size_t i = 0, j = 0, a = 0, s = 0;
    double val = 0.0;
    double diag = 0.0;
    /* string buffers */
    assert(mdp_avg->ord < 100000);
    char *col_name = ecalloc(256, sizeof(char)); /* 256 characters should be more than enough */
    const size_t ord = mdp_avg->ord;

    /* preprocess rows */
    Row ***rows[3];
    double **diags[3];
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        rows[i] = ecalloc(ord, sizeof(Row **));
        diags[i] = ecalloc(ord, sizeof(double *));
        for(s = 0; s < ord; ++s) {
            const size_t num_actions = mdp_avg->rows[i]->pord;
            rows[i][s] = ecalloc(num_actions, sizeof(Row *));
            diags[i][s] = ecalloc(num_actions, sizeof(double));
            for(a = 0; a < num_actions; ++a) {
                rows[i][s][a] = mat_row_new(ord);
                if(i == AVG_IDX) {
                    rows[i][s][a]->no_of_entries = mdp_avg->rows[s]->no_of_entries[a];
                    for(j = 0; j < rows[i][s][a]->no_of_entries; ++j) {
                        rows[i][s][a]->val[j] = mdp_avg->rows[s]->values[a][j];
                        rows[i][s][a]->colind[j] = mdp_avg->rows[s]->colind[a][j];
                    }
                    diags[i][s][a] = mdp_avg->rows[s]->diagonals[a];
                } else {
                    short comp_min = (i == MIN_IDX);
                    if(comp_min) {
                        bmdp_set_one_minmax_row(mdp_min->rows[s], mdp_max->rows[s], rows[i][s][a], &(diags[i][s][a]), s, a, hh_min, vv_min, comp_min);
                    } else {
                        bmdp_set_one_minmax_row(mdp_min->rows[s], mdp_max->rows[s], rows[i][s][a], &(diags[i][s][a]), s, a, hh_max, vv_max, comp_min);
                    }
                }
            }
        }
    }

    /* initialize mixed-integer linear problem */
    glp_prob *lp = glp_create_prob();
    glp_set_prob_name(lp, "Weight optimization, MILP formulation");
    if(comp_max) {
        glp_set_obj_dir(lp, GLP_MAX);
    } else {
        glp_set_obj_dir(lp, GLP_MIN);
    }

    /* compute the number of variables, essentially |S| |A| */
    int num_int_vars = 0;
    for(i = 0; i < mdp_avg->ord; ++i) {
        num_int_vars += mdp_avg->rows[i]->pord;
    }
    /* add the integer variables into account */
    int num_float_vars = 3 * num_int_vars;
    int num_vars = num_float_vars + num_int_vars;
    glp_add_cols(lp, num_vars + 3 * mdp_avg->ord);

    /*
     * Alignment considerations.
     *
     * We would like to align the LP as follows.
     *
     * x_low x_avg x_up d mu_low mu_avg mu_up
     *
     * The decision variables are grouped by state.
     */
    int mu_index;
    for(mu_index = 1; mu_index <= 3 * mdp_avg->ord; ++mu_index) {
        if(mu_index <= mdp_avg->ord) {
            sprintf(col_name, MU_MIN_COL_NAME_PATTERN, (size_t) (mu_index - 1));
        } else if(mu_index <= 2 * mdp_avg->ord) {
            sprintf(col_name, MU_AVG_COL_NAME_PATTERN, (size_t) (mu_index - 1 - mdp_avg->ord));
        } else if(mu_index <= 3 * mdp_avg->ord) {
            sprintf(col_name, MU_MAX_COL_NAME_PATTERN, (size_t) (mu_index - 1 - 2 * mdp_avg->ord));
        }
        glp_set_col_name(lp, num_vars + mu_index, col_name);
        glp_set_col_bnds(lp, num_vars + mu_index, GLP_DB, 0.0, 1.0);
        glp_set_col_kind(lp, num_vars + mu_index, GLP_BV);
        glp_set_obj_coef(lp, num_vars + mu_index, 0.0);
    }

    /* add constraints for mu */
    glp_add_rows(lp, 3);

    int *mu_indices = ecalloc(mdp_avg->ord + 1, sizeof(int));
    double *mu_values = ecalloc(mdp_avg->ord + 1, sizeof(double));
    for(i = 0; i < mdp_avg->ord; ++i) {
        mu_values[1 + i] = 1.0;
        mu_indices[1 + i] = num_vars + 1 + i;
    }
    glp_set_mat_row(lp, 1, mdp_avg->ord, mu_indices, mu_values);
    glp_set_row_bnds(lp, 1, GLP_FX, 1.0, 1.0);

    for(i = 0; i < mdp_avg->ord; ++i) {
        mu_indices[1 + i] = num_vars + mdp_avg->ord + 1 + i;
    }
    glp_set_mat_row(lp, 2, mdp_avg->ord, mu_indices, mu_values);
    glp_set_row_bnds(lp, 2, GLP_FX, 1.0, 1.0);

    for(i = 0; i < mdp_avg->ord; ++i) {
        mu_indices[1 + i] = num_vars + 2 * mdp_avg->ord + 1 + i;
    }
    glp_set_mat_row(lp, 3, mdp_avg->ord, mu_indices, mu_values);
    glp_set_row_bnds(lp, 3, GLP_FX, 1.0, 1.0);
    efree(mu_indices);
    efree(mu_values);

    int row_index = 4;
    int x_min_index = 1;
    int x_avg_index = 1 + num_int_vars;
    int x_max_index = 1 + 2 * num_int_vars;
    int d_index = 1 + num_float_vars;
    mu_index = 1 + num_vars;

    int *min_row_indices = ecalloc(2 + num_int_vars, sizeof(int));
    int *avg_row_indices = ecalloc(2 + num_int_vars, sizeof(int));
    int *max_row_indices = ecalloc(2 + num_int_vars, sizeof(int));
    double *min_row_values = ecalloc(2 + num_int_vars, sizeof(double));
    double *avg_row_values = ecalloc(2 + num_int_vars, sizeof(double));
    double *max_row_values = ecalloc(2 + num_int_vars, sizeof(double));

    int singleton_index[3] = {0, 1, 2};
    double singleton_value[3] = {0.0, -1.0, 0.0};
    singleton_value[2] = 1 / (1 - gamma);

    int **x = ecalloc(mdp_avg->ord, sizeof(int *));

    /* set up decision variables and coefficients */
    int s_a_index = 0;
    for(i = 0; i < mdp_avg->ord; ++i) {
        int num_actions = mdp_avg->rows[i]->pord;
        int *d_indices = ecalloc(1 + num_actions, sizeof(int));
        double *d_values = ecalloc(1 + num_actions, sizeof(double));
        x[i] = ecalloc(num_actions, sizeof(int));

        for(a = 0; a < mdp_avg->rows[i]->pord; ++a) {
            x[i][a] = s_a_index;

            d_indices[1 + a] = d_index + s_a_index;
            d_values[1 + a] = 1.0;

            sprintf(col_name, D_COL_NAME_PATTERN, i, a);
            glp_set_col_name(lp, d_index + x[i][a], col_name);
            glp_set_col_kind(lp, d_index + s_a_index, GLP_BV);
            glp_set_obj_coef(lp, d_index + s_a_index, 0.0);

            sprintf(col_name, X_MIN_COL_NAME_PATTERN, i, a);
            glp_set_col_name(lp, x_min_index + x[i][a], col_name);
            glp_set_col_kind(lp, x_min_index + s_a_index, GLP_CV);
            glp_set_col_bnds(lp, x_min_index + s_a_index, GLP_LO, 0.0, DBL_MAX);
            double coef = rew_min[a]->vektor[i] * weight_min;
            glp_set_obj_coef(lp, x_min_index + s_a_index, coef);

            sprintf(col_name, X_AVG_COL_NAME_PATTERN, i, a);
            glp_set_col_name(lp, x_avg_index + x[i][a], col_name);
            glp_set_col_kind(lp, x_avg_index + s_a_index, GLP_CV);
            glp_set_col_bnds(lp, x_avg_index + s_a_index, GLP_LO, 0.0, DBL_MAX);
            glp_set_obj_coef(lp, x_avg_index + s_a_index, rew_avg[a]->vektor[i] * (1.0 - weight_min - weight_max));

            sprintf(col_name, X_MAX_COL_NAME_PATTERN, i, a);
            glp_set_col_name(lp, x_max_index + x[i][a], col_name);
            glp_set_col_kind(lp, x_max_index + s_a_index, GLP_CV);
            glp_set_col_bnds(lp, x_max_index + s_a_index, GLP_LO, 0.0, DBL_MAX);
            glp_set_obj_coef(lp, x_max_index + s_a_index, rew_max[a]->vektor[i] * weight_max);

            glp_add_rows(lp, 3);

            singleton_index[2] = d_index + s_a_index;

            singleton_index[1] = x_min_index + s_a_index;
            glp_set_mat_row(lp, row_index, 2, singleton_index, singleton_value);
            glp_set_row_bnds(lp, row_index, GLP_LO, 0.0, DBL_MAX);
            ++row_index;

            singleton_index[1] = x_avg_index + s_a_index;
            glp_set_mat_row(lp, row_index, 2, singleton_index, singleton_value);
            glp_set_row_bnds(lp, row_index, GLP_LO, 0.0, DBL_MAX);
            ++row_index;

            singleton_index[1] = x_max_index + s_a_index;
            glp_set_mat_row(lp, row_index, 2, singleton_index, singleton_value);
            glp_set_row_bnds(lp, row_index, GLP_LO, 0.0, DBL_MAX);
            ++row_index;

            min_row_indices[2 + s_a_index] = x_min_index + s_a_index;
            avg_row_indices[2 + s_a_index] = x_avg_index + s_a_index;
            max_row_indices[2 + s_a_index] = x_max_index + s_a_index;

            ++s_a_index;
        }

        glp_add_rows(lp, 1);
        glp_set_mat_row(lp, row_index, num_actions, d_indices, d_values);
        glp_set_row_bnds(lp, row_index, GLP_FX, 1.0, 1.0);
        ++row_index;

        efree(d_indices);
        efree(d_values);
    }

    for(i = 0; i < mdp_avg->ord; ++i) {
        glp_add_rows(lp, 3);

        for(j = 2; j < 2 + num_int_vars; ++j){
            min_row_values[j] = 0.0;
            avg_row_values[j] = 0.0;
            max_row_values[j] = 0.0;
        }

        min_row_values[1] = -1.0;
        avg_row_values[1] = -1.0;
        max_row_values[1] = -1.0;
        min_row_indices[1] = mu_index + i;
        avg_row_indices[1] = mdp_avg->ord + mu_index + i;
        max_row_indices[1] = 2 * mdp_avg->ord + mu_index + i;

        for(a = 0; a < mdp_max->rows[i]->pord; ++a) {
            min_row_values[2 + x[i][a]] = 1.0 - gamma * diags[MIN_IDX][i][a];
            avg_row_values[2 + x[i][a]] = 1.0 - gamma * diags[AVG_IDX][i][a];
            max_row_values[2 + x[i][a]] = 1.0 - gamma * diags[MAX_IDX][i][a];
        }

        for(s = 0; s < ord; ++s) {
            for(a = 0; a < mdp_max->rows[s]->pord; ++a) {
                Row *min_row = rows[MIN_IDX][s][a];
                Row *avg_row = rows[AVG_IDX][s][a];
                Row *max_row = rows[MAX_IDX][s][a];

                for(j = 0; j < min_row->no_of_entries; ++j) {
                    if(min_row->colind[j] == i) {
                        min_row_values[2 + x[s][a]] = -gamma * min_row->val[j];
                    }
                }
                for(j = 0; j < avg_row->no_of_entries; ++j) {
                    if(avg_row->colind[j] == i) {
                        avg_row_values[2 + x[s][a]] = -gamma * avg_row->val[j];
                    }
                }
                for(j = 0; j < max_row->no_of_entries; ++j) {
                    if(max_row->colind[j] == i) {
                        max_row_values[2 + x[s][a]] = -gamma * max_row->val[j];
                    }
                }
            }
        }

        glp_set_mat_row(lp, row_index, 1 + num_int_vars, min_row_indices, min_row_values);
#if FX_CONSTRAINT
        glp_set_row_bnds(lp, row_index, GLP_FX, 0.0, 0.0);
#else
        glp_set_row_bnds(lp, row_index, GLP_UP, 0.0, 0.0);
#endif
        ++row_index;

        glp_set_mat_row(lp, row_index, 1 + num_int_vars, avg_row_indices, avg_row_values);
#if FX_CONSTRAINT
        glp_set_row_bnds(lp, row_index, GLP_FX, 0.0, 0.0);
#else
        glp_set_row_bnds(lp, row_index, GLP_UP, 0.0, 0.0);
#endif
        ++row_index;

        glp_set_mat_row(lp, row_index, 1 + num_int_vars, max_row_indices, max_row_values);
#if FX_CONSTRAINT
        glp_set_row_bnds(lp, row_index, GLP_FX, 0.0, 0.0);
#else
        glp_set_row_bnds(lp, row_index, GLP_UP, 0.0, 0.0);
#endif
        ++row_index;
    }
    efree(min_row_indices);
    efree(min_row_values);
    efree(avg_row_indices);
    efree(avg_row_values);
    efree(max_row_indices);
    efree(max_row_values);
    efree(col_name);

    /* define MILP parameters */
    glp_iocp params;
    glp_init_iocp(&params);
    params.presolve = GLP_ON;
    params.msg_lev = GLP_MSG_ALL;
    params.br_tech = GLP_BR_DTH;
    params.bt_tech = GLP_BT_BLB;
    params.pp_tech = GLP_PP_ALL;
    params.fp_heur = GLP_OFF;
    params.gmi_cuts = GLP_ON;
    params.mir_cuts = GLP_ON;
    params.cov_cuts = GLP_ON;
    params.clq_cuts = GLP_ON;
    params.tol_int = 1e-5;
    params.tol_obj = 1e-7;
    params.mip_gap = 0.0;
    params.tm_lim = INT_MAX;
    params.out_frq = 5000;
    params.out_dly = 10000;
    params.cb_func = NULL;
    params.cb_info = NULL;
    params.cb_size = 0;
    params.binarize = GLP_ON;
    /* print LP in MPS format */
    glp_write_lp(lp, NULL, "current.lp");
    /* solve */
    glp_intopt(lp, &params);

    /* get objective value */
    val = glp_mip_obj_val(lp);
    int status = glp_mip_status(lp);
    if(status != GLP_OPT) {
        printf("[BMDP_COMPUTE_WEIGHT] There is something seriously wrong with the LP\n");
    }
    assert(status == GLP_OPT);

    /* collect solution */
    for(i = 0; i < mdp_avg->ord; ++i) {

        double new_hh_min = 0.0;
        double new_hh_max = 0.0;
        double new_hh_avg = 0.0;

        for(a = 0; a < mdp_avg->rows[i]->pord; ++a) {
            new_hh_min += glp_mip_col_val(lp, x_min_index + x[i][a]) * rew_min[a]->vektor[i];
            new_hh_max += glp_mip_col_val(lp, x_max_index + x[i][a]) * rew_max[a]->vektor[i];
            new_hh_avg += glp_mip_col_val(lp, x_avg_index + x[i][a]) * rew_avg[a]->vektor[i];

            double choice = glp_mip_col_val(lp, d_index + x[i][a]);
            if(choice > 1e-7) {
                ind[i] = a;
            }

            mat_row_free(rows[MIN_IDX][i][a]);
            mat_row_free(rows[AVG_IDX][i][a]);
            mat_row_free(rows[MAX_IDX][i][a]);
        }
        hh_min[i] = new_hh_min;
        hh_avg[i] = new_hh_avg;
        hh_max[i] = new_hh_max;

        efree(x[i]);
    }
    efree(x);
    mat_row_free(hrow);
    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
        for(s = 0; s < ord; ++s) {
            efree(rows[i][s]);
        }
        efree(rows[i]);
    }
    return val;
}

/**********************
bmdp_find_and_set_new_weight_strategy
Datum:  03.03.15
Autor:  Dimitri Scheftelowitsch
Input:  mdp_min, mdp_max Bounding MDP matrices
        hh gain vector
        ind old politics
        hh auxiliary vectors
        min_weight weight of the worst case
        max_weight weight of the best case
Output: pp matrix of the politics
        ind new policy
        b reward vector of new policy
        return value true i fnew politics has bene found
Side-Effects:
Description:
The function computes a new policy and the correpsonding matrix and reward vector
for a BMDP.
***********************************************************************/
short bmdp_find_and_set_new_weight_strategy(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
        Sparse_matrix *pp_min, Sparse_matrix *pp_avg, Sparse_matrix *pp_max,
        size_t *ind, Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
        Double_vektor *b_min, Double_vektor *b_avg, Double_vektor *b_max,
        double *hh_min, double *hh_avg, double *hh_max, double *vv_min, double *vv_max, double gamma,
        double weight_min, double weight_max, short comp_max) {
    /*
    * Notes for myself, just to be clear:
    * pp_* are the preconditioned matrices (I - \gamma P)
    * hh_* are the gain vectors
    * b_* are the reward vectors
    * vv_* are help vectors for sorting in the inner minimization/maximization step.
    */
    short news = FALSE;
    size_t i, j, k, l, count = 0;
    double val, mval, diag;
    Row *hrow = mat_row_new(pp_avg->ord);

    double *new_hh_min = (double *) ecalloc(pp_avg->ord, sizeof(double));
    double *new_hh_avg = (double *) ecalloc(pp_avg->ord, sizeof(double));
    double *new_hh_max = (double *) ecalloc(pp_avg->ord, sizeof(double));

    for (i = 0; i < pp_avg->ord; i++) {
        if (comp_max)
            mval = -1.0e+12;
        else
            mval = 1.0e+12;
        l = ind[i];
        for (k = 0; k < mdp_max->rows[i]->pord; k++) {
            if (mdp_max->rows[i]->diagonals[k] > 0.0 || mdp_max->rows[i]->no_of_entries[k] > 0) {
                hrow->no_of_entries = mdp_min->rows[i]->no_of_entries[k];

                /* compute the pessimistic bound */

                bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], hrow, &diag, i, k, hh_min, vv_min, TRUE);
                double min_val = rew_min[k]->vektor[i] + gamma * hh_min[i] * diag;

                for (j = 0; j < mdp_max->rows[i]->no_of_entries[k]; j++)
                    min_val += gamma * hh_min[hrow->colind[j]] * hrow->val[j];

                /* compute the optimistic bound */
                bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], hrow, &diag, i, k, hh_max, vv_max, FALSE);
                double max_val = rew_max[k]->vektor[i] + gamma * hh_max[i] * diag;

                for (j = 0; j < mdp_max->rows[i]->no_of_entries[k]; j++)
                    max_val += gamma * hh_max[hrow->colind[j]] * hrow->val[j];

                /* TODO optimize the whole stuff */
                double avg_val = rew_avg[k]->vektor[i] + gamma * mdp_avg->rows[i]->diagonals[k] * hh_avg[i];
                for(j = 0; j < mdp_avg->rows[i]->no_of_entries[k]; j++)
                    avg_val += gamma * hh_avg[mdp_avg->rows[i]->colind[k][j]] * mdp_avg->rows[i]->values[k][j];

                val = weight_min * min_val + weight_max * max_val + (1.0 - weight_max - weight_min) * avg_val;

                short cond_on_max = (comp_max && (val > mval || (k == ind[i] && val + BMDP_MIN_VAL > mval)));
                short cond_on_min = (!comp_max && (val < mval || (k == ind[i] && val - BMDP_MIN_VAL < mval)));
                if (comp_max) {
                    if (val > mval || (k == ind[i] && val + BMDP_MIN_VAL > mval)) {
                        mval = val;
                        l = k;
                        new_hh_min[i] = min_val;
                        new_hh_avg[i] = avg_val;
                        new_hh_max[i] = max_val;
                        b_min->vektor[i] = rew_min[l]->vektor[i];
                        b_avg->vektor[i] = rew_avg[l]->vektor[i];
                        b_max->vektor[i] = rew_max[l]->vektor[i];
                    }
                } else {
                    if (val < mval || (k == ind[i] && val - BMDP_MIN_VAL < mval)) {
                        mval = val;
                        l = k;
                        new_hh_min[i] = min_val;
                        new_hh_avg[i] = avg_val;
                        new_hh_max[i] = max_val;
                        b_min->vektor[i] = rew_min[l]->vektor[i];
                        b_avg->vektor[i] = rew_avg[l]->vektor[i];
                        b_max->vektor[i] = rew_max[l]->vektor[i];
                    }
                }
            } /* non_zero row */
        }
        if (ind[i] != l) {
            count++;
            ind[i] = l;
            news = TRUE;
        }
        /* We have to reset the row because even if the policy remains the row may have been changed due to
           a different gain vector */

        bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], &(pp_min->rowind[i]), &pp_min->diagonals[i], i, ind[i], hh_min, vv_min, TRUE);
        bmdp_set_one_minmax_row(mdp_min->rows[i], mdp_max->rows[i], &(pp_max->rowind[i]), &pp_max->diagonals[i], i, ind[i], hh_max, vv_max, FALSE);

        pp_min->diagonals[i] = 1.0 - gamma * pp_min->diagonals[i];
        pp_max->diagonals[i] = 1.0 - gamma * pp_max->diagonals[i];

        pp_avg->rowind[i].no_of_entries = mdp_avg->rows[i]->no_of_entries[ind[i]];
        pp_avg->diagonals[i] = 1.0 - gamma * mdp_avg->rows[i]->diagonals[ind[i]];

        for (j = 0; j < (pp_max->rowind[i]).no_of_entries; j++) {
            (pp_max->rowind[i]).val[j] *= -gamma;
        }
        for (j = 0; j < (pp_min->rowind[i]).no_of_entries; j++) {
            (pp_min->rowind[i]).val[j] *= -gamma;
        }
        for (j = 0; j < (pp_avg->rowind[i]).no_of_entries; j++) {
            (pp_avg->rowind[i]).val[j] = -gamma * mdp_avg->rows[i]->values[ind[i]][j];
            pp_avg->rowind[i].colind[j] = mdp_avg->rows[i]->colind[ind[i]][j];
        }

    } /* for i = 0, ... ord-1 */

    mat_row_free(hrow);
    efree(new_hh_min);
    efree(new_hh_avg);
    efree(new_hh_max);

    return (news);
} /* bmdp_find_and_set_inew_strategy */

typedef struct value_pair {
    size_t ord; /* length */
    size_t *ind; /* policy */
    double *min_gain; /* lower bound */
    double *max_gain; /* upper bound */
    double min_weight; /* weighting function */
    double max_weight;
} value_pair_t;

value_pair_t *new_value_pair(const Mdp_matrix *mdp) {
    value_pair_t *pair = ecalloc(1, sizeof(value_pair_t));
    pair->ind = ecalloc(mdp->ord, sizeof(size_t));
    pair->ord = mdp->ord;

    return pair;
}

void remove_value_pair(value_pair_t *pair) {
    efree(pair->ind);
    efree(pair->min_gain);
    efree(pair->max_gain);
    efree(pair);
}

Pareto_vector_element *eval_weight_2d(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                      Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                      int max_iter, double ti_max, double epsilon, int method, double gamma,
                                      double min_weight, double max_weight, short comp_max) {
    Pareto_vector_element *pair = pareto_element_new_with_weights(mdp_avg->ord, min_weight, max_weight);
    eval_weight_in_place(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, max_iter, ti_max, epsilon, method, gamma, pair, comp_max);

    return pair;
}

void eval_weight_in_place(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                          Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                          int max_iter, double ti_max, double epsilon, int method, double gamma,
                          Pareto_vector_element *pair, short comp_max) {
    /* important: pair->policy has to be unallocated */
    pair->policy = ecalloc(mdp_avg->ord, sizeof(size_t));

    gain_vectors_t tuple = bmdp_compute_weight_case(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, pair->policy,
                                                    max_iter, ti_max, epsilon, 300, 1.0e-8, method, gamma, pair->weight_min,
                                                    pair->weight_max, comp_max, 0);

    pair->gain_min = tuple.gain_min;
    pair->gain_avg = tuple.gain_avg;
    pair->gain_max = tuple.gain_max;
}

Pareto_vector_element *pareto_element_new_with_weights(size_t ord, double min_weight, double max_weight) {
    Pareto_vector_element *element = bmpar_pareto_vector_element_new(ord);
    element->weight_min = min_weight;
    element->weight_max = max_weight;
    element->weight_avg = (1.0 - min_weight - max_weight);
    return element;
}

Pareto_vector_element *eval_weight_1d(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                      Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                      int max_iter, double ti_max, double epsilon, int method, double gamma,
                                      double min_weight, short comp_max) {
    return eval_weight_2d(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, max_iter, ti_max, epsilon, method, gamma, min_weight, 0.0, comp_max);
}

List **list_copy(List **list) {
    List **copy = list_new();
    List **it = list;
    while(!list_empty(it)) {
        list_prepend(copy, list_cont(it));
        it = list_next(it);
    }
    return copy;
}

short eq(double x, double y) {
    return fabs(x - y) <= BMDP_LO_EPS;
}

short approx(double x, double y) {
    return fabs(x - y) <= (BMDP_LO_EPS * 100);
}

int compare_weight(Pareto_vector_element *p1, Pareto_vector_element *p2) {
    if(p1->ord != p2->ord) {
        return 1; /* the simple case */
    }
    if(approx(p1->weight_min, p2->weight_min) && approx(p1->weight_avg, p2->weight_avg) && approx(p1->weight_max, p2->weight_max)) {
        return 0;
    } else {
        return 1;
    }
}

int compare_elements(Pareto_vector_element *p1, Pareto_vector_element *p2) {
    /* return 0 if equal, 1 if not equal */
    if(compare_weight(p1, p2)) {
        return 1;
    }
    int ret = 0;
    int i = 0;
    for(i = 0; i < p1->ord; ++i) {
        if(p1->policy[i] != p2->policy[i]) {
            ret = 1;
            break;
        }
    }
    return ret;
}

int list_length(List **pv) {
    int len = 0;
    List **it = pv;
    while(!list_empty(it)) {
        ++len;
        it = list_next(it);
    }
    return len;
}

List **bmdp_weight_opt3d(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                         Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                         int max_iter, double ti_max, double epsilon, int method, double gamma, short comp_max, enum weight_dimensions dim) {
    Pareto_vector_element *min_pair = eval_weight_2d(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max,
                                                     max_iter, ti_max, epsilon, method, gamma, 1.0, 0.0, comp_max);
    Pareto_vector_element *avg_pair = eval_weight_2d(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max,
                                                     max_iter, ti_max, epsilon, method, gamma, 0.0, 0.0, comp_max);
    Pareto_vector_element *max_pair = eval_weight_2d(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max,
                                                     max_iter, ti_max, epsilon, method, gamma, 0.0, 1.0, comp_max);

    List **pv = list_new();

    size_t insertions = 0;
    if(dim != avg_max) {
        list_prepend(pv, min_pair);
        ++insertions;
    }
    if(dim != min_max) {
        list_prepend(pv, avg_pair);
        ++insertions;
    }
    if(dim != min_avg) {
        list_prepend(pv, max_pair);
        ++insertions;
    }

    /* initialize cddlib */
    dd_set_global_constants();
    dd_ErrorType err;

    /* since there are only three columns in the LP, we can statically allocate the vectors */
    int indices[4] = {0, 1, 2, 3};
    double values[4] = {0.0, 0.0, 0.0, 0.0};

    /*
    weight_triple const_min_weight = {1.0, 0.0, 0.0, -DBL_MAX / 2};
    weight_triple const_avg_weight = {0.0, 1.0, 0.0, -DBL_MAX / 3};
    weight_triple const_max_weight = {0.0, 0.0, 1.0, -DBL_MAX / 4};
    */

    size_t s;
    for(s = 0; s < mdp_avg->ord; ++s) {
        /* do we have heaps or something in C? */
        /* until then, just consider all possible improvements */
        short news = TRUE;

        while(news) {
            /* deadline mechanism */
            news = FALSE;
            /* solve a chained optimization problem */
            List **it = pv;
            int pv_length = list_length(pv);
            dd_MatrixPtr matrix = dd_CreateMatrix(6 + pv_length, 5); /* 5 stands for x, u, rhs */
            /* b - Ax >= 0 */
            /* since we give -A, then we reformulate the problem as Ax + b >= 0 */

            /* create LP */
            glp_prob *lp = glp_create_prob();
            glp_add_cols(lp, 3);
            glp_set_col_name(lp, 1, "u_min");
            glp_set_col_name(lp, 2, "u_avg");
            glp_set_col_name(lp, 3, "u_max");
            glp_set_prob_name(lp, "problem");

            /**
             * NOTE
             *
             * Mathematically, we do not have to set the bounds. However, otherwise, the LP optimizers
             * (including, but not limited to, CPLEX, lp_solve and GLPK) all say the problem is unbounded.
             *
             * Hence, we physically bound the variables with the best known upper bounds which are the
             * optimal values for the extreme weights.
             */
            if(comp_max) {
                glp_set_obj_dir(lp, GLP_MAX);
                /* glp_set_col_bnds(lp, 1, GLP_UP, -DBL_MAX, min_pair->gain_min[s]); */
                glp_set_col_bnds(lp, 1, GLP_FR, -DBL_MAX, DBL_MAX);
                glp_set_col_bnds(lp, 2, GLP_FR, -DBL_MAX, DBL_MAX);
                /* glp_set_col_bnds(lp, 2, GLP_UP, -DBL_MAX, avg_pair->gain_avg[s]); */
                glp_set_col_bnds(lp, 3, GLP_UP, -DBL_MAX, max_pair->gain_max[s]);
            } else {
                glp_set_obj_dir(lp, GLP_MIN);
                glp_set_col_bnds(lp, 1, GLP_LO, min_pair->gain_min[s], DBL_MAX);
                /* glp_set_col_bnds(lp, 2, GLP_LO, avg_pair->gain_avg[s], DBL_MAX); */
                /* glp_set_col_bnds(lp, 3, GLP_LO, max_pair->gain_max[s], DBL_MAX); */
                glp_set_col_bnds(lp, 2, GLP_FR, -DBL_MAX, DBL_MAX);
                glp_set_col_bnds(lp, 3, GLP_FR, -DBL_MAX, DBL_MAX);
            }
            glp_add_rows(lp, pv_length + 2);

            /* enforce min \le avg, common constraint for all subproblems */
            values[1] = -1.0;
            values[2] = 1.0;
            values[3] = 0.0;
            glp_set_mat_row(lp, pv_length + 0, 3, indices, values);
            glp_set_row_bnds(lp, pv_length + 0, GLP_LO, 0.0, 0.0); /* the upper bound is ignored */

            values[1] = 0.0;
            values[2] = -1.0;
            values[3] = 1.0;
            glp_set_mat_row(lp, pv_length + 1, 3, indices, values);
            glp_set_row_bnds(lp, pv_length + 1, GLP_LO, 0.0, 0.0);

            size_t dd_index = 0;
            size_t lp_index = 1; /* GLPK counts from one. This is irritating. */

            while(!list_empty(it)) {
                /*
                 * w_a ( v_avg_i - v_avg_j ) + w_l ( v_min_i - v_min_j ) + w_u ( v_max_i - v_max_j ) >= 0
                 * w ( v_avg_i - v_avg_j - v_min_i + v_min_j ) >= - v_min_i + v_min_j
                 */
                Pareto_vector_element *p = ((Pareto_vector_element *) (*it)->elem);
                double v_j_min = p->gain_min[s];
                double v_j_avg = p->gain_avg[s];
                double v_j_max = p->gain_max[s];

                dd_set_d(matrix->matrix[dd_index][0], 0.0);
                dd_set_d(matrix->matrix[dd_index][1], v_j_min /* dmin */);
                dd_set_d(matrix->matrix[dd_index][2], v_j_avg /* davg */);
                dd_set_d(matrix->matrix[dd_index][3], v_j_max /* dmax */);
                dd_set_d(matrix->matrix[dd_index][4], -1.0);
                ++dd_index;
                /* introduce LP constraints */
                /* w_j_min(v_j_s_min - u_min) + w_j_avg(v_j_s_avg - u_avg) + w_j_max(v_j_s_max - u_max) \ge 0 */

                double constraint = p->weight_min * p->gain_min[s] + p->weight_avg * p->gain_avg[s] +
                                    p->weight_max * p->gain_max[s];

                values[1] = p->weight_min;
                values[2] = p->weight_avg;
                values[3] = p->weight_max;

                glp_set_mat_row(lp, lp_index, 3, indices, values);
                if(comp_max) {
                    glp_set_row_bnds(lp, lp_index, GLP_UP, 0.0, constraint);
                } else {
                    glp_set_row_bnds(lp, lp_index, GLP_LO, constraint, 0.0);
                }
                ++lp_index;
                it = list_next(it);
            }
            /* enforce boundary constraints, x_i >= 0 */
            dd_set_si(matrix->matrix[dd_index + 0][0], 0);
            dd_set_si(matrix->matrix[dd_index + 0][1], 1);
            dd_set_si(matrix->matrix[dd_index + 0][2], 0);
            dd_set_si(matrix->matrix[dd_index + 0][3], 0);
            dd_set_si(matrix->matrix[dd_index + 0][4], 0);

            dd_set_si(matrix->matrix[dd_index + 1][0], 0);
            dd_set_si(matrix->matrix[dd_index + 1][1], 0);
            dd_set_si(matrix->matrix[dd_index + 1][2], 1);
            dd_set_si(matrix->matrix[dd_index + 1][3], 0);
            dd_set_si(matrix->matrix[dd_index + 1][4], 0);

            dd_set_si(matrix->matrix[dd_index + 2][0], 0);
            dd_set_si(matrix->matrix[dd_index + 2][1], 0);
            dd_set_si(matrix->matrix[dd_index + 2][2], 0);
            dd_set_si(matrix->matrix[dd_index + 2][3], 1);
            dd_set_si(matrix->matrix[dd_index + 2][4], 0);
            /* enforce sum constraint, 1 + (- x_1 - x_2 - x_3) == 0 */
            dd_set_si(matrix->matrix[dd_index + 3][0], 1);
            dd_set_si(matrix->matrix[dd_index + 3][1], -1);
            dd_set_si(matrix->matrix[dd_index + 3][2], -1);
            dd_set_si(matrix->matrix[dd_index + 3][3], -1);
            dd_set_si(matrix->matrix[dd_index + 3][4], 0);

            dd_set_si(matrix->matrix[dd_index + 4][0], -1);
            dd_set_si(matrix->matrix[dd_index + 4][1], 1);
            dd_set_si(matrix->matrix[dd_index + 4][2], 1);
            dd_set_si(matrix->matrix[dd_index + 4][3], 1);
            dd_set_si(matrix->matrix[dd_index + 4][4], 0);

            dd_set_si(matrix->matrix[dd_index + 5][0], 0);
            dd_set_si(matrix->matrix[dd_index + 5][1], 0);
            dd_set_si(matrix->matrix[dd_index + 5][2], 0);
            dd_set_si(matrix->matrix[dd_index + 5][3], 0);
            dd_set_si(matrix->matrix[dd_index + 5][4], 0);

            /* set_addelem(matrix->linset, dd_index + 4); */
            if(dim == min_avg) {
                dd_set_si(matrix->matrix[dd_index + 5][3], -1); /* consider only w_min and w_avg, w_max = 0 */
            } else if(dim == avg_max) {
                dd_set_si(matrix->matrix[dd_index + 5][1], -1); /* w_min = 0 */
            } else if(dim == min_max) {
                dd_set_si(matrix->matrix[dd_index + 5][2], -1); /* w_avg = 0 */
            }

            /* find all vertices */
            matrix->representation = dd_Inequality;
            dd_PolyhedraPtr poly = dd_DDMatrix2Poly(matrix, &err);
            dd_MatrixPtr points;

            if(err == dd_NumericallyInconsistent) {
                /* oh. sorry. Too many hyperplanes. */
                break;
            }
            assert(err == dd_NoError);
            points = dd_CopyGenerators(poly);

            long k;
            for(k = 0; k < points->rowsize; ++k) {
                double is_vertex = dd_get_d(points->matrix[k][0]);
                double weight_min = dd_get_d(points->matrix[k][1]);
                double weight_avg = dd_get_d(points->matrix[k][2]);
                double weight_max = dd_get_d(points->matrix[k][3]);
                short nontrivial = weight_min + epsilon < 1.0 && weight_avg + epsilon < 1.0 && weight_max + epsilon < 1.0;

                if(is_vertex == 1 && nontrivial) {

                    glp_set_obj_coef(lp, 1, weight_min);
                    glp_set_obj_coef(lp, 2, weight_avg);
                    glp_set_obj_coef(lp, 3, weight_max);

                    glp_simplex(lp, NULL);
                    double v = glp_get_obj_val(lp);
                    if(glp_get_status(lp) != GLP_OPT) {
                        printf("[BMDP_COMPUTE_HULL] There is something seriously wrong with the LP");
                        glp_write_lp(lp, NULL, "problematic_lp.lp");
                        glp_write_mps(lp, GLP_MPS_FILE, 0, "problematic_lp.mps");
                    }
                    assert(glp_get_status(lp) == GLP_OPT);

                    /*
                     * More changes to the algorithm as described in the paper.
                     * Here, we probably should not filter all sub-optimal weights out. The problem seems to be that the optimal gap
                     * seems to lie at some other extreme weight value, which is mathematically plausible, but in our case utterly useless.
                     * Let us just consider all possible weights with nonzero gap.
                     */
                    short insert = TRUE;

                    List **it = pv;
                    while(!list_empty(it)) {
                        Pareto_vector_element *item = (Pareto_vector_element *) ((*it)->elem);
                        double value = item->gain_min[s] * weight_min + item->gain_avg[s] * weight_avg + item->gain_max[s] * weight_max;
                        insert &= (v - value > epsilon / 100);
                        it = list_next(it);
                    }

                    short success = TRUE;
                    if(insert) {
                        Pareto_vector_element *new_pair = pareto_element_new_with_weights(mdp_avg->ord, weight_min, weight_max);
                        void *adr = NULL;
                        short found = list_find(pv, &adr, new_pair, compare_weight); /* the common case is existence */
                        if(!found) {
                            eval_weight_in_place(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, max_iter, ti_max,
                                                 epsilon, method, gamma, new_pair, comp_max);
                            success &= list_insert(pv, &adr, new_pair, new_pair, compare_elements);
                            if(success) {
                                insertions++;
                                news = TRUE;
                            }
                        }
                        printf("weights: %f %f %f; already there: %d\n", weight_min, weight_avg, weight_max, !success);
                    }
                }
            }
            /* free stuff */
            dd_FreeMatrix(matrix);
            dd_FreePolyhedra(poly);
            dd_FreeMatrix(points);

            glp_delete_prob(lp);
        }
    }
    dd_free_global_constants();

    /* just to test stuff */
    List **it = pv;
    while(!list_empty(it)) {
        Pareto_vector_element *item = (Pareto_vector_element *) ((*it)->elem);

        List **jt = pv;
        while(!list_empty(jt)) {
            short fail = TRUE;
            Pareto_vector_element *other_item = (Pareto_vector_element *) ((*jt)->elem);
            int i;
            for(i = 0; i < item->ord; ++i) {
                double value_i = 0.0;
                double value_j = 0.0;

                value_i += item->weight_min * item->gain_min[i];
                value_i += item->weight_avg * item->gain_avg[i];
                value_i += item->weight_max * item->gain_max[i];

                value_j += item->weight_min * other_item->gain_min[i];
                value_j += item->weight_avg * other_item->gain_avg[i];
                value_j += item->weight_max * other_item->gain_max[i];

                if(value_j < value_i + 1e-5) {
                    fail = FALSE;
                }
            }
            if(fail) {
                printf("[BMDP_COMPUTE_HULL] Hull computation buggy…");
            }
            jt = list_next(jt);
        }
        it = list_next(it);
    }

    return pv;
}

List **bmdp_weight_opt3d_alt(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                             Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                             int max_iter, double ti_max, double epsilon, int method, double gamma, short comp_max, enum weight_dimensions dim) {
    Pareto_vector_element *min_pair = eval_weight_2d(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max,
                                                     max_iter, ti_max, epsilon, method, gamma, 1.0, 0.0, comp_max);
    Pareto_vector_element *avg_pair = eval_weight_2d(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max,
                                                     max_iter, ti_max, epsilon, method, gamma, 0.0, 0.0, comp_max);
    Pareto_vector_element *max_pair = eval_weight_2d(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max,
                                                     max_iter, ti_max, epsilon, method, gamma, 0.0, 1.0, comp_max);

    List **pv = list_new();

    size_t insertions = 0;
    if(dim != avg_max) {
        list_prepend(pv, min_pair);
        ++insertions;
    }
    if(dim != min_max) {
        list_prepend(pv, avg_pair);
        ++insertions;
    }
    if(dim != min_avg) {
        list_prepend(pv, max_pair);
        ++insertions;
    }

    /* initialize cddlib */
    dd_set_global_constants();
    dd_ErrorType err;

    /* since there are only three columns in the LP, we can statically allocate the vectors */
    int indices[4] = {0, 1, 2, 3};
    double values[4] = {0.0, 0.0, 0.0, 0.0};

    size_t s, i;
    for(s = 0; s < mdp_avg->ord; ++s) {
        List **pv_prime = list_copy(pv);

        while(!list_empty(pv_prime)) {
            /* extract an item */
            Pareto_vector_element *item = list_first(pv_prime);
            List *temp = *pv_prime;
            *pv_prime = (*pv_prime)->next;
            efree(temp);

            double v_i_min = item->gain_min[s];
            double v_i_avg = item->gain_avg[s];
            double v_i_max = item->gain_max[s];
            double v_i[3] = {item->gain_min[s], item->gain_avg[s], item->gain_max[s]};

            short stop;

            do {
                /* solve a chained optimization problem */
                List **it = pv;
                size_t pv_length = insertions;
                dd_MatrixPtr matrix = dd_CreateMatrix(6 + pv_length - 1, 4); /* 4 stands for x, rhs */
                /* b - Ax >= 0 */
                /* since we give -A, then we reformulate the problem as Ax + b >= 0 */

                /* create LP */
                glp_prob *lp = glp_create_prob();
                glp_add_cols(lp, 3);
                glp_set_col_name(lp, 1, "u_min");
                glp_set_col_name(lp, 2, "u_avg");
                glp_set_col_name(lp, 3, "u_max");
                glp_set_prob_name(lp, "problem");

                /**
                 * NOTE
                 *
                 * Mathematically, we do not have to set the bounds. However, otherwise, the LP optimizers
                 * (including, but not limited to, CPLEX, lp_solve and GLPK) all say the problem is unbounded.
                 *
                 * Hence, we physically bound the variables with the best known upper bounds which are the
                 * optimal values for the extreme weights.
                 */
                if(comp_max) {
                    glp_set_obj_dir(lp, GLP_MAX);
                    /* glp_set_col_bnds(lp, 1, GLP_UP, -DBL_MAX, min_pair->gain_min[s]); */
                    glp_set_col_bnds(lp, 1, GLP_FR, -DBL_MAX, DBL_MAX);
                    glp_set_col_bnds(lp, 2, GLP_FR, -DBL_MAX, DBL_MAX);
                    /* glp_set_col_bnds(lp, 2, GLP_UP, -DBL_MAX, avg_pair->gain_avg[s]); */
                    glp_set_col_bnds(lp, 3, GLP_UP, -DBL_MAX, max_pair->gain_max[s]);
                } else {
                    glp_set_obj_dir(lp, GLP_MIN);
                    glp_set_col_bnds(lp, 1, GLP_LO, min_pair->gain_min[s], DBL_MAX);
                    /* glp_set_col_bnds(lp, 2, GLP_LO, avg_pair->gain_avg[s], DBL_MAX); */
                    /* glp_set_col_bnds(lp, 3, GLP_LO, max_pair->gain_max[s], DBL_MAX); */
                    glp_set_col_bnds(lp, 2, GLP_FR, -DBL_MAX, DBL_MAX);
                    glp_set_col_bnds(lp, 3, GLP_FR, -DBL_MAX, DBL_MAX);
                }
                glp_add_rows(lp, pv_length + 2);

                /* enforce min \le avg, common constraint for all subproblems */
                values[1] = -1.0;
                values[2] = 1.0;
                values[3] = 0.0;
                glp_set_mat_row(lp, pv_length + 0, 3, indices, values);
                glp_set_row_bnds(lp, pv_length + 0, GLP_LO, 0.0, 0.0); /* the upper bound is ignored */

                values[1] = 0.0;
                values[2] = -1.0;
                values[3] = 1.0;
                glp_set_mat_row(lp, pv_length + 1, 3, indices, values);
                glp_set_row_bnds(lp, pv_length + 1, GLP_LO, 0.0, 0.0);

                size_t dd_index = 0;
                size_t lp_index = 1; /* GLPK counts from one. This is irritating. */

                while(!list_empty(it)) {
                    /*
                     * w_a ( v_avg_i - v_avg_j ) + w_l ( v_min_i - v_min_j ) + w_u ( v_max_i - v_max_j ) >= 0
                     * w ( v_avg_i - v_avg_j - v_min_i + v_min_j ) >= - v_min_i + v_min_j
                     */
                    Pareto_vector_element *p = ((Pareto_vector_element *) (*it)->elem);
                    double p_gain[3] = {p->gain_min[s], p->gain_avg[s], p->gain_max[s]};
                    double p_weight[3] = {p->weight_min, p->weight_avg, p->weight_max};

                    if(compare_weight(p, item)) {
                        double constraint = 0.0;

                        dd_set_d(matrix->matrix[dd_index][0], 0.0);

                        for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                            double d = v_i[i] - p_gain[i];
                            dd_set_d(matrix->matrix[dd_index][i + 1], d);

                            /* introduce LP constraints */
                            constraint += p_weight[i] * p_gain[i];
                            values[i + 1] = p_weight[i];
                        }
                        ++dd_index;
                        /* w_j_min(v_j_s_min - u_min) + w_j_avg(v_j_s_avg - u_avg) + w_j_max(v_j_s_max - u_max) \ge 0 */


                        glp_set_mat_row(lp, lp_index, 3, indices, values);
                        if(comp_max) {
                            glp_set_row_bnds(lp, lp_index, GLP_UP, 0.0, constraint);
                        } else {
                            glp_set_row_bnds(lp, lp_index, GLP_LO, constraint, 0.0);
                        }
                        ++lp_index;
                    }
                    it = list_next(it);
                }
                /* fixed constraints */
                {
                    /* enforce boundary constraints, x_i >= 0 */
                    for(i = MIN_IDX; i <= MAX_IDX; ++i) {
                        dd_set_si(matrix->matrix[dd_index + i][0], 0);
                        size_t j;
                        for(j = MIN_IDX; j <= MAX_IDX; ++j) {
                            int coef = (i == j) ? 1 : 0;
                            dd_set_si(matrix->matrix[dd_index + i][j + 1], coef);
                        }
                    }
                    /* enforce sum constraint, 1 + (- x_1 - x_2 - x_3) == 0 */
                    dd_set_si(matrix->matrix[dd_index + 3][0], 1);
                    dd_set_si(matrix->matrix[dd_index + 3][1], -1);
                    dd_set_si(matrix->matrix[dd_index + 3][2], -1);
                    dd_set_si(matrix->matrix[dd_index + 3][3], -1);

                    dd_set_si(matrix->matrix[dd_index + 4][0], -1);
                    dd_set_si(matrix->matrix[dd_index + 4][1], 1);
                    dd_set_si(matrix->matrix[dd_index + 4][2], 1);
                    dd_set_si(matrix->matrix[dd_index + 4][3], 1);

                    dd_set_si(matrix->matrix[dd_index + 5][0], 0);
                    dd_set_si(matrix->matrix[dd_index + 5][1], 0);
                    dd_set_si(matrix->matrix[dd_index + 5][2], 0);
                    dd_set_si(matrix->matrix[dd_index + 5][3], 0);

                    /* set_addelem(matrix->linset, dd_index + 4); */
                    if(dim == min_avg) {
                        dd_set_si(matrix->matrix[dd_index + 5][3], -1); /* consider only w_min and w_avg, w_max = 0 */
                    } else if(dim == avg_max) {
                        dd_set_si(matrix->matrix[dd_index + 5][1], -1); /* w_min = 0 */
                    } else if(dim == min_max) {
                        dd_set_si(matrix->matrix[dd_index + 5][2], -1); /* w_avg = 0 */
                    }
                }

                /* find all vertices */
                matrix->representation = dd_Inequality;
                dd_PolyhedraPtr poly = dd_DDMatrix2Poly(matrix, &err);
                dd_MatrixPtr points;

                if(err == dd_NumericallyInconsistent) {
                    /* oh. sorry. Too many hyperplanes. */
                    break;
                }
                assert(err == dd_NoError);
                points = dd_CopyGenerators(poly);

                double w_min = 0.0;
                double w_avg = 0.0;
                double w_max = 0.0;

                double u_min = 0.0;
                double u_avg = 0.0;
                double u_max = 0.0;

                double v_max = -DBL_MAX;

                size_t k;
                for(k = 0; k < points->rowsize; ++k) {
                    double is_vertex = dd_get_d(points->matrix[k][0]);
                    double weight_min = dd_get_d(points->matrix[k][1]);
                    double weight_avg = dd_get_d(points->matrix[k][2]);
                    double weight_max = dd_get_d(points->matrix[k][3]);
                    short nontrivial = weight_min + epsilon < 1.0 && weight_avg + epsilon < 1.0 && weight_max + epsilon < 1.0;

                    if(is_vertex == 1 && nontrivial) {

                        glp_set_obj_coef(lp, 1, weight_min);
                        glp_set_obj_coef(lp, 2, weight_avg);
                        glp_set_obj_coef(lp, 3, weight_max);

                        glp_simplex(lp, NULL);
                        double v = glp_get_obj_val(lp);
                        if(glp_get_status(lp) != GLP_OPT) {
                            printf("[BMDP_COMPUTE_HULL] There is something seriously wrong with the LP");
                            glp_write_lp(lp, NULL, "problematic_lp.lp");
                            glp_write_mps(lp, GLP_MPS_FILE, 0, "problematic_lp.mps");
                        }
                        assert(glp_get_status(lp) == GLP_OPT);

                        double value = item->gain_min[s] * weight_min + item->gain_avg[s] * weight_avg + item->gain_max[s] * weight_max;
                        if(v - value - epsilon > v_max) {
                            v_max = v - value;
                            w_min = weight_min;
                            w_avg = weight_avg;
                            w_max = weight_max;

                            u_min = glp_get_col_prim(lp, 1);
                            u_avg = glp_get_col_prim(lp, 2);
                            u_max = glp_get_col_prim(lp, 3);
                        }
                    }
                }

                short insert = v_max > epsilon;

                short success = TRUE;
                if(insert) {
                    Pareto_vector_element *new_pair = pareto_element_new_with_weights(mdp_avg->ord, w_min, w_max);
                    void *adr = NULL;
                    short found_in_pv = list_find(pv, &adr, new_pair, compare_weight); /* the common case is existence */
                    short found_in_pv_prime = list_find(pv_prime, &adr, new_pair, compare_weight);
                    if(!found_in_pv) {
                        eval_weight_in_place(mdp_min, mdp_avg, mdp_max, rew_min, rew_avg, rew_max, max_iter, ti_max,
                                             epsilon, method, gamma, new_pair, comp_max);

                        success &= list_insert(pv, &adr, new_pair, new_pair, compare_weight);
                        if(!found_in_pv_prime) {
                            list_insert(pv_prime, &adr, new_pair, new_pair, compare_weight);
                        }
                        if(success) {
                            insertions++;
                        }
                    } else {
                        u_min = v_i_min;
                        u_avg = v_i_avg;
                        u_max = v_i_max;
                    }
                    printf("weights: %f %f %f; already there: %d\n", w_min, w_avg, w_max, !success);
                } else {
                    u_min = v_i_min;
                    u_avg = v_i_avg;
                    u_max = v_i_max;
                }

                stop = (u_min == v_i_min && u_avg == v_i_avg && u_max == v_i_max);

                /* free stuff */
                dd_FreeMatrix(matrix);
                dd_FreePolyhedra(poly);
                dd_FreeMatrix(points);

                glp_delete_prob(lp);
            } while(!stop);
        }
    }
    dd_free_global_constants();
    return pv;
}


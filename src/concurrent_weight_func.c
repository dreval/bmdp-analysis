#include "concurrent_weight_func.h"
#include "ctmdp_stat_func.h"
#include "bmdp_func.h"
#include "polynomial.h"
#include "tst_bmdp.h"
#include "utils.h"
#include <stdbool.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_vector_double.h>
#include <gsl/gsl_poly.h>
#include <gsl/gsl_blas.h>
#include <ilcplex/cplex.h>
#include <nlopt.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

void gsl_dump_matrix(gsl_matrix *matrix)
{
    for (size_t i = 0; i < matrix->size1; ++i) {
        printf("[");
        for (size_t j = 0; j < matrix->size2; ++j) {
            printf("%f\t", gsl_matrix_get(matrix, i, j));
        }
        printf("]\n");
    }
}

gsl_matrix *mdp_policy_matrix(Mdp_matrix *mdp, double **policy)
{
    gsl_matrix *m = gsl_matrix_calloc(mdp->ord, mdp->ord);
    for (size_t s = 0; s < mdp->ord; ++s) {
        for (size_t action = 0; action < mdp->rows[s]->pord; ++action) {
            double action_weight = policy[s][action];
            double t = gsl_matrix_get(m, s, s);
            gsl_matrix_set(m, s, s, t + action_weight * mdp->rows[s]->diagonals[action]);
            for (size_t i = 0; i < (size_t)mdp->rows[s]->no_of_entries[action]; ++i) {
                size_t colind = mdp->rows[s]->colind[action][i];
                double t = gsl_matrix_get(m, s, colind);
                gsl_matrix_set(m, s, colind, t + action_weight * mdp->rows[s]->values[action][i]);
            }
        }
    }
    return m;
}

void update_policy_matrix(Mdp_matrix *mdp, size_t state, size_t action_from, size_t action_to,
                          double lambda, gsl_matrix *policy_matrix)
{
    double diag = gsl_matrix_get(policy_matrix, state, state);
    diag += lambda
        * (mdp->rows[state]->diagonals[action_to] - mdp->rows[state]->diagonals[action_from]);
    gsl_matrix_set(policy_matrix, state, state, diag);
    for (size_t i = 0; i < (size_t)mdp->rows[state]->no_of_entries[action_from]; ++i) {
        size_t colind = mdp->rows[state]->colind[action_from][i];
        double t = gsl_matrix_get(policy_matrix, state, colind);
        gsl_matrix_set(policy_matrix, state, colind,
                       t - lambda * mdp->rows[state]->values[action_from][i]);
    }
    for (size_t i = 0; i < (size_t)mdp->rows[state]->no_of_entries[action_to]; ++i) {
        size_t colind = mdp->rows[state]->colind[action_to][i];
        double t = gsl_matrix_get(policy_matrix, state, colind);
        gsl_matrix_set(policy_matrix, state, colind,
                       t + lambda * mdp->rows[state]->values[action_to][i]);
    }
}

gsl_matrix *mdp_identity_minus_gamma_matrix(Mdp_matrix *mdp, double **policy, double gamma)
{
    gsl_matrix *m = mdp_policy_matrix(mdp, policy);
    gsl_matrix_scale(m, -gamma);
    gsl_matrix_add_diagonal(m, 1.0);
    return m;
}

void update_i_minus_gamma_matrix(Mdp_matrix *mdp, double gamma, size_t state, size_t action_from,
                                 size_t action_to, double lambda, gsl_matrix *policy_matrix)
{
    double diag = gsl_matrix_get(policy_matrix, state, state);
    diag -= gamma * lambda
        * (mdp->rows[state]->diagonals[action_to] - mdp->rows[state]->diagonals[action_from]);
    gsl_matrix_set(policy_matrix, state, state, diag);
    for (size_t i = 0; i < (size_t)mdp->rows[state]->no_of_entries[action_from]; ++i) {
        size_t colind = mdp->rows[state]->colind[action_from][i];
        double t = gsl_matrix_get(policy_matrix, state, colind);
        gsl_matrix_set(policy_matrix, state, colind,
                       t + gamma * lambda * mdp->rows[state]->values[action_from][i]);
    }
    for (size_t i = 0; i < (size_t)mdp->rows[state]->no_of_entries[action_to]; ++i) {
        size_t colind = mdp->rows[state]->colind[action_to][i];
        double t = gsl_matrix_get(policy_matrix, state, colind);
        gsl_matrix_set(policy_matrix, state, colind,
                       t - gamma * lambda * mdp->rows[state]->values[action_to][i]);
    }
}

gsl_matrix *invert_matrix(gsl_matrix *matrix, size_t ord, gsl_matrix *inv)
{
    gsl_permutation *p = gsl_permutation_alloc(ord);
    int i;
    gsl_matrix *temp = gsl_matrix_alloc(ord, ord);
    gsl_matrix_memcpy(temp, matrix);
    gsl_linalg_LU_decomp(temp, p, &i);
    gsl_linalg_LU_invert(temp, p, inv);
    gsl_permutation_free(p);
    gsl_matrix_free(temp);
    return inv;
}

gsl_matrix *mdp_identity_minus_gamma_inv_matrix(Mdp_matrix *mdp, double **policy, double gamma)
{
    gsl_matrix *m = mdp_identity_minus_gamma_matrix(mdp, policy, gamma);
    gsl_permutation *p = gsl_permutation_alloc(mdp->ord);
    int i;
    gsl_matrix *inv = gsl_matrix_alloc(mdp->ord, mdp->ord);
    gsl_linalg_LU_decomp(m, p, &i);
    gsl_linalg_LU_invert(m, p, inv);
    gsl_matrix_free(m);
    gsl_permutation_free(p);
    return inv;
}

void dump_matrices(gsl_matrix **policy_matrices, gsl_matrix **c_matrices,
                   gsl_matrix **cbar_matrices, concurrent_mdp mdps)
{
#ifdef IMPDEBUG
    for (size_t k = 0; k < mdps.scenarios; ++k) {
        printf("=== Scenario %lu, policy matrix ===\n", k);
        gsl_dump_matrix(policy_matrices[k]);
        printf("=== Scenario %lu, I - \\gamma P matrix ===\n", k);
        gsl_dump_matrix(c_matrices[k]);
        printf("=== Scenario %lu, inverted matrix ===\n", k);
        gsl_dump_matrix(cbar_matrices[k]);
    }
#endif /* IMPDEBUG */
}

void get_uv_vector(Mdp_matrix *mdp, size_t state, size_t action_from, size_t action_to,
                   double gamma, gsl_vector *uv)
{
    gsl_vector_set_zero(uv);

    Mdp_row *row = mdp->rows[state];
    gsl_vector_set(uv, state, row->diagonals[action_to] - row->diagonals[action_from]);
    for (size_t i = 0; i < row->no_of_entries[action_to]; ++i) {
        const size_t index = row->colind[action_to][i];
        gsl_vector_set(uv, index, row->values[action_to][i]);
    }
    for (size_t i = 0; i < row->no_of_entries[action_from]; ++i) {
        const size_t index = row->colind[action_from][i];
        double val = gsl_vector_get(uv, index);
        gsl_vector_set(uv, index, val - row->values[action_from][i]);
    }
    gsl_vector_scale(uv, -gamma);
}

gsl_vector *****make_uv_vectors(concurrent_mdp mdps, double gamma)
{
    const size_t ord = mdps.matrices[0]->ord;
    const size_t actions = mdps.matrices[0]->rows[0]->pord;
    gsl_vector *****uv = (gsl_vector *****)calloc(mdps.scenarios, sizeof(gsl_vector ****));

    for (size_t k = 0; k < mdps.scenarios; ++k) {
        uv[k] = (gsl_vector ****)calloc(ord, sizeof(gsl_vector ***));
        for (size_t s = 0; s < ord; ++s) {
            uv[k][s] = (gsl_vector ***)calloc(actions, sizeof(gsl_vector **));
            for (size_t a = 0; a < actions; ++a) {
                uv[k][s][a] = (gsl_vector **)calloc(actions, sizeof(gsl_vector *));
                for (size_t aprime = 0; aprime < actions; ++aprime) {
                    if (aprime != a) {
                        uv[k][s][a][aprime] = gsl_vector_alloc(ord);
                        get_uv_vector(mdps.matrices[k], s, a, aprime, gamma, uv[k][s][a][aprime]);
                    }
                }
            }
        }
    }
    return uv;
}

void remove_uv_vectors(concurrent_mdp mdps, gsl_vector *****uv_vectors)
{
    const size_t ord = mdps.matrices[0]->ord;
    const size_t actions = mdps.matrices[0]->rows[0]->pord;

    for (size_t k = 0; k < mdps.scenarios; ++k) {
        for (size_t s = 0; s < ord; ++s) {
            for (size_t a = 0; a < actions; ++a) {
                for (size_t aprime = 0; aprime < actions; ++aprime) {
                    if (aprime != a) {
                        gsl_vector_free(uv_vectors[k][s][a][aprime]);
                    }
                }
                free(uv_vectors[k][s][a]);
            }
            free(uv_vectors[k][s]);
        }
        free(uv_vectors[k]);
    }
    free(uv_vectors);
}

void update_all_matrices(gsl_matrix **policy_matrices, gsl_matrix **c_matrices,
                         gsl_matrix **cbar_matrices, concurrent_mdp mdps, double gamma,
                         size_t state, size_t action_from, size_t action_to, double lambda,
                         short full_update, gsl_vector *****uv_cache, gsl_vector *cbars)
{
    const size_t ord = mdps.matrices[0]->ord;
    for (size_t k = 0; k < mdps.scenarios; ++k) {
        update_policy_matrix(mdps.matrices[k], state, action_from, action_to, lambda,
                             policy_matrices[k]);
        update_i_minus_gamma_matrix(mdps.matrices[k], gamma, state, action_from, action_to, lambda,
                                    c_matrices[k]);
        if (full_update) {
            invert_matrix(c_matrices[k], ord, cbar_matrices[k]);
        } else {
            // get_uv_vector(mdps.matrices[k], state, action_from, action_to, gamma, uv);
            gsl_matrix_get_col(cbars, cbar_matrices[k], state);

            double eta;
            gsl_blas_ddot(uv_cache[k][state][action_from][action_to], cbars, &eta);

            const double coeff = -lambda / (1 + lambda * eta);

            gsl_matrix_view uv_matrix =
                gsl_matrix_view_vector(uv_cache[k][state][action_from][action_to], 1, ord);
            gsl_matrix_view cbars_matrix = gsl_matrix_view_vector(cbars, ord, 1);

            gsl_matrix *product = gsl_matrix_alloc(ord, ord);
            gsl_matrix *temp = gsl_matrix_alloc(ord, ord);

            gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, &cbars_matrix, &uv_matrix, 0.0,
                           product);
            gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, coeff, product, cbar_matrices[k], 0.0, temp);

            /* use Intel dgemm */
            /*
            cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, (int) ord, (int) ord, (int) 1,
                        1.0,
                        cbars_matrix.matrix.data, (int) cbars_matrix.matrix.tda,
                        uv_matrix.matrix.data, (int) uv_matrix.matrix.tda,
                        0.0, product->data, (int) product->tda);

            cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, (int) ord, (int) ord, (int) ord,
                        coeff, cbar_matrices[k]->data, (int) cbar_matrices[k]->tda,
                        product->data, (int) product->tda,
                        0.0, temp->data, (int) temp->tda);
            */

            gsl_matrix_add(cbar_matrices[k], temp);
            gsl_matrix_free(temp);
            gsl_matrix_free(product);
        }
    }
    dump_matrices(policy_matrices, c_matrices, cbar_matrices, mdps);
}

gsl_vector *policy_delta_vector(Mdp_matrix *mdp, size_t state, double *policy, size_t action_from,
                                size_t action_to, double gamma)
{
    gsl_vector *uv = gsl_vector_alloc(mdp->ord);
    gsl_vector *tv = gsl_vector_alloc(mdp->ord);
    gsl_vector_set_zero(uv);
    gsl_vector_set_zero(tv);

    gsl_vector_set(uv, state, 1.0 - gamma * mdp->rows[state]->diagonals[action_from]);
    gsl_vector_set(tv, state, 1.0 - gamma * mdp->rows[state]->diagonals[action_to]);
    for (size_t i = 0; i < mdp->rows[state]->no_of_entries[action_from]; ++i) {
        const size_t index = mdp->rows[state]->colind[action_from][i];
        gsl_vector_set(uv, index, -gamma * mdp->rows[state]->values[action_from][i]);
    }
    for (size_t i = 0; i < mdp->rows[state]->no_of_entries[action_to]; ++i) {
        const size_t index = mdp->rows[state]->colind[action_to][i];
        gsl_vector_set(tv, index, -gamma * mdp->rows[state]->values[action_to][i]);
    }
    gsl_vector_scale(uv, policy[action_from]);
    gsl_vector_scale(tv, -policy[action_to]);
    gsl_vector_add(uv, tv);
    gsl_vector_free(tv);

    return uv;
}

void mdp_gradient_zeta_eta(Mdp_matrix *mdp, Double_vektor **rew, gsl_matrix *cbar_matrix,
                           gsl_vector *iniv, size_t state, size_t action_from, size_t action_to,
                           double gamma, double *zeta, double *eta, gsl_vector *cached_cbar_r,
                           gsl_vector *uv, gsl_vector *cbars)
{
    /* \zeta = \vec{q}^T \mat{C}(\bullet s) \vec{u} \mat{C} \vec{r} */
    /* \eta = \vec{u}^T \mat{C}(\bullet s) */
    /* \bar{\mat{C}} */
    gsl_matrix *cbar = cbar_matrix;
    /* \vec{u} */
    /* should be slready cached */
    // get_uv_vector(mdp, state, action_from, action_to, gamma, uv);

    /* \bar{\mat{C}}(\bullet s) */
    gsl_matrix_get_col(cbars, cbar, state);

    /* \eta = \vec{u} \mat{C}(\bullet s) */
    gsl_blas_ddot(uv, cbars, eta);

    double dot_iniv_cbars;
    gsl_blas_ddot(iniv, cbars, &dot_iniv_cbars);

    double dot_u_cbar_r = 0;
    /* compute \vec{u} (\mat{C} \vec{r}) */
    gsl_blas_ddot(uv, cached_cbar_r, &dot_u_cbar_r);

    *zeta = dot_iniv_cbars * dot_u_cbar_r;
}

double mdp_gradient(Mdp_matrix *mdp, Double_vektor **rew, gsl_matrix *cbar_matrix, gsl_vector *iniv,
                    double **policy, size_t state, size_t action_from, size_t action_to,
                    double gamma, double lambda, gsl_vector *ucbar, gsl_vector ****uv_cache,
                    gsl_vector *cbars, double *etap, double *zetap)
{
    double zeta = 0;
    double eta = 0;

    if (etap == NULL || zetap == NULL) {
        mdp_gradient_zeta_eta(mdp, rew, cbar_matrix, iniv, state, action_from, action_to, gamma,
                              &zeta, &eta, ucbar, uv_cache[state][action_from][action_to], cbars);
    }
    if (etap != NULL) {
        eta = *etap;
    }
    if (zetap != NULL) {
        zeta = *zetap;
    }
    double gradient = -lambda * zeta / (1.0 + lambda * eta);

    return gradient;
}

double concurrent_mdp_gradient(concurrent_mdp mdps, weight_tuple weights,
                               gsl_matrix **cbar_matrices, double **policy, size_t state,
                               size_t action_from, size_t action_to, double gamma, double lambda,
                               gsl_vector **cached_cbar_r, gsl_vector *****uv_cache,
                               gsl_vector *cbars, double *etas, double *zetas)
{
    double sum = 0.0;
    /* create initial vector */
    gsl_vector *iniv = gsl_vector_alloc(mdps.matrices[0]->ord);
    for (size_t i = 0; i < mdps.matrices[0]->ord; ++i) {
        gsl_vector_set(iniv, i, mdps.iniv[i]);
    }

    for (size_t i = 0; i < mdps.scenarios; ++i) {
        double *etap = NULL;
        double *zetap = NULL;
        if (etas != NULL) {
            etap = &(etas[i]);
        }
        if (zetas != NULL) {
            zetap = &(zetas[i]);
        }
        sum += weights.weights[i] * mdp_gradient(mdps.matrices[i], mdps.rewards[i],
                                                 cbar_matrices[i], iniv, policy, state, action_from,
                                                 action_to, gamma, lambda, cached_cbar_r[i],
                                                 uv_cache[i], cbars, etap, zetap);
    }
    gsl_vector_free(iniv);
    return sum;
}

/**
 * @brief concurrent_impp Local policy iteration optimization algorithm
 * @param mdps The scenario MDPs
 * @param policy A (deterministic) policy vector
 * @param gamma The discount factor
 * @param comp_max if we are amximizing
 * @return The value vectors of the best found policy
 */
value_vectors concurrent_impp(concurrent_mdp mdps, weight_tuple weights, size_t *policy,
                              double gamma, short comp_max, double *value, short presolved)
{
    /* initialize a policy */
    /* policy = (size_t *) calloc(mdps.scenarios, sizeof(size_t)); */
    const size_t ord = mdps.matrices[0]->ord;
    const size_t num_actions = mdps.matrices[0]->rows[0]->pord;

    double **stochastic_policy = (double **)calloc(ord, sizeof(double *));
    double *stochastic_policy_block = (double *)calloc(ord * num_actions, sizeof(double));

    gsl_matrix **policy_matrices = calloc(mdps.scenarios, sizeof(gsl_matrix *));
    gsl_matrix **c_matrices = calloc(mdps.scenarios, sizeof(gsl_matrix *));
    gsl_matrix **cbar_matrices = calloc(mdps.scenarios, sizeof(gsl_matrix *));
    gsl_vector **cached_cbar_r = (gsl_vector **)calloc(mdps.scenarios, sizeof(gsl_vector *));
    gsl_vector *uv = gsl_vector_alloc(ord);
    gsl_vector *cbars = gsl_vector_alloc(ord);
    gsl_vector *****uv_cache = make_uv_vectors(mdps, gamma);
    gsl_vector_view iniv = gsl_vector_view_array(mdps.iniv, ord);

    if (!presolved) {
        void *p = bmdp_policy_iteration(mdps.matrices[0], mdps.rewards[0], policy, 1000, 100, 1e-7,
                                        gamma, comp_max, FALSE);
        free(p);
    }

    for (size_t s = 0; s < ord; ++s) {
        stochastic_policy[s] = &(stochastic_policy_block[num_actions * s]);
        for (size_t a = 1; a < num_actions; ++a) {
            stochastic_policy[s][a] = 0.0;
        }
        stochastic_policy[s][policy[s]] = 1.0;
    }

    /* evaluate the policy */
    value_vectors vecs;
    vecs.gains = (double **)calloc(mdps.scenarios, sizeof(double *));
    vecs.ord = mdps.matrices[0]->ord;
    vecs.scenarios = mdps.scenarios;
    double save_value = 0.0;
    double alt_value = 0.0;

    for (size_t i = 0; i < mdps.scenarios; ++i) {
        policy_matrices[i] = mdp_policy_matrix(mdps.matrices[i], stochastic_policy);
        c_matrices[i] = mdp_identity_minus_gamma_matrix(mdps.matrices[i], stochastic_policy, gamma);
        cbar_matrices[i] =
            mdp_identity_minus_gamma_inv_matrix(mdps.matrices[i], stochastic_policy, gamma);
        cached_cbar_r[i] = gsl_vector_alloc(ord);
    }

    bool found = false;
    const size_t max_iter = 10000;
    size_t iter = 0;
    double t0 = mat_timeused();
    double dt = 0.0;
    do {
        found = false;

        // generate cached matrix-vector products
        for (size_t i = 0; i < mdps.scenarios; ++i) {
            gsl_vector_view rvec = gsl_vector_view_array(mdps.rewards[i][0]->vektor, ord);
            gsl_blas_dgemv(CblasNoTrans, 1.0, cbar_matrices[i], &rvec, 0.0, cached_cbar_r[i]);
            if (iter == 0) {
                double dot;
                gsl_blas_ddot(cached_cbar_r[i], &iniv, &dot);
                save_value += weights.weights[i] * dot;
            }
        }

        for (size_t s = 0; s < ord && !found; ++s) {
            for (size_t a = 0; a < num_actions && !found; ++a) {
                if (a != policy[s]) {
                    double gradient = concurrent_mdp_gradient(
                        mdps, weights, cbar_matrices, stochastic_policy, s, policy[s], a, gamma,
                        1.0, cached_cbar_r, uv_cache, cbars, NULL, NULL);
                    double coeff = comp_max ? 1.0 : -1.0;
                    if (coeff * gradient > 1e-8) {
                        stochastic_policy[s][a] = 1.0;
                        stochastic_policy[s][policy[s]] = 0.0;
                        update_all_matrices(policy_matrices, c_matrices, cbar_matrices, mdps, gamma,
                                            s, policy[s], a, 1.0, (iter % 100 == 0), uv_cache,
                                            cbars);
                        policy[s] = a;
                        found = true;
                        save_value += gradient;
                    }
                }
            }
        }
        ++iter;
        dt = mat_timeused() - t0;
    } while (found && iter < max_iter && dt < CMDP_MAX_TIME);

    *value = 0.0;

    for (size_t i = 0; i < mdps.scenarios; ++i) {
        // evaluate the policy, again.
        gsl_vector_view rvec = gsl_vector_view_array(mdps.rewards[i][0]->vektor, ord);
        gsl_blas_dgemv(CblasNoTrans, 1.0, cbar_matrices[i], &rvec, 0.0, cached_cbar_r[i]);
        double dot;
        gsl_blas_ddot(cached_cbar_r[i], &iniv, &dot);
        alt_value += weights.weights[i] * dot;

        gsl_matrix_free(policy_matrices[i]);
        gsl_matrix_free(c_matrices[i]);
        gsl_matrix_free(cbar_matrices[i]);
        gsl_vector_free(cached_cbar_r[i]);

        vecs.gains[i] =
            bmdp_evaluate_avg_policy(mdps.matrices[i], policy, mdps.rewards[i], gamma, FALSE);

        for (size_t s = 0; s < ord; ++s) {
            *value += weights.weights[i] * mdps.iniv[s] * vecs.gains[i][s];
        }
    }
    stdlog(LOG_TRACE, "impp", "%.5f accurate vs %.5f estimated vs %.5f computed by MV", *value, save_value, alt_value);

    gsl_vector_free(uv);
    gsl_vector_free(cbars);
    free(policy_matrices);
    free(c_matrices);
    free(cbar_matrices);
    free(stochastic_policy_block);
    free(stochastic_policy);
    free(cached_cbar_r);
    remove_uv_vectors(mdps, uv_cache);

    return vecs;
}

value_vectors concurrent_imgp(concurrent_mdp mdps, weight_tuple weights, double **policy,
                              double gamma, short comp_max, double *value, short presolved)
{
    /* TODO test performance, seems to work */
    /* initialize a policy */
    /* policy = (size_t *) calloc(mdps.scenarios, sizeof(size_t)); */
    const size_t ord = mdps.matrices[0]->ord;
    const size_t num_actions = mdps.matrices[0]->rows[0]->pord;

    gsl_matrix **policy_matrices = calloc(mdps.scenarios, sizeof(gsl_matrix *));
    gsl_matrix **c_matrices = calloc(mdps.scenarios, sizeof(gsl_matrix *));
    gsl_matrix **cbar_matrices = calloc(mdps.scenarios, sizeof(gsl_matrix *));
    gsl_vector *uv = gsl_vector_alloc(ord);
    gsl_vector *cbars = gsl_vector_alloc(ord);
    gsl_vector *****uv_cache = make_uv_vectors(mdps, gamma);

    // printf("ord = %lu, act = %lu\n", ord, num_actions);

    if (!presolved) {
        for (size_t s = 0; s < ord; ++s) {
            for (size_t a = 0; a < num_actions; ++a) {
                policy[s][a] = 1.0 / num_actions;
            }
        }
    }

    double temp[1] = { 1.0 };
    double temp_coeffs[3] = { 0.0, 0.0, 0.0 };

    /* create initial helper vector */
    gsl_vector *iniv = gsl_vector_alloc(mdps.matrices[0]->ord);
    for (size_t i = 0; i < ord; ++i) {
        gsl_vector_set(iniv, i, mdps.iniv[i]);
    }

    /* evaluate the policy */
    value_vectors vecs;
    vecs.gains = (double **)calloc(mdps.scenarios, sizeof(double *));
    vecs.ord = ord;
    vecs.scenarios = mdps.scenarios;

    gsl_vector **cached_cbar_r = (gsl_vector **)calloc(mdps.scenarios, sizeof(gsl_vector *));

    for (size_t i = 0; i < mdps.scenarios; ++i) {
        policy_matrices[i] = mdp_policy_matrix(mdps.matrices[i], policy);
        c_matrices[i] = mdp_identity_minus_gamma_matrix(mdps.matrices[i], policy, gamma);
        cbar_matrices[i] = mdp_identity_minus_gamma_inv_matrix(mdps.matrices[i], policy, gamma);
        cached_cbar_r[i] = gsl_vector_alloc(ord);
    }

    dump_matrices(policy_matrices, c_matrices, cbar_matrices, mdps);
    bool found = false;
    const size_t max_iter = 10000;
    double t0 = mat_timeused();
    double dt = 0.0;
    size_t iter = 0;

    do {
        found = false;
        // generate cached matrix-vector products
        for (size_t i = 0; i < mdps.scenarios; ++i) {
            gsl_vector_view rvec = gsl_vector_view_array(mdps.rewards[i][0]->vektor, ord);
            gsl_blas_dgemv(CblasNoTrans, 1.0, cbar_matrices[i], &rvec, 0.0, cached_cbar_r[i]);
        }

        for (size_t s = 0; s < ord && !found; ++s) {
            for (size_t a = 0; a < num_actions && !found; ++a) {
                if (policy[s][a] > 0) {
#ifdef IMPDEBUG
                    printf("pi[%lu][%lu] = %f\n", s, a, policy[s][a]);
#endif /* IMPDEBUG */
                    for (size_t aprime = 0; aprime < num_actions; ++aprime) {
                        if (a != aprime) {
                            /* construct polynomial whose roots are possible extreme points of the
                             * gradient function */
                            polynomial p = make_polynomial();

                            /* compute etas and zetas */
                            double *etas = (double *)calloc(weights.scenarios, sizeof(double));
                            double *zetas = (double *)calloc(weights.scenarios, sizeof(double));

                            for (size_t k = 0; k < weights.scenarios; ++k) {
                                etas[k] = zetas[k] = 0.0;
                                mdp_gradient_zeta_eta(mdps.matrices[k], mdps.rewards[k],
                                                      cbar_matrices[k], iniv, s, a, aprime, gamma,
                                                      &(zetas[k]), &(etas[k]), cached_cbar_r[k],
                                                      uv_cache[k][s][a][aprime], cbars);
                            }

                            for (size_t k = 0; k < weights.scenarios; ++k) {
                                polynomial p_k = make_polynomial();

                                for (size_t kprime = 0; kprime < weights.scenarios; ++kprime) {
                                    if (kprime != k) {
                                        polynomial one_minus_lambdaeta_squared;
                                        one_minus_lambdaeta_squared.ord = 2;
                                        one_minus_lambdaeta_squared.coeffs = temp_coeffs;

                                        temp_coeffs[0] = 1.0;
                                        temp_coeffs[1] = +2 * etas[kprime];
                                        temp_coeffs[2] = pow(etas[kprime], 2.0);

                                        polynomial_multiply(&p_k, &one_minus_lambdaeta_squared);
                                    }
                                }
                                polynomial_multiply_scalar(&p_k, weights.weights[k] * zetas[k]);
                                polynomial_add(&p, &p_k);
                                free(p_k.coeffs);
                            }

                            polynomial_compress(&p);

                            double *roots = (double *)calloc(2 * p.ord + 2, sizeof(double));
                            if (p.ord == 1) {
                                /* seems to be a bug in GSL */
                                roots[0] = -p.coeffs[0] / p.coeffs[1];
                                roots[1] = 0.0;
                            } else if (p.ord == 2) {
                                double a = p.coeffs[2];
                                double b = p.coeffs[1];
                                double c = p.coeffs[0];
                                double discr = pow(b, 2) - 4 * a * c;
                                if (discr > 0) {
                                    roots[1] = roots[3] = 0.0;
                                    roots[0] = (b - sqrt(discr)) / (2 * a);
                                    roots[2] = (b + sqrt(discr)) / (2 * a);
                                } else {
                                    roots[1] = roots[3] = 1.0;
                                    roots[0] = roots[2] = 1.0;
                                }
                            } else if (p.ord > 0) {
                                gsl_poly_complex_workspace *w =
                                    gsl_poly_complex_workspace_alloc(p.ord + 1);
                                gsl_poly_complex_solve(p.coeffs, p.ord + 1, w, roots);
                                gsl_poly_complex_workspace_free(w);
                            }
                            free(p.coeffs);
                            /* add additional root at one */
                            roots[2 * p.ord] = fmin(policy[s][a], 1.0 - policy[s][aprime]);
                            roots[2 * p.ord + 1] = 0.0;
                            double opt_gradient = 0.0;
                            double lambda_star = 0.0;

                            for (size_t i = 0; i < p.ord + 1; ++i) {
                                /* check root and gradient */

                                /* first, extract real and imaginary parts */
                                const double lambda_re = roots[2 * i];
                                const double lambda_im = roots[2 * i + 1];
                                /*
                                 * Check if the root is interesting.
                                 * Only real roots and those in range [\Pi(s, a), 1 - \Pi(s, a')]
                                 * are interesting.
                                 */
                                if (fabs(lambda_im) < 1e-8 && lambda_re >= 0.0
                                    && lambda_re <= policy[s][a]
                                    && lambda_re <= 1.0 - policy[s][aprime]) {
                                    /* search for the largest gradient */
                                    double gradient = concurrent_mdp_gradient(
                                        mdps, weights, cbar_matrices, policy, s, a, aprime, gamma,
                                        lambda_re, cached_cbar_r, uv_cache, cbars, etas, zetas);
                                    if (comp_max == (gradient > opt_gradient)
                                        && fabs(gradient) > 1.0e-7) {
                                        lambda_star = lambda_re;
                                        opt_gradient = gradient;
                                    }
                                }
                            }
                            free(etas);
                            free(zetas);
                            free(roots);

                            if (lambda_star > 1e-7) {
                                policy[s][a] -= lambda_star;
                                policy[s][aprime] += lambda_star;
                                short full_update = (iter % 100 == 0);
                                update_all_matrices(policy_matrices, c_matrices, cbar_matrices,
                                                    mdps, gamma, s, a, aprime, lambda_star,
                                                    full_update, uv_cache, cbars);
                                found = true;
#ifdef IMPDEBUG
                                double delta_t = mat_timeused() - t0;
                                double value = 0.0;
                                double temp_value = 0.0;
                                //                                gsl_vector value_vector =
                                //                                gsl_vector_alloc(ord);

                                //                                gsl_vector_free(value_vector)
                                printf("iteration %lu, dt = %f, maximal gradient is %f [%lu %lu - "
                                       "%f -> %lu]\n",
                                       iter, delta_t, opt_gradient, s, a, lambda_star, aprime);
#endif /* IMPDEBUG */
                            }
                        }
                    }
                }
            }
        }
        ++iter;
        dt = mat_timeused() - t0;
    } while (found && iter < max_iter && dt < CMDP_MAX_TIME);
    *value = 0.0;

    for (size_t i = 0; i < mdps.scenarios; ++i) {
        gsl_matrix_free(policy_matrices[i]);
        gsl_matrix_free(c_matrices[i]);
        gsl_matrix_free(cbar_matrices[i]);
        gsl_vector_free(cached_cbar_r[i]);

        vecs.gains[i] = bmdp_evaluate_avg_stationary_policy(mdps.matrices[i], policy,
                                                            mdps.rewards[i], gamma, FALSE);

        for (size_t s = 0; s < ord; ++s) {
            *value += weights.weights[i] * mdps.iniv[s] * vecs.gains[i][s];
        }
    }

    gsl_vector_free(iniv);

    gsl_vector_free(uv);
    gsl_vector_free(cbars);

    free(policy_matrices);
    free(c_matrices);
    free(cbar_matrices);
    free(cached_cbar_r);
    remove_uv_vectors(mdps, uv_cache);

    return vecs;
}

value_vectors concurrent_mip(concurrent_mdp mdps, weight_tuple weights, size_t *policy,
                             double gamma, short comp_max, double *value, short presolved)
{
    value_vectors vecs;
    // presolve with local optimization
    int boost = TRUE;
    if (boost) {
        vecs = concurrent_impp(mdps, weights, policy, gamma, comp_max, value, presolved);
    } else {
        vecs.ord = mdps.matrices[0]->ord;
        vecs.scenarios = mdps.scenarios;
        vecs.gains = calloc(mdps.scenarios, sizeof(double *));
    }
    /* initialize some useful constants */
    const size_t ord = mdps.matrices[0]->ord;
    const size_t actions = mdps.matrices[0]->rows[0]->pord;
    /* temporary buffer, faster than allocating & freeing */
    char buf[32] = "";

    int status;
    /* Initialize CPLEX environment */
    CPXENVptr env = CPXopenCPLEX(&status);
    if (status) {
        fprintf(stderr, "[concurrent_mip] Opening CPLEX failed! CPLEX error code: %d", status);
    }
    /* set parameters: Output, Data checking, ... */
    /* please do not tell us something... */
    CPXsetintparam(env, CPXPARAM_ScreenOutput, CPX_OFF);
    /* check the data */
    CPXsetintparam(env, CPXPARAM_Read_DataCheck, CPX_ON);
    /* automatically parallelize */
    CPXsetintparam(env, CPXPARAM_Threads, 1);
    CPXsetintparam(env, CPXPARAM_Parallel, CPX_PARALLEL_AUTO);
    /* set time limit */
    CPXsetdblparam(env, CPXPARAM_TimeLimit, CMDP_MAX_TIME);
    /* create problem */
    CPXLPptr lp = CPXcreateprob(env, &status, "Concurrent MDP");
    if (status) {
        fprintf(stderr, "[concurrent_mip] Creating LP failed! CPLEX error code: %d", status);
    }
    CPXchgobjsen(env, lp, CPX_MAX);
    CPXchgprobtype(env, lp, CPXPROB_MILP);
    /* add variables */
    int *decision_indices = calloc(ord * actions, sizeof(int));
    char *decision_ctype = calloc(ord * actions, sizeof(char));
    char **decision_colnames = calloc(ord * actions, sizeof(char *));
    char **cont_colnames = calloc(ord * actions * weights.scenarios, sizeof(char *));
    double *ones_decision_rhs = calloc(ord, sizeof(double));
    char *sense_decision_rhs = calloc(ord, sizeof(char));
    char *coupling_row_senses = calloc(ord * actions * weights.scenarios, sizeof(char));
    size_t i = 0;
    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            decision_indices[i] = (int)i;
            decision_ctype[i] = 'B';
            decision_colnames[i] = calloc(32, sizeof(char));
            sprintf(decision_colnames[i], DECISION_VARIABLE_NAME_PATTERN, s, a);
            for (size_t k = 0; k < weights.scenarios; ++k) {
                size_t idx = (size_t)cont_variable_index(ord, actions, k, s, a);
                cont_colnames[idx] = calloc(32, sizeof(char));
                sprintf(cont_colnames[idx], CONT_VARIABLE_NAME_PATTERN, k, s, a);
                coupling_row_senses[idx] = 'G';
            }
            ++i;
        }
        ones_decision_rhs[s] = 1.0;
        sense_decision_rhs[s] = 'L';
    }
    /* decision variables */
    CPXnewcols(env, lp, (int)(ord * actions), NULL, NULL, NULL, decision_ctype, decision_colnames);
    /* continuous variables */
    CPXnewcols(env, lp, (int)(weights.scenarios * ord * actions), NULL, NULL, NULL, NULL,
               cont_colnames);
    /* now add constraints */

    /* \sum_a d_{s, a} = 1 */
    CPXnewrows(env, lp, (int)ord, ones_decision_rhs, NULL, NULL, NULL);
    char **decision_constraint_row_names = (char **)calloc(ord, sizeof(char *));
    for (size_t s = 0; s < ord; ++s) {
        int decision_constraint_row = (int)s;
        decision_constraint_row_names[s] = (char *)calloc(32, sizeof(char));
        sprintf(decision_constraint_row_names[s], DECISION_CONSTRAINT_NAME_PATTERN, s);
        CPXchgrowname(
            env, lp, 1, &decision_constraint_row,
            &(decision_constraint_row_names[s])); /* looks fishy to the C compiler, should work */
        for (size_t a = 0; a < actions; ++a) {
            int decison_variable_s_a = decision_variable_index(ord, actions, s, a);
            CPXchgcoef(env, lp, decision_constraint_row, decison_variable_s_a, 1.0);
        }
    }
    /* \sum_a x_{k, s, a} - \gamma \sum_{a, s'} P_k^a(s', s) x_{k, s', a} \le q(s) */
    char **cont_constraint_names = (char **)calloc(ord * weights.scenarios, sizeof(char *));
    for (size_t k = 0; k < weights.scenarios; ++k) {
        CPXnewrows(env, lp, (int)ord, mdps.iniv, sense_decision_rhs, NULL, NULL);
        for (size_t s = 0; s < ord; ++s) {
            const size_t string_index = k * ord + s;
            cont_constraint_names[string_index] = (char *)calloc(32, sizeof(char));

            sprintf(cont_constraint_names[string_index], CONT_CONSTRAINT_NAME_PATTERN, s, k);
            int row_index = (int)(s + ord * (k + 1));
            CPXchgrowname(env, lp, 1, &row_index, &(cont_constraint_names[string_index]));

            for (size_t a = 0; a < actions; ++a) {
                int x_k_s_a_col_index = freq_variable_index(ord, actions, k, s, a);
                double p_s_s_a = mdps.matrices[k]->rows[s]->diagonals[a];
                /* 1 - \gamma * P_k^a{s, s} */
                CPXchgcoef(env, lp, row_index, x_k_s_a_col_index, (1.0 - gamma * p_s_s_a));
                for (size_t s_prime = 0; s_prime < ord; ++s_prime) {
                    for (size_t i = 0; i < mdps.matrices[k]->rows[s_prime]->no_of_entries[a]; ++i) {
                        if (s == mdps.matrices[k]->rows[s_prime]->colind[a][i]) {
                            int x_k_sprime_col_index =
                                freq_variable_index(ord, actions, k, s_prime, a);
                            double p_sprime_s_a = mdps.matrices[k]->rows[s_prime]->values[a][i];
                            CPXchgcoef(env, lp, row_index, x_k_sprime_col_index,
                                       -gamma * p_sprime_s_a);
                        }
                    }
                }
            }
        }
    }
    int rows_already_added = (int)(ord * (1 + weights.scenarios));
    /* d_{s, a} \ge (1 - \gamma) x_{k, s, a} */
    CPXnewrows(env, lp, (int)(ord * actions * weights.scenarios), NULL, coupling_row_senses, NULL,
               NULL);
    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            int decison_variable_s_a = decision_variable_index(ord, actions, s, a);
            for (size_t k = 0; k < weights.scenarios; ++k) {
                int x_k_s_a_col_index = freq_variable_index(ord, actions, k, s, a);
                int row_index =
                    rows_already_added + (int)cont_variable_index(ord, actions, k, s, a);
                CPXchgcoef(env, lp, row_index, decison_variable_s_a, 1.0);
                CPXchgcoef(env, lp, row_index, x_k_s_a_col_index, (gamma - 1));
            }
        }
    }
    /* objective function coefficients */
    /* first, compute a multiplier depending on optimization direction */
    /* naive way : double cmax = comp_max ? 1.0 : -1.0; */
    /* however, branching is evil */
    double cmax = 2 * comp_max - 1;

    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            for (size_t k = 0; k < weights.scenarios; ++k) {
                double coef = weights.weights[k] * mdps.rewards[k][a]->vektor[s];
                CPXchgcoef(env, lp, -1, freq_variable_index(ord, actions, k, s, a), cmax * coef);
            }
        }
    }
    /* if boosted, write an initial feasible solution */
    double *boost_values = NULL;
    int *boost_indices = NULL;
    if (boost) {
        boost_values = calloc(ord * actions, sizeof(double));
        boost_indices = calloc(ord * actions, sizeof(int));
        int beg[1];
        beg[0] = 0;
        char *startname = "Local optimization solution";
        int effortlevel = CPX_MIPSTART_SOLVEMIP;

        for (size_t s = 0; s < ord; ++s) {
            for (size_t a = 0; a < actions; ++a) {
                boost_indices[a * ord + s] = decision_variable_index(ord, actions, s, a);
                boost_values[a * ord + s] = 0.0;
            }
            boost_values[ord * policy[s] + s] = 1.0;
        }

        CPXaddmipstarts(env, lp, 1, (int)(ord * actions), beg, boost_indices, boost_values,
                        &effortlevel, &startname);
    }
    /* write out ILP */
    /* CPXwriteprob(env, lp, "cmdp_mip.lp", NULL); */
    /* solve ILP */
    status = CPXmipopt(env, lp);
    if (status != 0) {
        fprintf(stderr, "[concurrent_mip] Error while optimizing! Status: %d\n", status);
    }

    /* read result */
    CPXgetobjval(env, lp, value);
    for (size_t s = 0; s < ord; ++s) {
        policy[s] = 0;
        for (size_t a = 0; a < actions; ++a) {
            double d_s_a = 0;
            int index = decision_variable_index(ord, actions, s, a);
            CPXgetx(env, lp, &d_s_a, index, index);
            if (d_s_a > 1e-6) {
                policy[s] = a;
            }
            /* compute value */
            // for(size_t k = 0; k < weights.scenarios; ++k) {
            //    double freq_k_s_a;
            //    int findex = freq_variable_index(ord, actions, k, s, a);
            //    CPXgetx(env, lp, &freq_k_s_a, findex, findex);
            //    vecs.gains[k][s] += freq_k_s_a * mdps.rewards[k][a]->vektor[s];
            //}
        }
    }

    for (size_t i = 0; i < mdps.scenarios; ++i) {
        if (boost) {
            free(vecs.gains[i]);
        }
        vecs.gains[i] =
            bmdp_evaluate_avg_policy(mdps.matrices[i], policy, mdps.rewards[i], gamma, FALSE);
    }

    /* cleanup strings */
    for (size_t s = 0; s < ord; ++s) {
        free(decision_constraint_row_names[s]);
        for (size_t k = 0; k < weights.scenarios; ++k) {
            const size_t string_index = k * ord + s;
            free(cont_constraint_names[string_index]);
        }
    }
    free(cont_constraint_names);
    free(decision_constraint_row_names);
    for (size_t i = 0; i < ord * actions; ++i) {
        free(decision_colnames[i]);
        for (size_t k = 0; k < weights.scenarios; ++k) {
            free(cont_colnames[ord * actions * k + i]);
        }
    }
    free(decision_colnames);
    free(cont_colnames);
    free(ones_decision_rhs);
    free(sense_decision_rhs);
    free(decision_indices);
    free(decision_ctype);
    free(coupling_row_senses);
    if (boost) {
        free(boost_values);
        free(boost_indices);
    }
    CPXfreeprob(env, &lp);
    CPXcloseCPLEX(&env);

    return vecs;
}

int decision_variable_index(size_t ord, size_t actions, size_t s, size_t a)
{
    return (int)(s * actions + a);
}

size_t cont_variable_index(size_t ord, size_t actions, size_t k, size_t s, size_t a)
{
    return (ord * actions * k) + (s * actions) + a;
}

int freq_variable_index(size_t ord, size_t actions, size_t k, size_t s, size_t a)
{
    return (int)(ord * actions + cont_variable_index(ord, actions, k, s, a));
}

cmdp_problem concurrent_read_problem(char *basename)
{
    cmdp_problem prob;
    concurrent_mdp cmdp;

    /* open the file */
    FILE *fp = fopen(basename, "r");
    if (fp == NULL) {
        int error = errno;
        char *dir = getcwd(NULL, 0);
        fprintf(stderr, "Could not open file %s: %d (cwd = %s)!\n", basename, error, dir);
        free(dir);
        exit(error);
    }
    /*
     * Let's talk about the file format. We shall establish something like this:
     * 1x1 matrix (number of scenarios)
     * nxn matrix (P, a = 0, k = 0)
     * nxn matrix (P, a = 1, k = 0)
     * …
     * nxn matrix (r, k = 0)
     * nxn matrix (P, a = 0, k = 1)
     * …
     * nxn matrix (P, a = |A|, k = K)
     * nxn matrix (iniv)
     * kxk matrix (weights)
     *
     * This makes (|A| + 1)K + 2 matrices.
     */

    /* the first matrix in the file contains the number of MDPs */
    const int matrices = read_integer(fp);

    Sparse_matrix *scenarios;
    scenarios = bmdp_read_one_matrix(fp, 1);
    const size_t num_scenarios = (size_t)scenarios->diagonals[0];

    cmdp.scenarios = num_scenarios;
    cmdp.matrices = (Mdp_matrix **)calloc(num_scenarios, sizeof(Mdp_matrix *));
    cmdp.rewards = (Double_vektor ***)calloc(num_scenarios, sizeof(Double_vektor **));

    /* We now know the number of scenarios, which means that we can now know the number of actions
     */
    const size_t actions = (matrices - 3) / num_scenarios - 1;
    Sparse_matrix **temp_matrices = (Sparse_matrix **)calloc(actions, sizeof(Sparse_matrix *));
    size_t ord = 0;

    // printf("%zu scenarios, %zu actions\n", num_scenarios, actions);

    for (size_t k = 0; k < num_scenarios; ++k) {
        // printf("Reading scenario %zu of %zu\n", k, num_scenarios);
        /* read a single MDP. */

        for (size_t a = 0; a < actions; ++a) {
            temp_matrices[a] = bmdp_read_one_matrix(fp, -1);
            // printf("last diagonal entry: %f\n", temp_matrices[a]->diagonals[temp_matrices[a]->ord
            // - 1]);
        }
        ord = (size_t)temp_matrices[0]->ord;

        cmdp.matrices[k] = mdp_matrix_generate_discrete((int)actions, (int)ord, temp_matrices);

        for (size_t a = 0; a < actions; ++a) {
            efree(temp_matrices[a]->l);
            efree(temp_matrices[a]->u);
            efree(temp_matrices[a]->d);
            efree(temp_matrices[a]->diagonals);
            efree(temp_matrices[a]);
        }

        /* allocate space for reward vectors */
        cmdp.rewards[k] = (Double_vektor **)calloc(actions, sizeof(Double_vektor **));

        cmdp.rewards[k][0] = bmdp_read_one_reward_vector(fp, (int)ord);
        for (size_t a = 1; a < actions; ++a) {
            cmdp.rewards[k][a] = cmdp.rewards[k][0];
        }

        /* remove temporary matrices */
        for (size_t a = 0; a < actions; ++a) {
            // free(temp_matrices[a]->diagonals);
            // free(temp_matrices[a]->rowind);
            // mat_sparse_matrix_free(temp_matrices[a]);
        }
    }

    cmdp.iniv = (double *)calloc(ord, sizeof(double));
    Double_vektor *v = bmdp_read_one_reward_vector(fp, (int)ord);
    for (size_t s = 0; s < ord; ++s) {
        cmdp.iniv[s] = v->vektor[s];
    }

    prob.weights.weights = (double *)calloc(num_scenarios, sizeof(double));
    prob.weights.scenarios = num_scenarios;
    Double_vektor *w = bmdp_read_one_reward_vector(fp, (int)num_scenarios);
    for (size_t k = 0; k < num_scenarios; ++k) {
        prob.weights.weights[k] = w->vektor[k];
    }

    /* cleanup */
    mat_double_vektor_free(v);
    mat_double_vektor_free(w);
    free(temp_matrices);
    mat_sparse_matrix_free(scenarios);

    /* close the file */
    fclose(fp);
    prob.mdp = cmdp;
    return prob;
}

short cmdp_check_matrices(concurrent_mdp cmdp)
{
    const size_t scenarios = cmdp.scenarios;
    const size_t ord = cmdp.matrices[0]->ord;
    const size_t actions = cmdp.matrices[0]->rows[0]->pord;

    short valid = TRUE;

    for (size_t k = 0; k < scenarios; ++k) {
        Mdp_matrix *mdp = cmdp.matrices[k];

        /* check consistency */
        if (ord != mdp->ord) {
            valid = FALSE;
            fprintf(stderr, "Error! Scenario %lu has order %lu while scenario 0 has order %lu\n", k,
                    mdp->ord, ord);
        }

        for (size_t s = 0; s < ord; ++s) {
            Mdp_row *row = mdp->rows[s];
            /* again, check consistency */
            if (actions != row->pord) {
                valid = FALSE;
                fprintf(stderr, "Error! Scenario %lu, state %lu has %lu actions while scenario 0 "
                                "state 0 has %lu actions\n",
                        k, s, row->pord, actions);
            }

            for (size_t a = 0; a < actions; ++a) {
                double sum = row->diagonals[a];
                for (size_t i = 0; i < row->no_of_entries[a]; ++i) {
                    sum += row->values[a][i];
                }

                if (fabs(sum - 1.0) > 1e-8) {
                    /* not stochastic */
                    valid = FALSE;
                    fprintf(stderr,
                            "Error! Scenario %lu, state %lu, action %lu has row sum %0.10g\n", k, s,
                            a, sum);
                }
            }
        }
    }
    return valid;
}

value_vectors cmdp_optimize(cmdp_context *context)
{
    value_vectors ret;
    const size_t ord = context->problem->mdp.matrices[0]->ord;
    const size_t actions = context->problem->mdp.matrices[0]->rows[0]->pord;
    size_t *helper_policy = (size_t *)calloc(ord, sizeof(size_t));
    if (context->presolved) {
        for (size_t s = 0; s < ord; ++s) {
            for (size_t a = 0; a < actions; ++a) {
                if (context->policy[s][a] > 0) {
                    helper_policy[s] = a;
                }
            }
        }
    } else {
        if (context->policy != NULL) {
            for (size_t s = 0; s < ord; ++s) {
                free(context->policy[s]);
            }
            free(context->policy);
        }
        context->policy = (double **)calloc(ord, sizeof(double *));
        for (size_t s = 0; s < ord; ++s) {
            context->policy[s] = (double *)calloc(actions, sizeof(double));
            context->policy[s][0] = 1.0;
        }
    }

    if (context->presolve) {
        value_vectors tmp = concurrent_impp(context->problem->mdp, context->problem->weights, helper_policy,
                                            context->problem->gamma, context->problem->comp_max, &(context->result), context->presolved);
        context->presolved = TRUE;
    }

    switch (context->method) {
    case CMDP_METHOD_IMGP:
        ret = concurrent_imgp(context->problem->mdp, context->problem->weights, context->policy,
                              context->problem->gamma, context->problem->comp_max,
                              &(context->result), context->presolved);
        break;
    case CMDP_METHOD_QCLP:
        ret = concurrent_qclp(context->problem->mdp, context->problem->weights, context->policy,
                              context->problem->gamma, context->problem->comp_max,
                              &(context->result), context->presolved);
        break;
    case CMDP_METHOD_NLP:
        ret = concurrent_nlp_ipopt(context->problem->mdp, context->problem->weights,
                                   context->policy, context->problem->gamma,
                                   context->problem->comp_max, &(context->result), context->presolved);
        break;
    case CMDP_METHOD_NLP2:
        ret =
            concurrent_nlp(context->problem->mdp, context->problem->weights, context->policy,
                           context->problem->gamma, context->problem->comp_max, &(context->result), context->presolved);
        break;
    case CMDP_METHOD_IMPP:
        ret = concurrent_impp(context->problem->mdp, context->problem->weights, helper_policy,
                              context->problem->gamma, context->problem->comp_max,
                              &(context->result), context->presolved);
        break;
    case CMDP_METHOD_MIP:
        ret =
            concurrent_mip(context->problem->mdp, context->problem->weights, helper_policy,
                           context->problem->gamma, context->problem->comp_max, &(context->result), context->presolved);
        break;
    }
    if (context->method == CMDP_METHOD_IMPP || context->method == CMDP_METHOD_MIP) {
        for (size_t s = 0; s < ord; ++s) {
            context->policy[s][0] = 0.0; // reset the value
            context->policy[s][helper_policy[s]] = 1.0;
        }
    }
    context->presolved = TRUE;
    free(helper_policy);
    return ret;
}

void concurrent_delete_problem(cmdp_problem cmdp)
{
    free(cmdp.weights.weights);
    free(cmdp.mdp.iniv);
    for (size_t k = 0; k < cmdp.weights.scenarios; ++k) {
        mat_double_vektor_free(cmdp.mdp.rewards[k][0]);
        mdp_matrix_free(cmdp.mdp.matrices[k]);
        efree(cmdp.mdp.rewards[k]);
    }
    free(cmdp.mdp.matrices);
    free(cmdp.mdp.rewards);
}

/*********************
	Datei:	str_global.h
	Datum: 	28.11.96
	Autor:	Peter Buchholz
	Inhalt:	defines
		Datenstrukturen
 		Funktionskoepfe
		globale Variablen
		die fuer alle Module verwendet werden

memo:

	Datum 	Aenderung
        07.05.98 GEN_MATRIX eingefuehrt
        14.06.98 neue Numerierung der Loesungsverfahren
        02.07.98 EInfuegen weiterer Loesungsverfahren (BiCGSTAB)
        03.09.98 Einfuegen weiterer Loesungsverfahren (TFQMR, Pre-Power)
        19.09.07 EInfuegen der L�ser f�r inhomgene Systeme
----------------------------------------------------------------------

       28.11.96 Generierung aus global.h

**********************/
#ifndef STRGLOBAL_H
#define STRGLOBAL_H

#include <stdio.h>
/* #include <strings.h> Versuchsweise ersetzt wegen strdup */
#include <string.h>

#include <assert.h>
#include "ememory.h"
#include "global.h"
#include "mat_time.h"
#include "list.h"
#include "mat_int_vektor.h"

/*** Number of iterations between residual test ***/
#define RES_DIST 10

/*** NORM USED FOR STOPPING */
#define STOP_TWO_NORM FALSE /* if TRUE use two_norm else use maximum norm */

/*** Solution methods *****/

/*** without solution ***/
#define NO_SOLUTION                  0
#define GEN_MATRIX                   1

/*** conventional solutions with matrix generation ***/

/*** direct methods ***/
#define STD_LU                       2

/* just for compatinbility with sgspn2nsolve */
#define STD_UNI_NZ                   5
#define APNN                         8

/*** stationary iterative methods ***/
#define STD_POWER                   11
#define STD_JOR                     12
#define STD_SOR                     13

/*** projection methods ***/

/*** stationary iterative methods with a/d ***/
#define STD_POWER_AD                21  
#define STD_JOR_AD                  22
#define STD_SOR_AD                  23
#define STD_KMS                     24
#define STD_ML                      25

/*** projection methods ***/
#define STD_GMRES                   31
#define STD_BICGSTAB                32
#define STD_TFQMR                   33

/*** iteration methods with preconditioning ***/
#define STD_ILU0_POWER               61 
#define STD_ILUTH_POWER              62
#define STD_ILU0_GMRES               63
#define STD_ILUTH_GMRES              64
#define STD_ILU0_BICGSTAB            65
#define STD_ILUTH_BICGSTAB           66
#define STD_ILU0_TFQMR               67
#define STD_ILUTH_TFQMR              68

/*** Transient solvers ***/
#define STD_RANDOMIZATION            81
#define STD_RKF_45                   82
#define STD_RK_8PD                   83

/*** structured methods ***/

/*** stationary iterative methods ***/
#define STR_POWER                  111
#define STR_JOR                    112
#define STR_SOR                    113
#define STR_RSOR                   114

/*** stationary iterative methods with a/d ***/
#define STR_POWER_AD               121
#define STR_JOR_AD                 122
#define STR_SOR_AD                 123

/*** projection methods ***/
#define STR_GMRES                  131
#define STR_DQGMRES                132
#define STR_ARNOLDI                133
#define STR_CGS                    134
#define STR_BICGSTAB               135
#define STR_TFQMR                  136

/*** projection methods  with a/d ***/
#define STR_GMRES_AD               141
#define STR_DQGMRES_AD             142
#define STR_ARNOLDI_AD             143
#define STR_CGS_AD                 144
#define STR_BICGSTAB_AD            145
#define STR_TFQMR_AD               146 

/*** iteration methods with preconditioning ***/
#define PRE_POWER                  150
#define PRE_GMRES                  151
#define PRE_ARNOLDI                152
#define PRE_CGS                    154
#define PRE_BICGSTAB               155
#define PRE_TFQMR                  156 
/* ctd11.7 */
#define BSOR_BICGSTAB              157
/* ctd11.15 */
#define BSOR_GMRES                 158
#define BSOR_TFQMR                 159

/*** iteration methods with preconditioning and a/d ***/
#define PRE_POWER_AD               160
#define PRE_GMRES_AD               161
#define PRE_ARNOLDI_AD             162
#define PRE_CGS_AD                 164
#define PRE_BICGSTAB_AD            165
#define PRE_TFQMR_AD               166 

/*** block iteration techniques ***/
#define STB_JOR                    171
#define STB_SOR                    172
/* ctd9.18 */
#define STB_SOR_T                  173

/*** block iteration techniques with a/d ***/
#define STB_JOR_AD                 181
#define STB_SOR_AD                 182 

/*** Multi-level solution techniques ***/
#define ML_POWER                   211
#define ML_JOR                     212
#define ML_SOR                     214
/* ctd27.04.03 */
#define ML_POWER_W                 216
#define ML_JOR_W                   217
#define ML_SOR_W                   219
#define ML_POWER_F                 221
#define ML_JOR_F                   222
#define ML_SOR_F                   224

#define ML_GMRES                   231
#define ML_BICGSTAB                235
#define ML_TFQMR                   236

/* ctd25.06.04
   ML preconditioned projection methods */
#define ML_POWER_GMRES_V           241
#define ML_POWER_GMRES_W           242
#define ML_POWER_GMRES_F           243
#define ML_POWER_BICGSTAB_V        244
#define ML_POWER_BICGSTAB_W        245
#define ML_POWER_BICGSTAB_F        246
#define ML_POWER_TFQMR_V           247
#define ML_POWER_TFQMR_W           248
#define ML_POWER_TFQMR_F           249

#define ML_POWER_PRE_V             251
#define ML_POWER_PRE_W             252
#define ML_POWER_PRE_F             253

#define ML_JOR_GMRES_V             261
#define ML_JOR_GMRES_W             262
#define ML_JOR_GMRES_F             263
#define ML_JOR_BICGSTAB_V          264
#define ML_JOR_BICGSTAB_W          265
#define ML_JOR_BICGSTAB_F          266
#define ML_JOR_TFQMR_V             267
#define ML_JOR_TFQMR_W             268
#define ML_JOR_TFQMR_F             269

#define ML_JOR_PRE_V               271
#define ML_JOR_PRE_W               272
#define ML_JOR_PRE_F               273

#define ML_SOR_GMRES_V             281
#define ML_SOR_GMRES_W             282
#define ML_SOR_GMRES_F             283
#define ML_SOR_BICGSTAB_V          284
#define ML_SOR_BICGSTAB_W          285
#define ML_SOR_BICGSTAB_F          286
#define ML_SOR_TFQMR_V             287
#define ML_SOR_TFQMR_W             288
#define ML_SOR_TFQMR_F             289

#define ML_SOR_PRE_V               291
#define ML_SOR_PRE_W               292
#define ML_SOR_PRE_F               293

/*** Approximative Iteration ***/
#define APP_POWER                  311
#define APP_GS_POWER               312
#define APP_BLOCK_GS               313
#define APP_ML_POWER               314

/*** Approximative transient ***/
#define APP_RANDOMIZATION          411
#define APP_MULTI_RANDOMIZATION    412
#define APP_TRANS_BOUND            413
#define APP_MULT_TRANS_BOUND       414

/*** Transient Analysis ****/
#define STR_RANDOMIZATION          511
#define STR_RANDOMIZATION_PT       512
#define STR_RANDOM_REWARD          513
#define STR_RANDOM_REWARD_PT       514

/*** Transient Analysis for inhonogeneous systems ****/
#define INH_RANDOMIZATION          611
#define INH_RANDOM_APP1            612
#define INH_RANDOM_APP2            613
#define INH_RK_2                   614
#define INH_RK_4                   615
#define INH_RKF_45                 616
#define INH_RKCK                   617
#define INH_RK_8PD                 618
#define INH_RK_2IMP                619
#define INH_RK_4IMP                620
#define INH_GEAR_1                 621
#define INH_GEAR_2                 622

/**** Bounding Methods ****/
#define BO_COURTOIS                701 
#define BO_VALUE                   702
#define BO_POLICY                  703
#define BO_POWER                   704
#define BO_GS_POWER                705
#define BO_GS_ILU0                 706
#define BO_GS_ILUTH                707
#define BO_BICGSTAB                708
#define BO_BICGSTAB_ILU0           709
#define BO_BICGSTAB_ILUTH          710
#define BO_GMRES                   711
#define BO_GMRES_ILU0              712
#define BO_GMRES_ILUTH             713

/*** Solution methods for RPs */
#define RP_POWER                   801
#define RP_BSOR                    802
#define RP_BSOR_AD                 803
#define RP_BSOR_GMRES              804

/*** Product form solution ***/
#define APP_PRODFORM              1001
#define APP_SUCC_PRODFORM         1002
#define APP_NEG_PRODFORM          1003
#define APP_NEG_SUCC_PRODFORM     1004
#endif

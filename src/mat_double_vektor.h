/*********************
	Datei:	mat_double_vektor.h
	Datum: 	1.8.97
	Autor:	Peter Kemper
	Inhalt:	Funktionen zur Behandlung von Vektoren


	Datum 	Aenderung
----------------------------------------------------------------------
1.8.97 Datei angelegt, Funktionen entstammen matrix.c/matrix.h
7.2.03 Funktion mat_double_vektor_norm_diff erg�nzt
25.02.10 Funktion mat_double_vektor_tensor_product ergaenzt
25.03.10 Funktion mat_double_vektor_scalar_add ergaenzt

**********************/
#ifndef MAT_DOUBLE_VEKTOR_H
#define MAT_DOUBLE_VEKTOR_H

#include <stdio.h>
#include "basic_io.h" 

/******************* Defines	 **********************/
#ifndef BSP
#define BSP 0
#endif

#ifndef THRESHOLD
#define THRESHOLD 1.0e-50 
#endif

/******************* Data structures	 **********************/

/**************** Doublevektor **********************/
typedef struct
{
  unsigned no_of_entries ; /* gibt Laenge des Vektors an */
  double *vektor ;         /* Positionen 0,1, ... no_of_entries -1 */
} Double_vektor ;


/******************* Functions	 **********************/
/**********************
mat_double_vektor_new ##e Konstruktor
Datum:  14.2.95
Autor:  Peter Kemper
Input:  unsigned anzahl, falls 0 < anzahl Laenge des Vektors
Output: neues Element
Side-Effects:
Description:
Es wird ein neues Element vom Typ Double_vektor auf eigenem
Speicherplatz erzeugt, 
falls eine Vektorlaenge angegeben ist (anzahl > 0) wird 
zusaetzlich ein Vektor entsprechender Laenge fuer double Eintraege
allokiert
anderenfalls liegt kein Vektor vor.
**********************/
extern Double_vektor *mat_double_vektor_new(unsigned anzahl);

/**********************
mat_double_vektor_free ##e Destruktor
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
Der Speicherplatz fuer v wird freigegeben, dies gilt ebenso fuer 
die Komponente vektor.
**********************/
extern int mat_double_vektor_free(Double_vektor *v);

/**********************
mat_double_vektor_print_file ##e Ausgabefunktion (mit Kommentar, je 10 Eintraege 1 Zeile)
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege des Vektors wird in das angegebene File ausgegeben.
Annahme: fp zeigt auf eine Datei, die bereits zum Schreiben geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
Ausgabeformat: Kommentar zur Anzahl Eintraege
dann jeweils 10 Eintraege pro Zeile
**********************/
extern int mat_double_vektor_print_file(FILE *fp, Double_vektor *v) ;

/**********************
mat_double_vektor_output ##e Ausgabefunktion (ohne Kommentar, je Eintrag 1 Zeile)
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege des Vektors wird in das angegebene File ausgegeben.
Annahme: fp zeigt auf eine Datei, die bereits zum Schreiben geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
Ausgabeformat: Anzahl Eintraege \n Wert1 \n Wert2 ....
**********************/
extern int mat_double_vektor_output(FILE *fp, Double_vektor *v);

/**********************
mat_double_vektor_input ##e Einlesefunktion
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege eines Vektors werden aus dem angegebenen File 
eingelesen und in v gespeichert.
Es werden genau so viele Eintraege eingelesen, wie in v->no_of_entries
angegeben ist. 
Annahme: fp zeigt auf eine Datei, die bereits zum Lesen geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
**********************/
extern int mat_double_vektor_input(FILE *fp, Double_vektor *v);


/**********************
mat_double_vektor_sum ##e summiert Eintraege auf
Datum:  13.6.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
extern double mat_double_vektor_sum (Double_vektor *v) ;

/**********************
mat_double_vektor_scalar_mult
Datum:  03.03.05
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Multipliziert einen Vektor mit einem Skalar.
**********************/
extern int mat_double_vektor_scalar_mult(Double_vektor *v, double s) ;

/**********************
mat_double_vektor_scalar_add
Datum:  03.03.05
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Addiert einen Skalar zu einem Vektor.
**********************/
extern int mat_double_vektor_scalar_add(Double_vektor *v, double s) ;

/**********************
mat_double_vektor_norm_diff 
Datum:  07.02.03
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Berechnung der Summen- und Maximumsnorm der Differenz zweier Vektoren.
**********************/
#ifndef CC_COMP
extern int mat_double_vektor_norm_diff(Double_vektor *v1, Double_vektor *v2, double *n_max, double *n_sum);
#else
extern int mat_double_vektor_norm_diff() ;
#endif

/**********************
mat_compute_norms ##e Berechnung der Maximum- und Summennorm
Datum:  24.3.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
extern int mat_compute_norms(Double_vektor *v, double *n_max, double *n_sum);

/**********************
mat_compute_norms ##e Berechnung der Maximum-, Summen-
                     und Euklisdischen-Norm
Datum:  24.3.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
extern int mat_compute_all_norms(Double_vektor *v, double *n_max, double *n_sum, double *n_two) ;

/**********************
mat_double_vektor_tnorm ##e Berechnung der 2 Norm fuer Vektoren
Datum:  24.3.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
extern double mat_double_vektor_tnorm(Double_vektor *v) ;

/**********************
mat_double_vektor_inner_product ##e Berechnung des Produkts zweier Vektoren
Datum:  31.08.98
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
**********************/
extern double mat_double_vektor_inner_product(Double_vektor *v1, Double_vektor *v2) ;

/**********************
mat_add_vektor_skalar 
Datum:  24.3.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Adds v2 to v1 and sets v2 to 0
**********************/
extern int mat_add_vektor_skalar(Double_vektor *v1, Double_vektor *v2, double alpha);

/**********************
mat_double_vektor_normalise ##e Normalisiert einen Vektor auf 1.0
Datum:  24.3.95
Autor:  Peter Buchholz
Input:  v Double Vektor 
Output: v Vektor mit auf 1.0 normalisierten Elementen
Side-Effects:
Description:
Die Funktion normiert die Summe der Vektorelemente eines double Vektors auf
1.0. Dazu werden alle Elemente < 1.0e-100 auf 0 gesetzt. Falls die Summe der
vektorelemente nach dieser Operation 0 ist, so wird der Vektor mit einer
Gleichverteilung belegt.
**********************/
extern int mat_double_vektor_normalise(Double_vektor *v) ;

/**********************
mat_double_vektor_reset ##e Belegt einen double_vektor mit 0.0
Datum:  14.06.98
Autor:  Peter Buchholz
Input:  v Double Vektor 
Output: v Vektor mit auf 0.0 belegt
Side-Effects:
Description:
Die Funktion belegt die Elemente eines Vektors mit 0.0.
**********************/
extern int mat_double_vektor_reset(Double_vektor *v) ; 

/**********************
mat_double_vektor_copy 
Datum:  22.01.99
Autor:  Peter Buchholz
Input:  v Double Vektor
        w Double Vektor 
Output: w Vektor mit dem Inhalt von v
Side-Effects:
Description:
Die Funktion kopiert die Elemente von v nach w, wobei v und w
gleiche Laenge haben muessen.
**********************/
extern int mat_double_vektor_copy(Double_vektor *v, Double_vektor *w) ;

/**********************
mat_double_vektor_add_vektor 
Datum:  22.01.99
Autor:  Peter Buchholz
Input:  v Double Vektor
        w Double Vektor 
        a Skalar
Output: v = v + a * w
Side-Effects:
Description:
Die Funktion berechnet v = v + a*w. Im Gegensatz zur Funktion mat_add_vektor_skalar,
wird Vektor w nicht veraendert. Vektoren muessen gleiche Laenge haben.
**********************/
extern int mat_double_vektor_add_vektor(Double_vektor *v, Double_vektor *w, double a) ;

/**********************
mat_double_vektor_eliminate_small_values
Datum:  09.02.03
Autor:  Peter Buchholz
Input:  v Double Vektor
        sval kleiner Wert, ab dem Elemente gel�scht werden sollen
Output: v, wobei Elemente < sval gel�scht wurden
Side-Effects:
Description:
Die Funktion setzt alle Werte aus V, die kleiner als sval sind auf 0.
**********************/
#ifndef CC_COMP
extern int mat_double_vektor_eliminate_small_values(Double_vektor *v, double sval) ;
#else
extern int mat_double_vektor_eliminate_small_values() ;
#endif

/**********************
mat_double_vektor_tensor_product
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  p1, p2 Vektoren 
Output: Funktionswert
Side-Effects:
Description:
Die Funktion berechnet das Kronecker Produkt zweier Vektoren.
**********************/
extern Double_vektor *mat_double_vektor_tensor_product(Double_vektor *p1, Double_vektor *p2) ;

#endif

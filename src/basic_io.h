/*********************
        Datei:  basic_io.h
        Datum:  05.11.97
        Autor:  Peter Buchholz
        Inhalt: 

Side-effects: 

********************************************************/

#ifndef BASICIO_H
#define BASICIO_H

#include <stdio.h>
#include <values.h>

#define INT_STOP     MININT
#define DOUBLE_STOP  MINDOUBLE

/***** exportierte Funktionen *****/
/**********************
bg_copy_file
Datum:  29.11.02
Autor:  Peter Buchholz
Input:  fp_in Filedeskriptor zum Lesen ge�ffnet
        fp_out Filedeskriptor zum Schreiben ge�ffnet
Output: Kopie der Datei in fp_out
Side-Effects:
Description:
Die Funktion kopiert die Datei fp_in zeilenweise nach fp_out
***********************************************************************/
#ifndef CC_COMP
extern int bg_copy_file(FILE *fp_in, FILE *fp_out) ;
#else
extern int bg_copy_file() ;
#endif


/**********************
input_error
Datum:  9.6.95
Autor:  Peter Buchholz
Input:  
Output: F
Side-Effects:
Description:
Die Funktion druckt eine Fehlermeldung und bricht das Programmab.. 
***********************************************************************/
extern void input_error () ;

/********************
read_comment	##s Einlesen von Kommentaren (werden uebersprungen)
Change:	17.3.98
Input:	
Output:	TRUE	ok
	FALSE	Fehler
Side:	
Description: Verwendung: fp = read_comment(fp) 
             fp muss beim Aufruf geoeffnet sein.
********************/
#ifndef CC_COMP
extern FILE *read_comment(FILE *fp) ;
#else
extern FILE *read_comment() ;
#endif

/**********************
readc_double
Datum:  16.01.06
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen double Wert.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein double in der Zeile
gefunden wird, so wird der Wert DOUBLE_STOP zur�ckgegeben. 
Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
extern double readc_double (FILE *fp) ; 
#else
extern double readc_double () ;
#endif

/**********************
read_integer
Datum:  16.01.06
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen integer Wert.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein integer in der Zeile
gefunden wird, so wird der Wert INT_STOP zur�ckgegeben. 
Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
extern int readc_integer (FILE *fp) ;
#else
extern int readc_integer () ;
#endif

/**********************
read_double
Datum:  9.6.95
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen double Wert.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein double in der Zeile
gefunden wird, so wird das Programm nach Ausgabe einer Fehlermeldung
abgebrochen. Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
extern double read_double (FILE *fp) ;
#else
extern double read_double () ;
#endif


/**********************
read_integer
Datum:  9.6.95
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen integer Wert.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein integer in der Zeile
gefunden wird, so wird das Programm nach Ausgabe einer Fehlermeldung
abgebrochen. Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
extern int read_integer (FILE *fp) ;
#else
extern int read_integer () ;
#endif

/**********************
read_string
Datum:  19.05.11
Autor:  Peter Buchholz
Input:  fp Datei
Output: Funktionswert gelesener double Wert
Side-Effects:
Description:
Die Prozedur liest aus einer zum lesen geoeffneten Datei einen String.
Vorangestellte Kommentare, die mit # beginnen werden dabei ueberlesen.
Falls kein String in der Zeile
gefunden wird, so wird das Programm abgebrochen.   
Die Datei wird in der Funktion nicht geschlossen.
***********************************************************************/
#ifndef CC_COMP
extern char *read_string (FILE *fp) ;
#else
extern char *read_string () ;
#endif

#endif

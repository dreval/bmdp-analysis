/*********************
        Datei:  bmdp_func.h
        Datum:  13.3.15
        Autor:  Peter Buchholz
        Inhalt: Externe Deklarationen fuer bmdp_func.c
        Aenderungen:

**********************/
#ifndef BMDP_PARETO_FUNC_H
#define BMDP_PARETO_FUNC_H

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "global.h"
#include "str_global.h"
#include "bmdp_const.h"
#include "ememory.h"
#include "list.h" 
#include "mat_time.h"
#include "mat_int_vektor.h"
#include "mat_row.h"
#include "mat_sparse_matrix.h"
#include "solution.h"
#include "bmdp_func.h"

/* declaration of data types */
typedef struct 
{
    short   seen1    ;
    short   seen2    ;
    size_t  ord      ;
    double  stat_min ;
    double  stat_avg ;
    double  stat_max ;
    size_t *policy   ;
    double *gain_min ;
    double *gain_avg ;
    double *gain_max ;
    double weight_min;
    double weight_max;
    double weight_avg;
    double fitness;
    int strength;
} Pareto_vector_element ;

typedef struct
{
  int    no_entries ;
  List **list  ;
} Pareto_vector_set ;

typedef struct tree_t {
    size_t *elem;
    size_t ord;
    int balance; /* for later fun with balanced trees */
    struct tree_t *left_child;
    struct tree_t *right_child;
} tree;

/* Functions */

/*
 * Returns 0 if element already there, 1 if new
 */
extern short tree_insert(tree *tr, size_t *elem, size_t ord);

/**********************
bmpar_pure_opt
Datum:  14.03.15
Autor:  Peter Buchholz
Input: (mdp_.., rew_..) BMDP
       gamma discount factor
       comp_max maximum or minimum 
Output: return value pointer to teh policies plus corresponding vectors
        no_policies number of Pareto optimla polcies that have been computed 
Side-Effects:
Description:
The function computes the Pareto optimal policies for a BMDP.
**********************/
extern Pareto_vector_element **bmdp_pure_opt(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
					     Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
					     double gamma, short comp_max, size_t *no_policies);

/* A different implementation */
extern Pareto_vector_element **bmdp_pure_opt_bfs(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                                 Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                                 double gamma, short comp_max, size_t *no_policies);

/* An IPT heuristic */
extern Pareto_vector_element **bmdp_pure_opt_ipt_heuristic(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                                           Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                                           double gamma, short comp_max, size_t *no_policies);


/**********************
bmpar_print_pareto_results
Datum:  14.03.15
Autor:  Peter Buchholz
Input: base_name praefix of teh file names
       my_results result vectors of Pareto optimal policies
       no_policies number ofPareto optimal policies
     prt_ind 0 nothing, 1 only stationay, 2 stationary and policies, 3 everything
Output: 
Side-Effects:
Description:
The function writes the policy vectors and the gain vectors of a Pareto optimization to a file.
**********************/
extern void bmpar_print_pareto_results (char *base_name, Pareto_vector_element **my_results, size_t no_policies, short prt_id) ;
extern void bmpar_print_nonst_pareto_results(char *base_name, Pareto_vector_element *my_results, size_t no_policies);

extern void bmpar_print_pareto_values(char *base_name, Pareto_vector_element **results, size_t no_policies);

/**********************
bmpar_stationary_analysis
Datum:  16.03.15
Autor:  Peter Buchholz
Input: (mdp_..) BMDP matrices
       gamma discount factor
       my_results Pareto optimal policies
       no_policies number ofPareto optimal policies
Output: 
Side-Effects:
Description:
The function writes the policy vectors and the gain vectors of a Pareto optimization to a file.
**********************/
extern void bmpar_stationary_analysis(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
				      double gamma, Pareto_vector_element **my_results, size_t no_policies) ;


typedef struct tri_objective_t {
    double min;
    double avg;
    double max;
} tri_objective;

enum domination_t { GE, LE, EQ, IC };

short dominates(tri_objective x, tri_objective y, short comp_max);

/*
 * bmdp_pareto_opt
 * Datum: 12.06.2015
 * Autor: Dimitri Scheftelowitsch
 * Input: mdp_* BMDP matrices
 *        gamma discount factor
 * Output: no_policies number of Pareto optimal non-stationary policies
 * Side-Effects:
 * Description:
 * This function computes all (possibly non-stationary) Pareto optimal policies
 */
extern Pareto_vector_element * bmdp_pareto_opt(Mdp_matrix *mdp_min, Mdp_matrix *mdp_avg, Mdp_matrix *mdp_max,
                                               Double_vektor **rew_min, Double_vektor **rew_avg, Double_vektor **rew_max,
                                               double gamma, double maxerr, short comp_max, size_t *no_policies);

extern Pareto_vector_element *bmpar_pareto_vector_element_new(size_t ord);
extern int bmpar_pareto_vector_element_free(Pareto_vector_element *pareto);

extern int bmpar_vector_compare(Pareto_vector_element *elem1, Pareto_vector_element *elem2);

extern Pareto_vector_set *bmpar_pareto_vector_set_new();

extern int bmpar_pareto_vector_set_free(Pareto_vector_set *my_set);

extern Pareto_vector_element *bmpar_get_first(Pareto_vector_set *my_set);

extern short bmpar_in_list(Pareto_vector_set *my_set, Pareto_vector_element *my_elem);

extern int bmpar_add_remove_element(Pareto_vector_set *my_set, Pareto_vector_element *my_elem);
#endif /* BMDP_PARETO_FUNC_H */

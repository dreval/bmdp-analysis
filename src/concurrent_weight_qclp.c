#include "bmdp_const.h"
#include "concurrent_weight_func.h"
#include "bmdp_func.h"
#include <nlopt.h>

typedef struct qclp_constraint_params_t {
    concurrent_mdp mdps;
    weight_tuple weights;
    size_t k;
    size_t s;
    double gamma;
} qclp_constraint_params;

typedef struct qclp_f_params_t {
    concurrent_mdp mdps;
    weight_tuple weights;
} qclp_f_params;

double qclp_objective_function(unsigned n, const double* x, double* grad, void* f_data)
{
    qclp_f_params *fparams = (qclp_f_params *) f_data;
    concurrent_mdp *mdps = &(fparams->mdps);
    const size_t ord = mdps->matrices[0]->ord;
    const size_t actions = mdps->matrices[0]->rows[0]->pord;
    const size_t scenarios = mdps->scenarios;
    
    double ret = 0.0;
    for (size_t k = 0; k < scenarios; ++k) {
        for (size_t s = 0; s < ord; ++s) {
            const size_t x_k_s_index = ord * actions + ord * k + s;
            ret += x[x_k_s_index] * mdps->rewards[k][0]->vektor[s];
        }
    }
    
    assert(!isnan(ret));
    return ret;
}

double qclp_quadratic_constraint(unsigned n, const double *x, double *grad, void *constraint_data)
{
    qclp_constraint_params *params = (qclp_constraint_params *) constraint_data;
    const size_t ord = params->mdps.matrices[0]->ord;
    const size_t actions = params->mdps.matrices[0]->rows[0]->pord;

    double ret = - params->weights.weights[params->k] * params->mdps.iniv[params->s];
    
    for (size_t sprime = 0; sprime < ord; ++sprime) {
        double sum = 0.0;
        const double x_k_sprime = x[ord * actions + ord * params->k + sprime];
        for (size_t a = 0; a < actions; ++a) {
            const size_t d_sprime_a_index = sprime * actions + a;
            const double d_sprime_a = x[d_sprime_a_index];
            double a_value = 0.0;
            /* compute A_{s', k}(a, s) */
            /* we know that A_{s', k}(a \bullet) = e_{s'} - \gamma P^a_k(s' \bullet) */
            Mdp_row *row = params->mdps.matrices[params->k]->rows[sprime];
            /* find the s-th element */
            if (params->s == sprime) {
                a_value = 1.0 - params->gamma * row->diagonals[a];
            } else {
                for (size_t i = 0; i < row->no_of_entries[a]; ++i) {
                    if (params->s == row->colind[a][i]) {
                        a_value = -params->gamma * row->values[a][i];
                    }
                }
            }
            sum += d_sprime_a * a_value;
        }
        ret += x_k_sprime * sum;
    }
    
    assert(!isnan(ret));
    return ret;
}

double qclp_linear_constraint(unsigned n, const double *x, double *grad, void *constraint_data)
{
    qclp_constraint_params *params = (qclp_constraint_params *) constraint_data;
    const size_t actions = params->mdps.matrices[0]->rows[0]->pord;

    double ret = -1.0;
    
    for (size_t a = 0; a < actions; ++a) {
        const size_t d_s_a_index = params->s * actions + a;
        ret += x[d_s_a_index];
    }
    assert(!isnan(ret));
    return ret;
}

value_vectors concurrent_qclp(concurrent_mdp mdps, weight_tuple weights, double **policy, double gamma, short comp_max, double *value, __attribute__((unused)) short presolved)
{
    /* A QCLP formulation. Solved with NLOpt/COBYLA. */
    const size_t ord = mdps.matrices[0]->ord;
    const size_t actions = mdps.matrices[0]->rows[0]->pord;
    const size_t scenarios = mdps.scenarios;

    /* initialize return values */
    value_vectors gain;
    gain.scenarios = weights.scenarios;
    gain.ord = ord;
    gain.gains = calloc(weights.scenarios, sizeof(double *));
    
    const size_t num_variables = ord * actions + scenarios * ord;
    
    /* initialize COBYLA */
    nlopt_opt optimizer = nlopt_create(NLOPT_LN_COBYLA, num_variables);
    nlopt_opt local_optimizer = nlopt_create(NLOPT_LD_SLSQP, num_variables);
    // nlopt_set_local_optimizer(optimizer, local_optimizer);
    
    /* set up lower bounds. They are always zero, this is really simple. */
    nlopt_set_lower_bounds1(optimizer, 0.0);
    
    /* upper bounds are not necessary */
    /* define objective function */
    qclp_f_params fparams;
    fparams.mdps = mdps;
    fparams.weights = weights;
    
    if (comp_max) {
        nlopt_set_max_objective(optimizer, qclp_objective_function, (void *) &fparams);
    } else {
        nlopt_set_min_objective(optimizer, qclp_objective_function, (void *) &fparams);
    }
    
    /* add linear equality constraints */
    qclp_constraint_params *lineq_params = calloc(ord, sizeof(qclp_constraint_params));
    for (size_t s = 0; s < ord; ++s) {
        lineq_params[s].k = 0;
        lineq_params[s].s = s;
        lineq_params[s].weights = weights;
        lineq_params[s].mdps = mdps;
        lineq_params[s].gamma = gamma;
        nlopt_add_equality_constraint(optimizer, qclp_linear_constraint, (void *) &(lineq_params[s]), 1e-8);
    }
    
    /* add quadratic constraints */
    qclp_constraint_params *qneq_params = calloc(ord * scenarios, sizeof(qclp_constraint_params));
    for (size_t s = 0; s < ord; ++s) {
        for (size_t k = 0; k < scenarios; ++k) {
            const size_t index = k * ord + s;
            qneq_params[index].k = k;
            qneq_params[index].s = s;
            qneq_params[index].weights = weights;
            qneq_params[index].mdps = mdps;
            qneq_params[index].gamma = gamma;
            nlopt_add_equality_constraint(optimizer, qclp_quadratic_constraint, (void *) &(qneq_params[index]), 1e-8);
        }
    }
    
    /* set convergence criteria */
    nlopt_set_xtol_rel(optimizer, 1e-4);
    nlopt_set_maxtime(optimizer, CMDP_MAX_TIME);
    nlopt_set_maxeval(optimizer, 10000);
    nlopt_set_ftol_rel(optimizer, 1e-6);
    nlopt_set_xtol_rel(local_optimizer, 1e-6);
    nlopt_set_maxtime(local_optimizer, CMDP_MAX_TIME);
    nlopt_set_maxeval(local_optimizer, 10000);
    nlopt_set_ftol_rel(local_optimizer, 1e-6);

    /* optimize */
    /* create an initial point */
    double *x = calloc(num_variables, sizeof(double));
    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            x[s * actions + a] = 1.0 / actions;
        }
    }
    
    /* call to the optimizer */
    nlopt_result result = nlopt_optimize(optimizer, x, value);
    if (result < 0) {
        printf("nlopt failed, error %d\n", result);
    } else {
        printf("found optimum: %0.10g, nlopt returned %d\n", *value, result);
    }
    
    /* read values */
    for (size_t s = 0; s < ord; ++s) {
        for (size_t a = 0; a < actions; ++a) {
            policy[s][a] = x[s * actions + a];
        }
    }
    
    /* cleanup */
    free(x);
    free(lineq_params);
    free(qneq_params);
    nlopt_destroy(optimizer);
    
    /* evaluate policy */
    double v = 0.0;
    for (size_t k =0; k < mdps.scenarios; ++k) {
        gain.gains[k] = bmdp_evaluate_avg_stationary_policy(mdps.matrices[k], policy, mdps.rewards[k], gamma, FALSE);
        for (size_t s = 0; s < ord; ++s) {
            v += weights.weights[k] * mdps.iniv[s] * gain.gains[k][s];
        }
    }
    /* printf("|v - v'| = %f, v'/v = %f, v' = %f\n", fabs(v - *value), (*value)/v, v); */
    

    return gain;
}

#ifndef INH_FUNC_H

#define INH_FUNC_H

/**********************************************************
 * inh_func.h
 * Created: 24.11.2008
 * Modified: 24.11.2008
 *
 ***********************************************************/

#ifndef TRUE
#define TRUE  1
#define FALSE 0
#endif

#include "ememory.h"
#include "mat_int_vektor.h"
#include "mat_double_vektor.h"
#include "mat_row.h"
#include "mat_sparse_matrix.h"
#include "transient.h"


/************** Some basic constants *******************/
#define CMIN FALSE /* if TRUE minimum else maximum */
#define K_MAX 10
#define K_MIN  1
#define OMEGA  0.1
#define DELTA_MIN 1.0e-4

#define MIN_VAL 1.0e-6

#define TRC_STRATEGY  FALSE 
#define TRC_VECTOR    FALSE
#define TRC_ITERATION FALSE 
#define TRC_DETAIL    FALSE

#define IMPROVE_UPPER_BOUND TRUE 

#define DYNAMIC_REW FALSE /* if TRUE the last value in the upper reward vector will 
                             be computed according to the additive or multiplicative dynamics */
#define DYNAMIC_ADD TRUE  /* Only relevant if DYNAMIC_REW TRUE */


/************** Data Strutcures ************************/

typedef struct {
    size_t pord ; /* Number of alternative policies */
    double *diagonals ; /* Diagonal elements */
    size_t *no_of_entries ; /* Length of the alternative rows */
    size_t **colind ; /* Column indices */
    double **values ; /* Entries in the row */

} Mdp_row ;

typedef struct {
    size_t  ord  ; /* dimension of the matrix */
    Mdp_row **rows ; /* Rows of the MDP matrix */
} Mdp_matrix ;


/****************** External Function Declarations **************/
Mdp_row *mdp_row_new (size_t ord);
/**********************
mdp_matrix_new ##e
Datum:  28.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function allocates teh basic data structure for a
mdp_matrix
**********************/
extern Mdp_matrix *mdp_matrix_new (size_t ord) ;

/**********************
mdp_matrix_free ##e
Datum:  28.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function frees the basic data structure for a
mdp_matrix. 
**********************/
extern void mdp_matrix_free (Mdp_matrix *v) ;

/**********************
mdp_matrix_generate ##e
Datum:  28.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a new mdp_matrix from a set of matrices.
**********************/
extern Mdp_matrix *mdp_matrix_generate (int nf, int ord, double *alpha, Sparse_matrix **matrices, short transform) ;

/**********************
mdp_matrix_generate_discrete ##e
Datum:  24.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a new mdp_matrix from a set of matrices.
In contrast to mdp_matrix_generate it uses discrete time MDPS and does not 
interpret rows with self loops.
**********************/
extern Mdp_matrix *mdp_matrix_generate_discrete (int nf, int ord, Sparse_matrix **matrices) ;

/**********************
mdp_gen_sparse_matrix_structure
Datum:  17.06.2011
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a Sparse_matrix data structure that can be used to include
a transition matrix resulting from map_matrix mdp and an arbitrary decision vector. 
**********************/
extern Sparse_matrix *mdp_gen_sparse_matrix_structure (const Mdp_matrix *mdp) ;

/**********************
mdp_gen_precond_structure
Datum:  21.06.2011
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a Sparse_matrix data structure that can be used to include
a transition matrix resulting from map_matrix mdp and an arbitrary decision vector
for preconditioned iteration techniques that contains an additional state for the 
normalization condition. 
**********************/
extern Sparse_matrix *mdp_gen_precond_structure (Mdp_matrix *mdp) ;

/**********************
mdp_compute_unif_result ##e
Datum:  25.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the result vector.
**********************/
extern int mdp_compute_unif_result (double *gl, double *gu, double ti, double alpha, double *epsilon,
			       Double_vektor *rvl, Double_vektor *rvu, Double_vektor *pv, Double_vektor *gtl, 
			       Double_vektor *gtu, Mdp_matrix *mdpm, int *global_iter, int kmin, 
			       int kmax, double omega, double eps_base) ;

/**********************
mdp_compute_disc_result ##e
Datum:  27.06.2014
Autor:  Peter Buchholz
Co Autor: Ingo Schulz
Input:
Output:
Side-Effects:
Description:
The function computes the result vector with discretization.
**********************/
extern int mdp_compute_disc_result (double *gl, double ti, double delta, Double_vektor *rv, 
				    Double_vektor *pv, Double_vektor *gv, Mdp_matrix *mdpm) ;

/**********************
mdp_set_strategy
Datum:  17.06.2011
Autor:  Peter Buchholz
Input:  mdp MDP matrix
        ind decision vector
Output: pp Matrix resulting from decision vector
Side-Effects:
Description:
The function generates matrix for a given decision vector. Matrix pp has to 
adequately allocated e.g. by the function mdp_gen_sparse_matrix_structure. 
**********************/
extern void mdp_set_strategy(Mdp_matrix *mdp, Sparse_matrix *pp, int *ind) ;

/**********************
mdp_set_precond_strategy
Datum:  21.06.2011
Autor:  Peter Buchholz
Input:  mdp MDP matrix
        ind decision vector
Output: pp Matrix resulting from decision vector
Side-Effects:
Description:
The function generates the matrix for a given decision vector for preconditioned
iteration methods. Matrix pp has to adequately allocated e.g. by the function 
mdp_gen_precond_structure. 
**********************/
extern void mdp_set_precond_strategy(Mdp_matrix *mdp, Sparse_matrix *pp, int *ind) ;

/**********************
mdp_compute_max_reach ##e
Datum:  19.08.2009
Autor:  Peter Buchholz
Co Autor: Ingo Schulz
Input:
Output:
Side-Effects:
Description:
The function computes the time bounded maximum reachability.
**********************/

extern int mdp_compute_max_reach (double *gl, double *gu, double ti, double alpha, double *epsilon,  
				  Double_vektor *pv, Double_vektor *gtl, Double_vektor *gtu, Mdp_matrix *mdpm, 
                                  int *global_iter, int kmin, int kmax, double omega, double eps_base) ;

/**********************
mdp_matrix_prt ##e
Datum:  27.06.2014
Autor:  Peter Buchholz

Input:
Output:
Side-Effects:
Description:
The function prints a MDP matrix
**********************/
extern void mdp_matrix_prt (Mdp_matrix *mdpm) ;
#endif

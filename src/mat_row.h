/*********************
	Datei:	mat_row.h
	Datum: 	1.8.97
	Autor:	Peter Kemper
	Inhalt:	Funktionen zur Behandlung von spaerlich besetzten
	Matrixzeilen (= Vektoren)


	Datum 	Aenderung
----------------------------------------------------------------------
1.8.97 Datei angelegt, Funktionen entstammen matrix.c/matrix.h

**********************/
#ifndef MAT_ROW_H
#define MAT_ROW_H

#include <stddef.h>
#include <stdio.h>
#include "basic_io.h"

/******************* Defines	 **********************/


/******************* Data structures	 **********************/
/**************** Matrixzeile zur Unterscheidung von Zeile als Row **/
typedef struct 
{
  size_t no_of_entries ; /* gibt Laenge des Vektors an   */
  size_t *colind ;            /* Identifier, Indizes der Spalten
			      Positionen 0,1, ... no_of_entries -1 */ 
  double *val ;            /* Werteintraege
			      Positionen 0,1, ... no_of_entries -1 */ 
} Row ;


/******************* Functions	 **********************/
/**********************
mat_row_new ##e Konstruktor
Datum:  14.2.95
Autor:  Peter Kemper
Input:  unsigned anzahl, falls 0 < anzahl Laenge des Vektors
Output: neues Element
Side-Effects:
Description:
Es wird ein neues Element vom Typ Row auf eigenem
Speicherplatz erzeugt, 
falls eine Vektorlaenge angegeben ist (anzahl > 0) wird 
zusaetzlich ein Vektor entsprechender Laenge fuer int Eintraege
als Spaltenindizes und double Eintraege fuer Matrixeintraege
allokiert
anderenfalls liegen keine Vektoren vor.
**********************/
extern Row *mat_row_new(unsigned anzahl);

/**********************
mat_row_free ##e Destruktor
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
Der Speicherplatz fuer v wird freigegeben, dies gilt ebenso fuer 
die Komponente vektor.
**********************/
extern int mat_row_free(Row *v);

/**********************
mat_row_print_file ##e Ausgabefunktion (mit Kommentar, je 10 Eintraege 1 Zeile)
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege des Vektors wird in das angegebene File ausgegeben.
Annahme: fp zeigt auf eine Datei, die bereits zum Schreiben geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
Ausgabeformat: Kommentar zur Anzahl Eintraege
dann jeweils 10 Eintraege pro Zeile
**********************/
extern int mat_row_print_file(FILE *fp, Row *v) ;

/**********************
mat_row_output ##e Ausgabefunktion (ohne Kommentar, je Eintrag 1 Zeile)
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege des Vektors wird in das angegebene File ausgegeben.
Annahme: fp zeigt auf eine Datei, die bereits zum Schreiben geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
Ausgabeformat: Anzahl Eintraege \n Wert1 \n Wert2 ....
**********************/
extern int mat_row_output(FILE *fp, Row *v);

/**********************
mat_row_input ##e Einlesefunktion
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die numerischen Eintraege eines Vektors werden aus dem angegebenen File 
eingelesen und in v gespeichert.
Es werden genau so viele Eintraege eingelesen, wie in v->no_of_entries
angegeben ist. 
Annahme: fp zeigt auf eine Datei, die bereits zum Lesen geoeffnet wurde.
fp wird innerhalb der Funktion NICHT geschlossen.
**********************/
extern int mat_row_input(FILE *fp, Row *v);

/**
 * Aktualisieren und ggfs verkuerzen einer Matrixzeile gemaess Relation.
 * \date 7.3.00
 * \author Peter Kemper
 * \param r Matrixzeile
 * \param dim ???
 * \param relation Realtion
 * \param new_dim ???
 */
extern int mat_row_update(Row *r, int dim, int *relation, int new_dim) ;

#endif

#include "polynomial.h"
#include <stdlib.h>
#include <math.h>

void polynomial_add(polynomial *a, polynomial *b)
{
    /* handle size issues */
    if (a->ord < b->ord) {
        double *vals = (double *) calloc(b->ord + 1, sizeof(double));
        for (size_t i = 0; i <= a->ord; ++i) {
            vals[i] = a->coeffs[i];
        }
        free(a->coeffs);
        a->coeffs = vals;
        a->ord = b->ord;
    }
    for (size_t i = 0; i <= b->ord; ++i) {
        a->coeffs[i] += b->coeffs[i];
    }
}

void polynomial_multiply(polynomial *a, polynomial *b)
{
    /* allocate enough memory */
    double *coeffs = (double *) calloc(a->ord + b->ord + 1, sizeof(double));
    /* compute the product */
    for (size_t i = 0; i <= a->ord; ++i) {
        for (size_t j = 0; j <= b->ord; ++j) {
            coeffs[i + j] += a->coeffs[i] * b->coeffs[j];
        }
    }
    /* replace values */
    free(a->coeffs);
    a->coeffs = coeffs;
    a->ord += b->ord;
}

void polynomial_multiply_scalar(polynomial *a, double b)
{
    for (size_t i = 0; i <= a->ord; ++i) {
        a->coeffs[i] *= b;
    }
}

void polynomial_compress(polynomial *p)
{
    size_t size = 0;
    for (size_t i = 0; i <= p->ord; ++i) {
        if (fabs(p->coeffs[i]) > 1.0e-10) {
            size = i;
        } else {
            p->coeffs[i] = 0.0;
        }
    }
    if (size < p->ord) {
        double *new_coeffs = (double *) calloc(size + 1, sizeof(double));
        for (size_t i = 0; i <= size; ++i) {
            new_coeffs[i] = p->coeffs[i];
        }
        free(p->coeffs);
        p->coeffs = new_coeffs;
        p->ord = size;
    }
}

polynomial make_polynomial()
{
    polynomial p;
    p.ord = 0;
    p.coeffs = (double *) calloc(1, sizeof(double));
    p.coeffs[0] = 1.0;
    return p;
}

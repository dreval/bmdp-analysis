/* 
 * $Log: defines.h,v $
 * Revision 1.32  2015/03/11 22:04:33  buchholz
 * *** empty log message ***
 *
 * Revision 1.35  2015/03/07 21:55:05  buchholz
 * *** empty log message ***
 *
 * Revision 1.31  2015/03/01 19:33:20  buchholz
 * *** empty log message ***
 *
 * Revision 1.24  2014/02/28 14:26:06  peter
 * *** empty log message ***
 *
 * Revision 1.29  2013/12/23 14:42:29  buchholz
 * *** empty log message ***
 *
 * Revision 1.20  2013/09/20 06:25:06  peter
 * *** empty log message ***
 *
 * Revision 1.27  2013/09/01 21:49:14  buchholz
 * *** empty log message ***
 *
 * Revision 1.14  2013/08/30 19:56:32  peter
 * *** empty log message ***
 *
 * Revision 1.26  2012/05/31 19:29:33  buchholz
 * *** empty log message ***
 *
 * Revision 1.11  2011/11/10 15:50:17  peter
 * *** empty log message ***
 *
 * Revision 1.25  2011/10/30 19:37:11  buchholz
 * *** empty log message ***
 *
 * Revision 1.10  2011/08/02 19:35:56  peter
 * *** empty log message ***
 *
 * Revision 1.21  2011/05/30 07:10:27  buchholz
 * *** empty log message ***
 *
 * Revision 1.8  2011/05/04 17:36:25  peter
 * *** empty log message ***
 *
 * Revision 1.18  2011/05/04 14:41:29  buchholz
 * *** empty log message ***
 *
 * Revision 1.21  2011/05/04 06:27:05  buchholz
 * *** empty log message ***
 *
 * Revision 1.17  2011/05/03 18:55:41  buchholz
 * *** empty log message ***
 *
 * Revision 1.7  2010/11/20 10:22:09  peter
 * *** empty log message ***
 *
 * Revision 1.10  2010/10/30 16:04:05  buchholz
 * *** empty log message ***
 *
 * Revision 1.6  2010/07/18 10:54:16  peter
 * *** empty log message ***
 *
 * Revision 1.4  2010/03/18 22:21:23  buchholz
 * *** empty log message ***
 *
 * Revision 1.5  2010/03/14 11:28:36  peter
 * *** empty log message ***
 *
 * Revision 1.3  2010/02/15 15:37:36  buchholz
 * *** empty log message ***
 *
 * Revision 1.1  2008/11/20 21:14:26  peter
 * Initial revision
 *
 * Revision 1.6  2001/03/14 07:30:00  buchholz
 * defines bzgl LLONG_MAX angepasst
 *
 * Revision 1.5  2000/04/05 16:02:47  kemper
 * *** empty log message ***
 *
 * Revision 1.4  2000/04/05 15:06:06  kemper
 * LLONG_MAX Definition hierher verlagert
 *
 * Revision 1.3  2000/03/15 10:12:03  kemper
 * NONAME zur Vereinheitlichung hierhin verlagert
 *
 * Revision 1.2  2000/03/08 15:19:12  kemper
 * update
 *
 * Revision 1.1  1999/12/13 14:56:12  tepper
 * Initial revision
 *
 */
/*
 *
 *    Sammlung von defines
 *
 *
 */
/* jeweils nur einmal einbinden */
#ifndef DEFINES_H
#define DEFINES_H

/* einzelne defines */
#ifndef STATE_TYPE
#define STATE_TYPE int
#endif
#ifndef ST_MAX_STATE_TYPE
#define ST_MAX_STATE_TYPE 4.294967e+09 /* char: 256, short: 65536, int: 4.294967e+09 */
#endif
#ifndef NULL
#define NULL    0
#endif
#ifndef TRUE
#define TRUE    1
#endif
#ifndef FALSE
#define FALSE   0
#endif
#ifndef SUMME 
#define SUMME   0
#endif
#ifndef PRODUKT
#define PRODUKT 1
#endif
#ifndef RUNDUNGSEPSILON
#define RUNDUNGSEPSILON 1.0e-14
#endif

/* 14.3.00 PK Definition von NONAME zur Vereinheitlichung
   in allen Modulen zentral hierhin verlegt
   Name of the unnamed colour, e.g., occuring in normal Place/Transition-nets, 
   which do not have coloured tokens. */
#ifndef NONAME
#define NONAME "unnamed"
#endif

/* wg Linux Portierung hierher verlagert, LLONG_MAX einheitlich  definieren
   SUN: LONG_LONG_MAX damit 64 bit betrachtet werden
   LINUX: LONG_MAX
*/
#ifndef LLONG_MAX
#include <limits.h>
#endif

#ifndef LLONG_MAX
#ifdef LONG_LONG_MAX
#define LLONG_MAX LONG_LONG_MAX
#else
#define LLONG_MAX ((8==sizeof(long long)) ? 9223372036854775807L : 2147483647L)
#endif
#endif

#endif

#ifndef CONTINUOUS_WEIGHT_FUNC_H
#define CONTINUOUS_WEIGHT_FUNC_H

#include "bmdp_func.h"
#include <stddef.h>

typedef enum continuous_inner_mode_t {
    LOCAL_PURE,
    LOCAL_MIXED,
    GLOBAL_PURE,
    GLOBAL_MIXED
} continuous_inner_mode;

/// A data structure that controls the solution process
typedef struct continuous_optimization_options_t
{
    /// Which inner solver to use
    continuous_inner_mode inner_mode;
    /// A precision bound
    double precision;
    /// The maximal amount of scenarios
    size_t max_subproblems;
    /// A random seed
    unsigned int random_seed;
} continuous_optimization_options;

typedef struct continuous_optimization_problem_t
{
    /*
     * Here, we will make some assumption about the nature of this problem.
     *
     * First and foremost, we restrict the probability distribution to be normal
     * (I know, I know…), with the following properties:
     * - The distribution in each state and action is independent and normal (not
     * neccesary for our solution, but kinda useful for the algorithm and
     * representation)
     * - The distribution's mean is the mean value, and at least 95% of the mass
     * is concentrated between the bounds (and truncated there)
     * - hence, 2\sigma \le min { |p_{mean} - p_{up}|, |p_{mean} - p_{down}| }
     *
     * Second, we assume that the rewards are action-independent. This will simplify a lot if we use CMDP algorithms.
     */
    /// Number of states in the SMDP
    size_t ord;
    /// ``Average'' MDP
    Mdp_matrix *mdp_mean;
    /// Lower bound for the MDP model
    Mdp_matrix *mdp_lower;
    /// Upper bound for the MDP model
    Mdp_matrix *mdp_upper;
    /// The initial vector
    double *initial_vector;
    /// The reward vectors
    Double_vektor **rewards;
    /// The discount factor
    double gamma;
} continuous_optimization_problem;

/// A data structure top describe a solution for the continuous stochastic
/// problem.
typedef struct continuous_optimization_solution_t
{
    /// The policy
    size_t *policy;
    /// An approximate solution value
    double value;
    /// An bound for the error value
    double error;
} continuous_optimization_solution;

/// Optimizes the continuous stochastic optyimization problem
void continuous_weight_optimize(continuous_optimization_problem problem,
                                continuous_optimization_options options,
                                continuous_optimization_solution *solution);

/// Helper function to copy MDP structures
Mdp_matrix *mdp_copy_matrix(Mdp_matrix *src);

/// Helper function to set MDP row to a minimum or maximum
void set_minmax_row(const Mdp_row *restrict lower, const Mdp_row *restrict upper, Mdp_row *row,
                    size_t state, size_t action, const double *restrict values, double *helper,
                    short lower_bound);

/// Helper function which enforces consistent upper and lower bounds for a transition probability matrix row
void fix_row_bounds(Mdp_row *lower_row, Mdp_row *upper_row, size_t split_action);

/// Function that computes the mass of a given subproblem (relative in the given row and action)
double compute_subproblem_mass(Mdp_row* lower_row, Mdp_row* upper_row, size_t action, int parameter);

void trace_row_bounds(Mdp_row* lower_row, Mdp_row* upper_row, size_t action, int parameter);

void check_realloc_ptr(void *ptr);

#endif /* CONTINUOUS_WEIGHT_FUNC_H */

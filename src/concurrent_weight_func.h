#ifndef CONCURRENT_WEIGHT_FUNC_H
#define CONCURRENT_WEIGHT_FUNC_H

/* d_{s, a} */
#define DECISION_VARIABLE_NAME_PATTERN "d_{%lu, %lu}"
/* d_{s, a} */
#define STATE_FREQ_NAME_PATTERN "x_{%lu, %lu}"
/* x_{k, s, a} */
#define CONT_VARIABLE_NAME_PATTERN "x_{%lu, %lu, %lu}"
/* row that constrains decision variable sums */
#define DECISION_CONSTRAINT_NAME_PATTERN "\\sum_a d_{%lu, a} = 1"
/* row that constrains continuous variables */
#define CONT_CONSTRAINT_NAME_PATTERN "cont(%lu, %lu)"

#include "ctmdp_func.h"
#include <gsl/gsl_matrix.h>

typedef struct value_vectors_t {
    size_t ord;
    size_t scenarios;
    double **gains;
} value_vectors;

typedef struct weight_tuple_t {
    size_t scenarios;
    double* weights;
} weight_tuple;

typedef struct concurrent_mdp_t {
    size_t scenarios;
    double *iniv;
    Mdp_matrix **matrices;
    Double_vektor *** rewards;
} concurrent_mdp;

typedef struct cmdp_problem_t {
    concurrent_mdp mdp;
    weight_tuple weights;
    double gamma;
    short comp_max;
} cmdp_problem;

enum cmdp_method {CMDP_METHOD_IMPP, CMDP_METHOD_IMGP, CMDP_METHOD_QCLP, CMDP_METHOD_MIP, CMDP_METHOD_NLP, CMDP_METHOD_NLP2};

typedef struct cmdp_context_t {
    cmdp_problem *problem;
    enum cmdp_method method;
    double result;
    double **policy;
    short presolved;
    short presolve;
} cmdp_context;

/**
 * @brief cmdp_optimize Perform CMDP optimization
 * @param context The optimization parameters
 * @return The resulting value vectors
 */
extern value_vectors cmdp_optimize(cmdp_context *context);

/**
 * @brief mdp_policy_matrix Computes the policy matrix for a given stochastic policy.
 * @param mdp The MDP transition matrices
 * @param policy The (stochastic) policy
 * @return The resulting transition probability matrix
 */
extern gsl_matrix* mdp_policy_matrix(Mdp_matrix *mdp, double **policy);

/**
 * @brief update_policy_matrix Performs a rank-1 update of the transition probability matrix. Performs the update in place.
 * @param mdp The MDP transition probability matrices.
 * @param state The state the change occurs in
 * @param action_from Action from which we take decision probability mass
 * @param action_to Action to which we add decision probability mass
 * @param lambda The amount of change
 * @param policy_matrix The matrix that has to be updated.
 */
extern void update_policy_matrix(Mdp_matrix *mdp, size_t state, size_t action_from, size_t action_to, double lambda, gsl_matrix *policy_matrix);

/**
 * @brief mdp_identity_minus_gamma_matrix Computes the C (= I - \ref gamma P) matrix.
 * @param mdp The MDP transition matrices
 * @param policy The (stochastic) policy
 * @param gamma The discount factor
 * @return The resulting M-matrix
 */
extern gsl_matrix* mdp_identity_minus_gamma_matrix(Mdp_matrix *mdp, double **policy, double gamma);

/**
 * @brief update_i_minus_gamma_matrix Updates the C (= I - \ref gamma P) matrix. Performs the update in place.
 * @param mdp The MDP transition matrices
 * @param gamma Discount factor
 * @param state The state the change occurs in
 * @param action_from Action from which we take decision probability mass
 * @param action_to Action to which we add decision probability mass
 * @param lambda The amount of change
 * @param policy_matrix The matrix that has to be updated.
 */
extern void update_i_minus_gamma_matrix(Mdp_matrix *mdp, double gamma, size_t state, size_t action_from, size_t action_to, double lambda, gsl_matrix *policy_matrix);

/**
 * @brief invert_matrix Helper function to invert a generic square matrix
 * @param matrix The matrix to be inverted
 * @param ord Size of the matrix
 * @param inv The inverted matrix (return argument)
 * @return The inverted matrix \ref inv
 */
extern gsl_matrix *invert_matrix(gsl_matrix *matrix, size_t ord, gsl_matrix *inv);

/**
 * @brief mdp_identity_minus_gamma_inv_matrix Helper function to compute the inverted I - \ref gamma P matrix
 * @param mdp MDP transition matrices
 * @param policy Policy
 * @param gamma Discount factor
 * @return The inverted I - \ref gamma P matrix
 */
extern gsl_matrix *mdp_identity_minus_gamma_inv_matrix(Mdp_matrix *mdp, double **policy, double gamma);

/**
 * @brief update_all_matrices Helper function to update helper matrices after a rank-1 update
 * @param policy_matrices Policy matrices that have to be changed
 * @param c_matrices I - \ref gamma P matrices
 * @param cbar_matrices (I - \ref gamma P)^{-1} matrices
 * @param mdps MDP transition matrices
 * @param gamma The discount factor
 * @param state The state the change occurs in
 * @param action_from Action from which we take decision probability mass
 * @param action_to Action to which we add decision probability mass
 * @param lambda The amount of change
 */
extern void update_all_matrices(gsl_matrix **policy_matrices, gsl_matrix **c_matrices, gsl_matrix **cbar_matrices,
                                concurrent_mdp mdps, double gamma, size_t state, size_t action_from, size_t action_to, double lambda,
                                short full_update, gsl_vector *****uv_cache, gsl_vector *cbars);

/**
 * @brief policy_delta_vector Helper function for \ref mdp_gradient. Computes the difference vector of the rank-1 update operation in a single MDP.
 * @param mdp MDP transition matrices
 * @param state State where the change occurs in
 * @param policy The policy
 * @param action_from Action from which we take decision probability mass
 * @param action_to Action to which we add decision probability mass
 * @param gamma The discount factor
 * @return The difference vector
 */
extern gsl_vector *policy_delta_vector(Mdp_matrix *mdp, size_t state, double *policy, size_t action_from, size_t action_to, double gamma);

/**
 * @brief mdp_gradient Helper function for \ref concurrent_mdp_gradient. Computes the gradient for a rank-1 update in a single MDP.
 * @param mdp The MDP transition matrices we are considering.
 * @param rew The reward vector(s)
 * @param cbar_matrix Helper matrix (inverted identity - \ref gamma * policy matrices)
 * @param iniv Initial probability vector
 * @param policy The policy
 * @param state State where the change occurs in
 * @param action_from Action from which we take decision probability mass
 * @param action_to Action to which we add decision probability mass
 * @param gamma The discount factor
 * @param lambda The amount of change
 * @param cached_cbar_r Cached value vector of this MDP
 * @param uv Pre-allocated workspace for zetas_etas
 * @param cbars Pre-allocated workspace for zetas_etas
 * @param zetap Pointer to cached \zeta value (may be zero, then the value is re-evaluated)
 * @param etap Pointer to cached \eta value (may be zero, then the value is re-evaluated)
 * @return The difference in the value function for the MDP
 */
extern double mdp_gradient(Mdp_matrix *mdp, Double_vektor **rew,
                           gsl_matrix *cbar_matrix,
                           gsl_vector *iniv, double **policy, size_t state, size_t action_from, size_t action_to, double gamma, double lambda,
                           gsl_vector *cached_cbar_r, gsl_vector ****uv_cache, gsl_vector *cbars,
                           double *etap, double *zetap);

/**
 * @brief concurrent_mdp_gradient Helper function for \ref concurrent_impp and \ref concurrent_imgp.
 * Computes the difference if the policy changes in \ref state from \ref action_from to \ref action_to by \ref lambda.
 * @param mdps The concurrent scenario MDPs
 * @param weights The scenario weights
 * @param cbar_matrices Helper matrices (inverted identity - \ref gamma * policy matrices)
 * @param policy The policy
 * @param state State where the change occurs in
 * @param action_from Action from which we take decision probability mass
 * @param action_to Action to which we add decision probability mass
 * @param gamma The discount factor
 * @param lambda The amount of change
 * @param cached_cbar_r Cached value vectors
 * @param uv Pre-allocated workspace for the 'u' vector
 * @param cbars Pre-allocated workspace for the 'cbars' vector
 * @param zetas Cached zetas
 * @param etas Cached etas
 * @return The difference in the value function for all MDPs
 */
extern double concurrent_mdp_gradient(concurrent_mdp mdps, weight_tuple weights,
                                      gsl_matrix **cbar_matrices,
                                      double **policy, size_t state, size_t action_from, size_t action_to, double gamma, double lambda,
                                      gsl_vector **cached_cbar_r, gsl_vector *****uv_cache, gsl_vector *cbars,
                                      double *etas, double *zetas);

/**
 * @brief concurrent_impp Local policy iteration optimization algorithm. Computes a locally optimal pure policy.
 * @param mdps The scenario MDPs
 * @param weights The scenario weights
 * @param policy A (deterministic) policy vector
 * @param gamma The discount factor
 * @param comp_max if we are maximizing
 * @param value A pointer to a variable where the optimal value is stored
 * @return The value vectors of the best found policy
 */
extern value_vectors concurrent_impp(concurrent_mdp mdps, weight_tuple weights, size_t *policy, double gamma, short comp_max, double *value, short presolved);

/**
 * @brief concurrent_imgp Local policy iteration optimization for stochastic policies
 * @param mdps The MDP matrices and rewards
 * @param weights The weights of the different scenarios
 * @param policy A resulting stochastic policy
 * @param gamma The discount factor
 * @param comp_max Whether we maximize (1) or minimize (0)
 * @param value A pointer to a variable where the optimal value is stored
 * @return Value vectors of the most optimal policy that has been found
 */
extern value_vectors concurrent_imgp(concurrent_mdp mdps, weight_tuple weights, double **policy, double gamma, short comp_max, double *value, short presolved);

/**
 * @brief concurrent_mip MIP formulation for the stochastic optimization problem. Computes an optimal policy via CPLEX.
 * @param mdps The MDP scenarios
 * @param weights The weights for each scenario
 * @param policy The resulting policy vector
 * @param gamma The discount factor
 * @param comp_max whether we are maximizing
 * @param value A pointer to a variable where the optimal value is stored
 * @return Value vectors of the most optimal policy that has been found
 */
extern value_vectors concurrent_mip(concurrent_mdp mdps, weight_tuple weights, size_t *policy, double gamma, short comp_max, double *value, short presolved);

/**
 * @brief concurrent_nlp NLP formulation for the stochastic optimization problem
 * @param mdps The MDP matrices and rewards
 * @param weights The weights of the different scenarios
 * @param policy A resulting stochastic policy
 * @param gamma The discount factor
 * @param comp_max Whether we maximize (1) or minimize (0)
 * @param value A pointer to a variable where the optimal value is stored
 * @return Value vectors of the most optimal policy that has been found
 */
extern value_vectors concurrent_nlp(concurrent_mdp mdps, weight_tuple weights, double **policy, double gamma, short comp_max, double *value, short presolved);
extern value_vectors concurrent_nlp_ipopt(concurrent_mdp mdps, weight_tuple weights, double **policy, double gamma, short comp_max, double *value, short presolved);

/**
 * @brief concurrent_qclp QCLP formulation for the stochastic optimization problem
 * @param mdps The MDP matrices and rewards
 * @param weights The weights of the different scenarios
 * @param policy A resulting stochastic policy
 * @param gamma The discount factor
 * @param comp_max Whether we maximize (1) or minimize (0)
 * @param value A pointer to a variable where the optimal value is stored
 * @return Value vectors of the most optimal policy that has been found
 */
extern value_vectors concurrent_qclp(concurrent_mdp mdps, weight_tuple weights, double **policy, double gamma, short comp_max, double *value, short presolved);

/**
 * @brief decision_variable_index Computes the index of a decision variable.
 * @param ord The number of states
 * @param actions The number of actions
 * @param s The current state
 * @param a The current action
 * @return The index of the corresponding decision variable in the MIP formulation
 */
extern int decision_variable_index(size_t ord, size_t actions, size_t s, size_t a);

/**
 * @brief cont_variable_index Computes the index of a continuous variable.
 * @param ord The number of states
 * @param actions The number of actions
 * @param k The current scenario
 * @param s The current state
 * @param a The current action
 * @return The index of the corresponding continuous variable in the MIP formulation
 */
extern size_t cont_variable_index(size_t ord, size_t actions, size_t k, size_t s, size_t a);

/**
 * @brief freq_variable_index Computes the index of a decision variable.
 * @param ord The number of states
 * @param actions The number of actions
 * @param s The current state
 * @param a The current action
 * @return The index of the corresponding decision variable in the MIP formulation
 */
extern int freq_variable_index(size_t ord, size_t actions, size_t k, size_t s, size_t a);

/**
 * @brief concurrent_read_problem Reads a CMDP
 * @param basename The file name of the process file
 * @return the CMDP problem data structure
 */
extern cmdp_problem concurrent_read_problem(char *basename);

extern void concurrent_delete_problem(cmdp_problem cmdp);

/**
 * @brief cmdp_check_matrices Checks the consistency of a CMDP
 * @param cmdp The CMDP object
 * @return the validity, 1 if valid, 0 otherwise
 */
extern short cmdp_check_matrices(concurrent_mdp cmdp);

#endif /* CONCURRENT_WEIGHT_FUNC_H */

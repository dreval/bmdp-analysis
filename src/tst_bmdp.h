/*********************
        Datei:  tst_bmdp.h
        Datum:  1.3.15
        Autor:  Peter Buchholz
        Inhalt: Externe Deklarationen fuer tst_bmdp.c
        Aenderungen:

**********************/
#ifndef TST_BMDP_H
#define TST_BMDP_H

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "bmdp_const.h"
#include "basic_io.h"
#include "global.h"
#include "str_global.h"
#include "ememory.h"
#include "mat_time.h"
#include "mat_int_vektor.h"
#include "mat_row.h"
#include "mat_sparse_matrix.h"
#include "simple_io.h"
#include "ctmdp_func.h"
#include "ctmdp_stat_func.h"
#include "bmdp_func.h"
#include "bmdp_pareto_func.h"
#include "concurrent_weight_func.h"
#include "solution.h"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_linalg.h>

#define DEBUG

extern Sparse_matrix *bmdp_read_one_matrix(FILE *fp, int size);

extern Double_vektor *bmdp_read_one_reward_vector(FILE *fp, int size);

int cmdp_main(int argc, char **argv);
int smdp_main(int argc, char **argv);

#endif /* TST_BMDP_H */

/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c (C) Copyright 2008 by Peter Buchholz
c
c
c     All Rights Reserved
c
c ctmdp_func.c
c
c Functions for the transient analysis of CTMDPs
c
c
c       Datum           Modifikationen
c       ----------------------------------------------------------
c       24.11.08 Datei angelegt
c       20.08.09 Strategie nach Miller implementiert
c       21.07.10 Funktionen fuer time bounded reachability
c       17.08.10 Fehlerbeseitigung
c                Überarbeitung für akkumulierte Rewards fehlt noch!!!!
c       21.09.10 Fehlerbeseitigung Poisson-berechnung dynamische Ansatz max_reach
c       29.12.10 Übergabe der oberen und unteren Schranke in mdp_compute_max_reach
c       11.01.11 Bei CMIN = TRUE wird die untere Schranke berechnet
c bis   08.06.11 Überarbeitung für den akkumulierten Reward
c       17.06.11 Einfuegen der Funtionen mdp_gen_sparse_matrix_structure
c       21.06.11 Einfuegen von mdp_gen_precond_structure
c       31.10.11 Einfuegen von DYNAMIC_REW
c       27.06.14 Einfuegen eienr Funktion, zur Berechnung mittels Diskretisieurng
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/


#include <math.h>
#include "ctmdp_func.h"


/**********************
mdp_row_new ##e
Datum:  28.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function allocates the data structure for a mdp row. 
**********************/
Mdp_row *mdp_row_new (size_t ord)
{
    Mdp_row *mr = (Mdp_row *)ecalloc(1, sizeof(Mdp_row)) ;

    mr->pord = ord ;
    mr->diagonals = (double *) ecalloc((unsigned) ord, sizeof(double)) ;
    mr->no_of_entries = (size_t *) ecalloc((unsigned) ord, sizeof(size_t)) ;
    mr->colind = (size_t **) ecalloc((unsigned) ord, sizeof(size_t *)) ;
    mr->values = (double **) ecalloc((unsigned) ord, sizeof(double *)) ;
    return(mr) ;
} /* mdp_row_free */

/**********************
mdp_row_free ##e
Datum:  24.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function deallocates the data structure for a mdp row. It does not 
give the memory back for the column indices and the values in a row because 
these memory places are deallocated when the matrix structures are released.
**********************/
void mdp_row_free (Mdp_row *r)
{
    efree(r->diagonals) ;
    for (size_t i = 0; i < r->pord; ++i) {
        efree(r->colind[i]);
        efree(r->values[i]);
    }
    efree(r->no_of_entries) ;
    efree(r->colind) ;
    efree(r->values) ;
    efree(r);
} /* mdp_row_free */


/**********************
mdp_matrix_new ##e
Datum:  28.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function allocates the basic data structure for a
mdp_matrix
**********************/
Mdp_matrix *mdp_matrix_new (size_t ord)
{
    Mdp_matrix *v = (Mdp_matrix *)ecalloc(1,sizeof(Mdp_matrix)) ; ;

    v->ord = ord ;
    v->rows = (Mdp_row **) ecalloc (ord, sizeof(Mdp_row *)) ;
    return(v) ;
} /* mdp_matrix_new */

/**********************
mdp_matrix_free ##e
Datum:  24.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function frees the basic data structure for a
mdp_matrix. 
**********************/
void mdp_matrix_free (Mdp_matrix *v)
{
    int i ;

    for (i = 0; i < v->ord; i++) {
        mdp_row_free(v->rows[i]) ;
    }
    efree(v->rows) ;
    efree(v) ;
} /* mdp_matrix_free */

/**********************
mdp_matrix_generate ##e
Datum:  24.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a new mdp_matrix from a set of matrices.
If transform = TRUE the matrcies are transformed into stochastic matrices.
**********************/
Mdp_matrix *mdp_matrix_generate (int nf, int ord, double *alpha, Sparse_matrix **matrices, short transform)
{
    int         i, j, r, c, rord ;
    Mdp_matrix *mm ;

    *alpha = 0.0 ;
    mm = mdp_matrix_new(ord) ;
    /* run through all rows and all matrices */
    for (r = 0; r < ord; r++) {
	rord = 0 ;
	for (i = 0; i < nf; i++) {
	    /* check whether a new diagonal element has been found 
	       only transitions to another state count */
	    if (fabs(matrices[i]->diagonals[r]) > *alpha &&
		((matrices[i]->rowind[r]).no_of_entries > 1 ||
		 (matrices[i]->rowind[r]).colind[0] != r))
		*alpha = fabs(matrices[i]->diagonals[r]) ;
            /* count the number of non empty rows */
	    if ((matrices[i]->rowind[r]).no_of_entries > 0)
		rord++ ;
	} /* for all matrices i = 0 ... nf-1 */
        /* A row has at least one entry */
	if (rord == 0) {
	  rord = 1 ;
	}
	mm->rows[r] = mdp_row_new (rord) ;
        /* Fill the data structure with the row entries and transform the
           rate matrices into stochastic matrices */
	j = 0 ;
	for (i = 0; i < nf; i++) {
	    if ((matrices[i]->rowind[r]).no_of_entries > 0 || i == 0) {
		/* Store the entries in row j of mm */
		/* check for the special case of a self loop */
	      if (((matrices[i]->rowind[r]).no_of_entries == 1 &&
		   (matrices[i]->rowind[r]).colind[0] == r) ||
		  (i == 0 && (matrices[i]->rowind[r]).no_of_entries == 0)) {
		    /* self loop probability 1.0 in the diagonal */
		    mm->rows[r]->diagonals[j] = 0.0 ;
		    mm->rows[r]->no_of_entries[j] = 0 ;
		    mm->rows[r]->colind[j] = NULL ;
		    mm->rows[r]->values[j] = NULL ;
		    if (i == 0 && (matrices[i]->rowind[r]).no_of_entries == 0)
		      printf("Row %i no_entries 0 rord %i\n", r, rord) ;
		}
		else {
		    /* no self loop */
		    mm->rows[r]->diagonals[j] = matrices[i]->diagonals[r] ;
		    mm->rows[r]->no_of_entries[j] = (matrices[i]->rowind[r]).no_of_entries ;
		    mm->rows[r]->colind[j] = (matrices[i]->rowind[r]).colind ;
		    mm->rows[r]->values[j] = (matrices[i]->rowind[r]).val ;
		} /* else no self loop */
		/* next entry is j + 1 */
		j++ ;
	    } /* found a non empty row */

	} /* for all matrices i = 0 ... nf-1 */
    } /* for r = 0 ... ord-1 */
    if (transform) {
      /* now the matrix has to be transformed into a stochastic matrix using P = Q/alpha + I */
      for (r = 0; r < ord; r++) {
	for (j = 0; j < mm->rows[r]->pord; j++) {
	  if (mm->rows[r]->no_of_entries[j] > 0){
	    mm->rows[r]->diagonals[j] = 1.0 + mm->rows[r]->diagonals[j] / *alpha ;
	    for (c = 0; c < mm->rows[r]->no_of_entries[j]; c++) {
	      mm->rows[r]->values[j][c] /= *alpha ;
	    } /* for c = 0...no_of_entries */
	  }
	} /* for j = 0... prod */
      } /* for r = 0 ... ord -1 */
    } /* if transform */
    return(mm) ;
} /* mdp_matrix_generate */

/**********************
mdp_matrix_generate_discrete ##e
Datum:  24.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a new mdp_matrix from a set of matrices.
In contrast to mdp_matrix_generate it uses discrete time MDPS and does not 
interpret rows with self loops.
**********************/
Mdp_matrix *mdp_matrix_generate_discrete (int nf, int ord, Sparse_matrix **matrices)
{
  int         i, r;
  Mdp_matrix *mm ;
  
  mm = mdp_matrix_new(ord) ;
  /* run through all rows and all matrices */
  for (r = 0; r < ord; r++) {
    mm->rows[r] = mdp_row_new (nf) ;
    for (i = 0; i < nf; i++) {
      /* Store the entries in row r action i of mm */
      mm->rows[r]->diagonals[i] = matrices[i]->diagonals[r] ;
      mm->rows[r]->no_of_entries[i] = (matrices[i]->rowind[r]).no_of_entries ;
      mm->rows[r]->colind[i] = (matrices[i]->rowind[r]).colind ;
      mm->rows[r]->values[i] = (matrices[i]->rowind[r]).val ;
    } 
  } /* for r = 0 ... ord-1 */
  return(mm) ;
} /* mdp_matrix_generate_discrete */

/**********************
mdp_gen_sparse_matrix_structure
Datum:  17.06.2011
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a Sparse_matrix data structure that can be used to include
a transition matrix resulting from map_matrix mdp and an arbitrary decision vector. 
**********************/
Sparse_matrix *mdp_gen_sparse_matrix_structure (const Mdp_matrix *mdp)
{
  size_t i, j, k ;
  Sparse_matrix *q ;

  q = mat_sparse_matrix_new((unsigned) mdp->ord) ;
  for (i = 0; i < mdp->ord; i++) {
    k = 0 ;
    for (j = 0; j < mdp->rows[i]->pord; j++) {
      if (mdp->rows[i]->no_of_entries[j] > k) 
	k = mdp->rows[i]->no_of_entries[j] ;
    } /* j = 0 ... pord-1 */
    (q->rowind[i]).val = (double *) ecalloc(k, sizeof(double)) ;
    (q->rowind[i]).colind = (size_t *) ecalloc(k, sizeof(size_t)) ;
    (q->rowind[i]).no_of_entries = k ;
  } /* for i = 0 ...ord-1 */
  return(q) ;
} /* mdp_gen_sparse_matrix_structure */

/**********************
mdp_gen_precond_structure
Datum:  21.06.2011
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function generates a Sparse_matrix data structure that can be used to include
a transition matrix resulting from map_matrix mdp and an arbitrary decision vector
for preconditioned iteration techniques that contains an additional state for the 
normalization condition. 
**********************/
Sparse_matrix *mdp_gen_precond_structure (Mdp_matrix *mdp)
{
  size_t i, j, k ;
  Sparse_matrix *q ;

  q = mat_sparse_matrix_new(mdp->ord+1) ;
  for (i = 0; i < mdp->ord; i++) {
    k = 0 ;
    for (j = 0; j < mdp->rows[i]->pord; j++) {
      if (mdp->rows[i]->no_of_entries[j] > k) 
	k = mdp->rows[i]->no_of_entries[j] ;
    } /* j = 0 ... pord-1 */
    (q->rowind[i]).val = (double *) ecalloc(k+1, sizeof(double)) ;
    (q->rowind[i]).colind = (size_t *) ecalloc(k+1, sizeof(int)) ;
    (q->rowind[i]).no_of_entries = k+1 ;
  } /* for i = 0 ...ord-1 */
  (q->rowind[mdp->ord]).colind = (size_t *) ecalloc(1, sizeof(int)) ;
  (q->rowind[mdp->ord]).val = (double *) ecalloc(1, sizeof(double)) ;
  (q->rowind[mdp->ord]).no_of_entries = 1 ;
  /* The last element in the new row is one */
  (q->rowind[mdp->ord]).colind[0] = 0 ;
  (q->rowind[mdp->ord]).val[0] = 1.0 ;
  q->diagonals[mdp->ord] = -1.0 ;
  return(q) ;
} /* mdp_gen_precond_structure */

/**********************
mdp_set_strategy
Datum:  17.06.2011
Autor:  Peter Buchholz
Input:  mdp MDP matrix
        ind decision vector
Output: pp Matrix resulting from decision vector
Side-Effects:
Description:
The function generates matrix for a given decision vector. Matrix pp has to 
adequately allocated e.g. by the function mdp_gen_sparse_matrix_structure. 
**********************/
void mdp_set_strategy(Mdp_matrix *mdp, Sparse_matrix *pp, int *ind) 
{
  size_t i,j ;

  for (i = 0; i < mdp->ord; i++) {
    pp->diagonals[i] = mdp->rows[i]->diagonals[ind[i]] ;
    (pp->rowind[i]).no_of_entries = mdp->rows[i]->no_of_entries[ind[i]] ;
    for (j = 0; j < mdp->rows[i]->no_of_entries[ind[i]]; j++) {
      (pp->rowind[i]).val[j] = mdp->rows[i]->values[ind[i]][j] ;
      (pp->rowind[i]).colind[j] = mdp->rows[i]->colind[ind[i]][j] ;
    }
  } /* for i = 0,... ord- 1 */
} /* mdp_set_strategy */

/**********************
mdp_set_precond_strategy
Datum:  21.06.2011
Autor:  Peter Buchholz
Input:  mdp MDP matrix
        ind decision vector
Output: pp Matrix resulting from decision vector
Side-Effects:
Description:
The function generates the matrix for a given decision vector for preconditioned
iteration methods. Matrix pp has to adequately allocated e.g. by the function 
mdp_gen_precond_structure. 
**********************/
void mdp_set_precond_strategy(Mdp_matrix *mdp, Sparse_matrix *pp, int *ind) 
{
  size_t i,j ;

  for (i = 0; i < mdp->ord; i++) {
    pp->diagonals[i] = mdp->rows[i]->diagonals[ind[i]] ;
    (pp->rowind[i]).no_of_entries = mdp->rows[i]->no_of_entries[ind[i]]+1 ;
    for (j = 0; j < mdp->rows[i]->no_of_entries[ind[i]]; j++) {
      (pp->rowind[i]).val[j] = mdp->rows[i]->values[ind[i]][j] ;
      (pp->rowind[i]).colind[j] = mdp->rows[i]->colind[ind[i]][j] ;
    }
    (pp->rowind[i]).val[(pp->rowind[i]).no_of_entries-1]    = 1.0      ;
    (pp->rowind[i]).colind[(pp->rowind[i]).no_of_entries-1] = mdp->ord ;
  } /* for i = 0,... ord- 1 */
  (pp->rowind[mdp->ord]).colind[0] = 0 ;
  (pp->rowind[mdp->ord]).val[0]    = 1.0 ;
  /* We need I-P */
  for (i = 0; i < mdp->ord; i++) {
    pp->diagonals[i] = 1.0 - pp->diagonals[i] ;
    for (j = 0; j < (pp->rowind[i]).no_of_entries-1; j++) {
      (pp->rowind[i]).val[j] = -(pp->rowind[i]).val[j] ;
    }
  }
} /* mdp_set_precond_strategy */

/**********************
mdp_comp_vlk ##e
Datum:  21.07.2010
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the vector v[k] from v[k-1] using the optimal strategy for the lower bound.
**********************/
void mdp_comp_vlk (double alpha, Mdp_matrix *mdpm, Int_vektor *d, Double_vektor *vv1, Double_vektor *vv2)
{
  size_t i, j ;

  for (i = 0; i < mdpm->ord; i++) {
    if (mdpm->rows[i]->pord == 0) {
      vv2->vektor[i] = vv1->vektor[i] ;
    }
    else {
      vv2->vektor[i] = vv1->vektor[i] * (mdpm->rows[i]->diagonals[d->vektor[i]] / alpha + 1.0) ;
      for (j = 0; j < mdpm->rows[i]->no_of_entries[d->vektor[i]]; j++) {
	vv2->vektor[i] += mdpm->rows[i]->values[d->vektor[i]][j] * 
	  vv1->vektor[mdpm->rows[i]->colind[d->vektor[i]][j]] / alpha ;
      }
    }
  }
} /* mdp_comp_vlk */

/**********************
mdp_comp_vuk ##e
Datum:  21.07.2010
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the vector v[k] from v[k-1] for the upper bound.
**********************/
void mdp_comp_vuk (double alpha, Mdp_matrix *mdpm, Double_vektor *vv1, Double_vektor *vv2)
{
  size_t i, j, k ;
  double val ;

  for (i = 0; i < mdpm->ord; i++) {
    /* for a zero row the old value has to be kept */
    if (mdpm->rows[i]->pord == 0)
      vv2->vektor[i] = vv1->vektor[i] ;
    else
#if CMIN
      vv2->vektor[i] = 1.0e+12 ;
#else
      vv2->vektor[i] = -1.0e+12 ;
#endif
    for (k = 0; k < mdpm->rows[i]->pord; k++) {
      val =  vv1->vektor[i] * (mdpm->rows[i]->diagonals[k] / alpha + 1.0) ;
      for (j = 0; j < mdpm->rows[i]->no_of_entries[k]; j++) {
	val += mdpm->rows[i]->values[k][j] * vv1->vektor[mdpm->rows[i]->colind[k][j]] / alpha ;
      }
      /* This statement has to be after the closing bracket !!!! */
#if CMIN
	if (val < vv2->vektor[i])
#else
	if (val > vv2->vektor[i])
#endif
	  vv2->vektor[i] = val ;
    } /* for k = 0...pord */
  } /* for i = 0...ord */
} /* mdp_comp_vuk */

/**********************
mdp_find_opt_disc_strategy ##e
Datum:  27.06.2014
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the optimal strategy and the vector vv for the discretization
method.
**********************/
void mdp_find_opt_disc_strategy (Int_vektor *d, Mdp_matrix *mdpm, Double_vektor *vv, 
				 Double_vektor *wv, Double_vektor *rv, double delta) 
{
  size_t i, r, c;
  double Fval, opt_val ;
  
  for (r = 0; r < mdpm->ord; r++) {
#if CMIN
    opt_val = 1.0e+12 ;
#else
    opt_val = -1.0e+12 ;
#endif
    /* Compute the values for vv and the optimal decision */
    for (i = 0; i < mdpm->rows[r]->pord; i++) {
      if (fabs(mdpm->rows[r]->diagonals[i]*delta) > 1.0) {
        printf("delta * diagonal entry = %.3e > 1\nChoose a smaller delta\nSTOP EXECUTION\n",
                fabs(mdpm->rows[r]->diagonals[i]*delta));
      } /* delta too small */
      Fval = rv->vektor[r] * delta + vv->vektor[r] * (1.0 + delta*mdpm->rows[r]->diagonals[i]) ;	   
      for (c = 0; c < mdpm->rows[r]->no_of_entries[i]; c++) {
	Fval += delta*mdpm->rows[r]->values[i][c] *vv->vektor[mdpm->rows[r]->colind[i][c]]; 
      } /* for c = 0,...,no_of_entries */
#if CMIN   
      if (Fval < opt_val) {
#else
      if (Fval > opt_val) {
#endif
	d->vektor[r] = i ;
        opt_val = Fval ;
      } /* if new optimum */
    } /* for i = 0...prod-1 */
    wv->vektor[r] = opt_val ;
  } /* for r = 0 .... ord-1 */
} /* mdp_find_opt_disc_strategy */

/**********************
mdp_find_opt_strategy ##e
Datum:  10.08.2009
Autor:  Ingo Schulz
Input:
Output:
Side-Effects:
Description:
The function computes the optimal strategy and also the vector v^k and w^k (Miller Method).
**********************/
void mdp_find_opt_strategy (Int_vektor *d, short **opt, Mdp_matrix *mdpm, 
			    Double_vektor **vv, Double_vektor **wv, double alpha, int kmax) 
{
  size_t i, r, c;
  int j, k, unknown, sign;
  double Fval, opt_val ;
   
  /* initialize opt */
  for (r = 0; r < mdpm->ord; r++)
    for (i = 0; i < mdpm->rows[r]->pord; i++)
      opt[r][i] = TRUE ;
  sign = 1 ;
  for (k = 0; k <= kmax; k++) {
    for (r = 0; r < mdpm->ord; r++) {
#if CMIN
      opt_val = 1.0e+12 ;
#else
      opt_val = -1.0e+12 ;
#endif
      /* Compute the values for h^k and s^k */
      for (i = 0; i < mdpm->rows[r]->pord; i++) {
	if (opt[r][i]) {
	  Fval = mdpm->rows[r]->diagonals[i]*vv[k]->vektor[r] + wv[k]->vektor[r]   ;	   
	  for (c = 0; c < mdpm->rows[r]->no_of_entries[i]; c++) {
	    Fval += mdpm->rows[r]->values[i][c] *vv[k]->vektor[mdpm->rows[r]->colind[i][c]]; 
	  } /* for c = 0,...,no_of_entries */
          Fval *= (double) sign ;
#if CMIN   
	  if (Fval > opt_val)
#else
	  if (Fval < opt_val)
#endif
	    opt[r][i] = FALSE ;
#if CMIN
	  if (Fval < opt_val) {
	    opt_val = Fval ;
	    d->vektor[r] = i ; 
	    /* all policies with smaller indices are non optimal */
	    for (j = i - 1; j >= 0; j--)
	      opt[r][j] = FALSE ;
	  }
#else
	  if (Fval > opt_val) {
	    opt_val = Fval ;
	    d->vektor[r] = i ; 
	    /* all policies with smaller indices are non optimal */
	    for (j = i - 1; j >= 0; j--)
	      opt[r][j] = FALSE ;
	  }
#endif
	}
      } /* for i = 0...prod-1 */
      if (k < kmax)
	vv[k+1]->vektor[r] = opt_val ;
    } /* for r = 0 .... ord-1 */
    for (r = 0; r < mdpm->ord; r++) {
      unknown = 0 ;
      for (i = 0; i < mdpm->rows[r]->pord; i++) {
	if (opt[r][i]) 
	  unknown++ ;
	if (unknown > 1)
	  break ;
      } /* for i = 0...pord */
      if (unknown > 1) 
	break ;
    } /* for r = 0..ord-1 */
    if (unknown <= 1)
      break ;
    sign *= -1 ;
  } /* for k = 0.. kmax */
} /* mdp_find_opt_strategy */

/**********************
mdp_find_opt_mr_strategy ##e
Datum:  21.07.2010
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the optimal strategy (Miller Method).
**********************/
void mdp_find_opt_mr_strategy (Int_vektor *d, short **opt, Mdp_matrix *mdpm, Double_vektor **vv, int kmax) 
{
  int i, j, k, r, c, unknown, sign ;
  double Fval, opt_val ;

    /* initialize opt */
    for (r = 0; r < mdpm->ord; r++)
	for (i = 0; i < mdpm->rows[r]->pord; i++)
	    opt[r][i] = TRUE ;
    sign = 1 ;
    for (k = 0; k <= kmax; k++) {
	for (r = 0; r < mdpm->ord; r++) {
#if CMIN
	    opt_val =  1.0e+12 ;
#else
	    opt_val = -1.0e+12 ;
#endif
            /* Compute the values for h^k and s^k */
	    for (i = 0; i < mdpm->rows[r]->pord; i++) {
	      if (opt[r][i]) {
		Fval = mdpm->rows[r]->diagonals[i]*vv[k]->vektor[r]  ;	   
		for (c = 0; c < mdpm->rows[r]->no_of_entries[i]; c++) {
		  Fval += mdpm->rows[r]->values[i][c] * vv[k]->vektor[mdpm->rows[r]->colind[i][c]]; 
		} /* for c = 0,...,no_of_entries */
		Fval *= (double) sign ;
#if CMIN
		if (Fval > opt_val)
#else    
		if (Fval < opt_val)
#endif
		  opt[r][i] = FALSE ;
#if CMIN
		if (Fval < opt_val) {
#else
		if (Fval > opt_val) {
#endif
		  opt_val = Fval ;
		  d->vektor[r] = i ; 
		  /* all policies with smaller indices are non optimal */
		  for (j = i - 1; j >= 0; j--)
		    opt[r][j] = FALSE ;
		}
	      }
	    } /* for i = 0...prod-1 */
	    if (k < kmax)
	      vv[k+1]->vektor[r] = opt_val ; 
	} /* for r = 0 .... ord-1 */
	for (r =  0; r < mdpm->ord; r++) {
	  unknown = 0 ;
	  for (i = 0; i < mdpm->rows[r]->pord; i++) {
	    if (opt[r][i])
	      unknown++ ;
	    if (unknown > 1)  
	      break ;
	  }
	  if (unknown > 1)
	    break ;
	}
	if (unknown <= 1) 
	  break ;
	sign *= -1 ;
    } /* for k = 0...ord */
} /* mdp_find_opt_mr_strategy */

/**********************
mdp_compute_alpha
Datum:  21.07.2010
Autor:  Peter BUchholz
Input:
Output:
Side-Effects:
Description:
The function computes alpha for one strategy
**********************/
double mdp_compute_alpha(Int_vektor *d, Mdp_matrix *mdpm) 
{
  int i ;
  double alpha = 0.0 ;

  for (i = 0; i < mdpm->ord; i++) {
    if (mdpm->rows[i]->pord > 0)
      if (fabs(mdpm->rows[i]->diagonals[d->vektor[i]]) > alpha)
	alpha = fabs(mdpm->rows[i]->diagonals[d->vektor[i]]) ;
  }
  return(alpha) ;
} /* mdp_compute_alpha */

/**********************
mdp_poisson_rest ##e
Datum:  25.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the truncation error due to stopping after 
kmax iterations.
**********************/
double mdp_poisson_rest(double alpha, double delta, double rdiff, double gdiff, int kmax) 
{
    int k, left, right, kused ;
    double beta, scale, val, eps1, eps2,res ;

    eps1 = 1.0 ;
    tr_poisson_params (alpha*delta, 1.0e-15, &scale, &val, &left, &right) ;
#if TRC_DETAIL 
    printf("alpha %.3e delta %.3e left %i right %i\n", alpha, delta, left, right) ;
#endif 
    if (right > kmax)
	kused = kmax ;
    else
	kused = right ;
    for (k = left; k <= kused; k++) {
	beta = val /scale ;
	eps1 -= beta ;
        val *= alpha*delta / (double) (k + 1) ;
    }
    res   = eps1 * gdiff ;
    eps2  = eps1 * delta ;
    eps1 -= val /scale ;
    eps2 -= eps1 * ((double) (kused+1) / alpha) ;
    res  += eps2 * rdiff ;
    return(res) ;
} /* mdp_poisson_rest */

/**********************
mdp_poisson_k ##e
Datum:  25.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the truncation error due to stopping after 
iter iterations.
**********************/
double mdp_poisson_k(double alpha, double delta, double gdiff, int iter) 
{
    int k, left, right;
    double beta, scale, val, eps1, res ;

    eps1 = 1.0 ;
    tr_poisson_params (alpha*delta, 1.0e-15, &scale, &val, &left, &right) ;
#if TRC_DETAIL 
    printf("alpha %.3e delta %.3e left %i right %i\n", alpha, delta, left, right) ;
#endif 
    if (right > iter)
	right = iter ;
    for (k = left; k <= right; k++) {
	beta = val /scale ;
	eps1 -= beta ;
        val *= alpha*delta / (double) (k + 1) ;
    }
    res   = eps1 * gdiff ;
    return(res) ;
} /* mdp_poisson_k */

/**********************
mdp_comp_esucc
Datum:  21.07.2010
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the error e_succ.
**********************/
double mdp_comp_esucc(double alpha, double *vdiff, int k, double delta) 
{
  int i, left, right;
  double val, scale, beta=0.0, err = 0.0 ;

    tr_poisson_params (alpha*delta, 1.0e-15, &scale, &val, &left, &right) ;
    if (right > k)
	right = k ;
    for (i = left; i <= right; i++) {
      beta = val /scale ;
      val *= alpha*delta / (double) (i + 1) ;
      err += beta * vdiff[i] ;
    }
    return(err) ;
} /* mdp_comp_esucc */

/**********************
mdp_comp_eacc
Datum:  04.06.2011
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the error e_acc.
**********************/
double mdp_comp_eacc(double alpha, double *rdiff, int k, double delta) 
{
  int i, left, right;
  double val, scale, beta=0.0, err = 0.0, acc = 1.0 ;

    tr_poisson_params (alpha*delta, 1.0e-15, &scale, &val, &left, &right) ;
    if (right > k)
	right = k ;
    for (i = left; i <= right; i++) {
      beta = val /scale ;
      acc -= beta ;
      val *= alpha*delta / (double) (i + 1) ;
      err += acc * rdiff[i] / alpha ;
    }
    return(err) ;
} /* mdp_comp_eacc */

/**********************
 mdp_find_delta_trunc
Datum:  21.07.2010
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes delta and etrunc such that the truncation error is below the threshold
**********************/
double mdp_find_delta_trunc(double tt, double ebound, int k, double alpha1, double rmax, double gmax, 
			    double alpha2, double rmin, double gmin, double *etrunc) 
{
  double d_low, d_up, d_check, v_low, v_up, v_check ;

  d_low = DELTA_MIN ;
  d_up = tt ;
  if (d_up <= d_low) {
    if (rmin + rmax == 0.0)
      v_low = mdp_poisson_k(alpha1, d_low, gmax, k) - mdp_poisson_k(alpha2, d_low, gmin, k) ;
    else
      v_low = mdp_poisson_rest(alpha1, d_low, rmax, gmax, k) - mdp_poisson_rest(alpha1, d_low, rmin, gmin, k) ;
    *etrunc = v_low ;
    return(d_low) ;
  }
  else {
    if (rmin + rmax == 0.0)
      v_up = mdp_poisson_k(alpha1, d_up, gmax, k) - mdp_poisson_k(alpha2, d_up, gmin, k) ;
    else
      v_up = mdp_poisson_rest(alpha1, d_up, rmax, gmax, k) - mdp_poisson_rest(alpha1, d_up, rmin, gmin, k) ;
    if (v_up <= ebound*d_up) {
      *etrunc = v_up ;
      return(d_up) ;
    }
    if (rmin + rmax == 0.0)
      v_low = mdp_poisson_k(alpha1, d_low, gmax, k) - mdp_poisson_k(alpha2, d_low, gmin, k) ;
    else
      v_low = mdp_poisson_rest(alpha1, d_low, rmax, gmax, k) - mdp_poisson_rest(alpha1, d_low, rmin, gmin, k) ;
    if (v_low >= ebound*d_low) {
      *etrunc = v_low ;
      return(d_low) ;
    }
    while (TRUE) {
      d_check = (d_up + d_low) / 2.0 ;
      if (rmin + rmax == 0.0)
	v_check = mdp_poisson_k(alpha1, d_check, gmax, k) - mdp_poisson_k(alpha2, d_check, gmin, k) ;
      else
	v_check = mdp_poisson_rest(alpha1, d_check, rmax, gmax, k) - mdp_poisson_rest(alpha1, d_check, rmin, gmin, k) ;
      if (v_check < ebound*d_check) {
	d_low = d_check ;
	v_low = v_check ;
      }
      else {
	d_up = d_check ;
	v_up = v_check ;
      }
      if (fabs(d_up - d_low) < 1.0e-9) {
	*etrunc = v_low ;
	return(d_low) ;
      }
    }
  }
} /* mdp_find_delta_trunc */

/**********************
 mdp_find_delta_
Datum:  21.07.2010
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes delta such that e_succ + e_trunc < eps 
**********************/
double mdp_find_delta(double alpha1, double alpha2, double d_up, int k, double gmin, double gmax, 
		      double rmin, double rmax, double *rdiff, double *vdiff, double tt, double ti, 
		      double epsilon, double *eps, double eps_base)
{
  double d_low, d_check, v_low, v_up, v_check ;

  d_low = DELTA_MIN ;
  if (d_up <= d_low) {
    return(d_low) ;
  }
  else {
    if (rdiff == NULL)
      v_up = mdp_poisson_k(alpha1, d_up, gmax, k) - mdp_poisson_k(alpha2, d_up, gmin, k) + 
	      mdp_comp_esucc(alpha1, vdiff, k, d_up) ;
    else
      v_up = mdp_poisson_rest(alpha1, d_up, rmax, gmax, k) - mdp_poisson_rest(alpha1, d_up, rmin, gmin, k)  +
	mdp_comp_esucc(alpha1, vdiff, k, d_up) + mdp_comp_eacc(alpha1, rdiff, k, d_up) ;
    if (v_up < (tt + d_up) / ti * (epsilon - eps_base) + eps_base) {
      *eps = v_up ;
      return(d_up) ;
    }
    if (rdiff == NULL)
      v_low = mdp_poisson_k(alpha1, d_low, gmax, k) - mdp_poisson_k(alpha2, d_low, gmin, k) + 
	      mdp_comp_esucc(alpha1, vdiff, k, d_low) ;
    else
      v_low = mdp_poisson_rest(alpha1, d_low, rmax, gmax, k) - mdp_poisson_rest(alpha1, d_low, rmin, gmin, k)  +
	mdp_comp_esucc(alpha1, vdiff, k, d_low) + mdp_comp_eacc(alpha1, rdiff, k, d_low) ;
    if (v_low > (tt + d_low) / ti * (epsilon - eps_base) + eps_base) {
      *eps = v_low ;
      return(d_low) ;
    }
    while (TRUE) {
      d_check = (d_up + d_low) / 2.0 ;
      if (rdiff == NULL)
	v_check = mdp_poisson_k(alpha1, d_check, gmax, k) - mdp_poisson_k(alpha2, d_check, gmin, k) + 
	      mdp_comp_esucc(alpha1, vdiff, k, d_check) ;
    else
      v_check = mdp_poisson_rest(alpha1, d_check, rmax, gmax, k) - mdp_poisson_rest(alpha1, d_check, rmin, gmin, k)  +
	mdp_comp_esucc(alpha1, vdiff, k, d_check) + mdp_comp_eacc(alpha1, rdiff, k, d_check) ;
      if (v_check < (tt + d_check) / ti * (epsilon - eps_base) + eps_base) {
	d_low = d_check ;
	v_low = v_check ;
      }
      else {
	d_up = d_check ;
	v_up = v_check ;
      }
      if (fabs(d_up - d_low) < 1.0e-9) {
	*eps = v_low ;
	return(d_low) ;
      }
    }
  }
} /* mdp_find_delta */ 

/**********************
mdp_compute_bound
Datum:  25.11.2008
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the lower and upper bound from the known vectors vv and wv
**********************/
void mdp_compute_bound(Double_vektor *gt, Double_vektor **vv, Double_vektor **wv, double alpha, 
		       double delta, double gv, double rv, int k) 
{
  int i, left, right;
  unsigned j;
  double beta, scale, val, sum = 1.0, eps1, eps2;

  tr_poisson_params (alpha*delta, 1.0e-15, &scale, &val, &left, &right) ;
  if (right > k)
    right = k ;
  /* if (left > k) 
    left = k+1 ; */
  if (left > right) {
    printf("Left %i right %i (alpha*delta %.3e)approximate solution vector by vector %i\n", 
	   left, right, alpha*delta, k) ;
    for (j = 0; j < vv[0]->no_of_entries; j++) {
      gt->vektor[j] = vv[k]->vektor[j] + wv[k]->vektor[j] / alpha ;
    }
  }
  else {
    for (i = left; i <= right; i++) {
      beta = val /scale ;
      sum -= beta ;
      val *= alpha*delta / (double) (i + 1) ;
      if (i == left) {
	if (wv != NULL) {
	  for (j = 0; j < vv[i]->no_of_entries; j++) {
	    gt->vektor[j] = beta * vv[i]->vektor[j] + sum * wv[i]->vektor[j] / alpha ;
	  }
	}
	else {
	  for (j = 0; j < vv[i]->no_of_entries; j++) {
	    gt->vektor[j] = beta * vv[i]->vektor[j] ;
	  }
	}
      }
      else {
	if (wv != NULL) {
	  for (j = 0; j < vv[i]->no_of_entries; j++) {
	    gt->vektor[j] += beta * vv[i]->vektor[j] + sum * wv[i]->vektor[j] / alpha ;
	  }
	}
	else {
	  for (j = 0; j < vv[i]->no_of_entries; j++) {
	    gt->vektor[j] += beta * vv[i]->vektor[j] ;
	  }
	}
      }
    }
  }
  eps1  = sum * gv ;
  eps2  = sum * delta ;
  sum  -= val /scale ;
  eps2 -= sum * ((double) (right+1) / alpha) ;
  /* Avoid numerical inaccuracies */
  if (eps2 < 0.0)
    eps2 = 0.0 ;
  eps1 += eps2 * rv ;
  if (eps1 > 0.0) {
    for (j = 0; j < gt->no_of_entries; j++) {
      gt->vektor[j] += eps1 ;
    }
  }
} /* mdp_compute_bound */


#if IMPROVE_UPPER_BOUND
/**********************
mdp_compute_better_bound 
Datum:  03.06.2011
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the improved upper bounds for a known delta.
**********************/
void mdp_compute_better_bound(double alpha, Mdp_matrix *mdpm, Double_vektor **hv1, Double_vektor **hv2, int kmax) 
{
  int k;
  size_t r, i, c;
  double hval, opt_h ;
  Double_vektor *hv_help ;
  
  for (k = 1; k <= kmax; k++) {
    for (r = 0; r < mdpm->ord; r++) {
#if CMIN
      opt_h = 1.0e+12 ;
#else
      opt_h = -1.0e+12 ;
#endif
      /* Compute the values for h^k */
      for (i = 0; i < mdpm->rows[r]->pord; i++) {
	hval = (1.0 + mdpm->rows[r]->diagonals[i]/alpha) * (*hv1)->vektor[r] ;
	for (c = 0; c < mdpm->rows[r]->no_of_entries[i]; c++) {
	  hval += mdpm->rows[r]->values[i][c] / alpha * (*hv1)->vektor[mdpm->rows[r]->colind[i][c]] ;
      }
#if CMIN
	if (hval < opt_h) {
	  opt_h = hval ;
	}
#else
	if (hval > opt_h) {
	  opt_h = hval ;
	}
#endif
      } /* for i = 0...prod-1 */
      (*hv2)->vektor[r] = opt_h ;
    } /* for r = 0 .... ord-1 */
    hv_help = *hv1   ;
    *hv1    = *hv2   ;
    *hv2   = hv_help ;
  } /* for k = 1...k_max */
} /* mdp_compute_better_bound */

/**********************
mdp_improve_upper_bound
Datum:  0.3.06.2011
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
The function computes the improved upper bound
**********************/
double mdp_improve_upper_bound (int kmax, double delta, double alpha, double rmax, double gmax, Double_vektor *xv,
			     Double_vektor *yv, Double_vektor *gtl, Double_vektor *gtu, Mdp_matrix *mdpm)
{
  int     k, r, left, right, kused ;
  double  eps1, eps2, val, scale, beta, res ;
  Double_vektor *hv, *hv2 ;

  tr_poisson_params(alpha*delta, 1.0e-15, &scale, &val, &left, &right) ;
  if (right >= kmax)
    kused = kmax ;
  else
    kused = right ;
  if (kused < left) {
    eps1 = 0.0 ;
    for (r = 0; r < gtu->no_of_entries; r++)
      if (gtu->vektor[r] - gtl->vektor[r])
	eps1 = gtu->vektor[r] - gtl->vektor[r] ;
    return(eps1) ;
  }
  hv  = mat_double_vektor_new(gtu->no_of_entries) ;
  hv2 = mat_double_vektor_new(gtu->no_of_entries) ;
  for (r = 0; r < gtu->no_of_entries; r++) {
#if CMIN
    gtl->vektor[r] = 0.0 ;
#else
    gtu->vektor[r] = 0.0 ;
#endif
  } /* for r = 0...no_of_entries */
  eps1 = 1.0 ;
  for (k = 0; k <= kused; k++) {
    if (k >= left) {
      beta = val /scale ;
      eps1 -= beta ;
      /* Compute the new decision vector for the next transition */
      for (r = 0; r < gtu->no_of_entries; r++) {
	hv->vektor[r] = beta * xv->vektor[r] + eps1 * yv->vektor[r] / alpha ;
      }
      if (k > 0) {
	mdp_compute_better_bound(alpha, mdpm, &hv, &hv2, k) ;
      }
      for (r = 0; r < gtu->no_of_entries; r++) {
#if CMIN
	gtl->vektor[r] += hv->vektor[r] ;
#else
	gtu->vektor[r] += hv->vektor[r] ;
#endif
      } /* for r = 0 ... no_of_entrie s- 1*/
      val *= alpha*delta / (double) (k + 1) ;
    }
  } /* for k = left to right */
  res = eps1 * gmax ;
  eps2  = eps1 * delta ;
  eps1 -= val /scale ;
  eps2 -= eps1 * ((double) (kused+1) / alpha) ;
  /* Avoid numerical problems */
  if (eps2 < 0.0)
    eps2 = 0.0 ;
  res  += eps2 * rmax ;
  eps1 = 0.0 ;
  for (r = 0; r < gtl->no_of_entries; r++) {
#if CMIN
    gtl->vektor[r] += res ;
#else
    gtu->vektor[r] += res ;
#endif
    if (gtu->vektor[r] - gtl->vektor[r] > eps1) 
      eps1 = gtu->vektor[r] - gtl->vektor[r] ;
  } /* for r = 0...no_of_entries */
  mat_double_vektor_free(hv) ;
  mat_double_vektor_free(hv2) ;
  return(eps1) ;
} /* mdp_improve_upper_bound */

#endif

/**********************
mdp_compute_unif_result ##e
Datum:  19.08.2009
Autor:  Peter Buchholz
Co Autor: Ingo Schulz
Input:
Output:
Side-Effects:
Description:
The function computes the result vector.
**********************/
int mdp_compute_unif_result (double *gl, double *gu, double ti, double alpha, double *epsilon,
			Double_vektor *rvl,Double_vektor *rvu, Double_vektor *pv, Double_vektor *gtl, 
			Double_vektor *gtu, Mdp_matrix *mdpm, int *global_iter, int kmin, 
			int kmax, double omega, double eps_base)
{
  short           stop ;
  int             i, k, steps=0 ;
  double          alpha2, delta,  eps, tt, rmax, rmin, gmax, gmin, etrunc, esucc, eacc, *rdiff, *vdiff ;
  short         **opt ;
  Int_vektor     *d  ;
  Double_vektor **vv, **wv, **vvu, **wvu ;
#if DYNAMIC_REW
  double dynamic_val ;
#endif
#if IMPROVE_UPPER_BOUND
 /* double          eps_old ; */
#endif
#if TRC_STRATEGY
  short           new_str = FALSE ;
  double          tt_old  = 0.0   ;
  Int_vektor     *d_old ;
  
  d_old = mat_int_vektor_new(rvl->no_of_entries) ;
  for (i = 0; i < rvl->no_of_entries; i++)
    d_old->vektor[i] = -1 ;
#endif
#if DYNAMIC_REW
  dynamic_val = rvu->vektor[rvu->no_of_entries -1] ;
  printf("alpha %.3e dynamic val %.3e\n", alpha, dynamic_val) ;
#endif
  
  /* We need K_MAX+1 vectors for wv, wvu, vv, vvu */
  vv = (Double_vektor **) ecalloc(kmax+1, sizeof(Double_vektor *)) ;
  wv = (Double_vektor **) ecalloc(kmax+1, sizeof(Double_vektor *)) ;
  vvu = (Double_vektor **) ecalloc(kmax+1, sizeof(Double_vektor *)) ;
  wvu = (Double_vektor **) ecalloc(kmax+1, sizeof(Double_vektor *)) ;
  vdiff = (double *) ecalloc(kmax+1, sizeof(double)) ;
  rdiff = (double *) ecalloc(kmax+1, sizeof(double)) ;
  
  for (i = 0; i <= kmax; i++) {
    vv[i] = mat_double_vektor_new(rvl->no_of_entries) ;
    vvu[i] = mat_double_vektor_new(rvl->no_of_entries) ;
    wv[i]  = mat_double_vektor_new(rvl->no_of_entries) ;
    wvu[i] = mat_double_vektor_new(rvl->no_of_entries) ;
  } 
  /* wv[0] is always rv and hv[0] = gvl
     we copy vector rv into wv[0] to avoid problems with deallocation of memory */
  for (i = 0; i < rvl->no_of_entries; i++) {
    wv[0]->vektor[i]  = rvl->vektor[i] ;
    wvu[0]->vektor[i] = rvu->vektor[i] ;
  }
#if DYNAMIC_REW
#if DYNAMIC_ADD
  wvu[0]->vektor[rvu->no_of_entries - 1] = dynamic_val * (1.0 + ti *alpha) ;
  wv[0]->vektor[rvu->no_of_entries - 1] = dynamic_val * (1.0 + ti *alpha) ;
#else
  wvu[0]->vektor[rvu->no_of_entries - 1] = dynamic_val * exp((dynamic_val - 1.0)* ti *alpha) ;
  wv[0]->vektor[rvu->no_of_entries - 1] = dynamic_val * exp((dynamic_val - 1.0)* ti *alpha) ;
#endif
#else
#if CMIN
  /* for minimization the dynamic strategy gives the lower bound 
     the vectors are precomputed once */
  for (k = 1; k <= kmax; k++)
    mdp_comp_vuk(alpha, mdpm, wv[k-1], wv[k]) ;
#else
  /* for maximization the dynamic strategy gives the upper bound 
     the vectors are precomputed once */
  for (k = 1; k <= kmax; k++)
    mdp_comp_vuk(alpha, mdpm, wvu[k-1], wvu[k]) ;
#endif
#endif
  
  /* Vector for the optimal strategy */
  d = mat_int_vektor_new(rvl->no_of_entries) ;
  /* opt has to have dimension ord, mi */
  opt = (short **) ecalloc(mdpm->ord, sizeof(double *)) ;
  for (i = 0; i < mdpm->ord; i++) {
    opt[i] = (short *) ecalloc(mdpm->rows[i]->pord, sizeof(short)) ;
  }
  tt = eps = 0.0 ;
  delta=0.0; 
  while (tt < ti) {
#if DYNAMIC_REW
#if DYNAMIC_ADD
    wvu[0]->vektor[rvu->no_of_entries - 1] = dynamic_val * (1.0 + (ti-tt) *alpha) ;
    wv[0]->vektor[rvu->no_of_entries - 1] = dynamic_val * (1.0 + (ti-tt) *alpha) ;
    rvu->vektor[rvu->no_of_entries - 1] = dynamic_val * (1.0 + (ti-tt) *alpha) ;
    rvl->vektor[rvu->no_of_entries - 1] = dynamic_val * (1.0 + (ti-tt) *alpha) ;
#else
    wvu[0]->vektor[rvu->no_of_entries - 1] = dynamic_val * exp((dynamic_val - 1.0)* (ti-tt) *alpha) ;
    wv[0]->vektor[rvu->no_of_entries - 1] = dynamic_val * exp((dynamic_val - 1.0)* (ti-tt) *alpha) ;
    rvu->vektor[rvu->no_of_entries - 1] = dynamic_val * exp((dynamic_val - 1.0)* (ti-tt) *alpha) ;
    rvl->vektor[rvu->no_of_entries - 1] = dynamic_val * exp((dynamic_val - 1.0)* (ti-tt) *alpha) ;
#endif
#endif
    /* initialize vv[0] with gtl,
       we copy the values to avoid later problems when 
       memory is deallocated */
    vdiff[0] = rdiff[0] = 0.0 ;
    for (i = 0; i < rvl->no_of_entries; i++) {
      vv[0]->vektor[i]  = gtl->vektor[i] ;
      vvu[0]->vektor[i] = gtu->vektor[i] ; 
      if (fabs(vvu[0]->vektor[i] - vv[0]->vektor[i]) > vdiff[0])
	vdiff[0] = fabs(vvu[0]->vektor[i] - vv[0]->vektor[i]) ;
      if (fabs(wvu[0]->vektor[i] - wv[0]->vektor[i]) > rdiff[0]) 
	rdiff[0] = fabs(wvu[0]->vektor[i] - wv[0]->vektor[i]) ;
    }
    mdp_find_opt_strategy (d, opt, mdpm, vv, wv, alpha, kmax) ;
    alpha2 = mdp_compute_alpha(d, mdpm) ;
    if (kmin < kmax) {
      /* adaptive computation */
      k = 1 ;
      stop = FALSE ; 
      while (!(stop)) {
#if CMIN
	/* for minimization the fixed strategy gives the upper bound */
	mdp_comp_vlk(alpha, mdpm, d, vvu[k-1], vvu[k]) ;
	mdp_comp_vuk(alpha, mdpm, vv[k-1], vv[k]) ;
	mdp_comp_vlk(alpha, mdpm, d, wvu[k-1], wvu[k]) ;
	/* wv has been precomputed */
#if DYNAMIC_REW
	/* but needs to be recomputed if the rewards change */
	mdp_comp_vuk(alpha, mdpm, wv[k-1], wv[k]) ;
#endif
#else
	/* for maximization the fixed strategy gives the lower bound */
	mdp_comp_vlk(alpha, mdpm, d, vv[k-1], vv[k]) ;
	mdp_comp_vuk(alpha, mdpm, vvu[k-1], vvu[k]) ;
	mdp_comp_vlk(alpha, mdpm, d, wv[k-1], wv[k]) ;
	/* wvu has been precomputed */
#if DYNAMIC_REW
	/* but needs to be recomputed if the rewards change */
	mdp_comp_vuk(alpha, mdpm, wvu[k-1], wvu[k]) ;
#endif

#endif
	(*global_iter)++ ;
	/* Compute gmin, gmax, rmin and rmax */
	gmax = -1.0e+10 ;
	gmin =  1.0e+10 ;
	vdiff[k] = 0.0 ;
	for (i = 0; i < vv[k]->no_of_entries; i++) {
	  if (vvu[k]->vektor[i] > gmax)
	    gmax = vvu[k]->vektor[i] ;
	  if (vv[k]->vektor[i] < gmin)
	    gmin = vv[k]->vektor[i] ;
	  if (fabs(vvu[k]->vektor[i] - vv[k]->vektor[i]) > vdiff[k])
	    vdiff[k] = fabs(vvu[k]->vektor[i] - vv[k]->vektor[i]) ; 
	}
	rmax = -1.0e+10 ;
	rmin =  1.0e+10 ;
	rdiff[k] = 0.0 ;
	for (i = 0; i < wv[k]->no_of_entries; i++) {
	  if (wvu[k]->vektor[i] > rmax)
	    rmax = wvu[k]->vektor[i] ;
	  if (wv[k]->vektor[i] < rmin)
	    rmin = wv[k]->vektor[i] ;
	  if (fabs(wvu[k]->vektor[i] - wv[k]->vektor[i]) > rdiff[k])
	    rdiff[k] = fabs(wvu[k]->vektor[i] - wv[k]->vektor[i]) ; 
	}
	delta = mdp_find_delta_trunc(ti-tt, (*epsilon-eps_base)*omega/ti, k, alpha, rmax, 
				     gmax, alpha2, rmin, gmin, &etrunc) ;
	esucc = mdp_comp_esucc(alpha, vdiff, k, delta) ;
	eacc  = mdp_comp_eacc(alpha, rdiff, k, delta) ;
	if (etrunc + esucc + eacc > (tt +delta) / ti * (*epsilon-eps_base)+eps_base && k >= kmin 
	    && etrunc < (*epsilon-eps_base) * delta * omega / ti) {
	  delta = mdp_find_delta(alpha, alpha, ti-tt, k, gmin, gmax, rmin, rmax, 
				 rdiff, vdiff, tt, ti, *epsilon, &eps, eps_base) ;
	  stop = TRUE ;
	}
	else {
	  eps = etrunc + esucc + eacc ;
	  if (delta >= ti - tt) 
	    stop = TRUE ;
	  else {
	    if (k >= kmax) {
	      stop = TRUE ;
	    }
	    else
	      k++ ;
	  }
	}
      } /* end while */
    } /* kmin < kmax */
    else {
      /* non adaptive computation */
      for (k = 1; k <= kmax; k++) {
#if CMIN
	/* for minimization the fixed strategy gives the upper bound */
	mdp_comp_vlk(alpha, mdpm, d, vvu[k-1], vvu[k]) ;
	mdp_comp_vuk(alpha, mdpm, vv[k-1], vv[k]) ;
	mdp_comp_vlk(alpha, mdpm, d, wvu[k-1], wvu[k]) ;
	/* wv has been precomputed */
#if DYNAMIC_REW
	/* but needs to be recomputed if the rewards change */
	mdp_comp_vuk(alpha, mdpm, wv[k-1], wv[k]) ;
#endif
#else
	/* for maximization the fixed strategy gives the lower bound */
	mdp_comp_vlk(alpha, mdpm, d, vv[k-1], vv[k]) ;
	mdp_comp_vuk(alpha, mdpm, vvu[k-1], vvu[k]) ;
	mdp_comp_vlk(alpha, mdpm, d, wv[k-1], wv[k]) ;
	/* wvu has been precomputed */
#if DYNAMIC_REW
	/* but needs to be recomputed if the rewards change */
	mdp_comp_vuk(alpha, mdpm, wvu[k-1], wvu[k]) ;
#endif
#endif
	(*global_iter)++ ;
	/* compute vdiff and rdiff */
	vdiff[k] = rdiff[k] = 0.0 ;
	for (i = 0; i < vv[k]->no_of_entries; i++) {
	  if (fabs(vvu[k]->vektor[i] - vv[k]->vektor[i]) > vdiff[k])
	    vdiff[k] = fabs(vvu[k]->vektor[i] - vv[k]->vektor[i]) ;
	  if (fabs(wvu[k]->vektor[i] - wv[k]->vektor[i]) > rdiff[k]) 
	    rdiff[k] = fabs(wvu[k]->vektor[i] - wv[k]->vektor[i]) ;
	}
      } /* for k = 1...kmax */
      /* Compute gmin, gmax, rmin and rmax */
      gmax = -1.0e+10 ;
      gmin =  1.0e+10 ;
      for (i = 0; i < vv[kmax]->no_of_entries; i++) {
	if (vvu[kmax]->vektor[i] > gmax)
	  gmax = vvu[kmax]->vektor[i] ;
	if (vv[kmax]->vektor[i] < gmin)
	gmin = vv[kmax]->vektor[i] ; 
      } 
      /* compute rmin and rmax */
      rmax = -1.0e+10 ;
      rmin =  1.0e+10 ;
      for (i = 0; i < wv[kmax]->no_of_entries; i++) {
	if (wvu[kmax]->vektor[i] > rmax)
	  rmax = wvu[kmax]->vektor[i] ;
	if (wv[kmax]->vektor[i] < rmin)
	rmin = wv[kmax]->vektor[i] ; 
      } 
      k = kmax ;
      delta = mdp_find_delta(alpha, alpha, ti-tt, k, gmin, gmax, rmin, rmax, 
			     rdiff, vdiff, tt, ti, *epsilon, &eps, eps_base) ;
    } /* non_adaptive computation */
    /* The case alpha << alpha2 is not considered yet since this requires a recomputation of
       wv */
    if (delta*alpha > 3.0 *(double) k)
      delta = 3.0 * (double) k / alpha ;
    mdp_compute_bound(gtl, vv, wv, alpha, delta, gmin, rmin, k) ;
    mdp_compute_bound(gtu, vvu, wvu, alpha, delta, gmax, rmax, k) ; 
 #if IMPROVE_UPPER_BOUND 
    /*   eps_old = eps ; */
#if CMIN
    eps = mdp_improve_upper_bound(k, delta, alpha, rmin, gmin, vv[0], rvl, gtl, gtu, mdpm) ;
#else
    eps = mdp_improve_upper_bound(k, delta, alpha, rmax, gmax, vvu[0], rvu, gtl, gtu, mdpm) ;
#endif
    /* printf("Bound improved from %.5e to %.5e (diff %.3e)\n", eps_old, eps, eps_old - eps) ; */
    /* if (eps_old < eps)
       exit(-1) ; */
#endif
#if TRC_STRATEGY
    tt_old = tt ;
#endif
    tt += delta ;
    steps++ ;
#if TRC_ITERATION
    printf("Time step of length %.3e to time %.3e resulting in error %.3e\n", delta, tt, eps) ;
#endif
#if TRC_STRATEGY
    new_str = FALSE ;
    for (i = 0; i < d->no_of_entries; i++) {
      if (d->vektor[i] != d_old->vektor[i]) {
	new_str = TRUE ;
	break ;
      }
    }
    if (new_str) {
      printf("New strategy (%.5e): ", tt_old) ;
      for (i = 0; i < d->no_of_entries; i++) {
	printf("%i ", d->vektor[i]) ;
	d_old->vektor[i] = d->vektor[i] ;
      }
      printf("\n") ;
    }
#endif
  } /* while tt < ti */
    /* Compute the final results */
  esucc = eps ;
  eps = 0.0 ;
  for (i = 0; i < gtl->no_of_entries; i++) {
    if (gtu->vektor[i] - gtl->vektor[i] > eps)
      eps = gtu->vektor[i] - gtl->vektor[i] ;
    if (gtl->vektor[i] > gtu->vektor[i])
      printf("%i gtl %.3e gtu %.3e diff %.3e\n", i, gtl->vektor[i], gtu->vektor[i], gtu->vektor[i] - gtl->vektor[i]) ;
  }
  printf("Computed eps %.3e real eps %.3e diff %.3e\n", esucc, eps, esucc-eps) ;
  *gl = mat_double_vektor_inner_product(pv, gtl) ;
  *gu = mat_double_vektor_inner_product(pv, gtu) ;
#if TRC_VECTOR
  printf("g_min: ") ;
  for (i = 0; i < gtl->no_of_entries; i++)
    printf("%.5e ", gtl->vektor[i]) ;
  printf("\n") ;
  printf("g_max: ") ;
  for (i = 0; i < gtu->no_of_entries; i++)
    printf("%.5e ", gtu->vektor[i]) ;
  printf("\n") ;
#endif
  /* Deallocate some memory */
  for (i = 0; i <= kmax; i++) {
    mat_double_vektor_free(vv[i]) ;
    mat_double_vektor_free(vvu[i]) ;
    mat_double_vektor_free(wv[i]) ;
    mat_double_vektor_free(wvu[i]) ;
  }
  efree(vv)  ;
  efree(vvu) ;
  efree(wv)  ;
  efree(wvu) ;
  efree(vdiff) ;
  efree(rdiff) ;
  mat_int_vektor_free(d) ;
#if TRC_STRATEGY
  mat_int_vektor_free(d_old) ;
#endif
  for (i = 0; i < mdpm->ord; i++)
    efree(opt[i]) ;
  efree(opt) ;
  *epsilon = eps ;
  return(steps) ;
} /* mdp_compute_unif_result */

/**********************
mdp_compute_max_reach ##e
Datum:  19.08.2009
Autor:  Peter Buchholz
Co Autor: Ingo Schulz
Input:
Output:
Side-Effects:
Description:
The function computes the time bounded maximum reachability.
**********************/

int mdp_compute_max_reach (double *gl, double *gu, double ti, double alpha, double *epsilon,  
			   Double_vektor *pv, Double_vektor *gtl, Double_vektor *gtu, Mdp_matrix *mdpm, int *global_iter,
			   int kmin, int kmax, double omega, double eps_base)
{
  int             i, k, steps=0 ;
  double          alpha2, delta,  eps, tt, gmax, gmin, etrunc, esucc, *vdiff ;
    short         **opt, stop = FALSE ;
    Int_vektor     *d  ;
    Double_vektor **vv, **vvu ;

#if TRC_STRATEGY
    short           new_str = FALSE ;
    double          tt_old  = 0.0   ;
    Int_vektor     *d_old ;

    d_old = mat_int_vektor_new(gtl->no_of_entries) ;
    for (i = 0; i < gtu->no_of_entries; i++)
	d_old->vektor[i] = -1 ;
#endif

    /* We need kmax+1 vectors for vl and vu */
    vv = (Double_vektor **) ecalloc(kmax+1, sizeof(Double_vektor *)) ;
    vvu = (Double_vektor **) ecalloc(kmax+1, sizeof(Double_vektor *)) ;
    vdiff = (double *) ecalloc(kmax+1, sizeof(double)) ;
    for (i = 0; i <= kmax; i++) {
	vv[i] = mat_double_vektor_new(gtu->no_of_entries) ;
	vvu[i] = mat_double_vektor_new(gtu->no_of_entries) ;
    } 
    /* Vector for the optimal strategy */
    d = mat_int_vektor_new(gtu->no_of_entries) ;
    /* opt has to have dimension ord, mi */
    opt = (short **) ecalloc(mdpm->ord, sizeof(short *)) ;
    for (i = 0; i < mdpm->ord; i++) {
      opt[i] = (short *) ecalloc(mdpm->rows[i]->pord, sizeof(short)) ;
    }
    tt = eps = 0.0 ;
    delta=1.0; 
    while (tt < ti) {
	/* initialize gmax and gmin */
	gmax = -1.0e+10 ;
	gmin =  1.0e+10 ;
	for (i = 0; i < gtl->no_of_entries; i++) {
	    if (gtu->vektor[i] > gmax)
		gmax = gtu->vektor[i] ;
	    if (gtl->vektor[i] < gmin)
		gmin = gtl->vektor[i] ;
	}
	/* initialize vv[0] with gtl and vu with gtu,
	   we copy the values to avoid later problems when 
	   memory is deallocated */
	vdiff[0] = 0.0 ;
	for (i = 0; i < gtu->no_of_entries; i++) {
	  vv[0]->vektor[i]  = gtl->vektor[i] ;
	  vvu[0]->vektor[i] = gtu->vektor[i] ;
	  if (vvu[0]->vektor[i] - vv[0]->vektor[i] > vdiff[0])
	    vdiff[0] = vvu[0]->vektor[i] - vv[0]->vektor[i] ;
	}
	mdp_find_opt_mr_strategy (d, opt, mdpm, vv, kmax) ;
	alpha2 = mdp_compute_alpha(d, mdpm) ; 
	/* alpha2 = alpha ; */
	if (kmin < kmax) {
	  /* adaptive computation */
	  k = 1 ;
	  stop = FALSE ;
	  while (!(stop)) {
#if CMIN
	    /* for minimization the fixed strategy gives the upper bound !! */
	    mdp_comp_vlk (alpha, mdpm, d, vvu[k-1], vvu[k]) ;
	    mdp_comp_vuk(alpha, mdpm, vv[k-1], vv[k]) ;
#else
	    mdp_comp_vlk (alpha, mdpm, d, vv[k-1], vv[k]) ;
	    mdp_comp_vuk(alpha, mdpm, vvu[k-1], vvu[k]) ;
#endif
	    *global_iter = *global_iter + 1;
	    vdiff[k] = 0.0 ;
	    for (i = 0; i < gtu->no_of_entries; i++) {
	      if (fabs(vvu[k]->vektor[i] - vv[k]->vektor[i]) > vdiff[k]) 
		vdiff[k] = fabs(vvu[k]->vektor[i] - vv[k]->vektor[i]) ;
	    }
	    delta = mdp_find_delta_trunc(ti-tt, (*epsilon-eps_base)*omega/ti, k, alpha, 
					 0.0, gmax, alpha2, 0.0, gmin, &etrunc) ; 
	    esucc = mdp_comp_esucc(alpha, vdiff, k, delta) ;
	    if (etrunc + esucc > (tt + delta) / ti * (*epsilon - eps_base) + eps_base && k >= kmin && 
		etrunc < (*epsilon - eps_base) * delta * omega / ti) {
	      delta = mdp_find_delta(alpha, alpha2, ti-tt, k, gmin, gmax, 0.0, 0.0, NULL, 
				     vdiff, tt, ti, *epsilon, &eps, eps_base) ;
	      /* if (k <= 1)
		 exit(-1) ; */
	      stop = TRUE ;
	    }
	    else {
	      eps = etrunc + esucc ;
	      if (delta >= ti - tt) 
		stop = TRUE ;
	      else {
		if (k >= kmax) {
		  stop = TRUE ;
		}
		else
		  k++ ;
	      }
	    } 
	  } /* while */
	}
	else {
	  /* non adaptive computation */
	  for (k = 1; k <= kmax; k++) {
#if CMIN
	    mdp_comp_vlk (alpha, mdpm, d, vvu[k-1], vvu[k]) ;
	    mdp_comp_vuk(alpha, mdpm, vv[k-1], vv[k]) ;
#else
	    mdp_comp_vlk (alpha, mdpm, d, vv[k-1], vv[k]) ;
	    mdp_comp_vuk(alpha, mdpm, vvu[k-1], vvu[k]) ;
#endif
	    *global_iter = *global_iter + 1;
	    vdiff[k] = 0.0 ;
	    for (i = 0; i < gtu->no_of_entries; i++) {
	      if (fabs(vvu[k]->vektor[i] - vv[k]->vektor[i]) > vdiff[k]) 
		vdiff[k] = fabs(vvu[k]->vektor[i] - vv[k]->vektor[i]) ;
	    }
	  }
	  k = kmax ;
	  delta = mdp_find_delta(alpha, alpha2, ti-tt, k, gmin, gmax, 0.0, 0.0, NULL,
				 vdiff, tt, ti, *epsilon, &eps, eps_base) ;
	} /* kmin == kmax */
	if (alpha2 < 0.01*alpha) {
	  /*printf("alpha %.3e alpha2 %.3e diff %.3e recompute vl\n", alpha, alpha2, alpha-alpha2) ; */
	  /* Recompute vv according to alpha2 */
	  for (i = 1; i <= k; i++)
#if CMIN
	    mdp_comp_vlk (alpha2, mdpm, d, vvu[i-1], vvu[i]) ;
#else
	    mdp_comp_vlk (alpha2, mdpm, d, vv[i-1], vv[i]) ;
#endif
#if CMIN
	    mdp_compute_bound(gtu, vvu, NULL, alpha2, delta, gmin, 0.0, k) ;
#else
	    mdp_compute_bound(gtl, vv, NULL, alpha2, delta, gmin, 0.0, k) ;
#endif
	}
	else {
	  mdp_compute_bound(gtl, vv, NULL, alpha, delta, gmin, 0.0, k) ;
	}
#if CMIN
	if (alpha2 < 0.01*alpha)
	  mdp_compute_bound(gtl, vv, NULL, alpha, delta, gmax, 0.0, k) ;
	else
	  mdp_compute_bound(gtu, vvu, NULL, alpha, delta, gmax, 0.0, k) ;
#else
	mdp_compute_bound(gtu, vvu, NULL, alpha, delta, gmax, 0.0, k) ;
#endif
#if TRC_STRATEGY
	tt_old = tt ;
#endif
	tt += delta ;
	steps++ ;
#if TRC_ITERATION
	printf("Time step of length %.3e to time %.3e resulting in error %.3e\n", delta, ti-tt, eps) ;
#endif
#if TRC_STRATEGY
	new_str = FALSE ;
	for (i = 0; i < d->no_of_entries; i++) {
	    if (d->vektor[i] != d_old->vektor[i]) {
		new_str = TRUE ;
		break ;
	    }
	}
	if (new_str) {
	    printf("New strategy (%.5e): ", tt_old) ;
  	    for (i = 0; i < d->no_of_entries; i++) {
		printf("%i ", d->vektor[i]) ;
		d_old->vektor[i] = d->vektor[i] ;
	    }
	    printf("\n") ;
	}
#endif
    } /* while tt < ti */
    /* Compute the final results */
    *gl = mat_double_vektor_inner_product(pv, gtl) ;
    *gu = mat_double_vektor_inner_product(pv, gtu) ;
#if TRC_VECTOR
    printf("g_min: ") ;
    for (i = 0; i < gtl->no_of_entries; i++)
	printf("%.5e ", gtl->vektor[i]) ;
    printf("\n") ;
    printf("g_max: ") ;
    for (i = 0; i < gtu->no_of_entries; i++)
	printf("%.5e ", gtu->vektor[i]) ;
    printf("\n") ;
#endif
    /* Deallocate some memory */
    for (i = 0; i <= kmax; i++) {
      mat_double_vektor_free(vv[i]) ;
      mat_double_vektor_free(vvu[i]) ;
    }
    efree(vv) ;
    efree(vvu) ;
    efree(vdiff) ;
    mat_int_vektor_free(d) ;
#if TRC_STRATEGY
    mat_int_vektor_free(d_old) ;
#endif
    for (i = 0; i < mdpm->ord; i++)
	efree(opt[i]) ;
    efree(opt) ;
    *epsilon = eps ;
    return(steps) ;
} /* mdp_compute_max_reach */

/**********************
mdp_compute_disc_result ##e
Datum:  27.06.2014
Autor:  Peter Buchholz
Co Autor: Ingo Schulz
Input:
Output:
Side-Effects:
Description:
The function computes the result vector with discretization.
**********************/
int mdp_compute_disc_result (double *gl, double ti, double delta, Double_vektor *rv, 
			     Double_vektor *pv, Double_vektor *gv, Mdp_matrix *mdpm)
{
  int             i, ord, steps ;
  double          tt;
  Int_vektor     *d  ;
  Double_vektor *vv, *wv, *v_help;
#if TRC_STRATEGY
  short           new_str = FALSE ;
  double          tt_old  = 0.0   ;
  Int_vektor     *d_old ;
  
  d_old = mat_int_vektor_new(rvl->no_of_entries) ;
  for (i = 0; i < rvl->no_of_entries; i++)
    d_old->vektor[i] = -1 ;
#endif
  steps = 0.0 ;
  /* Set delta such that ti / delta becomes an integer */
  steps = (int) (ti / delta) ; 
  printf("Set delta from %.3e ", delta) ;
  delta = ti / (double) steps ;
  printf("to %.3e\n", delta) ;
  ord = pv->no_of_entries ;
  /* Vectors vv and wv have to be initialized  */
  vv = mat_double_vektor_new(ord) ;
  wv = mat_double_vektor_new(ord) ;
  
  /* Vector for the optimal strategy */
  d = mat_int_vektor_new(ord) ;
  tt = 0.0 ;
  /* initialize vv with gv the vector of final weights */
  mat_double_vektor_copy(gv, vv) ;
  while (tt < ti) {
    mdp_find_opt_disc_strategy (d, mdpm, vv, wv, rv, delta) ;
    /* exchange vv and wv */
    v_help = wv ;
    wv = vv ;
    vv = v_help ;
#if TRC_STRATEGY
    tt_old = tt ;
#endif
    tt += delta ;
    steps++ ;
#if TRC_STRATEGY
    new_str = FALSE ;
    for (i = 0; i < d->no_of_entries; i++) {
      if (d->vektor[i] != d_old->vektor[i]) {
	new_str = TRUE ;
	break ;
      }
    }
    if (new_str) {
      printf("New strategy (%.5e): ", tt_old) ;
      for (i = 0; i < d->no_of_entries; i++) {
	printf("%i ", d->vektor[i]) ;
	d_old->vektor[i] = d->vektor[i] ;
      }
      printf("\n") ;
    }
#endif
  } /* while tt < ti */
  /* Compute the final results */
  *gl = 0.0 ;
  for (i = 0; i < ord; i++) {
    gv->vektor[i] = vv->vektor[i] ;
    *gl += pv->vektor[i] * vv->vektor[i] ;
  }
#if TRC_VECTOR
  printf("gv: ") ;
  for (i = 0; i < ord; i++)
    printf("%.5e ", vv->vektor[i]) ;
  printf("\n") ;
#endif
  mat_double_vektor_free(vv)  ;
  mat_double_vektor_free(wv)  ;
  mat_int_vektor_free(d) ;
#if TRC_STRATEGY
  mat_int_vektor_free(d_old) ;
#endif
  return(steps) ;
} /* mdp_compute_disc_result */


/**********************
mdp_matrix_prt ##e
Datum:  27.06.2014
Autor:  Peter Buchholz

Input:
Output:
Side-Effects:
Description:
The function prints a MDP matrix
**********************/
void mdp_matrix_prt (Mdp_matrix *mdpm)
{
  int i, j, k, l ;

  printf("MPD-Matrix with %lu states\n", mdpm->ord) ;
  for (i = 0; i < mdpm->ord; i++) {
    printf("Row %i with %lu entries\n", i, mdpm->rows[i]->pord) ;
    for (k = 0; k < mdpm->rows[i]->pord; k++) {
      j = 0 ;
      for (l = 0;  l < mdpm->ord; l++) {
	if (l == i)
	    printf("%.5e ", mdpm->rows[i]->diagonals[k]) ;
	else {
	  if (j < mdpm->rows[i]->no_of_entries[k] && l == mdpm->rows[i]->colind[k][j]) {
	      printf("%.5e ", mdpm->rows[i]->values[k][j]) ;
	      j++ ;
	    }
	    else {
	      printf("0.000e+00 ") ;
	    }
	}
      }
      printf("\n") ;
    }
  } /* for i = 0...ord-1 */
} /* mdp_matrix_prt */

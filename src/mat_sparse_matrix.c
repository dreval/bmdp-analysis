/*
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c fuer die angegebenen Funktionen
c (C) Copyright 1995, 2009  by Peter Buchholz
c (C) Copyright 1993, 94 by Peter Kemper
c
c     All Rights Reserved
c
c mat_sparse_matrix.c
c
c Aufgabe ist Funktionen zur Erzeugung und Behandlung von 
c spaerlich besetzen Matrizen mit Eintraegen vom Typ double
c 
c
c	Datum		Modifikationen
c	----------------------------------------------------------
c 1.8.97 Datei angelegt, Funktionen aus matrix.c entnommen
c 06.02.98 Einfuegen der Funktionen mat_sparse_matrix_to_p, mat_sparse_matrix_normalize
c        und mat_sparse_matrix_mult_vektor
c 08.05.98 Einfuegen der Funktion mat_sparse_matrix_mult_scalar
c 02.06.98 Einfuegen der Funktion mat_sparse_matrix_add_mult_matrices
c 08.07.98 mat_sparse_matrix_sort_reduce_elems korrigiert
c 11.08.98 Kommentare werden mit der Funktion read_comment ueberlesen
c 31.08.98 Funktion mat_sparse_matrix_matrix_vektor_product eingefuegt
c 23.10.98 Funktion str_extract_matrix aus str_helpf.c als mat_sparse_matrix_extract
c          uebernommen
c 03.11.98 Nullmatrizen werden in mat_sparse_matrix_to_p behandelt.
c 05.11.98 mat_sparse_matrix_mult_vektor wird mit zusaetlichem Paramter aufgerufen,
c          damit die Multiplikation mit den Diagonalelementen aus- oder angeschaltet
c          werden kann
c 14.02.99 Beruecksichtigen der neuen Datenstruktur mit Arrays l, u, d und
c          Einfuegen der Funktion mat_set_lud
c 05.11.99 Fehler aus mat_set_lud beseitigt
c 24.11.99 Fehler aus mat_set_lud beseitigt
c 14.11.01 Funktionen mat_diagonal_vecto_mult und mat_sparse_matrix_vektor_mult   eingef�gt
c 22.02.02 Funktion mat_sparse_matrix__add_matrices und mat_add_mult_matrices ge�ndert
c 01.04.02 Funktion mat_sparse_matrix_add_mult_matrices ge�ndert
c 15.11.02 Funktionen mat_sparse_matrix_compute_bandwidth neu implementiert
c          und Funktion mat_sparse_matrix_permute_matrices  aus matorder.c �bernommen.
c 15.11.02 mat_sparse_matrix_prt_structure implementiert 
c 10.01.03 mat_sparse_matrix_norm und mat_sparse_matrix_overwrite implementiert
c 16.01.03 mat_sparse_matrix_eliminate_small_values implementiert
c 23.01.03 in mat_sparse_matrix_eliminate_small_values Vergleich auf > 0.0 durch != 0.0 ersetzt
c 07.02.03 mat_transform_matrix_to_vector eingef�gt
c 09.02.03 mat_transform_vectors_to_matrix
c 12.04.03 Fehler in mat_set_lud beim Setzen der Diagonalelemente beseitigt
c 21.05.03 Aenderung set_lud und insert_diagonal
c 11.03.09 Funktion mat_sparse_transposed_matrix_vektor_mult eingef�hrt
c 26.08.09 Funktion stg_expand_matrix von str_genmat �bernommen und in mat_expand_matrix umbenannt
c 12.02.10 Funktion mat_sparse_matrix_row_sum eingefuegt
c 15.02.10 Funktion mat_sparse_matrix_add_eps eingefuegt
c 25.02.10 Funktion mat_sparse_matrix_tensor-product eingef�hrt
c 01.03.10 Funktion mat_prt_small_matrix_without_diagonals eingef�gt.
c 25.03.10 Funktion mat_sparse_matrix_add_elem eingef�gt
c 17.06.10 Funktion mat_sparse_matrix_transpose_matrix_vektor_product eingef�gt
c 22.11.10 Funktion mat_sparse_matrix_add_identity hinzugefuegt
c 09.03.11 Funktion mat_sparse_matrix_output_with_comments hinzugefuegt
c 27.08.12 Funtkion mat_sparse_matrix_identical hinzugefuegt.
c 01.12.12 Fehler in mat_sparse_matrix_transform_stochastic korrigiert
c 28.08.13 Funktion mat_sparse_matrix_diagmax_diagmin_bandwidth, mat_sparse_matrix_update_index
C             mat_sparse_matrix_space mat_sparse_matrix_set_n_of_nonzeros 
c             aus sgspn2nsolve �bernommen
c 01.03.15 Neue Funktion mat_sparse_matrix_is_smaller
c 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
*/


#include <math.h>
#include <assert.h>
#include "list.h"
#include "global.h" 
#include "ememory.h"
#include "mat_double_vektor.h"
#include "mat_row.h"
#include "mat_sparse_matrix.h"

/**********************
mat_sparse_matrix_new ##e 
Datum:  14.2.95             (Aenderung 27.3.95 PB)
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
Sparse_matrix *mat_sparse_matrix_new(unsigned anzahl)
#else
Sparse_matrix *mat_sparse_matrix_new(anzahl)
     unsigned anzahl ;
#endif
{
  Sparse_matrix *v = (Sparse_matrix *)ecalloc(1,sizeof(Sparse_matrix)) ;
  
  if (0 < anzahl) 
  {
    v->ord = anzahl ;
    v->diagonals = (double *)ecalloc(anzahl,sizeof(double)) ;
    v->rowind = (Row *)ecalloc(anzahl,sizeof(Row)) ;
    v->l = NULL ;
    v->u = NULL ;
    v->d = NULL ;
  }
  return(v) ;
}
   
/**
 * Achtung: assignes q->n_of_nonzeros the appropriate value
 * \date 4.10.99
 * \author Peter Kemper
 * \param q Zeiger auf Sparse-Matrix
 * \return ???
 */
int mat_sparse_matrix_set_n_of_nonzeros (Sparse_matrix *q) 
{
  return(q->n_of_nonzeros = mat_sparse_matrix_non_zeros (q)) ;
}
 
/**********************
mat_sparse_matrix_set_diagonals ##e 
Datum:  14.2.95             (Aenderung 27.3.95 PB)
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
**********************/
#ifndef CC_COMP
int mat_sparse_matrix_set_diagonals (Sparse_matrix *q, int negative)
#else
int mat_sparse_matrix_set_diagonals (q, negative)
  Sparse_matrix *q ;
  int            negative ; /* == 0 positive Zeilensumme sonst negative */
#endif
{
  int r, c;

  if (q->diagonals == NULL)
    q->diagonals = (double *) ecalloc(q->ord, sizeof(double)) ;
  for (r = 0; r < q->ord; r++)
  {
    q->diagonals[r] = 0.0 ;
    if (negative)
    {
      for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
        q->diagonals[r] -= (q->rowind[r]).val[c] ;
    }
    else
    {
      for (c = 0; c < (q->rowind[r]).no_of_entries; c++)
        q->diagonals[r] += (q->rowind[r]).val[c] ;

    }
  } 
  return(NO_ERROR) ;
}

/**********************
mat_sparse_non_zeros  
Datum:  9.7.95 Aenderung 08.05.98
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Die Funktion zaehlt die Anzahl der Nichtnullelemente in den Zeilen der Matrix q.
Die Elemente der Diagonalen werden nicht mitgezaehlt! Das Ergebnis wird als
Funktionwert zurueckgegeben und in die Variable n_of_nonzeros geschrieben.
**********************/
#ifndef CC_COMP
int mat_sparse_matrix_non_zeros (Sparse_matrix *q) 
#else
int mat_sparse_matrix_non_zeros (q) 
  Sparse_matrix *q ;
#endif
{
  int nz = 0, i;
  
  for (i = 0; i < q->ord; i++)
  {
 /*   if (q->diagonals[i] != 0.0)
      nz++ ; */
    nz += (q->rowind[i]).no_of_entries ;
  }
  q->n_of_nonzeros = nz ;
  return(nz) ;
}
   
/**********************
mat_sparse_matrix_free ##e 
Datum:  14.2.95      (Aenderung 27.3.95 PB)
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:
Aenderung 14.02.99 Freigeben von l, u, d falls noetig.
**********************/
#ifndef CC_COMP
int mat_sparse_matrix_free(Sparse_matrix *v)
#else
int mat_sparse_matrix_free(v)
     Sparse_matrix *v ;
#endif
{
  unsigned i ;
  
  if (!v)
    return(NO_ERROR) ;
  
  for (i=0 ;i<v->ord ;i++ )
  {
    if ((v->rowind[i]).colind != NULL)
      efree((v->rowind[i]).colind) ;
    if ((v->rowind[i]).val != NULL)
      efree((v->rowind[i]).val) ;
  }
  efree(v->diagonals) ;
  efree(v->rowind) ;
  if (v->l != NULL)
    efree(v->l) ;
  if (v->u != NULL)
    efree(v->u) ;
  if (v->d != NULL)
    efree(v->d) ;

  efree(v) ; 
  return(NO_ERROR); 
}
   
/**********************
mat_sparse_matrix_output ##e 
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_sparse_matrix_output(FILE *fp, Sparse_matrix *v, short write_diag)
#else
int mat_sparse_matrix_output(fp, v, write_diag)
     FILE *fp ;
     Sparse_matrix *v ;
     short write_diag ;
#endif
{
  unsigned i ;
  
  if (!fp || !v)
    return(OUTPUT_ERROR) ;

  for (i=0 ;i < v->ord ; i++) 
  {
    fprintf (fp, "%lu", (v->rowind[i]).no_of_entries) ;
    if (write_diag == TRUE)
      fprintf (fp, " %.15e\n", v->diagonals[i]) ; 
    else
      fprintf(fp,"\n") ; 
    mat_row_output(fp, &(v->rowind[i])) ;    
  }
  return(NO_ERROR) ;  
}

/**********************
mat_sparse_matrix_output_with_comments 
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_sparse_matrix_output_with_comments(FILE *fp, Sparse_matrix *v, short write_diag)
#else
int mat_sparse_matrix_output_with_comments(fp, v, write_diag)
     FILE *fp ;
     Sparse_matrix *v ;
     short write_diag ;
#endif
{
  unsigned i ;
  
  if (!fp || !v)
    return(OUTPUT_ERROR) ;

  for (i=0 ;i < v->ord ; i++) 
  {
    fprintf(fp, "# Row %i number of nonzeros\n", i) ;
    fprintf (fp, "%lu", (v->rowind[i]).no_of_entries) ;
    if (write_diag == TRUE)
      fprintf (fp, " %.15e\n", v->diagonals[i]) ; 
    else
      fprintf(fp,"\n") ; 
    mat_row_output(fp, &(v->rowind[i])) ;    
  }
  return(NO_ERROR) ;  
}

/**********************
mat_sparse_matrix_input ##e 
Datum:  4.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_sparse_matrix_input(FILE *fp, Sparse_matrix *v, short read_diag)
#else
int mat_sparse_matrix_input(fp, v, read_diag)
     FILE *fp ;
     Sparse_matrix *v ;
     short read_diag ;
#endif
{
  unsigned  i, j=0 ;

  if (!fp || !v)
    return(INPUT_ERROR) ;
  for (i=0 ;i < v->ord ; i++) 
  {
    fp = read_comment(fp) ;
    if (fscanf(fp, "%u", &j) == EOF)
      return(INPUT_ERROR) ;
    fp = read_comment(fp) ;
    if (read_diag == TRUE && fscanf(fp, "%le", &(v->diagonals[i])) == EOF)
      return(INPUT_ERROR) ;
    v->rowind[i].no_of_entries = j ;
    v->rowind[i].colind = (size_t *) ecalloc (j, sizeof(size_t)) ;
    v->rowind[i].val = (double *) ecalloc (j, sizeof(double)) ;
    for (j=0 ; j < v->rowind[i].no_of_entries ; j++) 
    {
      fp = read_comment(fp) ;
      if (fscanf(fp,"%lu %le", 
          &v->rowind[i].colind[j], &v->rowind[i].val[j]) == EOF)
        return(INPUT_ERROR) ;
    }    
  }  
  return(NO_ERROR) ;  
}

/**********************
mat_sparse_matrix_copy ##e 
Datum:  1.9.95 Aenderung 08.05.98 (kopieren aler Variablenwerte)
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Aenderung 14.02.99 Beruecksichtigen von l, u, d.
**********************/

#ifndef CC_COMP
Sparse_matrix *mat_sparse_matrix_copy (Sparse_matrix *q) 
#else
Sparse_matrix *mat_sparse_matrix_copy (q) 
  Sparse_matrix *q ;
#endif
{
  Sparse_matrix *qc   ;
  int            i, j ;

  qc = mat_sparse_matrix_new(q->ord) ;
  qc->n_of_nonzeros = q->n_of_nonzeros ;
  qc->bandwidth     = q->bandwidth     ;
  qc->diagmax       = q->diagmax       ;
  qc->diagmin       = q->diagmin       ;
  qc->active        = q->active        ;
  for (i = 0; i < q->ord; i++)
  {
    qc->diagonals[i] = q->diagonals[i] ;
    (qc->rowind[i]).no_of_entries = (q->rowind[i]).no_of_entries ;
    (qc->rowind[i]).colind = 
      (size_t *) ecalloc ((qc->rowind[i]).no_of_entries, sizeof(size_t)) ;
    (qc->rowind[i]).val = 
      (double *) ecalloc ((qc->rowind[i]).no_of_entries, sizeof(double)) ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    {
      (qc->rowind[i]).colind[j] = (q->rowind[i]).colind[j] ;
      (qc->rowind[i]).val[j]    = (q->rowind[i]).val[j]    ;
    }
  }
  if (q->l != NULL)
  {
    qc->l = (int *) ecalloc(q->ord, sizeof(int)) ;
    for (i = 0; i < q->ord; i++)
      qc->l[i] = q->l[i] ;
  }
  if (q->u != NULL)
  {
    qc->u = (int *) ecalloc(q->ord, sizeof(int)) ;
    for (i = 0; i < q->ord; i++)
      qc->u[i] = q->u[i] ;
  }
  if (q->d != NULL)
  {
    qc->d = (int *) ecalloc(q->ord, sizeof(int)) ;
    for (i = 0; i < q->ord; i++)
      qc->d[i] = q->d[i] ;
  }
  return(qc) ;
}

/**********************
mat_sparse_set_lud ##e 
Datum:  14.02.99
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Setzt die Arrays l, u und d, um die obere und untere Dreiecksmatrix sowie die
Diagonalelemente in der Matrix zu identifizieren. l und u enthalten des letzte und
erste Element der Zeilen der unteren und oberen Dreiecksmatrix.
Falls eine Zeile der unteren Dreiecksmatrix leer ist, wird der zugehoerige Wert von l
mit 0 belegt, falls eine Zeile der oberen Dreiecksmatrix leer ist, so wird der 
Wert in u auf no_of_entries gesetzt. 
d enthaelt den Index eines evtl. Diagonalmatrix in der Matrix.
**********************/
#ifndef CC_COMP
void  mat_set_lud(Sparse_matrix *q) 
#else
void  mat_set_lud(q) 
  Sparse_matrix *q ;
#endif
{
  int i, j ;

  if (q->l != NULL)
    efree(q->l) ;
  if (q->u != NULL)
    efree(q->u) ;
  if (q->d != NULL)
    efree(q->d) ;
  q->l = (int *) ecalloc (q->ord, sizeof(int)) ;
  q->u = (int *) ecalloc (q->ord, sizeof(int)) ;
  q->d = (int *) ecalloc (q->ord, sizeof(int)) ;
  for (i = 0; i < q->ord; i++)
  {
    j = 0 ;
    while (j < (q->rowind[i]).no_of_entries && (q->rowind[i]).colind[j] < i)
      j++ ;
    q->l[i] = j ;
    if (j == (q->rowind[i]).no_of_entries)
    {
      q->d[i] = -1 ;
      q->u[i] = (q->rowind[i]).no_of_entries ;
    }
    else
    {
      if ((q->rowind[i]).colind[j] == i)
      {
	  /* j++ ; */
        q->d[i] = j ;
	j++ ; 
      }
      else
        q->d[i] = -1 ;
      q->u[i] = j ;
    }
  }
} /* mat_set_lud */

/**********************
mat_sparse_matrix_transpose ##e 
Datum:  9.8..95 Aenderung 08.05.98 (kopieren aller Variablen)
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:
Aenderung 14.2.99 PB Arrays l, u und d werden beruecksichtigt.
**********************/

#ifndef CC_COMP
Sparse_matrix *mat_sparse_matrix_transpose (Sparse_matrix *q, int col) 
#else
Sparse_matrix *mat_sparse_matrix_transpose (q, col) 
  Sparse_matrix *q   ;
  int            col ; 
#endif
{
  int            i, j, k  ;
  int            size = 0 ;
  int           *pos      ;
  Sparse_matrix *q_t      ;

  /* determine the size of the transposed matrix */
  for (i = 0; i < q->ord; i++)
  {
    if (q->diagonals[i] != 0.0 && i > size)
      size = i ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    if ((q->rowind[i]).colind[j] > size)
      size = (q->rowind[i]).colind[j] ; 
  }
  /* the size has to be at least col */
  if (size < col) 
    size = col ;
  if (size >= q->ord)
    pos = (int *) ecalloc(size+1, sizeof(int)) ;
  else
    pos = (int *) ecalloc(q->ord+1, sizeof(int)) ;

  for (i = 0; i < q->ord; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    pos[(q->rowind[i]).colind[j]]++ ;
  q_t = mat_sparse_matrix_new(size+1) ;
  for (i = 0; i < q_t->ord; i++)
  {
    (q_t->rowind[i]).val    = (double *) ecalloc (pos[i], sizeof(double)) ;
    (q_t->rowind[i]).colind = (size_t *) ecalloc (pos[i], sizeof(size_t)) ;
    (q_t->rowind[i]).no_of_entries = pos[i] ;
    pos[i] = 0 ;
  }
  for (i = 0; i < q->ord; i++) {
      if (i < q_t->ord)
	  q_t->diagonals[i] = q->diagonals[i] ;
      for (j = 0; j < (q->rowind[i]).no_of_entries; j++) {
	  k = (q->rowind[i]).colind[j] ;
	  (q_t->rowind[k]).colind[pos[k]] = i ;
	  (q_t->rowind[k]).val[pos[k]] = (q->rowind[i]).val[j] ;
	  pos[k]++ ;
      }
  }
/*
  for (i = 0; i < q_t->ord; i++)
  {
    if (i < q->ord)
      q_t->diagonals[i] = q->diagonals[i] ;
    k = 0 ;
    for (j = 0; j < q->ord; j++)
    {
      if (pos[j] < (q->rowind[j]).no_of_entries &&
          (q->rowind[j]).colind[pos[j]] == i      )
      {
        (q_t->rowind[i]).colind[k] = j ;
        (q_t->rowind[i]).val[k]    = (q->rowind[j]).val[pos[j]] ;
        pos[j]++ ;
        k++ ;
        if (k >= (q_t->rowind[i]).no_of_entries)
          break ;
      }
    }  
  }
*/
  q_t->n_of_nonzeros = q->n_of_nonzeros ;
  q_t->bandwidth     = q->bandwidth     ;
  q_t->diagmax       = q->diagmax       ;
  q_t->diagmin       = q->diagmin       ;
  q_t->active        = q->active        ;
  mat_set_lud(q_t) ;
  efree(pos)  ;
  return(q_t) ;
}

/**********************
mat_sparse_matrix_max_elem ##e berechnet das betragsmaessig maximale Element einer Matrix
Datum:  5.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
double mat_sparse_matrix_max_elem(Sparse_matrix *q)
#else
double mat_sparse_matrix_max_elem(q)
 Sparse_matrix *q ;
#endif
{
  double max = 0.0 ;
  int i, j ;

  for (i = 0; i < q->ord; i++)
  if (fabs(q->diagonals[i]) > max)
    max = fabs(q->diagonals[i]) ;
  for (i = 0; i < q->ord; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
   if (fabs((q->rowind[i]).val[j]) > max)
    max = fabs((q->rowind[i]).val[j]) ;
  return(max) ;
}


 

/**********************
mat_sparse_matrix_insert_diagonals
Datum:  8.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Ordnet die Diagonalelemente direkt in die Zeilen ein
**********************/

#ifndef CC_COMP
int mat_sparse_matrix_insert_diagonals (Sparse_matrix *q) 
#else
int mat_sparse_matrix_insert_diagonals (q) 
  Sparse_matrix *q ;
#endif
{
  int i, j      ;
  int diag      ;
  size_t *col   ;
  double *val   ;
  
  for (i = 0; i < q->ord; i++)
  {
    if (q->diagonals[i] != 0.0)
    {
      diag = -1 ;
      j    =  0 ;
      while ((diag < 0) && j < (q->rowind[i]).no_of_entries)
      {
        if ((q->rowind[i]).colind[j] >= i)
          diag = j ;
        else
          j++ ;
      }
      if (j == (q->rowind[i]).no_of_entries)
        diag = (q->rowind[i]).no_of_entries ;
      if ((diag == (q->rowind[i]).no_of_entries) ||
          ((q->rowind[i]).colind[diag] > i)        )
      {
	val = (double *) ecalloc ((q->rowind[i]).no_of_entries + 1, sizeof(double)) ;
        col = (size_t *) ecalloc ((q->rowind[i]).no_of_entries + 1, sizeof(size_t)) ;
        j = 0 ;
        while (j < diag)
        {
          col[j] = (q->rowind[i]).colind[j] ;
          val[j] = (q->rowind[i]).val[j]    ;
          j++ ;
        }
        col[diag] = i ;
        val[diag] = q->diagonals[i] ;
        while (j < (q->rowind[i]).no_of_entries)
        {
          col[j+1] = (q->rowind[i]).colind[j] ;
          val[j+1] = (q->rowind[i]).val[j]    ;
          j++ ;
        }
	efree((q->rowind[i]).colind) ;
	efree((q->rowind[i]).val) ;
	(q->rowind[i]).no_of_entries += 1 ;
	(q->rowind[i]).colind = col ;
	(q->rowind[i]).val = val ;
      }
      else
        (q->rowind[i]).val[diag] += q->diagonals[i] ;
      q->diagonals[i] = 0.0 ;
    }
  }
  return(NO_ERROR) ;
}
   

/**********************
mat_extract_diagonals
Datum:  8.5.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Entfernt Diagonalelemente aus einer Zeile und speichert 
             sie separat. Evtl. vorher vorhandene Diagonalelemente werden 
	     �berschrieben.
**********************/
#ifndef CC_COMP
int mat_sparse_matrix_extract_diagonals (Sparse_matrix *q)
#else
int mat_sparse_matrix_extract_diagonals (q)
  Sparse_matrix *q ;
#endif
{
  int i, j      ;
  int diag      ;
  Row *help_row ;
  
  for (i = 0; i < q->ord; i++)
  {
    diag = -1 ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    if ((q->rowind[i]).colind[j] == i)
    {
      diag = j ;
      break ;
    }
    if (diag >= 0)
    {
      help_row = mat_row_new((q->rowind[i]).no_of_entries - 1) ;
      q->diagonals[i] = (q->rowind[i]).val[j] ;
      for (j = 0; j < diag; j++)
      {
        help_row->colind[j] = (q->rowind[i]).colind[j] ;
        help_row->val[j]    = (q->rowind[i]).val[j]    ;
      }
      for (j = diag + 1; j < (q->rowind[i]).no_of_entries; j++)
      {
        help_row->colind[j-1] = (q->rowind[i]).colind[j] ;
        help_row->val[j-1]    = (q->rowind[i]).val[j]    ;
      }
/* Change 13.5.96 Kemper, original code:
      mat_row_free(q->rowind[i]) ;
      q->rowind[i] = *help_row   ;
   The idea is to replace the old row by the newly created help_row:
*/
      efree((q->rowind[i]).colind) ;
      efree((q->rowind[i]).val) ;
      (q->rowind[i]).no_of_entries = help_row->no_of_entries ;
      (q->rowind[i]).colind = help_row->colind ;
      (q->rowind[i]).val = help_row->val ;
      efree(help_row) ;
/* end of change */
    }
    else
      q->diagonals[i] = 0.0 ;
  }
  return(NO_ERROR) ;
} /* mat_sparse_matrix_extract_diagonals */ 


/**********************
mat_sparse_matrix_max_diagonal ##e
Datum:  20.6.95
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Compute the absolute value of the maximum diagonal element
**********************/

#ifndef CC_COMP
double mat_sparse_matrix_max_diagonal(Sparse_matrix *q)
#else
double mat_sparse_matrix_max_diagonal(q)
  Sparse_matrix *q ;
#endif
{
  double max = 0.0 ;
  int    i ;

  for (i = 0; i < q->ord; i++)
  if (fabs(q->diagonals[i]) > max)
    max = fabs(q->diagonals[i]) ;
  return(max) ;
}

/**********************
mat_sparse_matrix_reset ##e
Datum:  13.02.97
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: Set all elements to zero
**********************/
#ifndef CC_COMP
int mat_sparse_matrix_reset(Sparse_matrix *q)
#else
int mat_sparse_matrix_reset(q)
  Sparse_matrix *q ;
#endif
{
  int i, j ;

  for (i = 0; i < q->ord; i++)
  {
    q->diagonals[i] = 0.0 ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
      (q->rowind[i]).val[j] = 0.0 ;
  }
  return(NO_ERROR) ;
}

/**********************
mat_sparse_matrix_check_zero
Datum:  13.02.97
Autor:  Peter Buchholz
Input:
Output:
Side-Effects:
Description: The procedure yields FALS if one value is zero and
TRUE otherwise
**********************/
#ifndef CC_COMP
int mat_sparse_matrix_check_zero(Sparse_matrix *q)
#else
int mat_sparse_matrix_check_zero(q)
  Sparse_matrix *q ;
#endif
{
  int i, j ;

  for (i = 0; i < q->ord; i++)
  {
    if (q->diagonals[i] == 0.0)
      return(FALSE) ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    if  ((q->rowind[i]).val[j] == 0.0)
      return(FALSE) ;
  }
  return(TRUE) ;
}

/**********************
mat_sparse_matrix_add_matrices
Datum:  22.01.97 (�ndeurng 20.2.02 q2 wird nicht mehr ver�ndert!)
Autor:  Peter Buchholz
Input:  q1, q2 Eingabematrizen gleicher Dimension!
Output: q1 Summe der beiden Eingabematrizen
Side-Effects:
Description:
Die Prozedur addiert Matrix q2 zu q1. Die Diagonalelemente werden
addiert
Die Arrays l, u und d werden dabei nicht veraendert!
***********************************************************************/
#ifndef CC_COMP
int mat_sparse_matrix_add_matrices(Sparse_matrix *q1, Sparse_matrix *q2)
#else
int mat_sparse_matrix_add_matrices(q1, q2)
  Sparse_matrix *q1 ;
  Sparse_matrix *q2 ;
#endif
{
  int     i, j, k, l, max_ind ;
  double *v          ;

  if (q1->ord != q2->ord)
  {
    printf("Fatal error try to add matrices with different dimensions\n") ;
    exit(-1) ;
  }
  max_ind = 0 ;
  for (i = 0; i < q1->ord; i++) {
    for (j = 0; j < (q1->rowind[i]).no_of_entries; j++)
      if ((q1->rowind[i]).colind[j] > max_ind)
	max_ind = (q1->rowind[i]).colind[j] ;
    for (j = 0; j < (q2->rowind[i]).no_of_entries; j++)
      if ((q2->rowind[i]).colind[j] > max_ind)
	max_ind = (q2->rowind[i]).colind[j] ;
  }
  v = (double *) ecalloc (max_ind + 1, sizeof(double)) ;
  for (i = 0; i < q1->ord; i++)
  {
    q1->diagonals[i] += q2->diagonals[i] ;
    if ((q1->rowind[i]).no_of_entries == 0 && 
        (q2->rowind[i]).no_of_entries >  0   )
    {
      (q1->rowind[i]).no_of_entries = (q2->rowind[i]).no_of_entries ;
      (q1->rowind[i]).colind        = (size_t *) ecalloc ((q2->rowind[i]).no_of_entries, sizeof(size_t))       ;
      (q1->rowind[i]).val           = (double *) ecalloc ((q2->rowind[i]).no_of_entries, sizeof(double)) ;
      for (j = 0; j < (q2->rowind[i]).no_of_entries; j++) {
	(q1->rowind[i]).colind[j] =(q2->rowind[i]).colind[j] ;
	(q1->rowind[i]).val[j]    =(q2->rowind[i]).val[j]    ;
      }
    }
    else {
      if ((q1->rowind[i]).no_of_entries > 0 && 
	  (q2->rowind[i]).no_of_entries > 0    ) {
	  for (j = 0; j < (q1->rowind[i]).no_of_entries; j++)
	    v[(q1->rowind[i]).colind[j]] = (q1->rowind[i]).val[j] ;
	  for (j = 0; j < (q2->rowind[i]).no_of_entries; j++)
	    v[(q2->rowind[i]).colind[j]] += (q2->rowind[i]).val[j] ;
	  l = 0 ;
	  for (k = 0; k <= max_ind; k++)
	    if (v[k] != 0.0)
	      l++ ;
	  efree ((q1->rowind[i]).colind) ;
	  (q1->rowind[i]).colind = (size_t *) ecalloc(l, sizeof(size_t)) ;
	  efree ((q1->rowind[i]).val) ;
	  (q1->rowind[i]).val = (double *) ecalloc(l, sizeof(double)) ;
	  (q1->rowind[i]).no_of_entries = l ;
	  l = 0 ;
	  for (k = 0; k <= max_ind; k++) {
	    if (v[k] != 0.0) {
	      (q1->rowind[i]).colind[l] = k ;
	      (q1->rowind[i]).val[l] = v[k] ;
	      l++ ;
	      v[k] = 0.0 ;
	    }
	  }
	}
    }
  }
  efree(v) ;
  return(NO_ERROR) ;
} /* mat_sparse_matrix_add_matrices */

/**********************
mat_sparse_matrix_add_mult_matrices
Datum:  22.01.97 (�ndeurng 20.2.02 q2 wird nicht mehr ver�ndert!)
Autor:  Peter Buchholz
Input:  q1, q2 Eingabematrizen gleicher Dimension!
Output: q1 Summe der beiden Eingabematrizen
Side-Effects:
Description:
Die Prozedur berechnet q1 = q1 + s*q2, wobei q1, q2 Matrizen sind und s ein Skalar ist.
Die Diagonalelemente werdne ebenfalls addiert.
Die Arrays l, u und d werden dabei nicht veraendert!
***********************************************************************/
#ifndef CC_COMP
int mat_sparse_matrix_add_mult_matrices(Sparse_matrix *q1, Sparse_matrix *q2, double s)
#else
int mat_sparse_matrix_add_mult_matrices(q1, q2, s)
  Sparse_matrix *q1 ;
  Sparse_matrix *q2 ;
  double         s  ;
#endif
{
  int     i, j, k, l, max_ind ;
  double *v          ;

  if (q1->ord != q2->ord)
  {
    printf("Fatal error try to add matrices with different dimensions\n") ;
    exit(-1) ;
  }
  max_ind = 0 ;
  for (i = 0; i < q1->ord; i++) {
    for (j = 0; j < (q1->rowind[i]).no_of_entries; j++)
      if ((q1->rowind[i]).colind[j] > max_ind)
	max_ind = (q1->rowind[i]).colind[j] ;
    for (j = 0; j < (q2->rowind[i]).no_of_entries; j++)
      if ((q2->rowind[i]).colind[j] > max_ind)
	max_ind = (q2->rowind[i]).colind[j] ;
  }
  v = (double *) ecalloc (max_ind + 1, sizeof(double)) ;
  for (i = 0; i < q1->ord; i++)
  {
    q1->diagonals[i] += s * q2->diagonals[i] ;
    if ((q1->rowind[i]).no_of_entries == 0 && 
        (q2->rowind[i]).no_of_entries >  0   )
    {
      (q1->rowind[i]).no_of_entries = (q2->rowind[i]).no_of_entries ;
      (q1->rowind[i]).colind        = (size_t *) ecalloc ((q2->rowind[i]).no_of_entries, sizeof(size_t)) ;
      (q1->rowind[i]).val           = (double *) ecalloc ((q2->rowind[i]).no_of_entries, sizeof(double)) ;
      for (j = 0; j < (q2->rowind[i]).no_of_entries; j++) {
	(q1->rowind[i]).colind[j] =(q2->rowind[i]).colind[j] ;
	(q1->rowind[i]).val[j]    = s * (q2->rowind[i]).val[j]    ;
      }
    }
    else {
      if ((q1->rowind[i]).no_of_entries > 0 && 
	  (q2->rowind[i]).no_of_entries > 0    ) {
	  for (j = 0; j < (q1->rowind[i]).no_of_entries; j++)
	    v[(q1->rowind[i]).colind[j]] = (q1->rowind[i]).val[j] ;
	  for (j = 0; j < (q2->rowind[i]).no_of_entries; j++)
	    v[(q2->rowind[i]).colind[j]] += s * (q2->rowind[i]).val[j] ;
	  l = 0 ;
	  for (k = 0; k <= max_ind; k++)
	    if (v[k] != 0.0)
	      l++ ;
	  efree ((q1->rowind[i]).colind) ;
	  (q1->rowind[i]).colind = (size_t *) ecalloc(l, sizeof(size_t));
	  efree ((q1->rowind[i]).val) ;
	  (q1->rowind[i]).val = (double *) ecalloc(l, sizeof(double)) ;
	  (q1->rowind[i]).no_of_entries = l ;
	  l = 0 ;
	  for (k = 0; k <= max_ind; k++) {
	    if (v[k] != 0.0) {
	      (q1->rowind[i]).colind[l] = k ;
	      (q1->rowind[i]).val[l] = v[k] ;
	      l++ ;
	      v[k] = 0.0 ;
	    }
	  }
	}
    }
  }
  efree(v) ;
  return(NO_ERROR) ;
} /* mat_sparse_matrix_add_mult_matrices */


/**********************
mat_sparse_matrix_mult
Datum:  01.07.97
Autor:  Peter Buchholz
Input: q1 first matrix
       qi second matrix
       trans TRUE for q1*qi FALSE for q1*(qi)^T
Output:
Side-Effects:
Description: The procedure multiplies two sparse_matrices, the
result is the product. Diagonal elements have to be inserted in the rows.
See function for insertion and extraction of diagonal elements.
If trans > 0, then the second matrix is transposed, otherwise it is assumed that the
matrix is already transposed.
Die Arrays l, u und d werden dabei nicht veraendert!
**********************/
#ifndef CC_COMP
Sparse_matrix *mat_sparse_matrix_mult(Sparse_matrix *q1, Sparse_matrix *qi, int trans)
#else
Sparse_matrix *mat_sparse_matrix_mult(q1, qi, trans)
  Sparse_matrix *q1     ;
  Sparse_matrix *qi     ;
  int             trans ;
#endif
{
  Sparse_matrix *qr, *q2 ;
  double *values ;
  int i1, i2, j1, j2, k ;

  if (trans > 0)
    q2 = mat_sparse_matrix_transpose(qi, 1) ;
  else
    q2 = qi ;

  values = (double *) ecalloc (q2->ord, sizeof(double)) ;
  qr = mat_sparse_matrix_new(q1->ord) ;
  for (i1 = 0; i1 < q1->ord; i1++)
  {
    k  = 0 ;
    j1 = 0 ;
    for (i2 = 0; i2 < q2->ord; i2++)
    {
      j1 = 0 ;
      j2 = 0 ;
      while(j1 < (q1->rowind[i1]).no_of_entries &&
            j2 < (q2->rowind[i2]).no_of_entries    )
      {
        if ((q1->rowind[i1]).colind[j1] == (q2->rowind[i2]).colind[j2])
        {
          values[i2] += 
           (q1->rowind[i1]).val[j1++] * (q2->rowind[i2]).val[j2++] ;
        }
        else
        {
          if ((q1->rowind[i1]).colind[j1] < (q2->rowind[i2]).colind[j2])
            j1++ ;
          else
            j2++ ;
        }
      }
      if (values[i2] != 0.0)
        k++ ;
    }
    (qr->rowind[i1]).no_of_entries = k ;
    (qr->rowind[i1]).colind = (size_t *) ecalloc (k, sizeof(size_t)) ;
    (qr->rowind[i1]).val = (double *) ecalloc (k, sizeof(double)) ;
    k = 0 ;
    for (j2 = 0; j2 < q2->ord; j2++)
    if (values[j2] != 0.0)
    {
      (qr->rowind[i1]).colind[k] = j2 ;
      (qr->rowind[i1]).val[k++] = values[j2] ;
      values[j2] = 0.0 ;
    }
  } 
#if SHOW_TEST2 
  printf("Multiply matrix q1\n") ;
  mat_sparse_matrix_output(stdout, q1, TRUE) ;
  printf("and matrix q2^T\n") ;
  mat_sparse_matrix_output(stdout, q2, TRUE) ;
  printf("giving result\n") ;
  mat_sparse_matrix_output(stdout, qr, TRUE) ;
#endif 
  efree(values) ;
  if (trans > 0)
    mat_sparse_matrix_free(q2) ;
  return(qr) ;
}


/**********************
mat_sparse_matrix_sort_reduce_elems    
Datum:  29.3.95
Autor:  Peter Buchholz
Input:  q Sparse_matrix
Output: q Sparse_matrix mit zeilenweise sortierten Elementen
Side-Effects:
Description:
Die Funktion sortiert die Elemente in den Zeilen einer Sparse_matrix nach
ihren Spaltenindizes und addiert Raten von Transitionen mit
gleichem Spaltenidenx
Die Arrays l, u und d werden dabei nicht veraendert!
**********************/
#ifndef CC_COMP
int mat_sparse_matrix_sort_reduce_elems(Sparse_matrix *q) 
#else
int mat_sparse_matrix_sort_reduce_elems(q) 
  Sparse_matrix *q ;
#endif
{
  Double_vektor *help_row ;
  int i, j, k, last, sort, max, leng ;

  help_row = mat_double_vektor_new(q->ord) ;
  max = q->ord ;
  leng = max   ;
  for (i = 0; i < q->ord; i++)
  {
    sort = FALSE ;
    last = -1    ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    {
      if (last >= (q->rowind[i]).colind[j] || (q->rowind[i]).colind[j] < 0)
        sort = TRUE ;
      else
        last = (q->rowind[i]).colind[j] ;
      if ((q->rowind[i]).colind[j] >= max)
        max = (q->rowind[i]).colind[j] ;
    }
    if (max >= leng)
    {
      mat_double_vektor_free(help_row) ;
      help_row = mat_double_vektor_new(max+1) ;
      leng = max + 1 ;
    }
    if (sort)
    {
      for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
      if ((q->rowind[i]).colind[j] >= 0)
        help_row->vektor[(q->rowind[i]).colind[j]] += 
          (q->rowind[i]).val[j] ;
      k = 0 ;
      for (j = 0; j < leng; j++)
      if (help_row->vektor[j] != 0.0)
      {
        (q->rowind[i]).val[k]    = help_row->vektor[j] ;
        (q->rowind[i]).colind[k] = j                   ;
        k++ ;
        help_row->vektor[j] = 0.0 ;
      }
      if (k < (q->rowind[i]).no_of_entries)
      {
        for (j = k; j < (q->rowind[i]).no_of_entries; j++)
        {
          (q->rowind[i]).val[j]    = 0.0 ;
          (q->rowind[i]).colind[j] = 0   ;
        }
        (q->rowind[i]).no_of_entries = k;
      }
    }
  }
  mat_double_vektor_free (help_row)   ;
  return(NO_ERROR) ;
}


/**********************
mat_sparse_matrix_print_file ##e 
Datum:  14.2.95
Autor:  Peter Kemper
Input:
Output:
Side-Effects:
Description:

**********************/
#ifndef CC_COMP
int mat_sparse_matrix_print_file(FILE *fp, Sparse_matrix *v)
#else
int mat_sparse_matrix_print_file(fp, v)
     FILE *fp ;
     Sparse_matrix *v ;
#endif
{
  unsigned i ;
  
  if (!fp)
    return FALSE ;
  if (!v) 
  {
    fprintf(fp,"Sparse_Matrix ist leer\n") ;
    return TRUE ; 
  }
  fprintf(fp,"\nAusgabe eines Sparse_Matrix-Objektes mit %i Zeilen\n", 
	  v->ord) ;
  fprintf(fp,
	  "Anzahl non-zeros %i, Bandwidth %i, Diagmax %f, Diagmin %f, active %i\n",
	  v->n_of_nonzeros, v->bandwidth, v->diagmax, v->diagmin, v->active ) ; 
  fprintf(fp,"Ausgabe der einzelnen Diagonaleintraege und Row-Objekte:\n") ;
  if (v->diagonals)
  {
    for (i=0 ;i<v->ord ; i++) 
    {
      fprintf(fp, "Diag: %f\n", (v->diagonals)[i]) ; 
      mat_row_print_file(fp, &(v->rowind[i])) ; 
    }
    fprintf(fp,"\nEnde des Sparse_Matrix-Objektes \n") ; 
  }
  else
  {
    for (i=0 ;i<v->ord ; i++) 
    {
      fprintf(fp, "Diag: MISSING, NULL!\n") ; 
      mat_row_print_file(fp, &(v->rowind[i])) ; 
    }
    fprintf(fp,"\nEnde des Sparse_Matrix-Objektes \n") ;
  }
      
  return TRUE ;  
}



/**********************
mat_sparse_matrix_mult_vektor
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Hilfsvektor
        diag Flag, ob die Diagonalelemente mit einbezogen werden sollen
Output: 
Side-Effects:
Description:
Die Funktion berechnet einen Iterationsschritt der Power Methode
p = p*q. Das Ergebnis wrid im Vektor p zurueckgeliefert, der auch den
Iterationsvektor als Eingabeparameter beihaltet. Falls diag=TRUE ist, wird auch
mit den Diagonalelementen multipliziert, bei diag=FALSE, werden die Diagonalelemente
weggelassen.
Die Laenge des Vektor ph wird nicht ueberprueft, da die Funktion auch fuer 
n x m Matrizen verwendet werden kann. 
**********************/
#ifndef CC_COMP
void mat_sparse_matrix_mult_vektor 
      (Sparse_matrix *q, Double_vektor **p, Double_vektor **ph, short diag) 
#else
void mat_sparse_matrix_mult_vektor (q, p, ph, diag) 
  Sparse_matrix  *q    ;
  Double_vektor **p    ;
  Double_vektor **ph   ;
  short           diag ;
#endif
{
  Double_vektor *ph2 ;
  int i, j ;

  if (q == NULL || (*p) == NULL || (*ph) == NULL ||
      ((*p)->no_of_entries < q->ord))
  {
    printf("Wrong parameter in mat_sparse_matrix_mult_vektor\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  if (diag)
  {
    for (i = 0; i < q->ord && i < (*ph)->no_of_entries; i++)
      (*ph)->vektor[i] = (*p)->vektor[i] * q->diagonals[i] ;
  }
  else
  {
    for (i = 0; i < (*ph)->no_of_entries; i++)
      (*ph)->vektor[i] = 0.0 ;
  }
  for (i = 0; i < q->ord; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    (*ph)->vektor[(q->rowind[i]).colind[j]] += 
      (q->rowind[i]).val[j] * (*p)->vektor[i] ;
  ph2 = *p  ;
  *p  = *ph ;
  *ph = ph2 ;   
} /* mat_sparse_matrix_mult_vektor */

/**********************
mat_diagonal_vektor_mult
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Zielvektor
Output: 
Side-Effects:
Description:
Die Funktion berechnet das Produkt
ph = p*q.diag. Das Ergebnis wird im Vektor ph zurueckgeliefert. 
Der Vektor ph wird nicht initialisiert, sondern die neuen Werten zu den 
bisherigen Werten dazu addiert.
**********************/
#ifndef CC_COMP
void mat_diagonal_vektor_mult 
      (Sparse_matrix *q, Double_vektor *p, Double_vektor *ph) 
#else
void mat_diagonal_vektor_mult (q, p, ph) 
  Sparse_matrix *q    ;
  Double_vektor *p    ;
  Double_vektor *ph   ;
#endif
{
  int i ;

  if (q == NULL || p == NULL || ph == NULL ||
      (p->no_of_entries < q->ord))
  {
    printf("Wrong parameter in mat_diagonal_mult_vektor\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  for (i = 0; i < q->ord && i < ph->no_of_entries; i++)
    ph->vektor[i] += p->vektor[i] * q->diagonals[i] ;
} /* mat_diagonal_vektor_mult */

/**********************
mat_sparse_matrix_vektor_mult
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Zielvektorvektor
        diag Flag, ob die Diagonalelemente mit einbezogen werden sollen
Output: 
Side-Effects:
Description:
Die Funktion berechnet das Produkt
ph = p*q. Das Ergebnis wird im Vektor ph zurueckgeliefert. Falls diag=TRUE ist, wird auch
mit den Diagonalelementen multipliziert, bei diag=FALSE, werden die Diagonalelemente
weggelassen. Der Vektor ph wird nicht initialisiert, sondern die neuen Werten zu den 
bisherigen Werten dazu addiert.
Die Laenge des Vektor ph wird nicht ueberprueft, da die Funktion auch fuer 
n x m Matrizen verwendet werden kann. 
**********************/
#ifndef CC_COMP
void mat_sparse_matrix_vektor_mult 
      (Sparse_matrix *q, Double_vektor *p, Double_vektor *ph, short diag) 
#else
void mat_sparse_matrix_vektor_mult (q, p, ph, diag) 
  Sparse_matrix *q    ;
  Double_vektor *p    ;
  Double_vektor *ph   ;
  short          diag ;
#endif
{
  int i, j ;

  if (q == NULL || p == NULL || ph == NULL ||
      (p->no_of_entries < q->ord))
  {
    printf("Wrong parameter in mat_sparse_matrix_mult_vektor\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  if (diag)
  {
    for (i = 0; i < q->ord && i < ph->no_of_entries; i++)
      ph->vektor[i] += p->vektor[i] * q->diagonals[i] ;
  }
  for (i = 0; i < q->ord; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    ph->vektor[(q->rowind[i]).colind[j]] += 
      (q->rowind[i]).val[j] * p->vektor[i] ; 
} /* mat_sparse_matrix_vektor_mult */

/**********************
mat_sparse_matrix_vektor_mult
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        p Iterationsvektor
        ph Zielvektorvektor
        diag Flag, ob die Diagonalelemente mit einbezogen werden sollen
Output: 
Side-Effects:
Description:
Die Funktion berechnet das Produkt
ph = q*p. Das Ergebnis wird im Vektor ph zurueckgeliefert. Falls diag=TRUE ist, wird auch
mit den Diagonalelementen multipliziert, bei diag=FALSE, werden die Diagonalelemente
weggelassen. Der Vektor ph wird nicht initialisiert, sondern die neuen Werten zu den 
bisherigen Werten dazu addiert.
Die Laenge des Vektor ph wird nicht ueberprueft, da die Funktion auch fuer 
n x m Matrizen verwendet werden kann. 
**********************/
#ifndef CC_COMP
void mat_sparse_transposed_matrix_vektor_mult 
      (Sparse_matrix *q, Double_vektor *p, Double_vektor *ph, short diag) 
#else
void mat_sparse_transposed_matrix_vektor_mult (q, p, ph, diag) 
  Sparse_matrix *q    ;
  Double_vektor *p    ;
  Double_vektor *ph   ;
  short          diag ;
#endif
{
  int i, j ;

  if (q == NULL || p == NULL || ph == NULL ||
      (p->no_of_entries < q->ord))
  {
    printf("Wrong parameter in mat_sparse_matrix_mult_vektor\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  if (diag)
  {
    for (i = 0; i < q->ord && i < ph->no_of_entries; i++)
      ph->vektor[i] += p->vektor[i] * q->diagonals[i] ;
  }
  for (i = 0; i < q->ord; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    ph->vektor[i] += 
      (q->rowind[i]).val[j] * p->vektor[(q->rowind[i]).colind[j]] ; 
} /* mat_sparse_transposed_matrix_vektor_mult */

/**********************
mat_sparse_matrix_matrix_vektor_product
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        v Iterationsvektor
Output: w Ergebnisvektor
Side-Effects:
Description:
Die Funktion berechnet das Produkt w = v*q. v und q muessen die gleiche L�nge haben!
**********************/
#ifndef CC_COMP
void mat_sparse_matrix_matrix_vektor_product 
      (Sparse_matrix *q, Double_vektor *v, Double_vektor *w) 
#else
void mat_sparse_matrix_matrix_vektor_product (q, v, w) 
  Sparse_matrix *q  ;
  Double_vektor *v  ;
  Double_vektor *w ;
#endif
{
  int i, j ;

  if (q == NULL || v == NULL || w == NULL ||
      (v->no_of_entries < q->ord))
  {
    printf("Wrong parameter in mat_sparse_matrix_matrix_vektor_product\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  for (i = 0; i < q->ord; i++)
    w->vektor[i] = v->vektor[i] * q->diagonals[i] ;
  for (i = 0; i < q->ord; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    w->vektor[(q->rowind[i]).colind[j]] += 
      (q->rowind[i]).val[j] * v->vektor[i] ;
} /* mat_sparse_matrix_matrix_vektor_product  */

/**********************
mat_sparse_matrix_transposed_matrix_vektor_product
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
        v Iterationsvektor
Output: w Ergebnisvektor
Side-Effects:
Description:
Die Funktion berechnet das Produkt w = q*v. v und q muessen die gleiche L�nge haben!
**********************/
#ifndef CC_COMP
void mat_sparse_matrix_transposed_matrix_vektor_product 
      (Sparse_matrix *q, Double_vektor *v, Double_vektor *w) 
#else
void mat_sparse_matrix_transposed_matrix_vektor_product (q, v, w) 
  Sparse_matrix *q  ;
  Double_vektor *v  ;
  Double_vektor *w ;
#endif
{
  int i, j ;

  if (q == NULL || v == NULL || w == NULL ||
      (v->no_of_entries < q->ord))
  {
    printf("Wrong parameter in mat_sparse_matrix_matrix_vektor_product\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  for (i = 0; i < q->ord; i++)
    w->vektor[i] = v->vektor[i] * q->diagonals[i] ;
  for (i = 0; i < q->ord; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    w->vektor[i] += 
      (q->rowind[i]).val[j] * v->vektor[(q->rowind[i]).colind[j]] ; 
} /* mat_sparse_matrix_transposed_matrix_vektor_product  */

/**********************
mat_sparse_matrix_to_p
Datum:
Autor:  Peter Buchholz
Input:  q Generatormatrix
Output: q transformierte stochastische Matrix
        alpha Wert des absolut groessten Diagonalelements
Side-Effects:
Description:
Die Funktion transformiert eine Generatormatrix mittels Randomization in
eine stochastische Matrix. Falls die Matrix negative Elemente enthaelt,
so bleibt so wird WRONG_MATRIX als Funktionswert 
zurueckgegebem, im Erfolgsfall wird NO_ERROR zurueckgegeben.
ACHTUNG: Diagonalelemente in der Eingabematrix werden nicht beachtet.
Die Arrays l, u und d werden dabei nicht gesetzt.
**********************/
#ifndef CC_COMP
int mat_sparse_matrix_to_p (Sparse_matrix *q, double *alpha)
#else
int mat_sparse_matrix_to_p (q, alpha)
  Sparse_matrix *q     ;
  double        *alpha ;
#endif
{
  int    i,   j   ;
  double val, max ;

  if (q == NULL)
  {
    printf("Wrong parameter in mat_sparse_matrix_to_p\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  max = 0.0 ;
  for (i = 0; i < q->ord; i++)
  {
    val = 0.0 ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    if ((q->rowind[i]).val[j] < 0.0)
      return(WRONG_MATRIX) ;
    else
      val += (q->rowind[i]).val[j] ;
    q->diagonals[i] = val ;
    if (val > max)
      max = val ;
  }
  /* Diagonalelemente gesetzt und maximales Diagonalelement bestimmt */
  *alpha = max ;
  if (max > 0.0)
    max = 1.0 / max ;
  for (i = 0; i < q->ord; i++)
  {
    q->diagonals[i] = 1.0 - (q->diagonals[i] * max) ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
      (q->rowind[i]).val[j] *= max ;
  }
  return(NO_ERROR) ;
} /* mat_sparse_matrix_to_p */


/**********************
mat_sparse_matrix_normalize
Datum:
Autor:  Peter Buchholz
Input:  q Eingabematrix
        diag falls diag=TRUE werden in NUllzeilen die Diagonalelemente auf 1 gesetzt
Output: q transformierte Eingabematrix
Side-Effects:
Description:
Die Funktion normalisiert die Zeilen einer nicht negativen Matrix so, dass
die Zeilensumme 1 ergibt. Falls diag FALSE ist, werden NUllzeilen nicht veraendert, 
bei diag TRUE werden die Diagonalelemente der Nullzeilen auf 1 gesetzt.
ACHTUNG: Wenn die Matrix negative Elemente beinhaltet, so bricht die Funktion mit
Rueckgabewert WRONG_MATRIX ab, ansonsten liefert sie NO_ERROR.
**********************/
#ifndef CC_COMP
int mat_sparse_matrix_normalize  (Sparse_matrix *q, short diag)
#else
int mat_sparse_matrix_normalize  (q, diag)
  Sparse_matrix *q    ;
  short          diag ;
#endif
{
  int    i, j ;
  double val  ;

  if (q == NULL)
  {
    printf("Wrong parameter in mat_sparse_matrix_normalize\nSTOP EXECUTION!!\n") ;
    exit(-1) ;
  }
  for (i = 0; i < q->ord; i++)
  {
    if (q->diagonals[i] < 0.0)
      return(WRONG_MATRIX) ;
    else
      val = q->diagonals[i] ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
    if ((q->rowind[i]).val[j] < 0.0)
      return(WRONG_MATRIX) ;
    else
      val += (q->rowind[i]).val[j] ;
    if (val > 0.0)
    {
      if (fabs(val - 1.0) > THRESHOLD)
      {
        val = 1.0 / val ;
        q->diagonals[i] *= val ;
        for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
          (q->rowind[i]).val[j] *= val ;
      }
    }
    else
    {
      if (diag == TRUE)
        q->diagonals[i]  = 1.0 ;
    }
  }
  return(NO_ERROR) ;
} /* mat_sparse_matrix_normalize */

/**********************
mat_sparse_matrix_mult_scalar
Datum:  08.05.98
Autor:  Peter Buchholz
Input:  q Eingabematrix
        s Skalarwert
Output: q wobei alle Elemente mit s multipliziert wurden
Side-Effects:
Description:
Die Funktion multipliziert alle Elemente von q mit s.
**********************/
#ifndef CC_COMP
void mat_sparse_matrix_mult_scalar (Sparse_matrix *q, double s) 
#else
void mat_sparse_matrix_mult_scalar (q, s) 
  Sparse_matrix *q ;
  double         s ;
#endif  
{
  int i, j ;

  if (q != NULL)
  {
    for (i = 0; i < q->ord; i++)
    {
      q->diagonals[i] *= s ;
      for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
        (q->rowind[i]).val[j] *= s ;
    } 
  }
} /* mat_sparse_matrix_mult_scalar */

/**********************
mat_sparse_matrix_extract
Datum:  12.6.95
Autor:  Peter Buchholz
Input:  q Subnetzmatrix
        row_start Erste Zeile der Submatrix
        row_stop Letzte Zeile + 1 der Submatrix
        col_start Erste Zeile der Submatrix
        col_stop Letzte Zeile + 1 der Submatrix
Output: Funktionswert liefert die Untermatrix, die zum angegebenen Block
        gehoert.
Side-Effects:
Description:
Die Funktion extrahiert eine Submatrix mit den angegbenen Blockgrenzen aus 
einer Sparse_matrix. Die Submatrix wird als Sparse_matrix zurueckgegeben. Die
urspruengliche Sparse_matrix wird nicht veraendert. Falls die Submatrix keine
Elemente enthaelt wird NULL zurueckgegeben!!
***********************************************************************/
#ifndef CC_COMP
Sparse_matrix *mat_sparse_matrix_extract
  (Sparse_matrix *q, int row_start, int row_stop, int col_start, int col_stop)
#else
Sparse_matrix *mat_sparse_matrix_extract
                    (q, row_start, row_stop, col_start, col_stop) 
  Sparse_matrix *q         ;
  int            row_start ;
  int            row_stop  ;
  int            col_start ;
  int            col_stop  ;
#endif
{
  unsigned       i, j, k, elems  ;
  unsigned       length   ;
  double        *values   ;
  Sparse_matrix *q_sub    ;

  elems = 0 ;
  q_sub = mat_sparse_matrix_new (row_stop - row_start) ;
  values = (double *) ecalloc(col_stop-col_start, sizeof(double)) ;
  for (i = row_start; i < row_stop; i++)
  {
    for (k = 0; k < col_stop - col_start; k++)
      values[k] = 0.0 ;
    length = 0 ;
    j = 0 ;
    while (j < (q->rowind[i]). no_of_entries && 
           (q->rowind[i]).colind[j] < col_start)
      j++ ;
    while (j < (q->rowind[i]). no_of_entries && 
           (q->rowind[i]).colind[j] < col_stop)
    {
      length++ ;
      k = (q->rowind[i]).colind[j] - col_start ;
      values[k] =  (q->rowind[i]).val[j] ;
      j++ ;
    }    
    q_sub->rowind[i-row_start].no_of_entries = length ;
    q_sub->rowind[i-row_start].colind = (size_t *) ecalloc(length,sizeof(size_t)) ;
    q_sub->rowind[i-row_start].val = (double *) ecalloc(length,sizeof(double)) ;
    elems += length ;
    k = 0 ;
    j = 0 ;
    while (k < length  && j < col_stop - col_start)
    {
      if (values[j] != 0.0)
      {
        q_sub->rowind[i-row_start].colind[k] = j         ;
        q_sub->rowind[i-row_start].val[k]    = values[j] ;
        k++ ;
      }
      j++ ;
    }
  }
  efree(values) ;
  if (elems > 0)
  {
    mat_sparse_matrix_set_diagonals(q_sub, FALSE) ;
    return(q_sub) ;
  }
  else
  {
    mat_sparse_matrix_free(q_sub) ;
    return(NULL) ;
  }
} /* mat_sparse_matrix_extract */

/**********************
mat_sparse_matrix_extract_with_diagonals
Datum:  27.08.12
Autor:  Peter Buchholz
Input:  q Subnetzmatrix
        row_start Erste Zeile der Submatrix
        row_stop Letzte Zeile + 1 der Submatrix
        col_start Erste Zeile der Submatrix
        col_stop Letzte Zeile + 1 der Submatrix
Output: Funktionswert liefert die Untermatrix, die zum angegebenen Block
        gehoert.
Side-Effects:
Description:
Die Funktion extrahiert eine Submatrix mit den angegbenen Blockgrenzen aus 
einer Sparse_matrix. Die Submatrix wird als Sparse_matrix zurueckgegeben. Die
urspruengliche Sparse_matrix wird nicht veraendert. Falls die Submatrix keine
Elemente enthaelt wird NULL zurueckgegeben!! Im Gegensatz zur
Funktion mat_sparse_matrix_extract werden die Diagonalelemente aus den Diagonalsubmatrizen
�bernommen und sonst auf 0 belassen.
***********************************************************************/
#ifndef CC_COMP
Sparse_matrix *mat_sparse_matrix_extract_with_diags
  (Sparse_matrix *q, int row_start, int row_stop, int col_start, int col_stop)
#else
Sparse_matrix *mat_sparse_matrix_extract_with_diags
                    (q, row_start, row_stop, col_start, col_stop) 
  Sparse_matrix *q         ;
  int            row_start ;
  int            row_stop  ;
  int            col_start ;
  int            col_stop  ;
#endif
{
  unsigned       i, j, k, elems  ;
  unsigned       length   ;
  double        *values   ;
  Sparse_matrix *q_sub    ;

  elems = 0 ;
  q_sub = mat_sparse_matrix_new (row_stop - row_start) ;
  values = (double *) ecalloc(col_stop-col_start, sizeof(double)) ;
  for (i = row_start; i < row_stop; i++)
  {
    for (k = 0; k < col_stop - col_start; k++)
      values[k] = 0.0 ;
    length = 0 ;
    j = 0 ;
    while (j < (q->rowind[i]). no_of_entries && 
           (q->rowind[i]).colind[j] < col_start)
      j++ ;
    while (j < (q->rowind[i]). no_of_entries && 
           (q->rowind[i]).colind[j] < col_stop)
    {
      length++ ;
      k = (q->rowind[i]).colind[j] - col_start ;
      values[k] =  (q->rowind[i]).val[j] ;
      j++ ;
    }    
    q_sub->rowind[i-row_start].no_of_entries = length ;
    q_sub->rowind[i-row_start].colind = (size_t *) ecalloc(length,sizeof(size_t)) ;
    q_sub->rowind[i-row_start].val = (double *) ecalloc(length,sizeof(double)) ;
    elems += length ;
    k = 0 ;
    j = 0 ;
    while (k < length  && j < col_stop - col_start)
    {
      if (values[j] != 0.0)
      {
        q_sub->rowind[i-row_start].colind[k] = j         ;
        q_sub->rowind[i-row_start].val[k]    = values[j] ;
        k++ ;
      }
      j++ ;
    }
    /* Diagonalelemente f�r Diagonalmatizen �bernehmen */
    if (row_start == col_start && row_stop == col_stop)
      if (fabs(q->diagonals[i]) > 1.0e-12) {
	q_sub->diagonals[i - row_start] = q->diagonals[i] ;
	elems++ ;
      }
  } /* for i */
  efree(values) ;
  if (elems > 0)
  {
    // JUST TO TEST BEGIN
    mat_sparse_matrix_set_diagonals(q_sub, FALSE) ;
    // JUST TO TEST END 
    return(q_sub) ;
  }
  else
  {
    mat_sparse_matrix_free(q_sub) ;
    return(NULL) ;
  }
} /* mat_sparse_matrix_extract_with_diagonals */

 
/**********************
mat_sparse_matrix_gen_identity
Datum:  20.02.02
Autor:  Peter Buchholz
Input:  ord Dimenion der Matrix
        in_row falls true Werte in Zeile + Diagonalen bei false nur in der Diagonalen
Output: Funktionswert liefert eine Einheitsmatrix der Dimesnion ord
Side-Effects:
Description:
Die Funktion generiert eine Einheitsmatrix der Diemsnion ord.
***********************************************************************/
#ifndef CC_COMP
Sparse_matrix *mat_sparse_matrix_gen_identity (int ord, short in_row)
#else
Sparse_matrix *mat_sparse_matrix_gen_identity (ord, in_row)
     int   ord     ;
     short in_row  ;
#endif
{
  int i ;
  Sparse_matrix *q ;

  q = mat_sparse_matrix_new(ord) ;
  if (in_row) {
    for (i = 0; i < ord; i++) {
      (q->rowind[i]).val    = (double *) ecalloc (1, sizeof(double)) ;
      (q->rowind[i]).colind = (size_t *) ecalloc (1, sizeof(size_t)) ;
      (q->rowind[i]).val[0] = 1.0 ;
      (q->rowind[i]).colind[0] = i ;
      (q->rowind[i]).no_of_entries = 1 ;
    }
  }
  else {
    for (i = 0; i < ord; i++) {
      q->diagonals[i] = 1.0 ;
      (q->rowind[i]).no_of_entries = 0 ;
    }
  }
  return(q) ;
} /* mat_sparse_matrix_gen_identity */

/**********************
mat_sparse_matrix_compute_bandwidth
Datum:  14.11.02
Autor:  Peter Buchholz
Input:  q Matrix
        diag falls diag > 0 wird angenommen, dass das Diagonalelement != 0 ist
             falls diaf = 0 wird das Diagonalelement nicht beachtet!!
Output: Funktionswert liefert die Bandbreite, die auch im Parameter
        bandwidth der Matrix gesetzt wird.
Side-Effects:
Description:
Die Funktion berechnet die maximale Bandbreite einer Matrix.
Es wird davon ausgegangen, dass die Elemente in den Matrixzeilen geordnet sind!
***********************************************************************/
#ifndef CC_COMP
int mat_sparse_matrix_compute_bandwidth (Sparse_matrix *q, short diag) 
#else
int mat_sparse_matrix_compute_bandwidth (q, diag) 
  Sparse_matrix *q    ;
  short          diag ;
#endif
{
  unsigned       i, bw=0, first, last  ;

  for (i = 0; i < q->ord; i++) {
    if (diag) 
      first = last = i ;
    else {
      first = q->ord ;
      last  = 0      ;
    }
    if ((q->rowind[i]).no_of_entries > 0) {
      if ((q->rowind[i]).colind[0] < first)
        first = (q->rowind[i]).colind[0] ;
      if (last < (q->rowind[i]).colind[(q->rowind[i]).no_of_entries-1]) 
        last =  (q->rowind[i]).colind[(q->rowind[i]).no_of_entries-1] ; 
      if (last - first + 1 > bw) 
        bw = last - first + 1 ;
    }
    else {
      if (diag && bw == 0)
	bw = 1 ; 
    }
  }
  q->bandwidth = bw ;
  return(bw) ;
} /* mat_sparse_matrix_compute_bandwidth */

/**********************
mat_sparse_matrix_permute_matrices
Datum:  12.08.97 (urspr�ngliche Realisierung in matorder)
Autor:  Peter Buchholz
Input:  matrices Matrizenarray
        perm Zustandspermutation
        n Anzahl Matrizen in matrices
Output: matrices Matrizenarray in dme die Zeilen und Spalten
                aller Matrizen wie in perm beschrieben permutiert wurden
Side-Effects:
Description:
Die Funktion permutiert die Zeilen und Spalten der Matrizen in matrices,
wie in der Permutation perm angegeben.
***********************************************************************/
#ifndef CC_COMP
int mat_sparse_matrix_permute_matrices(Sparse_matrix **matrices, int *perm, int n) 
#else
int mat_sparse_matrix_permute_matrices(matrices, perm, n) 
  Sparse_matrix **matrices ;
  int            *perm     ;
  int             n        ;
#endif
{
  int i, j, k, p, ord ;
  Sparse_matrix *q ;

  ord = matrices[0]->ord ;
  for (k = 0; k < n; k++)
  {
    q = mat_sparse_matrix_new(ord) ;
    for (i = 0; i < ord; i++)
    {
      p = perm[i] ;
      (q->rowind[p]).no_of_entries = (matrices[k]->rowind[i]).no_of_entries ;
      (q->rowind[p]).colind        = (matrices[k]->rowind[i]).colind        ;
      (matrices[k]->rowind[i]).colind = NULL ;
      (q->rowind[p]).val           = (matrices[k]->rowind[i]).val           ;
      (matrices[k]->rowind[i]).val = NULL  ;

      /* ctd12.11 */
      q->diagonals[p] = matrices[k]->diagonals[i];
    }
    mat_sparse_matrix_free(matrices[k]) ;
    for (i = 0; i < q->ord; i++)
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
      (q->rowind[i]).colind[j] = perm[(q->rowind[i]).colind[j]] ;
    mat_sparse_matrix_sort_reduce_elems(q) ;
    matrices[k] = q ;
  }
  return(NO_ERROR) ;
} /* mat_sparse_matrix_permute_matrices */

/**********************
mat_sparse_matrix_prt_small_matrix
Datum:  25.08..05
Autor:  Peter Buchholz
Input:  q Matrix
Output: 
Side-Effects:
Description:
Die Funktion druckt eine Matrix als dichtbesetzte Matrix aus
***********************************************************************/
#ifndef CC_COMP
void mat_sparse_matrix_prt_small_matrix (Sparse_matrix *q)
#else
void mat_sparse_matrix_prt_small_matrix (q)
     Sparse_matrix *q    ; 
#endif
{
  int i, j, k ;

  for (i = 0; i < q->ord; i++) {
    j = 0 ;
    for (k = 0; k < q->ord; k++) {
	if (k == i)
	    printf("%.5e ", q->diagonals[i]) ;
	else {
	    if (j < (q->rowind[i]).no_of_entries && k == (q->rowind[i]).colind[j]) {
		printf("%.5e ", (q->rowind[i]).val[j]) ;
		j++ ;
	    }
	    else {
		printf("0.000e+00 ") ;
	    }
	}
    }
    printf("\n") ;
  }
  printf("\n") ;
} /* mat_sparse_matrix_small_matrix */

/**********************
mat_prt_small_matrix_without_diagonals
Datum:  01.03.10
Autor:  Peter Buchholz
Input:  q Matrix
Output: 
Side-Effects:
Description:
Die Funktion druckt eine Matrix als dichtbesetzte Matrix aus und geht davon aus, dass
die Diagonalelements in dne Zielne eingeordnet sind.
***********************************************************************/
extern void mat_prt_small_matrix_without_diagonals(Sparse_matrix *q, int co) 
{
  int i, j, k ;

  for (i = 0; i < q->ord; i++) {
    j = 0 ;
    for (k = 0; k < co; k++) {
      if (j < (q->rowind[i]).no_of_entries && k == (q->rowind[i]).colind[j]) {
	printf("%.3e ", (q->rowind[i]).val[j]) ;
	j++ ;
      }
      else {
	printf("0.000e+00 ") ;
      }
    }
    printf("\n") ;
  }
  printf("\n") ;
} /* mat_prt_small_matrix_without_diagonals */

/**********************
mat_sparse_matrix_prt_structure
Datum:  15.11.02
Autor:  Peter Buchholz
Input:  q Matrix
        diag 0 keine ber�cksichtigung der Diagonalaelemente
             1 Ber�cksichtigung der Diagonalelemente
            -1 Diagonalelemenet als ungleich 0 angenommen
Output: 
Side-Effects:
Description:
Die Funktion druckt die Nichtnullstruktur einer Matrix.
***********************************************************************/
#ifndef CC_COMP
void mat_sparse_matrix_prt_structure (Sparse_matrix *q, int diag)
#else
void mat_sparse_matrix_prt_structure (q, diag)
     Sparse_matrix *q    ; 
     int            diag ;
#endif
{
  int i, j, k, c ;

  for (i = 0; i < q->ord; i++) {
    printf("|") ,
    j = 0 ;
    for (k = 0; k < (q->rowind[i]).no_of_entries; k++) {
      c = (q->rowind[i]).colind[k] ;
      while (j < c) {
	if ((i == j) && (diag == -1 || (diag == 1 && q->diagonals[i] != 0.0)))
	  printf("X") ;
        else
          printf(" ") ;
	j++ ;
      }
      printf("X") ;
      j++ ;
    }
    while (j  < q->ord) {
      if ((i == j) && (diag == -1 || (diag == 1 && q->diagonals[i] != 0.0)))
	printf("X") ;
      else
        printf(" ") ;
      j++ ;
    }
    printf("|\n") ;
  }
  printf("\n") ;
} /* mat_sparse_matrix_prt_structure */


/**********************
mat_sparse_matrix_norm
Datum:  10.01.03
Autor:  Peter Buchholz
Input:  q1, q2 Matrizen
Output: n_sum, n_max Summen- und Maximumsnorm der Matrizen
Side-Effects:
Description:
Die Funktion berechent die Normen der Matrix |q1 -q2|. Die Matrizen m�ssen gleiche 
Dimension haben. 
***********************************************************************/
#ifndef CC_COMP
int mat_sparse_matrix_norm (Sparse_matrix *q1, Sparse_matrix *q2,
                            double *max_norm, double *sum_norm) 
#else
int mat_sparse_matrix_norm (q1, q2, max_norm, sum_norm)
  Sparse_matrix *q1       ;
  Sparse_matrix *q2       ;
  double        *max_norm ;
  double        *sum_norm ;
#endif
{
  int r, i1, i2 ;
  double val ;

  if (q1->ord != q2->ord) {
    printf("Fatal Error: Different matrix dimensions in  mat_sparse_matrix_norm\n") ;
    printf("STOP EXECUTION\n") ;
    exit(-1) ; 
  }
  *max_norm = *sum_norm = 0.0 ;
  for (r = 0; r < q1->ord; r++) {
    i1 = i2 = 0 ;
    while (i1 < (q1->rowind[r]).no_of_entries && i2 < (q2->rowind[r]).no_of_entries) {
      if ((q1->rowind[r]).colind[i1] == (q2->rowind[r]).colind[i2]) {
	val = fabs((q1->rowind[r]).val[i1] - (q2->rowind[r]).val[i2]) ;
	*sum_norm += val ;
	if (val > *max_norm)
	  *max_norm = val ;
	i1++ ;
	i2++ ;
      }
      else { 
	if ((q1->rowind[r]).colind[i1] < (q2->rowind[r]).colind[i2]) {
	  val = fabs((q1->rowind[r]).val[i1]) ;
	  *sum_norm += val ;
	  if (val > *max_norm)
	    *max_norm = val ;
	  i1++ ;
	} 
	else {
	  if ((q1->rowind[r]).colind[i1] > (q2->rowind[r]).colind[i2]) {
	    val = fabs((q2->rowind[r]).val[i2]) ;
	    *sum_norm += val ;
	    if (val > *max_norm)
	      *max_norm = val ;
	    i2++ ;
	  } 
	}
      }
    }
    while (i1 < (q1->rowind[r]).no_of_entries) {
      val = fabs((q1->rowind[r]).val[i1]) ;
      *sum_norm += val ;
      if (val > *max_norm)
	*max_norm = val ;
      i1++ ;
    }
    while (i2 < (q2->rowind[r]).no_of_entries) {
      val = fabs((q2->rowind[r]).val[i2]) ;
      *sum_norm += val ;
      if (val > *max_norm)
	*max_norm = val ;
      i2++ ;
    }
  }
  return(NO_ERROR) ;
} /* mat_sparse_matrix_norm */

/**********************
mat_sparse_matrix_overwrite
Datum:  10.01.03
Autor:  Peter Buchholz
Input:  q1, q2 Matrizen
Output: q1 enth�lt Kopie von q2
Side-Effects:
Description:
Die Funktion �berschreibt q1 mit q2. Die Matrizen m�ssen gleiche 
Dimension und identische Nichtnullelemente haben. 
***********************************************************************/
#ifndef CC_COMP
int mat_sparse_matrix_overwrite(Sparse_matrix *q1, Sparse_matrix *q2) 
#else
int mat_sparse_matrix_overwrite(q1, q2)
  Sparse_matrix *q1 ;
  Sparse_matrix *q2 ;
#endif 
{
  int r, i ;

  if (q1->ord != q2->ord) {
    printf("Fatal Error: Different matrix dimensions in  mat_sparse_matrix_overwrite\n") ;
    printf("STOP EXECUTION\n") ;
    exit(-1) ; 
  }
  for (r = 0; r < q1->ord; r++) {
    if ((q1->rowind[r]).no_of_entries != (q2->rowind[r]).no_of_entries) {
      printf("Fatal Error: Different number of non-zeros in row %i in mat_sparse_matrix_overwrite\n", r) ;
      printf("STOP EXECUTION\n") ;
      exit(-1) ; 
    }
    q1->diagonals[r] = q2->diagonals[r] ;
    for (i = 0; i < (q1->rowind[r]).no_of_entries; i++) {
      if ((q1->rowind[r]).colind[i] != (q2->rowind[r]).colind[i]) {
        printf("Fatal Error: Different column index in row %i pos %i in mat_sparse_matrix_overwrite\n", r, i) ;
        printf("STOP EXECUTION\n") ;
        exit(-1) ; 
      }
      (q1->rowind[r]).val[i] = (q2->rowind[r]).val[i] ;
    }
  }
  return(NO_ERROR) ;
} /* mat_sparse_matrix_overwrite */

/**********************
mat_sparse_matrix_eliminate_small_values
Datum:  16.01.03
Autor:  Peter Buchholz
Input:  q Matrix, deren Element gl�scht werdne sollen
        threshold Grenze, ab der Elemente gel�scht werden
Output: q modifizierte Matrix
Side-Effects:
Description:
Die Funktion l�scht alle ELemente < threshold aus einer Matrix.
***********************************************************************/
#ifndef CC_COMP
short mat_sparse_matrix_eliminate_small_values(Sparse_matrix *q, double threshold)
#else
short mat_sparse_matrix_eliminate_small_values(q, threshold)
  Sparse_matrix *q         ;
  double         threshold ;
#endif
{
  short modified = 0 ;
  size_t i, j, k, *col, no ;
  double *val ;

  for (i = 0; i < q->ord; i++) {
    no = 0 ;
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++) {
      if (fabs((q->rowind[i]).val[j]) < threshold) {
	(q->rowind[i]).val[j] = 0.0 ;
	modified = 1 ; 
      }
      else {
	no++ ;
      }
    }
    /* Falls Elemente auf 0 gesetzt wurden, werden diese nun aus der Datenstruktur entfernt */
    if (no < (q->rowind[i]).no_of_entries) {
      col = (size_t *) ecalloc (no, sizeof(size_t)) ;
      val = (double *) ecalloc (no, sizeof(double)) ;
      k = 0 ;
      for (j = 0; j < (q->rowind[i]).no_of_entries; j++) {
	if ((q->rowind[i]).val[j] != 0.0) {
	  col[k] = (q->rowind[i]).colind[j] ;
	  val[k] = (q->rowind[i]).val[j] ;
	  k++ ;
	}
      }
      (q->rowind[i]).no_of_entries = no ;
      efree((q->rowind[i]).colind) ;
      (q->rowind[i]).colind = col ;
      efree((q->rowind[i]).val) ;
      (q->rowind[i]).val = val ;
    }
  }
  return(modified) ;
} /* mat_sparse_matrix_eliminate_small_values */


/**********************
mat_transform_matrix_to_vector
Datum:  07.02.03
Autor:  Peter Buchholz
Input:  p1 substochastische Eingabematrix
Output: pv Vektor der Zeilensummen der Matrix
        pi auf 1 normierter Vektor der Spaltensummen
        R�ckgabewert 1, falls p1 = pv^T pi, 0 falls p1=0 und -1 sonst
Side-Effects:
Description:
Die Funktion berechnet aus einer Matrix p den Vektor der Zeilensummen und
der normierten Spaltensummen. 
Diagoinalelemente werden nicht ber�cksichtigt!!
Der R�ckgabewert zeigt an, ob P=pv^T * pi gilt.
***********************************************************************/
#ifndef CC_COMP
int mat_transform_matrix_to_vector(Sparse_matrix *p1, Double_vektor **pv, Double_vektor **pi) 
#else
int mat_transform_matrix_to_vector(p1, pv, pi)
  Sparse_matrix  *p1  ; 
  Double_vektor **pv  ;
  Double_vektor **pi  ;
#endif
{
  int     i, j, k ;
  double *rh, *ch, sum ;

  /* Speicherplatz allokieren */
  *pv = mat_double_vektor_new(p1->ord) ;
  rh = (*pv)->vektor ;
  *pi = mat_double_vektor_new(p1->ord) ;
  ch = (*pi)->vektor ;
  for (i = 0; i < p1->ord; i++) {
      for (j = 0; j < (p1->rowind[i]).no_of_entries; j++) {
	  rh[i] += (p1->rowind[i]).val[j] ;
	  ch[(p1->rowind[i]).colind[j]] += (p1->rowind[i]).val[j] ; 
      }
  }
  /* Normieren der Spaltensummen */
  sum = 0.0 ;
  for (i = 0; i < p1->ord; i++)
      sum += ch[i] ;
  if (sum != 0.0) {
      sum = 1.0 / sum ;
      for (i = 0; i < p1->ord; i++)
	  ch[i] *= sum ;
  }
  else {
      return(0) ;
  }
  /* Test, ob jede Zeil von der Form a*pi f�r eine KOnstante a ist.
     Falls dies der Fall ist 1 zur�ckgeben, ansonsten -1 zur�ckgeben */
  for (i = 0; i < p1->ord; i++) {
      if ((p1->rowind[i]).no_of_entries > 0) {
          /* Der erste Wert bestimmt die Normierung der Zeile.
             Falls der Wert im Vektor 0 ist, wurde eine Diskrepanz gefunden,
             ansonsten muss weitergesucht werden */
	  if (ch[(p1->rowind[i]).colind[0]] == 0.0) {
	      return(-1) ;
	  }
	  else {
	      sum = (p1->rowind[i]).val[0] / ch[(p1->rowind[i]).colind[0]] ;
	  }
          /* Teste alle Elemente der Zeile, wenn eine Diskrepanz festgestellt wurde, gib -1 zur�ck */
	  k = 0 ;
	  for (j = 0; j < (p1->rowind[i]).no_of_entries; j++) {
              /* Da sp�rlich besetzte Matrizen gespeichert werden, muss getestet werden, ob
                 ausgelassene Element im vektor 0.0 sind */
	      while (k < (p1->rowind[i]).colind[j]) {
		  if (ch[k] != 0.0)
		      return(-1) ;
                  else
		      k++ ;
	      }
              /* Element j der Zeile tesen */
	      if (fabs((p1->rowind[i]).val[j] -  ch[(p1->rowind[i]).colind[j]]*sum) > THRESHOLD)
		  return(-1) ;
	      else 
		  k++ ;
	  }
          /* Eventuelle noch fehlende Elemente in der Zeile im Vektor testen */
	  while (k < p1->ord - 1) {
	      if (ch[k] != 0.0)
		  return(-1) ;
              else
		  k++ ;
	  }
      }
  }
  return(1) ;
} /*  mat_transform_matrix_to_vector */

/**********************
mat_column_vector_to_matrix
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  p
Output: R�ckgabewert erzeugte Matrix
Side-Effects:
Description:
Die Funktion transformiert einen Spaltenvektor in eine Matrix.
***********************************************************************/
#ifndef CC_COMP
Sparse_matrix *mat_column_vector_to_matrix (Double_vektor *p) 
#else
Sparse_matrix *mat_column_vector_to_matrix (p)
  Double_vektor *p  ;
#endif
{
    int i;
    Sparse_matrix *q ;

    q = mat_sparse_matrix_new(p->no_of_entries) ;
    for (i = 0; i < q->ord; i++) {
      if (p->vektor[i] != 0.0 ) {
	(q->rowind[i]).no_of_entries = 1 ;
	(q->rowind[i]).colind = (size_t *) ecalloc (1, sizeof(size_t)) ;
	(q->rowind[i]).val = (double *) ecalloc (1, sizeof(double)) ;
	(q->rowind[i]).colind[0] = 0 ;
	(q->rowind[i]).val[0] = p->vektor[i] ;
      }
    }
    return(q) ;
} /* mat_row_vector_to_matrix */


/**********************
mat_row_vector_to_matrix
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  p
Output: R�ckgabewert erzeugte Matrix
Side-Effects:
Description:
Die Funktion transformiert einen Zeilenvektor in eine Matrix.
***********************************************************************/
#ifndef CC_COMP
Sparse_matrix *mat_row_vector_to_matrix (Double_vektor *p) 
#else
Sparse_matrix *mat_row_vector_to_matrix (p)
  Double_vektor *p  ;
#endif
{
  int i, k, nz ;
    Sparse_matrix *q ;

    q = mat_sparse_matrix_new(1) ;
    nz = 0 ;
    for (i = 0; i < p->no_of_entries; i++)
	if(p->vektor[i] != 0.0)
	    nz++ ;
    (q->rowind[0]).no_of_entries = nz ;
    (q->rowind[0]).colind = (size_t *) ecalloc (nz, sizeof(size_t)) ;
    (q->rowind[0]).val = (double *) ecalloc (nz, sizeof(double)) ;
    k = 0 ;
    for (i = 0; i < p->no_of_entries; i++) {
      if (p->vektor[i] != 0.0) {
	(q->rowind[0]).colind[k] = i ;
	(q->rowind[0]).val[k] = p->vektor[i] ;
	k++ ;
      }
    }
    return(q) ;
} /* mat_row_vector_to_matrix */

/**********************
mat_transform_vectors_to_matrix
Datum:  09.02.03
Autor:  Peter Buchholz
Input:  p1, p2 Eingabevektoren
Output: R�ckgabewert erzeugte Matrix
Side-Effects:
Description:
Die Funktion berechnet P = p1^T * p2 und gibt resultierende Matrix als Funktionswert zur�ck.
***********************************************************************/
#ifndef CC_COMP
Sparse_matrix *mat_transform_vectors_to_matrix (Double_vektor *p1, Double_vektor *p2) 
#else
Sparse_matrix *mat_transform_vectors_to_matrix (p1, p2)
  Double_vektor *p1  ;
  Double_vektor *p2  ;
#endif
{
    int i, j, k, nz ;
    Sparse_matrix *q ;

    q = mat_sparse_matrix_new(p1->no_of_entries) ;
    nz = 0 ;
    for (i = 0; i < p2->no_of_entries; i++)
	if(p2->vektor[i] != 0.0)
	    nz++ ;
    for (i = 0; i < q->ord; i++) {
	if (p1->vektor[i] != 0.0) {
	    (q->rowind[i]).no_of_entries = nz ;
	    (q->rowind[i]).colind = (size_t *) ecalloc (nz, sizeof(size_t)) ;
	    (q->rowind[i]).val = (double *) ecalloc (nz, sizeof(double)) ;
	    k = 0 ;
	    for (j = 0; j < p2->no_of_entries; j++) {
		if (p2->vektor[j] != 0.0) {
		    (q->rowind[i]).colind[k] = j ;
		    (q->rowind[i]).val[k] = p1->vektor[i] * p2->vektor[j] ;
		    k++ ;
		}
	    }
	}
	else {
	    (q->rowind[i]).no_of_entries = 0 ;
	}
    }
    return(q) ;
} /* mat_transform_vectors_to_matrix */

/**********************
mat_expand_matrix 
Datum:  7.5.98
Autor:  Peter Buchholz
Input:  q Sparse Matrix
        col Spaltendimension
        l, u Dimension der linken, rechtenIdentitaetsmatrix 
Output: Funktionswert expandierte Matrix
Side-Effects:
Description:
Die Funktion expandiert eine Matrix q zu I_l \otimes q \times I_u.
***********************************************************************/
#ifndef CC_COMP
Sparse_matrix *mat_expand_matrix (Sparse_matrix *q, int cols, int l, int u) 
#else
Sparse_matrix *mat_expand_matrix (q, cols, l, u) 
  Sparse_matrix *q    ;
  int            cols ;
  int            l    ;
  int            u    ;
#endif
{
  int            i, li, ui, j, leng, k;
  Sparse_matrix *qe        ;

  qe = mat_sparse_matrix_new(q->ord * l * u) ;
  k = 0 ;
  for (li = 0; li < l; li++)
  for (i = 0; i < q->ord; i++)
  {
    leng = (q->rowind[i]).no_of_entries ;
    for (ui = 0; ui < u; ui++)
    {
      qe->diagonals[k] = q->diagonals[i] ;
      (qe->rowind[k]).no_of_entries = leng ;
      (qe->rowind[k]).colind = (size_t *) ecalloc (leng, sizeof(size_t)) ;
      (qe->rowind[k]).val = (double *) ecalloc (leng, sizeof(double)) ;
      for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
      {
        (qe->rowind[k]).colind[j] = 
         (q->rowind[i]).colind[j] * u + li * cols * u + ui ;
        (qe->rowind[k]).val[j] = (q->rowind[i]).val[j] ;
      }
      k++ ;
    }
  }
  return(qe) ;
} /* mat_expand_matrix */

/**********************
mat_sparse_matrix_row_sum
Datum:  12.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
        diag treu falls Diagonalelement ber�cksichtigt werden
Output: rs row sums
Side-Effects:
Description:
Die Funktion berechnet die Zielensummen der Matrix q. Falls diag=true,
werden Diagonalelemente ber�cksichtigt, ansonsten nicht.
Die Summe werden in den Vektor rs geschrieben, der die richtige
L�nge haben muss.
***********************************************************************/
void mat_sparse_matrix_row_sum (Sparse_matrix *q, Double_vektor *rs, short diag) 
{
    int i, j ;

    if (q->ord != rs->no_of_entries) {
	printf("mat_sparse_matrix_row_sum matrix dim %i vector length %i\nSTOP EXECUTION\n",
	       q->ord, rs->no_of_entries) ;
	exit(-1) ;
    }
    for (i = 0; i < q->ord; i++) {
	if (diag) 
	    rs->vektor[i] = q->diagonals[i] ;
	else
	    rs->vektor[i] = 0.0 ;
	for (j = 0; j < (q->rowind[i]).no_of_entries; j++) {
	    rs->vektor[i] += (q->rowind[i]).val[j] ;
	}
    }
} /* mat_sparse_matrix_row_sum */

/**********************
mat_sparse_matrix_upper_triangular
Datum:  12.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: TRUE q is upper triangular, 
        FALSE otherwhise
Side-Effects:
Description:
Die Funktion �berpr�ft, ob q eine obere Dreiecksmatrix ist. 
***********************************************************************/
short mat_sparse_matrix_upper_triangular(Sparse_matrix *q) 
{
    int i, j ;

    for (i = 1; i < q->ord; i++) {
	for (j = 0; j < (q->rowind[i]).no_of_entries; j++) {
	  if ((q->rowind[i]).colind[j] < i)
	    return(FALSE) ;
	}
    }
    return(TRUE) ;
} /* mat_sparse_matrix_upper_triangular */


/**********************
mat_sparse_matrix_transform_stochastic
Datum:  12.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: q transformierte Matrix
Side-Effects:
Description:
Die Funktion transformiert die Matrix q in eine (sub)stochastische Matrix mit Hilfe von alpha.
Falls alpha nicht gro� genug gew�hlt wurde, wird der Wert auf das betragsm��ig gr��te
Diagonalelement gesetzt. 
***********************************************************************/
extern void mat_sparse_matrix_transform_stochastic (Sparse_matrix *q, double *alpha)
{
  int i, j ;
  double val ;

  val = mat_sparse_matrix_max_elem(q) ;
  if (val > *alpha)
    *alpha = val ;
  else
    val = *alpha ;
  printf("val %.3e\n", val) ;
  val = 1.0 / val ;
  for (i = 0; i < q->ord; i++) {
    for (j = 0; j < (q->rowind[i]).no_of_entries; j++) 
      (q->rowind[i]).val[j] *= val ;
    q->diagonals[i] *= val ;
    q->diagonals[i] += 1.0 ;
  }
} /* mat_sparse_matrix_transform_stochastic */

/**********************
mat_sparse_matrix_transform_rate
Datum:  12.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
Output: q transformierte Matrix
Side-Effects:
Description:
Die Funktion transformiert die (sub)stochastische Matrix q in eine Ratenmatrix.
***********************************************************************/
void mat_sparse_matrix_transform_rate (Sparse_matrix *q, double alpha) 
{
    int i, j ;

    for (i = 0; i < q->ord; i++) {
	q->diagonals[i] = (q->diagonals[i] - 1.0) * alpha ;
	for (j = 0; j < (q->rowind[i]).no_of_entries; j++) {
	    (q->rowind[i]).val[j] *= alpha ;
	}
    }
} /* mat_sparse_matrix_transform_rate */

/**********************
mat_sparse_matrix_add_eps
Datum:  15.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
        eps Elemnt, das dazu addiert wird
        diag_eps Element, das zur Diagonalen dazu addiert wird
Output: q transformierte Matrix
Side-Effects:
Description:
Die Funktion addiert eps zu allen Nichtdiagonalelemente und diag_eps zu allen
Diagonalelementen
***********************************************************************/
void mat_sparse_matrix_add_eps (Sparse_matrix *q, double eps, double diag_eps)
{
    size_t     i, j, k, *colind;
    double *val ;

    for (i = 0; i < q->ord; i++) {
	q->diagonals[i] += diag_eps ;
	colind = (size_t *) ecalloc(q->ord-1, sizeof(size_t)) ;
	val = (double *) ecalloc(q->ord-1, sizeof(double)) ;
	for (j = 0; j < q->ord-1; j++) {
	    val[j] = eps ;
	    if (j >= i) 
		colind[j] = j+1 ;
	    else
		colind[j] = j ;
	}
	for (j = 0; j < (q->rowind[i]).no_of_entries; j++) {
	    k = (q->rowind[i]).colind[j] ;
	    if (k >= i)
		k-- ;
	    val[k] += (q->rowind[i]).val[j] ;
	}
	efree((q->rowind[i]).colind) ;
	efree((q->rowind[i]).val) ;
	(q->rowind[i]).colind = colind ;
	(q->rowind[i]).val = val ;
	(q->rowind[i]).no_of_entries = q->ord - 1 ;
    } /* for i */
}

/**********************
mat_add_submatrix
Datum:  5.7.95
Autor:  Peter Buchholz
Input:  q Ausgangsmatrix
        qh zu addierende Submatrix
        ro, co Spalten und Zeilenoffset
        s Skalarwert
Output: q mit addiertem Block s*qh
Side-Effects:
Description:
Die Funktion addiert die Untermatrix qh multipliziert mit dem Skalar s zu Matrix q. 
Die Position wird durch den Zeilenoffset ro und Spaltenoffset co gegeben.
VorsichtDiagonalelemente muessen einsortiert sein.
***********************************************************************/
#ifndef CC_COMP
void mat_add_submatrix(Sparse_matrix *q, Sparse_matrix *qh, int ro, int co, double s) 
#else
void mat_add_submatrix(q, qh, ro, co, s) 
  Sparse_matrix *q    ;
  Sparse_matrix *qh   ;
  int            ro   ;
  int            co   ;
  double         s    ;
#endif
{
  int     i, j, ih, jh, c ;
  double *values  ;
  
  /* Teste die Konsistenz */
  if (qh == NULL)
    return ;
  if (q == NULL || q->ord < qh->ord + ro)
  {
    printf("Fatal error: Wrong matrix dimension in expansion\nSTOP EXECUTION\n") ;
    printf("q rows %i qh rows %i row offset %i\n", q->ord, qh->ord, ro) ;
    exit(-1) ;
  }
  /* Bestimme den maximalen Spaltenindex */
  c =  0 ;
  for (i = 0; i < q->ord; i++)
  for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
  if ((q->rowind[i]).colind[j] > c)
    c = (q->rowind[i]).colind[j] ;
  for (i = 0; i < qh->ord; i++)
  for (j = 0; j < (qh->rowind[i]).no_of_entries; j++)
  if ((qh->rowind[i]).colind[j] + co > c)
    c = (qh->rowind[i]).colind[j] + co ;
  c++ ;
  values = (double *) ecalloc(c, sizeof(double)) ;
  /* Addiere die Submatrix qh zu q */ 
  for (ih = 0; ih < qh->ord; ih++)
  if ((qh->rowind[ih]).no_of_entries > 0)
  {
    i = ih + ro ;
    if ((q->rowind[i]).no_of_entries == 0)
    {
      (q->rowind[i]).val = 
        (double *) ecalloc ((qh->rowind[ih]).no_of_entries, sizeof(double)) ;
      (q->rowind[i]).colind = 
        (size_t *) ecalloc ((qh->rowind[ih]).no_of_entries, sizeof(size_t)) ;
      (q->rowind[i]).no_of_entries = (qh->rowind[ih]).no_of_entries ;
      for (jh = 0; jh < (qh->rowind[ih]).no_of_entries; jh++)
      {
        (q->rowind[i]).val[jh]    = (qh->rowind[ih]).val[jh] * s     ;
        (q->rowind[i]).colind[jh] = (qh->rowind[ih]).colind[jh] + co ;
      }
    }
    else
    {
      for (j = 0; j < (q->rowind[i]).no_of_entries; j++)
        values[(q->rowind[i]).colind[j]] += (q->rowind[i]).val[j] ;
      for (jh = 0; jh < (qh->rowind[ih]).no_of_entries; jh++)
        values[(qh->rowind[ih]).colind[jh] + co] += (qh->rowind[ih]).val[jh] * s ;
      jh = 0 ;
      for (j = 0; j < c; j++)
      if (values[j] != 0.0)
        jh++ ;
      if (jh != (q->rowind[i]).no_of_entries)
      {
         efree((q->rowind[i]).val)    ;
         efree((q->rowind[i]).colind) ;
         (q->rowind[i]).val    = (double *) ecalloc (jh, sizeof(double));
         (q->rowind[i]).colind = (size_t *) ecalloc (jh, sizeof(size_t));
         (q->rowind[i]).no_of_entries = jh ;
      }
      jh = 0 ;
      for (j = 0; j < c; j++)
      if (values[j] != 0.0)
      {
        (q->rowind[i]).val[jh]    = values[j] ;
        (q->rowind[i]).colind[jh] = j         ;
        values[j]                 = 0.0       ;
        jh++ ;
        if (jh == (q->rowind[i]).no_of_entries)
          break ;
      }
    }
  }
  efree(values) ;
} /* mat_add_submatrix */

/**********************
mat_sparse_matrix_tensor_product
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  q1, q2 Sparse Matrix
Output: Kroneckerprodukt von q1 und q2
Side-Effects:
Description:
Die Funktion berechnet das Kroneckerprodukt zweier Matrizen.
Die Diagonalelemente muessen in die Zeilen einsortiert sein!
***********************************************************************/
Sparse_matrix *mat_sparse_matrix_tensor_product (Sparse_matrix *q1, Sparse_matrix *q2, int q2col)
{
  int i, j ;
  Sparse_matrix *q0 ;

  q0 = mat_sparse_matrix_new(q1->ord*q2->ord) ;
  for (i = 0; i < q1->ord; i++)
    for (j = 0; j < (q1->rowind[i]).no_of_entries; j++)
      mat_add_submatrix (q0, q2, i*q2->ord, 
			 (q1->rowind[i]).colind[j] * q2col, (q1->rowind[i]).val[j]);
  return(q0) ; 
} /* mat_sparse_matrix_tensor_product */

/**********************
mat_sparse_matrix_add_elem
Datum:  25.02.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix
        ri, ci Zeilen- und Spaltenindex
        val Wert
        diag bei TRUE werden die Diagonalelement in diagonals gespeichert, sonst in der Zeile
Output: Matrix mit Q(ri, ci) += val 
Side-Effects:
Description:
Die Funktion addiert val zum Element ri,ci der Matrix. Falls das Element nicht vorhanden ist, wir es neu erzeugt.
***********************************************************************/
void mat_sparse_matrix_add_elem (Sparse_matrix *q, int ri, int ci, double val, short diag)
{
  short found=FALSE ;
  size_t i, j, *columns ;
  double *values ;

  if (ri > q->ord) {
    printf("Wrong index in mat_sparse_matrix_add_elem\nSTOP EXECUTION\n") ;
    exit(-1) ;
  }
  if (diag && ri == ci) {
    q->diagonals[ri] += val ;
  }
  else {
    for (i = 0; i < (q->rowind[ri]).no_of_entries; i++) {
      if ((q->rowind[ri]).colind[i] == ci) {
	(q->rowind[ri]).val[i] += val ;
	found = TRUE ;
	break ;
      }
    }
    if (!(found)) {
	/* A new row has to be build */
	columns = (size_t *) ecalloc((q->rowind[ri]).no_of_entries+1, sizeof(size_t)) ;
	values = (double *) ecalloc((q->rowind[ri]).no_of_entries+1, sizeof(double)) ;
	j = 0 ;
	i = 0 ;
	while (i < (q->rowind[ri]).no_of_entries) {
	    if ((q->rowind[ri]).colind[i] < ci || found) {
		columns[j] = (q->rowind[ri]).colind[i] ;
		values[j] = (q->rowind[ri]).val[i] ;
		j++ ;
		i++ ;
	    }
	    else {
		columns[j] = ci ;
		values[j] = val ;
		j++ ;
		found = TRUE ;
	    }
	}
	if (!(found)) {
	    j = (q->rowind[ri]).no_of_entries ;
	    columns[j] = ci ;
	    values[j] = val ;
	}
	free((q->rowind[ri]).colind) ;
	(q->rowind[ri]).colind = columns ;
	free((q->rowind[ri]).val) ;
	(q->rowind[ri]).val = values ;
	(q->rowind[ri]).no_of_entries++ ;
    }
  }
} /*  mat_sparse_matrix_add_elem */


/**********************
mat_sparse_matrix_add_identity
Datum:  21.11.10
Autor:  Peter Buchholz
Input:  q Sparse Matrix


Output: 
Side-Effects:
Description:
Die Funktion ersetzt die letzte Spalte von q durch den Einheitsvektor.
***********************************************************************/
void mat_sparse_matrix_add_identity(Sparse_matrix *q)
{
  size_t i, j, *cols ;
  double *val ;

  for (i = 0; i < q->ord; i++) {
    if ((q->rowind[i]).no_of_entries > 0) {
      if ((q->rowind[i]).colind[(q->rowind[i]).no_of_entries -1] == q->ord-1) {
	(q->rowind[i]).val[(q->rowind[i]).no_of_entries - 1] = 1.0 ; 
      }
      else {
	cols = (size_t *) ecalloc((q->rowind[i]).no_of_entries+1, sizeof(size_t)) ;
	val = (double *) ecalloc((q->rowind[i]).no_of_entries+1, sizeof(double)) ;
	for (j = 0; j < (q->rowind[i]).no_of_entries; j++) {
	  cols[j] = (q->rowind[i]).colind[j] ;
	  val[j] = (q->rowind[i]).val[j] ;
	}
	cols[(q->rowind[i]).no_of_entries] = q->ord - 1 ;
	val[(q->rowind[i]).no_of_entries] = 1.0 ;
	efree((q->rowind[i]).colind) ;
	(q->rowind[i]).no_of_entries++ ;
	(q->rowind[i]).val = val ;
	(q->rowind[i]).colind = cols ;
      }
    }
    else {
      (q->rowind[i]).colind = (size_t *) ecalloc(1, sizeof(size_t)) ;
      (q->rowind[i]).colind[0] = q->ord-1 ;
      (q->rowind[i]).val    = (double *) ecalloc(1, sizeof(double)) ;
      (q->rowind[i]).val[0] = 1.0 ;
      (q->rowind[i]).no_of_entries = 1 ;
    } /* no_of_entries == 0 */
  }
} /* mat_sparse_matrix_add_identity */


/**********************
mat_sparse_matrix_identical
Datum:  21.11.10
Autor:  Peter Buchholz
Input:  q1, q2 Sparse Matrix
        threshold maximaler Unterschied in den Elementen

Output: Rueckgabewert TRUE or FALSE
Side-Effects:
Description:
Die Funktion untersucht, ob zwei Matrizen identisch sind.
***********************************************************************/
int mat_sparse_matrix_identical(Sparse_matrix *q1, Sparse_matrix *q2, double threshold)
{
  int i, j ;

  if (q1 == q2) 
    return(TRUE) ;
  if (q1->ord != q2->ord)
    return(FALSE) ;
  if (q1->n_of_nonzeros != q2->n_of_nonzeros)
    return(FALSE) ;
  if (q1->bandwidth != q2->bandwidth)
    return(FALSE) ;
  if (q1->diagmax != q2->diagmax)
    return(FALSE) ;
  if (q1->diagmin != q2->diagmin)
    return(FALSE) ;
  if (q1->active != q2->active)
    return(FALSE) ;
  for (i = 0; i < q1->ord; i++) {
    if ((q1->rowind[i]).no_of_entries != (q2->rowind[i]).no_of_entries)
      return(FALSE) ;
    if (fabs(q1->diagonals[i] - q2->diagonals[i]) > threshold)
      return(FALSE) ;
    for (j = 0; j < (q1->rowind[i]).no_of_entries; j++) {
      if((q1->rowind[i]).colind[j] != (q2->rowind[i]).colind[j])
	return(FALSE) ;
      if(fabs((q1->rowind[i]).val[j] - (q2->rowind[i]).val[j]) > threshold)
	return(FALSE) ;
    } /* for j */
  } /* for i */
  return(TRUE) ;
} /* mat_sparse_matrix_identical */

/**********************
mat_sparse_matrix_is_smaller
Datum:  1.3.15
Autor:  Peter Buchholz
Input:  q1, q2 Sparse Matrix
        threshold maximaler Unterschied in den Elementen

Output: Rueckgabewert TRUE or FALSE
Side-Effects:
Description:
Die Funktion untersucht, ob q1 - threshold <= q2 ist.
***********************************************************************/
int mat_sparse_matrix_is_smaller(Sparse_matrix *q1, Sparse_matrix *q2, double threshold, short with_diag)
{
    int i, k1, k2 ;

    if (q1 == q2) {
        return(TRUE) ;
    }
    if (q1->ord != q2->ord) {
        printf("Order violated!\n");
        return(FALSE) ;
    }
    for (i = 0; i < q1->ord; i++) {
        if (with_diag) {
            if (q1->diagonals[i] - threshold > q2->diagonals[i]) {
                printf("Violation at %d, %d!\n", i, i);
                return(FALSE) ;
            }
        }
        if ((q1->rowind[i]).no_of_entries > (q2->rowind[i]).no_of_entries) {
            printf("Order violation, found %zu entries in the first matrix and only %zu entries in the second matrix\n",
                (q1->rowind[i]).no_of_entries, (q2->rowind[i]).no_of_entries);
            for (size_t j = 0; j < (q1->rowind[i]).no_of_entries; ++j) {
                printf("A[%d, %zu] = %f\n", i, (q1->rowind[i]).colind[j], (q1->rowind[i]).val[j]);
            }
            for (size_t j = 0; j < (q2->rowind[i]).no_of_entries; ++j) {
                printf("B[%d, %zu] = %f\n", i, (q2->rowind[i]).colind[j], (q2->rowind[i]).val[j]);
            }
            return(FALSE) ;
        }
        k1 = k2 = 0 ;
        while (k1 < (q1->rowind[i]).no_of_entries &&  k2 < (q2->rowind[i]).no_of_entries) {
            if ((q1->rowind[i]).colind[k1] < (q2->rowind[i]).colind[k2]) {
                printf("Order violation, entry at %zu in the first matrix while entry at %zu was expected\n",
                       (q1->rowind[i]).colind[k1],
                       (q2->rowind[i]).colind[k2]);
                return(FALSE) ;
            }
            if ((q1->rowind[i]).colind[k1] > (q2->rowind[i]).colind[k2]) {
                k2++ ;
            } else {
                if ((q1->rowind[i]).val[k1] - threshold > (q2->rowind[i]).val[k2]) {
                    printf("Violation at %d, %zu!\n", i, q1->rowind[i].colind[k2]);
                    return(FALSE) ;
                }
                k1++ ;
                k2++ ;
            }
        }
    } /* for i */
    return(TRUE) ;
} /* mat_sparse_matrix_is_smaller */

/**
 * Werte bestimmen.
 * Achtung: colind muss aufsteigend sortiert sein !!!
 * \date 9.7.95
 * \author Peter Buchholz
 * \param q ???
 * \param diagmax ???
 * \param diagmin ???
 * \param bandwidth ???
 * \return ???
 */
int mat_sparse_matrix_diagmax_diagmin_bandwidth(Sparse_matrix *q_nm, 
                                                double *diagmax, 
                                                double *diagmin, 
                                                unsigned *bandwidth)
{
  int i,j ;
  Row *r ;

  assert(q_nm && q_nm->diagonals && q_nm->rowind) ;
  r = q_nm->rowind ;
  *diagmax = 0.0 ;
  *diagmin = q_nm->diagonals[0] ;
  *bandwidth = 0 ;
  for (i = 0  ; i < q_nm->ord ; i++ )
    {
      if (q_nm->diagonals[i] < *diagmin)
        *diagmin = q_nm->diagonals[i] ;

      if (q_nm->diagonals[i] > *diagmax)
        *diagmax = q_nm->diagonals[i] ;

      if (0 < r[i].no_of_entries)
        {
          j = r[i].colind[r[i].no_of_entries-1] - r[i].colind[0] + 1  ;
          assert(0 <= j) ; /* colinds sollten aufsteigend sortiert sein */
          if (q_nm->bandwidth < (unsigned)j)
            q_nm->bandwidth = (unsigned)j ;
        }
    } 
  return(NO_ERROR); 
}

/**
 * fuehrt Reindizierung und Verkleinerung durch.
 * dient der Verkleinerung von Matrizen bzgl erreichbarer Teilraueme
 * Achtung: relation muss ordnungsvertraeglich sein, Bildmenge muss mit kleinerer
 * Indexmenge auskommen.
 * \date 6.3.00
 * \author Peter Kemper
 * \param q zu modifizierende Matrix
 * \param relation Vektor der Laenge der Anzahl Matrixzeilen
 * \param new_dim Anzahl Zeilen der gekuerzten Matrix
 * \return ???
 */
int mat_sparse_matrix_update_index(Sparse_matrix *q, int *relation, int new_dim)
{
  int i ;


  /* Behandlung der Matrixzeilen */
  for (i=0 ; i < q->ord ; i++)
    { /* i beschreibt den aktuellen Zustand */
      /* ungenutzte Zeilen freigeben */
      if (q->ord == relation[i] && q->rowind[i].no_of_entries != 0)
	{
	  if (q->rowind[i].colind)
	    efree( q->rowind[i].colind) ;
	  if (q->rowind[i].val)
	    efree( q->rowind[i].val) ;
	  q->rowind[i].no_of_entries = 0 ;
	}

      /* wg Ordnungsvertraeglichkeit der Abbildung/Reindizierung koennen Matrixzeilen
	 direkt verschoben werden */
      if (q->ord != relation[i])
	{
	  /* Zeileneintraeg bzgl der colind aktualisieren */
	  if (0 < q->rowind[i].no_of_entries)
	    mat_row_update(&(q->rowind[i]), q->ord, relation, new_dim) ;
  	  q->rowind[relation[i]] = q->rowind[i] ; /* cp Inhalt des Structs,
						     Zeile innerhalb der Matrix verschieben */ 
	}
    }

  /* Behandlung der Diagonaleintraege */
  if (q->diagonals)
    {
      for (i=0 ; i < q->ord ; i++)
	{ /* i beschreibt den aktuellen Zustand */
	  if (q->ord != relation[i])
	    { /* Zustand ist relevant */
	      q->diagonals[relation[i]] = q->diagonals[i] ; 
	    }
	  else
	    { /* Zustand ist irrelevant, wg Ordnungsvertraeglichkeit der Abbildung
	         kann hierdurch nichts ueberschrieben werden */
	      q->diagonals[i] = 0.0 ;
	    } 
	}
    }

  /* abschliessend Dimension aktualisieren */
  q->ord = new_dim ;
  return(NO_ERROR) ;

}

/**
 * \date 7.4.99
 * \author Peter Kemper
 * \param q Sparse_matrix
 * \return  Space used for complete data structurs
 */
int mat_sparse_matrix_space(Sparse_matrix *q)
{
  int space  = (int)sizeof(Sparse_matrix) ;
  
  if (q->diagonals)
    space += (int)(q->ord * sizeof(double)) ;
  if (q->rowind)
    space += (int)( (q->ord * sizeof(Row)) 
		   + q->n_of_nonzeros * (sizeof(double) + sizeof(size_t))
		   ) ;
  return(space) ;
}

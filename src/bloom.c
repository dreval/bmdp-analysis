#include "bloom.h"
#include "ememory.h"
#include <math.h>
#include <stdlib.h>
#include <assert.h>

bloom_filter bloom_filter_new(size_t num_hash_functions, hash_function *hash_functions, size_t size) {
    bloom_filter filter;

    filter.num_hash_functions = num_hash_functions;
    filter.hash_functions = hash_functions;
    unsigned long num_bytes = (size / BYTE_WIDTH) + ((size % BYTE_WIDTH == 0) ? 0 : 1);
    filter.bitmap = ecalloc(num_bytes, 1);

    return filter;
}

bloom_filter bloom_filter_new_auto(size_t num_hash_functions, hash_function *hash_functions, size_t num_items, double false_positive_rate) {
    // first: calculate the Bloom filter size
    size_t size = (size_t) ceil(- (num_items * log(false_positive_rate)) / pow(log(2.0), 2));
    return bloom_filter_new(num_hash_functions, hash_functions, size);
}

void bloom_filter_add(bloom_filter filter, void *item, size_t ord){
    size_t i;
    for(i = 0; i < filter.num_hash_functions; ++i) {
        /* int (*hash_function) (void *) = filter.hash_functions[i]; */
        size_t index = universal_hash_function(item, ord, i);
        bit_set(filter.bitmap, index, 1);
    }
}

int bloom_filter_query(bloom_filter filter, void *item, size_t ord){
    size_t i;
    for(i = 0; i < filter.num_hash_functions; ++i) {
        /*
        int (*hash_function) (void *) = (int (*) (void *)) filter.hash_functions[i];
        */
        size_t index = universal_hash_function(item, ord, i);
        if(!bit_query(filter.bitmap, index)) {
            return 0;
        }
    }
    return 1;
}

int bit_query(char *bitmap, size_t index){
    char mask;
    size_t byte_index = bitmap_index(bitmap, index, &mask);
    return (bitmap[byte_index] & mask) ? 1 : 0;
}

void bit_set(char *bitmap, size_t index, int value){
    char mask;
    size_t byte_index = bitmap_index(bitmap, index, &mask);
    if(value) {
        bitmap[byte_index] |= mask;
    } else {
        bitmap[byte_index] &= (~mask);
    }
}

size_t bitmap_index(char *bitmap, size_t index, char *mask){
    size_t byte_index = index / BYTE_WIDTH;
    size_t rest = index % BYTE_WIDTH;
    *mask = 1 << rest;
    return byte_index;
}

size_t universal_hash_function(size_t *policy, size_t ord, size_t index) {
    size_t i;
    size_t shift = (sizeof(size_t) * 8 - 25);
    size_t hash_max = 1 << 25;

    size_t cumulative_sum = 0;
    for(i = 0; i < ord; ++i) {
        size_t a = hash_coeff[index][i * 2] % hash_max;
        size_t b = hash_coeff[index][i * 2 + 1];
        unsigned long long madd = a * policy[i] + b;
        unsigned int h = ((unsigned int) madd) >> shift;
        /* assert(h >= 0); */
        cumulative_sum = (cumulative_sum + h) % hash_max;
    }
    return cumulative_sum;
}

size_t hash_func_1(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 0);
}

size_t hash_func_2(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 1);
}

size_t hash_func_3(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 2);
}

size_t hash_func_4(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 3);
}

size_t hash_func_5(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 4);
}

size_t hash_func_6(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 5);
}

size_t hash_func_7(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 6);
}

size_t hash_func_8(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 7);
}

size_t hash_func_9(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 8);
}

size_t hash_func_10(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 9);
}

size_t hash_func_11(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 10);
}

size_t hash_func_12(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 11);
}

size_t hash_func_13(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 12);
}

size_t hash_func_14(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 13);
}

size_t hash_func_15(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 14);
}

size_t hash_func_16(void *policy, size_t ord) {
    return universal_hash_function(policy, ord, 15);
}

void init_hash_functions(size_t ord){
    size_t i;
    size_t j;
    int shift = RAND_MAX / (1 << 25) + 1;
    srand(42);
    hash_coeff = ecalloc(num_hash_functions, sizeof(size_t *));
    for(i = 0; i < num_hash_functions; ++i) {
        hash_coeff[i] = ecalloc(2 * ord, sizeof(int));
        for(j = 0; j < ord; ++j) {
            hash_coeff[i][j * 2] = rand() / 2 * 2 + 1;
            hash_coeff[i][j * 2 + 1] = rand() / shift;
        }
    }

    hash_functions = ecalloc(num_hash_functions, sizeof(void *));
    hash_functions[0] = hash_func_1;
    hash_functions[1] = hash_func_2;
    hash_functions[2] = hash_func_3;
    hash_functions[3] = hash_func_4;
    hash_functions[4] = hash_func_5;
    hash_functions[5] = hash_func_6;
    hash_functions[6] = hash_func_7;
    hash_functions[7] = hash_func_8;
    hash_functions[8] = hash_func_9;
    hash_functions[9] = hash_func_10;
    hash_functions[10] = hash_func_11;
    hash_functions[11] = hash_func_12;
    hash_functions[12] = hash_func_13;
    hash_functions[13] = hash_func_14;
    hash_functions[14] = hash_func_15;
    hash_functions[15] = hash_func_16;
}

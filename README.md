This repository contains a tool for BMDP analysis.

The state of the tool is the following:
+ min/max/avg optimization wrt expected discounted reward
+ Pareto optimization
+ Scalarization (Stochastic optimization)
+ Continuous stochastic optimization wrt uniform prior

